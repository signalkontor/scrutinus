!include MUI2.nsh

!define APP_NAME "TPS.net 2019"
!define COMP_NAME "signalkontor GmbH"
!define WEB_SITE "http://www.signalkontor.com"
!define VERSION "03.01.308.00"
!define COPYRIGHT "signalkontor GmbH / Olav Groehn (c) 2019"
!define DESCRIPTION "Application"
!define INSTALLER_NAME "..\Setup\TpsnetInstaller.exe"
!define MAIN_APP_EXE "Scrutinus.exe"
!define INSTALL_TYPE "SetShellVarContext current"
!define REG_ROOT "HKCU"
!define REG_APP_PATH "Software\Microsoft\Windows\CurrentVersion\App Paths\${MAIN_APP_EXE}"
!define UNINSTALL_PATH "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APP_NAME}"


VIProductVersion  "${VERSION}"
VIAddVersionKey "ProductName"  "${APP_NAME}"
VIAddVersionKey "CompanyName"  "${COMP_NAME}"
VIAddVersionKey "LegalCopyright"  "${COPYRIGHT}"
VIAddVersionKey "FileDescription"  "${DESCRIPTION}"
VIAddVersionKey "FileVersion"  "${VERSION}"

######################################################################

SetCompressor ZLIB
Name "${APP_NAME}"
Caption "${APP_NAME}"
OutFile "${INSTALLER_NAME}"
BrandingText "${APP_NAME}"
XPStyle on
InstallDirRegKey "${REG_ROOT}" "${REG_APP_PATH}" ""
InstallDir "$PROGRAMFILES\TPSnet"

######################################################################

!define MUI_ABORTWARNING
!define MUI_UNABORTWARNING

!insertmacro MUI_PAGE_WELCOME

!ifdef LICENSE_TXT
!insertmacro MUI_PAGE_LICENSE "${LICENSE_TXT}"
!endif

!ifdef REG_START_MENU
!define MUI_STARTMENUPAGE_NODISABLE
!define MUI_STARTMENUPAGE_DEFAULTFOLDER "TPS8"
!define MUI_STARTMENUPAGE_REGISTRY_ROOT "${REG_ROOT}"
!define MUI_STARTMENUPAGE_REGISTRY_KEY "${UNINSTALL_PATH}"
!define MUI_STARTMENUPAGE_REGISTRY_VALUENAME "${REG_START_MENU}"
!insertmacro MUI_PAGE_STARTMENU Application $SM_Folder
!endif

!insertmacro MUI_PAGE_INSTFILES

!define MUI_FINISHPAGE_RUN "$INSTDIR\${MAIN_APP_EXE}"
!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_UNPAGE_CONFIRM

!insertmacro MUI_UNPAGE_INSTFILES

!insertmacro MUI_UNPAGE_FINISH

!insertmacro MUI_LANGUAGE "English"
!insertmacro MUI_LANGUAGE "German"

Section -MainProgram
${INSTALL_TYPE}
SetOverwrite ifnewer




### C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release,  Configuration: Release##

SetOutPath "$INSTDIR"

			
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\AsyncIO.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Common.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Common.dll.config";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\ControlzEx.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\CsQuery.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\DataModel.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\DataModel.dll.config";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\DtvEsvModule.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\DtvEsvModule.dll.config";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\EntityFramework.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\EntityFramework.SqlServer.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\EntityFramework.SqlServerCompact.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\EPPlus.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Exceptionless.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Exceptionless.Portable.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Exceptionless.Wpf.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\FtsRestAPI.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\FtsRestAPI.dll.config";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\GalaSoft.MvvmLight.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\GalaSoft.MvvmLight.Extras.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\General.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\GongSolutions.Wpf.DragDrop.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Helpers.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Helpers.dll.config";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\log4net.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\MahApps.Metro.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\MahApps.Metro.SimpleChildWindow.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Microsoft.Practices.ServiceLocation.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\MobileControlLib.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\MobileControlLib.dll.config";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\MoveResizeRotateWithAdorners.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\MoveResizeRotateWithAdorners.dll.config";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Nancy.Authentication.Forms.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Nancy.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Nancy.Hosting.Self.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Nancy.Testing.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Nancy.ViewEngines.Razor.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\NancyServer.exe";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\NancyServer.exe.config";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\NetMQ.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Newtonsoft.Json.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\NLog.config";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\NLog.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Scrutinus.exe";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Scrutinus.exe.config";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Scrutinus.vshost.exe";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Scrutinus.vshost.exe.config";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\System.Data.DataSetExtensions.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\System.Data.SqlServerCe.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\System.Net.Http.Extensions.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\System.Net.Http.Primitives.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\System.Reactive.Core.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\System.Reactive.Interfaces.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\System.Reactive.Linq.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\System.Reactive.PlatformServices.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\System.Reactive.Windows.Threading.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\System.Web.Razor.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\System.Windows.Interactivity.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\TPSModel.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Wdsf.Api.Client.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\WPFLocalizeExtension.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\XAMLMarkupExtensions.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Xceed.Wpf.AvalonDock.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Xceed.Wpf.AvalonDock.Themes.Aero.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Xceed.Wpf.AvalonDock.Themes.Metro.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Xceed.Wpf.AvalonDock.Themes.VS2010.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Xceed.Wpf.DataGrid.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Xceed.Wpf.Toolkit.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\XD Server.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\XD Server.dll.config";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\XDPushServer.local.log4net";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Zen.Barcode.Core.dll";
					
### C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release,  Configuration: Release##

SetOutPath "$INSTDIR\amd64"

			
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\amd64\sqlceca40.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\amd64\sqlcecompact40.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\amd64\sqlceer40EN.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\amd64\sqlceme40.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\amd64\sqlceqp40.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\amd64\sqlcese40.dll";
					
### C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release,  Configuration: Release##

SetOutPath "$INSTDIR\amd64\Microsoft.VC90.CRT"

			
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\amd64\Microsoft.VC90.CRT\Microsoft.VC90.CRT.manifest";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\amd64\Microsoft.VC90.CRT\msvcr90.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\amd64\Microsoft.VC90.CRT\README_ENU.txt";
					
### C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release,  Configuration: Release##

SetOutPath "$INSTDIR\content"

			
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\metro-bootstrap.css";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\metro-bootstrap.min.css";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\style.css";
					
### C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release,  Configuration: Release##

SetOutPath "$INSTDIR\content\bootstrap"

			
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\alerts.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\badges.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\bootstrap.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\breadcrumbs.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\button-groups.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\buttons.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\carousel.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\close.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\code.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\component-animations.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\dropdowns.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\forms.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\glyphicons.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\grid.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\input-groups.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\jumbotron.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\labels.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\list-group.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\media.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\mixins.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\modals.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\navbar.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\navs.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\normalize.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\pager.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\pagination.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\panels.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\popovers.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\print.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\progress-bars.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\responsive-embed.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\responsive-utilities.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\scaffolding.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\tables.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\theme.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\thumbnails.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\tooltip.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\type.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\utilities.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\variables.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\wells.less";
					
### C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release,  Configuration: Release##

SetOutPath "$INSTDIR\content\bootstrap\mixins"

			
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\mixins\alerts.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\mixins\background-variant.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\mixins\border-radius.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\mixins\buttons.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\mixins\center-block.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\mixins\clearfix.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\mixins\forms.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\mixins\gradients.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\mixins\grid-framework.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\mixins\grid.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\mixins\hide-text.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\mixins\image.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\mixins\labels.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\mixins\list-group.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\mixins\nav-divider.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\mixins\nav-vertical-align.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\mixins\opacity.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\mixins\pagination.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\mixins\panels.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\mixins\progress-bar.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\mixins\reset-filter.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\mixins\resize.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\mixins\responsive-visibility.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\mixins\size.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\mixins\tab-focus.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\mixins\table-row.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\mixins\text-emphasis.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\mixins\text-overflow.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\bootstrap\mixins\vendor-prefixes.less";
					
### C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release,  Configuration: Release##

SetOutPath "$INSTDIR\content\css"

			
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\css\carousel.css";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\css\font-awesome.css";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\css\font-awesome.min.css";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\css\style.css";
					
### C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release,  Configuration: Release##

SetOutPath "$INSTDIR\content\fonts"

			
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\fonts\fontawesome-webfont.eot";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\fonts\fontawesome-webfont.svg";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\fonts\fontawesome-webfont.ttf";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\fonts\fontawesome-webfont.woff";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\fonts\fontawesome-webfont.woff2";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\fonts\FontAwesome.otf";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\fonts\glyphicons-halflings-regular.eot";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\fonts\glyphicons-halflings-regular.svg";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\fonts\glyphicons-halflings-regular.ttf";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\fonts\glyphicons-halflings-regular.woff";
					
### C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release,  Configuration: Release##

SetOutPath "$INSTDIR\content\less"

			
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\less\animated.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\less\bordered-pulled.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\less\core.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\less\fixed-width.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\less\font-awesome.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\less\icons.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\less\larger.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\less\list.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\less\mixins.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\less\path.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\less\rotated-flipped.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\less\stacked.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\less\variables.less";
					
### C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release,  Configuration: Release##

SetOutPath "$INSTDIR\content\metro-bootstrap"

			
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\metro-bootstrap\alerts.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\metro-bootstrap\checkbox.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\metro-bootstrap\labels.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\metro-bootstrap\metro-bootstrap.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\metro-bootstrap\modals.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\metro-bootstrap\navbar-side.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\metro-bootstrap\radio.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\metro-bootstrap\tiles.less";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\metro-bootstrap\variables.less";
					
### C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release,  Configuration: Release##

SetOutPath "$INSTDIR\content\scss"

			
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\scss\font-awesome.scss";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\scss\_animated.scss";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\scss\_bordered-pulled.scss";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\scss\_core.scss";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\scss\_fixed-width.scss";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\scss\_icons.scss";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\scss\_larger.scss";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\scss\_list.scss";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\scss\_mixins.scss";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\scss\_path.scss";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\scss\_rotated-flipped.scss";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\scss\_stacked.scss";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\content\scss\_variables.scss";
					
### C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release,  Configuration: Release##

SetOutPath "$INSTDIR\de"

			
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\de\DataModel.resources.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\de\NancyServer.resources.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\de\Scrutinus.resources.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\de\Xceed.Wpf.AvalonDock.resources.dll";
					
### C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release,  Configuration: Release##

SetOutPath "$INSTDIR\en"

			
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\en\DataModel.resources.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\en\NancyServer.resources.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\en\Scrutinus.resources.dll";
					
### C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release,  Configuration: Release##

SetOutPath "$INSTDIR\es"

			
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\es\Xceed.Wpf.AvalonDock.resources.dll";
					
### C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release,  Configuration: Release##

SetOutPath "$INSTDIR\fi"

			
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\fi\DataModel.resources.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\fi\NancyServer.resources.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\fi\Scrutinus.resources.dll";
					
### C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release,  Configuration: Release##

SetOutPath "$INSTDIR\fr"

			
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\fr\Xceed.Wpf.AvalonDock.resources.dll";
					
### C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release,  Configuration: Release##

SetOutPath "$INSTDIR\HelpFiles"

			
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\HelpFiles\Styles.css";
					
### C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release,  Configuration: Release##

SetOutPath "$INSTDIR\HelpFiles\de"

			
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\HelpFiles\de\InputMarking.html";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\HelpFiles\de\Main.html";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\HelpFiles\de\StartList.html";
					
### C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release,  Configuration: Release##

SetOutPath "$INSTDIR\HelpFiles\en"

			
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\HelpFiles\en\Main.html";
					
### C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release,  Configuration: Release##

SetOutPath "$INSTDIR\hu"

			
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\hu\Xceed.Wpf.AvalonDock.resources.dll";
					
### C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release,  Configuration: Release##

SetOutPath "$INSTDIR\it"

			
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\it\Xceed.Wpf.AvalonDock.resources.dll";
					
### C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release,  Configuration: Release##

SetOutPath "$INSTDIR\pt-BR"

			
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\pt-BR\Xceed.Wpf.AvalonDock.resources.dll";
					
### C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release,  Configuration: Release##

SetOutPath "$INSTDIR\pt-PT"

			
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\pt-PT\Scrutinus.resources.dll";
					
### C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release,  Configuration: Release##

SetOutPath "$INSTDIR\ro"

			
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\ro\Xceed.Wpf.AvalonDock.resources.dll";
					
### C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release,  Configuration: Release##

SetOutPath "$INSTDIR\ru"

			
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\ru\Xceed.Wpf.AvalonDock.resources.dll";
					
### C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release,  Configuration: Release##

SetOutPath "$INSTDIR\Scripts"

			
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Scripts\bootstrap.js";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Scripts\bootstrap.min.js";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Scripts\jquery-1.9.0.intellisense.js";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Scripts\jquery-1.9.0.js";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Scripts\jquery-1.9.0.min.js";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Scripts\jquery.min.map";
					
### C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release,  Configuration: Release##

SetOutPath "$INSTDIR\sv"

			
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\sv\Xceed.Wpf.AvalonDock.resources.dll";
					
### C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release,  Configuration: Release##

SetOutPath "$INSTDIR\Views"

			
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Views\web.config";
					
### C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release,  Configuration: Release##

SetOutPath "$INSTDIR\Views\Carousel"

			
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Views\Carousel\Carousel.cshtml";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Views\Carousel\DroppedOutCarousel.cshtml";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Views\Carousel\QualifiedCarousel.cshtml";
					
### C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release,  Configuration: Release##

SetOutPath "$INSTDIR\Views\Competition"

			
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Views\Competition\Competition.cshtml";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Views\Competition\Index.cshtml";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Views\Competition\Overview.cshtml";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Views\Competition\Participants.cshtml";
					
### C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release,  Configuration: Release##

SetOutPath "$INSTDIR\Views\Export"

			
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Views\Export\FinalTable.cshtml";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Views\Export\Index.cshtml";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Views\Export\JS2Final.cshtml";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Views\Export\MarkingTable.cshtml";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Views\Export\Officials.cshtml";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Views\Export\Overview.cshtml";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Views\Export\RegisteredCouples.cshtml";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Views\Export\Result.cshtml";
					
### C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release,  Configuration: Release##

SetOutPath "$INSTDIR\Views\Home"

			
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Views\Home\AllCompetitions.cshtml";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Views\Home\Index.cshtml";
					
### C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release,  Configuration: Release##

SetOutPath "$INSTDIR\Views\Login"

			
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Views\Login\login.cshtml";
					
### C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release,  Configuration: Release##

SetOutPath "$INSTDIR\Views\Marking"

			
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Views\Marking\Final.cshtml";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Views\Marking\Marks.cshtml";
					
### C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release,  Configuration: Release##

SetOutPath "$INSTDIR\Views\Projector"

			
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Views\Projector\Event.cshtml";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Views\Projector\Heats.cshtml";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Views\Projector\PricePresentation.cshtml";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Views\Projector\Qualified.cshtml";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Views\Projector\Round.cshtml";
					
### C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release,  Configuration: Release##

SetOutPath "$INSTDIR\Views\Round"

			
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Views\Round\Drawing.cshtml";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Views\Round\DrawingWithNames.cshtml";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Views\Round\FinalResult.cshtml";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Views\Round\Index.cshtml";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Views\Round\Officials.cshtml";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Views\Round\Qualified.cshtml";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Views\Round\Result.cshtml";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Views\Round\ResultByRegion.cshtml";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Views\Round\Rounds.cshtml";
					
### C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release,  Configuration: Release##

SetOutPath "$INSTDIR\Views\ScoreBoard"

			
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Views\ScoreBoard\Heat.cshtml";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Views\ScoreBoard\Index.cshtml";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Views\ScoreBoard\PricePresentation.cshtml";
					
### C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release,  Configuration: Release##

SetOutPath "$INSTDIR\Views\Shared"

			
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Views\Shared\Error.cshtml";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Views\Shared\LeftNavigation.cshtml";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Views\Shared\_layout.cshtml";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Views\Shared\_layoutNoHeader.cshtml";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Views\Shared\_layoutProjector.cshtml";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Views\Shared\_layoutTotalMarking.cshtml";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\Views\Shared\_scoreboard.cshtml";
					
### C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release,  Configuration: Release##

SetOutPath "$INSTDIR\x86"

			
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\x86\sqlceca40.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\x86\sqlcecompact40.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\x86\sqlceer40EN.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\x86\sqlceme40.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\x86\sqlceqp40.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\x86\sqlcese40.dll";
					
### C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release,  Configuration: Release##

SetOutPath "$INSTDIR\x86\Microsoft.VC90.CRT"

			
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\x86\Microsoft.VC90.CRT\Microsoft.VC90.CRT.manifest";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\x86\Microsoft.VC90.CRT\msvcr90.dll";
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\x86\Microsoft.VC90.CRT\README_ENU.txt";
					
### C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release,  Configuration: Release##

SetOutPath "$INSTDIR\XMLRules"

			
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\XMLRules\SpanishData.xml";
					
### C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release,  Configuration: Release##

SetOutPath "$INSTDIR\zh-Hans"

			
			File "C:\Projekte\Scrutinus\Scrutinus\Scrutinus\bin\Release\zh-Hans\Xceed.Wpf.AvalonDock.resources.dll";
					SectionEnd
          
Section -Icons_Reg
SetOutPath "$INSTDIR"
WriteUninstaller "$INSTDIR\uninstall.exe"

!ifdef REG_START_MENU
!insertmacro MUI_STARTMENU_WRITE_BEGIN Application
CreateDirectory "$SMPROGRAMS\$SM_Folder"
CreateShortCut "$SMPROGRAMS\$SM_Folder\${APP_NAME}.lnk" "$INSTDIR\${MAIN_APP_EXE}"
CreateShortCut "$DESKTOP\${APP_NAME}.lnk" "$INSTDIR\${MAIN_APP_EXE}"
CreateShortCut "$SMPROGRAMS\$SM_Folder\Uninstall ${APP_NAME}.lnk" "$INSTDIR\uninstall.exe"

!ifdef WEB_SITE
WriteIniStr "$INSTDIR\${APP_NAME} website.url" "InternetShortcut" "URL" "${WEB_SITE}"
CreateShortCut "$SMPROGRAMS\$SM_Folder\${APP_NAME} Website.lnk" "$INSTDIR\${APP_NAME} website.url"
!endif
!insertmacro MUI_STARTMENU_WRITE_END
!endif

!ifndef REG_START_MENU
CreateDirectory "$SMPROGRAMS\TPS8"
CreateShortCut "$SMPROGRAMS\TPS8\${APP_NAME}.lnk" "$INSTDIR\${MAIN_APP_EXE}"
CreateShortCut "$DESKTOP\${APP_NAME}.lnk" "$INSTDIR\${MAIN_APP_EXE}"
CreateShortCut "$SMPROGRAMS\TPS8\Uninstall ${APP_NAME}.lnk" "$INSTDIR\uninstall.exe"

!ifdef WEB_SITE
WriteIniStr "$INSTDIR\${APP_NAME} website.url" "InternetShortcut" "URL" "${WEB_SITE}"
CreateShortCut "$SMPROGRAMS\TPS8\${APP_NAME} Website.lnk" "$INSTDIR\${APP_NAME} website.url"
!endif
!endif

WriteRegStr ${REG_ROOT} "${REG_APP_PATH}" "" "$INSTDIR\${MAIN_APP_EXE}"
WriteRegStr ${REG_ROOT} "${UNINSTALL_PATH}"  "DisplayName" "${APP_NAME}"
WriteRegStr ${REG_ROOT} "${UNINSTALL_PATH}"  "UninstallString" "$INSTDIR\uninstall.exe"
WriteRegStr ${REG_ROOT} "${UNINSTALL_PATH}"  "DisplayIcon" "$INSTDIR\${MAIN_APP_EXE}"
WriteRegStr ${REG_ROOT} "${UNINSTALL_PATH}"  "DisplayVersion" "${VERSION}"
WriteRegStr ${REG_ROOT} "${UNINSTALL_PATH}"  "Publisher" "${COMP_NAME}"

!ifdef WEB_SITE
WriteRegStr ${REG_ROOT} "${UNINSTALL_PATH}"  "URLInfoAbout" "${WEB_SITE}"
!endif
SectionEnd

######################################################################

Section Uninstall
${INSTALL_TYPE}

Delete "$INSTDIR\AsyncIO.dll"
Delete "$INSTDIR\Common.dll"
Delete "$INSTDIR\Common.dll.config"
Delete "$INSTDIR\ControlzEx.dll"
Delete "$INSTDIR\CsQuery.dll"
Delete "$INSTDIR\DataModel.dll"
Delete "$INSTDIR\DataModel.dll.config"
Delete "$INSTDIR\DtvEsvModule.dll"
Delete "$INSTDIR\DtvEsvModule.dll.config"
Delete "$INSTDIR\EntityFramework.dll"
Delete "$INSTDIR\EntityFramework.SqlServer.dll"
Delete "$INSTDIR\EntityFramework.SqlServerCompact.dll"
Delete "$INSTDIR\EPPlus.dll"
Delete "$INSTDIR\Exceptionless.dll"
Delete "$INSTDIR\Exceptionless.Portable.dll"
Delete "$INSTDIR\Exceptionless.Wpf.dll"
Delete "$INSTDIR\FtsRestAPI.dll"
Delete "$INSTDIR\FtsRestAPI.dll.config"
Delete "$INSTDIR\GalaSoft.MvvmLight.dll"
Delete "$INSTDIR\GalaSoft.MvvmLight.Extras.dll"
Delete "$INSTDIR\General.dll"
Delete "$INSTDIR\GongSolutions.Wpf.DragDrop.dll"
Delete "$INSTDIR\Helpers.dll"
Delete "$INSTDIR\Helpers.dll.config"
Delete "$INSTDIR\log4net.dll"
Delete "$INSTDIR\MahApps.Metro.dll"
Delete "$INSTDIR\MahApps.Metro.SimpleChildWindow.dll"
Delete "$INSTDIR\Microsoft.Practices.ServiceLocation.dll"
Delete "$INSTDIR\MobileControlLib.dll"
Delete "$INSTDIR\MobileControlLib.dll.config"
Delete "$INSTDIR\MoveResizeRotateWithAdorners.dll"
Delete "$INSTDIR\MoveResizeRotateWithAdorners.dll.config"
Delete "$INSTDIR\Nancy.Authentication.Forms.dll"
Delete "$INSTDIR\Nancy.dll"
Delete "$INSTDIR\Nancy.Hosting.Self.dll"
Delete "$INSTDIR\Nancy.Testing.dll"
Delete "$INSTDIR\Nancy.ViewEngines.Razor.dll"
Delete "$INSTDIR\NancyServer.exe"
Delete "$INSTDIR\NancyServer.exe.config"
Delete "$INSTDIR\NetMQ.dll"
Delete "$INSTDIR\Newtonsoft.Json.dll"
Delete "$INSTDIR\NLog.config"
Delete "$INSTDIR\NLog.dll"
Delete "$INSTDIR\Scrutinus.exe"
Delete "$INSTDIR\Scrutinus.exe.config"
Delete "$INSTDIR\Scrutinus.vshost.exe"
Delete "$INSTDIR\Scrutinus.vshost.exe.config"
Delete "$INSTDIR\System.Data.DataSetExtensions.dll"
Delete "$INSTDIR\System.Data.SqlServerCe.dll"
Delete "$INSTDIR\System.Net.Http.Extensions.dll"
Delete "$INSTDIR\System.Net.Http.Primitives.dll"
Delete "$INSTDIR\System.Reactive.Core.dll"
Delete "$INSTDIR\System.Reactive.Interfaces.dll"
Delete "$INSTDIR\System.Reactive.Linq.dll"
Delete "$INSTDIR\System.Reactive.PlatformServices.dll"
Delete "$INSTDIR\System.Reactive.Windows.Threading.dll"
Delete "$INSTDIR\System.Web.Razor.dll"
Delete "$INSTDIR\System.Windows.Interactivity.dll"
Delete "$INSTDIR\TPSModel.dll"
Delete "$INSTDIR\Wdsf.Api.Client.dll"
Delete "$INSTDIR\WPFLocalizeExtension.dll"
Delete "$INSTDIR\XAMLMarkupExtensions.dll"
Delete "$INSTDIR\Xceed.Wpf.AvalonDock.dll"
Delete "$INSTDIR\Xceed.Wpf.AvalonDock.Themes.Aero.dll"
Delete "$INSTDIR\Xceed.Wpf.AvalonDock.Themes.Metro.dll"
Delete "$INSTDIR\Xceed.Wpf.AvalonDock.Themes.VS2010.dll"
Delete "$INSTDIR\Xceed.Wpf.DataGrid.dll"
Delete "$INSTDIR\Xceed.Wpf.Toolkit.dll"
Delete "$INSTDIR\XD Server.dll"
Delete "$INSTDIR\XD Server.dll.config"
Delete "$INSTDIR\XDPushServer.local.log4net"
Delete "$INSTDIR\Zen.Barcode.Core.dll"
RmDir "$INSTDIR"
Delete "$INSTDIR\amd64\sqlceca40.dll"
Delete "$INSTDIR\amd64\sqlcecompact40.dll"
Delete "$INSTDIR\amd64\sqlceer40EN.dll"
Delete "$INSTDIR\amd64\sqlceme40.dll"
Delete "$INSTDIR\amd64\sqlceqp40.dll"
Delete "$INSTDIR\amd64\sqlcese40.dll"
RmDir "$INSTDIR\amd64"
Delete "$INSTDIR\amd64\Microsoft.VC90.CRT\Microsoft.VC90.CRT.manifest"
Delete "$INSTDIR\amd64\Microsoft.VC90.CRT\msvcr90.dll"
Delete "$INSTDIR\amd64\Microsoft.VC90.CRT\README_ENU.txt"
RmDir "$INSTDIR\amd64\Microsoft.VC90.CRT"
Delete "$INSTDIR\content\metro-bootstrap.css"
Delete "$INSTDIR\content\metro-bootstrap.min.css"
Delete "$INSTDIR\content\style.css"
RmDir "$INSTDIR\content"
Delete "$INSTDIR\content\bootstrap\alerts.less"
Delete "$INSTDIR\content\bootstrap\badges.less"
Delete "$INSTDIR\content\bootstrap\bootstrap.less"
Delete "$INSTDIR\content\bootstrap\breadcrumbs.less"
Delete "$INSTDIR\content\bootstrap\button-groups.less"
Delete "$INSTDIR\content\bootstrap\buttons.less"
Delete "$INSTDIR\content\bootstrap\carousel.less"
Delete "$INSTDIR\content\bootstrap\close.less"
Delete "$INSTDIR\content\bootstrap\code.less"
Delete "$INSTDIR\content\bootstrap\component-animations.less"
Delete "$INSTDIR\content\bootstrap\dropdowns.less"
Delete "$INSTDIR\content\bootstrap\forms.less"
Delete "$INSTDIR\content\bootstrap\glyphicons.less"
Delete "$INSTDIR\content\bootstrap\grid.less"
Delete "$INSTDIR\content\bootstrap\input-groups.less"
Delete "$INSTDIR\content\bootstrap\jumbotron.less"
Delete "$INSTDIR\content\bootstrap\labels.less"
Delete "$INSTDIR\content\bootstrap\list-group.less"
Delete "$INSTDIR\content\bootstrap\media.less"
Delete "$INSTDIR\content\bootstrap\mixins.less"
Delete "$INSTDIR\content\bootstrap\modals.less"
Delete "$INSTDIR\content\bootstrap\navbar.less"
Delete "$INSTDIR\content\bootstrap\navs.less"
Delete "$INSTDIR\content\bootstrap\normalize.less"
Delete "$INSTDIR\content\bootstrap\pager.less"
Delete "$INSTDIR\content\bootstrap\pagination.less"
Delete "$INSTDIR\content\bootstrap\panels.less"
Delete "$INSTDIR\content\bootstrap\popovers.less"
Delete "$INSTDIR\content\bootstrap\print.less"
Delete "$INSTDIR\content\bootstrap\progress-bars.less"
Delete "$INSTDIR\content\bootstrap\responsive-embed.less"
Delete "$INSTDIR\content\bootstrap\responsive-utilities.less"
Delete "$INSTDIR\content\bootstrap\scaffolding.less"
Delete "$INSTDIR\content\bootstrap\tables.less"
Delete "$INSTDIR\content\bootstrap\theme.less"
Delete "$INSTDIR\content\bootstrap\thumbnails.less"
Delete "$INSTDIR\content\bootstrap\tooltip.less"
Delete "$INSTDIR\content\bootstrap\type.less"
Delete "$INSTDIR\content\bootstrap\utilities.less"
Delete "$INSTDIR\content\bootstrap\variables.less"
Delete "$INSTDIR\content\bootstrap\wells.less"
RmDir "$INSTDIR\content\bootstrap"
Delete "$INSTDIR\content\bootstrap\mixins\alerts.less"
Delete "$INSTDIR\content\bootstrap\mixins\background-variant.less"
Delete "$INSTDIR\content\bootstrap\mixins\border-radius.less"
Delete "$INSTDIR\content\bootstrap\mixins\buttons.less"
Delete "$INSTDIR\content\bootstrap\mixins\center-block.less"
Delete "$INSTDIR\content\bootstrap\mixins\clearfix.less"
Delete "$INSTDIR\content\bootstrap\mixins\forms.less"
Delete "$INSTDIR\content\bootstrap\mixins\gradients.less"
Delete "$INSTDIR\content\bootstrap\mixins\grid-framework.less"
Delete "$INSTDIR\content\bootstrap\mixins\grid.less"
Delete "$INSTDIR\content\bootstrap\mixins\hide-text.less"
Delete "$INSTDIR\content\bootstrap\mixins\image.less"
Delete "$INSTDIR\content\bootstrap\mixins\labels.less"
Delete "$INSTDIR\content\bootstrap\mixins\list-group.less"
Delete "$INSTDIR\content\bootstrap\mixins\nav-divider.less"
Delete "$INSTDIR\content\bootstrap\mixins\nav-vertical-align.less"
Delete "$INSTDIR\content\bootstrap\mixins\opacity.less"
Delete "$INSTDIR\content\bootstrap\mixins\pagination.less"
Delete "$INSTDIR\content\bootstrap\mixins\panels.less"
Delete "$INSTDIR\content\bootstrap\mixins\progress-bar.less"
Delete "$INSTDIR\content\bootstrap\mixins\reset-filter.less"
Delete "$INSTDIR\content\bootstrap\mixins\resize.less"
Delete "$INSTDIR\content\bootstrap\mixins\responsive-visibility.less"
Delete "$INSTDIR\content\bootstrap\mixins\size.less"
Delete "$INSTDIR\content\bootstrap\mixins\tab-focus.less"
Delete "$INSTDIR\content\bootstrap\mixins\table-row.less"
Delete "$INSTDIR\content\bootstrap\mixins\text-emphasis.less"
Delete "$INSTDIR\content\bootstrap\mixins\text-overflow.less"
Delete "$INSTDIR\content\bootstrap\mixins\vendor-prefixes.less"
RmDir "$INSTDIR\content\bootstrap\mixins"
Delete "$INSTDIR\content\css\carousel.css"
Delete "$INSTDIR\content\css\font-awesome.css"
Delete "$INSTDIR\content\css\font-awesome.min.css"
Delete "$INSTDIR\content\css\style.css"
RmDir "$INSTDIR\content\css"
Delete "$INSTDIR\content\fonts\fontawesome-webfont.eot"
Delete "$INSTDIR\content\fonts\fontawesome-webfont.svg"
Delete "$INSTDIR\content\fonts\fontawesome-webfont.ttf"
Delete "$INSTDIR\content\fonts\fontawesome-webfont.woff"
Delete "$INSTDIR\content\fonts\fontawesome-webfont.woff2"
Delete "$INSTDIR\content\fonts\FontAwesome.otf"
Delete "$INSTDIR\content\fonts\glyphicons-halflings-regular.eot"
Delete "$INSTDIR\content\fonts\glyphicons-halflings-regular.svg"
Delete "$INSTDIR\content\fonts\glyphicons-halflings-regular.ttf"
Delete "$INSTDIR\content\fonts\glyphicons-halflings-regular.woff"
RmDir "$INSTDIR\content\fonts"
Delete "$INSTDIR\content\less\animated.less"
Delete "$INSTDIR\content\less\bordered-pulled.less"
Delete "$INSTDIR\content\less\core.less"
Delete "$INSTDIR\content\less\fixed-width.less"
Delete "$INSTDIR\content\less\font-awesome.less"
Delete "$INSTDIR\content\less\icons.less"
Delete "$INSTDIR\content\less\larger.less"
Delete "$INSTDIR\content\less\list.less"
Delete "$INSTDIR\content\less\mixins.less"
Delete "$INSTDIR\content\less\path.less"
Delete "$INSTDIR\content\less\rotated-flipped.less"
Delete "$INSTDIR\content\less\stacked.less"
Delete "$INSTDIR\content\less\variables.less"
RmDir "$INSTDIR\content\less"
Delete "$INSTDIR\content\metro-bootstrap\alerts.less"
Delete "$INSTDIR\content\metro-bootstrap\checkbox.less"
Delete "$INSTDIR\content\metro-bootstrap\labels.less"
Delete "$INSTDIR\content\metro-bootstrap\metro-bootstrap.less"
Delete "$INSTDIR\content\metro-bootstrap\modals.less"
Delete "$INSTDIR\content\metro-bootstrap\navbar-side.less"
Delete "$INSTDIR\content\metro-bootstrap\radio.less"
Delete "$INSTDIR\content\metro-bootstrap\tiles.less"
Delete "$INSTDIR\content\metro-bootstrap\variables.less"
RmDir "$INSTDIR\content\metro-bootstrap"
Delete "$INSTDIR\content\scss\font-awesome.scss"
Delete "$INSTDIR\content\scss\_animated.scss"
Delete "$INSTDIR\content\scss\_bordered-pulled.scss"
Delete "$INSTDIR\content\scss\_core.scss"
Delete "$INSTDIR\content\scss\_fixed-width.scss"
Delete "$INSTDIR\content\scss\_icons.scss"
Delete "$INSTDIR\content\scss\_larger.scss"
Delete "$INSTDIR\content\scss\_list.scss"
Delete "$INSTDIR\content\scss\_mixins.scss"
Delete "$INSTDIR\content\scss\_path.scss"
Delete "$INSTDIR\content\scss\_rotated-flipped.scss"
Delete "$INSTDIR\content\scss\_stacked.scss"
Delete "$INSTDIR\content\scss\_variables.scss"
RmDir "$INSTDIR\content\scss"
Delete "$INSTDIR\de\DataModel.resources.dll"
Delete "$INSTDIR\de\NancyServer.resources.dll"
Delete "$INSTDIR\de\Scrutinus.resources.dll"
Delete "$INSTDIR\de\Xceed.Wpf.AvalonDock.resources.dll"
RmDir "$INSTDIR\de"
Delete "$INSTDIR\en\DataModel.resources.dll"
Delete "$INSTDIR\en\NancyServer.resources.dll"
Delete "$INSTDIR\en\Scrutinus.resources.dll"
RmDir "$INSTDIR\en"
Delete "$INSTDIR\es\Xceed.Wpf.AvalonDock.resources.dll"
RmDir "$INSTDIR\es"
Delete "$INSTDIR\fi\DataModel.resources.dll"
Delete "$INSTDIR\fi\NancyServer.resources.dll"
Delete "$INSTDIR\fi\Scrutinus.resources.dll"
RmDir "$INSTDIR\fi"
Delete "$INSTDIR\fr\Xceed.Wpf.AvalonDock.resources.dll"
RmDir "$INSTDIR\fr"
Delete "$INSTDIR\HelpFiles\Styles.css"
RmDir "$INSTDIR\HelpFiles"
Delete "$INSTDIR\HelpFiles\de\InputMarking.html"
Delete "$INSTDIR\HelpFiles\de\Main.html"
Delete "$INSTDIR\HelpFiles\de\StartList.html"
RmDir "$INSTDIR\HelpFiles\de"
Delete "$INSTDIR\HelpFiles\en\Main.html"
RmDir "$INSTDIR\HelpFiles\en"
Delete "$INSTDIR\hu\Xceed.Wpf.AvalonDock.resources.dll"
RmDir "$INSTDIR\hu"
Delete "$INSTDIR\it\Xceed.Wpf.AvalonDock.resources.dll"
RmDir "$INSTDIR\it"
Delete "$INSTDIR\pt-BR\Xceed.Wpf.AvalonDock.resources.dll"
RmDir "$INSTDIR\pt-BR"
Delete "$INSTDIR\pt-PT\Scrutinus.resources.dll"
RmDir "$INSTDIR\pt-PT"
Delete "$INSTDIR\ro\Xceed.Wpf.AvalonDock.resources.dll"
RmDir "$INSTDIR\ro"
Delete "$INSTDIR\ru\Xceed.Wpf.AvalonDock.resources.dll"
RmDir "$INSTDIR\ru"
Delete "$INSTDIR\Scripts\bootstrap.js"
Delete "$INSTDIR\Scripts\bootstrap.min.js"
Delete "$INSTDIR\Scripts\jquery-1.9.0.intellisense.js"
Delete "$INSTDIR\Scripts\jquery-1.9.0.js"
Delete "$INSTDIR\Scripts\jquery-1.9.0.min.js"
Delete "$INSTDIR\Scripts\jquery.min.map"
RmDir "$INSTDIR\Scripts"
Delete "$INSTDIR\sv\Xceed.Wpf.AvalonDock.resources.dll"
RmDir "$INSTDIR\sv"
Delete "$INSTDIR\Views\web.config"
RmDir "$INSTDIR\Views"
Delete "$INSTDIR\Views\Carousel\Carousel.cshtml"
Delete "$INSTDIR\Views\Carousel\DroppedOutCarousel.cshtml"
Delete "$INSTDIR\Views\Carousel\QualifiedCarousel.cshtml"
RmDir "$INSTDIR\Views\Carousel"
Delete "$INSTDIR\Views\Competition\Competition.cshtml"
Delete "$INSTDIR\Views\Competition\Index.cshtml"
Delete "$INSTDIR\Views\Competition\Overview.cshtml"
Delete "$INSTDIR\Views\Competition\Participants.cshtml"
RmDir "$INSTDIR\Views\Competition"
Delete "$INSTDIR\Views\Export\FinalTable.cshtml"
Delete "$INSTDIR\Views\Export\Index.cshtml"
Delete "$INSTDIR\Views\Export\JS2Final.cshtml"
Delete "$INSTDIR\Views\Export\MarkingTable.cshtml"
Delete "$INSTDIR\Views\Export\Officials.cshtml"
Delete "$INSTDIR\Views\Export\Overview.cshtml"
Delete "$INSTDIR\Views\Export\RegisteredCouples.cshtml"
Delete "$INSTDIR\Views\Export\Result.cshtml"
RmDir "$INSTDIR\Views\Export"
Delete "$INSTDIR\Views\Home\AllCompetitions.cshtml"
Delete "$INSTDIR\Views\Home\Index.cshtml"
RmDir "$INSTDIR\Views\Home"
Delete "$INSTDIR\Views\Login\login.cshtml"
RmDir "$INSTDIR\Views\Login"
Delete "$INSTDIR\Views\Marking\Final.cshtml"
Delete "$INSTDIR\Views\Marking\Marks.cshtml"
RmDir "$INSTDIR\Views\Marking"
Delete "$INSTDIR\Views\Projector\Event.cshtml"
Delete "$INSTDIR\Views\Projector\Heats.cshtml"
Delete "$INSTDIR\Views\Projector\PricePresentation.cshtml"
Delete "$INSTDIR\Views\Projector\Qualified.cshtml"
Delete "$INSTDIR\Views\Projector\Round.cshtml"
RmDir "$INSTDIR\Views\Projector"
Delete "$INSTDIR\Views\Round\Drawing.cshtml"
Delete "$INSTDIR\Views\Round\DrawingWithNames.cshtml"
Delete "$INSTDIR\Views\Round\FinalResult.cshtml"
Delete "$INSTDIR\Views\Round\Index.cshtml"
Delete "$INSTDIR\Views\Round\Officials.cshtml"
Delete "$INSTDIR\Views\Round\Qualified.cshtml"
Delete "$INSTDIR\Views\Round\Result.cshtml"
Delete "$INSTDIR\Views\Round\ResultByRegion.cshtml"
Delete "$INSTDIR\Views\Round\Rounds.cshtml"
RmDir "$INSTDIR\Views\Round"
Delete "$INSTDIR\Views\ScoreBoard\Heat.cshtml"
Delete "$INSTDIR\Views\ScoreBoard\Index.cshtml"
Delete "$INSTDIR\Views\ScoreBoard\PricePresentation.cshtml"
RmDir "$INSTDIR\Views\ScoreBoard"
Delete "$INSTDIR\Views\Shared\Error.cshtml"
Delete "$INSTDIR\Views\Shared\LeftNavigation.cshtml"
Delete "$INSTDIR\Views\Shared\_layout.cshtml"
Delete "$INSTDIR\Views\Shared\_layoutNoHeader.cshtml"
Delete "$INSTDIR\Views\Shared\_layoutProjector.cshtml"
Delete "$INSTDIR\Views\Shared\_layoutTotalMarking.cshtml"
Delete "$INSTDIR\Views\Shared\_scoreboard.cshtml"
RmDir "$INSTDIR\Views\Shared"
Delete "$INSTDIR\x86\sqlceca40.dll"
Delete "$INSTDIR\x86\sqlcecompact40.dll"
Delete "$INSTDIR\x86\sqlceer40EN.dll"
Delete "$INSTDIR\x86\sqlceme40.dll"
Delete "$INSTDIR\x86\sqlceqp40.dll"
Delete "$INSTDIR\x86\sqlcese40.dll"
RmDir "$INSTDIR\x86"
Delete "$INSTDIR\x86\Microsoft.VC90.CRT\Microsoft.VC90.CRT.manifest"
Delete "$INSTDIR\x86\Microsoft.VC90.CRT\msvcr90.dll"
Delete "$INSTDIR\x86\Microsoft.VC90.CRT\README_ENU.txt"
RmDir "$INSTDIR\x86\Microsoft.VC90.CRT"
Delete "$INSTDIR\XMLRules\SpanishData.xml"
RmDir "$INSTDIR\XMLRules"
Delete "$INSTDIR\zh-Hans\Xceed.Wpf.AvalonDock.resources.dll"
RmDir "$INSTDIR\zh-Hans"

Delete "$INSTDIR\uninstall.exe"
!ifdef WEB_SITE
Delete "$INSTDIR\${APP_NAME} website.url"
!endif

RmDir "$INSTDIR"

!ifdef REG_START_MENU
!insertmacro MUI_STARTMENU_GETFOLDER "Application" $SM_Folder
Delete "$SMPROGRAMS\$SM_Folder\${APP_NAME}.lnk"
Delete "$SMPROGRAMS\$SM_Folder\Uninstall ${APP_NAME}.lnk"
!ifdef WEB_SITE
Delete "$SMPROGRAMS\$SM_Folder\${APP_NAME} Website.lnk"
!endif
Delete "$DESKTOP\${APP_NAME}.lnk"

RmDir "$SMPROGRAMS\$SM_Folder"
!endif

!ifndef REG_START_MENU
Delete "$SMPROGRAMS\TPS8\${APP_NAME}.lnk"
Delete "$SMPROGRAMS\TPS8\Uninstall ${APP_NAME}.lnk"
!ifdef WEB_SITE
Delete "$SMPROGRAMS\TPS8\${APP_NAME} Website.lnk"
!endif
Delete "$DESKTOP\${APP_NAME}.lnk"

RmDir "$SMPROGRAMS\TPS8"
!endif

DeleteRegKey ${REG_ROOT} "${REG_APP_PATH}"
DeleteRegKey ${REG_ROOT} "${UNINSTALL_PATH}"
SectionEnd

######################################################################

