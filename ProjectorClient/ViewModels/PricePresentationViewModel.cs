﻿// // TPS.net TPS8 ProjectorClient
// // PricePresentationViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows.Media;
using System.Xml.Linq;
using ProjectorClient.Properties;

namespace ProjectorClient.ViewModels
{
    internal class PricePresentationViewModel : ProjectorClientViewModelBase
    {
        public ObservableCollection<Qualified> Qualifieds { get; set; } = new ObservableCollection<Qualified>();

        public string Title { get; set; }

        public double HeaderFontSize { get; set; }

        public double HeaderTopMargin { get; set; }

        public double ContentTopMargin { get; set; }

        public double ContentFontSize { get; set; }

        public double HeaderHeight { get; set; }

        public double FooterHeight { get; set; }

        public Brush HeaderBrush { get; set; }

        public Brush ContentBrush { get; set; }


        public void UpdatePlacings()
        {
            this.LoadData();
        }

        protected override void LoadData()
        {
            var file = Settings.Default.DataPath + @"\PricePresentation.txt";
            if (!File.Exists(file))
            {
                return;
            }

            var stream = new StreamReader(file);

            this.Title = stream.ReadLine();

            while (!stream.EndOfStream)
            {
                var data = stream.ReadLine().Split(',');
                var couple = new Qualified(data);
                // ReSharper disable once SimplifyLinqExpression
                if (!this.Qualifieds.Any(q => q.Number == couple.Number) && this.Qualifieds.Any())
                {
                    this.Qualifieds.Insert(0, couple);
                }
                else if(!this.Qualifieds.Any())
                {
                    this.Qualifieds.Add(couple);
                }
            }

            stream.Close();
        }

        protected override void LoadLayout(string path)
        {
            var doc = XDocument.Load(new StreamReader(path));
            var drawLayout = doc.Root.Elements().FirstOrDefault(x => x.Name == "PricePresentation");

            this.Height = double.Parse(drawLayout.Attributes("Height").First().Value);
            this.Width = double.Parse(drawLayout.Attributes("Width").First().Value);
            this.BackgroundImageFile = drawLayout.Attributes("BackgroundImage").First().Value;

            var headerLayout = drawLayout.Element("Header");
            var contentLayout = drawLayout.Element("Content");
            var footerLayout = drawLayout.Element("Footer");

            this.HeaderFontSize = double.Parse(headerLayout.Attributes("FontSize").First().Value);
            this.HeaderTopMargin = double.Parse(headerLayout.Attributes("TopMargin").First().Value);
            this.HeaderHeight = double.Parse(headerLayout.Attributes("Height").First().Value);
            this.HeaderBrush = headerLayout.Attribute("FontColor") != null ? new SolidColorBrush((Color) ColorConverter.ConvertFromString(headerLayout.Attribute("FontColor").Value)) : Brushes.Black;

            this.ContentTopMargin = double.Parse(contentLayout.Attributes("TopMargin").First().Value);
            this.ContentFontSize = double.Parse(contentLayout.Attributes("FontSize").First().Value);
            this.FooterHeight = double.Parse(footerLayout.Attributes("FooterHeight").First().Value);
            this.ContentBrush = contentLayout.Attribute("FontColor") != null ? new SolidColorBrush((Color)ColorConverter.ConvertFromString(contentLayout.Attribute("FontColor").Value)) : Brushes.Black;

            if (drawLayout.Attributes("FontColor").Any())
            {
                var color = (Color)ColorConverter.ConvertFromString(drawLayout.Attributes("FontColor").First().Value);
                this.TextColor = new SolidColorBrush(color);
            }
        }
    }
}
