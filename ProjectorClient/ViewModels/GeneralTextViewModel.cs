﻿// // TPS.net TPS8 ProjectorClient
// // GeneralTextViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.IO;
using System.Linq;
using System.Windows.Media;
using System.Xml.Linq;
using ProjectorClient.Properties;

namespace ProjectorClient.ViewModels
{
    public class GeneralTextViewModel : ProjectorClientViewModelBase
    {
        public string Header { get; set; }

        public string Body { get; set; }

        public double HeaderTopMargin { get; set; }

        public double ContentTopMargin { get; set; }

        public double HeaderHeight { get; set; }

        public double HeaderFontSize { get; set; }

        public double ContentFontSize { get; set; }

        protected override void LoadData()
        {
            var file = Settings.Default.DataPath + @"\GeneralText.txt";
            if (!File.Exists(file))
            {
                return;
            }

            var stream = new StreamReader(file);

            this.Header = stream.ReadLine();
            this.Body = stream.ReadToEnd();

            stream.Close();
        }

        protected override void LoadLayout(string path)
        {
            var doc = XDocument.Load(new StreamReader(path));
            var drawLayout = doc.Root.Elements().FirstOrDefault(x => x.Name == "GeneralText");

            this.Height = double.Parse(drawLayout.Attributes("Height").First().Value);
            this.Width = double.Parse(drawLayout.Attributes("Width").First().Value);
            this.BackgroundImageFile = drawLayout.Attributes("BackgroundImage").First().Value;

            var headerLayout = drawLayout.Element("Header");
            var contentLayout = drawLayout.Element("Content");

            this.HeaderFontSize = double.Parse(headerLayout.Attributes("FontSize").First().Value);
            this.HeaderTopMargin = double.Parse(headerLayout.Attributes("TopMargin").First().Value);
            this.HeaderHeight = double.Parse(headerLayout.Attributes("Height").First().Value);

            this.ContentTopMargin = double.Parse(contentLayout.Attributes("TopMargin").First().Value);
            this.ContentFontSize = double.Parse(contentLayout.Attributes("FontSize").First().Value);

            if (drawLayout.Attributes("FontColor").Any())
            {
                var color = (Color)ColorConverter.ConvertFromString(drawLayout.Attributes("FontColor").First().Value);
                this.TextColor = new SolidColorBrush(color);
            }
        }
    }

}
