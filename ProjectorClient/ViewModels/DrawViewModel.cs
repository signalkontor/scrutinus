﻿// // TPS.net TPS8 ProjectorClient
// // DrawViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Media;
using System.Xml.Linq;
using ProjectorClient.Properties;

namespace ProjectorClient.ViewModels
{
    public class DrawViewModel : ProjectorClientViewModelBase
    {
        public string Title { get; set; }

        public IEnumerable<string> Couples { get; set; }

        public double TopOffset { get; set; }

        public double TopWidth { get; set; }

        public double HeaderFontSize { get; set; }

        public double ContentFontSize { get; set; }

        public double ContentTopOffset { get; set; }


        protected override void LoadData()
        {
            var file = Settings.Default.DataPath + @"\Heat.txt";

            if (!File.Exists(file))
            {
                return;
            }

            var stream = new StreamReader(file);

            this.Title = stream.ReadLine();

            var couples = stream.ReadLine();

            this.Couples = couples.Split(',');

            stream.Close();
        }

        protected override void LoadLayout(string path)
        {
            var doc = XDocument.Load(new StreamReader(path));
            var drawLayout = doc.Root.Elements().FirstOrDefault(x => x.Name == "Draw");

            this.Height = double.Parse(drawLayout.Attributes("Height").First().Value);
            this.Width = double.Parse(drawLayout.Attributes("Width").First().Value);
            this.BackgroundImageFile = drawLayout.Attributes("BackgroundImage").First().Value;

            var headerLayout = drawLayout.Element("Header");
            var contentLayout = drawLayout.Element("Content");

            this.HeaderFontSize = double.Parse(headerLayout.Attributes("FontSize").First().Value);
            this.TopOffset = double.Parse(headerLayout.Attributes("TopMargin").First().Value);
            this.TopWidth = double.Parse(headerLayout.Attributes("Height").First().Value);

            this.ContentTopOffset = double.Parse(contentLayout.Attributes("TopMargin").First().Value);
            this.ContentFontSize = double.Parse(contentLayout.Attributes("FontSize").First().Value);

            if (drawLayout.Attributes("FontColor").Any())
            {
                var color = (Color)ColorConverter.ConvertFromString(drawLayout.Attributes("FontColor").First().Value);
                this.TextColor = new SolidColorBrush(color);
            }
        }
    }
}
