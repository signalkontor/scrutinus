﻿// // TPS.net TPS8 ProjectorClient
// // RoundViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.IO;
using System.Linq;
using System.Windows.Media;
using System.Xml.Linq;
using ProjectorClient.Properties;

namespace ProjectorClient.ViewModels
{
    public class RoundViewModel : ProjectorClientViewModelBase
    {
        public string Round { get; set; }

        public string Competition { get; set; }

        public double HeaderTopMargin { get; set; }

        public double FontSize { get; set; }

        protected override void LoadData()
        {
            var file = Settings.Default.DataPath + @"\Round.txt";

            if (!File.Exists(file))
            {
                return;
            }

            var stream = new StreamReader(file);

            this.Competition = stream.ReadLine();
            this.Round = stream.ReadLine();

            stream.Close();
        }

        protected override void LoadLayout(string path)
        {
            var doc = XDocument.Load(new StreamReader(path));
            var drawLayout = doc.Root.Elements().FirstOrDefault(x => x.Name == "Round");

            this.Height = double.Parse(drawLayout.Attributes("Height").First().Value);
            this.Width = double.Parse(drawLayout.Attributes("Width").First().Value);
            this.BackgroundImageFile = drawLayout.Attributes("BackgroundImage").First().Value;

            var headerLayout = drawLayout.Element("Header");

            this.FontSize = double.Parse(headerLayout.Attributes("FontSize").First().Value);
            this.HeaderTopMargin = double.Parse(headerLayout.Attributes("TopMargin").First().Value);

            if (drawLayout.Attributes("FontColor").Any())
            {
                var color = (Color)ColorConverter.ConvertFromString(drawLayout.Attributes("FontColor").First().Value);
                this.TextColor = new SolidColorBrush(color);
            }
        }
    }
}
