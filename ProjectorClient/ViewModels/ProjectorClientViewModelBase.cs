﻿// // TPS.net TPS8 ProjectorClient
// // ProjectorClientViewModelBase.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using GalaSoft.MvvmLight;

namespace ProjectorClient.ViewModels
{
    public abstract class ProjectorClientViewModelBase : ViewModelBase
    {
        public ProjectorClientViewModelBase()
        {
            var path = Environment.CurrentDirectory + @"\Content\layout.xml";
            this.LoadLayout(path);

            path = Environment.CurrentDirectory + @"\Content\" + this.BackgroundImageFile;
            this.BackgroundImage = new BitmapImage(new Uri(path));

            this.LoadData();
        }

        public ImageSource BackgroundImage { get; set; }

        public Brush TextColor { get; set; }


        public double Width { get; set; }

        public double Height { get; set; }

        public string BackgroundImageFile { get; set; }

        protected abstract void LoadData();

        protected abstract void LoadLayout(string file);
    }
}
