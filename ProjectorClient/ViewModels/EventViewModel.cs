﻿// // TPS.net TPS8 ProjectorClient
// // EventViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.IO;
using System.Linq;
using System.Windows.Media;
using System.Xml.Linq;
using ProjectorClient.Properties;

namespace ProjectorClient.ViewModels
{
    public class EventViewModel : ProjectorClientViewModelBase
    {
        public string EventTitle { get; set; }

        public double TopMargin { get; set; }

        public double FontSize { get; set; }

        protected override void LoadData()
        {
            if (!File.Exists(Settings.Default.DataPath + @"\Event.txt"))
            {
                return;
            }

            this.EventTitle = File.ReadAllText(Settings.Default.DataPath + @"\Event.txt");
        }

        protected override void LoadLayout(string path)
        {
            var doc = XDocument.Load(new StreamReader(path));
            var eventLayout = doc.Root.Elements().FirstOrDefault(x => x.Name == "Event");

            this.Height = double.Parse(eventLayout.Attributes("Height").First().Value);
            this.Width = double.Parse(eventLayout.Attributes("Width").First().Value);
            this.BackgroundImageFile = eventLayout.Attributes("BackgroundImage").First().Value;

            var headerLayout = eventLayout.Element("Header");

            this.FontSize = double.Parse(headerLayout.Attributes("FontSize").First().Value);
            this.TopMargin = double.Parse(headerLayout.Attributes("TopMargin").First().Value);

            if (eventLayout.Attributes("FontColor").Any())
            {
                var color = (Color)ColorConverter.ConvertFromString(eventLayout.Attributes("FontColor").First().Value);
                this.TextColor = new SolidColorBrush(color);
            }

        }
    }
}
