﻿// // TPS.net TPS8 ProjectorClient
// // QualifiedViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml.Linq;
using ProjectorClient.Properties;

namespace ProjectorClient.ViewModels
{
    public class Qualified
    {
        public Qualified(string[] data)
        {
            this.Number = data[0];
            this.Name = data[1];
            this.Country = data[2];

            if (data.Length > 3)
            {
                this.Place = data[3];
            }

            var path = Environment.CurrentDirectory + @"\Content\Flags\" + this.Country + ".png";
            if (File.Exists(path))
            {
                this.CountryFlag = new BitmapImage(new Uri(path));
            }
        }

        public string Number { get; set; }

        public string Name { get; set; }

        public string Country { get; set; }

        public ImageSource CountryFlag { get; set; }

        public string CountryWithComma { get { return ", " + this.Country; } }

        public string Place { get; set; }
    }

    public class QualifiedViewModel : ProjectorClientViewModelBase
    {
        public List<Qualified> Qualifieds { get; set; } = new List<Qualified>();

        public string Title { get; set; }

        public double HeaderFontSize { get; set; }

        public double HeaderTopMargin { get; set; }

        public double ContentTopMargin { get; set; }

        public double ContentFontSize { get; set; }

        public double HeaderHeight { get; set; }

        protected override void LoadData()
        {
            var file = Settings.Default.DataPath + @"\Qualified.txt";
            if (!File.Exists(file))
            {
                return;
            }

            var stream = new StreamReader(file);

            this.Title = "Qualified for " + stream.ReadLine();

            while (!stream.EndOfStream)
            {
                var data = stream.ReadLine().Split(',');
                this.Qualifieds.Add(new Qualified(data));
            }

            stream.Close();
        }

        protected override void LoadLayout(string path)
        {
            var doc = XDocument.Load(new StreamReader(path));
            var drawLayout = doc.Root.Elements().FirstOrDefault(x => x.Name == "Qualified");

            this.Height = double.Parse(drawLayout.Attributes("Height").First().Value);
            this.Width = double.Parse(drawLayout.Attributes("Width").First().Value);
            this.BackgroundImageFile = drawLayout.Attributes("BackgroundImage").First().Value;

            var headerLayout = drawLayout.Element("Header");
            var contentLayout = drawLayout.Element("Content");

            if (headerLayout == null || contentLayout == null)
            {
                return;
            }

            this.HeaderFontSize = double.Parse(headerLayout.Attributes("FontSize").First().Value);
            this.HeaderTopMargin = double.Parse(headerLayout.Attributes("TopMargin").First().Value);
            this.HeaderHeight = double.Parse(headerLayout.Attributes("Height").First().Value);

            this.ContentTopMargin = double.Parse(contentLayout.Attributes("TopMargin").First().Value);
            this.ContentFontSize = double.Parse(contentLayout.Attributes("FontSize").First().Value);

            if (drawLayout.Attributes("FontColor").Any())
            {
                var color = (Color)ColorConverter.ConvertFromString(drawLayout.Attributes("FontColor").First().Value);
                this.TextColor = new SolidColorBrush(color);
            }
        }
    }
}
