﻿// // TPS.net TPS8 ProjectorClient
// // MainWindow.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.IO;
using System.Threading;
using System.Windows;
using ProjectorClient.Properties;
using ProjectorClient.ViewModels;
using ProjectorClient.Views;

namespace ProjectorClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly FileSystemWatcher fileSystemWatcher;

        private DateTime lastActivity = DateTime.Now;

        private LowerThirdView lowerThirdView;

        private TVTitleView tvTitleView;

        public MainWindow()
        {
            this.InitializeComponent();

            try
            {
                this.fileSystemWatcher = new FileSystemWatcher(Settings.Default.DataPath, "*.txt");
            }
            catch (Exception ex)
            {
                MessageBox.Show(
                    "Could not configure file system. Please check folder 'DataPath' in the settings. This folder musst point to the same folder as configured in TPS.net");
                this.Close();
                return;
            }

            this.fileSystemWatcher.Changed += this.FileSystemWatcherOnChanged;
            this.fileSystemWatcher.Created += this.FileSystemWatcherOnChanged;

            this.fileSystemWatcher.EnableRaisingEvents = true;

            this.TransitioningContentControl.Content = new EventView();
        }

        private void FileSystemWatcherOnChanged(object sender, FileSystemEventArgs fileSystemEventArgs)
        {
            var diff = this.lastActivity.Subtract(DateTime.Now);
            if (Math.Abs(diff.TotalMilliseconds) < 500)
            {
                return;
            }

            this.lastActivity = DateTime.Now;

            // Check, we can open the file (could be in use)
            var errors = 0;
            while (errors < 20)
            {
                try
                {
                    var stream = new StreamReader(fileSystemEventArgs.FullPath);
                    // Ok, worked - just close the file again
                    stream.Close();
                    break;
                }
                catch (Exception)
                {
                    errors++;
                    Thread.Sleep(250);
                }
            }

            if (errors == 20)
            {
                return;
            }

            this.Dispatcher.Invoke(() =>
            {
                if (fileSystemEventArgs.Name == "Heat.txt")
                {
                    this.TransitioningContentControl.Content = new DrawView();
                }

                if (fileSystemEventArgs.Name == "Event.txt")
                {
                    this.TransitioningContentControl.Content = new EventView();
                }

                if (fileSystemEventArgs.Name == "Qualified.txt")
                {
                    this.TransitioningContentControl.Content = new QualifiedView();
                }

                if (fileSystemEventArgs.Name == "Round.txt")
                {
                    this.TransitioningContentControl.Content = new RoundView();
                }

                if (fileSystemEventArgs.Name == "GeneralText.txt")
                {
                    this.TransitioningContentControl.Content = new GeneralTextView();
                }

                if (fileSystemEventArgs.Name == "LowerThird.txt")
                {
                    if (this.lowerThirdView == null)
                    {
                        this.lowerThirdView = new LowerThirdView();
                    }

                    this.TransitioningContentControl.TransitionCompleted += this.FadeInLowerThirdEvent;

                    if(this.TransitioningContentControl.Content == this.lowerThirdView)
                    {
                        this.lowerThirdView.FadeInOrOut();
                    }
                    else
                    {
                        this.TransitioningContentControl.Content = this.lowerThirdView;
                    }
                }

                if (fileSystemEventArgs.Name == "TVTitle.txt")
                {
                    if (this.tvTitleView == null)
                    {
                        this.tvTitleView = new TVTitleView();
                    }

                    this.TransitioningContentControl.TransitionCompleted += this.FadeInTVTitleEvent;

                    if (this.TransitioningContentControl.Content == this.tvTitleView)
                    {
                        this.tvTitleView.FadeInOrOut();
                    }
                    else
                    {
                        this.TransitioningContentControl.Content = this.tvTitleView;
                    }
                }

                if (fileSystemEventArgs.Name == "PricePresentation.txt")
                {
                    if (this.TransitioningContentControl.Content is PricePresentationView)
                    {
                        var view = this.TransitioningContentControl.Content as PricePresentationView;
                        var viewModel = view.DataContext as PricePresentationViewModel;
                        viewModel.UpdatePlacings();
                    }
                    else
                    {
                        this.TransitioningContentControl.Content = new PricePresentationView();
                    }
                }
            });

            
        }

        private void FadeInTVTitleEvent(object sender, RoutedEventArgs e)
        {
            this.tvTitleView.FadeInOrOut();
            this.TransitioningContentControl.TransitionCompleted -= this.FadeInTVTitleEvent;
        }

        private void FadeInLowerThirdEvent(object sender, RoutedEventArgs e)
        {
            this.lowerThirdView.FadeInOrOut();
            this.TransitioningContentControl.TransitionCompleted -= this.FadeInLowerThirdEvent;
        }
    }
}
