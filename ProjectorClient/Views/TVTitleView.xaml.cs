﻿// // TPS.net TPS8 ProjectorClient
// // TVTitleView.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using ProjectorClient.Properties;

namespace ProjectorClient.Views
{
    /// <summary>
    /// Interaction logic for LowerThirdView.xaml
    /// </summary>
    public partial class TVTitleView : UserControl
    {
        private string text = "";

        public TVTitleView()
        {
            var path = Environment.CurrentDirectory + @"\Content\Tite.png";
            this.BackgroundImage = new BitmapImage(new Uri(path));

            this.InitializeComponent();

            this.LoadData();
        }

        public ImageSource BackgroundImage { get; set; }

        public void FadeInOrOut()
        {
            this.LoadData();

            if(string.IsNullOrWhiteSpace(this.text))
            {
                this.FadeOut();
            }
            else
            {
                this.FadeIn();
            }

        }

        private void FadeOut()
        {
            var sb = this.FindResource("FadeOutTitle") as Storyboard;
            Storyboard.SetTarget(sb, this.TitleText);

            sb.Begin();
        }

        private void FadeIn()
        {
            this.TitleText.Text = this.text;
            this.TitleText.Visibility = Visibility.Visible;
            this.TitleText.Height = 0;

            var sb = this.FindResource("FadeInTitle") as Storyboard;
            Storyboard.SetTarget(sb, this.TitleText);

            sb.Begin();
        }

        private void LoadData()
        {
            var file = Settings.Default.DataPath + @"\TVTitle.txt";
            if (!File.Exists(file))
            {
                return;
            }

            this.text = File.ReadAllText(file);
        }
    }
}
