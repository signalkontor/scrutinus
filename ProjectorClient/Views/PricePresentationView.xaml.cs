﻿// // TPS.net TPS8 ProjectorClient
// // PricePresentationView.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows.Controls;

namespace ProjectorClient.Views
{
    /// <summary>
    /// Interaction logic for PricePresentationView.xaml
    /// </summary>
    public partial class PricePresentationView : UserControl
    {
        public PricePresentationView()
        {
            this.InitializeComponent();
        }
    }
}
