﻿// // TPS.net TPS8 ProjectorClient
// // GeneralTextView.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows.Controls;

namespace ProjectorClient.Views
{
    /// <summary>
    /// Interaction logic for GeneralTextView.xaml
    /// </summary>
    public partial class GeneralTextView : UserControl
    {
        public GeneralTextView()
        {
            this.InitializeComponent();
        }
    }
}
