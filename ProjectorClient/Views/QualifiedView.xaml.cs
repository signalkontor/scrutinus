﻿// // TPS.net TPS8 ProjectorClient
// // QualifiedView.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows.Controls;

namespace ProjectorClient.Views
{
    /// <summary>
    /// Interaction logic for QualifiedView.xaml
    /// </summary>
    public partial class QualifiedView : UserControl
    {
        public QualifiedView()
        {
            this.InitializeComponent();
        }
    }
}
