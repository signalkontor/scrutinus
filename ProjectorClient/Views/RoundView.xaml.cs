﻿// // TPS.net TPS8 ProjectorClient
// // RoundView.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows.Controls;

namespace ProjectorClient.Views
{
    /// <summary>
    /// Interaction logic for RoundView.xaml
    /// </summary>
    public partial class RoundView : UserControl
    {
        public RoundView()
        {
            this.InitializeComponent();
        }
    }
}
