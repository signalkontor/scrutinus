﻿// // TPS.net TPS8 ProjectorClient
// // LowerThirdView.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using ProjectorClient.Properties;

namespace ProjectorClient.Views
{
    /// <summary>
    /// Interaction logic for LowerThirdView.xaml
    /// </summary>
    public partial class LowerThirdView : UserControl
    {
        private string text = "";

        public LowerThirdView()
        {
            var path = Environment.CurrentDirectory + @"\Content\bgLowerThird.png";
            this.BackgroundImage = new BitmapImage(new Uri(path));

            this.InitializeComponent();

            this.LoadData();
        }

        public ImageSource BackgroundImage { get; set; }

        public void FadeInOrOut()
        {
            this.LoadData();

            if(string.IsNullOrWhiteSpace(this.text))
            {
                this.FadeOut();
            }
            else
            {
                this.FadeIn();
            }

        }

        private void FadeOut()
        {
            

            var sb = this.FindResource("FadeOutLowerThird") as Storyboard;
            Storyboard.SetTarget(sb, this.LowerThirdText);

            sb.Begin();
        }

        private void FadeIn()
        {
            this.LowerThirdText.Height = 0;
            this.LowerThirdText.Text = this.text;
            this.LowerThirdText.Visibility = Visibility.Visible;
            var sb = this.FindResource("FadeInLowerThird") as Storyboard;
            Storyboard.SetTarget(sb, this.LowerThirdText);

            sb.Begin();
        }

        private void LoadData()
        {
            var file = Settings.Default.DataPath + @"\LowerThird.txt";
            if (!File.Exists(file))
            {
                return;
            }


            this.text = File.ReadAllText(file);

            
        }
    }
}
