package com.signalkontor;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;
import com.signalkontor.constants.Constants;
import com.signalkontor.data.Tournament;
import com.signalkontor.messaging.MessageManager;
import com.signalkontor.messaging.TPSMessage;

import android.util.TypedValue;
import android.view.Gravity;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TabHost.OnTabChangeListener;
import android.app.TabActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.gesture.Gesture;
import android.gesture.GestureLibraries;
import android.gesture.GestureLibrary;
import android.gesture.GestureOverlayView;
import android.gesture.Prediction;
import android.gesture.GestureOverlayView.OnGesturePerformedListener;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.HorizontalScrollView;
import android.widget.TabHost;
import android.widget.TextView;
import android.view.View.OnClickListener;
import com.signalkontor.utils.BatteryUtils;
import com.signalkontor.utils.LocaleUtils;

public class PagerActivity extends TabActivity implements OnGesturePerformedListener, OnTabChangeListener, OnClickListener, DialogInterface.OnClickListener, Observer, Restartable {
	
	private static final String SCROLL_X = "scrollX";
	private static final String SELECTED_TAB = "selectedTab";
	private static final int TAB_WIDTH = 60;
    private static final int TAB_HEIGHT = 40;
    private static final int TAB_BASE_LENGTH = 9;

    private int tabWidth;
    private TextView marksTextView, helpMarks1TextView, helpMarks2TextView, heatsTextView, couplesTextView;
	private HorizontalScrollView scrollView;
	private GestureLibrary gestureLibrary;

	private Tournament tournament;
	private Tournament.Dance dance;

	private void loadGestureLibrary() {
		gestureLibrary = GestureLibraries.fromRawResource(this, R.raw.gestures);
		if (!gestureLibrary.load()) {
		    System.exit(0);
		}
		GestureOverlayView gestures = (GestureOverlayView)findViewById(R.id.gestures);
		gestures.addOnGesturePerformedListener(this);		
	}

	@Override
    protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        tournament = Tournament.getInstance();
        if(LocaleUtils.changeLocale(this, tournament.getLanguage())) {
            return; // language has to be changed => do not continue
        }

        setContentView(R.layout.pager);

        if(!MessageManager.isInitialized()) {
            AutoConfigureTask autoConfigureTask = new AutoConfigureTask(this);
            autoConfigureTask.disableDialog();
            autoConfigureTask.execute((Object[]) null);
        }

		if(gestureLibrary == null)
			loadGestureLibrary();
		
		// For some unknown reason it's not possible to disable scrollbar-fading
		// by an attribute in pager.xml. So we do it manually.
		scrollView = (HorizontalScrollView)findViewById(R.id.tabScroller);
		scrollView.setScrollbarFadingEnabled(false);

		marksTextView = (TextView)findViewById(R.id.pagerMarks);
        helpMarks1TextView = (TextView) findViewById(R.id.pagerHelpMarks1);
        helpMarks2TextView = (TextView) findViewById(R.id.pagerHelpMarks2);
		heatsTextView = (TextView)findViewById(R.id.pagerHeats);
		couplesTextView = (TextView)findViewById(R.id.pagerCouples);

        findViewById(R.id.pagerContinueButton).setOnClickListener(this);

        tournament.addObserver(this);
		dance = tournament.getCurrentDance();
        tabWidth = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, TAB_WIDTH, getResources().getDisplayMetrics());

        updateTitle();
		updateTournamentInfos();
        createTabs();

        if(tournament.isChangedByUser())
        {
            final SharedPreferences prefs = getPreferences(MODE_PRIVATE);
            final int scrollX = prefs.getInt(SCROLL_X, 0);
            final int selectedTab = prefs.getInt(SELECTED_TAB, 0);
            getTabHost().setCurrentTab(selectedTab);
            onTabChanged(selectedTab);
            // For some mysterious reason one cannot scroll a scroll view
            // directly. Instead one have to add a runnable the the scroll
            // views message queue that does the scrolling.
            scrollView.post(new Runnable() {
                public void run() {
                    scrollView.scrollTo(scrollX, 0);
                }
            });
        }
        getTabHost().setOnTabChangedListener(this);
	}

    private void clearTabs() {
        final TabHost tabHost = getTabHost();
        tabHost.setCurrentTab(0); // some source say this has to be done before calling clearAllTabs()
        tabHost.getTabWidget().removeAllViews();
        tabHost.clearAllTabs();
    }

    // Da Android ein völlig sinnfreies Caching von Tab-Inhalten durchführt müssen wir bei jedem Tab
    // das wir anzeigen wollen diesen Wert hochzählen. Dieser Wert wird im Tag des Tabs benutzt.
    // Verwendet man für einen Tab ein Tag das man schon zuvor verwendet hat fügt Android einfach
    // den alten Tabinhalt wieder ein.
    private static int tabCounter = 0;

    private void createTabs() {
        final TabHost tabHost = getTabHost();
        final String tabBaseName = String.format("tab_%04d_", tabCounter++);
        final int length = dance.getHeats().length;
	    for(int i = 0; i < length; i++)
	    {
		    tabHost.addTab(tabHost
		    	.newTabSpec(tabBaseName + i)
		    	.setIndicator(makeTabIndicator(Integer.toString(i+1)))
		    	.setContent(new Intent()
		    		.setClass(this, PageActivity.class)
		    		.putExtra(Constants.EXTRA_DANCE_INDEX, dance.getIndex())
		    		.putExtra(Constants.EXTRA_HEAT_INDEX, i))
		    	);
	    }
    }

    public void updateTitle() {
        setTitle(new StringBuffer()
            .append(dance.getDisplayName())
            .append(" (")
            .append(tournament.getDeviceId())
            .append('@')
            .append(MessageManager.getSSID())
            .append(')')
            .append(" - ")
            .append(Tournament.getInstance().getJudgeFullName())
         );
    }

    private TextView makeTabIndicator(String text) {
		final TextView tabView = new TextView(this);
		final int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, TAB_HEIGHT, getResources().getDisplayMetrics());
		final LayoutParams layoutParams = new LayoutParams(tabWidth, height, 1);
		layoutParams.setMargins(1, 0, 1, 0);
		tabView.setLayoutParams(layoutParams);
		tabView.setText(text);
		tabView.setTextColor(Color.WHITE);
		tabView.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL);
		tabView.setBackgroundDrawable(getResources().getDrawable(R.drawable.tab_indicator));
		tabView.setPadding(13, 0, 13, 0);
		return tabView;
	}
	
	@Override
    public void onConfigurationChanged(Configuration configuration) {
		super.onConfigurationChanged(configuration);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); // prevent screen rotation
	}

    @Override
    public void onResume() {
        super.onResume();
        BatteryUtils.startReceiving(this);
    }

	@Override
    public void onPause() {
		getPreferences(MODE_PRIVATE).edit()
			.putInt(SELECTED_TAB, getTabHost().getCurrentTab())
			.putInt(SCROLL_X, scrollView.getScrollX())
			// save more settings here
			.commit();

        BatteryUtils.stopReceiving(this);
		super.onPause();
	}

	public void onGesturePerformed(GestureOverlayView overlay, Gesture gesture) {
		ArrayList<Prediction> predictions = gestureLibrary.recognize(gesture);

	    // We want at least one prediction
	    if (!predictions.isEmpty()) {
	        Prediction prediction = predictions.get(0);
	        // We want at least some confidence in the result
	        if (prediction.score > 1.0) {
	        	final TabHost tabHost = getTabHost();
	        	final int currentTab = tabHost.getCurrentTab(); 
	        	final int tabCount = tabHost.getTabWidget().getChildCount();
	        	
	        	if("NextPage".equals(prediction.name) && currentTab + 1 < tabCount) {
	        		tabHost.setCurrentTab(currentTab + 1);
	        	} else if("PreviousPage".equals(prediction.name) && currentTab > 0) {
	        		tabHost.setCurrentTab(currentTab - 1);
	        	} else if("FirstPage".equals(prediction.name)) {
	        		tabHost.setCurrentTab(0);
	        	} else if("LastPage".equals(prediction.name)) {
	        		tabHost.setCurrentTab(tabCount - 1);
	        	}	        	
	        	
	        	final int scrollX = tabHost.getCurrentTab() * tabWidth;
	        	scrollView.post(new Runnable() {
	    			public void run() {
	    				scrollView.smoothScrollTo(scrollX, 0);
	    			}
	    		});	        	
	        }
	    }
	}

	private void updateTournamentInfos() {
		final int setMarks = dance.getMarkCount();
        final int helpmarks1 = dance.getHelpMark1Count();
        final int helpmarks2 = dance.getHelpMark2Count();
        // final int Helpmarks1 = dance.get
		final int minMarks = tournament.getMinMarks();
		final int maxMarks = tournament.getMaxMarks();
		final Tournament.Heat[] heats = dance.getHeats();
		final int heatCount = heats.length;
		int tmp = 0;
		for(int i = 0; i < heatCount; i++) {
			tmp += heats[i].getCouples().length;
		}
		final int coupleCount = tmp;
		// TODO re-use Runnables
		marksTextView.post(new Runnable() {
			public void run() {
                if(minMarks != maxMarks)
				    marksTextView.setText(String.format("%1$d/%2$d-%3$d", setMarks, minMarks, maxMarks));
                else
                    marksTextView.setText(String.format("%1$d/%2$d", setMarks, minMarks));
				if(dance.hasValidNumberOfMarks()) {
                    marksTextView.setTextColor(Color.GREEN);
				} else {
                    marksTextView.setTextColor(Color.WHITE);
				}
			}
		});
		heatsTextView.post(new Runnable() {
			public void run() {
				heatsTextView.setText("" + heatCount);
			}
		});
		couplesTextView.post(new Runnable() {
			public void run() {
				couplesTextView.setText("" + coupleCount);
			}
		});		
        
        helpMarks1TextView.post(new Runnable() {
            @Override
            public void run() {
                helpMarks1TextView.setText(String.format("%1$d", helpmarks1));
            }
        });

        helpMarks2TextView.post(new Runnable() {
            @Override
            public void run() {
                helpMarks2TextView.setText(String.format("%1$d", helpmarks2));
            }
        });
	}

    private void switchToActivity(Class nextActivityClass) {
        // remove us from the observer list to prevent dangling references
        Tournament.getInstance().deleteObserver(this);
        startActivity(new Intent(this, nextActivityClass).putExtra(Constants.EXTRA_FROM_JUDGING_INDEX, true));
        finish();
    }

	public void update(Observable observable, Object data) {
		if(observable instanceof Tournament) {
			Tournament t = (Tournament)observable;
			if(t.isClosing()) {
                switchToActivity(StartActivity.class);
			} else if(data == Tournament.COUPLES_MODIFIED) {
                switchToActivity(PagerActivity.class);
            } else if(data == Tournament.HEAT_DANCE_CHANGED) {
                getPreferences(MODE_PRIVATE).edit()
                        .putInt(SELECTED_TAB, t.getCurrentGroup());
                switchToActivity(PagerActivity.class);
            } else {
			    updateTournamentInfos();
            }
		}
	}

    private void onTabChanged(int index) {
        final int dance = 1 + this.dance.getIndex();
        MessageManager.setCurrentStatusMessage(TPSMessage.newStatusMessage(1, 0, dance, index));
    }

	// tabId has format "tab_XX_#" where # is the number of the tab, which is one less than the heat number (XX is the dance index)
	public void onTabChanged(String tabId) {
		onTabChanged(1 + Integer.parseInt(tabId.substring(TAB_BASE_LENGTH)));
	}

    public void onClick(View view) {
        final Resources resources = getResources();
        final Tournament tournament = Tournament.getInstance();
        final int minMarks = tournament.getMinMarks();
        final int maxMarks = tournament.getMaxMarks();
        final int markCount = dance.getMarkCount();
        final String requiredMarks = minMarks == maxMarks ? "" + minMarks : String.format("%d-%d", minMarks, maxMarks);

        if(dance.hasValidNumberOfMarks()) {
            final String message = dance.isLast() ?
                    resources.getString(R.string.continueToSigning) :
                    dance.hasCorrectNumberOfMarks() ?
                        resources.getString(R.string.advanceToNextDance) :
                        resources.getString(R.string.pagerContinueConfirmWrongNumberOfMarksMessage, requiredMarks, markCount);

            new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton(R.string.yes, this)
                .setNegativeButton(R.string.no, null)
			    .show();
            if(dance.isLast())
            {
                // Send complete marking back to server
                MessageManager.offer(TPSMessage.newFullJudgingMessage(0, false));
            }
        } else if(getTabHost().getCurrentTab() + 1 == dance.getHeats().length) {
            final String message = resources.getString(R.string.pagerInvalidNumberOfMarks, requiredMarks, markCount);
            Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        } else {
            getTabHost().setCurrentTab(getTabHost().getCurrentTab() + 1);
        }
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        final Tournament tournament = Tournament.getInstance();
        if(tournament.advanceToNextDance()){
            dance = tournament.getCurrentDance();
            updateTitle();
            updateTournamentInfos();
            clearTabs();
            createTabs();
        } else {
            tournament.deleteObserver(this); // remove us from the observer list to prevent dangling references
            startActivityForResult(new Intent(this, SigningActivity.class), 1);
        }
    }

    public void switchDance()
    {
        final Tournament tournament = Tournament.getInstance();
        dance = tournament.getCurrentDance();
        updateTitle();
        updateTournamentInfos();
        clearTabs();
        createTabs();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data) {
        // signature is done, lets finish
        finish();
    }

    @Override
    public void restart() {
        // reload activity to get new language
        finish();
        startActivity(new Intent(this, PagerActivity.class));
    }

    @Override
    public void onAttachedToWindow()
    {
           // this.getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD);
           super.onAttachedToWindow();
    }
}