package com.signalkontor.messaging;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.*;
import java.util.Enumeration;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import android.content.Context;
import android.net.DhcpInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;
import android.widget.Toast;
import com.signalkontor.data.Tournament;

public class MessageManager {

	// The .NET side uses UTF-16 with Little Endian byte orders
	private static final int AUTOCONFIG_PORT = 9095;
	private static final int AUTOCONFIG_RECEIVE_TIMEOUT = 10000; // 10 seconds
	private static final int AUTOCONFIG_RECEIVE_BUFFER_LENGTH = 256;
	private static final String AUTOCONFIG_BROADCAST_ADDRESS = "255.255.255.255";
	private static final String AUTOCONFIG_CHARACTER_ENCODING = "UTF-16LE";
	
	private static final int MESSAGING_PORT = 9090;
	private static final String MESSAGE_CHARACTER_ENCODING = "UTF-8";
	private static final String PLATFORM_IDENTIFIER = "android";

	private static InetAddress serverAddress;

    private static boolean initialized = false;
	
	private static String deviceId;
	private static String ssid;
	
	private static final Object currentStatusMessageLocker = new Object();
	private static COMMessage currentStatusMessage = null;
	private static final BlockingQueue<COMMessage> messageQueue = new ArrayBlockingQueue<COMMessage>(2048);
    private static final String TAG = "MessageManager";

    private MessageManager() { }

	@SuppressWarnings({"BooleanMethodIsAlwaysInverted"})
    public static boolean isInitialized() {
		return initialized;
	}

	private static String getLocalIpAddress() {
		try {
			for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
				NetworkInterface intf = en.nextElement();
				for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
					InetAddress inetAddress = enumIpAddr.nextElement();
                    java.net.Inet4Address v4Address = null;
                    try{
                        v4Address = (Inet4Address) inetAddress;
                    }catch (Exception ignore){}

					if (v4Address != null && !inetAddress.isLoopbackAddress()) {
						return inetAddress.getHostAddress();
					}
				}
			}
		} catch (Exception ignored) { }
		return null;
	}

    static InetAddress getBroadcastAddress(Context mContext) throws IOException {
        WifiManager wifi = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
        DhcpInfo dhcp = wifi.getDhcpInfo();
        // handle null somehow

        int broadcast = (dhcp.ipAddress & dhcp.netmask) | ~dhcp.netmask;
        byte[] quads = new byte[4];
        for (int k = 0; k < 4; k++)
            quads[k] = (byte) ((broadcast >> k * 8) & 0xFF);
        return InetAddress.getByAddress(quads);
    }

	@SuppressWarnings({"BooleanMethodIsAlwaysInverted"})
    public static boolean initialize(Context context) throws IOException {
		if(initialized)
			throw new IllegalStateException("already initialized");

		ssid = deviceId = null;
		
		final WifiManager wifiManager = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);
		final WifiInfo wifiInfo = wifiManager.getConnectionInfo();

		// Check if WiFi is available. On some devices (e.g. Samsung Galaxy Pad) this does not work.
		// On those devices WifiInfo contains the data from the last WLAN network they were connected to.
		// In that case we just continue and check if we can send the discovery packet (see below).    
		/*if(wifiInfo.getNetworkId() == -1)
			throw new NoWifiException();*/

		ssid = wifiInfo.getSSID();

		final byte[] payload = new StringBuffer()
			.append(ssid)
			.append(';')
			.append(wifiInfo.getMacAddress())
			.append(';')
			.append(PLATFORM_IDENTIFIER)
			.toString()
			.getBytes(AUTOCONFIG_CHARACTER_ENCODING);

		// final InetAddress broadcastAddress = getBroadcastAddress(context); //InetAddress.getByName(AUTOCONFIG_BROADCAST_ADDRESS);
        final InetAddress broadcastAddress = InetAddress.getByName(AUTOCONFIG_BROADCAST_ADDRESS);
		final InetAddress localAddress = InetAddress.getByName(getLocalIpAddress());
		// final DatagramSocket socket = new DatagramSocket(AUTOCONFIG_PORT, localAddress);
        DatagramSocket socket = new DatagramSocket();
        // final MulticastSocket socket = new MulticastSocket();
        // socket.joinGroup(broadcastAddress);
        socket.setBroadcast(true);
		DatagramPacket packet = new DatagramPacket(payload, payload.length, broadcastAddress, AUTOCONFIG_PORT);

		try
		{
			socket.send(packet);
		} catch(Exception e) {
			socket.close();
			throw new NoWifiException(e.getMessage());
		}
        // socket = new DatagramSocket(AUTOCONFIG_PORT, localAddress);
		// receive answer
        WifiManager.MulticastLock lock = wifiManager.createMulticastLock("ejudge");
        lock.acquire();

		byte[] buffer = new byte[AUTOCONFIG_RECEIVE_BUFFER_LENGTH];
        DatagramSocket socketrec = new DatagramSocket(AUTOCONFIG_PORT);
		DatagramPacket packetRec = new DatagramPacket(buffer, buffer.length);
		socketrec.setSoTimeout(AUTOCONFIG_RECEIVE_TIMEOUT);
        socketrec.setBroadcast(true);

		try
		{
			socketrec.receive(packetRec);
            String data = new String(packetRec.getData(), 0, packetRec.getLength(), AUTOCONFIG_CHARACTER_ENCODING);
            if(data.indexOf(";android") >0 )
            {
                // We got our own message, read again
                socketrec.receive(packetRec);
            }
		} catch(SocketTimeoutException e)
		{
			socketrec.close();
			return initialized;
		}
        finally {
            lock.release();
        }

		serverAddress = packetRec.getAddress();

		deviceId = new String(packetRec.getData(), 0, packetRec.getLength(), AUTOCONFIG_CHARACTER_ENCODING);
        Tournament.getInstance().setDeviceId(deviceId);
		
		if(beginSendMessage())
		{
			initialized = sendMessage(new LogonMessage(deviceId, ""));
			endSendMessage();
		}

		if(initialized) {
			new Thread(new Runnable() {
				public void run() {
					messageListenerThread(localAddress);
				}
			}).start();
			
			new Thread(new Runnable() {
				public void run() {
					messageSenderThread();
				}
			}).start();
		}
		
		setCurrentStatusMessage(TPSMessage.newStatusMessage(0, 0, 0, 0));
		return initialized;
	}
		
	private static void messageListenerThread(InetAddress localAddress) {
		Socket socket = null;
		InputStream in = null;
		OutputStream out = null;
        ServerSocket serverSocket = null;

        while(true)
        {
            try {
                serverSocket = new ServerSocket(MESSAGING_PORT);

                while(true)
                {
                    socket = serverSocket.accept();

                    // we don't expect many parallel connection, so we don't spawn new threads
                    // for incoming connections
                    in = socket.getInputStream();
                    out = socket.getOutputStream();

                    // read length of message
                    byte[] lengthBuffer = new byte[4];
                    if(readComplete(in, lengthBuffer)) {
                        // got length
                        int messageLength = convert(lengthBuffer);
                        byte[] messageBuffer = new byte[messageLength];
                        if(readComplete(in, messageBuffer)) {
                            if(messageLength > 1) {
                                COMMessage message = COMMessage.fromJson(new String(messageBuffer, MESSAGE_CHARACTER_ENCODING));
                                Log.v(TAG, "COMMessage: " + message.getAppId());
                                MessageHandler.handle(message);
                            } else {
                                Log.v(TAG, "Ping");
                            }
                            out.write(1);
                            out.flush();
                        }
                    }
                    out.close();
                    in.close();
                    socket.close();
                }
            } catch(Exception e) {
                Log.w(TAG, e);
                if(out != null) try { out.close(); } catch (IOException ignored) { }
                if(in != null) try { in.close(); } catch (IOException ignored) { }
                if(socket != null) try { socket.close(); } catch (IOException ignored) { }
                if(serverSocket != null) try { serverSocket.close(); } catch (IOException ignored) { }
            }
        }
	}
	
	private static boolean readComplete(InputStream in, byte[] buffer) throws IOException {
		int totalBytesRead = 0;
		while(totalBytesRead < buffer.length) {
			int bytesRead = in.read(buffer, totalBytesRead, buffer.length - totalBytesRead);
			if(bytesRead == -1)
				return false;
			totalBytesRead += bytesRead;
		}
		return true;
	}
	
	private static int convert(byte[] data) {
		int number = 0;
		for (int i = 0; i < 4; ++i) {
		    number |= (data[i] & 0xff) << (i << 3);
		}
		return number;
	}

	private static boolean sendStatusMessageIfAvailable() {
		final COMMessage statusMessage;
		synchronized (currentStatusMessageLocker) {
			statusMessage = currentStatusMessage;
			currentStatusMessage = null;
		}

        return statusMessage == null || sendMessage(statusMessage);
	}

    private static void sendPing() {
        sendMessage(new LogonMessage(deviceId, ""));
    }
		
	private static void messageSenderThread() {
		while(true) {
			boolean lastSendSucceeded = false;
			if(beginSendMessage()) {
                sendPing();
				lastSendSucceeded = sendStatusMessageIfAvailable();
				COMMessage msg;
				while((msg = messageQueue.peek()) != null)
				{
					if(lastSendSucceeded = sendMessage(msg))
					{
						messageQueue.remove();
					} else {
						break;
					}
				}
				endSendMessage();
			}
			try {
				if(lastSendSucceeded) {
					// wait indefinitely until new messages are queued
                    synchronized (MessageManager.class) {
                        MessageManager.class.wait(15000); // sleep for max 15 seconds
                    }
				} else {
					// sleep for some time, then retry sending the queued messages
                    //offer(new LogonMessage(deviceId, ""));
					Thread.sleep(5000);

				}
			} catch(InterruptedException ignored) {}
		}
	}
	
	private static Socket socket;
	private static InputStream inputStream;
	private static OutputStream outputStream;
	
	private static boolean beginSendMessage() {
		try
		{
			socket = new Socket(serverAddress, MESSAGING_PORT);
			outputStream = socket.getOutputStream();
			inputStream = socket.getInputStream();
		}
		catch(IOException e) {
			if(socket != null) {
				try {
					socket.close();
					socket = null;
				} catch(Exception ignored) {}
			}
			return false;
		}
		return true;
	}
	
	private static void endSendMessage() {
		try {
			if(socket != null) {
				socket.close();
				inputStream = null;
				outputStream = null;
				socket = null;
			}
		} catch(IOException ignored) {}
	}

    private static boolean sendMessage(COMMessage msg) {
        try
        {
            final String json = msg.toJson();
            final byte[] data = json.getBytes(MESSAGE_CHARACTER_ENCODING);
            return sendMessage(data);
        } catch(IOException e) {
            return false;
        }
    }
	
	private static boolean sendMessage(final byte[] data) {
		try
		{
			final byte[] length = new byte[4];
			for (int i = 0; i < 4; ++i) {
			    int shift = i << 3; // i * 8
			    length[i] = (byte)((data.length & (0xff << shift)) >>> shift);
			}
			outputStream.write(length);
			outputStream.write(data);
			outputStream.flush();
			final int res = inputStream.read();
			return res != -1;
		}
		catch(IOException e) { return false; }
	}

	public static String getSSID() {
		return ssid != null ? ssid : "offline";
	}

	public static String getDeviceId() {
		return deviceId != null ? deviceId : "offline";
	}
	
	public synchronized static void setCurrentStatusMessage(TPSMessage statusMessage) {
		synchronized (currentStatusMessageLocker) {
			currentStatusMessage = statusMessage;
		}
        MessageManager.class.notify();
	}
	
	public synchronized static boolean offer(COMMessage msg) {
		final boolean res = messageQueue.offer(msg);
        MessageManager.class.notify();
		return res;
	}
}
