package com.signalkontor;

/**
 * User: Mikko
 * Date: 24.06.11
 * Time: 13:16
 */
public interface Restartable {
    public void restart();
}
