package com.signalkontor.utils;

import java.util.Locale;
import java.util.Observer;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.content.Context;
import com.signalkontor.Restartable;
import com.signalkontor.StartActivity;
import com.signalkontor.data.Tournament;

public class LocaleUtils {
    public static Locale currentLocale;

    public static String getLocaleString(int language) {
         switch(language) {
            case 1: return "de";
            case 2: return "en";
            case 3: return "it";
            case 4: return "es";
            case 5: return "pt";
            case 6: return "ru";
            case 7: return "nl";
            case 8: return "ca";
            case 9: return "fr";
            case 10: return "dk";
            case 11: return "hu";
            case 12: return "fi";
            default: return null;
        }
    }

	public static boolean changeLocale(final Activity activity, final int language) {
        String localeString = getLocaleString(language);
        return localeString != null && changeLocale(activity, localeString);
    }

	private static boolean changeLocale(final Activity activity, String localeStr) {
        final Locale locale = new Locale(localeStr);
        if(locale.equals(currentLocale))
            return false;

        final Configuration config = new Configuration();
        Locale.setDefault(locale);
        config.locale = locale;
        activity.getResources().updateConfiguration(config, activity.getResources().getDisplayMetrics());
        currentLocale = locale;

        if(activity instanceof Restartable) {
            ((Restartable)activity).restart();
        }
        return true;
	}
}
