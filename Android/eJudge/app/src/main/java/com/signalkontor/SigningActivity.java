/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.signalkontor;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.signalkontor.constants.Constants;
import com.signalkontor.messaging.MessageManager;
import com.signalkontor.messaging.TPSMessage;
import com.signalkontor.widget.SignatureView;

/**
 * Demonstrates the handling of touch screen and trackball events to
 * implement a simple painting app.
 */
public class SigningActivity extends Activity implements View.OnClickListener {
    private static float scale;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Create and attach the view that is responsible for painting.
        setContentView(R.layout.signature);

        final SignatureView signatureView = (SignatureView)findViewById(R.id.signatureView);
        final int signatureHeight = (int) (getResources().getDisplayMetrics().widthPixels / 1.8);
        scale = 120.0f / signatureHeight;

        signatureView.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, signatureHeight));
        signatureView.requestFocus();

        findViewById(R.id.signatureClearButton).setOnClickListener(this);
        findViewById(R.id.signatureContinueButton).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.signatureClearButton:
                ((SignatureView)findViewById(R.id.signatureView)).clear();
                break;
            case R.id.signatureContinueButton:
                CharSequence signature = ((SignatureView)findViewById(R.id.signatureView)).getSignature(scale);
                MessageManager.offer(TPSMessage.newSigningMessage(signature));
                setResult(1);
                try{
                    Thread.sleep(2000);
                }catch(Exception ex)
                {

                }

                startActivity(new Intent(this, StartActivity.class).putExtra(Constants.EXTRA_FROM_JUDGING_INDEX, true));
                finish();
                break;
        }
    }

    @Override
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); // prevent screen rotation
    }

    @Override
    public void onAttachedToWindow()
    {
           // this.getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD);
           super.onAttachedToWindow();
    }

    @Override
    public void onBackPressed() {
        Toast.makeText(this, R.string.signaturePleaseSign, Toast.LENGTH_SHORT).show();
    }
}