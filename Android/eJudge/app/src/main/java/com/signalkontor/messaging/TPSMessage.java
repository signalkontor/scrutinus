package com.signalkontor.messaging;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import android.util.Log;
import com.signalkontor.utils.BatteryUtils;
import com.signalkontor.utils.StringUtils;
import org.json.JSONException;

import com.signalkontor.data.Tournament;
import com.signalkontor.utils.JsonUtils;

public class TPSMessage extends COMMessage {
	private static final String PROPERTY_TIMESTAMP = "FTimeStamp";
	private static final String PROPERTY_SENDER = "FSender";
	private static final String PROPERTY_RECEIVER = "FReceiver";
	private static final String PROPERTY_RECEIVER_IP = "FReceiverIP";
	private static final String PROPERTY_TARGET_ID = "FTargetId";
	private static final String PROPERTY_RESPONSE_ID = "FResponseId";
	private static final String PROPERTY_DO_CMD = "FDoCmd";
	private static final String PROPERTY_MSG_DATA = "FMsgData";
	
	public TPSMessage() {
		setAppId(120);
        try { jsonObject.put(PROPERTY_RECEIVER, "mTPS"); } catch (JSONException ignored) { }
	}
	
	public Date getTimeStamp() {
		try { return JsonUtils.convert(jsonObject.getString(PROPERTY_TIMESTAMP)); } catch (JSONException e) { return null; }
	}
	
	public void setTimeStamp(Date timeStamp) {
		try { jsonObject.put(PROPERTY_TIMESTAMP, JsonUtils.convert(timeStamp)); } catch (JSONException ignored) { }
	}
	
	public String getSender() {
		try { return jsonObject.getString(PROPERTY_SENDER); } catch (JSONException e) { return null; }
	}
	
	public void setSender(String sender) {
		try { jsonObject.put(PROPERTY_SENDER, sender); } catch (JSONException ignored) { }
	}	
	
	public String getReceiver() {
		try { return jsonObject.getString(PROPERTY_RECEIVER); } catch (JSONException e) { return null; }
	}

    public String getReceiverIp() {
		try { return jsonObject.getString(PROPERTY_RECEIVER_IP); } catch (JSONException e) { return null; }
	}
	
	public void setReceiverIp(String receiverIp) {
		try { jsonObject.put(PROPERTY_RECEIVER_IP, receiverIp); } catch (JSONException ignored) { }
	}	
	
	public int getTargetId() {
		try { return jsonObject.getInt(PROPERTY_TARGET_ID); } catch (JSONException e) { return 0; }
	}
	
	public void setTargetId(int targetId) {
		try { jsonObject.put(PROPERTY_TARGET_ID, targetId); } catch (JSONException ignored) { }
	}	
	
	public int getResponseId() {
		try { return jsonObject.getInt(PROPERTY_RESPONSE_ID); } catch (JSONException e) { return 0; }
	}
	
	public void setResponseId(int responseId) {
		try { jsonObject.put(PROPERTY_RESPONSE_ID, responseId); } catch (JSONException ignored) { }
	}
	
	public String getDoCmd() {
		try { return jsonObject.getString(PROPERTY_DO_CMD); } catch (JSONException e) { return null; }
	}
	
	public void setDoCmd(String doCmd) {
		try { jsonObject.put(PROPERTY_DO_CMD, doCmd); } catch (JSONException ignored) { }
	}
	
	public String getMsgData() {
		try { return jsonObject.getString(PROPERTY_MSG_DATA); } catch (JSONException e) { return null; }
	}
	
	public void setMsgData(String msgData) {
		try { jsonObject.put(PROPERTY_MSG_DATA, msgData); } catch (JSONException ignored) { }
	}
	
	private static TPSMessage newMessage(int responseId, int targetId) {
		final TPSMessage msg = new TPSMessage();
		msg.setTimeStamp(new Date());
		msg.setSender(MessageManager.getDeviceId());
		msg.setResponseId(responseId);
		msg.setTargetId(targetId);
		msg.setDoCmd(Tournament.getInstance().getTournamentId());
		return msg;
	}
	
	public static TPSMessage newStatusMessage(final int windowId, final int windowStatus, final int dance, final int heat) {
		final TPSMessage msg = newMessage(0, 1);
		final int battery = BatteryUtils.getBatteryPercentage();
        final String status = BatteryUtils.getBatteryStatus();
		final int markCount = dance != 0 ? Tournament.getInstance().getDances()[dance-1].getMarkCount() : 0;
        //Log.d("TPSMessage", String.format("%d;%d;%d;%d;%d;%d;%s", battery, windowId, windowStatus, dance, heat, markCount, status));
		msg.setMsgData(String.format("%d;%d;%d;%d;%d;%d;%s", battery, windowId, windowStatus, dance, heat, markCount, status));
		return msg;
	}

    public static TPSMessage newJudgeDeviceMessage(final String judge){
        TPSMessage msg = newMessage(0, 35);
        msg.setMsgData(judge);
        return msg;
    }


    private static String toMarkString(int mark) {
        switch(mark) {
            case 1: return "X";
            case 2: return "1";
            case 3: return "2";
            case 4: return "L";
            case 0:
            default: return " ";
        }
    }

    public static TPSMessage newFinalJudgingMessage(String couple, int danceNumber, int position) {
        final TPSMessage msg = newMessage(0, 21);
        msg.setMsgData(String.format("%s;%d;%d", couple, danceNumber, position));
        return msg;
    }

	public static TPSMessage newJudgingMessage(String couple, int danceNumber, int mark) {
		final TPSMessage msg = newMessage(0, 11);
		msg.setMsgData(String.format("%s;%d;%s", couple, danceNumber, toMarkString(mark)));
		return msg;
	}

    public static TPSMessage newFullJudgingMessage(final int responseId, final boolean finals) {
        final TPSMessage msg = newMessage(responseId, finals ? 22 : 12);
        final Tournament tournament = Tournament.getInstance();
        final Tournament.Dance[] dances = tournament.getDances();

        // If I only had map/reduce/filter (select/aggregate/where).
        // Writing loops is so painful!
        HashMap<String, String[]> map = new HashMap<String, String[]>();
        for(int danceIndex = 0; danceIndex < dances.length; danceIndex++) {
            final Tournament.Heat[] heats = dances[danceIndex].getHeats();
            for (Tournament.Heat heat : heats) {
                final String[] couples = heat.getCouples();
                final int[] marks = heat.getMarks();
                for (int coupleIndex = 0; coupleIndex < couples.length; coupleIndex++) {
                    final String coupleId = couples[coupleIndex];
                    String[] marksArray = map.get(coupleId);
                    if (marksArray == null) {
                        marksArray = new String[dances.length];
                        map.put(coupleId, marksArray);
                    }
                    marksArray[danceIndex] = finals ? Integer.toString(marks[coupleIndex]) : toMarkString(marks[coupleIndex]);
                }
            }
        }

        final StringBuilder data = new StringBuilder()
                .append(map.size())
                .append(';');

        for(Map.Entry<String, String[]> item : map.entrySet()) {
            data
                .append(item.getKey())
                .append(";\"")
                .append(StringUtils.joinWithAdditionalDelimiterAtEnd(item.getValue(), ";"))
                .append("\";"); // it is ok that the last entry gets a ';'
        }
        msg.setMsgData(data.toString());
        return msg;
    }

    public static TPSMessage newSigningMessage(CharSequence signature) {
        final TPSMessage msg = newMessage(0, 31);
        final Tournament tournament = Tournament.getInstance();
        final StringBuilder data = new StringBuilder()
                .append('"')
                .append(tournament.getTournamentId())
                .append('"')
                .append(';')
                .append('"')
                .append(tournament.getJudgeId())
                .append('"')
                .append(';')
                .append(signature);
        msg.setMsgData(data.toString());
        return msg;
    }
}
