package com.signalkontor;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import com.signalkontor.data.Tournament;

/**
 * User: Mikko
 * Date: 24.06.11
 * Time: 10:58
 */
public class ResetActivity extends Activity implements View.OnClickListener {
    private static final String PASSWORD = "test";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Tournament.restore(this);
        final Tournament t = Tournament.getInstance();
        setContentView(R.layout.reset);
        findViewById(R.id.resetButton).setOnClickListener(this);
        ((TextView)findViewById(R.id.resetDeviceID)).setText(t.getDeviceId());
        ((TextView)findViewById(R.id.resetCompetition)).setText(t.getFullTitle());
        ((TextView)findViewById(R.id.resetJudge)).setText(t.getJudgeFullName());
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.resetButton:
                final TextView t = (TextView)findViewById(R.id.resetPIN);
                if(t.getText().toString().equals(PASSWORD)) {
                    Tournament.delete(this);
                    this.finish();
                } else {
                    Toast.makeText(this, "Incorrect PIN", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
}