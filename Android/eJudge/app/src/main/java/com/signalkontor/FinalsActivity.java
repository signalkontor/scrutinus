package com.signalkontor;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.*;
import com.signalkontor.constants.Constants;
import com.signalkontor.data.Tournament;
import com.signalkontor.messaging.MessageManager;
import com.signalkontor.messaging.TPSMessage;
import com.signalkontor.utils.BatteryUtils;
import com.signalkontor.widget.FinalsButton;

import java.util.Observable;
import java.util.Observer;

/**
 * User: Mikko
 * Date: 14.06.11
 * Time: 12:43
 */
public class FinalsActivity extends Activity implements OnClickListener, DialogInterface.OnClickListener, Observer {

    /*
     * Two-dimensional array of buttons. Format is: button[couple][position]
      *
      * Where
      *     couple = n: couple in the (n+1)th column
      *     position = n: (n+1)th place
     */
    private FinalsButton[][] buttons;
    private Button continueButton;
    private Tournament.Dance dance;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BatteryUtils.startReceiving(this);

        if (!MessageManager.isInitialized()) {
            AutoConfigureTask autoConfigureTask = new AutoConfigureTask(this);
            autoConfigureTask.disableDialog();
            autoConfigureTask.execute((Object[]) null);
        }

        final TableLayout tableLayout = new TableLayout(this);
        tableLayout.setOrientation(TableLayout.VERTICAL);
        tableLayout.setStretchAllColumns(true);
        tableLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        final Tournament tournament = Tournament.getInstance();
        tournament.addObserver(this);
        dance = tournament.getCurrentDance();
        updateTitle();
        final Tournament.Heat heat = dance.getHeats()[0];
        final String[] couples = heat.getCouples();

        final TableRow headerRow = new TableRow(this);
        headerRow.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        for (String couple : couples) {
            final TextView textView = new TextView(this);
            textView.setText(couple);
            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            textView.setGravity(Gravity.CENTER_HORIZONTAL);
            headerRow.addView(textView);
        }
        tableLayout.addView(headerRow);

        buttons = new FinalsButton[couples.length][couples.length];

        for (int positionIndex = 0; positionIndex < couples.length; positionIndex++) {
            final TableRow tableRow = new TableRow(this);
            for (int coupleIndex = 0; coupleIndex < couples.length; coupleIndex++) {
                final FinalsButton button = new FinalsButton(this);
                button.setText("" + (positionIndex + 1));
                button.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
                button.setRowIndex(positionIndex);
                button.setColumnIndex(coupleIndex);
                button.setOnClickListener(this);
                buttons[coupleIndex][positionIndex] = button;
                tableRow.addView(button);
            }
            tableLayout.addView(tableRow);
        }

        continueButton = new Button(this);
        continueButton.setText(R.string._continue);
        continueButton.setVisibility(View.INVISIBLE);
        continueButton.setOnClickListener(this);

        tableLayout.addView(continueButton);

        final int[] marks = heat.getMarks();
        for (int i = 0; i < marks.length; i++) {
            final int mark = marks[i] - 1;
            if (mark >= 0) {
                final FinalsButton button = buttons[i][mark];
                final boolean crossed = button.toggleCrossed();
                updateButtons(button, crossed);
            }
        }

        postStatusMessage();
        setContentView(tableLayout);
    }

    private void postStatusMessage() {
        MessageManager.setCurrentStatusMessage(TPSMessage.newStatusMessage(2, dance.getMarkCount() == buttons.length ? 1 : 0, dance.getIndex() + 1, 0));
    }

    @Override
    public void update(Observable observable, Object o) {
        if (observable instanceof Tournament) {
            final Tournament tournament = (Tournament) observable;

            if (tournament.isClosing()) {
                switchToActivity(StartActivity.class);
            } else if(o == Tournament.COUPLES_MODIFIED) {
                switchToActivity(FinalsActivity.class);
            }
        }
    }

    private void switchToActivity(Class nextActivityClass) {
        // remove us from the observer list to prevent dangling references
        Tournament.getInstance().deleteObserver(this);
        startActivity(new Intent(this, nextActivityClass).putExtra(Constants.EXTRA_FROM_JUDGING_INDEX, false));
        finish();
    }

    private void updateOtherButtons(boolean crossed, int rowToDisable, int columnToDisable) {
        for (int i = 0; i < buttons.length; i++) {
            final FinalsButton rowButton = buttons[i][rowToDisable];
            rowButton.updateLocks(crossed);

            final FinalsButton colButton = buttons[columnToDisable][i];
            colButton.updateLocks(crossed);
        }

        continueButton.setVisibility(dance.getMarkCount() == buttons.length ? View.VISIBLE : View.INVISIBLE);
    }

    private void updateButtons(FinalsButton button, boolean crossed) {
        final int rowToDisable = button.getRowIndex();
        final int columnToDisable = button.getColumnIndex();
        updateOtherButtons(crossed, rowToDisable, columnToDisable);
    }

    @Override
    public void onClick(View view) {
        if (view == continueButton) {
            handleContinueClick();
        } else if (view instanceof FinalsButton) {
            handleJudgingClick((FinalsButton) view);
        }
    }

    private void handleContinueClick() {
        new AlertDialog.Builder(this)
                .setMessage(dance.isLast() ? R.string.continueToSigning : R.string.advanceToNextDance)
                .setPositiveButton(R.string.yes, this)
                .setNegativeButton(R.string.no, null)
                .show();
    }

    private void handleJudgingClick(FinalsButton button) {
        final boolean crossed = button.toggleCrossed();
        dance.setPosition(button.getColumnIndex(), crossed ? button.getRowIndex() + 1 : 0);
        updateButtons(button, crossed);
        postStatusMessage();
    }

    @Override
    public void onPause() {
        BatteryUtils.stopReceiving(this);
        super.onPause();
    }

    @Override
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); // prevent screen rotation
    }

    public void updateTitle() {
        setTitle(new StringBuffer()
                .append(dance.getDisplayName())
                .append(" (")
                .append(Tournament.getInstance().getDeviceId())
                .append('@')
                .append(MessageManager.getSSID())
                .append(')'));
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        final boolean hasNextDance = Tournament.getInstance().advanceToNextDance();
        if(!hasNextDance)
            MessageManager.offer(TPSMessage.newFullJudgingMessage(0, true));       // send complete marking of final

        switchToActivity(hasNextDance ? FinalsActivity.class : SigningActivity.class);
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
            .setMessage(R.string.quitApplication)
            .setPositiveButton(R.string.no, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    System.exit(0);
                }
            })

            .setNegativeButton(R.string.yes, null)
            .show();
    }

    @Override
    public void onAttachedToWindow()
    {
           // this.getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD);
           super.onAttachedToWindow();
    }
}