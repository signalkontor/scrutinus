package com.signalkontor.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;
import com.signalkontor.R;

/**
* User: Mikko
* Date: 24.06.11
* Time: 17:12
*/
public class SignatureView extends View {
    private Bitmap mBitmap;
    private Canvas mCanvas;
    private final Paint mPaint;

    private float currentX, currentY;
    private final int[] x1Coords = new int[2048];
    private final int[] y1Coords = new int[2048];
    private final int[] x2Coords = new int[2048];
    private final int[] y2Coords = new int[2048];

    private int position = 0;

    public CharSequence getSignature(float scale) {
        StringBuilder stringBuilder = new StringBuilder(16384); // TODO good initial capacity?
        stringBuilder.append(position).append(';');
        for(int i = 0; i < position; i++) {
            stringBuilder
                    .append('"')
                    .append((int)(x1Coords[i] * scale)).append(';')
                    .append((int)(y1Coords[i] * scale)).append(';')
                    .append((int)(x2Coords[i] * scale)).append(';')
                    .append((int)(y2Coords[i] * scale))
                    .append('"').append(';');
        }
        return stringBuilder;
    }

    public SignatureView(Context context) {
        super(context);
        setFocusable(true);
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setARGB(255, 255, 255, 255);
    }

    public SignatureView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setFocusable(true);
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setARGB(255, 255, 255, 255);
    }

    public void clear() {
        if (mCanvas != null) {
            mPaint.setARGB(255, 0, 0, 0);
            mCanvas.drawPaint(mPaint);
            mPaint.setARGB(255, 255, 255, 255);
            invalidate();
            position = 0;
        }
    }

    @Override protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        int curW = mBitmap != null ? mBitmap.getWidth() : 0;
        int curH = mBitmap != null ? mBitmap.getHeight() : 0;
        if (curW >= w && curH >= h) {
            return;
        }

        if (curW < w) curW = w;
        if (curH < h) curH = h;

        Bitmap newBitmap = Bitmap.createBitmap(curW, curH, Bitmap.Config.RGB_565);
        Canvas newCanvas = new Canvas();
        newCanvas.setBitmap(newBitmap);
        if (mBitmap != null) {
            newCanvas.drawBitmap(mBitmap, 0, 0, null);
        }
        mBitmap = newBitmap;
        mCanvas = newCanvas;
    }

    @Override protected void onDraw(Canvas canvas) {
        if (mBitmap != null) {
            canvas.drawBitmap(mBitmap, 0, 0, null);
        }
    }

    @Override public boolean onTouchEvent(MotionEvent event) {
        int action = event.getActionMasked();
        switch(action) {
            case MotionEvent.ACTION_DOWN:
                currentX = event.getX();
                currentY = event.getY();
                return true; // we want more events for this movement
            case MotionEvent.ACTION_MOVE:
                float newX = event.getX();
                float newY = event.getY();
                mCanvas.drawLine(currentX, currentY, newX, newY, mPaint);
                x1Coords[position] = (int) currentX;
                y1Coords[position] = (int) currentY;
                x2Coords[position] = (int) newX;
                y2Coords[position]= (int) newY;
                position++;
                currentX = newX;
                currentY = newY;
                invalidate(); // force redraw
                return true; // we want more events for this movement
            case MotionEvent.ACTION_UP:
                return false; // mo more event's for us (motion has ended)
        }
        return false;
    }
}
