package com.signalkontor;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.TableLayout;
import android.widget.TableRow;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.FileWriter;

import android.os.Environment;
import android.widget.Toast;

import com.signalkontor.constants.Constants;
import com.signalkontor.data.Tournament;
import com.signalkontor.widget.StatefulButton;

public class PageActivity extends Activity {

	private static final float MAX_COLUMNS = 4; 
	private int fontSize = 25;

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        readSetting();

        final TableLayout tableLayout = new TableLayout(this);
        tableLayout.setOrientation(TableLayout.VERTICAL);
        tableLayout.setStretchAllColumns(true);
        tableLayout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

        final Bundle extras = getIntent().getExtras(); 
        final int danceIndex = extras.getInt(Constants.EXTRA_DANCE_INDEX); 
        final int heatIndex = extras.getInt(Constants.EXTRA_HEAT_INDEX);
        final Tournament tournament = Tournament.getInstance();
        final Tournament.Heat heat = tournament.getDances()[danceIndex].getHeats()[heatIndex];        
        final String[] couples = heat.getCouples();
        final int[] marks = heat.getMarks();
        final int helperCrossesCount = tournament.showHelperCross2() ? 2 : (tournament.showHelperCross1() ? 1 : 0);
        
        final int rows = (int)Math.ceil(couples.length / MAX_COLUMNS);
        final DisplayMetrics m = getResources().getDisplayMetrics();
        final int height = (int) Math.min((m.heightPixels - getResources().getDimension(R.dimen.unusableHeight)) / rows, (m.heightPixels / 5));
        final int width = (int) (m.widthPixels / MAX_COLUMNS);
        
    	TableRow tr = null;
    	for(int i = 0; i < couples.length; i++) {
    		if(i % MAX_COLUMNS == 0) {
    			tr = new TableRow(this);
    			tr.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
            	tableLayout.addView(tr);
    		}
    		StatefulButton b = new StatefulButton(this);
    		b.setHeight(height);
    		b.setWidth(width);
    		b.setCouple(couples[i]);
    		b.setInitialState(marks[i]);
    		b.setId(i);
    		b.setDanceIndex(danceIndex);
    		b.setHeatIndex(heatIndex);
    		b.setHelperCrosses(helperCrossesCount);
    		b.setTextSize(TypedValue.COMPLEX_UNIT_SP, fontSize);
            //noinspection ConstantConditions
            tr.addView(b);
    	}
    	setContentView(tableLayout);
	}

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
            .setMessage(R.string.quitApplication)
            .setPositiveButton(R.string.no, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    System.exit(0);
                }
            })
            .setNegativeButton(R.string.yes, null)
            .show();
    }

    @Override
    public void onAttachedToWindow()
    {
           // this.getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD);
           super.onAttachedToWindow();
    }

    private void readSetting()
    {
        try {
            File storage = Environment.getExternalStorageDirectory();
            File file = new File(storage,"/eJudgeConfig.txt");
            if(!file.canRead()){
                // if it is not existing, we try to create it
                //if(file.canWrite())
                //{
                    FileWriter fr = new FileWriter(file);
                    fr.write("buttontextsize=25\r\n");
                    fr.close();
                //}
                return;
            }

            StringBuilder text = new StringBuilder();

            BufferedReader br = new BufferedReader(new FileReader(file));
            String line = "";
            while((line = br.readLine()) != null) {
                String[] data = line.split("=");
                if(data.length == 2)
                {
                    if(data[0].equals("buttontextsize"))
                    {
                        this.fontSize = Integer.parseInt(data[1]);
                    }
                }
            }
        }catch(Exception ex)
        {
            // not much to do ...
            Toast.makeText(this, ex.getMessage(), 2000);
        }
    }
}
