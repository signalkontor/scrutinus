﻿using System;
using System.Collections.Generic;
using com.signalkontor.Common;

namespace com.signalkontor.XD_Server
{
    class ServerCore
    {
        private readonly IListenerThread _listenerThread;
        private readonly MSQueueThread _queueThread;
        private readonly AutoConfigThread _autoConfigThread;
        private readonly PingThread _pingThread;
        private readonly FileHandler _fileUpdateHandler;

        private readonly Dictionary<string, Queue<TPSMessage>> _messages;

        public static ServerCore Instance { get; set; }

        public ServerCore()
        {
            // Wir holen unseren ListenerThread
            Instance = this;
            _messages = new Dictionary<string, Queue<TPSMessage>>();
            _listenerThread = new ListenerThreadEx(XDServer.Default.Port);
            System.Threading.Tasks.Task.Factory.StartNew(() => _listenerThread.Start());
            _autoConfigThread = new AutoConfigThread();
            _queueThread = new MSQueueThread();
            _queueThread.Run();
            _pingThread = new PingThread();
            _fileUpdateHandler = new FileHandler(XDServer.Default.AutoConfigFilePort);
        }

        public void SendMessageToMobile(TPSMessage msg)
        {
            if (_listenerThread.SendMessage(msg, msg.FReceiver) != 0)
            {
                // wir müssen diese Nachricht für den User zwischenspeichern
                // ToDo: Zwischenspeichern
                lock (_messages)
                {
                    Queue<TPSMessage> queue;
                    if (!_messages.ContainsKey(msg.FReceiver))
                    {
                        queue = new Queue<TPSMessage>();
                        _messages.Add(msg.FReceiver, queue);
                    }
                    else
                    {
                        queue = _messages[msg.FReceiver];
                    }

                    queue.Enqueue(msg);
                }
            }
        }

        public int SendPingToMobile(string user)
        {
            var ret = _listenerThread.SendPing(user);
            if (ret == 0)
            {
                // Wir prüfen nun, ob wir daten für diesen User haben
                lock (_messages)
                {
                    if (_messages.ContainsKey(user))
                    {
                        var queue = _messages[user];
                        while (queue.Count > 0)
                        {
                            TPSMessage msg = queue.Peek();
                            if (_listenerThread.SendMessage(msg, user) == 0)
                            {
                                queue.Dequeue();
                            }
                            else
                            {
                                break;
                            }
                        }
                        if (queue.Count == 0)
                        {
                            _messages.Remove(user);
                        }
                    }
                }
            }
            return ret;
        }

        public void SendLogonMessage(string user)
        {
            var tmqeJsMsg = new TMQeJSMsg
                                {
                                    TargetId = 2,
                                    DoCmd = "",
                                    MsgData = user,
                                    TimeStamp = DateTime.Now,
                                    Sender = user
                                };
            SendMessageToQueue(tmqeJsMsg);
        }

        public void SendLogoffMessage(string user)
        {
            var tmqeJsMsg = new TMQeJSMsg
                                {
                                    TargetId = 3,
                                    DoCmd = "",
                                    MsgData = user,
                                    TimeStamp = DateTime.Now,
                                    Sender = user
                                };
            SendMessageToQueue(tmqeJsMsg);
        }

        public void SendMessageToQueue(TMQeJSMsg msg)
        {
            _queueThread.SendMessage(msg);
        }
    }
}
