﻿using System.Text;

namespace com.signalkontor.XD_Server
{
    class FileHandler : GenericListenerThread
    {

        public FileHandler(int port)
            : base(port)
        {

        }

        /// <summary>
        /// Nimmt die Anfrage eines Clients entgegen
        /// </summary>
        /// <param name="socket">Der geöffnete Socket zur Kommunikation mit dem Client</param>
        protected override void HandleIncommingConnection(System.Net.Sockets.Socket socket)
        {
            // Hier können wir nun die Daten verarbeiten ...
            // Wir warten zunächst auf ein Byte, das uns sagt, welche Datei gewünscht wird
            var counter = 0;
            while (socket.Available < 1)
            {
                System.Threading.Thread.Sleep(500);
                counter++;
                if (counter > 20)
                {
                    socket.Close();
                    return;
                }
            }
            // Wir erhalten zunächst den gewünschen Dateinamen als String mit der fixen Länge 255 Bytes
            var buffer = new byte[1024];
            socket.Receive(buffer, 0, 255, System.Net.Sockets.SocketFlags.None);

            // Wir wandeln die Daten in String um
            var file = Encoding.Unicode.GetString(buffer, 0, 255);

            // und senden Die Datei zum client
            SendFile(socket, file);
        }
        /// <summary>
        /// Liefert das 3. größte Byte eines Int zurück (Chifft um 16 Bits)
        /// </summary>
        /// <param name="value">Ausgangswert</param>
        /// <returns></returns>
        private static byte GetHighByte2(int value)
        {
            return (byte)((value & (16711680)) >> 16);
        }

        private static byte GetHighByte(int value)
        {
            return (byte)((value & (65280)) >> 8);
        }

        private static byte GetLowByte(int value)
        {
            return (byte)(value & 255);
        }
        /// <summary>
        /// Sendet eine Datei mit dem übergebenen Dateinamen an einen Socket. Anschließend wird der MD5 Hash der Datei 
        /// übergeben, so dass der Client prüfen kann, ob die Datei korrekt übertragen wurde
        /// </summary>
        /// <param name="socket">Der zu verwendende Socket</param>
        /// <param name="filename">Dateinamen der zu sendenden Datei. Die Datei muss sich im Ordner
        /// 'AutoUpdatePaht" befinden, der in der Konfiguration angegeben ist</param>
        private static void SendFile(System.Net.Sockets.Socket socket, string filename)
        {

            var index = filename.IndexOf('\0');
            filename = filename.Substring(0, index);
            var path = string.Format("{0}\\{1}", XDServer.Default.AutoUpdatePath, filename);

            var buffer = new byte[2048];
            var output = new byte[2048];

            var reader = new System.IO.FileStream(path, System.IO.FileMode.Open);
            var binaryReader = new System.IO.BinaryReader(reader);
            var size = 1;

            var md5 = System.Security.Cryptography.MD5.Create();

            var info = new System.IO.FileInfo(path);
            var fileSize = (int)info.Length;

            buffer[0] = GetLowByte(fileSize);
            buffer[1] = GetHighByte(fileSize);
            buffer[2] = GetHighByte2(fileSize);

            // Wir senden die Gesamtlänge der daten, die folgen werden.
            socket.Send(buffer, 0, 3, System.Net.Sockets.SocketFlags.None);

            while (size > 0)
            {
                size = binaryReader.Read(buffer, 0, buffer.Length);
                socket.Send(buffer, 0, size, System.Net.Sockets.SocketFlags.None);
                if (size > 0)
                    md5.TransformBlock(buffer, 0, size, output, 0);
                else
                    md5.TransformFinalBlock(buffer, 0, 0);
            }

            // Wir berechnen noch den Hashwert der Datei
            socket.Send(md5.Hash, 0, md5.HashSize / 8, System.Net.Sockets.SocketFlags.None);
            // Fertig ...
            binaryReader.Close();
        }
    }
}
