﻿
using System;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.signalkontor.Common;

namespace com.signalkontor.XD_Server
{

    public interface IListenerThread
    {
        int SendMessage(COMMessage msg, string user);
        int SendPing(string user);
        void Start();
    }


    public class MessageEventArgs : EventArgs
    {
        public MessageEventArgs(int appId, byte[] data)
        {
            switch (appId)
            {
                case 1:
                    Message = COMMessage.FromJson<LogonMessage>(data);
                    break;
                case 2:
                    Message = COMMessage.FromJson<StringMessage>(data);
                    break;
                case 120:
                    Message = COMMessage.FromJson<TPSMessage>(data);
                    break;
            }
        }

        public MessageEventArgs(COMMessage message)
        {
            Message = message;
        }

        public string User { get; set; }
        public COMMessage Message { get; set; }
    }


    public delegate void MessageReceivedDelegate(object sender, MessageEventArgs e);

    class ListenerThreadEx : IListenerThread
    {
        private static readonly Encoding Encoding = Encoding.UTF8;

        private readonly int _port;
        private readonly TcpListener _tcpListener;

        public ListenerThreadEx(int port)
        {
            _port = port;

            /*var ipAddress = NetworkInterface.GetAllNetworkInterfaces()
                .SelectMany(ni => ni.GetIPProperties().UnicastAddresses)
                .Where(ua => ua.Address.AddressFamily == AddressFamily.InterNetwork && !IPAddress.IsLoopback(ua.Address))
                .Select(ua => ua.Address)
            .First();*/

            _tcpListener = new TcpListener(IPAddress.Any, port);

        }

        public void Start()
        {
            _tcpListener.Start();

            while (true)
            {    
                var tcpClient = _tcpListener.AcceptTcpClient();
                new Task(obj => ReceiveMessageMulti(obj as TcpClient), tcpClient).Start();
            }
        }

        public void Stop()
        {
            lock (_tcpListener)
            {
                _tcpListener.Stop();
            }
        }

        private static void ReceiveMessageMulti(TcpClient tcpClient)
        {
            int count = 0;
            while (ReceiveMessage(tcpClient))
            {
                count++;
                Console.WriteLine(count);
            }
            // Fertig, Schließen
            tcpClient.Close();
        }

        private static bool ReceiveMessage(TcpClient tcpClient)
        {
            try
            {
                //using (var stream = tcpClient.GetStream())
                //{
                    var stream = tcpClient.GetStream();
                    stream.ReadTimeout = 10000;

                    var pos = 0;
                    var lenBuffer = new byte[4];
                    var gelesen = stream.Read(lenBuffer, 0, 4);
                    if (gelesen == 0)
                    {
                        return false;
                    }
                    
                    int dataSize = BitConverter.ToInt32(lenBuffer, 0);
                    var buffer = new byte[dataSize];
    
                    if (dataSize == 1)
                    {
                        // Dies ist ein Ping und wir senden nun eine Bestätigung zurück
                        stream.Write(BitConverter.GetBytes(true), 0, 1);
                        Console.WriteLine("Ping von " + tcpClient.Client.RemoteEndPoint);
                        stream.Close();                    
                        return false;
                    }

                    while (pos < buffer.Length)
                    {
                        var bytesRead = stream.Read(buffer, pos, buffer.Length - pos);
                        if (bytesRead == -1) break;
                        pos += bytesRead;
                    }

                    var genericMessage = COMMessage.FromJson<COMMessage>(buffer);
                    var tmqeJsMsg = new TMQeJSMsg();
                    switch (genericMessage.AppID)
                    {
                        case 1: // logon message
                            var logonMessage = COMMessage.FromJson<LogonMessage>(buffer);
                            IPEndPoint end = (IPEndPoint)tcpClient.Client.RemoteEndPoint;
                            UserManager.Logon(logonMessage.User, end.Address);
                            break;
                        case 2: // string message
                            var stringMessage = COMMessage.FromJson<StringMessage>(buffer);
                            tmqeJsMsg.TargetId = 3; // User Logoff
                            tmqeJsMsg.DoCmd = "";
                            tmqeJsMsg.MsgData = stringMessage.Message;
                            tmqeJsMsg.TimeStamp = DateTime.Now;
                            tmqeJsMsg.Sender = stringMessage.Message;
                            break;

                        case 120: // tps message
                            var tpsMessage = COMMessage.FromJson<TPSMessage>(buffer);
                            tmqeJsMsg.DoCmd = tpsMessage.FDoCmd;
                            tmqeJsMsg.MsgData = tpsMessage.FMsgData;
                            tmqeJsMsg.ResponseId = tpsMessage.FResponseId;
                            tmqeJsMsg.Sender = tpsMessage.FSender;
                            tmqeJsMsg.TargetId = tpsMessage.FTargetId;
                            tmqeJsMsg.TimeStamp = tpsMessage.FTimeStamp;
                            tmqeJsMsg.ReceiverIP = tpsMessage.FReceiverIP;
                            tmqeJsMsg.Receiver = tpsMessage.FReceiver;
                            Console.WriteLine("Sequence No.:" + tpsMessage.FResponseId);
                            IPEndPoint endIp = (IPEndPoint)tcpClient.Client.RemoteEndPoint;
                            UserManager.Logon(tmqeJsMsg.Sender, endIp.Address);
                            break;

                        default:
                            tmqeJsMsg.DoCmd = "";
                            tmqeJsMsg.MsgData = "unknown Message: " + genericMessage.AppID;
                            tmqeJsMsg.Receiver = "";
                            tmqeJsMsg.ReceiverIP = "";
                            tmqeJsMsg.ResponseId = 0;
                            tmqeJsMsg.Sender = null; // will be set later
                            tmqeJsMsg.TargetId = 500;
                            tmqeJsMsg.TimeStamp = DateTime.Now;
                            break;
                    }

                    if (tmqeJsMsg.Sender == null)
                    {
                        var endpoint = tcpClient.Client.RemoteEndPoint;
                        string user = null;
                        if (endpoint is IPEndPoint)
                        {
                            user = UserManager.GetUser((endpoint as IPEndPoint).Address);
                        }
                        tmqeJsMsg.Sender = user ?? "";
                    }

                    ServerCore.Instance.SendMessageToQueue(tmqeJsMsg);

                    // wir senden eine Quittung zurück
                    stream.Write(BitConverter.GetBytes(true), 0, 1);
                    return true;
               // }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public int SendPing(string user)
        {
            var tcpClient = new TcpClient();
            var address = UserManager.GetAddress(user);
            try
            {
                tcpClient.Connect(new IPEndPoint(address, _port));
                using (var stream = tcpClient.GetStream())
                {
                    
                    // wir senden nun die Länge unserer Daten
                    byte[] len = BitConverter.GetBytes(1);
                    stream.Write(len, 0, len.Length);
                    stream.WriteByte(1);
                    stream.Flush();

                    // -1 == no acknowlege byte sent => user is offline
                    if (stream.ReadByte() == -1)
                        return 1;
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                // something went wrong => assume message did not reach the recipient
                return 1;
            }
            finally
            {
                tcpClient.Close();
            }
            // no errors, got acknowledge byte => all ok
            return 0;
        }

        public int SendMessage(COMMessage msg, string user)
        {
            var tcpClient = new TcpClient();
            var address = UserManager.GetAddress(user);
            if (address == null)
                return 10;

            try
            {
                tcpClient.Connect(new IPEndPoint(address, _port));
                using (var stream = tcpClient.GetStream())
                {
                    var bytes = Encoding.GetBytes(msg.ToJson());
                    // wir senden nun die Länge unserer Daten
                    byte[] len = BitConverter.GetBytes(bytes.Length);
                    stream.Write(len, 0, len.Length);
                    stream.Write(bytes, 0, bytes.Length);
                    stream.Flush();

                    // -1 == no acknowlege byte sent => user is offline
                    if (stream.ReadByte() == -1)
                        return 1;
                }
            }
            catch
            {
                // something went wrong => assume message did not reach the recipient
                return 1;
            }
            finally
            {
                tcpClient.Close();
            }
            // no errors, got acknowledge byte => all ok
            return 0;
        }
    }
}
