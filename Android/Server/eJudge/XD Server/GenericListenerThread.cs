﻿using System;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using com.signalkontor.Common;

namespace com.signalkontor.XD_Server
{
    public abstract class GenericListenerThread : IListenerThread
    {
        readonly Thread _thread;
        readonly int _localPort;

        protected GenericListenerThread(int localPort)
        {
            _localPort = localPort;
            _thread = new Thread(Run);
            _thread.Start();
        }

        private void HandleConnection(Socket socket)
        {
            var thread = new Thread(ConnectionThread);
            thread.Start(socket);
        }

        private void ConnectionThread(object socket)
        {
            var s = (Socket)socket;
            HandleIncommingConnection(s);
        }

        private void Run()
        {
            // Wir öffnen den Port und los geht's
            Console.WriteLine("Listener Thread started, listening at port " + _localPort);

            try
            {
                while (true)
                {
                    var listenSocket = new Socket(AddressFamily.InterNetwork,
                                     SocketType.Stream,
                                     ProtocolType.Tcp);

                    // bind the listening socket to the port
                    var hostIP = IPAddress.Parse("0.0.0.0");
                    // hostIP = (Dns.Resolve(IPAddress.Any.ToString())).AddressList[0];
                    var ep = new IPEndPoint(hostIP, _localPort);
                    listenSocket.Bind(ep);

                    // start listening
                    while (true)
                    {
                        try
                        {
                            listenSocket.Listen(1024);
                            var mySocket = listenSocket.Accept();
                            mySocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive, false);
                            Console.WriteLine("[{0}] Connection from {1}", _localPort, mySocket.RemoteEndPoint);
                            HandleConnection(mySocket);
                        }
                        catch (Exception ex2)
                        {
                            Console.WriteLine(ex2);
                        }

                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        protected abstract void HandleIncommingConnection(Socket socket);

        #region IListenerThread Members

        public int SendMessage(COMMessage msg, string user)
        {
            throw new NotImplementedException();
        }

        public int SendPing(string user)
        {
            throw new NotImplementedException();
        }

        public void Start()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
