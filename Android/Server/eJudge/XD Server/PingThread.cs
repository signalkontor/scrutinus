﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace com.signalkontor.XD_Server
{
    class PingThread
    {
        public PingThread()
        {
            new Thread(Run) {IsBackground = false}.Start();
        }

        private static void Run()
        {
            while (true)
            {
                Thread.Sleep(10000);
                // wir senden an alle User ein Ping und schlafen dann wieder ein
                var users = new List<string>(UserManager.GetAllUser());
                foreach (var user in users)
                {
                    if (ServerCore.Instance.SendPingToMobile(user) == 0)
                        continue;

                    var tmqeJsMsg = new TMQeJSMsg
                                        {
                                            TargetId = 3,
                                            DoCmd = "",
                                            MsgData = user,
                                            TimeStamp = DateTime.Now,
                                            Sender = user
                                        };
                    UserManager.Logoff(user);
                    ServerCore.Instance.SendMessageToQueue(tmqeJsMsg);
                }
            }
        }
    }
}
