﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Net;
using System.Net.Sockets;

namespace com.signalkontor.XD_Server
{
    class AutoConfigThread
    {
        int _currentDevice;
        readonly int _localPort;
        readonly string _autoconfigfile;
        readonly int _firstDevice = 130;
        readonly List<String> _accessPoints = new List<string>();
        readonly Thread _thread;
        readonly Dictionary<string, string> _deviceList;

        public AutoConfigThread()
        {
            _localPort = XDServer.Default.AutoConfigPort;
            _autoconfigfile = XDServer.Default.AutoConfigFile;
            _firstDevice = XDServer.Default.AutoConfigStart;
            var ap = XDServer.Default.AutoConfigAccessPoint.Split(',');
            foreach (var t in ap)
            {
                _accessPoints.Add(t.Trim().ToLower());
            }

            _deviceList = new Dictionary<string, string>();
            _currentDevice = _firstDevice;
            _thread = new Thread(Run) { IsBackground = true };
            LoadDeviceList();
            _thread.Start();
        }

        private void LoadDeviceList()
        {
            if (!System.IO.File.Exists(XDServer.Default.AutoUpdatePath + "\\" + _autoconfigfile))
            {
                Console.WriteLine("No devices.txt found for Auto Config thread");
                return;
            }
            var stream = new System.IO.StreamReader(XDServer.Default.AutoUpdatePath + "\\" + _autoconfigfile);
            while (!stream.EndOfStream)
            {
                var line = stream.ReadLine();
                var data = line.Split(new[] { '=' });
                if (data.Length == 2)
                {
                    _deviceList.Add(data[0], data[1]);

                    // current Device hochzählen ...
                    int intId;
                    var id = data[1].StartsWith("eJS") ? data[1].Substring(3) : data[1];

                    if (Int32.TryParse(id, out intId))
                    {
                        if (_currentDevice <= intId)
                            _currentDevice = intId + 1;
                    }
                }
            }
            stream.Close();
        }

        private void SaveDeviceList()
        {
            lock (_deviceList)
            {
                var writer = new System.IO.StreamWriter(XDServer.Default.AutoUpdatePath + "\\" + _autoconfigfile);
                foreach (var key in _deviceList.Keys)
                {
                    writer.WriteLine(key + "=" + _deviceList[key]);
                }
                writer.Close();
            }
        }

        private string FindNextId()
        {
            var id = XDServer.Default.AutoConfigStart;
            while (true)
            {
                var devId = "eJS" + String.Format("{0}", id);
                if (_deviceList.ContainsValue(devId))
                    id++;
                else
                    return devId;
            }
        }

        private void Run()
        {
            // UDP Socket aufmachen und auf Verbindungen warten ...
            var udpclient = new UdpClient(_localPort, AddressFamily.InterNetwork) {EnableBroadcast = true};
            var remoteIPEndPoint = new IPEndPoint(0, 0);

            while (true)
            {
                try
                {
                    var buffer = udpclient.Receive(ref remoteIPEndPoint);
                    // Wir brauchen nun einen String
                    var data = Encoding.Unicode.GetString(buffer, 0, buffer.Length);
                    Console.WriteLine("Packet von: {0}: {1}", remoteIPEndPoint, data);
                    char[] sep = { ';' };
                    var s = data.Split(sep);
                    // Damit es keine Vertipper gibt vergleichen wir Caseinsensitiv
                    if (_accessPoints.Contains(s[0].ToLower()) || _accessPoints.Contains("*"))
                    {
                        if (_accessPoints.Contains("*"))
                        {
                            Console.WriteLine("* => Accept any Access Points");
                        }

                        lock (_deviceList)
                        {
                            string deviceName;
                            // OK, wir sind zuständig, also schauen, ob Device bekannt.
                            if (_deviceList.ContainsKey(s[1]))
                            {   // ja, bekannt ...
                                deviceName = _deviceList[s[1]];
                                SendConfig(remoteIPEndPoint, deviceName);
                            }
                            else
                            {   // nein, also neues device anlegen
                                deviceName = FindNextId();
                                _deviceList.Add(s[1], deviceName);
                                SendConfig(remoteIPEndPoint, deviceName);
                                SaveDeviceList();
                            }
                            UserManager.Logon(deviceName, remoteIPEndPoint.Address);
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("Error sending config" + e.Message);
                }
            }
        }

        private static void SendConfig(IPEndPoint remoteIPEndPoint, string device)
        {
            var client = new UdpClient();
            var enc = new UnicodeEncoding();
            var b = enc.GetBytes(device);

            remoteIPEndPoint.Port = 9095;
            client.Send(b, b.Length, remoteIPEndPoint);
            // client.Send(b, b.Length, remoteIPEndPoint);

            Console.WriteLine("Paket gesendet an: " + remoteIPEndPoint + ": " + device);
        }

    }
}
