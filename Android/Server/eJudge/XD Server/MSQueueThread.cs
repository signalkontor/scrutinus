﻿using System;
using System.Messaging;
using com.signalkontor.Common;
using System.Threading.Tasks;

namespace com.signalkontor.XD_Server
{
    public class MSQueueThread
    {
        private MessageQueue _queue;
        private MessageQueue _outQueue;

        public void Run()
        {

            // string queuePath = @"FORMATNAME:DIRECT=OS:skwww2\private$\TPS.eJudge.out";
            try
            {
                var queuePath = XDServer.Default.OutQueue;

                _queue = new MessageQueue(queuePath);
                _queue.ReceiveCompleted += QueueReceiveCompleted;
                _queue.Formatter = new XmlMessageFormatter(new[] { typeof(TMQeJSMsg) });
                _queue.BeginReceive();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }

        private static void ProcessMessage(TMQeJSMsg tps)
        {
            if (tps == null)
                return;

            if (tps.Sender == null)
                tps.Sender = "";
            if (tps.DoCmd == null)
                tps.DoCmd = "";
            if (tps.MsgData == null)
                tps.MsgData = "";
            if (tps.Receiver == null)
                tps.Receiver = "";

            var t = new TPSMessage
            {
                FDoCmd = tps.DoCmd,
                FMsgData = tps.MsgData,
                FResponseId = tps.ResponseId,
                FSender = tps.Sender,
                FTargetId = tps.TargetId,
                FTimeStamp = tps.TimeStamp,
                FReceiverIP = tps.ReceiverIP,
                FReceiver = tps.Receiver
            };

            if (t.FDoCmd == null)
                t.FDoCmd = "";
            if (t.FMsgData == null)
                t.FMsgData = "";
            if (t.FSender == null)
                t.FSender = "";

            // Die Nachricht wird nun asynchron losgesendet

            Task.Factory.StartNew(() => ServerCore.Instance.SendMessageToMobile(t));
        }

        void QueueReceiveCompleted(object sender, ReceiveCompletedEventArgs e)
        {

            try
            {
                var msg = _queue.EndReceive(e.AsyncResult);

                if (msg.Body is TMQeJSMsg)
                {
                    var tps = (TMQeJSMsg)msg.Body;

                    ProcessMessage(tps);

                    while (tps != null)
                    {
                        msg = _queue.Receive(new TimeSpan(0, 0, 0, 5));
                        if (msg == null)
                        {
                            break;
                        }

                        tps = msg.Body as TMQeJSMsg;

                        ProcessMessage(tps);

                    }

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            _queue.BeginReceive();
        }

        public void SendMessage(TMQeJSMsg msg)
        {
            if (_outQueue == null)
            {
                _outQueue = new MessageQueue(XDServer.Default.InQueue);
                var formatter = new XmlMessageFormatter(new[] { typeof(TMQeJSMsg) });
                _queue.Formatter = formatter;
            }

            try
            {
                _outQueue.Send(msg);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

    }
}
