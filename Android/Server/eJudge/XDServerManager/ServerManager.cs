using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Messaging;
using com.signalkontor.XDPushServer.MessageHandler;
using com.signalkontor.Common;
using System.Collections.Specialized;
using System.Configuration;

namespace XDServerManager
{
    public partial class ServerManager : Form
    {
        MessageQueue _queue;

        public delegate void AddMessageDelegate(string text);
        public delegate void UserListDelegate(int mode, string user);
        public delegate void ShowQueueSizes();

        public ServerManager()
        {
            InitializeComponent();
            _queue = new MessageQueue(@".\private$\xdserver.service.out");
            _queue.Formatter = new XmlMessageFormatter(new Type[] { typeof(UserMessage), typeof(StringMessage) });
            _queue.ReceiveCompleted += new ReceiveCompletedEventHandler(_queue_ReceiveCompleted);
            _queue.BeginReceive();

            lblQueueName1.Text = "";
            lblQueueName2.Text = "";
            lblQueueName3.Text = "";
            lblQueueSize1.Text = "";
            lblQueueSize2.Text = "";
            lblQueueSize3.Text = "";

            timer1.Enabled = true;
        }

        private string getNamefromPath(string path)
        {
            int lastPos = 0;

            while (path.IndexOf('\\', lastPos) > 0)
            {
                lastPos = path.IndexOf('\\', lastPos) + 1;
            }

            return path.Substring(lastPos);
        }

        private void showQueueSizesImpl()
        {

            NameValueCollection appStgs = System.Configuration.ConfigurationManager.AppSettings;
            
            if (appStgs["queue1"] != null)
            {
                string value = appStgs["queue1"];
                MessageQueue q = new MessageQueue(value);
                lblQueueName1.Text = getNamefromPath(value);
                lblQueueSize1.Text = "" + q.GetAllMessages().Length;
            }
            if (appStgs["queue2"] != null)
            {
                string value = appStgs["queue2"];
                MessageQueue q = new MessageQueue(value);
                lblQueueName2.Text = getNamefromPath(value);
                lblQueueSize2.Text = "" + q.GetAllMessages().Length;
            }

            if (appStgs["queue3"] != null)
            {
                string value = appStgs["queue3"];
                MessageQueue q = new MessageQueue(value);
                lblQueueName3.Text = getNamefromPath(value);
                lblQueueSize3.Text = "" + q.GetAllMessages().Length;
            }
            
        }

        void AddMessageDelegateImpl(string text)
        {
            int oldLength = logOutput.Text.Length;

            logOutput.Text += text + "\r\n";

            if (text.Contains("ERROR"))
            {
                logOutput.Select(oldLength, text.Length + 2);

                logOutput.SelectionColor = Color.Red;
            }

            if (logOutput.Lines.Length > 100)
            {
                logOutput.Text = "";
                
            }

        }



        void UserListDelegateImpl(int mode, string user)
        {
            listUserOnline.BeginUpdate();

            if (mode == 0)
            {
                this.listUserOnline.Items.Remove(user);
            }
            else
            {
                this.listUserOnline.Items.Remove(user);
                this.listUserOnline.Items.Add(user);
            }
            
            listUserOnline.EndUpdate();
            listUserOnline.Refresh();
        }


        void _queue_ReceiveCompleted(object sender, ReceiveCompletedEventArgs e)
        {
            try
            {
 
                System.Messaging.Message message = _queue.EndReceive(e.AsyncResult);
                System.Diagnostics.Debug.WriteLine("Label: " + message.Label);
                System.Diagnostics.Debug.WriteLine("Type: " + message.Body.GetType().ToString());


                if (message.Body is UserMessage)
                {
                    UserMessage msg = (UserMessage)message.Body;
                    COMMessage cm = new COMMessage(msg.Buffer);

                    switch (cm.AppID)
                    {
                        case 1: // Logon
                            // User hat sich angemeldet... Der Liste hinzufügen
                            LogonMessage lm = new LogonMessage(msg.Buffer);
                            UserListDelegate dl1 = new UserListDelegate(UserListDelegateImpl);
                            listUserOnline.Invoke(dl1, 1, lm.User);

                            break;
                        case 2:
                            StringMessage sm = new StringMessage(msg.Buffer);

                            UserListDelegate dl2 = new UserListDelegate(UserListDelegateImpl);
                            listUserOnline.Invoke(dl2, 0, sm.Message);

                            break;
                        case 120:
                            TPSMessage tps = new TPSMessage(msg.Buffer);
                            AddMessageDelegate dl = new AddMessageDelegate(AddMessageDelegateImpl);
                            this.Invoke(dl, tps.FMsgData);
                            break;
                    }
                }

                if (message.Body is StringMessage)
                {
                    StringMessage sm = (StringMessage)message.Body;
                    AddMessageDelegate dl = new AddMessageDelegate(AddMessageDelegateImpl);
                    this.Invoke(dl, sm.Message);
                }
 
            }
            catch (Exception)
            {
                // System.Diagnostics.Debug.WriteLine(ex.Message);
            }
            _queue.BeginReceive();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void beginReceiveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _queue.BeginReceive();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            ShowQueueSizes dl = new ShowQueueSizes(showQueueSizesImpl);
            this.Invoke(dl);

        }
    }
}