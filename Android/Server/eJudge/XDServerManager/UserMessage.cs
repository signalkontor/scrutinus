using System;
using System.Collections.Generic;
using System.Text;

namespace com.signalkontor.XDPushServer.MessageHandler
{
    public class UserMessage
    {
        private string _user = "";
        private byte[] _buffer = null;

        public string User
        {
            get { return _user; }
            set { _user = value; }
        }

        public byte[] Buffer
        {
            set { _buffer = value; }
            get { return _buffer; }
        }
    }
}