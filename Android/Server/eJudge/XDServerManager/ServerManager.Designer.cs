﻿namespace XDServerManager
{
    partial class ServerManager
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menu = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.xDServerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stopToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.beginReceiveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.listUserOnline = new System.Windows.Forms.ListBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.logOutput = new System.Windows.Forms.RichTextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lblQueueSize3 = new System.Windows.Forms.Label();
            this.lblQueueName3 = new System.Windows.Forms.Label();
            this.lblQueueSize2 = new System.Windows.Forms.Label();
            this.lblQueueName2 = new System.Windows.Forms.Label();
            this.lblQueueSize1 = new System.Windows.Forms.Label();
            this.lblQueueName1 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.menu.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // menu
            // 
            this.menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.xDServerToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menu.Location = new System.Drawing.Point(0, 0);
            this.menu.Name = "menu";
            this.menu.Size = new System.Drawing.Size(740, 24);
            this.menu.TabIndex = 0;
            this.menu.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(99, 22);
            this.exitToolStripMenuItem.Text = "&Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // xDServerToolStripMenuItem
            // 
            this.xDServerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stopToolStripMenuItem,
            this.beginReceiveToolStripMenuItem});
            this.xDServerToolStripMenuItem.Name = "xDServerToolStripMenuItem";
            this.xDServerToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.xDServerToolStripMenuItem.Text = "XD Server";
            // 
            // stopToolStripMenuItem
            // 
            this.stopToolStripMenuItem.Name = "stopToolStripMenuItem";
            this.stopToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.stopToolStripMenuItem.Text = "Stop";
            // 
            // beginReceiveToolStripMenuItem
            // 
            this.beginReceiveToolStripMenuItem.Name = "beginReceiveToolStripMenuItem";
            this.beginReceiveToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.beginReceiveToolStripMenuItem.Text = "Begin Receive";
            this.beginReceiveToolStripMenuItem.Click += new System.EventHandler(this.beginReceiveToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.aboutToolStripMenuItem.Text = "&About";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.listUserOnline);
            this.groupBox1.Location = new System.Drawing.Point(17, 42);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(248, 451);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "User\'s Online";
            // 
            // listUserOnline
            // 
            this.listUserOnline.FormattingEnabled = true;
            this.listUserOnline.Location = new System.Drawing.Point(13, 19);
            this.listUserOnline.Name = "listUserOnline";
            this.listUserOnline.Size = new System.Drawing.Size(219, 420);
            this.listUserOnline.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.logOutput);
            this.groupBox2.Location = new System.Drawing.Point(288, 42);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(436, 450);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Server Messages";
            // 
            // logOutput
            // 
            this.logOutput.Location = new System.Drawing.Point(13, 20);
            this.logOutput.Name = "logOutput";
            this.logOutput.Size = new System.Drawing.Size(413, 418);
            this.logOutput.TabIndex = 0;
            this.logOutput.Text = "";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lblQueueSize3);
            this.groupBox3.Controls.Add(this.lblQueueName3);
            this.groupBox3.Controls.Add(this.lblQueueSize2);
            this.groupBox3.Controls.Add(this.lblQueueName2);
            this.groupBox3.Controls.Add(this.lblQueueSize1);
            this.groupBox3.Controls.Add(this.lblQueueName1);
            this.groupBox3.Location = new System.Drawing.Point(20, 498);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(244, 97);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Queue - Sizes";
            // 
            // lblQueueSize3
            // 
            this.lblQueueSize3.AutoSize = true;
            this.lblQueueSize3.Location = new System.Drawing.Point(194, 64);
            this.lblQueueSize3.Name = "lblQueueSize3";
            this.lblQueueSize3.Size = new System.Drawing.Size(35, 13);
            this.lblQueueSize3.TabIndex = 5;
            this.lblQueueSize3.Text = "label2";
            // 
            // lblQueueName3
            // 
            this.lblQueueName3.AutoSize = true;
            this.lblQueueName3.Location = new System.Drawing.Point(18, 64);
            this.lblQueueName3.Name = "lblQueueName3";
            this.lblQueueName3.Size = new System.Drawing.Size(173, 13);
            this.lblQueueName3.TabIndex = 4;
            this.lblQueueName3.Text = "xxxxxxxxxxxxxxxxxxlblQueueName1";
            // 
            // lblQueueSize2
            // 
            this.lblQueueSize2.AutoSize = true;
            this.lblQueueSize2.Location = new System.Drawing.Point(194, 42);
            this.lblQueueSize2.Name = "lblQueueSize2";
            this.lblQueueSize2.Size = new System.Drawing.Size(35, 13);
            this.lblQueueSize2.TabIndex = 3;
            this.lblQueueSize2.Text = "label2";
            // 
            // lblQueueName2
            // 
            this.lblQueueName2.AutoSize = true;
            this.lblQueueName2.Location = new System.Drawing.Point(18, 42);
            this.lblQueueName2.Name = "lblQueueName2";
            this.lblQueueName2.Size = new System.Drawing.Size(173, 13);
            this.lblQueueName2.TabIndex = 2;
            this.lblQueueName2.Text = "xxxxxxxxxxxxxxxxxxlblQueueName1";
            // 
            // lblQueueSize1
            // 
            this.lblQueueSize1.AutoSize = true;
            this.lblQueueSize1.Location = new System.Drawing.Point(194, 21);
            this.lblQueueSize1.Name = "lblQueueSize1";
            this.lblQueueSize1.Size = new System.Drawing.Size(35, 13);
            this.lblQueueSize1.TabIndex = 1;
            this.lblQueueSize1.Text = "label2";
            // 
            // lblQueueName1
            // 
            this.lblQueueName1.AutoSize = true;
            this.lblQueueName1.Location = new System.Drawing.Point(18, 21);
            this.lblQueueName1.Name = "lblQueueName1";
            this.lblQueueName1.Size = new System.Drawing.Size(173, 13);
            this.lblQueueName1.TabIndex = 0;
            this.lblQueueName1.Text = "xxxxxxxxxxxxxxxxxxlblQueueName1";
            // 
            // timer1
            // 
            this.timer1.Interval = 2000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // ServerManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(740, 607);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menu);
            this.MainMenuStrip = this.menu;
            this.Name = "ServerManager";
            this.Text = "XD Server Manager";
            this.menu.ResumeLayout(false);
            this.menu.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menu;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem xDServerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stopToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListBox listUserOnline;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ToolStripMenuItem beginReceiveToolStripMenuItem;
        private System.Windows.Forms.RichTextBox logOutput;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label lblQueueSize1;
        private System.Windows.Forms.Label lblQueueName1;
        private System.Windows.Forms.Label lblQueueSize3;
        private System.Windows.Forms.Label lblQueueName3;
        private System.Windows.Forms.Label lblQueueSize2;
        private System.Windows.Forms.Label lblQueueName2;
        private System.Windows.Forms.Timer timer1;
    }
}

