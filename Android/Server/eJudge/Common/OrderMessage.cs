using System;
using System.Collections.Generic;
using System.Text;

namespace com.signalkontor.Common
{
    public class OrderMessage : SerializeableObject
    {
        // Ship to address
        string _toName = string.Empty;
        string _toCompany = string.Empty;
        string _toStreet = string.Empty;
        string _toZIP = string.Empty;
        string _toCity = string.Empty;
        string _toCountry = string.Empty;
        string _toRemark = string.Empty;

        // ship pickup address
        string _fromName = string.Empty;
        string _fromCompany = string.Empty;
        string _fromStreet = string.Empty;
        string _fromZIP = string.Empty;
        string _fromCity = string.Empty;
        string _fromCountry = string.Empty;
        string _fromRemark = string.Empty;
        // Other
        int _id;
        string _description = string.Empty;
        string _remarks = string.Empty;
        int _canRejact;        
        double _payment;
        int _status;

        public OrderMessage()
            : base(11)
        {
        }

#if !Android
        public OrderMessage(byte[] buffer)
            : base(buffer)
        {
        }
#endif

        /// <summary>
        /// The ID of the Order
        /// </summary>
        public int ID
        {
            set { _id = value; }
            get { return _id; }
        }

        public string ToName
        {
            set { _toName = value; }
            get { return _toName; }
        }

        public string ToCompany
        {
            set { _toCompany = value; }
            get { return _toCompany; }
        }

        public string ToStreet
        {
            set { _toStreet = value; }
            get { return _toStreet; }
        }

        public string ToZIP
        {
            set { _toZIP = value; }
            get { return _toZIP; }
        }

        public string ToCity
        {
            set { _toCity = value; }
            get { return _toCity; }
        }

        public string ToCountry
        {
            set { _toCountry = value; }
            get { return _toCountry; }
        }

        public string FromName
        {
            set { _fromName = value; }
            get { return _fromName; }
        }

        public string FromCompany
        {
            set { _fromCompany = value; }
            get { return _fromCompany; }
        }

        public string FromStreet
        {
            set { _fromStreet = value; }
            get { return _fromStreet; }
        }

        public string FromZIP
        {
            set { _fromZIP = value; }
            get { return _fromZIP; }
        }

        public string FromCity
        {
            set { _fromCity = value; }
            get { return _fromCity; }
        }

        public string FromCountry
        {
            set { _fromCountry = value; }
            get { return _fromCountry; }
        }

        /// <summary>
        /// Driver can rejact this order
        /// </summary>
        public int CanRejact
        {
            set { _canRejact = value; }
            get { return _canRejact; }
        }

        /// <summary>
        /// Payment to driver
        /// </summary>
        public double Payment
        {
            set { _payment = value; }
            get { return _payment; }
        }

        public string ToRemark
        {
            set { _toRemark = value; }
            get { return _toRemark; }
        }

        public string FromRemark
        {
            set { _fromRemark = value; }
            get { return _fromRemark; }
        }

        public string Remark
        {
            set { _remarks = value; }
            get { return _remarks; }
        }

        public string Description
        {
            set { _description = value; }
            get { return _description; }
        }

        public int Status
        {
            set { _status = value; }
            get { return _status; }
        }
    }
}
