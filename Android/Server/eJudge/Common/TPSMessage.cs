using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace com.signalkontor.Common
{
    [DataContract]
    public class TPSMessage : COMMessage
    {
        [DataMember]
        public DateTime FTimeStamp;    // Lokaler Timestamp beim Erstellen der Message
        [DataMember]
        public string FSender;         // Sender der Nachricht
        [DataMember]
        public string FReceiver;       // Adresse des Empfängers
        [DataMember]
        public string FReceiverIP;
        [DataMember]
        public int FTargetId;          // Art der Nachricht
        [DataMember]
        public int FResponseId;        // Im Falle einer Antwort die TargetId der empfangenen Nachricht,
        // sonst 0
        [DataMember]
        public string FDoCmd;          // Auszuführendes Kommando
        [DataMember]
        public string FMsgData;        // Die eigentliche Nachricht

#if !Android
        public TPSMessage(byte[] data)
            : base(data)
        {

        }
#endif

        public TPSMessage(int target, string data)
            : base(1001)
        {
            FTargetId = target;
            FMsgData = data;
        }

        public TPSMessage()
            : base(120)
        { }


    }
}
