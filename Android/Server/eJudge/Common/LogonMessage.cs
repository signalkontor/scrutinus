using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace com.signalkontor.Common
{
    [DataContract]
    public class LogonMessage : COMMessage
    {
        [DataMember]
        public string User { get; set; }
        [DataMember]
        public string Password { get; set; }

#if !Android
        public LogonMessage(byte[] buffer) : base(buffer)
        {

        }
#endif

        public LogonMessage(string user, string password) : base(1)
        {
            User = user;
            Password = password;
        }

    }
}
