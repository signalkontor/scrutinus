using System;
using System.Collections.Generic;
using System.Text;

namespace com.signalkontor.Common
{
    public class IntMessage : COMMessage
    {

        int _value;

        public int Value
        {
            get { return _value; }
            set { _value = value; setPayload(); }
        }

        public IntMessage(int i)
            : base()
        {
            AppID = 3;
            _value = i;
            setPayload();
        }

#if !Android
        public IntMessage(byte[] buffer)
            : base(buffer)
        {
            _value = _payload[0] * 256 + _payload[1];
        }
#endif

        protected override void setPayload()
        {
            _payload = new byte[2];
            _payload[0] = getHighByte(_value);
            _payload[1] = getLowByte(_value);
        }
    }
}
