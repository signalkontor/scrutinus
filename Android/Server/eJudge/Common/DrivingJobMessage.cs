using System;
using System.Collections.Generic;
using System.Text;

namespace com.signalkontor.Common
{
    public class DrivingJobMessage : StringMessage
    {
        private static int appId = 4;
        private DrivingJob _job = null;

        public DrivingJob Job
        {
            get { return _job; }
            set { _job = value; Message = value.ToXmlString(); }
        }

        public DrivingJobMessage(string message)
            : base(message, appId)
        {
            _job = DrivingJob.FromXmlString(message);
        }

        public DrivingJobMessage(DrivingJob dj)
            : base(dj.ToXmlString(), appId)
        {
        }
    }
}
