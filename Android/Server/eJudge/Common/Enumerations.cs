namespace com.signalkontor.Common
{    
    public enum DrivingJobStatus
    {
        /*
        New,
        Accepted,
        Started,
        Finished,
        Interrupted
        */

        Neu = 10,
        Offen = 20,
        Vergeben = 30,
        Akzeptiert = 40,
        Unterwegs = 50,
        Geladen = 60,
        Abgeladen = 70,
        Abgeschlossen = 80,
        Abgerechnet = 90,
        Unterbrochen = 100,
        Started = 110
    }

    public enum GeneralStatus : int
    {
        /*
        Loading,
        Unloading,
        TrafficJam,
        Pause,
        OnRoad
        */

        Abwesend = 10,      // Krank, Urlaub...
        Freigemeldet = 20,
        Unterwegs = 30,
        Pause = 40,
        TrafficJam = 50
    }

}