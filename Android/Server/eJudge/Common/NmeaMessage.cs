using System;
using System.Collections.Generic;
using System.Text;

namespace com.signalkontor.Common
{
    public class NmeaMessage : StringMessage
    {
        private const int _thisAppId = 5;

        public NmeaMessage(string message)
            : base(message, _thisAppId)
        {
        }
    }
}
