using System;
using System.Collections.Generic;
using System.Text;
using com.signalkontor.Common;

namespace com.signalkontor.Common
{
    public class XDQueue<T>
    {
        List<T> _buffer;

        public XDQueue()
        {
            _buffer = new List<T>();
        }

        public int Count
        {
            get
            {
                lock (_buffer)
                {
                    return _buffer.Count;
                }
            }
        }

        public void Enqueue(T msg)
        {
            lock (_buffer)
            {
                _buffer.Add(msg);
            }
        }

        public T Peak()
        {
            lock(_buffer)
            {
                if (_buffer.Count > 0)
                    return _buffer[0];
                else
                    return default(T);
            }
        }

        public void Remove(T msg)
        {
            lock (_buffer)
            {
                _buffer.Remove(msg);
            }
        }

        public T Dequeue()
        {
            lock (_buffer)
            {
                if (_buffer.Count > 0)
                {
                    T msg = _buffer[0];
                    _buffer.RemoveAt(0);
                    return msg;
                }
                else
                    return default(T);
            }
        }
    }
}
