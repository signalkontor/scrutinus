using System;
using System.Collections.Generic;
using System.Text;

namespace com.signalkontor.Common
{
    public class GPSMessage : StringMessage
    {
        public double Longitude { get; private set; }
        public double Latitude { get; private set; }
        public double Speed { get; private set; }
        public DateTime TimeOfFix { get; private set; }

        public GPSMessage(double lo, double la, double speed, DateTime timeOfFix)
            : base(lo + "|" + la + "|" + speed + "|" + timeOfFix, 5)
        {
            Longitude = lo;
            Latitude = la;
            Speed = speed;
            TimeOfFix = timeOfFix;
        }

#if !Android
        public GPSMessage(byte[] buffer)
            : base(buffer)
        {
            var list = _message.Split('|');

            if (list.Length != 4)
            {
                throw new FormatException("Wrong Format of Position String, expected 4 parms - got " + list.Length);
            }

            Longitude = Double.Parse(list[0]);
            Latitude = Double.Parse(list[1]);
            Speed = Double.Parse(list[2]);
            TimeOfFix = DateTime.Parse(list[3]);

        }
#endif
    }
}
