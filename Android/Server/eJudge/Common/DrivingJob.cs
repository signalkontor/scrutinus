using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace com.signalkontor.Common
{
    public class DrivingJob
    {
        public int ID { get; private set; }
        public string To { get; private set; }
        public string From { get; private set; }
        public string Cargo { get; private set; }
        public string Description { get; private set; }
        public double Fee { get; private set; }
        public DrivingJobStatus Status { get; set; }

        public DrivingJob(int id, string toLocation, string fromLocation, string cargo, string description, double fee, DrivingJobStatus status)
        {
            this.ID = id;
            this.To = toLocation;
            this.From = fromLocation;
            this.Cargo = cargo;
            this.Description = description;
            this.Fee = fee;
            this.Status = status;
        }

        public string ToXmlString()
        {
            string result = String.Empty;
            result = "<DrivingJob>"
                + "<ID>" + this.ID.ToString() + "</ID>"
                + "<ToLocation>" + this.To + "</ToLocation>"
                + "<FromLocation>" + this.From + "</FromLocation>"
                + "<Cargo>" + this.Cargo + "</Cargo>"
                + "<Description>" + this.Description + "</Description>"
                + "<Fee>" + this.Fee.ToString() + "</Fee>"
                + "<Status>" + ((int)this.Status).ToString() + "</Status>"
                + "</DrivingJob>";

            return result;
        }

        public static DrivingJob FromXmlString(string xml) 
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);

            XmlElement root = doc.DocumentElement;
            XmlElement id = root["ID"];
            XmlElement toLocation = root["ToLocation"];
            XmlElement fromLocation = root["FromLocation"];
            XmlElement cargo = root["Cargo"];
            XmlElement description = root["Description"];
            XmlElement fee = root["Fee"];
            XmlElement status = root["Status"];

            return new DrivingJob(Int32.Parse(id.InnerText), toLocation.InnerText, fromLocation.InnerText, cargo.InnerText, description.InnerText, Double.Parse(fee.InnerText), (DrivingJobStatus)Int32.Parse(status.InnerText));
        }
    }
}
