using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;

namespace com.signalkontor.Common
{
    [DataContract]
    public class COMMessage
    {
        protected byte[] _buffer;
        protected byte[] _payload = null;

        [DataMember]
        public int AppID { get; set; }

        public COMMessage()
        {
            AppID = 0;
        }

        public COMMessage(int appId)
            : base()
        {
            AppID = appId;
        }
        public COMMessage(string str, int appId)
        {
            System.Text.ASCIIEncoding enc = new System.Text.ASCIIEncoding();
            _payload = enc.GetBytes(str);
            AppID = appId;
        }

#if !Android
        public COMMessage(byte[] buffer)
        {

            _buffer = buffer;
            AppID = buffer[2] * 256 + buffer[3];
            /*
                        _payload = new byte[buffer.Length - 4];
                        for (int i=0; i<_payload.Length; i++)
                        {
                            _payload[i] = buffer[i+4];
                        }
            */
            ObjectSerializer ser = new ObjectSerializer();
            ser.deserialize(buffer, this);
        }
#endif

        public static T FromJson<T>(byte[] json) where T : class
        {
            var dataContractJsonSerializer = new DataContractJsonSerializer(typeof(T));
            var memoryStream = new MemoryStream(json);
            return dataContractJsonSerializer.ReadObject(memoryStream) as T;
        }

        public static T FromJson<T>(string json) where T : class
        {
            return FromJson<T>(Encoding.UTF8.GetBytes(json));
        }

        public string ToJson()
        {
            var memoryStream = new MemoryStream();
            var dataContractJsonSerializer = new DataContractJsonSerializer(GetType());
            dataContractJsonSerializer.WriteObject(memoryStream, this);
            memoryStream.Position = 0;
            var streamReader = new StreamReader(memoryStream);
            return streamReader.ReadToEnd();
        }

        private string _json;
        public string GetJson()
        {
            return _json ?? (_json = ToJson());
        }

        protected virtual void setPayload() { }

#if !Android
        public byte[] getBuffer()
        {

            if (_buffer != null)
            {
                // Sicherheitshalber noch die Buffer - Length sezen
                _buffer[0] = getHighByte(_buffer.Length);
                _buffer[1] = getLowByte(_buffer.Length);
                return _buffer;
            }
            ObjectSerializer ser = new ObjectSerializer();
            return ser.serialize(this, this.AppID);
        }
#endif

        public byte[] getSizeArray()
        {
            byte[] buffer = new byte[2];
            int size = _payload.Length + 2 + 2;
            buffer[0] = this.getHighByte(size);
            buffer[1] = this.getLowByte(size);

            return buffer;
        }


        protected byte getHighByte(int value)
        {
            int test1 = value >> 8;
            int test2 = value >> 4;
            return (byte) ((value & (65280)) >> 8);
        }

        protected byte getLowByte(int value)
        {
            return (byte)(value & 255);
        }

        public override string ToString()
        {
            return "\nApplication ID: " + AppID.ToString() + "\nPayload: " + ASCIIEncoding.ASCII.GetString(_payload, 0, _payload.Length) + "\n";
        }
    }
}
