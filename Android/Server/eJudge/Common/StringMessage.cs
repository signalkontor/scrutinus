using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace com.signalkontor.Common
{
    [DataContract]
    public class StringMessage : COMMessage
    {
        protected string _message;

        [DataMember]
        public string Message
        {
            get { return _message; }
            set { _message = value; setPayload(); }
        }

        public StringMessage()
        {
            _message = "";
        }

#if !Android
        public StringMessage(byte[] buffer)
            : base(buffer)
        {
            // this.message = new string(AdressOf(_payload), 0, _payload.Length);
            _message = System.Text.ASCIIEncoding.ASCII.GetString(_payload, 0, _payload.Length);
        }
#endif

        public StringMessage(string message, int appId)
            : base(message, appId)
        {
            _message = message;
            AppID = appId;
        }

        protected override void setPayload()
        {
            System.Text.ASCIIEncoding enc = new System.Text.ASCIIEncoding();
            _payload = enc.GetBytes(_message);
        }
    }
}
