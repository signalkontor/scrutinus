using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace com.signalkontor.Common
{
    [DataContract]
    public class TimeReferenceMessage : COMMessage
    {
        [DataMember]
        public DateTime ReferenceTime;

        public TimeReferenceMessage()
            : base(10)
        {
            ReferenceTime = DateTime.Now;
        }

#if !Android
        public TimeReferenceMessage(byte[] data)
            : base(data)
        {

        }
#endif
    }
}
