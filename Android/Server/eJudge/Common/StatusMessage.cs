using System;
using System.Collections.Generic;
using System.Text;

namespace com.signalkontor.Common
{
    public class StatusMessage : StringMessage
    {
        private static int _thisAppId = 6;
        private GeneralStatus _status;

        public GeneralStatus Status 
        {
            get { return _status; }
            set { _status = value; }
        }

        public int StatusInt
        {
            get { return (int)_status; }
            set { _status = (GeneralStatus)value; }
        }

        public string StatusString
        {
            get { return _status.ToString(); }
            set
            {
                switch (value)
                {
                    /*case "Loading": _status = GeneralStatus.Loading;
                        break;
                    case "Unloading": _status = GeneralStatus.Unloading;
                        break;*/
                    case "Pause": _status = GeneralStatus.Pause;
                        break;
                    case "TrafficJam": _status = GeneralStatus.TrafficJam;
                        break;
                    case "Abwesend": _status = GeneralStatus.Abwesend;
                        break;
                    case "Freigemeldet": _status = GeneralStatus.Freigemeldet;
                        break;
                    case "Unterwegs": _status = GeneralStatus.Unterwegs;
                        break;
                    default: _status = GeneralStatus.Pause;
                        break;
                }
            }
        }

        public StatusMessage(string message)
            : base(message, _thisAppId)
        {
            this.StatusString = message;
        }

        public StatusMessage(GeneralStatus status) 
            : base(status.ToString(), _thisAppId)
        {
            _status = status;
        }

#if !Android
        public StatusMessage(byte[] buffer)
            : base(buffer)
        {
            this.StatusString = System.Text.ASCIIEncoding.ASCII.GetString(_payload);
        }
#endif
    }
}
