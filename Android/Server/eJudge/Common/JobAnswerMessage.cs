using System;
using System.Collections.Generic;
using System.Text;

namespace com.signalkontor.Common
{
    public class JobAnswerMessage : StringMessage
    {

        public const int Rejacted   = 0;
        public const int Accepted   = 1;
        public const int Started    = 2;
        public const int Interruped = 3;
        public const int Finished   = 4;

        public string User { get; set; }
        public int State { get; set; }
        public int JobID { get; set; }

#if !Android
        public JobAnswerMessage(byte[] buffer) : base(buffer)
        {
            string[] list = _message.Split('|');

            if (list.Length != 3)
            {
                throw new FormatException("Wrong Format of JobAnswer String, expected 3 parms - got " + list.Length);
            }

            User = list[0];
            JobID = Int32.Parse(list[1]);
            State = Int32.Parse(list[2]);
        }
#endif

        public JobAnswerMessage(string user, int jobId, int state)
            : base(user + "|" + jobId.ToString() + "|" + state.ToString(), 14)
        {
            User = user;
            JobID = jobId;
            State = state;
        }
    }
}
