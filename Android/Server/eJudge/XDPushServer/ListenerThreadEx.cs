﻿
using System;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.signalkontor.Common;
using System.Configuration;

namespace com.signalkontor.XDPushServer
{
    class ListenerThreadEx : IListenerThread
    {
        private static readonly Encoding Encoding = Encoding.UTF8;

        private readonly int _port;
        private readonly TcpListener _tcpListener;

        public ListenerThreadEx(int port)
        {
            _port = port;
            _port = int.Parse(ConfigurationManager.AppSettings["port"]); // FIXME

            var ipAddress = NetworkInterface.GetAllNetworkInterfaces()
                .SelectMany(ni => ni.GetIPProperties().UnicastAddresses)
                .Where(ua => ua.Address.AddressFamily == AddressFamily.InterNetwork && !IPAddress.IsLoopback(ua.Address))
                .Select(ua => ua.Address)
                .First();

            _tcpListener = new TcpListener(ipAddress, port);
        }

        public void Start()
        {
            while (true)
            {
                var tcpClient = _tcpListener.AcceptTcpClient();
                new Task(obj => ReceiveMessage(obj as TcpClient), tcpClient).Start();
            }
        }

        public void Stop()
        {
            lock (_tcpListener)
            {
                _tcpListener.Stop();
            }
        }

        public event MessageReceivedDelegate MessageReceivedEvent;

        private static void ReceiveMessage(TcpClient tcpClient)
        {
            try
            {
                using (var stream = tcpClient.GetStream())
                {
                    var pos = 0;
                    var buffer = new byte[4096];
                    while(pos < buffer.Length)
                    {
                        var bytesRead = stream.Read(buffer, pos, buffer.Length - pos);
                        if (bytesRead == -1) break;
                        pos += bytesRead;
                    }

                    var genericMessage = COMMessage.FromJson<COMMessage>(buffer);
                    var tmqeJsMsg = new PushClient.objects.TMQeJSMsg();
                    switch (genericMessage.AppID)
                    {
                        case 1: // logon message
                            var logonMessage = COMMessage.FromJson<LogonMessage>(buffer);
                            tmqeJsMsg.TargetId = 2; // User Logon
                            tmqeJsMsg.DoCmd = "";
                            tmqeJsMsg.MsgData = logonMessage.User;
                            tmqeJsMsg.TimeStamp = DateTime.Now;
                            tmqeJsMsg.Sender = logonMessage.User;
                            tmqeJsMsg.Receiver = "";
                            tmqeJsMsg.ReceiverIP = "";
                            break;
                        case 2: // string message
                            var stringMessage = COMMessage.FromJson<StringMessage>(buffer);
                            tmqeJsMsg.TargetId = 3; // User Logoff
                            tmqeJsMsg.DoCmd = "";
                            tmqeJsMsg.MsgData = stringMessage.Message;
                            tmqeJsMsg.TimeStamp = DateTime.Now;
                            tmqeJsMsg.Sender = stringMessage.Message;
                            break;

                        case 120: // tps message
                            var tpsMessage = COMMessage.FromJson<TPSMessage>(buffer);
                            tmqeJsMsg.DoCmd = tpsMessage.FDoCmd;
                            tmqeJsMsg.MsgData = tpsMessage.FMsgData;
                            tmqeJsMsg.ResponseId = tpsMessage.FResponseId;
                            tmqeJsMsg.Sender = tpsMessage.FSender;
                            tmqeJsMsg.TargetId = tpsMessage.FTargetId;
                            tmqeJsMsg.TimeStamp = tpsMessage.FTimeStamp;
                            tmqeJsMsg.ReceiverIP = tpsMessage.FReceiverIP;
                            tmqeJsMsg.Receiver = tpsMessage.FReceiver;
                            break;

                        default:
                            tmqeJsMsg.DoCmd = "";
                            tmqeJsMsg.MsgData = "unknown Message: " + genericMessage.AppID;
                            tmqeJsMsg.Receiver = "";
                            tmqeJsMsg.ReceiverIP = "";
                            tmqeJsMsg.ResponseId = 0;
                            tmqeJsMsg.Sender = null; // will be set later
                            tmqeJsMsg.TargetId = 500;
                            tmqeJsMsg.TimeStamp = DateTime.Now;
                            break;
                    }

                    if (tmqeJsMsg.Sender == null)
                    {
                        var endpoint = tcpClient.Client.RemoteEndPoint;
                        string user = null;
                        if(endpoint is IPEndPoint)
                        {
                            user = UserManager.GetUser((endpoint as IPEndPoint).Address);
                        }
                        tmqeJsMsg.Sender = user ?? "";
                    }
                }
            }
            catch { }
            finally
            {
                tcpClient.Close();
            }
        }

        public int PushMessage(COMMessage msg, string user)
        {
            var tcpClient = new TcpClient();
            var address = UserManager.GetAddress(user);
            try
            {
                tcpClient.Connect(new IPEndPoint(address, _port));
                using (var stream = tcpClient.GetStream())
                {
                    var bytes = Encoding.GetBytes(msg.ToJson());
                    stream.Write(bytes, 0, bytes.Length);
                    stream.Flush();

                    // -1 == no acknowlege byte sent => user is offline
                    if (stream.ReadByte() == -1)
                        return 1;
                }
            }
            catch
            {
                // something went wrong => assume message did not reach the recipient
                return 1;
            }
            finally
            {
                tcpClient.Close();
            }
            // no errors, got acknowledge byte => all ok
            return 0;
        }
    }
}
