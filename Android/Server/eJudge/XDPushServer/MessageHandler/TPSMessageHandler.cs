using System;
using System.Collections.Generic;
using System.Text;
using System.Messaging;
using com.signalkontor.Common;
using System.Xml.Serialization;

namespace com.signalkontor.PushClient.objects
{
    public class TMQeJSMsg
    {
        public DateTime TimeStamp;    // Lokaler Timestamp beim Erstellen der Message
        public string Sender;         // Sender der Nachricht
        public string Receiver;       // Adresse des Empf�ngers
        public string ReceiverIP;     // IP Adresse Empf�nger wird nicht mehr genutzt
        public int TargetId;          // Art der Nachricht
        public int ResponseId;        // Im Falle einer Antwort die TargetId der empfangenen Nachricht,
                                      // sonst 0
        public string DoCmd;          // Auszuf�hrendes Kommando
        public string MsgData;        // Die eigentliche Nachricht

    }
}


namespace com.signalkontor.XDPushServer.MessageHandler
{

    class TPSMessageHandler : GenericMessageHandler
    {

        private string _queuePath;
        private IListenerThread _thread;
        MessageQueue _queue;
#if USEFILE
        object syncObject = new object();
#endif
        readonly string _messagePath;

        public TPSMessageHandler(IListenerThread thread)
            : base(new int[] {1, 2, 120 }, thread)
        {
            _thread = thread;
            _queuePath = System.Configuration.ConfigurationManager.AppSettings["tps.in"];
            _messagePath = System.Configuration.ConfigurationManager.AppSettings["message.Path.out"];


            if (_messagePath == null)
                throw new Exception("Message.Path.out in app.config not set!!!");
            
            if (!_messagePath.EndsWith(@"\"))
                _messagePath = _messagePath + "\\";

            if(!System.IO.Directory.Exists(_messagePath))
            {
                Console.WriteLine("Creating Directory " + _messagePath);
                System.IO.Directory.CreateDirectory(_messagePath);
            }
        }

        protected override void HandleMessage(COMMessage msg, string user)
        {
            Console.WriteLine(msg.ToJson());
            try
            {
                // Setup Message Queue at first run
                if (_queue == null)
                {
                    _queue = new MessageQueue(_queuePath);
                    var formatter = new XmlMessageFormatter(new [] { typeof(PushClient.objects.TMQeJSMsg) });
                    _queue.Formatter = formatter;
                }

                var tmqeJsMsg = new PushClient.objects.TMQeJSMsg();
                switch (msg.AppID)
                {
                    case 1:
                        var logonMessage =
#if Android
                            msg as LogonMessage;
#else
                            new LogonMessage(msg.getBuffer());
#endif
                        tmqeJsMsg.TargetId = 2; // User Logon
                        tmqeJsMsg.DoCmd = "";
                        tmqeJsMsg.MsgData = logonMessage.User;
                        tmqeJsMsg.TimeStamp = DateTime.Now;
                        tmqeJsMsg.Sender = logonMessage.User;
                        tmqeJsMsg.Receiver = "";
                        tmqeJsMsg.ReceiverIP = "";
                        break;

                    case 2:
                        var stringMessage =
#if Android
                            msg as StringMessage;
#else
                            (StringMessage)msg;
#endif
                        tmqeJsMsg.TargetId = 3; // User Logoff
                        tmqeJsMsg.DoCmd = "";
                        tmqeJsMsg.MsgData = stringMessage.Message;
                        tmqeJsMsg.TimeStamp = DateTime.Now;
                        tmqeJsMsg.Sender = stringMessage.Message;
                        break;

                    case 120:
                        var tpsMessage =
#if Android
                            msg as TPSMessage;
#else
                            new TPSMessage(msg.getBuffer());
#endif
                        tmqeJsMsg.DoCmd = tpsMessage.FDoCmd;
                        tmqeJsMsg.MsgData = tpsMessage.FMsgData;
                        tmqeJsMsg.ResponseId = tpsMessage.FResponseId;
                        tmqeJsMsg.Sender = tpsMessage.FSender;
                        tmqeJsMsg.TargetId = tpsMessage.FTargetId;
                        tmqeJsMsg.TimeStamp = tpsMessage.FTimeStamp;
                        tmqeJsMsg.ReceiverIP = tpsMessage.FReceiverIP;
                        tmqeJsMsg.Receiver = tpsMessage.FReceiver;
                        break;

                    default:
                        tmqeJsMsg.DoCmd = "";
                        tmqeJsMsg.MsgData = "unknown Message: " + msg.AppID;
                        tmqeJsMsg.Receiver = "";
                        tmqeJsMsg.ReceiverIP = "";
                        tmqeJsMsg.ResponseId = 0;
                        tmqeJsMsg.Sender = user;
                        tmqeJsMsg.TargetId = 500;
                        tmqeJsMsg.TimeStamp = DateTime.Now;
                        break;
                }

                if (tmqeJsMsg.Sender == null)
                {
                    tmqeJsMsg.Sender = user ?? "";
                }

#if USEFILE
                lock(syncObject)
                {
                    // MEssage Z�hler erh�hen und pr�fen
                    messageNumber++;
                    if(messageNumber == Int32.MaxValue)
                        messageNumber = 0;
                    DateTime now = DateTime.Now;
                    // Tempdatei erzeugen
                    string filename = messagePath + String.Format("{0:HHmmss}_{1}.tmp", now, messageNumber);
                    
                    System.IO.StreamWriter stream = new System.IO.StreamWriter(filename);
                    XmlSerializer serializer = new XmlSerializer(t.GetType());
                    serializer.Serialize(stream, t);
                    stream.Close();
                    // Rename der TMP Datei nach XML
                    string newName = System.IO.Path.ChangeExtension(filename, "xml"); 
                    System.IO.File.Move(filename, newName);

                }

#endif

                lock (_queue)
                {
                    _queue.Send(tmqeJsMsg);
#if DEBUG
                    Console.WriteLine(String.Format("{0} : Nachricht an MSQueue �bergeben : {1} TargetID: {2}", DateTime.Now.ToLongTimeString(), tmqeJsMsg.DoCmd, tmqeJsMsg.TargetId));
#endif
                }
            }
            catch (Exception ex)
            {
                //log.Info(t.MsgData);
                log.Error(ex);
            }
        }
    } // class
} // namespace
