#if !Android
using System;
using System.Collections.Generic;
using System.Text;
using com.signalkontor.Common;
using com.signalkontor.PushClient.objects;
namespace com.signalkontor.XDPushServer.MessageHandler
{
    class TPSSocketMessageHandler : GenericMessageHandler
    {
        private ListenerThread _thread;

        public TPSSocketMessageHandler(ListenerThread thread)
            : base(new int[] {1, 2, 110, 120 }, thread)
        {
            _thread = thread;
        }

        protected override void HandleMessage(COMMessage msg, string user)
        {
            var t = new TMQeJSMsg();

            switch (msg.AppID)
            {

                case 1:
                    var l = new LogonMessage(msg.getBuffer());
                    t.TargetId = 2; // User Logon
                    t.DoCmd = "";
                    t.MsgData = l.User;
                    t.TimeStamp = DateTime.Now;
                    t.Sender = l.User;
                    t.Receiver = "";
                    t.ReceiverIP = "";
                    SendToTPS(t);
                    break;
                case 2:
                    var s = (StringMessage)msg;
                    t.TargetId = 3; // User Logoff
                    t.DoCmd = "";
                    t.MsgData = s.Message;
                    t.TimeStamp = DateTime.Now;
                    t.Sender = s.Message;
                    SendToTPS(t);
                    break;
                case 110:
                    // Wir haben eine Nachricht vom TPS Client bekommen, die nun 
                    // weiter geleitet werden muss an den eJudge Client
                    var sm = new StringMessage(msg.getBuffer());
                    var tpsout = ParseString(sm.Message);
                    // Wir senden nun diese Nachricht an den Client weiter
                    _thread.PushMessage(tpsout, tpsout.FReceiver);
                    break;
           
                case 120:
                    var tps = new TPSMessage(msg.getBuffer());
                    t.DoCmd = tps.FDoCmd;
                    t.MsgData = tps.FMsgData;
                    t.ResponseId = tps.FResponseId;
                    t.Sender = tps.FSender;
                    t.TargetId = tps.FTargetId;
                    SendToTPS(t);
                    break;

                default:
                    t.DoCmd = "";
                    t.MsgData = "unknown Message: " + msg.AppID;
                    t.Receiver = "";
                    t.ReceiverIP = "";
                    t.ResponseId = 0;
                    t.Sender = user;
                    t.TargetId = 500;
                    t.TimeStamp = DateTime.Now;
                    SendToTPS(t);
                    break;
            }
        }

        private void SendToTPS(TMQeJSMsg message)
        {
            var m = message.TargetId + "\t" + message.DoCmd + "\t" + message.MsgData + "\t" + message.Receiver + "\t" + message.ReceiverIP + "\t" + message.ResponseId + "\t" + message.Sender + "\t" + message.TimeStamp.ToLongTimeString();
            _thread.PushMessage(new StringMessage(m, 110), "TPS");
        }

        private TPSMessage ParseString(string p)
        {
#if DEBUG
            log.Info("TPSMessage erhalten parse String: " + p);
#endif
            string[] elem = p.Split('\t');
            TPSMessage msg = new TPSMessage();
            msg.FTargetId = Int32.Parse(elem[0]);
            msg.FDoCmd = elem[1];
            msg.FMsgData = elem[2];
            msg.FReceiver = elem[3];
            msg.FTimeStamp = DateTime.Now;
            msg.FReceiverIP = elem[4];
            msg.FResponseId = Int32.Parse(elem[5]);
            msg.FSender = elem[6];

#if DEBUG
            log.Info(String.Format("Daten erhalten - TargetID:{0}, DoCmd:{1}, MsgData:{2} Receiver:{3},ResponseId:{4}, Sender:{5}", msg.FTargetId, msg.FDoCmd, msg.FMsgData, msg.FReceiver, msg.FResponseId, msg.FSender));
#endif
            return msg;
        }
    }
}
#endif
