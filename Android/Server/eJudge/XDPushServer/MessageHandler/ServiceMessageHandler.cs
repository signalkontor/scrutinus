using System;
using System.Collections.Generic;
using System.Text;
using System.Messaging;

namespace com.signalkontor.XDPushServer.MessageHandler
{

    public class UserMessage
    {
        private string _user = "";
        private byte[] _buffer = null;

        public string User
        {
            get { return _user;}
            set {_user = value;}
        }

        public byte[] Buffer
        {
            set{_buffer = value;}
            get{return _buffer;}
        }
    }


    public class ServiceMessageHandler : GenericMessageHandler
    {

        string _queue = @".\private$\xdserver.service.out";

        public ServiceMessageHandler(ListenerThread thread)
            : base(0, thread)
        {

            if(!MessageQueue.Exists(_queue))
            {
                MessageQueue.Create(_queue);
            }

        }

        
    
        protected override void  HandleMessage(Common.COMMessage msg, string user)
        {
#if Android
            var u = Common.COMMessage.FromJson<UserMessage>(msg.ToJson());
#else
 	        var u = new UserMessage {Buffer = msg.getBuffer()};
#endif
            u.User = user;

            var queue = new MessageQueue(_queue)
                        {
                            Formatter = new XmlMessageFormatter(new Type[] {typeof (UserMessage)})
                        };
            queue.Send(u);
        }
    }
}
