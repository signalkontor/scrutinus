using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using com.signalkontor.Common;

namespace com.signalkontor.XDPushServer.MessageHandler
{
    abstract public class GenericMessageHandler
    {
        protected int[] _appID;
        protected static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected GenericMessageHandler(int appID, IListenerThread thread)
        {
            _appID = new int[1];
            _appID[0] =   appID;
            thread.MessageReceivedEvent += MessageReceived;
        }

        protected GenericMessageHandler(int[] appID, IListenerThread thread)
        {
            _appID = new int[appID.Length];
            appID.CopyTo(_appID, 0);
            thread.MessageReceivedEvent += MessageReceived;
        }

        abstract protected void HandleMessage(COMMessage msg, string user);

        protected void MessageReceived(object sender, MessageEventArgs e)
        {
            try
            {
                if (_appID[0] == 0)
                {
                    HandleMessage(e.Message, e.User);
                }
                else
                {
                    if (_appID.Any(appId => appId == e.Message.AppID))
                    {
                        HandleMessage(e.Message, e.User);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
        }
    }
}
