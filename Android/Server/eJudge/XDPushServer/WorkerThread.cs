using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Net.Sockets;
using com.signalkontor.Common;

namespace com.signalkontor.XDPushServer
{

    public delegate void LostConnectionDelegate(object sender, EventArgs e);
    public delegate void MessageReceivedDelegate(object sender, MessageEventArgs e);

    public class MessageEventArgs : EventArgs
    {
        public MessageEventArgs(int appId, byte[] data)
        {
            switch(appId)
            {
                case 1:
                    Message = COMMessage.FromJson<LogonMessage>(data);
                    break;
                case 2:
                    Message = COMMessage.FromJson<StringMessage>(data);
                    break;
                case 120:
                    Message = COMMessage.FromJson<TPSMessage>(data);
                    break;
            }
        }

        public MessageEventArgs(COMMessage message)
        {
            Message = message;
        }

        public string User { get; set; }
        public COMMessage Message { get; set; }
    }

    public interface IWorkerThread
    {
        event LostConnectionDelegate LostConnection;
        event MessageReceivedDelegate MessageReceived;
    }

    public class WorkerThread : IWorkerThread
    {
        public event LostConnectionDelegate LostConnection;
        public event MessageReceivedDelegate MessageReceived;
        
        Thread _thread;
        Thread monitorThread;
        bool _running = true;
        Socket _inSocket;
        Socket _outSocket;
        string _user = "";

        const int ACKBUFSIZE = 150;

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        byte[] _ackBuffer;

        public WorkerThread(Socket inSocket)
        {
            _ackBuffer = new byte[ACKBUFSIZE];
            _ackBuffer[0] = 251;

           _inSocket = inSocket;
           _outSocket = null;
            _thread = new Thread(new ThreadStart(run));
            _inSocket.SendTimeout = 5000;
            _inSocket.SetSocketOption(SocketOptionLevel.Tcp, SocketOptionName.NoDelay, true);
        }
        
        public void setUser(string user)
        {
            _user = user;
        }

        public void SetOutSocket(Socket outSocket)
        {
            _outSocket = outSocket;
            _outSocket.ReceiveTimeout = 5000;
            _outSocket.SetSocketOption(SocketOptionLevel.Tcp, SocketOptionName.NoDelay, true);
            _outSocket.NoDelay = true;
            // wir starten nun unseren Monitor Thread ...
            monitorThread = new Thread(Monitor);
            monitorThread.Start();
        }

        public void Start()
        {
            _thread.Start();
        }

        public void Monitor()
        {
            try
            {
                MainClass.Instance.ReportThreadStart(_inSocket.RemoteEndPoint.ToString());
                // Wir senden erst mal eine Zeit
                // damit der Client seine interne Uhr
                // synchronisieren kann
                PushMessage(new TimeReferenceMessage());
                while (_running)
                {
#if !Android
                    var msg = new byte[] { 0, 4, 255, 255 };
#endif

                    Thread.Sleep(5000);
                    if(
#if Android
                        PushMessage(new COMMessage(255*256 + 255))
#else
                        PushMessage(new COMMessage(msg))
#endif
                    != 0)
                    {
                        _running = false;
                        if (_outSocket != null)
                            _outSocket.Close();
                        _inSocket.Close();
                    }
                }
            }
            catch (Exception e)
            {
                log.Warn(e);
                this._running = false;
                if(_outSocket != null)
                    _outSocket.Close();
                _inSocket.Close();
            }
            if(LostConnection != null)
                LostConnection(this, new EventArgs());
            MainClass.Instance.ReportThreadStop(_user);
        }

        public void run()
        {
            try
            {

                _running = true;
                MainClass.Instance.ReportThreadStart(_inSocket.RemoteEndPoint.ToString());
                // Unser Client erwartet ein byte, das wir ihm nun senden

                _inSocket.Send(_ackBuffer);
                
                while (_running)
                {
                    // log.Info("Anfang der Schleife");
                    byte[] buffer = new byte[2];

                    //log.Info("Vor Receive");
                    _inSocket.ReceiveTimeout = -1;
                    int count = _inSocket.Receive(buffer, 2, SocketFlags.None);
                    if (count == 2)
                    {
                        //log.Info("Rest lesen");
                        // read rest of message
                        int size = buffer[0] * 256 + buffer[1];
#if DEBUG
                  
                        log.Info(String.Format("Message erhalten mit Gesamtgröße {0}", size));
                        
#endif
                        var read = new byte[size];
                        //log.Info("Rest lesen - Receive");
                        int readed = 0;
                        int loopCount = 0;
                        while (readed < (size - 2))
                        {
                            readed += _inSocket.Receive(read, readed + 2, size - 2 - readed, SocketFlags.None);
                            loopCount++;
                            if (loopCount > 10)
                                throw new Exception("Loopcount > 10 in workerthread.run()");
                        }


                        if ((read[2] != 255) && (read[3] != 255))
                        {
#if DEBUG
                            // dumpMessage(read);
#endif

                            
                            var tmpMessage =
#if Android
                                COMMessage.FromJson<COMMessage>(read);
#else
                                new COMMessage(read);
#endif



#if DEBUG
                            log.Info("Message AppID = " + tmpMessage.AppID);
#endif
                            if (tmpMessage.AppID == 5)
                            {
                                // Client meldet sich ab
                                throw new Exception("Logoff Client!!!");
                            }
                            if (MessageReceived != null)
                            {
                                var eventArgs = 
#if Android
                                    new MessageEventArgs(tmpMessage.AppID, read);
#else
                                    new MessageEventArgs(tmpMessage);
#endif
                                MessageReceived(this, eventArgs);
                            }
                            else
                            {
                                log.Error("Keine Event - Listener vorhanden");
                            }
                        }
                        else
                        {
#if DEBUG
                            log.Info("Message PING erhalten");
#endif
                        }

                        // Wir sinden ein Ack zum Client
                        _inSocket.Send(_ackBuffer);
                    }
                    else
                    {
                        
                        log.Info("Count war 0, socket wird geschlossen");
                        
                        _inSocket.Close();
                        if (_outSocket != null)
                            _outSocket.Close();
                        //log.Info("Else-teil - Event auslösen");
                        this.LostConnection(this, new EventArgs());
                        _running = false;
                        if(monitorThread != null)
                            monitorThread.Interrupt();
                        return;
                        
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("Exception in Worker - run()");
                log.Error(e);
                _running = false;
                if(monitorThread != null)
                    monitorThread.Interrupt();
                //log.Info("Socket schliessen");
                this._inSocket.Close();
                if (_outSocket != null)
                    _outSocket.Close();
            }
            
            //log.Info("Event auslösen");
            MainClass.Instance.ReportThreadStop(_user);
            this.LostConnection(this, new EventArgs());
            _running = false;
        }

        private void DumpMessage(byte[] buffer)
        {
            string s = "";
            int count = 0;
            for (int i = 0; i < buffer.Length; i++)
            {
                count++;
                if (count == 12)
                {
                    s += "\r\n";
                    count = 0;
                }

                s += buffer[i].ToString("X2") + " ";
            }
            log.Info("Message Dump");
            log.Info("\n\r" + s);
        }

        public int PushMessage(COMMessage msg)
        {
            if (_outSocket == null)
                return 1;

            try
            {
                lock (_outSocket)
                {
                    var buffer =
#if Android
                        Encoding.UTF8.GetBytes(msg.ToJson());
#else
                        msg.getBuffer();
#endif
                    _outSocket.NoDelay = true;
                    _outSocket.Send(buffer);
                    
                    // Wir warten auf ein ACK vom Client;
                    int count = 0;
                    while (_outSocket.Available < 1)
                    {
                        count++;
                        Thread.Sleep(500);
                        if (count > 60)
                            throw new TimeoutException("Timeout while waiting for ACK, User " + _user );
                    }
#if DEBUG
                    if(count > 5)
                        log.Error(String.Format("sendMessage() count war: {0}, User {1}", count, this._user)); 
#endif

                    _outSocket.ReceiveTimeout = 15000; // Max 10 Sek. auf Antwort warten
                    _outSocket.Receive(_ackBuffer);
                    log.Info(String.Format("Message gesendet mit Gesamtgröße {0}", buffer.Length));
                }
            }
            catch (Exception e)
            {
                log.Error(e);
                try
                {
                    LostConnection(this, new EventArgs());
                    _outSocket.Close();
                    _inSocket.Close();
                    monitorThread.Interrupt();
                }
                catch (Exception ex)
                {
                    log.Error(ex);
                }
                return 1;
            }

            return 0;
        }

        

        public void CloseConnection()
        {
            _running = false;
            LostConnection(this, new EventArgs());
            _inSocket.Close();
            if (_outSocket != null)
                _outSocket.Close();
            monitorThread.Interrupt();
        }
    }
}
