using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using com.signalkontor.Common;

namespace com.signalkontor.XDPushServer
{
    public abstract class GenericListenerThread : IListenerThread
    {
        protected static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        Thread _thread;
        int _localPort;
        bool _running = true;    

        public event MessageReceivedDelegate MessageReceivedEvent;
        public abstract int PushMessage(COMMessage msg, string user);

        public GenericListenerThread(int localPort)
        {
            _localPort = localPort;
            _thread = new Thread(new ThreadStart(Run));
            _thread.Start();
        }

        private void HandleConnection(Socket socket)
        {
            var thread = new Thread(ConnectionThread);
            thread.Start(socket);
        }

        private void ConnectionThread(object socket)
        {
            Socket s = (Socket)socket;
            HandleIncommingConnection(s);
        }

        public void Run()
        {
            // Wir �ffnen den Port und los geht's
            log.Info("Listener Thread started, listening at port " + _localPort);

            try
            {
                while (_running)
                {
                    Socket listenSocket = new Socket(AddressFamily.InterNetwork,
                                     SocketType.Stream,
                                     ProtocolType.Tcp);

                    // bind the listening socket to the port
                    IPAddress hostIP;
                    hostIP = IPAddress.Parse("0.0.0.0");
                    // hostIP = (Dns.Resolve(IPAddress.Any.ToString())).AddressList[0];
                    IPEndPoint ep = new IPEndPoint(hostIP, _localPort);
                    listenSocket.Bind(ep);

                    // start listening
                    while (_running)
                    {
                        try
                        {
                            listenSocket.Listen(1024);
                            Socket mySocket = listenSocket.Accept();
                            mySocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive, false);
                            log.Info("[" + _localPort +"] Connection from " + mySocket.RemoteEndPoint.ToString());
                            HandleConnection(mySocket);
                        }
                        catch (Exception ex2)
                        {
                            log.Error(ex2);
                        }
   
                    }
                }
            }
            catch (Exception e)
            {
                log.Fatal(e);
            }
        }

        protected abstract void HandleLostConnection(object sender, EventArgs e);
        protected abstract void HandleMessageReceived(object sender, MessageEventArgs e);
        protected abstract void HandleIncommingConnection(Socket socket);

        public void LostConnection(object sender, EventArgs e)
        {
            HandleLostConnection(sender, e);
        }

        public void MessageReceived(object sender, MessageEventArgs e)
        {
            HandleMessageReceived(sender, e);
        }

        public void RaiseMessageReceivedEvent(object sender, MessageEventArgs e)
        {
            if (MessageReceivedEvent != null)
            {
                MessageReceivedEvent(sender, e);
            }
        }
    }
}
