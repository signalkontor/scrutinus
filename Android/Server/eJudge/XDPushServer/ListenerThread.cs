using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Net.Sockets;
using System.Net;
using System.IO;
using com.signalkontor.Common;
// using com.signalkontor.Common.XDPushServerDataSetTableAdapters;

namespace com.signalkontor.XDPushServer
{
    public class ListenerThread : GenericListenerThread
    {

        Dictionary<string, WorkerThread> _threadList;
        Dictionary<string, XDQueue<COMMessage>> _queueList;
        
        public static ListenerThread instance;

        public ListenerThread(int localPort) : base (localPort)
        {
            _threadList = new Dictionary<string, WorkerThread>();
            _queueList = new Dictionary<string, XDQueue<COMMessage>>();
            instance = this;
        }

        public void SetOutgoingSocket(string user, Socket socket)
        {
            if (!_threadList.ContainsKey(user))
            {
                log.Error("User [" + user + "] wurde nicht in der Thread-List gefunden");
                socket.Close();
                return;
            }
            var worker = _threadList[user];

            var logon = new LogonMessage(user, "");

            RaiseMessageReceivedEvent(this, new MessageEventArgs(logon) );

            if (worker != null)
            {
                worker.setUser(user);
                worker.SetOutSocket(socket);
                // Der User hat sich nun erfolgreich angemeldet
                // wir checken, ob er noch was in seiner Queue hat
                CheckOutQueueForUser(user);
            }
            else
            {
                log.Error("SetOutgoingSocket: Konnte workerthread zu user [" + user + "] nicht finden");
                socket.Close();
            }
        }

        protected string GetUserfromThread(WorkerThread sender)
        {

            if (sender == null)
                return null;

            if(_threadList.ContainsValue(sender))
            {
                foreach(string user in _threadList.Keys)
                {
                    if(_threadList[user] == sender)
                        return user;
                }
            }
            // Kein user gefunden
            return null;
        }

        protected override void HandleLostConnection(object sender, EventArgs e)
        {
            string user = GetUserfromThread(sender as WorkerThread);
            if (user != null)
            {

                object thread = _threadList[user];
                if (thread == sender)
                {
                    _threadList.Remove(user);
                    log.Info("Lost connection to User " + user);
                    var msg = new StringMessage(user, 2);
                    var arg = new MessageEventArgs(msg);
                    RaiseMessageReceivedEvent(sender, arg);
                }
            }
            else
            {
                log.Info("Lost connection to unknown User ");
            }
  
        }

        protected override void HandleIncommingConnection(Socket socket)
        {
            var worker = new WorkerThread(socket);
            worker.LostConnection += LostConnection;
            worker.MessageReceived += MessageReceived;
            worker.Start();
        }

        protected override void HandleMessageReceived(object sender, MessageEventArgs e)
        {
            var thread = sender as WorkerThread;

            // Check for logon
            if (e.Message.AppID == 1)
            {
                var msg =
#if Android
                    COMMessage.FromJson<LogonMessage>(e.Message.ToJson());
#else
                    new LogonMessage(e.Message.getBuffer());
#endif
 log.Info("User " + msg.User + " logon");
                if (!AuthenticateUser(msg))
                {
                    var w = (WorkerThread)sender;
                    w.PushMessage(new StringMessage("Could not authenticate user", 99));
                    w.CloseConnection();
                    return;
                }

                if (_threadList.ContainsKey(msg.User))
                {
                    _threadList.Remove(msg.User);
                }

                _threadList.Add(msg.User, thread);
                // RaiseMessageReceivedEvent(sender, e);
            }
            else
            {
                // Wir suchen den User zum Thread
                var user = GetUserfromThread(thread);
                if (e.User == null)
                {
                    e.User = user;
                }
                log.Info("Incoming Message type [" + e.Message.GetType().ToString() + "] from user " + e.User);
                // wir leiten den Event weiter, falls jemand ihn bearbeiten m�chte
                log.Info("Before Raise Event");
                RaiseMessageReceivedEvent(sender, e);
                log.Info("After Raise Event");
            }
        }

        public void CheckOutQueueForUser(string user)
        {


            if (!_queueList.ContainsKey(user))
            {
                return;
            }

            var thread = (WorkerThread)_threadList[user];
            if (thread == null)
                return;

            XDQueue<COMMessage> queue;

            lock (_queueList)
            {
                queue = _queueList[user];
            }

            if(queue != null)
            {
                while(queue.Count > 0)
                {
                    if(thread.PushMessage(queue.Peak()) == 0)
                    {
                        queue.Dequeue();
                        log.Info("Queued Message for User [" + user + "] successfully sendet");
                    }
                    else
                    {   // Senden hat nicht geklappt, wir probieren
                        // es dann auch nicht weiter.
                        log.Info("Queued Message for User [" + user + "] NOT sendet");
                        return;
                    }
                }
            }
            

        }

        public override int PushMessage(COMMessage msg, string user)
        {

            // log.Info("pushMessage(msg, " + user + ")");
            int ret = 1;

            if (_threadList.ContainsKey(user))
            {

                WorkerThread thread = (WorkerThread)_threadList[user];
                ret = thread.PushMessage(msg);
            }
            
            if(ret == 1)
            {
                // user offline
                // Wir speichern die Nachricht in der OutQueue
                lock (_queueList)
                {
                    if (_queueList.ContainsKey(user))
                    {
                        XDQueue<COMMessage> queue = _queueList[user];
                        queue.Enqueue(msg);
                    }
                    else
                    {
                        XDQueue<COMMessage> queue = new XDQueue<COMMessage>();
                        queue.Enqueue(msg);
                        _queueList.Add(user, queue);
                    }
                }
                log.Info("User [" + user + "] offline, message queued");
            }

            return ret;
        }

        public int pushMessage(string msg, string user)
        {

            // log.Info("pushMessage(" + msg + ", " + user + ")");

            StringMessage message = new StringMessage(msg, 2);

            return PushMessage(message, user);
        }

        public bool AuthenticateUser(LogonMessage msg)
        {
            return true;
        }

    }

}
