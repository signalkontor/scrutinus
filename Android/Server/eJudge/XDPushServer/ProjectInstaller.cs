using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;

namespace com.signalkontor.XDPushServer
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();
        }

        public override void Install(System.Collections.IDictionary stateSaver)
        {
            base.Install(stateSaver);
        }
    }
}