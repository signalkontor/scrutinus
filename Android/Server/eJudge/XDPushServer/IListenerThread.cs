﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using com.signalkontor.Common;

namespace com.signalkontor.XDPushServer
{
    public interface IListenerThread
    {
        event MessageReceivedDelegate MessageReceivedEvent;
        int PushMessage(COMMessage msg, string user);
    }
}
