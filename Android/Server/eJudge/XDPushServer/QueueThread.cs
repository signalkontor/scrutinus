using System;
using System.Messaging;
using com.signalkontor.PushClient.objects;
using System.Collections;
using System.IO;

namespace com.signalkontor.XDPushServer
{
    public class QueueThread
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private Sonic.Net.ThreadPool _threadPool = new Sonic.Net.ThreadPool(11, 11);

        private IListenerThread _callback;
        private MessageQueue _queue;
        private bool _running;
        private ArrayList _internalQueue;
        private string messagePath;
        private System.IO.FileSystemWatcher watcher;

        public static QueueThread instance;

        public QueueThread(IListenerThread server)
        {
            instance = this;

            _internalQueue = new ArrayList();
            _callback = server;
            Run();
            _running = true;

            messagePath = System.Configuration.ConfigurationManager.AppSettings["message.Path.in"];

            if (messagePath == null)
                throw new Exception("Message.Path in app.config not set!!!");

            if (!Directory.Exists(messagePath))
            {
                Console.WriteLine("Creating Directory " + messagePath);
                Directory.CreateDirectory(messagePath);
            }

            ProcessOldMessages();

            watcher = new FileSystemWatcher(messagePath, "*.xml")
                          {
                              NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite
                                             | NotifyFilters.FileName | NotifyFilters.DirectoryName,
                              Filter = "*.xml"
                          };

            watcher.Created += FileWatcher_Created;
         
            watcher.EnableRaisingEvents = true;
        }

        private void ProcessOldMessages()
        {
            string[] files = Directory.GetFiles(messagePath, "*.xml");
            for (int i = 0; i < files.Length; i++)
            {
                string filename = Path.GetFileName(files[i]);
                FileSystemEventArgs e = new FileSystemEventArgs(WatcherChangeTypes.Created, messagePath, filename);
                FileWatcher_Created(this, e);
            }
        }

        void FileWatcher_Created(object sender, FileSystemEventArgs e)
        {
            // Wir versuchen die Datei zu �ffnen, vielleicht
            // ist sie aber noch vom anderen Prozess ge�ffnet ...
            Console.WriteLine("Found file: " + e.Name);
            int count = 0;
            System.IO.StreamReader reader;
            while (true)
            {
                try
                {
                    reader = new StreamReader(e.FullPath);
                    break;
                }
                catch (Exception)
                {
                    System.Threading.Thread.Sleep(100);
                    count++;
                    if (count > 300) // 30 Sekunden maximal warten ...
                        return;
                }
            }

            try
            {
                var serializer = new System.Xml.Serialization.XmlSerializer(typeof(com.signalkontor.PushClient.objects.TMQeJSMsg));
                var msg = (TMQeJSMsg)serializer.Deserialize(reader);
                reader.Close();
                File.Delete(e.FullPath);
                ProcessMessage(msg);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                log.Fatal(ex);
            }
        }

        public void Run()
        {
            
            // string queuePath = @"FORMATNAME:DIRECT=OS:skwww2\private$\TPS.eJudge.out";
            try
            {
                var queuePath = System.Configuration.ConfigurationManager.AppSettings["tps.out"];

                _queue = new MessageQueue(queuePath);
                _queue.ReceiveCompleted += new ReceiveCompletedEventHandler(_queue_ReceiveCompleted);
                _queue.Formatter = new XmlMessageFormatter(new Type[] { typeof(com.signalkontor.PushClient.objects.TMQeJSMsg) });
                _queue.BeginReceive();
            }
            catch (Exception e)
            {
                log.Fatal(e);
                throw;
            }
        }

        private void ProcessMessage(TMQeJSMsg tps)
        {
            if (tps == null)
                return;

            if (tps.Sender == null)
                tps.Sender = "";
            if (tps.DoCmd == null)
                tps.DoCmd = "";
            if (tps.MsgData == null)
                tps.MsgData = "";
            if (tps.Receiver == null)
                tps.Receiver = "";
            if (tps.TimeStamp == null)
                tps.TimeStamp = DateTime.Now;


            var t = new Common.TPSMessage
                        {
                            FDoCmd = tps.DoCmd,
                            FMsgData = tps.MsgData,
                            FResponseId = tps.ResponseId,
                            FSender = tps.Sender,
                            FTargetId = tps.TargetId,
                            FTimeStamp = tps.TimeStamp,
                            FReceiverIP = tps.ReceiverIP,
                            FReceiver = tps.Receiver
                        };

            if (t.FDoCmd == null)
                t.FDoCmd = "";
            if (t.FMsgData == null)
                t.FMsgData = "";
            if (t.FSender == null)
                t.FSender = "";
            if (t.FTimeStamp == null)
                t.FTimeStamp = DateTime.Now;

            // Die Nachricht wird nun asynchron losgesendet
            
           
            new AsynchronousCodeBlocks.async(() => _callback.PushMessage(t, t.FReceiver), _threadPool);
        }

        void _queue_ReceiveCompleted(object sender, ReceiveCompletedEventArgs e)
        {

            try
            {
                Message msg = _queue.EndReceive(e.AsyncResult);

                if (msg.Body is TMQeJSMsg)
                {
                    var tps = (TMQeJSMsg)msg.Body;

                    ProcessMessage(tps);

                    while (tps != null)
                    {
                        msg = _queue.Receive(new TimeSpan(0, 0, 0, 5));
                        if (msg == null)
                        {
                            tps = null;
                            break;
                        }

                        tps = msg.Body as TMQeJSMsg;

                        ProcessMessage(tps);

                    }
                    
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }

            _queue.BeginReceive();
        }
    }
}
