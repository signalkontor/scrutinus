#define MSMQ

using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Collections.Specialized;

namespace com.signalkontor.XDPushServer
{
    class MainClass : System.ServiceProcess.ServiceBase
    {

        public static MainClass _instance;
        public int _runningThreads;

        public ListenerThread _listenerClients;
        FileHandler _fileHandler;
        SecondaryListenerThread _secondaryListener;
        AutoConfigThread _autoConfigThread;

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static MainClass Instance
        {
            get { return _instance; }
        }

        public void RunConsole()
        {
            OnStart(null);
        }

        protected void LogFallback(string message)
        {
            
            string filename = System.IO.Path.GetTempPath() + "XDCommunicationServerError.txt";
            System.IO.StreamWriter writer = new System.IO.StreamWriter(filename, true);
            writer.WriteLine(message);
            writer.Close();
            
        }

        protected override void OnStart(string[] args)
        {
            _instance = this;

            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
            NameValueCollection appStgs = System.Configuration.ConfigurationManager.AppSettings;
            
            if(appStgs["logconfig"] == null)
            {
                LogFallback("App-Config falsch");
                throw new System.ApplicationException("missing or wrong configuration file");
            }
            var fileInfo = new System.IO.FileInfo(appStgs["logconfig"]);
            if (!fileInfo.Exists)
            {
                fileInfo = new System.IO.FileInfo(AppDomain.CurrentDomain.BaseDirectory + "\\" + appStgs["logconfig"]);
                if (!fileInfo.Exists)
                {
                    LogFallback("Log4Net-Config Datei fehlt");
                    LogFallback(AppDomain.CurrentDomain.BaseDirectory);
                    throw new System.ApplicationException("Cannot find log4net configuration file at " + fileInfo.FullName);
                }
            }
            log4net.Config.XmlConfigurator.ConfigureAndWatch(fileInfo);
            
            _listenerClients = new ListenerThread(Int32.Parse(appStgs["port"]));
            _secondaryListener = new SecondaryListenerThread(Int32.Parse(appStgs["port"]) + 1);
            _fileHandler = new FileHandler(Settings.Default.FileHandlerPort);
            // MessageHandler.ServiceMessageHandler shandler = new com.signalkontor.XDPushServer.MessageHandler.ServiceMessageHandler(_listenerClients);
            // MessageHandler.TPSChatMessageHandler tpsChatHandler = new com.signalkontor.XDPushServer.MessageHandler.TPSChatMessageHandler(_listenerClients);
            _autoConfigThread = new AutoConfigThread();
            Console.WriteLine("Started");

#if MSMQ
            var tpsHandler = new MessageHandler.TPSMessageHandler(_listenerClients);
            var queueThread = new QueueThread(_listenerClients);
            log.Error(@"
***************************************
* XD Server Version 1.2.4.550 started *
* !!!      TPS MSMQ VERSION       !!! * 
***************************************");
#else

            MessageHandler.TPSSocketMessageHandler tpsSocketHandler = new MessageHandler.TPSSocketMessageHandler(_listenerClients);
            log.Error(@"
***************************************
* XD Server Version 1.2.4.540 started *
* !!!      TPS TCP/IP VERSION     !!! * 
***************************************");
#endif
        }

        void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Console.Out.WriteLine("ERROR: " + e.ExceptionObject.ToString());
            log.Fatal(e.ExceptionObject.ToString());
        }

        public void ReportThreadStart(string address)
        {
            try
            {
                _runningThreads++;
                // log.Info("Running Threads: " + _runningThreads + " connected from " + address);
            }
            catch (Exception)
            {

            }
        } 

        public void ReportThreadStop(string address)
        {
            try
            {
                _runningThreads--;
                // log.Info("Running Threads: " + _runningThreads + " Thread ended from " + address);
            }
            catch (Exception)
            { }
        }
    }
}
