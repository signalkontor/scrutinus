using System;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Text;
using com.signalkontor.Common;

namespace com.signalkontor.XDPushServer
{
/*
    class ServerListener : GenericListenerThread
    {

        IListenerThread _clientListener;

        public ServerListener(int Port, ListenerThread clientListener)
            : base(Port)
        {
            _clientListener = clientListener;
        }

        public override int PushMessage(COMMessage msg, string user)
        {
            throw new NotImplementedException();
        }

        protected override void HandleLostConnection(object sender, EventArgs e)
        {
            // Das ist voll OK, der Client hat nun einfach die Verbindung abgebaut
        }

        protected override void HandleIncommingConnection(Socket socket)
        {
            WorkerThread worker = new WorkerThread(socket);
            // 
            worker.LostConnection += LostConnection;
            worker.MessageReceived += MessageReceived;
            worker.Start();
        }

        protected override void HandleMessageReceived(object sender, MessageEventArgs e)
        {
            var result = 101;
            try
            {
                if (e.Message.AppID == 9)
                {
                    var buffer = e.Message.getBuffer();
                    var name = new byte[20];
                    for(int i=0;i<20;i++)
                        name[i] = buffer[i+4];
                    // 
                    string user = System.Text.ASCIIEncoding.ASCII.GetString(name, 0, name.Length);
                    user = user.Trim();

                    log.Info("ServerListener message for user[" + user + "]");

                    // Rest Der Nachricht ist encapsulated ...
                    byte[] payload = new byte[buffer.Length - 24];
                    for (int i = 0; i < buffer.Length - 24; i++)
                        payload[i] = buffer[i + 24];
                    COMMessage sendMsg = new COMMessage(payload);
                    // log.Info("AppID of Message:" + sendMsg.AppID);
                    if (sendMsg.AppID == 11)
                    {
                        OrderMessage order = new OrderMessage(payload);
                        log.Info("New Order to <" + user + "> " + order.FromName);
                    }
                    result = _clientListener.PushMessage(sendMsg, user);
                }
                else
                {
                    // OK, wir haben eine String Message
                    StringMessage msg = new StringMessage(e.Message.getBuffer());
                    // die ersten 20 Zeichen sind der Username:
                    string user = msg.Message.Substring(0, 20).Trim();
                    string str = msg.Message.Substring(20, msg.Message.Length - 20);

                    result = _clientListener.PushMessage(new StringMessage(str, e.Message.AppID), user);
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
            finally
            {
                var ret = (WorkerThread)sender;
                ret.PushMessage(new IntMessage(result));
            }
        }
    }
*/
}
