// #define SERVICE

using System;


namespace com.signalkontor.XDPushServer
{
    static class Program
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
/*            var msg = new Common.TPSMessage();
            msg.FTimeStamp = DateTime.Now;
            Console.WriteLine("\n\n\n\n\n\nJSON:");
            Console.WriteLine(msg.ToJson());
            Console.WriteLine("ENDE\n\n\n\n\n\n");
            Console.ReadLine();
            var x = Common.COMMessage.FromJson<Common.TPSMessage>(msg.ToJson());
            Console.ReadLine();*/

            //NameValueCollection appStgs = System.Configuration.ConfigurationManager.AppSettings;

            //System.IO.FileInfo fi = new System.IO.FileInfo(appStgs["logconfig"]);
            //if (!fi.Exists)
            //    throw new System.ApplicationException("Cannot find log4net configuration file at " + fi.FullName);

            //log4net.Config.DOMConfigurator.ConfigureAndWatch(fi);

#if SERVICE
            // log.Fatal("Starte Main()");
            ServiceBase[] ServicesToRun = new ServiceBase[] { new MainClass() };
            ServiceBase.Run(ServicesToRun);
#else
            MainClass mc = new MainClass();
            mc.RunConsole();
#endif
        }
    }
}