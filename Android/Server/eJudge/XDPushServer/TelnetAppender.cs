#region Copyright & License
//
// Copyright 2001-2005 The Apache Software Foundation
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
#endregion

using System;
using System.Net.Sockets;
using System.Net;
using com.signalkontor.Common;
using log4net.Core;
using System.Collections.Generic;
using AsynchronousCodeBlocks;

namespace com.signalkontor.Appender
{
    /// <summary>
    /// Appender writes to a Microsoft Message Queue
    /// </summary>
    /// <remarks>
    /// This appender sends log events via a specified MSMQ queue.
    /// The queue specified in the QueueName (e.g. .\Private$\log-test) must already exist on
    /// the source machine.
    /// The message label and body are rendered using separate layouts.
    /// </remarks>
    public class TelnetAppender : log4net.Appender.AppenderSkeleton
    {
        private System.Threading.Thread _thread;
        private int _port;
        private log4net.Layout.PatternLayout m_labelLayout;
        private List<Socket> _sockets;
        private XDQueue<string> _queue;

        public TelnetAppender()
        {
            _port = 9095;
            _sockets = new List<Socket>();
            _queue = new XDQueue<string>();
            _thread = new System.Threading.Thread(Listener);
            _thread.IsBackground = true;
            _thread.Name = "TelnetAppender Listener Thread";
            _thread.Start();
        }

        public int Port
        {
            get { return _port; }
            set { _port = value; }
        }


        public log4net.Layout.PatternLayout LabelLayout
        {
            get { return m_labelLayout; }
            set { m_labelLayout = value; }
        }

        protected void sendMessage(Socket socket, string message)
        {
            message += "\r\n";
            System.Text.UnicodeEncoding enc = new System.Text.UnicodeEncoding();
            byte[] b = enc.GetBytes(message);

            socket.Send(b);
        }

        protected void Listener()
        {
            while (true)
            {
                try
                {
                    Socket listenSocket = new Socket(AddressFamily.InterNetwork,
                         SocketType.Stream,
                         ProtocolType.Tcp);

                    // bind the listening socket to the port
                    IPAddress hostIP = (Dns.Resolve(IPAddress.Any.ToString())).AddressList[0];
                    IPEndPoint ep = new IPEndPoint(hostIP, _port);
                    listenSocket.Bind(ep);

                    while (true)
                    {

                        listenSocket.Listen(1024);
                        Socket mySocket = listenSocket.Accept();

                        sendMessage(mySocket, "Wellcome to XD Communication Server");

                        _sockets.Add(mySocket);

                    }
                }
                catch (Exception)
                {

                }
            }
        }

        protected void sendQueue()
        {

            List<Socket> blacklist = new List<Socket>();

            while (_queue.Count > 0)
            {
                string message = _queue.Dequeue();
                lock (_sockets)
                {
                    foreach (Socket socket in _sockets)
                    {
                        try
                        {
                            sendMessage(socket, message);
                        }
                        catch (Exception)
                        {
                            blacklist.Add(socket);
                        }
                    }

                    foreach (Socket socket in blacklist)
                    {
                        _sockets.Remove(socket);
                    }
                }
            }
        }

        override protected void Append(LoggingEvent loggingEvent) 
        {

            List<Socket> blacklist = new List<Socket>();

            if (_sockets.Count > 0)
            {
                string label = RenderLabel(loggingEvent);
                string message = base.RenderLoggingEvent(loggingEvent);

                // Wir senden nun an alle Sockets in der List
                _queue.Enqueue(message);

                new async(delegate { sendQueue(); }); 
            }
        }

        private string RenderLabel(LoggingEvent loggingEvent)
        {
            if (m_labelLayout == null)
            {
                return null;
            }

            System.IO.StringWriter writer = new System.IO.StringWriter();
            m_labelLayout.Format(writer, loggingEvent);

            return writer.ToString();
        }
    }
}
