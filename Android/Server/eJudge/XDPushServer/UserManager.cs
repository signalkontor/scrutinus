﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Linq;
using System.Runtime.CompilerServices;

namespace com.signalkontor.XDPushServer
{
    static class UserManager
    {
        private static readonly Dictionary<string, IPAddress> UsersIpAddresses = new Dictionary<string, IPAddress>();

        [MethodImpl(MethodImplOptions.Synchronized)]
        public static void Logon(string user, IPAddress ipAddress)
        {
            if(user == null)
                throw new ArgumentException("user must not be null");
            if(ipAddress == null)
                throw new ArgumentException("ipAddress must not be null");

            var old = UsersIpAddresses.SingleOrDefault(o => o.Value == ipAddress).Key;
            if (old != null)
            {
                UsersIpAddresses.Remove(old); // we don't want duplicate ip addresses
            }

            UsersIpAddresses[user] = ipAddress; 
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public static IPAddress GetAddress(string user)
        {
            if(!UsersIpAddresses.ContainsKey(user))
                throw new ArgumentException("unknown user");

            return UsersIpAddresses[user];
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public static string GetUser(IPAddress address)
        {
            return UsersIpAddresses.Single(o => o.Value == address).Key;
        }
   
    }
}
