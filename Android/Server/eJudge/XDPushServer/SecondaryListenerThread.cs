using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using com.signalkontor.Common;

namespace com.signalkontor.XDPushServer
{
    class SecondaryListenerThread : GenericListenerThread
    {
        const int ACKBUFSIZE = 150;

        public SecondaryListenerThread(int port) : base(port)
        {

        }

        public override int PushMessage(COMMessage msg, string user)
        {
            throw new NotImplementedException();
        }

        protected override void HandleLostConnection(object sender, EventArgs e)
        {
            // throw new Exception("The method or operation is not implemented.");
        }

        protected override void HandleMessageReceived(object sender, MessageEventArgs e)
        {
            // throw new Exception("The method or operation is not implemented.");
        }

        protected override void HandleIncommingConnection(Socket socket)
        {
            try
            {
                var xBuffer = new byte[ACKBUFSIZE];
                xBuffer[0] = 203;
                socket.Send(xBuffer);

                var buffer = new byte[2];

                //log.Info("Vor Receive");
                socket.ReceiveTimeout = 10000;
                var count = socket.Receive(buffer, 2, SocketFlags.None);
                if (count == 2)
                {
                    // read rest of message
                    var size = buffer[0] * 256 + buffer[1];
                    var read = new byte[size];

                    int readed = 0;
                    int loopCount = 0;
                    while (readed < (size - 2))
                    {
                        readed += socket.Receive(read, readed + 2, size - 2 - readed, SocketFlags.None);

                        loopCount++;
                        if (loopCount > 10)
                        {
                            throw new Exception("Timeout waiting for client in SecondaryListener");
                        }
                    }

                    // wir sollten eine LogonMesage bekommen haben
                    var cmsg = 
#if Android
                        COMMessage.FromJson<COMMessage>(read);
#else
                        new COMMessage(read);
#endif

                    if (cmsg.AppID == 1)
                    {
                        // Wir senden ein Ack zum Client
                        socket.Send(xBuffer);
                        var msg =
#if Android
                            COMMessage.FromJson<LogonMessage>(read);
#else
                            new LogonMessage(cmsg.getBuffer());
#endif
                        ListenerThread.instance.SetOutgoingSocket(msg.User, socket);

                    }
                    else
                    {
                        log.Fatal("Client hat in SecondaryListenerThread kein Login gesendet!");
                        socket.Close();
                    }
                }
            }
            catch (Exception e)
            {
                log.Error(e);
            }

        }
    }
}
