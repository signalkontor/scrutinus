using System;
using System.Collections.Generic;
using System.Text;

namespace com.signalkontor.StressTest
{
    public class Logger
    {
        private static object syncObject = new object();

        public static void logError(Exception e)
        {
            lock(syncObject)
            {
                System.IO.StreamWriter sw = new System.IO.StreamWriter("C:\\temp\\log\\StressTest.log", true);
                Exception ex = e;
                sw.WriteLine("--------------------------------------------");
                sw.WriteLine(e.Message);
                sw.WriteLine("=====>");
                while(ex != null)
                {
                    sw.WriteLine(ex.StackTrace);
                    ex = ex.InnerException;
                }
                sw.Close();    
            }
        }

    }
}
