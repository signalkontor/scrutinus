using System;
using System.Collections.Generic;
using System.Text;

namespace com.signalkontor.Common
{
    public class COMMessage
    {
        protected byte[] _buffer = null;
        protected int _appId = 0;
        protected byte[] _payload = null;


        public int AppID
        {
            get { return _appId; }
            set { _appId = value; }
        }

        public COMMessage()
        {

        }
        public COMMessage(int appId)
            : base()
        {
            _appId = appId;
        }
        public COMMessage(string str, int appId)
        {
            System.Text.ASCIIEncoding enc = new System.Text.ASCIIEncoding();
            _payload = enc.GetBytes(str);
            _appId = appId;
        }

        public COMMessage(byte[] buffer)
        {

            _buffer = buffer;
            _appId = buffer[2] * 256 + buffer[3];

            _payload = new byte[buffer.Length - 4];

            System.Array.Copy(buffer, 4,_payload, 0, buffer.Length - 4);

            ObjectSerializer ser = new ObjectSerializer();
            ser.deserialize(buffer, this);
        }

        protected virtual void setPayload() { }

        public byte[] getBuffer()
        {
/*
            setPayload();
            int size = _payload.Length + 2 + 2;

            _buffer = new byte[size];
            _payload.CopyTo(_buffer, 4);

            _buffer[0] = this.getHighByte(size);
            _buffer[1] = this.getLowByte(size);
            _buffer[2] = this.getHighByte(_appId);
            _buffer[3] = this.getLowByte(_appId);
 */

            if (_buffer != null)
            {
                return _buffer;
            }
            else
            {
                ObjectSerializer ser = new ObjectSerializer();
                return ser.serialize(this, this.AppID);
            }
            
        }

        public byte[] getSizeArray()
        {
            byte[] buffer = new byte[2];
            int size = _payload.Length + 2 + 2;
            buffer[0] = this.getHighByte(size);
            buffer[1] = this.getLowByte(size);

            return buffer;
        }


        protected byte getHighByte(int value)
        {
            int test1 = value >> 8;
            int test2 = value >> 4;
            return (byte) ((value & (65280)) >> 8);
        }

        protected byte getLowByte(int value)
        {
            return (byte)(value & 255);
        }

        public override string ToString()
        {
            return "\nApplication ID: " + _appId.ToString() + "\nPayload: " + ASCIIEncoding.ASCII.GetString(_payload, 0, _payload.Length) + "\n";
        }
    }
}
