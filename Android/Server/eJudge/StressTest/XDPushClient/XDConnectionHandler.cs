using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.Net;
using com.signalkontor.Common;
using TPS_eJudge.XDPushClient;
using System.Runtime.InteropServices;

namespace com.signalkontor.PushClient.objects
{

    public delegate void LostConnectionDelegate(object sender, EventArgs e);
    public delegate void ConnectionEstablishedDelegate(object sender, EventArgs e);
    public delegate void MessageReceivedDelegate(object sender, MessageEventArgs e);

    public struct SYSTEMTIME
    {
        public ushort wYear;
        public ushort wMonth;
        public ushort wDayOfWeek;
        public ushort wDay;
        public ushort wHour;
        public ushort wMinute;
        public ushort wSecond;
        public ushort wMilliseconds;

        /// <summary>
        /// Convert form System.DateTime
        /// </summary>
        /// <param name="time"></param>
        public void FromDateTime(DateTime time)
        {
            wYear = (ushort)time.Year;
            wMonth = (ushort)time.Month;
            wDayOfWeek = (ushort)time.DayOfWeek;
            wDay = (ushort)time.Day;
            wHour = (ushort)time.Hour;
            wMinute = (ushort)time.Minute;
            wSecond = (ushort)time.Second;
            wMilliseconds = (ushort)time.Millisecond;
        }

        /// <summary>
        /// Convert to System.DateTime
        /// </summary>
        /// <returns></returns>
        public DateTime ToDateTime()
        {
            return new DateTime(wYear, wMonth, wDay, wHour, wMinute, wSecond, wMilliseconds);
        }
        /// <summary>
        /// STATIC: Convert to System.DateTime
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        public static DateTime ToDateTime(SYSTEMTIME time)
        {
            return time.ToDateTime();
        }
    }


    public class MessageEventArgs : EventArgs
    {
        protected COMMessage _message;

        public MessageEventArgs(COMMessage message)
            : base()
        {
            this._message = message;
        }

        public COMMessage Message
        {
            get { return _message; }
            set { _message = value; }
        }
    }

    public class XDConnectionHandler
    {
        private string _server;
        private bool _running;
        private string _user;
        private string _password;

        const int ACKBUFSIZE = 150;
        private byte[] _ackBuffer;

        private Socket _outSocket;
        private Socket _inSocket;
        private int _port;
        private int _monitorTimer;
        private int _monitorCounter;
        private XDQueue _outQueue;
        private XDQueue _localQueue;
        private int _timeoutTicker;
        private bool _connected;

        // private System.Threading.Thread _thread;
        private System.Threading.Thread _monitor;
        private System.Threading.Thread _queueThread;

        private int _sequenceNumber = 0;

        private byte[] _receiveBuffer;

        public string User
        {
            get { return this._user; }
            set { _user = value; }
        }

        internal bool Connected
        {
            get { return _outSocket != null ? _outSocket.Connected : false; }
        }

        /// <summary>
        /// Wird gerufen, wenn ein VErbindungsabbruch erkannt wurde
        /// </summary>
        public event LostConnectionDelegate lostConnection;
        /// <summary>
        /// Wird gerufen, wenn eine Nachricht empfangen wurde
        /// </summary>
        public event MessageReceivedDelegate messageReceived;
        /// <summary>
        /// Wird gerufen, wenn erfolgreich eine Verbindung zum Server aufgebaut werden
        /// konnte und die Anwendung somit online ist
        /// </summary>
        public event ConnectionEstablishedDelegate connectionEstablished;
        /// <summary>
        /// Erzeugt ein Conection Manager Objekt und starte Arbeits-Threads
        /// </summary>
        /// <param name="server">IP Adresse des Servers</param>
        /// <param name="port">Port des Servers</param>
        /// <param name="user">das zu verwendene Username</param>
        /// <param name="password">Das zu verwendene Passwort</param>
        /// <param name="monitorTimer">Periodendauer des Monitor Threads in Millisekunden</param>
        public XDConnectionHandler(string server, int port, string user, string password, int monitorTimer)
        {
            _server = server;
            _user = user;
            _password = password;
            _outSocket = null;
            _inSocket = null;
            _port = port;
            _monitorTimer = monitorTimer;
            _running = true;
            _ackBuffer = new byte[ACKBUFSIZE];
            _ackBuffer[0] = 201;
            _connected = false;
            //_thread = new System.Threading.Thread(new System.Threading.ThreadStart(this.eventLoop));
            //_thread.Name = "EventLoop";
            //_thread.IsBackground = true;
            //_thread.Start();
            _outQueue = new XDQueue();
            _localQueue = new XDQueue();
            _monitor = new System.Threading.Thread(new System.Threading.ThreadStart(this.monitorThread));
            _monitor.IsBackground = true;
            _monitor.Start();

            _queueThread = new System.Threading.Thread(QueueSendThread);
            _queueThread.IsBackground = true;
            _queueThread.Start();

        }

        [DllImport("coredll.dll")]
        static extern bool SetLocalTime([In] ref SYSTEMTIME lpLocalTime);


        /// <summary>
        /// Entry Point des Monitor Threads.
        /// Dieser sendet alle xxx Millisekunden eine Ping Message an den Server
        /// Sollte dabei eine Exception geworfen werden, wird der Socket geschlossen
        /// </summary>
        private void monitorThread()
        {

            byte[] msg = new byte[4];
            msg[0] = 0;
            msg[1] = 4;
            msg[2] = 255;
            msg[3] = 255;

            int offlineCounter = 0;

            bool connected = connectToServer();

            while (_running)
            {

                try
                {
                    // Unsere Ping Nachricht
                    COMMessage msg2 = new COMMessage(msg);

                    //TPS_eJudge.AppInstance.Instance.logMessages(99, "Sende Ping");
                    if (sendMessage(msg2))
                    {
                        if (this.connectionEstablished != null)
                            connectionEstablished(this, new EventArgs());
                        //TPS_eJudge.AppInstance.Instance.logMessages(99, "Ping erfolgreich");
                    }
                    else
                    {
                        // Not connected ...
                        // wir schlie�en unsere Sockets
                        if (this.lostConnection != null) this.lostConnection(this, new EventArgs());
                        //TPS_eJudge.AppInstance.Instance.logMessages(99, "Ping nicht erfolgreich");


                        if (_outSocket != null)
                            _outSocket.Close();
                        if (_inSocket != null)
                            _inSocket.Close();

                        // Wir machen nun eine Schleife, bis wir wieder eine Verbindung haben
                        while (!connectToServer())
                        {
                            System.Threading.Thread.Sleep(2000);
                            offlineCounter++;
                            if (offlineCounter > 7) // = ca. 2 Minuten
                            {
                                offlineCounter = 0;
                            }
                        }

                        offlineCounter = 0;

                        // Hier sollten wir verbunden sein...
                        // Daher pr�fen, ob wir etwas in der OutQueue haben.
                        if (_outQueue.Count > 0)
                        {
                            sendQueue();
                        }
                    }

                    // Wir schlafen eine Runde ...
                    _monitorCounter = _monitorTimer / 1000;
                    while (_monitorCounter > 0)
                    {
                        System.Threading.Thread.Sleep(1000);
                        _monitorCounter--;
                    }

                }
                catch (Exception ex)
                {
                    // wir sind wohl nicht connected und m�ssen daher
                    // unsere Verbindungen pr�fen...
                    //TPS_eJudge.AppInstance.Instance.logMessages(0, ex);
                }

            }

            //TPS_eJudge.AppInstance.Instance.logMessages(0, "Monitor Thread beendet");
        }

 

        /// <summary>
        /// Der Queue Thread sendet alle Nachrichten, die sich in der ausgehenden Queue befinden 
        /// an den XD Server. Konnte die Nachricht nicht versendet werden, wird sie wieder in
        /// die Queue an gleicher Stelle zur�ck kopiert
        /// </summary>
        private void QueueSendThread()
        {

            int timer = 2000;

            while (_running)
            {
                try
                {
                    // F�r die Thread-Synchronisation mit dem UI Thread, der gegebenenfalls
                    // Nachrichten in die localQueue einstellt, werden die nachrichten aus der 
                    // localQueue zun�chst in die outQueue umkopiert.
                    lock (_localQueue)
                    {
                        while (_localQueue.Count > 0)
                        {
                            _outQueue.Enqueue(_localQueue.Dequeue());
                        }
                    }

                    if (_outQueue.Count > 0)
                    {
                        sendQueue();
                        if (_outQueue.Count > 0)
                        {
                            // Die Nachrichten konnten nicht gesendet werden, da keine 
                            // Verbindung besteht. Wir warten daher 10 Sek. und hoffen,
                            // dass wir dann eine Verbindung hin bekommen
                            timer = 10000;
                            System.Threading.Thread.Sleep(timer);
                        }
                    }
                    else
                    {
                        timer = 1000;
                        System.Threading.Thread.Sleep(timer);
                    }
                }
                catch (Exception e)
                {
                    if (e is System.Threading.ThreadAbortException)
                        return;
                }
            }
        }

        private void receiveData()
        {
            try
            {
                // read rest of message
                int size = _receiveBuffer[0] * 256 + _receiveBuffer[1];
               // TPS_eJudge.AppInstance.Instance.logMessages(99, "Empfange gr��e " + size);

                if (size == 0)
                {
                    //TPS_eJudge.AppInstance.Instance.logMessages(99, "Available: " + _inSocket.Available);
                    abortConnection();
                    return;
                }

                byte[] msg = new byte[size];

                int readed = 0;
                // Wir lesen nun den Rest der Nachricht ein.
                // Dies in einer Schleife, da die TCP Schicht uns m�glicherweise
                // weniger Bytes zur�ck gibt als wir angefordert haben, da noch
                // nicht die gesamte Nachricht empfangen war.
                int sek = 30;
                while (true)
                {
                    if (_inSocket == null)
                        throw new Exception("Socket closed while waiting for data");
                    if (_inSocket.Available > 1)
                        break;

                    sek--;

                    if (sek <= 0)
                        throw new Exception("Socket Timeout while waiting for data");

                    //TPS_eJudge.AppInstance.Instance.logMessages(99, "waiting for data: " + _inSocket.Available + " / " + (size - 2));
                    System.Threading.Thread.Sleep(1000);
                }
                // Die Daten sollten nun verf�gbar sein
                sek = 200;
                while (readed < (size - 2))
                {
                    //TPS_eJudge.AppInstance.Instance.logMessages(99, "Lese Daten");
                    readed += _inSocket.Receive(msg, readed + 2, size - 2 - readed, SocketFlags.None);
                    if (readed < (size - 2))
                    {
                        sek--;
                        if (sek == 0)
                            throw new Exception("Socket Timeout while reading data");
                        System.Threading.Thread.Sleep(100);
                    }
                }
                if ((msg[2] == 255) && (msg[3] == 255))
                {
                    //TPS_eJudge.AppInstance.Instance.logMessages(99, "Ping erhalten");
                }
                else
                {   // Verarbeitung der Nachricht
                    //TPS_eJudge.AppInstance.Instance.logMessages(99, "Verarbeite Nachricht");

                    COMMessage commsg = new COMMessage(msg);

                    switch (commsg.AppID)
                    {

                        case 120:
                            if (this.messageReceived != null)
                            {
                                this.messageReceived(this, new MessageEventArgs(commsg));
                            }
                            else
                            {
                                // Fehler: Kein Eventhandler aboniert, die Nachricht
                                // kann nicht zugestellt werden
                                //TPS_eJudge.AppInstance.Instance.logMessages(99, "Kein Abo auf Event");
                                // TPS_eJudge.XDPushClient.TMQeJSMsg tpsmsg2 = new TPS_eJudge.XDPushClient.TMQeJSMsg(500, "Alert: No Message Receiver registered");
                                // this.sendMessage(tpsmsg2);
                            }
                            break;
                        case 10: // TimeReferenceMessage
                            
                            break;

                        default:
                            //TPS_eJudge.AppInstance.Instance.logMessages(0, "Unknow AppID: " + commsg.AppID + "!!!");
                            break;
                    }

                } // else (kein ping)
                // wir senden noch ein Ack zur�ck an den Server
                //TPS_eJudge.AppInstance.Instance.logMessages(99, "Sende ACK");

                _inSocket.Send(_ackBuffer);

                //TPS_eJudge.AppInstance.Instance.logMessages(99, "Daten empfangen");
            }
            catch (Exception e)
            {
                //TPS_eJudge.AppInstance.Instance.logMessages(0, e);

                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);

                if (_outSocket != null)
                {
                    if (_outSocket.Connected)
                    {
                        try
                        {
                            TPS_eJudge.XDPushClient.TMQeJSMsg tpsmsg = new TPS_eJudge.XDPushClient.TMQeJSMsg(500, e.Message + ": " + e.StackTrace);
                            tpsmsg.FReceiver = "";
                            tpsmsg.FDoCmd = "";
                            tpsmsg.FReceiverIP = "";
                            tpsmsg.FSender = _user;
                            tpsmsg.FTimeStamp = DateTime.Now;
                            _outSocket.Send(tpsmsg.getBuffer());
                        }
                        catch (Exception e2)
                        {
                            Console.WriteLine(e2.Message);
                        }
                    }
                }

                System.Diagnostics.Debug.WriteLine(e.Message);
                abortConnection();
                if (this.lostConnection != null) this.lostConnection(this, new EventArgs());
            }
        }



 
        void ReceiveCallback(IAsyncResult res)
        {
            if (res.IsCompleted)
            {
                try
                {
                    receiveData();
                    if (_inSocket != null)
                        _inSocket.BeginReceive(_receiveBuffer, 0, 2, SocketFlags.None, ReceiveCallback, 1);
                }
                catch (Exception e)
                {
                    //TPS_eJudge.AppInstance.Instance.logMessages(0, e);
                }
            }
            else
            {
                // Beenden wir die Connection?
                try
                {
                    abortConnection();
                }
                catch (Exception ex)
                {
                    //TPS_eJudge.AppInstance.Instance.logMessages(99, ex);
                }
            }
        }

        private void waitForData(int size, Socket socket, int timeout)
        {
            int sek = timeout * 2;
            while (socket.Available < size)
            {
                System.Threading.Thread.Sleep(500);
                sek--;
                if (sek <= 0)
                    throw new Exception("Timeout while waiting for server data");
            }
        }

        /// <summary>
        /// Versucht eine Verbindung zum Server aufzubauen.
        /// Nach dem Socket-Connect wird ein Byte gelesen. Falls eine Exception geworfen
        /// wird, wird false zur�ck gegeben. Andernfalls wird angenommen, dass
        /// der Verbindungsaufbau erfolgreich war.
        /// </summary>
        /// <returns>true, wenn erfolgreich, andernfalls false</returns>
        private bool connectToServer()
        {

            try
            {
                _connected = false;
                _outSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
                IPAddress ip = IPAddress.Parse(_server);
                int iPortNo = _port;
                // Create the end point 
                IPEndPoint ipEnd = new IPEndPoint(ip, iPortNo);
                // Connect to the remote host
                //TPS_eJudge.AppInstance.Instance.logMessages(99, "Try Connect Out Socket");
                //_outSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive, 0);
                _outSocket.Connect(ipEnd);
                _outSocket.SetSocketOption(SocketOptionLevel.Tcp, SocketOptionName.NoDelay, 1);
                //Wir lesen einmal um zu pr�fen ob die Verbindung tats�chlich steht
                waitForData(ACKBUFSIZE, _outSocket, 15);
                int count = _outSocket.Receive(_ackBuffer, ACKBUFSIZE, SocketFlags.None);

                if (count != ACKBUFSIZE)
                    return false;

                LogonMessage msg = new LogonMessage(_user, _password);
                int i = _outSocket.Send(msg.getBuffer());

                waitForData(ACKBUFSIZE, _outSocket, 15);
                count = _outSocket.Receive(_ackBuffer, ACKBUFSIZE, SocketFlags.None);
                if (count != ACKBUFSIZE)
                {
                    //TPS_eJudge.AppInstance.Instance.logMessages(99, "Fehler: count war 0 bei _outSocket");
                    return false;
                }
                _inSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
                ipEnd = new IPEndPoint(ip, iPortNo + 1);
                //TPS_eJudge.AppInstance.Instance.logMessages(99, "Try Connect In Socket");
                //_inSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive, true);
                _inSocket.Connect(ipEnd);
                _inSocket.SetSocketOption(SocketOptionLevel.Tcp, SocketOptionName.NoDelay, 1);
                //TPS_eJudge.AppInstance.Instance.logMessages(99, "Sockets sind verbunden");

                waitForData(ACKBUFSIZE, _inSocket, 15);
                count = _inSocket.Receive(_ackBuffer, ACKBUFSIZE, SocketFlags.None);
                //TPS_eJudge.AppInstance.Instance.logMessages(99, "Server sendet: " + _ackBuffer[0]);
                if (count != ACKBUFSIZE)
                {
                    //TPS_eJudge.AppInstance.Instance.logMessages(99, "Fehler: count war " + count + " bei _inSocket");
                    return false;
                }
                i = _inSocket.Send(msg.getBuffer());

                waitForData(ACKBUFSIZE, _inSocket, 15);
                count = _inSocket.Receive(_ackBuffer, ACKBUFSIZE, SocketFlags.None);
                //TPS_eJudge.AppInstance.Instance.logMessages(99, "Server sendet: " + _ackBuffer[0]);
                if (count == ACKBUFSIZE)
                {
                    // Wir benachrichtigen die Anwendung, dass wir online sind.
                    connectionEstablished(this, new EventArgs());
                    //TPS_eJudge.AppInstance.Instance.logMessages(99, "Verbindung hergestellt");
                }
                else
                {
                    //TPS_eJudge.AppInstance.Instance.logMessages(99, "Verbindung nicht hergestellt, count != 1");
                    _inSocket.Close();
                    _outSocket.Close();
                    _inSocket = null;
                    _outSocket = null;

                    return false;
                }
                _receiveBuffer = new byte[2];
                _inSocket.BeginReceive(_receiveBuffer, 0, 2, SocketFlags.None, ReceiveCallback, 1);

            }
            catch (Exception e)
            {   // Thread Abort wird weiter gereicht,
                // damit wir uns hier sauber beenden k�nnen
                if (e is System.Threading.ThreadAbortException)
                {
                    throw e;
                }

                //TPS_eJudge.AppInstance.Instance.logMessages(0, e);
                System.Diagnostics.Debug.WriteLine(e.Message);
                System.Diagnostics.Debug.WriteLine(e.StackTrace);
                if (_outSocket != null)
                    _outSocket.Close();
                if (_inSocket != null)
                    _inSocket.Close();

                _inSocket = null;
                _outSocket = null;
                return false;
            }

            _connected = true;
            return true;
        }
        /// <summary>
        /// Sendet eine Nachricht an den Server. 
        /// Die Nachricht wird zun�chst in die interne Queue eingestellt
        /// Der Queue Thread liest diese dann aus und versendet die Nachrichten
        /// </summary>
        /// <param name="message">Die Nachrich, die zu versenden ist</param>
        public void sendMessage(TPS_eJudge.XDPushClient.TMQeJSMsg message)
        {
            // Wir f�llen noch die unbekannten Felder aus
            message.FReceiverIP = _server;
            message.FReceiver = _server;
            message.FSender = _user;
            message.FResponseId = _sequenceNumber;
            message.FDoCmd = "10";

            _sequenceNumber++;

            if (_sequenceNumber == Int32.MaxValue)
                _sequenceNumber = 0;
            // message.FDoCmd = TPS_eJudge.AppInstance.Instance.TournamentID;

            if (message.FDoCmd == null)
                message.FDoCmd = "";
            if (message.FMsgData == null)
                message.FMsgData = "";

            lock (_localQueue)
            {
                _localQueue.Enqueue(message);
            }
        }

        private void abortConnection()
        {
            try
            {
                if (_connected == false)
                    return; // Wir sind eh nicht verbunden, also wird hier auch nichts abgebrochen

                _connected = false;

                if (_outSocket != null)
                {
                    _outSocket.Close();
                    _outSocket = null;
                }

                if (_inSocket != null)
                {
                    _inSocket.Close();
                    _inSocket = null;
                }


                if (this.lostConnection != null) this.lostConnection(this, new EventArgs());

            }
            catch (Exception e)
            {
                //TPS_eJudge.AppInstance.Instance.logMessages(0, e);
            }

            // Falls der Monitor wartet, sollte er nun gleich
            // ein Ping senden und dann die Verbindung neu aufbauen
            _monitorCounter = 0;
        }



        /// <summary>
        /// Versendet eine einzelne Nachricht an den Server.
        /// </summary>
        /// <param name="msg">Nachricht, die versendet werden soll</param>
        /// <returns>true, wenn Nachricht erfolgreich versendet wurde, sonst false</returns>
        private bool sendMessage(COMMessage msg)
        {

            if (_outSocket == null)
                return false;

            try
            {
                lock (_outSocket)
                {
                    //TPS_eJudge.AppInstance.Instance.logMessages(99, "Sende Daten sendMessage");
                    _outSocket.Send(msg.getBuffer());
                    //TPS_eJudge.AppInstance.Instance.logMessages(99, "Daten gesendet");
                    // wir lesen nun den ACKBUFFER ein
                    _timeoutTicker = 150;
                    while (_outSocket.Available == 0)
                    {
                        _timeoutTicker--;
                        if (_timeoutTicker <= 0)
                        {
                            //TPS_eJudge.AppInstance.Instance.logMessages(99, "Timeout beim warten auf Antwort");
                            return false;
                        }
                        System.Threading.Thread.Sleep(100);
                    }

                    int i = 0;
                    _timeoutTicker = 150;

                    while (i < ACKBUFSIZE)
                    {
                        i += _outSocket.Receive(_ackBuffer, i, ACKBUFSIZE - i, SocketFlags.None);
                        if (i < ACKBUFSIZE)
                        {
                            _timeoutTicker--;
                            if (_timeoutTicker <= 0)
                            {
                                //TPS_eJudge.AppInstance.Instance.logMessages(99, "Timeout beim warten auf Antwort");
                                return false;
                            }

                            System.Threading.Thread.Sleep(100);
                        }
                    }

                    //TPS_eJudge.AppInstance.Instance.logMessages(99, "Best�tigung erhalten");

                    return true;
                }
            }
            catch (Exception e)
            {
                //TPS_eJudge.AppInstance.Instance.logMessages(99, e);
                abortConnection();

                return false;
            }
        }

        /// <summary>
        /// Versucht alle Elemente, die sich in der _outQueue befinden zu versenden
        /// Gelingt dies nicht, wird das Element wieder in de Queue eingestellt
        /// </summary>
        private void sendQueue()
        {
            COMMessage msg = null;
            byte[] xBuffer = new byte[1];
            try
            {
                lock (_outQueue)
                {
                    while (_outQueue.Count > 0)
                    {
                        // Wir holen uns die erste Nachricht per Peak
                        msg = _outQueue.Peak();
                        if (sendMessage(msg))
                        {
                            // Senden war erfolgreich, wir entnehmen die Nachricht
                            _outQueue.Remove(msg);
                        }
                        else
                        {   // Fehler beim Senden, wir brechen hier ab.
                            return;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                //TPS_eJudge.AppInstance.Instance.logMessages(99, e);
            }
        }


        /// <summary>
        /// Beendet alle Threads und schlie�t die VErbindung zum Server
        /// </summary>
        public void Stop()
        {
            _running = false;
            Disconnect();
            if (_monitor != null)
                _monitor.Abort();
            if (_queueThread != null)
                _queueThread.Abort();
        }
        /// <summary>
        /// Schlie�t die Verbindung zum Server und beendet alle Threads.
        /// </summary>
        public void Disconnect()
        {
            _running = false;
            try
            {
                _inSocket.Close();
                _outSocket.Close();
            }
            catch (Exception)
            {
            }
        }
        /// <summary>
        /// Dies ist Testcode um das Verhalten bei Verbindunsabbruch testen zu k�nnen
        /// </summary>
        public void CrashConnection()
        {
            _inSocket.Close();
            _outSocket.Close();
        }
    }
}
