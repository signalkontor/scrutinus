using System;
using System.Collections.Generic;
using System.Text;
using com.signalkontor.Common;

namespace TPS_eJudge.XDPushClient
{
    public class TMQeJSMsg : COMMessage
    {
        public DateTime FTimeStamp;    // Lokaler Timestamp beim Erstellen der Message
        public string FSender;         // Sender der Nachricht
        public string FReceiver;       // Adresse des Empfängers
        public string FReceiverIP;
        public int FTargetId;          // Art der Nachricht
        public int FResponseId;        // Im Falle einer Antwort die TargetId der empfangenen Nachricht,
        // sonst 0
        public string FDoCmd;          // Auszuführendes Kommando
        public string FMsgData;        // Die eigentliche Nachricht

        public TMQeJSMsg(byte[] data)
        {
            FTimeStamp = DateTime.Now;
            FDoCmd = "";
            FMsgData = "";
            FSender = "";
            // Sicherheitshalber die APP-ID setzen
            _appId = 120;
            com.signalkontor.Common.ObjectSerializer ser = new ObjectSerializer();
            ser.deserialize(data, this);
        }

        public TMQeJSMsg(int target, string data) : base(120)
        {

            FTimeStamp = DateTime.Now;
            FDoCmd = "";
            FMsgData = "";
            FSender = "";

            FTargetId = target;
            FMsgData = data;
        }

        public TMQeJSMsg()
            : base(120)
        {
            FTimeStamp = DateTime.Now;
            FDoCmd = "";
            FMsgData = "";
            FSender = "";
        
        }


    }
}
