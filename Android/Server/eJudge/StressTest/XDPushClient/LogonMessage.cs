using System;
using System.Collections.Generic;
using System.Text;

namespace com.signalkontor.Common
{
    class LogonMessage : COMMessage
    {

        private string _user;
        private string _password;

        public string User
        {
            get { return _user; }
            set { _user = value;}
        }

        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }

        public LogonMessage(byte[] buffer) : base(buffer)
        {

        }

        public LogonMessage(string user, string password) : base(1)
        {
            _user = user;
            _password = password;
        }

    }
}
