using System;
using System.Collections.Generic;
using System.Text;

namespace com.signalkontor.Common
{
    public class SerializeableObject
    {
        private int _appID;

        public int AppID
        {
            get { return _appID; }
            set { _appID = value; }
        }

        public SerializeableObject(int appID)
        {
            _appID = appID;
        }

        public SerializeableObject(byte[] buffer)
        {
            ObjectSerializer ser = new ObjectSerializer();
            ser.deserialize(buffer, this);
        }

        public byte[] getBytes()
        {
            ObjectSerializer ser = new ObjectSerializer();
            return ser.serialize(this, _appID);
        }
    }
}
