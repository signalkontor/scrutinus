using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Collections;
using System.Globalization;

namespace com.signalkontor.Common
{
    class ObjectSerializer
    {
        int _bufferSize;
        byte[] _buffer;
        int _index;
        bool _write = false;
        CultureInfo _ci;
            
        public ObjectSerializer()
        {
            _buffer = new byte[1024];
            _bufferSize = 1024;
            _index = 4;
            _ci = new CultureInfo("de-DE");
        }

        protected void addToBuffer(object obj, object target)
        {

            if (obj == null)
            {
                obj = "";
            }

            byte[] buffer = null;

            Type type = obj.GetType();

            
            if (obj is int)
            {
                buffer = getByte((int) obj);
            }
            if (obj is double)
            {
                buffer = getByte((double)obj);
            }
            if (obj is string)
            {
                buffer = getByte((string)obj);
            }
            if (obj is DateTime)
            {
                buffer = getByte((DateTime)obj);
            }

            // wir pr�fen, ob wir noch genug platz haben
            if ((_bufferSize - _index) <= buffer.Length)
            {
                byte[] newArray = new byte[_index + buffer.Length + 1];
                _buffer.CopyTo(newArray, 0);
                _buffer = newArray;
            }

            buffer.CopyTo(_buffer, _index);
            _index += buffer.Length;
            
        }


        public byte[] serialize(object obj, int AppID)
        {
            PropertyInfo[] props = obj.GetType().GetProperties();
            foreach (PropertyInfo prop in props)
            {
                if (prop.CanRead && prop.CanWrite)
                {
                    if(prop.Name != "AppID")
                        addToBuffer(prop.GetValue(obj, null), obj);
                }
            }

            FieldInfo[] fields = obj.GetType().GetFields();
            foreach (FieldInfo info in fields)
            {
                if (info.IsPublic)
                {
                    addToBuffer(info.GetValue(obj), obj);
                }
            }

            byte[] ret = new byte[_index];
            for (int i = 0; i < _index; i++)
                ret[i] = _buffer[i];

            ret[0] = getHighByte(ret.Length);
            ret[1] = getLowByte(ret.Length);
            ret[2] = getHighByte(AppID);
            ret[3] = getLowByte(AppID);

            return ret;
        }

        public void setValue(PropertyInfo info, object target)
        {
            if (info.PropertyType == typeof(int))
            {
                info.SetValue(target, getInt(_index), null);
            }
            if (info.PropertyType == typeof(string))
            {
                info.SetValue(target, getString(_index), null);
            }
            if (info.PropertyType == typeof(double))
            {
                info.SetValue(target, getDouble(_index), null);
            }
            if (info.PropertyType == typeof(DateTime))
            {
                info.SetValue(target, getDateTime(_index), null);
            }            
        }
        public void setValue(FieldInfo info, object target)
        {
            if (info.FieldType == typeof(int))
            {
                info.SetValue(target, getInt(_index));
            }
            if (info.FieldType == typeof(string))
            {
                info.SetValue(target, getString(_index));
            }
            if (info.FieldType == typeof(double))
            {
                info.SetValue(target, getDouble(_index));
            }
            if (info.FieldType == typeof(DateTime))
            {
                info.SetValue(target, getDateTime(_index));
            }
        }
        public object deserialize(byte[] buffer, object obj)
        {
            _buffer = buffer;
            _index = 4;

            PropertyInfo[] props = obj.GetType().GetProperties();
            foreach (PropertyInfo prop in props)
            {
                if (prop.CanRead && prop.CanWrite)
                {
                    if(prop.Name != "AppID")
                        setValue(prop, obj);
                }
            }

            FieldInfo[] fields = obj.GetType().GetFields();
            foreach (FieldInfo info in fields)
            {
                if (info.IsPublic)
                {
                    if (info.Name == "FMsgData")
                        _write = true;
                    else
                        _write = false;

                    setValue(info, obj);
                }
            }
            return obj;
        }

        protected byte[] getByte(string val)
        {
            System.Text.UnicodeEncoding enc = new System.Text.UnicodeEncoding();
            byte[] b = enc.GetBytes(val);
            byte[] ret = new byte[b.Length + 1];
            ret[b.Length] = 0;
            b.CopyTo(ret, 0);
            return ret;
        }

        protected byte[] getByte(int val)
        {
            byte[] ret = new byte[2];
            ret[0] = getHighByte(val);
            ret[1] = getLowByte(val);
            return ret;
        }

        protected byte[] getByte(double val)
        {
            return getByte(val.ToString(_ci));
        }

        protected byte[] getByte(DateTime val)
        {   
            // set the output to be
            // a proper German format

            return getByte(val.ToString(_ci));
        }

        protected byte getHighByte(int value)
        {
            return (byte)((value & (65280)) >> 8);
        }

        protected byte getLowByte(int value)
        {
            return (byte)(value & 255);
        }

        protected int getInt(int index)
        {
            _index += 2;
            return _buffer[index] *256 + _buffer[index+1];
        }

        protected double getDouble(int index)
        {
            string str = getString(index);

            return Double.Parse(str, _ci);
        }

        protected string getString(int index)
        {
            int i;
            for (i = index; i < _buffer.Length; i += 2)
            {
                if (_buffer[i] == 0)
                    break;
            }

            int count = i - index;

            byte[] strbuf = new byte[count];
            for (int j = 0; j < count; j++)
                strbuf[j] = _buffer[j + index];

            // Wir speichern mal die bytes vom strbuf
            /*
            if (_write)
            {
                System.IO.StreamWriter w = new System.IO.StreamWriter("debug.bin");
                System.IO.BinaryWriter br = new System.IO.BinaryWriter(w.BaseStream);
                br.Write(_buffer);
                w.Close();
            }
            */
            System.Text.UnicodeEncoding enc = new System.Text.UnicodeEncoding();
            string str = enc.GetString(strbuf, 0, strbuf.Length);

            _index = i+1;

            return str;
        }

        protected DateTime getDateTime(int index)
        {
            string str = getString(index);
            return DateTime.Parse(str, _ci);
        }
    }
}
