using System;
using System.Collections.Generic;
using System.Text;

namespace com.signalkontor.Common
{
    public class StringMessage : COMMessage
    {

        protected string _message = null;

        public string Message
        {
            get { return _message; }
            set { _message = value; setPayload();}
        }

        public StringMessage(byte[] buffer)
            : base(buffer)
        {
            // this.message = new string(AdressOf(_payload), 0, _payload.Length);
            _message = System.Text.ASCIIEncoding.ASCII.GetString(_payload, 0, _payload.Length);
        }

        public StringMessage(string message, int appId) : base(message, appId)
        {
            _message = message;
            _appId = appId;
        }

        protected override void setPayload()
        {
            System.Text.ASCIIEncoding enc = new System.Text.ASCIIEncoding();
            _payload = enc.GetBytes(_message);
        }
    }
}
