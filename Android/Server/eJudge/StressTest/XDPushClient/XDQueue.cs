using System;
using System.Collections.Generic;
using System.Text;
using com.signalkontor.Common;

namespace TPS_eJudge.XDPushClient
{
    class XDQueue
    {
        List<COMMessage> _buffer;

        public XDQueue()
        {
            _buffer = new List<COMMessage>();
        }

        public int Count
        {
            get
            {
                lock (_buffer)
                {
                    return _buffer.Count;
                }
            }
        }

        public void Enqueue(COMMessage msg)
        {
            lock (_buffer)
            {
                _buffer.Add(msg);
            }
        }

        public COMMessage Peak()
        {
            lock (_buffer)
            {
                if (_buffer.Count > 0)
                    return _buffer[0];
                else
                    return null;
            }
        }

        public void Remove(COMMessage msg)
        {
            lock (_buffer)
            {
                _buffer.Remove(msg);
            }
        }

        public COMMessage Dequeue()
        {
            lock (_buffer)
            {
                if (_buffer.Count > 0)
                {
                    COMMessage msg = _buffer[0];
                    _buffer.RemoveAt(0);
                    return msg;
                }
                else
                    return null;
            }
        }
    }
}
