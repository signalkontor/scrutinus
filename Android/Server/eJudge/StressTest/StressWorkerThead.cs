using System;
using System.Collections.Generic;
using System.Text;

namespace com.signalkontor.StressTest
{
    class StressWorkerThead
    {
        com.signalkontor.PushClient.objects.XDConnectionHandler _handler;
        System.Threading.Thread _thread;
        int _count;
        string _user;
        bool _running;

        public StressWorkerThead(string user)
        {
            _handler = new PushClient.objects.XDConnectionHandler("127.0.0.1", 9090, user, "", 10000);
            _handler.connectionEstablished += HandlerConnectionEstablished;
            _handler.lostConnection += HandlerLostConnection;
            _handler.messageReceived += HandlerMessageReceived;
            _user = user;
        }

        static void HandlerMessageReceived(object sender, com.signalkontor.PushClient.objects.MessageEventArgs e)
        {
            Console.WriteLine("Message Received: " + e.Message.ToString());         
        }

        void HandlerConnectionEstablished(object sender, EventArgs e)
        {
            Console.WriteLine("user [" + _user + "] : " + _count + " connected");

            if (_running == false)
            {
                _thread = new System.Threading.Thread(Run);
                _running = true;
                _thread.Start();
            }
        }

        private void Run()
        {

            while (_running)
            {
                var msg = new TPS_eJudge.XDPushClient.TMQeJSMsg
                              {
                                  AppID = 120,
                                  FDoCmd = "siodsodsidjaodjsdjsdjoasjd siodsodsidjaodjsdjsdjoasjd",
                                  FMsgData = "sjdhkjfdfhdsfhdkcfhdskjf jfsdkfhsdj siodsodsidjaodjsdjsdjoasjd",
                                  FReceiver = "",
                                  FReceiverIP = "",
                                  FResponseId = 0,
                                  FSender = _user,
                                  FTargetId = 3,
                                  FTimeStamp = DateTime.Now
                              };

                _handler.sendMessage(msg);

                // System.Threading.Thread.Sleep(1000);

                _count++;

                if(_count == 10000)
                {
                    _count = 0;
                    // _handler.CrashConnection();
                    break;
                }

                if (_count % 100 == 0)
                {
                    Console.WriteLine("Count: " + _count);
                }
            }

            Console.WriteLine("Stress-Thread exits");
            Console.ReadLine();
        }

        void HandlerLostConnection(object sender, EventArgs e)
        {
            System.Console.WriteLine("[" + _user + "] lost connection");
        }
    }
}
