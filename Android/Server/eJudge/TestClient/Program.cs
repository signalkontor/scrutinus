﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using com.signalkontor.Common;

namespace TestClient
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.ReadLine();
            var thread = new ListenerThread(9100);
            
            thread.SendMessages();

            Console.ReadLine();
        }
    }
}
