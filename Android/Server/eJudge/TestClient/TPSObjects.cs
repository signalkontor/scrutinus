﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace com.signalkontor.PushClient.objects
    {
        public class TMQeJSMsg
        {
            public DateTime TimeStamp;    // Lokaler Timestamp beim Erstellen der Message
            public string Sender;         // Sender der Nachricht
            public string Receiver;       // Adresse des Empfängers
            public string ReceiverIP;     // IP Adresse Empfänger wird nicht mehr genutzt
            public int TargetId;          // Art der Nachricht
            public int ResponseId;        // Im Falle einer Antwort die TargetId der empfangenen Nachricht,
            // sonst 0
            public string DoCmd;          // Auszuführendes Kommando
            public string MsgData;        // Die eigentliche Nachricht

        }
    }

