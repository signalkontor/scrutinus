﻿using System;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.signalkontor.Common;
using System.Configuration;

namespace TestClient
{

    public interface IListenerThread
    {
        int SendMessage(COMMessage msg, NetworkStream stream);
    }


    public class MessageEventArgs : EventArgs
    {
        public MessageEventArgs(int appId, byte[] data)
        {
            switch (appId)
            {
                case 1:
                    Message = COMMessage.FromJson<LogonMessage>(data);
                    break;
                case 2:
                    Message = COMMessage.FromJson<StringMessage>(data);
                    break;
                case 120:
                    Message = COMMessage.FromJson<TPSMessage>(data);
                    break;
            }
        }

        public MessageEventArgs(COMMessage message)
        {
            Message = message;
        }

        public string User { get; set; }
        public COMMessage Message { get; set; }
    }


    public delegate void MessageReceivedDelegate(object sender, MessageEventArgs e);

    class ListenerThread : IListenerThread
    {
        private static readonly Encoding Encoding = Encoding.UTF8;

        private readonly int _port;
        private readonly TcpListener _tcpListener;

        public ListenerThread(int port)
        {
            _port = port;
            
            var ipAddress = NetworkInterface.GetAllNetworkInterfaces()
                .SelectMany(ni => ni.GetIPProperties().UnicastAddresses)
                .Where(ua => ua.Address.AddressFamily == AddressFamily.InterNetwork && !IPAddress.IsLoopback(ua.Address))
                .Select(ua => ua.Address)
                .First();

            _tcpListener = new TcpListener(ipAddress, port);
        }

        public void Start()
        {
            while (true)
            {
                var tcpClient = _tcpListener.AcceptTcpClient();
                new Task(obj => ReceiveMessage(obj as TcpClient), tcpClient).Start();
            }
        }

        public void Stop()
        {
            lock (_tcpListener)
            {
                _tcpListener.Stop();
            }
        }

        public void LogOn()
        {
            com.signalkontor.Common.LogonMessage logon = new LogonMessage("eJS001", "");
            //this.SendMessage(logon, "eJS001");
        }

        private static void ReceiveMessage(TcpClient tcpClient)
        {
            try
            {
                using (var stream = tcpClient.GetStream())
                {
                    stream.ReadTimeout = 10000;

                    var pos = 0;
                    var lenBuffer = new byte[4];
                    stream.Read(lenBuffer, 0, 4);
                    int dataSize = BitConverter.ToInt32(lenBuffer, 0);
                    var buffer = new byte[dataSize];

                    while (pos < buffer.Length)
                    {
                        var bytesRead = stream.Read(buffer, pos, buffer.Length - pos);
                        if (bytesRead == -1) break;
                        pos += bytesRead;
                    }

                    if (buffer.Length == 1)
                    {
                        // Das ist ein Ping, wir senden unser OK zurück und fertif
                        stream.Write(BitConverter.GetBytes(true), 0, 1);
                        tcpClient.Close();
                    }

                    var genericMessage = COMMessage.FromJson<COMMessage>(buffer);
                    var tmqeJsMsg = new com.signalkontor.PushClient.objects.TMQeJSMsg();
                    switch (genericMessage.AppID)
                    {
                        case 1: // logon message
                            var logonMessage = COMMessage.FromJson<LogonMessage>(buffer);
                            tmqeJsMsg.TargetId = 2; // User Logon
                            tmqeJsMsg.DoCmd = "";
                            tmqeJsMsg.MsgData = logonMessage.User;
                            tmqeJsMsg.TimeStamp = DateTime.Now;
                            tmqeJsMsg.Sender = logonMessage.User;
                            tmqeJsMsg.Receiver = "";
                            tmqeJsMsg.ReceiverIP = "";
                            IPEndPoint end = (IPEndPoint) tcpClient.Client.RemoteEndPoint;
                            
                            break;
                        case 2: // string message
                            var stringMessage = COMMessage.FromJson<StringMessage>(buffer);
                            tmqeJsMsg.TargetId = 3; // User Logoff
                            tmqeJsMsg.DoCmd = "";
                            tmqeJsMsg.MsgData = stringMessage.Message;
                            tmqeJsMsg.TimeStamp = DateTime.Now;
                            tmqeJsMsg.Sender = stringMessage.Message;
                            break;

                        case 120: // tps message
                            var tpsMessage = COMMessage.FromJson<TPSMessage>(buffer);
                            tmqeJsMsg.DoCmd = tpsMessage.FDoCmd;
                            tmqeJsMsg.MsgData = tpsMessage.FMsgData;
                            tmqeJsMsg.ResponseId = tpsMessage.FResponseId;
                            tmqeJsMsg.Sender = tpsMessage.FSender;
                            tmqeJsMsg.TargetId = tpsMessage.FTargetId;
                            tmqeJsMsg.TimeStamp = tpsMessage.FTimeStamp;
                            tmqeJsMsg.ReceiverIP = tpsMessage.FReceiverIP;
                            tmqeJsMsg.Receiver = tpsMessage.FReceiver;
                            break;

                        default:
                            tmqeJsMsg.DoCmd = "";
                            tmqeJsMsg.MsgData = "unknown Message: " + genericMessage.AppID;
                            tmqeJsMsg.Receiver = "";
                            tmqeJsMsg.ReceiverIP = "";
                            tmqeJsMsg.ResponseId = 0;
                            tmqeJsMsg.Sender = null; // will be set later
                            tmqeJsMsg.TargetId = 500;
                            tmqeJsMsg.TimeStamp = DateTime.Now;
                            break;
                    }

                    if (tmqeJsMsg.Sender == null)
                    {
                        var endpoint = tcpClient.Client.RemoteEndPoint;
                        string user = null;
                        if (endpoint is IPEndPoint)
                        {
                           
                        }
                        tmqeJsMsg.Sender = user ?? "";
                    }

                    // Quittung senden ...
                    stream.Write(BitConverter.GetBytes(true), 0, 1);

                }
            }
            catch { }
            finally
            {
                tcpClient.Close();
            }
        }

        public void SendMessages()
        {
            var tcpClient = new TcpClient();
            var address = new IPAddress(new byte[] { 127,0,0,1 });
            Console.WriteLine("Verbindugnsaufbau...");
            tcpClient.Connect(address, 9090);
            var stream = tcpClient.GetStream();
            
            var tpsMessage = new TPSMessage(120, "Hello world, dies soll mal ein etws längerer text werden, damit die Nachricht etwas größer wird als geplant");
            tpsMessage.FDoCmd = "";
            tpsMessage.FTimeStamp = DateTime.Now;
            tpsMessage.FMsgData = "Hello world";
            tpsMessage.FReceiver = "";
            tpsMessage.FSender = "Ich";
            for (int i = 0; i < 100;i++ )
                SendMessage(tpsMessage, stream);
        }

        public int SendMessage(COMMessage msg, NetworkStream stream)
        {

            try
            {
                    // Timeout zur Sicherheit setzen
                    stream.WriteTimeout = 5000; // Länger als 5 Sekunden darf das nicht dauern
                    var str = msg.ToJson();
                    var bytes = Encoding.GetBytes(msg.ToJson());
                    // wir senden nun die Länge unserer Daten
                    byte[] len = BitConverter.GetBytes(bytes.Length);
                    stream.Write(len, 0, len.Length);
                    stream.Write(bytes, 0, bytes.Length);
                    stream.Flush();
                    Console.WriteLine("Nachricht gesendet");
                    // -1 == no acknowlege byte sent => user is offline
                    if (stream.ReadByte() == -1)
                    {
                        Console.WriteLine("Fehler beim Lesen der Quittung");
                        return 1;
                    }
                    else
                    {
                        Console.WriteLine("Erfolgreich gesendet");
                    }
            }
            catch(Exception ex)
            {
                // something went wrong => assume message did not reach the recipient
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(ex.Message);
                Console.ResetColor();
                return 1;
            }
            finally
            {
                
            }
            // no errors, got acknowledge byte => all ok
            Console.WriteLine("Fertig mit senden");
            return 0;
        }
    }
}
