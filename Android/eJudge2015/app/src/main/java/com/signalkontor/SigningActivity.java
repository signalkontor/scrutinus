/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.signalkontor;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.signalkontor.constants.Constants;
import com.signalkontor.data.Tournament;
import com.signalkontor.messaging.MessageManager;
import com.signalkontor.messaging.TPSMessage;
import com.signalkontor.widget.SignatureView;

import java.io.File;
import java.io.FileOutputStream;

/**
 * Demonstrates the handling of touch screen and trackball events to
 * implement a simple painting app.
 */
public class SigningActivity extends Activity implements View.OnClickListener {
    private static float scale;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Create and attach the view that is responsible for painting.
        setContentView(R.layout.signature);

        final SignatureView signatureView = (SignatureView)findViewById(R.id.signatureView);
        final int signatureHeight = (int) (getResources().getDisplayMetrics().widthPixels / 1.8);
        scale = 120.0f / signatureHeight;

        signatureView.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, signatureHeight));
        signatureView.requestFocus();

        findViewById(R.id.signatureClearButton).setOnClickListener(this);
        findViewById(R.id.signatureContinueButton).setOnClickListener(this);

        TextView textview = (TextView)(findViewById(R.id.checksumView));

        try {
            int checksum = Tournament.getInstance().getChecksum();
            textview.setText(checksum + "");
            writeChecksumToFile(checksum, Tournament.getInstance().getTournamentId(), Tournament.getInstance().getJudgeId());
        }catch(Exception ex)
        {
            Toast.makeText(this, "An error occurred while creating checksum: " + ex.getMessage(), Toast.LENGTH_LONG);
        }
        

    }

    private void writeChecksumToFile(int checksum, String tournamentId, String judgeSign) {
        if(isExternalStorageWritable()){
            try
            {
                String documentFolder = Environment.getExternalStorageDirectory() + "/Documents";

                try {
                    // Check whether this app has write external storage permission or not.
                    int writeExternalStoragePermission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
                    // If do not grant write external storage permission.
                    if (writeExternalStoragePermission != PackageManager.PERMISSION_GRANTED) {
                        // Request user to grant write external storage permission.
                        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 10);
                    }
                } catch(Exception err) {
                    Toast.makeText(this, err.getMessage(), Toast.LENGTH_LONG);
                }
                if(!checkFolderExists(documentFolder))
                {
                    Toast.makeText(this,"The document folder does not exists. Please write down the checksum!", Toast.LENGTH_LONG).show();
                    return;
                }

                File file = new File(documentFolder, judgeSign + "_" + tournamentId + "_Checksum.txt");
                if(file.exists()) {
                    file.delete();
                }

                file.createNewFile();
                FileOutputStream fOut = new FileOutputStream(file);
                fOut.write((checksum + "").getBytes());
                fOut.close();

            }catch(Exception ex)
            {
                Toast.makeText(this,"An error occurred while writing the Checksum to disk. Please write down the checksum!", Toast.LENGTH_LONG).show();
            }
        }
    }

    private boolean checkFolderExists(String folder)
    {
        File docsFolder = new File(folder);
        boolean isPresent = true;

        if (!docsFolder.exists()) {
            isPresent = docsFolder.mkdir();
        }

        return isPresent;
    }

    private boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }


    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.signatureClearButton:
                ((SignatureView)findViewById(R.id.signatureView)).clear();
                break;
            case R.id.signatureContinueButton:
                CharSequence signature = ((SignatureView)findViewById(R.id.signatureView)).getSignature(scale);

                if(signature.length() < 10)
                {
                    Toast.makeText(this, R.string.signaturePleaseSign, Toast.LENGTH_SHORT).show();
                    break;
                }

                MessageManager.offer(TPSMessage.newSigningMessage(signature));
                setResult(1);

                // todo: why do we sleep here???
                try{
                    Thread.sleep(200);
                }catch(Exception ex)
                {

                }

                startActivity(new Intent(this, StartActivity.class).putExtra(Constants.EXTRA_FROM_JUDGING_INDEX, true));
                finish();
                break;
        }
    }

    @Override
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        try {
            // setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
        }catch(Exception ex){

        }
    }

    @Override
    public void onAttachedToWindow()
    {
           // this.getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD);
           super.onAttachedToWindow();
    }

    @Override
    public void onBackPressed() {
        Toast.makeText(this, R.string.signaturePleaseSign, Toast.LENGTH_SHORT).show();
    }
}