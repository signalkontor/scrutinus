package com.signalkontor.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.util.Log;

public class BatteryUtils extends BroadcastReceiver {

    private static final Object batterStateLocker = new Object();
    private static float batteryPercentage = 0f;
    private static int batteryStatus;

    private static final Object batteryReceiverLocker = new Object();
    private static BatteryUtils instance;
    private static final String TAG = "BatteryUtils";

    private BatteryUtils() { }

    public static int getBatteryPercentage() {
        synchronized (batterStateLocker) {
            return (int)batteryPercentage;
        }
    }

    public static String getBatteryStatus() {
        synchronized (batterStateLocker) {
            // TODO what is the correct string for "charging"? charging? ac? plugged?
            return batteryStatus == BatteryManager.BATTERY_STATUS_CHARGING ? "charging" : "battery";
        }
    }

    /**
     * Call this in every activities onResume method with 'this' as parameter.
     * @param context
     */
    public static void startReceiving(Context context) {
        synchronized (batteryReceiverLocker) {
            if(instance != null) {
                Log.w(TAG, "Not registering a second battery receiver");
                return;
            }
            instance = new BatteryUtils();
            IntentFilter filter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
            context.registerReceiver(instance, filter);
        }
    }

    /**
     * Call this in every activities onPause method with 'this' as parameter.
     * @param context
     */
    public static void stopReceiving(Context context) {
        synchronized (batteryReceiverLocker) {
            if(instance != null) {
                context.unregisterReceiver(instance);
                instance = null;
            }
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        final int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        final int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
        synchronized (batterStateLocker) {
            batteryPercentage = 100.0f * ((float)level/(float)scale);
            batteryStatus = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
        }
    }
}
