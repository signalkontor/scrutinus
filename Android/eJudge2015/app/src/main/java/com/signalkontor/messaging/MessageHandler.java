package com.signalkontor.messaging;

import com.signalkontor.data.Tournament;

import android.util.Log;

public class MessageHandler {
	private static final String TAG = "MessageHandler";

	public static void handle(COMMessage message) {
		final String tmp = message.toJson();
		final Tournament tournament = Tournament.getInstance();
		Log.v(TAG, tmp);
	
		if(message instanceof TPSMessage) {
			final TPSMessage tpsMessage = (TPSMessage)message;

            // always accept the following messages
            switch(tpsMessage.getTargetId()) {
                case 101:
                case 111:
                    if(tournament.getTournamentId() != null)
                    {
                        // do nothing if a competition has been loaded before ...
                        return;
                    }

                    tournament.newTournamentData(tpsMessage.getDoCmd(), tpsMessage.getMsgData(), ((TPSMessage) message).getReceiver());
                    return;
                case 401: System.exit(0); return;
            }

            if(tournament.getTournamentId() == null || !tournament.getTournamentId().equals(tpsMessage.getDoCmd())) {
                // for all other messages the tournamentId of the message must match the current tournamentId!
                return;
            }

			switch(tpsMessage.getTargetId()) {
                case 102:
                case 112:
                    tournament.restoreJudgmentData(tpsMessage.getMsgData(), tpsMessage.getTargetId() == 112);
                    break;

                case 121: tournament.start(); break;
                case 122: tournament.clear(); break;

                case 131: tournament.updatedTouramentData(tpsMessage.getMsgData()); break;

                case 200: tournament.moveCouple(tpsMessage.getMsgData()); break;
                case 210: tournament.removeCouple(tpsMessage.getMsgData()); break;
                case 220: tournament.addCouple(tpsMessage.getMsgData()); break;

                case 103: judgmentDataRequested(tpsMessage); break;
                case 113: judgmentDataRequestedFinals(tpsMessage); break;
                case 300: tournament.goToHeadDance(tpsMessage.getMsgData()); break;
                default:
                    System.out.println("unknown TargetId " + tpsMessage.getTargetId());
			}
		} else {
			Log.w(TAG, "I don't handle messages of type " + message.getClass());
		}
	}	

	//	Wertungen sichern und zurücksenden.
	//	eJS soll jetzt vorhandene Wertungen speichern und an
	//	mTPS senden (TargetId=12).
	private static void judgmentDataRequested(TPSMessage tpsMessage) {
        if(tpsMessage.getDoCmd().equals(Tournament.getInstance().getTournamentId())) {
            MessageManager.offer(TPSMessage.newFullJudgingMessage(tpsMessage.getTargetId(), false));
        }
	}
	
	// Wertungen sichern und zurücksenden.
	private static void judgmentDataRequestedFinals(TPSMessage tpsMessage) {
        if(tpsMessage.getDoCmd().equals(Tournament.getInstance().getTournamentId())) {
            MessageManager.offer(TPSMessage.newFullJudgingMessage(tpsMessage.getTargetId(), true));
        }
	}

}
