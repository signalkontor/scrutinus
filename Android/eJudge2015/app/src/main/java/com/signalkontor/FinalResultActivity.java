package com.signalkontor;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.signalkontor.constants.Constants;
import com.signalkontor.data.Tournament;

public class FinalResultActivity extends ListActivity implements View.OnClickListener, DialogInterface.OnClickListener {

    private ListView listView;

    private int[] marks;

    private String[] couples;

    private ArrayAdapter<String> mAdapter;

    private Class nextClass;

    private Button backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_final_result);

        Bundle extras = getIntent().getExtras();
        this.nextClass = (Class) extras.get(Constants.EXTRA_NEXT_CLASS);

        Button button = (Button) findViewById(R.id.btn);

        button.setOnClickListener(this);

        backButton = (Button) findViewById(R.id.btnResultBack);
        backButton.setOnClickListener(this);

        updateTitle();

        Tournament.Dance dance = Tournament.getInstance().getCurrentDance();

        this.couples = dance.getHeats()[0].getCouples();
        this.marks = dance.getHeats()[0].getMarks();

        mAdapter = new ArrayAdapter<String>(this, R.layout.result_list_layout, R.id.coupleNumber, couples) {

            public View getView(int position, View convertView, ViewGroup parent) {

                View view = super.getView(position, convertView, parent);

                TextView text1 = (TextView) view.findViewById(R.id.coupleNumber);
                TextView text2 = (TextView) view.findViewById(R.id.couplePlace);

                text1.setText(couples[position]);
                text2.setText(marks[position] + ".");

                return view;
            }
        };

        setListAdapter(mAdapter);
    }

    @Override
    public void onClick(View v) {
        if(v == backButton)
        {
            startActivity(new Intent(this, FinalsActivity.class).putExtra(Constants.EXTRA_FROM_JUDGING_INDEX, false));
            return;
        }

        boolean hasMoreDances = !Tournament.getInstance().getCurrentDance().isLast();

        new AlertDialog.Builder(this)
                .setMessage(hasMoreDances ? R.string.advanceToNextDance : R.string.continueToSigning)
                .setPositiveButton(R.string.yes, this)
                .setNegativeButton(R.string.no, null)
                .show();
    }

    @Override
    public void onBackPressed() {
        // wir not doing anything here ...
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        boolean hasMoreDances = Tournament.getInstance().advanceToNextDance();
        startActivity(new Intent(this, this.nextClass).putExtra(Constants.EXTRA_FROM_JUDGING_INDEX, false));
    }

    public void updateTitle() {

        Tournament.Dance dance = Tournament.getInstance().getCurrentDance();

        setTitle(new StringBuffer()
                .append(Tournament.getInstance().getJudgeFullName())
                .append(" - ")
                .append(dance.getDisplayName())
                .append(" (")
                .append(Tournament.getInstance().getDeviceId())
                .append(')'));
    }
}
