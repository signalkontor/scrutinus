package com.signalkontor;

import com.signalkontor.data.Tournament;

import java.util.Comparator;

/**
 * Created by groehnol on 29.03.2016.
 */
public class DanceComparator implements Comparator<Tournament.Dance> {
    @Override
    public int compare(Tournament.Dance lhs, Tournament.Dance rhs) {
        return lhs.getDisplayName().compareToIgnoreCase(rhs.getDisplayName());
    }
}
