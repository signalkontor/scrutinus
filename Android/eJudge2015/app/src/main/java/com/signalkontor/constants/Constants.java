package com.signalkontor.constants;

public class Constants {
	public static final String EXTRA_DANCE_INDEX = "DanceIndex";
	public static final String EXTRA_HEAT_INDEX = "HeatIndex";
    public static final String EXTRA_FROM_JUDGING_INDEX = "FromJudging";
    public static final String EXTRA_CHANGED_LANGUAGE_INDEX = "changedLanguage";
    public static final String EXTRA_NEXT_CLASS = "nextIntentClass";
}
