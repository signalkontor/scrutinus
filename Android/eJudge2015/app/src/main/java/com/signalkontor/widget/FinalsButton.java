package com.signalkontor.widget;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.widget.Button;

/**
 * User: Mikko
 * Date: 14.06.11
 * Time: 14:42
 */
public class FinalsButton extends Button {

    private boolean crossed;
    private int rowIndex = -1, columnIndex = -1, locks = 0;
    private String subCompetition = "";
    private String number;

    PorterDuffColorFilter greenFilter = new PorterDuffColorFilter(Color.GREEN, PorterDuff.Mode.SRC_ATOP);

    PorterDuffColorFilter redFilter = new PorterDuffColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);

    PorterDuffColorFilter whiteFilter = new PorterDuffColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);

    public FinalsButton(Context context) {
        super(context);
    }

    public int getRowIndex() {
        return rowIndex;
    }

    public void setRowIndex(int rowIndex) {
        this.rowIndex = rowIndex;
    }

    public int getColumnIndex() {
        return columnIndex;
    }

    public void setColumnIndex(int columnIndex) {
        this.columnIndex = columnIndex;
    }

    public String getSubCompetition() { return this.subCompetition; }

    public void setSubCompetition(String subCompetition) { this.subCompetition = subCompetition; }

    public String getNumber() { return this.number; }

    public void setNumber(String value) { this.number = value; }

    public void updateLocks(boolean increase) {
        if(increase)
            locks++;
        else
            locks--;

        update();
    }

    public boolean toggleCrossed() {
        crossed = !crossed;
        return crossed;
    }

    private void update() {
        final PorterDuffColorFilter filter = crossed ? greenFilter : locks > 0 ? redFilter : whiteFilter;
        getBackground().setColorFilter(filter);
        setEnabled(crossed || locks == 0);
    }
}
