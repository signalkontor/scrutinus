package com.signalkontor;

import java.util.Comparator;

class CoupleNumberComparer implements java.util.Comparator<String> {
    @Override
    public int compare(String o1, String o2) {

        int i1;
        int i2;

        try {

            i1 = Integer.parseInt(o1);
            i2 = Integer.parseInt(o2);
        } catch(Exception ex) {
            return -1;
        }

        if(i1 < i2) {
            return -1;
        } else if (i1 > i2){
            return 1;
        }

        return 0;
    }
}
