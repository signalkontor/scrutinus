package com.signalkontor.utils;

import java.util.Date;

public class JsonUtils {
	public static Date convert(String jsonDate) {
		// input: "Date(1306487422531+0200)"
		//
		// The first part of the number are the ticks (in milliseconds).
		// It seems that we can ignore the offset part, since the Ticks
		// are in UTC and the constructor of Date() converts it to local
		// time
		int ticksStart = jsonDate.indexOf('(') + 1;
		int ticksEnd = jsonDate.indexOf(')') - 5;
		String ticksStr = jsonDate.substring(ticksStart, ticksEnd);
		long ticks = Long.parseLong(ticksStr);
		return new Date(ticks);
	}
	
	public static String convert(Date javaDate) {
		// This will always return a string with offset +0000
		// instead of the correct time zone offset.
		return new StringBuilder()
			.append("/Date(")
			.append(javaDate.getTime())
			.append("+0000)/").toString();
	}
}
