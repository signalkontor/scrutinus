package com.signalkontor;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.signalkontor.constants.Constants;
import com.signalkontor.data.Tournament;
import com.signalkontor.messaging.MessageManager;
import com.signalkontor.messaging.TPSMessage;
import com.signalkontor.utils.BatteryUtils;
import com.signalkontor.widget.FinalsButton;

import java.io.Console;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

/**
 * User: Mikko
 * Date: 14.06.11
 * Time: 12:43
 */
public class FinalsActivity extends Activity implements OnClickListener, DialogInterface.OnClickListener, Observer {

    /*
     * Two-dimensional array of buttons. Format is: button[couple][position]
      *
      * Where
      *     couple = n: couple in the (n+1)th column
      *     position = n: (n+1)th place
     */
    private HashMap<String,FinalsButton[][]> buttons;
    private Button continueButton;
    private Tournament.Dance dance;
    private int coupleCount;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BatteryUtils.startReceiving(this);

        try{
            // setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
        }catch(Exception ex){}


        if (!MessageManager.isInitialized()) {
            AutoConfigureTask autoConfigureTask = new AutoConfigureTask(this);
            autoConfigureTask.disableDialog();
            autoConfigureTask.execute((Object[]) null);
        }

        final Tournament tournament = Tournament.getInstance();
        tournament.addObserver(this);

        dance = tournament.getCurrentDance();
        final Tournament.Heat heat = dance.getHeats()[0];
        this.coupleCount = heat.getCouples().length;

        updateTitle();

        buttons = new HashMap<String, FinalsButton[][]>();
        final HashMap<String, ArrayList<String>> couplesGrouped = this.getCouplesGrouped(heat.getCouples());

        TableRow firstTableRow = new TableRow(this);
        TableRow mainContentRow = new TableRow(this);
        TableRow continueButtonRow = new TableRow(this);

        TableLayout layout = new TableLayout(this);
        layout.setOrientation(TableLayout.VERTICAL);
        layout.setStretchAllColumns(true);
        layout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        // layout.addView(firstTableRow);

        int index = 0;

        for(String key: couplesGrouped.keySet()) {
            // TableLayout tableLayout = this.createTableView(couples);
            ArrayList<String> couples = couplesGrouped.get(key);
            ArrayList<TableRow> rows = this.createTableView(key, couples);
            for(TableRow row: rows) {
                layout.addView(row);
            }
            // mainContentRow.addView(tableLayout);
            // firstTableRow.addView(getPageButton(layout, couples[0]));
            index++;
        }

        continueButton = new Button(this);
        continueButton.setText(R.string._continue);
        continueButton.setVisibility(View.VISIBLE);
        continueButton.setOnClickListener(this);
        continueButtonRow.addView(continueButton);

        final int[] marks = heat.getMarks();
        for (int i = 0; i < marks.length; i++) {
            final int mark = marks[i] - 1;
            if (mark >= 0 && mark < heat.getCouples().length) {
                // Todo: need to fix this with group
                final FinalsButton button = findButton(heat.getCouples()[i], mark);
                if(button != null) {
                    final boolean crossed = button.toggleCrossed();
                    updateButtons(button, crossed);
                }
            }
        }

        postStatusMessage();

        layout.addView(mainContentRow);
        layout.addView(continueButtonRow);

        ScrollView scrollView = new ScrollView(this);
        scrollView.addView(layout);

        setContentView(scrollView);

        continueButton.setVisibility(dance.getMarkCount() >= this.coupleCount ? View.VISIBLE : View.INVISIBLE);
    }


    private FinalsButton findButton(String couple, int mark) {
        // find the right group and button:
        for(String key: this.buttons.keySet()) {
            FinalsButton[][] group = this.buttons.get(key);
            // check, if we have the right group:
            for(FinalsButton[] button: group) {
                if(button[0].getNumber().equals(couple)) {
                    return button[mark];
                }
            }
        }

        return null;
    }

    private Button getPageButton(TableLayout tableLayout, String text) {
        Button button = new Button(this);
        button.setText(text);
        button.setOnClickListener(this);

        return button;
    }

    private HashMap<String, ArrayList<String>> getCouplesGrouped(String[] allCouplesArray) {
        HashMap<String, ArrayList<String>> lists = new HashMap<String, ArrayList<String>>();

        ArrayList<String> allCouples = new ArrayList<String>();
        for(String couple: allCouplesArray) {
            allCouples.add(couple);
        }
        Collections.sort(allCouples);

        for(String couple: allCouples) {
            String[] data = couple.split(" ");

            String group = "";
            String number = "";

            if (data.length > 1) {
                group = data[0];
                number = data[1];
            } else {
                number = data[0];
            }

            if (!lists.containsKey(group)) {
                ArrayList<String> list = new ArrayList<String>();
                lists.put(group, list);
            }

            lists.get(group).add(number);
        }

        String[][] result = new String[lists.size()][];
        int index = 0;
        for(String key: lists.keySet()) {
            Object[] array = lists.get(key).toArray();
            try {
                result[index] = Arrays.copyOf(array, array.length, String[].class);
            }catch(Exception ex) {

                Log.e("ERROR", ex.getMessage());
            }
            index++;
        }

        for(String key: lists.keySet()) {
            Collections.sort(lists.get(key), new CoupleNumberComparer());
        }

        return lists;
    }

    private ArrayList<TableRow> createTableView(String group, ArrayList<String> couples) {

        ArrayList<TableRow> rows = new ArrayList<TableRow>();

        // final TableLayout tableLayout = new TableLayout(this);
        // tableLayout.setOrientation(TableLayout.VERTICAL);
        // tableLayout.setStretchAllColumns(true);
        // tableLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        final TableRow headerRow = new TableRow(this);
        headerRow.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        rows.add(headerRow);

        for (String couple : couples) {

            String[] coupleSplitted = couple.split(" ");

            final TextView textView = new TextView(this);
            if(coupleSplitted.length > 1) {
                textView.setText(coupleSplitted[1]);
            } else {
                textView.setText(couple);
            }

            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            textView.setGravity(Gravity.CENTER_HORIZONTAL);
            headerRow.addView(textView);
        }
        // tableLayout.addView(headerRow);

        if(!this.buttons.containsKey(group)) {
            FinalsButton[][] finalButtons = new FinalsButton[couples.size()][couples.size()];
            this.buttons.put(group, finalButtons);
        }

        for (int positionIndex = 0; positionIndex < couples.size(); positionIndex++) {

            String[] coupleSplitted = couples.get(positionIndex).split(" ");

            final TableRow tableRow = new TableRow(this);
            rows.add(tableRow);

            for (int coupleIndex = 0; coupleIndex < couples.size(); coupleIndex++) {
                final FinalsButton button = new FinalsButton(this);
                button.setText("" + (positionIndex + 1));
                button.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
                // button.setWidth(56);
                // button.setHeight(56);
                button.setRowIndex(positionIndex);
                button.setColumnIndex(coupleIndex);
                button.setSubCompetition(group);
                button.setNumber(group + " " + couples.get(coupleIndex));

                button.setOnClickListener(this);

                this.buttons.get(group)[coupleIndex][positionIndex] = button;
                tableRow.addView(button);
            }
            // tableLayout.addView(tableRow);
        }

        // return table layout
        return rows;
    }

    private void postStatusMessage() {
        MessageManager.setCurrentStatusMessage(TPSMessage.newStatusMessage(2, dance.getMarkCount() == this.coupleCount ? 1 : 0, dance.getIndex() + 1, 0));
    }

    @Override
    public void update(Observable observable, Object o) {
        if (observable instanceof Tournament) {
            final Tournament tournament = (Tournament) observable;

            if (tournament.isClosing()) {
                switchToActivity(StartActivity.class);
            } else if(o == Tournament.COUPLES_MODIFIED) {
                switchToActivity(FinalsActivity.class);
            }
        }
    }

    private void switchToActivity(Class nextActivityClass) {
        // remove us from the observer list to prevent dangling references
        Tournament.getInstance().deleteObserver(this);

        if(Tournament.getInstance().showResultsPage()){
            Intent intent = new Intent(this, FinalResultActivity.class);
            intent.putExtra(Constants.EXTRA_FROM_JUDGING_INDEX, false);
            intent.putExtra(Constants.EXTRA_NEXT_CLASS, nextActivityClass);
            startActivity(intent);
        }
        else {
            startActivity(new Intent(this, nextActivityClass).putExtra(Constants.EXTRA_FROM_JUDGING_INDEX, false));
        }

        finish();
    }

    private void updateOtherButtons(String group, boolean crossed, int rowToDisable, int columnToDisable) {

        FinalsButton[][] finalButtons = this.buttons.get(group);

        for (int i = 0; i < finalButtons.length; i++) {
            final FinalsButton rowButton = finalButtons[i][rowToDisable];
            rowButton.updateLocks(crossed);

            final FinalsButton colButton = finalButtons[columnToDisable][i];
            colButton.updateLocks(crossed);
        }

        continueButton.setVisibility(dance.getMarkCount() >= this.coupleCount ? View.VISIBLE : View.INVISIBLE);
    }

    private void updateButtons(FinalsButton button, boolean crossed) {
        final int rowToDisable = button.getRowIndex();
        final int columnToDisable = button.getColumnIndex();
        final String group = button.getSubCompetition();

        updateOtherButtons(button.getSubCompetition(), crossed, rowToDisable, columnToDisable);
    }

    @Override
    public void onClick(View view) {
        if (view == continueButton) {
            if(Tournament.getInstance().showResultsPage())
            {
                // Directly go to result page, switch to next dance on the result page
                final boolean hasNextDance = !Tournament.getInstance().getCurrentDance().isLast();
                switchToActivity(hasNextDance ? FinalsActivity.class : SigningActivity.class);
                return;
            }

            handleContinueClick();

        } else if (view instanceof FinalsButton) {
            handleJudgingClick((FinalsButton) view);
        }
    }

    private void handleContinueClick() {
        new AlertDialog.Builder(this)
                .setMessage(dance.isLast() ? R.string.continueToSigning : R.string.advanceToNextDance)
                .setPositiveButton(R.string.yes, this)
                .setNegativeButton(R.string.no, null)
                .show();
    }

    private void handleJudgingClick(FinalsButton button) {
        final boolean crossed = button.toggleCrossed();
        dance.setPosition(button.getNumber(), crossed ? button.getRowIndex() + 1 : 0);
        updateButtons(button, crossed);
        postStatusMessage();
    }

    @Override
    public void onPause() {
        BatteryUtils.stopReceiving(this);
        super.onPause();
    }

    @Override
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        try {
            // setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); // prevent screen rotation
        }catch(Exception ex) {}

    }

    public void updateTitle() {
        setTitle(new StringBuffer()
                .append(Tournament.getInstance().getJudgeFullName())
                .append(" - ")
                .append(dance.getDisplayName())
                .append(" (")
                .append(Tournament.getInstance().getDeviceId())
                .append(')'));
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        final boolean hasNextDance = Tournament.getInstance().advanceToNextDance();

        if(!hasNextDance)
            MessageManager.offer(TPSMessage.newFullJudgingMessage(0, true));       // send complete marking of final

        if(Tournament.getInstance().showResultsPage()){
            // We show the FinalResultActivity
        }

        switchToActivity(hasNextDance ? FinalsActivity.class : SigningActivity.class);
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
            .setMessage(R.string.quitApplication)
            .setPositiveButton(R.string.no, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Tournament tournament = Tournament.getInstance();
                    if(tournament != null)
                    {
                        tournament.saveAndExit();
                    }
                    System.exit(0);
                }
            })

            .setNegativeButton(R.string.yes, null)
            .show();
    }

    @Override
    public void onAttachedToWindow()
    {
           // this.getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD);
           super.onAttachedToWindow();
    }
}