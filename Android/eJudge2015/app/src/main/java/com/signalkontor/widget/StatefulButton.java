package com.signalkontor.widget;

import java.util.HashMap;

import com.signalkontor.R;
import com.signalkontor.data.Tournament;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.Button;

public class StatefulButton extends Button implements OnClickListener, OnLongClickListener {

	private static final int O = 0;
	private static final int X = 1;
	private static final int P1 = 2;
	private static final int P2 = 3;
    private static final int L = 4;
	
    private static final String TAG = "StatefulButton";
	private int state = O;
	private CharSequence couple;
	private int danceIndex;
	private int heatIndex;
	
	private int popupItems;

	private static final HashMap<Integer, Integer> buttonColors;
	private static final HashMap<Integer, CharSequence> stateChars;
	static {
		buttonColors = new HashMap<Integer, Integer>();
		buttonColors.put(O, Color.WHITE);
		buttonColors.put(X, Color.GREEN);
		buttonColors.put(P1, Color.YELLOW);
		buttonColors.put(P2, Color.MAGENTA);
        buttonColors.put(L, Color.RED);

		stateChars = new HashMap<Integer, CharSequence>();
		stateChars.put(O, "");
		stateChars.put(X, "X");
		stateChars.put(P1, "●");
		stateChars.put(P2, "●●");
        stateChars.put(L, "L");
	}

	public void setHelperCrosses(int count) {
		switch(count) {
		case 0: setLongClickable(false); break;
		case 1: popupItems = R.array.judgeValuesOneHelper; break;
		case 2: popupItems = R.array.judgeValuesBothHelpers; break;
		}
	}
	
	public void setDanceIndex(int danceIndex) {
		this.danceIndex = danceIndex;
	}
	
	public void setHeatIndex(int heatIndex) {
		this.heatIndex = heatIndex;
	}

    public void setCouple(CharSequence couple) {
		this.couple = couple;
		update();
	}

	private void setupEventListener()
	{
		setOnClickListener(this);
		setOnLongClickListener(this);
	}
	
	public StatefulButton(Context context) {
		super(context);
		setupEventListener();
	}

	public void setInitialState(int initialState) {
		Log.d(TAG, "Initialstate " + initialState);
		state = initialState;
		update();
	}
	
	public void setState(int newState) {
		state = newState;
		Log.d(TAG, "Selected " + newState);
		Tournament.getInstance().setMark(danceIndex, heatIndex, getId(), newState);
		update();
	}
	
	private void update() {
		final int color = buttonColors.get(state);
		getBackground().setColorFilter(color, PorterDuff.Mode.MULTIPLY);
		setText(state == X ? new StringBuilder().append(couple).append("\r\n") : new StringBuilder().append(couple).append("\r\n").append(stateChars.get(state)));
	}

	public void onClick(View v) {
		setState(state == X || state == L ? O : X);
	} 
	
	public boolean onLongClick(View v) {
		openJudgement();
		return true;
	}

	private void openJudgement() {
		new AlertDialog.Builder(getContext())
			.setTitle(getResources().getString(R.string.judgmentPopupText, getText()))
			.setItems(popupItems, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					setState(which); 
				}
			})
			.show();
	}
}
