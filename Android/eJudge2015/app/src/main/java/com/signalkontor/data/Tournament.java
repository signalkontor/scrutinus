package com.signalkontor.data;

import android.content.Context;
import android.util.Log;

import com.signalkontor.CoupleNumberComparator;
import com.signalkontor.DanceComparator;
import com.signalkontor.messaging.MessageManager;
import com.signalkontor.messaging.TPSMessage;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Observable;

public class Tournament extends Observable implements Serializable {

    public static final Object NEW_TOURNAMENT_DATA = new Object();
    public static final Object STARTED_CHANGED = new Object();
    public static final Object TOURNAMENT_UPDATED = new Object();
    public static final Object SET_MARK = new Object();
    public static final Object SET_POSITION = new Object();
    public static final Object CLEARED_CHANGED = new Object();
    public static final Object COUPLES_MODIFIED = new Object();
    public static final Object HEAT_DANCE_CHANGED = new Object();

	private static final String TAG = "Tournament";

    public CharSequence getJudgeFullName() {
        final StringBuffer fullName = new StringBuffer()
                .append(judgeFirstname)
                .append(' ')
                .append(judgeLastname);
            if(judgeFirstname.length() > 0) {
                fullName.append(" (")
                    .append(judgeId)
                    .append(')');
            }
        return fullName;
    }

    public class Dance implements Serializable {
		private static final long serialVersionUID = 1L;
		private final int index;
		private final String id;
		private final String name;
		private final String shortName;
		private int markCount;
        private int helpmark1Count;
        private int helpmark2Count;
		private Heat[] heats;
		
		public Dance(final int index, final String id, final String name, final String shortName, final int numberOfHeats) {
			this.index = index;
			this.id = clean(id);
			this.name = clean(name);
			this.shortName = clean(shortName);
			this.heats = new Heat[numberOfHeats];
			this.markCount = 0;
            helpmark1Count = 0;
            helpmark2Count = 0;
		}
		
		public String getId() {
			return id;
		}

		public String getName() {
			return name;
		}

		public String getShortName() {
			return shortName;
		}
		
		public int getMarkCount() {
			return markCount;
		}

        public int getHelpMark1Count()
        {
            return helpmark1Count;
        }
        
        public int getHelpMark2Count() {
            return helpmark2Count;
        }
        
		public Heat[] getHeats() {
			return heats;
		}
		
		public String getDisplayName() {
			return name;
		}

		public int getIndex() {
			return index;
		}

        public boolean hasCorrectNumberOfMarks() {
            return minMarks <= markCount && markCount <= maxMarks;
        }

        public boolean hasValidNumberOfMarks() {
            return !strictMarks || hasCorrectNumberOfMarks();
        }

        synchronized public void setPosition(final String coupleCode, final int position) {

            final Heat heat = heats[0];
            int coupleIndex = 0;

            for(String couple: heat.getCouples()) {

                if (couple.equals(coupleCode)) {
                    break;
                }
                coupleIndex++;
            }

            final int previousMark = heat.marks[coupleIndex];

            if(position != previousMark) {
                changedByUser = true;
                heat.marks[coupleIndex] = position;
                markCount += (position > 0) ? 1 : -1;
                //MessageManager.setCurrentStatusMessage(TPSMessage.newStatusMessage(2, (markCount == heat.couples.length ? 1 : 0), index, 0));
                boolean added = MessageManager.offer(TPSMessage.newFinalJudgingMessage(heat.couples[coupleIndex], index + 1, position));
                if(!added) {
                    Log.e(TAG, "Could not add message to queue!");
                    // TODO maybe show Toast to user?
                }
                save();
                reportChanges(SET_POSITION);
            }
        }

        public boolean isLast() {
            return index + 1 == dances.length;
        }
    }
	
	public class Heat implements Serializable {
		private static final long serialVersionUID = 1L;

		public Heat(String data) {
			couples = clean(data).split(";");
			marks = new int[couples.length];
		}
		
		private String[] couples;
		private int[] marks;
		
		public String[] getCouples() {
			return couples;
		}
		
		public int[] getMarks() {
			return marks;
		}
	}

    public class PinCode implements Serializable {
        private String pin;
        private String sign;
        private String firstName;
        private String lastName;

        public PinCode(String pin, String sign, String firstName, String lastName){
            this.pin = pin;
            this.sign = sign;
            this.firstName = firstName;
            this.lastName = lastName;
        }

        public String getPin(){return pin;}
        public String getSign() {return sign;}
        public String getFirstName() {return firstName;}
        public String getLastName() { return lastName;}
    }

	private static final String SERIALIZATION_FILENAME = "tournament.ser";
	private static final long serialVersionUID = 1L;
	
	private static final int INDEX_RND_TYPE = 0;
	private static final int INDEX_LANG = 1;
	private static final int INDEX_CONTROL_FLAGS = 2;
	private static final int INDEX_TN_TITLE = 3;
	private static final int INDEX_ST_GR_KL_ART = 4;
	private static final int INDEX_RND_TXT = 5;
	private static final int INDEX_MARKS_VON = 6;
	private static final int INDEX_MARKS_BIS = 7;
	private static final int INDEX_MARKS_STRICT = 8;
	private static final int INDEX_WR_KZ = 9;
	private static final int INDEX_WR_VN = 10;
	private static final int INDEX_WR_NN = 11;
	private static final int INDEX_WR_CLUB = 12;
	private static final int INDEX_GRP_OFFSET = 13;
	private static final int INDEX_ANZ_TANZ = 14;

	transient private Context context;

    private static final Object instanceLocker = new Object();
	private static Tournament instance;
	private Tournament() {
        started = false;
        cleared = true;

    }

	public static Tournament getInstance() {
        synchronized (instanceLocker) {
            if(instance == null)
            {
                instance = new Tournament();
            }
        }
		return instance;
	}

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public boolean IsRecoveredFromFile(){ return this.recovered && this.tournamentId != null; }

    public void UnsetRecoveredFlag(){
        this.recovered = false;
    }

    public boolean advanceToNextDance() {
        currentDance++;
        save();
        return currentDance < dances.length;
    }

    public boolean moveBackDance()
    {
        if(currentDance == 0)
        {
            return false;
        }

        currentDance--;
        save();
        return true;
    }

	private void reportChanges(Object tag) {
		setChanged();
		notifyObservers(tag);
	}
	
	private synchronized void save() {
		FileOutputStream fileOutputStream;
		ObjectOutputStream objectOutputStream;
		try {
			fileOutputStream = context.openFileOutput(SERIALIZATION_FILENAME, Context.MODE_PRIVATE);
			objectOutputStream = new ObjectOutputStream(fileOutputStream);
			objectOutputStream.writeObject(this);
			objectOutputStream.close();
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
			e.printStackTrace();
		}
	}
	
	public static void restore(Context context) {
        synchronized (instanceLocker)
        {
            instance = null;
            try {
                FileInputStream fileInputStream;
                ObjectInputStream objectInputStream;
                fileInputStream = context.openFileInput(SERIALIZATION_FILENAME);
                objectInputStream = new ObjectInputStream(fileInputStream);
                instance = (Tournament) objectInputStream.readObject();
                instance.recovered = true;
                objectInputStream.close();
            } catch(IOException ioException) {

            } catch(Exception ignored) {
            } finally {
                if(instance == null) {
                    instance = new Tournament();
                }
            }
            instance.context = context;
        }
	}

    public void saveAndExit()
    {
        this.save();
        this.tournamentId = null;
    }

    public int getChecksum()
    {
        if(this.roundType == 2)
        {
            return getChecksumFinal();
        }
        else
        {
            return getChecksumMarks();
        }
    }

    private int getChecksumFinal()
    {
        int checksum = 0;

        List<Dance> danceList = new ArrayList<Dance>();
        List<Byte> hashList = new ArrayList<Byte>();

        for (Dance dance:this.dances) {
            danceList.add(dance);
        }

        Collections.sort(danceList, new DanceComparator());

        for (Dance dance:danceList){
            String resultString = "";
            for (Heat heat:dance.heats) {
                // we need a string with numbers sorted by places
                for(int i=0;i<heat.couples.length;i++)
                {
                    // find index of place
                    int index = findIndexOfPlace(i + 1, heat.marks);
                    resultString += heat.couples[index];
                }
            }

            byte[] hash = calculateSHA1(resultString);

            for (byte b:hash) {
                hashList.add(b);
            }
        }

        // now calculate hash for all dances:
        byte[] bytes = new byte[hashList.size()];
        for(int i=0;i<hashList.size();i++)
        {
            bytes[i] = hashList.get(i).byteValue();
        }
        byte[] hash = calculateSHA1(bytes);

        checksum = (hash[19] & 0xFF) | (hash[18] & 0xFF) << 8;

        return checksum % 10000;
    }

    private byte[] calculateSHA1(String data)
    {
        return calculateSHA1(data.getBytes());
    }

    private byte[] calculateSHA1(byte[] data)
    {
        MessageDigest md = null;
        try{
            md = MessageDigest.getInstance("SHA-1");
            return md.digest(data);
        }catch(NoSuchAlgorithmException ex)
        {
            return null;
        }
    }

    private int findIndexOfPlace(int place, int[] marks) {
        for(int i=0;i<marks.length;i++)
        {
            if(marks[i] == place)
            {
                return i;
            }
        }

        return 0;
    }

    private int getChecksumMarks()
    {
        int checksum = 0;

        List<Dance> danceList = new ArrayList<Dance>();
        List<Byte> hashList = new ArrayList<Byte>();
        List<String> markedCouples;

        for (Dance dance:this.dances) {
            danceList.add(dance);
        }

        Collections.sort(danceList, new DanceComparator());

        for (Dance dance:danceList){
            markedCouples = new ArrayList<String>();
            for (Heat heat:dance.heats) {
                for (int i=0;i<heat.couples.length;i++ ) {
                    // todo: check that 1 is the right code...
                    if(heat.marks[i] == 1)
                    {
                        markedCouples.add(heat.couples[i]);
                    }
                }
            }

            Collections.sort(markedCouples, new CoupleNumberComparator());
            String resultString = "";

            for (String marked:markedCouples) {
                resultString += marked;
            }

            byte[] hash = calculateSHA1(resultString);

            for (byte b:hash) {
                hashList.add(b);
            }
        }

        byte[] bytes = new byte[hashList.size()];

        for(int i=0;i<hashList.size();i++)
        {
            bytes[i] = hashList.get(i).byteValue();
        }
        byte[] hash = calculateSHA1(bytes);

        checksum = (hash[19] & 0xFF) | (hash[18] & 0xFF) << 8;

        return checksum % 10000;
    }

    public static void delete(Context context) {
        try {
            context.deleteFile(SERIALIZATION_FILENAME);
        } catch(Exception ignored) {

        }
    }
	
	private String clean(String str) {
		return str.replaceAll("\"", "");
	}
	
	public static final int ROUND_TYPE_PRELIMINARY  = 1;
	public static final int ROUND_TYPE_FINAL = 2;

	private String tournamentId;
    private String deviceId;

	transient private boolean closing;
	private boolean cleared;
	private boolean started;

    private boolean changedByUser;

	private int roundType;
	private int language;
	private int flags;
	private String title = "";
	private String details = "";
	@SuppressWarnings({"FieldCanBeLocal"})
    private String roundDetails;
	private int minMarks;
	private int maxMarks;
	private boolean strictMarks;
	private String judgeId = "";
	private String judgeFirstname = "";
	private String judgeLastname = "";
	@SuppressWarnings({"FieldCanBeLocal"})
    private String judgeClub = "";
	@SuppressWarnings({"FieldCanBeLocal"})
    private int groupOffset;
    private int currentDance;
    private int currentGroup;
    private boolean recovered;
	private Dance[] dances;
    private List<PinCode> pinCodes;

	public String getTournamentId() {
		return tournamentId;
	}
	
	public boolean isStarted() {
		return started;
	}

    public boolean isChangedByUser() {
        return changedByUser;
    }

    public PinCode getPinCode(String code){
        for(PinCode pin : pinCodes){
            if(pin.pin.equals(code)){
                return pin;
            }
        }
        return null;
    }

    public boolean hasPinCodes() { return pinCodes != null && pinCodes.size() > 0;}

	public void start() {
        if(!started) {
            started = true;
            save();
            MessageManager.setCurrentStatusMessage(TPSMessage.newStatusMessage(0, 2, 0, 0));
            reportChanges(STARTED_CHANGED);
        }
	}	
	
	public boolean isCleared() {
		synchronized (this) {
			return cleared;
		}		
	}

    public boolean isFinished() {
        return dances != null && currentDance >= dances.length;
    }

	public void clear() {
        synchronized (instanceLocker)
        {
            instance = new Tournament();
            instance.setContext(context);
            instance.save(); // save empty instance
        }
        closing = true;
        reportChanges(CLEARED_CHANGED);
	}
	
	public int getRoundType() {
		return roundType;
	}

    public void setLanguage(int language) {
        this.language = language;
        save();
    }

	public int getLanguage() {
		return language;
	}

    public String getFullTitle() {
		return new StringBuffer()
			.append(title)
			.append(' ')
			.append(details).toString().trim();
		}

    public int getMinMarks() {
		return minMarks;
	}

	public int getMaxMarks() {
		return maxMarks;
	}

	public String getJudgeId() {
		return judgeId;
	}

    public Dance getCurrentDance() {
        if(currentDance >= dances.length) {
            currentDance = 0;
        }
        return dances[currentDance];
    }

    public Dance getPreviousDance() {
        if(currentDance == 0)
        {
            return dances[0];
        }

        if(isFinished())
        {
            return dances[dances.length - 1];
        }

        return dances[currentDance - 1];
    }

    public int getCurrentGroup()
    {
        if(currentGroup >= 0)
            return currentGroup;
        else
            return 0;
    }
    
    public void setCurrentGroup(int group)
    {
        currentGroup = group;
    }
	public Dance[] getDances() {
		return dances;
	}
	
	public boolean showHelperCross1() {
		return (flags & 1 + 32) != 0; // show helper cross 1 if helper cross 2 is enabled  
	}
	
	public boolean showHelperCross2() {
		return (flags & 32) != 0;
	}

	public boolean showHelperCrossOnRoundEnd() {
		return (flags & 2) != 0;
	}
	
	public boolean multipleDownVotesAllowed() {
		return (flags & 4) != 0;
	}
	
	public boolean showResultsPage() {
		return (flags & 8) != 0;
	}

	public boolean showLift() {
		return (flags & 16) != 0;
	}
	
	public boolean showLanguageDialog() {
		return (flags & 64) != 0;
	}
	
	private void resetMarkCounts() {
		for(Dance d : dances) {
			d.markCount = 0;
            d.helpmark1Count = 0;
            d.helpmark2Count = 0;
		}
	}

    public void setJudge(String judgeSign, String judgeFirstName, String judgeLastName)
    {
        this.judgeId = judgeSign;
        this.judgeFirstname = judgeFirstName;
        this.judgeLastname = judgeLastName;
        this.save();
    }

	public void restoreJudgmentData(final String data, final boolean finals) {
        if(isChangedByUser())
            return;

		final String[] d = parse(data);
		final int numCouples = Integer.parseInt(d[0]);
		
		resetMarkCounts(); // set markCounts for all dances to zero
		
		for(int coupleIndex = 0; coupleIndex < numCouples; coupleIndex ++) {
			final int index = coupleIndex * 2;
			final String coupleId = d[index + 1];
			final String[] judgements = d[index + 2].split(";");
			for(int danceIndex = 0; danceIndex < judgements.length; danceIndex++) {
				final Dance dance = dances[danceIndex];
                boolean found = false;
				for(Heat heat : dance.heats) {
					for(int i = 0; i < heat.couples.length; i++) {
                        
						if(heat.couples[i].equals(coupleId)) {
                            found = true;
							final String mark = judgements[danceIndex];
                            int intMark = 0;
                            if(finals) {
                                // in finals the position is transmitted (with 0 meaning no position set yet)
                                try {
                                    intMark = Integer.parseInt(mark);
                                } catch(NumberFormatException e) {
                                    Log.w(TAG, e);
                                }
                                if(intMark > 0 && intMark <= numCouples) {
                                    dance.markCount++;
                                }
                            } else if("X".equals(mark)) {
								intMark = 1;
								dance.markCount++;
							} else if("1".equals(mark)) {
								intMark = 2;
                                dance.helpmark1Count++;
							} else if("2".equals(mark)) {
								intMark = 3;
                                dance.helpmark2Count++;
							}
							heat.marks[i] = intMark;
						}
					}

				}
                if(!found)
                {
                    Log.e(TAG, "Couple not found");
                }
			}
		}
        this.save();
        Log.e(TAG, "Data updated");
	}
	
	// CSV vorrunde:
	// 1;1;170;"";"Test Vorrunde";Vorrunde;5;7;1;DM;Marion;Dehling;"Tanz Akademie Berlin";0;4;LW;Waltz;Waltz;2;"2;3;5;7;9;";"1;4;6;8;10;";T;Tango;Tango;2;"4;5;7;9;10;";"1;2;3;6;8;";SL;"Slow Foxtrot";Slow;2;"1;2;3;5;8;";"4;6;7;9;10;";Q;Quickstep;Quick;2;"2;5;7;9;10;";"1;3;4;6;8;"
	
	// CSV finale:
	//   2;1;170;"Michel-Pokal So Sen II C Std";"Senior II C Standard";Final;0;0;1;BD;Doris;Bahr;"TSA des TSV Glinde";0;4;LW;Waltz;Waltz;1;"592;595;596;599;603;605;";T;Tango;Tango;1;"592;595;596;599;603;605;";SL;"Slow Foxtrot";Slow;1;"592;595;596;599;603;605;";Q;Quickstep;Quick;1;"592;595;596;599;603;605;"
	public void newTournamentData(final String tournamentId, final String data, final String deviceId) {
		// only allow new data if previous data was cleared (by dedicated clear command of server)
		synchronized (this) {
			if(!cleared) {
				// return;
			}
			cleared = false;
		}
        instance.changedByUser = false;
		this.tournamentId = tournamentId;
		this.deviceId = deviceId;

		final String[] d = parse(data);
		roundType = Integer.parseInt(d[INDEX_RND_TYPE]);
		language = Integer.parseInt(d[INDEX_LANG]);
		flags = Integer.parseInt(d[INDEX_CONTROL_FLAGS]);
		title = clean(d[INDEX_TN_TITLE]);
		details = clean(d[INDEX_ST_GR_KL_ART]);
		roundDetails = d[INDEX_RND_TXT];
		minMarks = Integer.parseInt(d[INDEX_MARKS_VON]);
		maxMarks = Integer.parseInt(d[INDEX_MARKS_BIS]);
		strictMarks = "1".equals(d[INDEX_MARKS_STRICT]);
		judgeId = d[INDEX_WR_KZ];
		judgeFirstname = d[INDEX_WR_VN];
		judgeLastname = d[INDEX_WR_NN];
		judgeClub = clean(d[INDEX_WR_CLUB]);
		groupOffset = Integer.parseInt(d[INDEX_GRP_OFFSET]);

        currentDance = 0;
		final int numberOfDances = Integer.parseInt(d[INDEX_ANZ_TANZ]);
		int position = INDEX_ANZ_TANZ + 1;
		dances = new Dance[numberOfDances];
		for(int i = 0; i < numberOfDances; i++) {
			final Dance dance = new Dance(i, d[position++], d[position++], d[position++], Integer.parseInt(d[position++]));
			for(int j = 0; j < dance.getHeats().length; j++) {
				dance.getHeats()[j] = new Heat(d[position++]);
			}
			dances[i] = dance;
		}

        MessageManager.setCurrentStatusMessage(TPSMessage.newStatusMessage(0, 1, 0, 0));
		save();
		reportChanges(NEW_TOURNAMENT_DATA);
	}

    public void moveCouple(final String data) {
        final String[] d = data.split(";");
        final String coupleId = d[0];

        for(int i = 1; i < d.length; i += 2) {
            final int danceIndex = Integer.parseInt(d[i]) - 1;
            final int heatIndex = Integer.parseInt(d[i+1]) - 1;
            int mark = removeCouple(coupleId, danceIndex);
            addCouple(coupleId, danceIndex, heatIndex, mark);
        }
        save();
        reportChanges(COUPLES_MODIFIED);
    }

    public void removeCouple(final String data) {
        final String[] d = data.split(";");
        final String coupleId = d[0];
        for(int i = 1; i < d.length; i++) {
            final int danceIndex = Integer.parseInt(d[i]) - 1;
            removeCouple(coupleId, danceIndex);
        }
        save();
        reportChanges(COUPLES_MODIFIED);
    }

    private int removeCouple(final String coupleId, final int danceIndex) {
        final Dance dance = dances[danceIndex];
        for(Heat heat : dance.heats) {
            final String[] couples = heat.couples;
            final int coupleIndex = Arrays.asList(couples).indexOf(coupleId);

            if(coupleIndex == -1)
                continue; // couple is not in this heat

            final int[] marks = heat.marks;
            heat.couples = new String[couples.length-1];
            heat.marks = new int[marks.length-1];
            System.arraycopy(couples, 0, heat.couples, 0, coupleIndex);
            System.arraycopy(marks, 0, heat.marks, 0, coupleIndex);
            final int length = couples.length - coupleIndex - 1;
            if(length > 0)
            {
                System.arraycopy(couples, coupleIndex+1, heat.couples, coupleIndex, length);
                System.arraycopy(marks, coupleIndex+1, heat.marks, coupleIndex, length);
            }
            // no need to look further for the couple (couple can only be in one heat)
            // we return the mark the deleted couple had (needed for moving couples)
            final int mark = marks[coupleIndex];
            if(mark == 1) {
                dance.markCount--;
            }
            return mark;
        }
        throw new IllegalArgumentException("No couple with id " + coupleId);
    }

    public void addCouple(final String data) {
        final String[] d = data.split(";");
        final String coupleId = d[0];
        for(int i = 1; i < d.length; i += 2) {
            final int danceIndex = Integer.parseInt(d[i]) - 1;
            final int heatIndex = Integer.parseInt(d[i+1]) - 1;

            addCouple(coupleId, danceIndex, heatIndex, 0);
        }
        save();
        reportChanges(COUPLES_MODIFIED);
    }

    private void addCouple(final String coupleId, final int danceIndex, final int heatIndex, int mark) {
        final int coupleIdInt = Integer.parseInt(coupleId);
        Heat heat = dances[danceIndex].heats[heatIndex];
        final int length = heat.couples.length + 1;
        final String[] newCouples = new String[length];
        final int[] newMarks = new int[length];
        // Check, if couple is allready in this heat:
        for(int i=0;i<heat.couples.length;i++)
        {
            if(heat.couples[i].equals(coupleId))
            {
                // Couple is already added, so do not add again.
                return;
            }
        }
        int insertIndex;
        for(insertIndex = 0; insertIndex < heat.couples.length; insertIndex++) {
            if(Integer.parseInt(heat.couples[insertIndex]) > coupleIdInt) {
                break;
            }
        }

        System.arraycopy(heat.couples, 0, newCouples, 0, insertIndex);
        System.arraycopy(heat.marks, 0, newMarks, 0, insertIndex);
        newCouples[insertIndex] = coupleId;
        newMarks[insertIndex] = mark;
        if(insertIndex < heat.couples.length)
        {
            System.arraycopy(heat.couples, insertIndex, newCouples, insertIndex + 1, heat.couples.length - insertIndex);
            System.arraycopy(heat.marks, insertIndex, newMarks, insertIndex + 1, heat.couples.length - insertIndex);
        }
        heat.couples = newCouples;
        heat.marks = newMarks;
        if(mark == 1) {
            dances[danceIndex].markCount++;
        }
    }

    public void goToHeadDance(String data)
    {
        final String[] d = data.split(";");
        final int dance = Integer.parseInt(d[0]) - 1;
        currentDance = dance;
        currentGroup = Integer.parseInt(d[1]) - 1;
        save();
        reportChanges(HEAT_DANCE_CHANGED);
    }
    
    public void updatedTouramentData(String data) {
        final String[] d = parse(data);
        minMarks = Integer.parseInt(d[INDEX_MARKS_VON]);
        maxMarks = Integer.parseInt(d[INDEX_MARKS_BIS]);
        strictMarks = "1".equals(d[INDEX_MARKS_STRICT]);
        save();
        reportChanges(TOURNAMENT_UPDATED);
    }

	private String[] parse(final String data) {
		final int length = data.length();
		final List<String> tmp = new ArrayList<String>(64);
		int start = 0;

        boolean inQuotedMode = false;
        String tempStr = "";

        for(int i=0;i<length;i++)
        {
            if(data.substring(i, i + 1).equals("\""))
            {
                if(!inQuotedMode)
                {
                    inQuotedMode = true;
                }
                else
                {
                    inQuotedMode = false;
                }
            }
            else
            {
                if(data.substring(i, i + 1).equals(";"))
                {
                    if(inQuotedMode)
                    {
                        tempStr += data.substring(i, i + 1);
                    }
                    else
                    {
                        tmp.add(tempStr);
                        tempStr = "";
                    }
                }
                else
                {
                    // normal char ...
                    tempStr += data.substring(i, i + 1);
                }
            }
        }

        if(tempStr.length() > 0)
        {
            tmp.add(tempStr);
        }

		return tmp.toArray(new String[tmp.size()]);
	}

	synchronized public void setMark(final int danceIndex, final int heatIndex, final int coupleIndex, final int mark) {
		final Dance dance = dances[danceIndex];
		final Heat heat = dance.heats[heatIndex];
		final int previousMark = heat.marks[coupleIndex]; 
		if(mark != previousMark) {
            changedByUser = true;
			heat.marks[coupleIndex] = mark;

            switch(previousMark)
            {
                case 1: dance.markCount--;
                    break;
                case 2: dance.helpmark1Count--;
                    break;
                case 3: dance.helpmark2Count--;
            }                   
            
            switch(mark)
            {
                case 1: dance.markCount++;
                    break;
                case 2: dance.helpmark1Count++;
                    break;
                case 3: dance.helpmark2Count++;
            }
            
            MessageManager.setCurrentStatusMessage(TPSMessage.newStatusMessage(1, 0, danceIndex + 1, heatIndex + 1));
			boolean added = MessageManager.offer(TPSMessage.newJudgingMessage(heat.couples[coupleIndex], danceIndex + 1, mark));
			if(!added) {
				Log.e(TAG, "Could not add message to queue!");
				// TODO maybe show Toast to user?
			}
			save();
			reportChanges(SET_MARK);
		}
	}

	public void setContext(final Context context) {
		this.context = context;
	}

	public boolean isClosing() {
		return closing;
	}
}
