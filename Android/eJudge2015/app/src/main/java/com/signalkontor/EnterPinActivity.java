package com.signalkontor;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.signalkontor.data.Tournament;
import com.signalkontor.messaging.MessageManager;
import com.signalkontor.utils.LocaleUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Pattern;

public class EnterPinActivity extends Activity implements View.OnClickListener {

    private static final String TAG = "EnterPinActivity";

    private ProgressDialog progressDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            // setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
        }catch (Exception ex){}

        setContentView(R.layout.enter_pin);

        final Button button = (Button) findViewById(R.id.okbutton);
        TextView pinCodeView = (TextView) findViewById(R.id.pinText);
        button.setOnClickListener(this);

        pinCodeView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    onClick(button);
                }
                return false;
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch(v.getId())
        {
            case R.id.okbutton:
                final EditText pinView = (EditText) findViewById(R.id.pinText);
                this.progressDialog = ProgressDialog.show(this, "Please wait...", "Loading data!");
                // Run in a new Thread:
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        checkDataForPin(pinView.getText().toString());
                    }
                });
                thread.start();
                break;
        }
    }
    @Override
    public void onBackPressed() {
        this.switchToActivity(StartActivity.class);
    }


    private void showMessage(final String message)
    {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView view = (TextView) findViewById(R.id.resultMessage);
                view.setText(message);
            }
        });
    }

    private void checkDataForPin(String pin)
    {
        try{
            StringBuilder builder = new StringBuilder();
            builder.append("http:/")
                    .append(MessageManager.getServerIpAddress())
                    .append(":9100/GetData/")
                    .append(pin)
                    .append("?device=")
                    .append(MessageManager.getDeviceId());


            URL url = new URL(builder.toString());
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(10000);
            connection.setReadTimeout(10000);
            connection.setRequestMethod("GET");

            connection.connect();
            InputStream stream = connection.getInputStream();

            if(connection.getResponseCode() == 404)
            {
                this.showMessage("No data found, are you really judging?");
                return;
            }

            BufferedReader buffered = new BufferedReader(new InputStreamReader(stream));
            builder = new StringBuilder();
            String line;
            while((line = buffered.readLine()) != null)
            {
                builder.append(line);
            }
            // Now we should have the data -> Load it ...
            final Tournament tournament = Tournament.getInstance();
            String dataSend = builder.toString();

            final LinearLayout linearLayout = (LinearLayout)  this.findViewById(R.id.layoutPin);

            if(dataSend.startsWith("{"))
            {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.dismiss();
                        progressDialog = null;
                        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputMethodManager.toggleSoftInputFromWindow(linearLayout.getApplicationWindowToken(), InputMethodManager.SHOW_IMPLICIT, 0);
                        inputMethodManager.toggleSoftInputFromWindow(linearLayout.getApplicationWindowToken(), InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                    }
                });

                this.switchToList(dataSend, pin);

                return;
            }

            String[] data = dataSend.split(Pattern.quote("|"));
            tournament.newTournamentData(data[0], data[1], MessageManager.getDeviceId());

            if(data.length > 2 && data[2].length() > 0) {
                tournament.restoreJudgmentData(data[2], tournament.getRoundType() == 2);
            }

            LocaleUtils.changeLocale(this, tournament.getLanguage());

            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressDialog.dismiss();
                    progressDialog = null;
                    InputMethodManager inputMethodManager=(InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputMethodManager.toggleSoftInputFromWindow(linearLayout.getApplicationWindowToken(), InputMethodManager.SHOW_IMPLICIT, 0);
                    inputMethodManager.toggleSoftInputFromWindow(linearLayout.getApplicationWindowToken(), InputMethodManager.HIDE_IMPLICIT_ONLY, 0);



                    switch(tournament.getRoundType()) {
                        case Tournament.ROUND_TYPE_PRELIMINARY:
                            switchToActivity(PagerActivity.class);
                            break;
                        case Tournament.ROUND_TYPE_FINAL:
                            switchToActivity(FinalsActivity.class);
                            break;
                        default:

                            Log.e(TAG, "Unknown round type " + tournament.getRoundType());
                            //System.exit(0);
                    }
                }
            });
        }
        catch(MalformedURLException malformedException)
        {
            this.showMessage("Malformed URL exception");
        }
        catch(FileNotFoundException fileNotFoundException)
        {
            this.showMessage("No data found, are you really judging?");
        }
        catch(IOException ioException)
        {
            this.showMessage("IO Exception while opening connection: " + ioException.getMessage());
        }
        catch(Exception exception)
        {
            StackTraceElement[] stackTrace = exception.getStackTrace();
            String message = "";

            for (int i=0;i<stackTrace.length;i++)
            {
                message += stackTrace[i].getClassName() + " " + stackTrace[i].getMethodName() + " " + stackTrace[i].getLineNumber();
            }

            this.showMessage("Internal Error: " + exception.getMessage() + "\r\n" + message);
        }
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(progressDialog != null) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }
            }
        });

    }

    private void switchToActivity(Class nextActivityClass) {
        startActivity(new Intent(this, nextActivityClass));
        finish();
    }

    private void switchToList(String jsonData, String pin){
        // Get the data from the json string
        try{
            JSONObject jObject = new JSONObject(jsonData);
            JSONArray idArray = jObject.getJSONArray("roundIds");
            JSONArray roundNamesArray = jObject.getJSONArray("roundNames");

            int[] ids = new int[idArray.length()];
            String[] roundNames = new String[idArray.length()];

            for (int i=0; i < idArray.length(); i++)
            {
                ids[i] = idArray.getInt(i);
                roundNames[i] = roundNamesArray.getString(i);
            }

            Intent intent = new Intent(this, SelectRoundActivity.class);
            intent.putExtra("Ids", ids);
            intent.putExtra("Names", roundNames);
            intent.putExtra("PIN", pin);
            startActivity(intent);
            finish();
        }
        catch(JSONException jsonException)
        {
            this.showMessage("Error parsing the json result from the server");
        }

    }
}
