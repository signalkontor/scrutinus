package com.signalkontor;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.signalkontor.data.Tournament;
import com.signalkontor.messaging.MessageManager;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Pattern;

public class SelectRoundActivity extends ListActivity {

    private int[] ids;

    private String[] roundNames;

    private String pin;

    private ProgressDialog progressDialog = null;

    private ArrayAdapter<String> mAdapter;

    private static final String TAG = "SelectRoundActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_round);

        Bundle extras = getIntent().getExtras();

        if(extras != null){
            this.ids = extras.getIntArray("Ids");
            this.roundNames = extras.getStringArray("Names");
            this.pin = extras.getString("PIN");
        }

        mAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_2, android.R.id.text1, roundNames) {

            public View getView(int position, View convertView, ViewGroup parent) {

                View view = super.getView(position, convertView, parent);

                TextView text1 = (TextView) view.findViewById(android.R.id.text1);
                TextView text2 = (TextView) view.findViewById(android.R.id.text2);

                String[] data = roundNames[position].split(Pattern.quote("/"));

                text1.setText(data[0]);
                text2.setText(data[1]);

                return view;
            }
        };

        setListAdapter(mAdapter);

    }

    @Override
    public void onListItemClick(ListView list, View view, int position, long id){
        final int roundId = ids[position];
        // Get the data now and create the competition

        this.progressDialog = ProgressDialog.show(this, "Please wait...", "Loading data!");

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                checkDataForRound(roundId);
            }
        });
        thread.start();
    }

    private void checkDataForRound(int roundId)
    {
        try{
            StringBuilder builder = new StringBuilder();
            builder.append("http:/")
                    .append(MessageManager.getServerIpAddress())
                    .append(":9100/SetDevice/")
                    .append(this.pin)
                    .append("?device=")
                    .append(MessageManager.getDeviceId())
                    .append("&roundid=")
                    .append(roundId);



            URL url = new URL(builder.toString());
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(10000);
            connection.setReadTimeout(10000);
            connection.setRequestMethod("GET");

            connection.connect();
            InputStream stream = connection.getInputStream();

            if(connection.getResponseCode() == 404)
            {
                this.showMessage("No data found, are you really judging?");
                return;
            }

            BufferedReader buffered = new BufferedReader(new InputStreamReader(stream));
            builder = new StringBuilder();
            String line;
            while((line = buffered.readLine()) != null)
            {
                builder.append(line);
            }
            // Now we should have the data -> Load it ...
            final Tournament tournament = Tournament.getInstance();
            String dataSend = builder.toString();

            String[] data = dataSend.split(Pattern.quote("|"));
            tournament.newTournamentData(data[0], data[1], MessageManager.getDeviceId());

            if(data.length > 2 && data[2].length() > 0) {
                tournament.restoreJudgmentData(data[2], tournament.getRoundType() == 2);
            }

            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(progressDialog != null){
                        progressDialog.dismiss();
                    }

                    progressDialog = null;

                    switch(tournament.getRoundType()) {
                        case Tournament.ROUND_TYPE_PRELIMINARY:
                            switchToActivity(PagerActivity.class);
                            break;
                        case Tournament.ROUND_TYPE_FINAL:
                            switchToActivity(FinalsActivity.class);
                            break;
                        default:

                            Log.e(TAG, "Unknown round type " + tournament.getRoundType());
                            //System.exit(0);
                    }
                }
            });
        }
        catch(MalformedURLException malformedException)
        {
            this.showMessage("Malformed URL exception");
        }
        catch(FileNotFoundException fileNotFoundException)
        {
            this.showMessage("No data found, are you really judging?");
        }
        catch(IOException ioException)
        {
            this.showMessage("IO Exception while opening connection: " + ioException.getMessage());
        }
        catch(Exception exception)
        {
            StackTraceElement[] stackTrace = exception.getStackTrace();
            String message = "";

            for (int i=0;i<stackTrace.length;i++)
            {
                message += stackTrace[i].getClassName() + " " + stackTrace[i].getMethodName() + " " + stackTrace[i].getLineNumber();
            }

            this.showMessage("Internal Error: " + exception.getMessage() + "\r\n" + message);
        }
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }
            }
        });

    }

    private void showMessage(String message)
    {
        final String msg = message;

        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Context context = getApplicationContext();
                CharSequence text = msg;
                int duration = Toast.LENGTH_LONG;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
        });
    }
        private void switchToActivity(Class nextActivityClass) {
                startActivity(new Intent(this, nextActivityClass));
                finish();
            }
        }
