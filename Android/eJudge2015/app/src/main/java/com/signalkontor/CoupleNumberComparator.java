package com.signalkontor;

import java.util.Comparator;

/**
 * Created by groehnol on 29.03.2016.
 */
public class CoupleNumberComparator implements Comparator<String> {

    @Override
    public int compare(String lhs, String rhs) {
        int leftInt = Integer.parseInt(lhs);
        int rightInt = Integer.parseInt(rhs);

        if(leftInt == rightInt){
            return 0;
        }
        if(leftInt < rightInt)
        {
            return -1;
        }

        return 1;
    }
}
