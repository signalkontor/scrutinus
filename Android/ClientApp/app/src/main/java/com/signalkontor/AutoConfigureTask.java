package com.signalkontor;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;

import com.github.droidfu.concurrent.BetterAsyncTask;
import com.signalkontor.messaging.MessageManager;
import com.signalkontor.messaging.NoWifiException;

import java.net.BindException;

public class AutoConfigureTask extends BetterAsyncTask<Object, Context, Exception> {

    public static AutoConfigureTask runningInstance = null;
    public static final Object SingleInstanceLocker = new Object();

    public boolean running = true;

	public AutoConfigureTask(Context context) {
		super(context);

        synchronized (SingleInstanceLocker) {
            if(runningInstance != null)
                runningInstance.running = false;
            runningInstance = this;
        }
	}

	@Override
	protected Exception doCheckedInBackground(Context context, Object... params) {
		try
		{
			publishProgress(context);
			while(running && !MessageManager.initialize(context))
			{
				publishProgress(context);
				Thread.sleep(2000);
			}
            if(running)
            {
			    publishProgress(context);
            }
        } catch(BindException e) {
            // ignore
		} catch(Exception e) {
			return e;
		}
		return null;
	}
	
    // TODO use oberserver pattern instead casting the context in an if-else-if-chain
	private void updateStatusInfo(Context context) {
        if(context instanceof StartActivity)
        {
		    StartActivity startActivity = (StartActivity)context;
		    startActivity.updateStatusText();
        } else if(context instanceof PagerActivity) {
            PagerActivity pagerActivity = (PagerActivity)context;
            pagerActivity.updateTitle();
        } else if(context instanceof FinalsActivity) {
            FinalsActivity finalsActivity = (FinalsActivity)context;
            finalsActivity.updateTitle();
        }
	}	
	
	@Override
	protected void handleError(Context context, Exception error) {
	}

	@Override
	protected void after(Context context, Exception result) {
		if(!(context instanceof StartActivity))
			return; // only show messages if called from StartActivity

		final StartActivity startActivity = (StartActivity)context;
		startActivity.getProgressDialog().dismiss();
		
		if(result == null)
			return; // no exceptions everything OK
		
		if(result instanceof NoWifiException) {
            NoWifiException ex = (NoWifiException) result;
            String stack = "";
            for(int i=0;i<ex.getStackTrace().length;i++)
            {
                stack += ex.getStackTrace()[i];
            }

            AlertDialog dlg = new AlertDialog.Builder(startActivity)
				.setTitle(R.string.noWifiDialogTitle)
				.setMessage(ex.getMessage() + " " + stack)
				.create();
			dlg.setButton(AlertDialog.BUTTON_POSITIVE,
						  context.getResources().getString(R.string.noWifiDialogButton),
						  new OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {
								startActivity.finish();
							}
						  });
			dlg.show();
		} else {
			new AlertDialog.Builder(startActivity)
				.setTitle(R.string.unknownErrorDialogTitle)
				.setMessage(R.string.unknownErrorDialogMessage)
				.setOnCancelListener(new OnCancelListener() {
					public void onCancel(DialogInterface dialog) {
						startActivity.finish();
					}
				}).show();
		}	
	}

	@Override
	protected void onProgressUpdate(Context... values) {
		updateStatusInfo(values[0]);
	}
}
