package com.signalkontor.utils;

import com.signalkontor.data.Tournament;

import java.util.Collection;
import java.util.Iterator;
import java.util.zip.ZipEntry;

/**
 * User: Mikko
 * Date: 09.06.11
 * Time: 18:12
 */
public class StringUtils {

    public static String joinWithAdditionalDelimiterAtEnd(Object[] s, String delimiter) {
        StringBuilder builder = new StringBuilder();
        for (Object value : s) {
            builder.append(value);
            builder.append(delimiter);
        }
        return builder.toString();
    }

}
