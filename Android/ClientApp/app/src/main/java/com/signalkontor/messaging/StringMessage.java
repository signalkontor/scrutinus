package com.signalkontor.messaging;

import org.json.JSONException;

public class StringMessage extends COMMessage {
	private static final String PROPERTY_MESSAGE = "Message";
	
	public StringMessage() {
		setAppId(2);
	}
	
	public String getMessage() {
		try { return jsonObject.getString(PROPERTY_MESSAGE); } catch (JSONException e) { return null; }
	}
	
	public void setMessage(String message) {
		try { jsonObject.put(PROPERTY_MESSAGE, message); } catch (JSONException ignored) { }
	}
}
