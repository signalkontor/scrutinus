package com.signalkontor.messaging;

import org.json.JSONException;

public class LogonMessage extends COMMessage {
	
	private static final String PROPERTY_USER = "User";
	private static final String PROPERTY_PASSWORD = "Password";

	public LogonMessage() {
		setAppId(1);
	}
	
	public LogonMessage(String user, String password) {
		setAppId(1);
		setUser(user);
		setPassword(password);
	}
	
	public void setUser(String user) {
		try { jsonObject.put(PROPERTY_USER, user); } catch (JSONException ignored) { }
	}
	
	public void setPassword(String password) {
		try { jsonObject.put(PROPERTY_PASSWORD, password); } catch (JSONException ignored) { }
	}
}
