package com.signalkontor;

import java.sql.Time;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Timer;
import java.util.TimerTask;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.*;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.github.droidfu.activities.BetterDefaultActivity;
import com.signalkontor.constants.Constants;
import com.signalkontor.data.Tournament;
import com.signalkontor.messaging.MessageManager;
import com.signalkontor.messaging.TPSMessage;
import com.signalkontor.utils.BatteryUtils;
import com.signalkontor.utils.LocaleUtils;

public class StartActivity extends BetterDefaultActivity implements OnClickListener, Observer, Restartable {

    private static final String TAG = "StartActivity";

    private TextView accessPointTextView, deviceIdTextView, stateTextView, tournamentTextView, judgeTextView;
	private ProgressDialog progressDialog;
	private View startButton, languageSelectorButton;

    private final Object CALLED_FROM_ONRESUME = new Object();
    private final Object CALLED_FROM_ONRESUME_LANGUAGE_CHANGE = new Object();

    private static Thread statusThread;
    private static boolean statusThreadRunning;

    private List<String> judgesList;
/*    private final Object statusMessageTimerLocker = new Object();
    private Timer statusMessageTimer = new Timer();*/

    /** Called when the activity is first created. */
	@Override
	public void onResume() {
		super.onResume();
        BatteryUtils.startReceiving(this);
        statusThreadRunning = true;
        if(statusThread == null)
        {
            statusThread = 	new Thread(new Runnable() {
                public void run() {
                    StatusSenderThread();
                }
            });
            statusThread.start();
        }
		if (isLaunching()) {
			Tournament.restore(this);
            final Tournament tournament = Tournament.getInstance();
            final Bundle extras = getIntent().getExtras();
            final boolean changedLanguage = extras != null && extras.getBoolean(Constants.EXTRA_CHANGED_LANGUAGE_INDEX, false);
            final boolean fromJudging = extras != null && extras.getBoolean(Constants.EXTRA_FROM_JUDGING_INDEX, false);

            setContentView(R.layout.main);
			if(tournament.isStarted() && !changedLanguage && (!tournament.isFinished() || !fromJudging)) {
                startJudging();
			}
			else {
                accessPointTextView = (TextView)findViewById(R.id.startAP);
                deviceIdTextView = (TextView)findViewById(R.id.startDeviceID);
                stateTextView = (TextView)findViewById(R.id.startState);
                tournamentTextView = (TextView)findViewById(R.id.startCompetition);
                judgeTextView = (TextView)findViewById(R.id.startJudge);

                languageSelectorButton = findViewById(R.id.languageSelectorButton);
                languageSelectorButton.setOnClickListener(this);

                startButton = findViewById(R.id.startButton);
				startButton.setOnClickListener(this);

                tournament.addObserver(this);
                update(tournament, changedLanguage ? CALLED_FROM_ONRESUME_LANGUAGE_CHANGE : CALLED_FROM_ONRESUME);

                judgesList = new ArrayList<String>();
                judgesList.add("A) Test Test");
                judgesList.add("B) Test2 Test");
                ListAdapter adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, judgesList);
                final ListView lv = (ListView)findViewById(R.id.listOfJudges);

                lv.setAdapter(adapter);

                if(!MessageManager.isInitialized()) {
					setProgressDialog(ProgressDialog.show(this, "", getText(R.string.progressDialogMessage), true));
					AutoConfigureTask autoConfigureTask = new AutoConfigureTask(this);
					autoConfigureTask.disableDialog();
					autoConfigureTask.execute((Object[]) null);
				} else {
					updateStatusText();
				}
			}
		}
	}

    @Override
    public void onPause() {
        BatteryUtils.stopReceiving(this);
        super.onPause();
    }

    private void StatusSenderThread()
    {
        while(statusThreadRunning)
        {
            try{

                final Tournament tournament = Tournament.getInstance();
                if(!tournament.isStarted())
                {
                    if(tournament.isCleared())
                    {
                        MessageManager.setCurrentStatusMessage(TPSMessage.newStatusMessage(0, 0, 0, 0));
                    }
                    else
                    {
                        MessageManager.setCurrentStatusMessage(TPSMessage.newStatusMessage(0, 1, 0, 0));
                    }
                }
                else
                {
                    if(tournament.isFinished()) {
                        MessageManager.setCurrentStatusMessage(TPSMessage.newStatusMessage(0, 3, 0, 0));
                    } else {
                        MessageManager.setCurrentStatusMessage(TPSMessage.newStatusMessage(0, 2, 0, 0));
                    }
                }

                Thread.sleep(10000);
            }catch(InterruptedException ex)
            {
                // Wir sollen wohl beendet werden ...
                return;
            }
        }
        Log.v("Start", "Thread beendet")   ;
    }

    private void updateStateText() {
        final Tournament tournament = Tournament.getInstance();
        if(tournament.isStarted()) {
            if(tournament.isFinished()) {
                stateTextView.setText(R.string.startStateFinished);
                MessageManager.setCurrentStatusMessage(TPSMessage.newStatusMessage(0, 3, 0, 0));
            } else {
                stateTextView.setText(R.string.startStateDataPlusStart);
                MessageManager.setCurrentStatusMessage(TPSMessage.newStatusMessage(0, 2, 0, 0));
            }
        } else {
            if(tournament.isCleared()) {
                stateTextView.setText(R.string.startStateNoData);
                MessageManager.setCurrentStatusMessage(TPSMessage.newStatusMessage(0, 0, 0, 0));
            } else {
                stateTextView.setText(R.string.startStateData);
                MessageManager.setCurrentStatusMessage(TPSMessage.newStatusMessage(0, 1, 0, 0));
            }
        }

        /*synchronized (statusMessageTimerLocker)
        {
            statusMessageTimer.cancel();
            statusMessageTimer = new Timer();
            statusMessageTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    final Tournament tournament = Tournament.getInstance();
                    if(tournament.isStarted()) {
                        if(tournament.isFinished()) {
                            MessageManager.setCurrentStatusMessage(TPSMessage.newStatusMessage(0, 3, 0, 0));
                        } else {
                            MessageManager.setCurrentStatusMessage(TPSMessage.newStatusMessage(0, 2, 0, 0));
                        }
                    } else {
                        if(tournament.isCleared()) {
                            MessageManager.setCurrentStatusMessage(TPSMessage.newStatusMessage(0, 0, 0, 0));
                        } else {
                            MessageManager.setCurrentStatusMessage(TPSMessage.newStatusMessage(0, 1, 0, 0));
                        }
                    }
                }
            }, 15000, 15000);
        } */
    }
	
	public void updateStatusText() {
		accessPointTextView.setText(MessageManager.getSSID());
		deviceIdTextView.setText(MessageManager.getDeviceId());
        updateStateText();
	}

	@Override
	public void onConfigurationChanged(Configuration configuration) {
		super.onConfigurationChanged(configuration);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        final Configuration c = new Configuration();
        c.locale = LocaleUtils.currentLocale;
        getBaseContext().getResources().updateConfiguration(c, getBaseContext().getResources().getDisplayMetrics());
	}

    @Override
    public void onAttachedToWindow()
    {
           this.getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD);
           super.onAttachedToWindow();
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
            .setMessage(R.string.quitApplication)
            .setPositiveButton(R.string.no, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    System.exit(0);
                }
            })
            .setNegativeButton(R.string.yes, null)
            .show();
    }

	private void setProgressDialog(ProgressDialog progressDialog) {
		this.progressDialog = progressDialog;
	}

	public ProgressDialog getProgressDialog() {
		return progressDialog;
	}

    public void restart() {
        // reload activity to get new language
        Tournament.getInstance().deleteObserver(this);
        finish();
        startActivity(new Intent(this, StartActivity.class).putExtra(Constants.EXTRA_CHANGED_LANGUAGE_INDEX, true));
    }

    private void switchToActivity(Class nextActivityClass) {
        // Stop Background Thread
        if(statusThread != null)
        {
            statusThreadRunning = false;
            statusThread = null;
        }
        statusThreadRunning = false;
        // remove us from the observer list to prevent dangling references
        Tournament.getInstance().deleteObserver(this);
/*        synchronized (statusMessageTimerLocker) {
            statusMessageTimer.cancel();
        }*/
        startActivity(new Intent(this, nextActivityClass));
        finish();
    }

    private void startJudging() {
        final Tournament tournament = Tournament.getInstance();
        switch(tournament.getRoundType()) {
            case Tournament.ROUND_TYPE_PRELIMINARY:
                switchToActivity(PagerActivity.class);
                break;
            case Tournament.ROUND_TYPE_FINAL:
                switchToActivity(FinalsActivity.class);
                break;
            default:

                Log.e(TAG, "Unknown round type " + tournament.getRoundType());
                System.exit(0);
        }
    }

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.startButton:
			startJudging();
			break;
        case R.id.languageSelectorButton:
/*            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.setClassName("com.android.settings", "com.android.settings.LanguageSettings");
            startActivity(intent);*/

		    new AlertDialog.Builder(this)
                .setItems(R.array.languages, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        final int languageId = which + 1;
                        Tournament.getInstance().setLanguage(languageId);
                        LocaleUtils.changeLocale(StartActivity.this, languageId);
                    }
                })
                .show();
            break;
		}
	}

	public void update(Observable observable, Object data) {
		if(!(observable instanceof Tournament)) {
            return; // for safety only, should never be any different
        }

        Tournament t = (Tournament)observable;
        if(t.isClosing()) {
            t.deleteObserver(this); // this instance is old, we don't want to observe it any longer
            t = Tournament.getInstance();
            t.addObserver(this); // instead we observe the new one
        }

        final Tournament tournament = t;

        startButton.post(new Runnable() {
            @Override
            public void run() {
                startButton.setEnabled(tournament.isStarted() && !tournament.isFinished());
            }
        });

        languageSelectorButton.post(new Runnable() {
            @Override
            public void run() {
                int visibility;
                if(tournament.showLanguageDialog() && !tournament.isFinished()) {
                    visibility = View.VISIBLE;
                } else {
                    visibility = View.INVISIBLE;
                }
                languageSelectorButton.setVisibility(visibility);
            }
        });

        stateTextView.post(new Runnable() {
            @Override
            public void run() {
                updateStateText();
            }
        });

        tournamentTextView.post(new Runnable() {
            public void run() {
                tournamentTextView.setText(tournament.getFullTitle());
            }
        });

        tournamentTextView.post(new Runnable() {
            public void run() {
                judgeTextView.setText(tournament.getJudgeFullName());
            }
        });

        // only change language if new tournament data was received
        if(data == Tournament.NEW_TOURNAMENT_DATA) {
            // set language received with tournament data
            LocaleUtils.changeLocale(this, tournament.getLanguage());
        }
	}
}
