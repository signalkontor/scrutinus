package com.signalkontor.messaging;

import org.json.JSONException;
import org.json.JSONObject;

public class COMMessage {
	private static final String PROPERTY_APP_ID = "AppID";
	protected JSONObject jsonObject = new JSONObject();
		
	public String toJson() {
		return jsonObject.toString();
	}
	
	public COMMessage with(JSONObject jsonObject) {
		this.jsonObject = jsonObject;
		return this;
	}
	
	public static COMMessage fromJson(String data) {
		try {
			COMMessage comMessage;
			JSONObject jsonObject = new JSONObject(data);
			
			switch(jsonObject.getInt(PROPERTY_APP_ID)) {
			case 1:
				comMessage = new LogonMessage().with(jsonObject);
				break;
			case 2:
				comMessage = new StringMessage().with(jsonObject);
				break;
			case 120:
				comMessage = new TPSMessage().with(jsonObject);
				break;
			default:
				comMessage = new COMMessage().with(jsonObject);
			}
			return comMessage;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	public void setAppId(int appId) {
		try { jsonObject.put(PROPERTY_APP_ID, appId); } catch (JSONException ignored) { }
	}

	public int getAppId() {
		try { return jsonObject.getInt(PROPERTY_APP_ID); } catch (JSONException e) { return 0; }
	}
}
