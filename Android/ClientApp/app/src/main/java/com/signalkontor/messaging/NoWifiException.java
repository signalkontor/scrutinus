package com.signalkontor.messaging;

public class NoWifiException extends IllegalStateException {
	private static final long serialVersionUID = 272662147082467779L;
	
	public NoWifiException() {
		super("no WiFi connection");
	}

    public NoWifiException(String errorMessage)
    {
        super(errorMessage);
    }
}
