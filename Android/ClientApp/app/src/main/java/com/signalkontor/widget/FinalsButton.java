package com.signalkontor.widget;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * User: Mikko
 * Date: 14.06.11
 * Time: 14:42
 */
public class FinalsButton extends Button {

    private boolean crossed;
    private int rowIndex = -1, columnIndex = -1, locks = 0;

    public FinalsButton(Context context) {
        super(context);
    }

    public int getRowIndex() {
        return rowIndex;
    }

    public void setRowIndex(int rowIndex) {
        this.rowIndex = rowIndex;
    }

    public int getColumnIndex() {
        return columnIndex;
    }

    public void setColumnIndex(int columnIndex) {
        this.columnIndex = columnIndex;
    }

    public void updateLocks(boolean increase) {
        if(increase)
            locks++;
        else
            locks--;

        update();
    }

    public boolean toggleCrossed() {
        crossed = !crossed;
        return crossed;
    }

    private void update() {
        final int color = crossed ? Color.GREEN : locks > 0 ? Color.RED : Color.WHITE;
        getBackground().setColorFilter(color, PorterDuff.Mode.MULTIPLY);
        setEnabled(crossed || locks == 0);
    }
}
