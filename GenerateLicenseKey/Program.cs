﻿// // TPS.net TPS8 GenerateLicenseKey
// // Program.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.IO;
using Helpers;

namespace GenerateLicenseKey
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var inputFile = args[0];

            var outputFile = args[1];

            var source = File.ReadAllText(inputFile);

            var enrypted = EncryptionHelper.Encrypt(source, "jhfeihi corjwoh h ercwer");

            File.WriteAllText(outputFile, enrypted);
        }
    }
}
