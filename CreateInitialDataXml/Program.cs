﻿// // TPS.net TPS8 CreateInitialDataXml
// // Program.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;

namespace CreateInitialDataXml
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            if (args.Length < 2)
            {
                Console.WriteLine("Wrong ussage");
                return;
            }

            var generator = new CreateXmlFromExcel(args[0], args[1]);
            generator.CreateOutput();
        }
    }
}
