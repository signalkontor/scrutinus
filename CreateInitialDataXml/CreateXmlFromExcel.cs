﻿// // TPS.net TPS8 CreateInitialDataXml
// // CreateXmlFromExcel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using DataModel.Models;
using OfficeOpenXml;

namespace CreateInitialDataXml
{
    public class RoundTypeModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public bool IsFinal { get; set; }

        public bool Draw { get; set; }

        public string MarkingType { get; set; }
    }

    public class DanceTamplate
    {
        public DanceTamplate()
        {
            this.Dances = new List<Dance>();
        }

        public int ClassId { get; set; }

        public int SectionId { get; set; }

        public List<Dance> Dances { get; set; }
    }

    internal class CreateXmlFromExcel
    {
        private List<AgeGroup> ageGroups;

        private List<Class> classes;

        private List<CompetitionType> competitionTypes;

        private List<Dance> dances;

        private List<DanceTamplate> danceTamplates;
        private readonly string excelFile;

        private readonly string outputFile;

        private List<CompetitionRule> rules;

        private List<Section> sections;

        public CreateXmlFromExcel(string excelFile, string outputFile)
        {
            this.excelFile = excelFile;
            this.outputFile = outputFile;
        }

        public void CreateOutput()
        {
            var excelPackage = new ExcelPackage(new FileInfo(this.excelFile));

            var fileInfo = new FileInfo(this.excelFile);

            var content = new XElement("Content", new XAttribute("RuleName", fileInfo.Name));


            this.CreateSections(excelPackage.Workbook.Worksheets["Sections"], content);
            this.CreateDances(excelPackage.Workbook.Worksheets["Dances"], content);
            this.CreateRoles(excelPackage.Workbook.Worksheets["Roles"], content);
            this.CreateRoundTypes(excelPackage.Workbook.Worksheets["Round Types"], content);
            this.CreateAgeGroups(excelPackage.Workbook.Worksheets["Age Groups"], content);
            this.CreateClasses(excelPackage.Workbook.Worksheets["Classes"], content);
            this.CreateCompetitionRules(excelPackage.Workbook.Worksheets["Rules"], content);
            this.CreateCompetitionTypes(excelPackage.Workbook.Worksheets["Competition Types"], content);
            this.CreateDanceTemplates(excelPackage.Workbook.Worksheets["Groups Classes Dances"], content);
            this.CreateSectionAgeGroupClassesMappings(content);

            var document = new XDocument(content);
            document.Save(this.outputFile);
        }

        private void CreateCompetitionRules(ExcelWorksheet sheet, XElement content)
        {
            this.rules = new List<CompetitionRule>();
            var line = 2;

            while (!string.IsNullOrEmpty(sheet.Cells[line, 1].Text))
            {
                this.rules.Add(new CompetitionRule()
                {
                    Id = int.Parse(sheet.Cells[line, 1].Text), RuleName = sheet.Cells[line, 2].Text,
                    RedanceRequired = sheet.Cells[line, 3].Text == "X",
                    ChairmanRequired = sheet.Cells[line, 4].Text == "X",
                    SupervisorRequired = sheet.Cells[line, 5].Text == "X",
                    AssosiateRequired = sheet.Cells[line, 6].Text == "X",
                    MinimumJudgeNumber = int.Parse(sheet.Cells[line, 7].Text)
                });
                line++;
            }

            content.Add(new XElement("CompetitionRules", this.rules.Select(d => new XElement("CompetitionRule",
                new XAttribute("Id", d.Id),
                new XAttribute("RuleName", d.RuleName),
                new XAttribute("FirstRoundTypeId", "1"),
                new XAttribute("MarkingType", "1"),
                new XAttribute("RedanceRequired", d.RedanceRequired),
                new XAttribute("ChairmanRequired", d.ChairmanRequired),
                new XAttribute("SupervisorRequired", d.SupervisorRequired),
                new XAttribute("AssosiateRequired", d.AssosiateRequired),
                new XAttribute("MinimumJudgeNumber", d.MinimumJudgeNumber),
                new XAttribute("SecondFirstRound", "False")
                ))));
        }

        private void CreateCompetitionTypes(ExcelWorksheet sheet, XElement content)
        {
            this.competitionTypes = new List<CompetitionType>();
            var line = 2;

            while (!string.IsNullOrEmpty(sheet.Cells[line, 1].Text))
            {
                this.competitionTypes.Add(new CompetitionType()
                {
                    Id = int.Parse(sheet.Cells[line, 1].Text),
                    Federation = sheet.Cells[line, 2].Text,
                    TypeString = sheet.Cells[line, 3].Text,
                    CompetitionKind = "Single Competition",
                    CompetitionRule = this.rules.First(r => r.RuleName == sheet.Cells[line, 4].Text)
                });
                line++;
            }

            content.Add(new XElement("CompetitionTypes", this.competitionTypes.Select(d => new XElement("CompetitionType",
                new XAttribute("Id", d.Id),
                new XAttribute("TypeString", d.TypeString),
                new XAttribute("Federation", d.Federation),
                new XAttribute("CompetitionKind", d.CompetitionKind),
                new XAttribute("CompetitionRuleId", d.CompetitionRule.Id)
                ))));
        }

        private void CreateDanceTemplates(ExcelWorksheet sheet, XElement content)
        {
            this.danceTamplates = new List<DanceTamplate>();

            var line = 2;

            while (!string.IsNullOrEmpty(sheet.Cells[line, 1].Text))
            {
                var template = new DanceTamplate();
                this.danceTamplates.Add(template);

                template.ClassId = int.Parse(sheet.Cells[line, 3].Text);
                template.SectionId = int.Parse(sheet.Cells[line, 4].Text);

                if (sheet.Cells[line, 5].Text.ToLower() == "x")
                {
                    template.Dances.Add(this.dances[0]);
                }
                if (sheet.Cells[line, 6].Text.ToLower() == "x")
                {
                    template.Dances.Add(this.dances[1]);
                }
                if (sheet.Cells[line, 7].Text.ToLower() == "x")
                {
                    template.Dances.Add(this.dances[2]);
                }
                if (sheet.Cells[line, 8].Text.ToLower() == "x")
                {
                    template.Dances.Add(this.dances[3]);
                }
                if (sheet.Cells[line, 9].Text.ToLower() == "x")
                {
                    template.Dances.Add(this.dances[4]);
                }
                if (sheet.Cells[line, 10].Text.ToLower() == "x")
                {
                    template.Dances.Add(this.dances[5]);
                }
                if (sheet.Cells[line, 11].Text.ToLower() == "x")
                {
                    template.Dances.Add(this.dances[6]);
                }
                if (sheet.Cells[line, 12].Text.ToLower() == "x")
                {
                    template.Dances.Add(this.dances[7]);
                }
                if (sheet.Cells[line, 13].Text.ToLower() == "x")
                {
                    template.Dances.Add(this.dances[8]);
                }
                if (sheet.Cells[line, 14].Text.ToLower() == "x")
                {
                    template.Dances.Add(this.dances[9]);
                }

                line++;
            }

            content.Add(new XElement("DanceTemplates", this.danceTamplates.Select(t => new XElement("DanceTemplate",
                new XAttribute("SectionId", t.SectionId),
                new XAttribute("ClassId", t.ClassId),
                new XAttribute("Dances", string.Join(",", t.Dances.Select(d => d.Id)))
                ))));
        }

        private void CreateSectionAgeGroupClassesMappings(XElement content)
        {
            var mapping = new XElement("SectionAgeGroupClassesMappings");
            

            foreach (var ageGroup in this.ageGroups)
            {
                foreach (var section  in this.sections)
                {
                    mapping.Add(new XElement("SectionAgeGroupClassesMapping",
                        new XAttribute("SectionId", section.Id),
                        new XAttribute("AgeGroupId", ageGroup.Id),
                        new XAttribute("Classes", string.Join(",", this.danceTamplates.Where(t => t.SectionId == section.Id).Select(t => t.ClassId)))
                        ));   
                }
            }

            content.Add(mapping);
        }

        private void CreateAgeGroups(ExcelWorksheet sheet, XElement content)
        {
            this.ageGroups = new List<AgeGroup>();
            var line = 2;

            while (!string.IsNullOrEmpty(sheet.Cells[line, 1].Text))
            {
                this.ageGroups.Add(new AgeGroup() { Id = int.Parse(sheet.Cells[line, 1].Text), AgeGroupName = sheet.Cells[line, 4].Text, ShortName = sheet.Cells[line, 5].Text });
                line++;
            }

            content.Add(new XElement("AgeGroups", this.ageGroups.Select(d => new XElement("AgeGroup",
                new XAttribute("Id", d.Id),
                new XAttribute("AgeGroupName", d.AgeGroupName),
                new XAttribute("ShortName", d.ShortName)
                ))));
        }

        private void CreateRoundTypes(ExcelWorksheet sheet, XElement content)
        {
            var roles = new List<RoundTypeModel>();
            var line = 2;

            while (!string.IsNullOrEmpty(sheet.Cells[line, 1].Text))
            {
                roles.Add(new RoundTypeModel() { Id = int.Parse(sheet.Cells[line, 1].Text), Name = sheet.Cells[line, 3].Text, IsFinal = sheet.Cells[line, 4].Text == "X", Draw = sheet.Cells[line, 5].Text == "X", MarkingType = sheet.Cells[line, 6].Text});
                line++;
            }

            content.Add(new XElement("RoundTypes", roles.Select(s => new XElement("RoundType",
                new XAttribute("Id", s.Id),
                new XAttribute("Name", s.Name),
                new XAttribute("IsFinal", s.IsFinal),
                new XAttribute("HasDrawing", s.Draw),
                new XAttribute("AllowedMarkingTypes", s.MarkingType)))));
        }

        private void CreateClasses(ExcelWorksheet sheet, XElement content)
        {
            this.classes = new List<Class>();
            var line = 2;

            while (!string.IsNullOrEmpty(sheet.Cells[line, 1].Text))
            {
                this.classes.Add(new Class() { Id = int.Parse(sheet.Cells[line, 1].Text), ClassLongName = sheet.Cells[line, 2].Text, ClassShortName = sheet.Cells[line, 3].Text});
                line++;
            }

            content.Add(new XElement("Classes", this.classes.Select(c => new XElement("Class",
                new XAttribute("Id", c.Id),
                new XAttribute("ClassLongName", c.ClassLongName),
                new XAttribute("ClassShortName", c.ClassShortName),
                new XAttribute("OrderWhenCombined", c.Id)
                ))));
        }

        private void CreateRoles(ExcelWorksheet sheet, XElement content)
        {
            var roles = new List<Role>();
            var line = 2;

            while (!string.IsNullOrEmpty(sheet.Cells[line, 1].Text))
            {
                roles.Add(new Role() { Id = int.Parse(sheet.Cells[line, 1].Text), RoleLong = sheet.Cells[line, 3].Text, RoleShort = sheet.Cells[line, 4].Text });
                line++;
            }

            content.Add(new XElement("Dances", roles.Select(s => new XElement("Role",
                new XAttribute("Id", s.Id),
                new XAttribute("RoleLong", s.RoleLong),
                new XAttribute("RoleShort", s.RoleShort)))));
        }

        private void CreateDances(ExcelWorksheet sheet, XElement content)
        {
            this.dances = new List<Dance>();
            var line = 2;

            while (!string.IsNullOrEmpty(sheet.Cells[line, 1].Text))
            {
                this.dances.Add(new Dance() { Id = int.Parse(sheet.Cells[line, 1].Text), DanceName = sheet.Cells[line, 3].Text, ShortName = sheet.Cells[line, 4].Text });
                line++;
            }

            content.Add(new XElement("Dances", this.dances.Select(s => new XElement("Dance",
                new XAttribute("Id", s.Id),
                new XAttribute("DanceName", s.DanceName),
                new XAttribute("ShortName", s.ShortName),
                new XAttribute("DefaultOrder", s.Id)))));
        }

        private void CreateSections(ExcelWorksheet sheet, XElement content)
        {
            this.sections = new List<Section>();
            var line = 2;

            while (!string.IsNullOrEmpty(sheet.Cells[line, 1].Text))
            {
                this.sections.Add(new Section() { Id = int.Parse(sheet.Cells[line, 1].Text), SectionName = sheet.Cells[line, 3].Text, ShortName = sheet.Cells[line, 4].Text});
                line++;
            }

            content.Add(new XElement("Sections", this.sections.Select(s => new XElement("Section",
                new XAttribute("Id", s.Id),
                new XAttribute("SectionName", s.SectionName),
                new XAttribute("ShortName", s.ShortName)))));
        }
    }
}
