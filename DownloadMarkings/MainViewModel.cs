﻿// // TPS.net TPS8 DownloadMarkings
// // MainViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using Wdsf.Api.Client;
using Wdsf.Api.Client.Models;

namespace DownloadMarkings
{
    public class MainViewModel
    {
        private readonly IList<OfficialDetail> officials;

        private CompetitionDetail competition;

        public MainViewModel()
        {
            this.officials = new List<OfficialDetail>();
            this.StartDownloadCommand = new RelayCommand(this.Download);
        }

        public string DownloadPath { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public int CompetitionId { get; set; }

        public ICommand StartDownloadCommand { get; set; }

        private void Download()
        {
            var client = new Client(this.UserName, this.Password, WdsfEndpoint.Services);

            this.competition = client.GetCompetition(this.CompetitionId);

            var officialsList = client.GetOfficials(this.CompetitionId);

            foreach (var official in officialsList)
            {
                var details = client.GetOfficial(official.Id);
                this.officials.Add(details);
            }

            var participants = client.GetCoupleParticipants(this.CompetitionId);

            foreach (var participant in participants)
            {
                var details = client.GetCoupleParticipant(participant.Id);

                this.WriteResultToFile(details);
            }
        }

        private void WriteResultToFile(ParticipantCoupleDetail results)
        {
            var stream = new StreamWriter($"c:\\temp\\{this.CompetitionId}.txt", true);

            foreach (var round in results.Rounds)
            {
                var lineWritten = false;

                if (!round.Dances.Any())
                {
                    continue;
                }
                if (!round.Dances.First().Scores.Any())
                {
                    continue;
                }
                if (!(round.Dances.First().Scores.First() is OnScale2Score))
                {
                    continue;
                }

                foreach (var dance in round.Dances)
                {
                    foreach (var mark in dance.Scores)
                    {
                        var component = "";
                        decimal points = 0;

                        if (mark is OnScale2Score)
                        {
                            var score = mark as OnScale2Score;
                            if (score.TQ > 0)
                            {
                                component = "TQ";
                                points = score.TQ;
                            }
                            if (score.MM > 0)
                            {
                                component = "MM";
                                points = score.MM;
                            }
                            if (score.PS > 0)
                            {
                                component = "PS";
                                points = score.PS;
                            }
                            if (score.CP > 0)
                            {
                                component = "CP";
                                points = score.CP;
                            }

                            var official = this.officials.First(o => o.Id == score.OfficialId);
                            lineWritten = true;
                            stream.Write(
                                $"{round.Name};{dance.Name};{official.AdjudicatorChar};{component};{points:#.0}");
                        }
                    }
                }
                if (lineWritten)
                {
                    stream.WriteLine();
                }
            }

            stream.Close();
        }
    }
}