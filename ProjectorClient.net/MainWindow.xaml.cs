﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Visibility = System.Windows.Visibility;

namespace ProjectorClient.net
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private bool webView1Initialized = false;
        private bool webView2Initialized = false;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void WebView1_OnLoaded(object sender, RoutedEventArgs e)
        {
            WebView1.Navigate("https://www.google.de");
            webView1Initialized = true;
            CheckThread();
        }

        private void WebView2_OnLoaded(object sender, RoutedEventArgs e)
        {
            WebView2.Navigate("https://www.heise.de");
            webView2Initialized = true;
            CheckThread();
        }
        private void CheckThread()
        {
            if (webView1Initialized && webView2Initialized)
            {
                Debug.WriteLine("Starte Thread");
                Task.Factory.StartNew(() =>
                {
                    while (true)
                    {
                        Thread.Sleep(2000);

                        this.Dispatcher.Invoke(() =>
                        {
                            WebView1.Opacity = 1;
                            WebView2.Opacity = 0;
                            WebView1.Visibility = Visibility.Hidden;
                            Debug.WriteLine("Changed Opacity");
                        });

                        Thread.Sleep(2000);

                        this.Dispatcher.Invoke(() =>
                        {
                            WebView1.Opacity = 0;
                            WebView2.Opacity = 1;
                            WebView1.Visibility = Visibility.Visible;
                            Debug.WriteLine("CHanged Opacitx again");
                        });
                    }
                });
            }
        }


    }
}
