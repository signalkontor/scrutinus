﻿// // TPS.net TPS8 NancyServer
// // OfficialsViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using DataModel.Models;

namespace NancyServer.ViewModels
{
    public class OfficialsViewModel : IHasCompetition
    {
        public IEnumerable<Official> Judges { get; set; }

        public IEnumerable<OfficialInCompetition> Officials { get; set; }

        public Competition Competition { get; set; }

        public IEnumerable<Competition> RunningCompetitions { get; set; }
    }
}
