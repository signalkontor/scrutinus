﻿// // TPS.net TPS8 NancyServer
// // CompetitionViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using DataModel.Models;

namespace NancyServer.ViewModels
{
    public class CompetitionViewModel : IHasCompetition
    {
        public Competition Competition { get; set; }

        public IEnumerable<Competition> RunningCompetitions { get; set; }
    }
}