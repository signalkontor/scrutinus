﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModel.ModelHelper;
using DataModel.Models;

namespace NancyServer.ViewModels.Presentation
{
    public static class PresentationViewModelFactory
    {
        public static EventViewModel GetEventViewModel(ScrutinusContext context)
        {
            return new EventViewModel()
            {
                Event = context.Events.First()
            };
        }

        public static RoundViewModel GetRoundViewModel(ScrutinusContext context, int roundId)
        {
            var round = context.Rounds.First(r => r.Id == roundId);

            return new RoundViewModel()
            {
                Competition = round.Competition,
                Round = round
            };
        }

        public static HeatViewModel GetHeatViewModel(ScrutinusContext context, int roundId, int danceId, int heatIndex)
        {
            var round = context.Rounds.First(r => r.Id == roundId);

            var heats = HeatsHelper.GetHeats(context, roundId);

            var heatsOfDance = heats.First(h => h.Key.Id == danceId);

            return new HeatViewModel()
            {
                Dance = heatsOfDance.Key.Dance,
                Heat = heatsOfDance.Value[heatIndex],
                HeatNumber = heatIndex + 1,
                Round = round
            };
        }

        public static QualifiedCouplesViewModel GetQualifiedViewModel(ScrutinusContext context, int roundId, int skip)
        {
            var round = context.Rounds.First(r => r.Id == roundId);

            var participants = round.Qualifieds.Select(p => p.Participant);

            return new QualifiedCouplesViewModel()
            {
                Qualified = participants.OrderBy(p => p.Number).Skip(skip).Take(8).ToList(),
                Round = round
            };
        }

        public static PricePresentationViewModel GetPricePresentationViewModel(ScrutinusContext context, int competitionId, int place)
        {
            var final = context.Rounds.FirstOrDefault(r => r.Competition.Id == competitionId && r.RoundType.IsFinal);

            if (final == null)
            {
                return new PricePresentationViewModel();
            }

            var test = new PricePresentationViewModel()
            {
                FinalRound = final,

                Participants = 
                    final.Qualifieds.Where(q => q.PlaceFrom > place)
                        .OrderBy(q => q.Place)
                        .Select(q => q.Participant)
            };

            return test;
        }
    }

    public class EventViewModel
    {
        public Event Event { get; set; }
    }

    public class RoundViewModel
    {
        public Competition Competition { get; set; }

        public Round Round { get; set; }
    }

    public class HeatViewModel
    {
        public Round Round { get; set; }

        public Dance Dance { get; set; }

        public int HeatNumber { get; set; }

        public List<Participant> Heat { get; set; }
    }

    public class QualifiedCouplesViewModel
    {
        public Round Round { get; set; }

        public List<Participant> Qualified { get; set; }
    }

    public class PricePresentationViewModel
    {
        public IEnumerable<Participant> Participants { get; set; }

        public Round FinalRound { get; set; }
    }
}
