﻿// // TPS.net TPS8 NancyServer
// // IHasCompetition.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using DataModel.Models;

namespace NancyServer.ViewModels
{
    public interface IHasCompetition
    {
        Competition Competition { get; }

        IEnumerable<Competition> RunningCompetitions { get; set; }
    }
}
