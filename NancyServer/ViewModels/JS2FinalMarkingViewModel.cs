﻿// // TPS.net TPS8 NancyServer
// // JS2FinalMarkingViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using DataModel.Models;
using General.NewJudgingSystem;

namespace NancyServer.ViewModels
{
    public class JS2MarkingPerDance
    {
        public Dictionary<string, Marking> Marks { get; set; }

        public double TotalPoints { get; set; }

        public Dance Dance { get; set; }

        public int Place { get; set; }

        public Participant Participant { get; set; }
    }

    public class JS2MarkingPerParticipant
    {
        public Participant Participant { get; set; }

        public List<JS2MarkingPerDance> Results { get; set; }

        public double TotalPoints { get; set; }

        public int Place { get; set; }
    }

    public class JS2MarkingViewModel : IHasCompetition
    {
        public JS2MarkingViewModel(ScrutinusContext context, Round round)
        {
            this.MarkingPerParticipants = new List<JS2MarkingPerParticipant>();

            this.Competition = round.Competition;

            this.Round = round;

            this.Judges = round.Competition.GetJudges();

            this.Dances = round.DancesInRound.Select(d => d.Dance).ToList();

            this.Participants = round.Qualifieds.OrderBy(q => q.Participant.Number).Select(p => p.Participant).ToList();

            var pointCalculator = new PointPlaceCalculator2014();
            pointCalculator.CalculatePointsAndPlaces(context, round, round.Competition.CompetitionType.JudgingComponents.ToList());

            foreach (var qualified in round.Qualifieds)
            {
                var marking = new JS2MarkingPerParticipant();
                this.MarkingPerParticipants.Add(marking);
                marking.Participant = qualified.Participant;
                marking.Place = qualified.Participant.PlaceFrom.HasValue ? qualified.Participant.PlaceFrom.Value : 0;
                marking.TotalPoints = qualified.Points;
                marking.Results = new List<JS2MarkingPerDance>();

                foreach (var danceInRound in round.DancesInRound)
                {
                    var js2marking = new JS2MarkingPerDance();
                    marking.Results.Add(js2marking);
                    js2marking.Dance = danceInRound.Dance;
                    js2marking.Participant = qualified.Participant;
                    js2marking.Marks = new Dictionary<string, Marking>();
                    js2marking.TotalPoints = qualified.PointsPerDance[danceInRound.Dance.Id];

                    foreach (var official in this.Judges)
                    {
                        var mark = round.Markings.SingleOrDefault(m => m.Judge.Sign == official.Sign && m.Dance.Id == danceInRound.Dance.Id && m.Participant.Id == qualified.Participant.Id);
                        js2marking.Marks.Add(official.Sign, mark);
                    }
                }
            }
        }

        public List<JS2MarkingPerParticipant> MarkingPerParticipants { get; set; }

        public Round Round { get; set; }

        public List<Official> Judges { get; set; }

        public List<Dance> Dances { get; set; }

        public List<Participant> Participants { get; set; }

        public Competition Competition { get; set; }

        public IEnumerable<Competition> RunningCompetitions { get; set; }
    }
}
