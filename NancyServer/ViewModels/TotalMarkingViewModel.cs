﻿// // TPS.net TPS8 NancyServer
// // TotalMarkingViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using DataModel.Models;

namespace NancyServer.ViewModels
{
    public class TotalMarkingViewModel
    {
        public Dance Dance { get; set; }

        public Official Judge { get; set; }

        public double SumPerDance { get; set; }
        public string MarkingString { get; set; }
    }

    public class TotalMarkingOfParticipant
    {
        public Participant Participant { get; set; }

        public Dictionary<Round, List<TotalMarkingViewModel>> Markings { get; set; }

        public Dictionary<Round, double> Sums { get; set; }
    }

    public class TotalMarkingTableViewModel : IHasCompetition
    {
        public List<TotalMarkingOfParticipant> ParticipantsMarkings { get; set; }

        public IEnumerable<Competition> RunningCompetitions { get; set; }

        public Competition Competition { get; set; }
    }
}
