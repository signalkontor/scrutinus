// // TPS.net TPS8 NancyServer
// // ResultGroupedViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using DataModel.Models;

namespace NancyServer.ViewModels
{
    public class ResultGroupedViewModel : IHasCompetition
    {
        public IEnumerable<IGrouping<Round, Participant>> ResultGrouped { get; set; }
        public Competition Competition { get; set; }

        public IEnumerable<Competition> RunningCompetitions { get; set; }
    }

    public class ResultGroupedByRegionViewModel : IHasCompetition
    {

        public IEnumerable<IGrouping<string, Participant>> ResultGroupedByRegion { get; set; }
        public Competition Competition { get; set; }

        public IEnumerable<Competition> RunningCompetitions { get; set; }
    }
}