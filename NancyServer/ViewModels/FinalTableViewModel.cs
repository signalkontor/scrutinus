﻿// // TPS.net TPS8 NancyServer
// // FinalTableViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using DataModel.Models;
using Helpers.Skating;

namespace NancyServer.ViewModels
{
    public class FinalTableViewModel : IHasCompetition
    {
        public SkatingViewModel SkatingModel { get; set; }

        public Competition Competition { get; set; }

        public IEnumerable<Competition> RunningCompetitions { get; set; }
    }
}
