// // TPS.net TPS8 NancyServer
// // MarksViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System.Collections.Generic;
using DataModel.Models;

namespace NancyServer.ViewModels
{
    public class MarkViewModel
    {
        public Participant Participant { get; set; }

        public Official Judge { get; set; }

        public Dance Dance { get; set; }

        public int MarksCount { get; set; }
    }


    public class MarksViewModel
    {
        public Round Round { get; set; }

        public IEnumerable<Official> Judges { get; set; }

        public IEnumerable<Participant> Participants { get; set; }

        public IEnumerable<Dance> Dances { get; set; }

        public List<MarkViewModel> Marks { get; set; }
    }
}