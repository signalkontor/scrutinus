﻿// // TPS.net TPS8 NancyServer
// // ViewModelFactory.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using DataModel;
using DataModel.Models;
using Helpers.Skating;
using Nancy.ErrorHandling;

namespace NancyServer.ViewModels
{
    public static class ViewModelFactory
    {
        public static List<Competition> GetCompetitionsViewModel()
        {
            var context = new ScrutinusContext();

            return context.Competitions.OrderBy(c => c.StartTime).ToList();
        }

        public static List<Competition> GetCompetitionsViewModel(IEnumerable<string> ids)
        {
            var result = new List<Competition>();

            var context = new ScrutinusContext();

            foreach (var idString in ids)
            {
                var id = int.Parse(idString);
                result.Add(context.Competitions.Single(c => c.Id == id));
            }

            return result;
        }

        public static IEnumerable<Participant> GetRegisteredCouplesViewModel(int competitionId)
        {
            var context = new ScrutinusContext();

            return context.Participants.Where(p => p.Competition.Id == competitionId).OrderBy(p => p.Number);
        }

        public static CompetitionViewModel GetCompetitionViewModel(int competitionId)
        {
            var context = new ScrutinusContext();

            return new CompetitionViewModel(){Competition = context.Competitions.Single(c => c.Id == competitionId), RunningCompetitions = GetRunningCompetitions(context)};
        }

        public static OfficialsViewModel GetOfficialsViewModel(int competitionId)
        {
            var context = new ScrutinusContext();

            var competition = context.Competitions.Single(c => c.Id == competitionId);

            var result = new OfficialsViewModel()
                             {
                                 Competition = competition,
                                 RunningCompetitions = GetRunningCompetitions(context),
                                 Judges = competition.GetJudges().OrderBy(j => j.Sign),
                                 Officials =
                                     competition.Officials.Where(o => o.Role.Id != Roles.Judge).OrderBy(o => o.Official.Sign)
                                     .ToList()
                             };
            return result;
        }

        public static ResultViewModel GetFinalResultViewModel(int competitionId)
        {
            var dataContext = new ScrutinusContext();

            var competition = dataContext.Competitions.Single(c => c.Id == competitionId);

            var final = competition.Rounds.FirstOrDefault(r => r.RoundType.IsFinal);

            if (final == null)
            {
                return null;
            }

            var model = final.Qualifieds.Select(q => q.Participant).OrderBy(p => p.PlaceFrom).ToList();

            return new ResultViewModel() { Competition = competition, Participants = model, RunningCompetitions = GetRunningCompetitions(dataContext)};
        }

        public static ResultGroupedViewModel GetResultGroupedViewModel(int competitionId)
        {
            var dataContext = new ScrutinusContext();
            // Find the final round:
            var compId = competitionId;
            var competition = dataContext.Competitions.Single(c => c.Id == compId);

            var participants =
                competition.Participants.Where(p => p.PlaceFrom.HasValue && p.QualifiedRound != null)
                    .OrderBy(p => p.PlaceFrom)
                    .GroupBy(p => p.QualifiedRound);

            return new ResultGroupedViewModel() { Competition = competition, ResultGrouped = participants, RunningCompetitions = GetRunningCompetitions(dataContext)};
        }

        public static ResultGroupedByRegionViewModel GetResultGroupedByRegionViewModel(int competitionId)
        {
            var dataContext = new ScrutinusContext();
            // Find the final round:
            var compId = competitionId;
            var competition = dataContext.Competitions.Single(c => c.Id == compId);

            var participants =
                competition.Participants.Where(p => p.PlaceFrom.HasValue && p.QualifiedRound != null)
                    .OrderBy(p => p.Couple.Region)
                    .ThenBy(p => p.PlaceFrom)
                    .GroupBy(p => p.Couple.Region);

            return new ResultGroupedByRegionViewModel() { Competition = competition, ResultGroupedByRegion = participants, RunningCompetitions = GetRunningCompetitions(dataContext)};
        }

        public static FinalTableViewModel GetFinalTableViewModel(int competitionId)
        {
            var dataContext = new ScrutinusContext();

            var competition = dataContext.Competitions.Single(c => c.Id == competitionId);

            var round = competition.Rounds.First(r => r.RoundType.IsFinal);

            var skatingViewModel = SkatingHelper.CreateViewModel(dataContext, round);

            return new FinalTableViewModel()
                       {
                           Competition = competition,
                           RunningCompetitions = GetRunningCompetitions(dataContext),
                           SkatingModel = skatingViewModel
                       };
        }

        public static TotalMarkingTableViewModel GetTotalMarkingViewModel(int competitionId)
        {
            var dataContext = new ScrutinusContext();
            var competition = dataContext.Competitions.Single(c => c.Id == competitionId);

            var result = new List<TotalMarkingOfParticipant>();

            foreach (var participant in competition.Participants.OrderBy(p => p.PlaceFrom))
            {
                var model = new TotalMarkingOfParticipant()
                                {
                                    Participant = participant,
                                    Markings = new Dictionary<Round, List<TotalMarkingViewModel>>(),
                                    Sums = new Dictionary<Round, double>(),
                                };

                // get all rounds this participant was dancing:
                var rounds = competition.Rounds.Where(r => r.Qualifieds.Any(q => q.Participant.Id == participant.Id)).OrderBy(r => r.Number);
                foreach (var round in rounds)
                {
                    var marksInDances = new List<TotalMarkingViewModel>();

                    
                    foreach (var judge in round.Competition.GetJudges())
                    {
                        foreach (var danceInRound in round.DancesInRound.OrderBy(d => d.SortOrder))
                        {
                            var marking = new TotalMarkingViewModel()
                            {
                                Dance = danceInRound.Dance,
                                Judge = judge,
                                MarkingString = GetMarkingString(round, danceInRound.Dance, participant, judge),
                                SumPerDance = GetSumPerDance(round, danceInRound.Dance, participant, judge)   
                            };
                            marksInDances.Add(marking);
                        }
                    }
                    
                   
                    model.Markings.Add(round, marksInDances);
                    model.Sums.Add(round, GetSum(round, participant));
                }

                result.Add(model);
            }

            return new TotalMarkingTableViewModel()
                       {
                           ParticipantsMarkings = result,
                           Competition = competition,
                           RunningCompetitions = GetRunningCompetitions(dataContext)
                       };
        }

        private static double GetSumPerDance(Round round, Dance dance, Participant participant, Official judge)
        {
            if (round.MarksInputType == MarkingTypes.Skating || round.MarksInputType == MarkingTypes.MarksSumOnly)
            {
                return -1;
            }

            var marks =
                round.Markings.Where(
                    m => m.Dance.Id == dance.Id && m.Participant.Id == participant.Id && m.Judge.Id == judge.Id).ToList();

            return marks.Any(m => m.Mark == 1) ? 1 : 0;
        }

        public static JS2MarkingViewModel GetJS2FinalViewModel(int competitionId)
        {
            var dataContext = new ScrutinusContext();
            var competition = dataContext.Competitions.Single(c => c.Id == competitionId);
            var final = competition.Rounds.FirstOrDefault(r => r.RoundType.IsFinal);

            if (final == null)
            {
                return null;
            }

            return new JS2MarkingViewModel(dataContext, final);
        }

        private static double GetSum(Round round, Participant participant)
        {
            var qualified = round.Qualifieds.Single(q => q.Participant.Id == participant.Id);

            switch (round.MarksInputType)
            {
                case MarkingTypes.NewJudgingSystemV2:
                    return qualified.Points;

                case MarkingTypes.Marking:
                    return qualified.Sum;

                default:
                    return -1;
            }
        }

        private static string GetMarkingString(Round round, Dance dance, Participant participant, Official judge)
        {
            var result = "";

            Marking marking;

            if (round.MarksInputType == MarkingTypes.MarksSumOnly)
            {

                if (round.MarksInputType == MarkingTypes.MarksSumOnly)
                {
                    marking = round.Markings.LastOrDefault(m => m.Dance == null && m.Participant.Id == participant.Id && m.Judge.Id == judge.Id);
                }
                else
                {
                    var list =
                        round.Markings.Where(
                            m =>
                            m.Dance != null && m.Dance.Id == dance.Id && m.Participant.Id == participant.Id
                            && m.Judge.Id == judge.Id).ToList();

                    if (list.Count() > 1)
                    {
                        Debugger.Break();
                    }

                    marking = round.Markings.LastOrDefault(m => m.Dance != null && m.Dance.Id == dance.Id && m.Participant.Id == participant.Id && m.Judge.Id == judge.Id);
                }

                if (marking == null)
                {
                    result += "-";
                }
                else
                {
                    switch (round.MarksInputType)
                    {
                        case MarkingTypes.Marking:
                            result += marking.Mark == 1 ? "X" : "-";
                            break;
                        case MarkingTypes.MarksSumOnly:
                            result += marking.Mark.ToString();
                            break;
                        case MarkingTypes.Skating:
                            result += marking.Mark;
                            break;
                        case MarkingTypes.NewJudgingSystemV2:
                            result += " " + marking.Mark20.ToString("#0.00") + " ";
                            break;
                    }
                }
            }
            else
            {
                marking = round.Markings.LastOrDefault(m => m.Dance != null && m.Dance.Id == dance.Id && m.Participant.Id == participant.Id && m.Judge.Id == judge.Id);
            }

            if (marking == null)
            {
                return "-";
            }
            else
            {
                switch (round.MarksInputType)
                {
                    case MarkingTypes.Marking:
                        result += marking.Mark == 1 ? "X" : "-";
                        break;
                    case MarkingTypes.MarksSumOnly:
                        break;
                    case MarkingTypes.Skating:
                        result += marking.Mark;
                        break;
                    case MarkingTypes.NewJudgingSystemV2:
                        result += " " + marking.Mark20.ToString("#0.00") + " ";
                        break;
                }
            }
            
            return result;
        }

        public static IEnumerable<Competition> GetRunningCompetitions(ScrutinusContext context)
        {
            var result = new List<Competition>();

            result.AddRange(context.Competitions.Where(c => c.CurrentRound != null && c.State != CompetitionState.Finished));

            result.AddRange(context.Competitions.Where(c => c.CurrentRound == null && c.State == CompetitionState.Startlist || c.State == CompetitionState.CheckIn).OrderBy(c => c.StartTime));

            result.AddRange(context.Competitions.Where(c => c.State == CompetitionState.Finished));

            return result;
        }
    }
}
