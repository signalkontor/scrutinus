﻿// // TPS.net TPS8 NancyServer
// // TestViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

namespace NancyServer.ViewModels
{
    public class TestViewModel
    {
        public string Name { get; set; }

        public string City { get; set; }
    }
}
