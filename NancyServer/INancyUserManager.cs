﻿// // TPS.net TPS8 NancyServer
// // INancyUserManager.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using Nancy.Security;

namespace NancyServer
{
    public interface INancyUserManager
    {
        Guid ValidateUser(string user, string password);

        IUserIdentity GetUserIdentity(Guid guid);
    }
}
