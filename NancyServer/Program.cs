﻿// // TPS.net TPS8 NancyServer
// // Program.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using DataModel.Models;

namespace NancyServer
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Uri[] uris;

            if (NancyServerSettings.Default.ListenUrl == "*")
            {
                uris = HttpServer.GetUriList(NancyServerSettings.Default.Port);
            }
            else
            {
                uris = new Uri[]{ new Uri(NancyServerSettings.Default.ListenUrl), };
            }

            var context = new ScrutinusContext(@"C:\Users\hh-groehn\Documents\TPS.net\GMNord.sdf", null);

            var userManager = new SimpleNancyUserManager();
            userManager.Users.Add(new UserIdentity(){ Claims = new List<string> {"chairman", "marking"}, UserName = "chairman", Password = "123456"});

            var nancyServer = new HttpServer(uris, NancyServerSettings.Default.DataSource, userManager);
            nancyServer.StartHost();
            Console.WriteLine("Server started");
            Console.ReadLine();
            nancyServer.StopHost();
        }
    }
}
