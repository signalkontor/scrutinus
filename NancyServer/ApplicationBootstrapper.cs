﻿// // TPS.net TPS8 NancyServer
// // ApplicationBootstrapper.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using Nancy;
using Nancy.Authentication.Forms;
using Nancy.Bootstrapper;
using Nancy.Conventions;
using Nancy.TinyIoc;
using NancyServer.Authentification;

namespace NancyServer
{
    public class ApplicationBootstrapper : DefaultNancyBootstrapper
    {
        public ApplicationBootstrapper(INancyUserManager userManager)
        {
            TinyIoCContainer.Current.Register<INancyUserManager>(userManager);
        }

        protected override void ConfigureConventions(NancyConventions nancyConventions)
        {
            base.ConfigureConventions(nancyConventions);
            
            nancyConventions.StaticContentsConventions.Add(StaticContentConventionBuilder.AddDirectory("Content", "Content"));
            nancyConventions.StaticContentsConventions.Add(StaticContentConventionBuilder.AddDirectory("Scripts", "Scripts"));
        }

        protected override void ApplicationStartup(TinyIoCContainer container, IPipelines pipelines)
        {
            container.Register<IUserMapper>(new ScrutinusUserMapper());

            base.ApplicationStartup(container, pipelines);

            // Enable Forms
            var formsAuthConfiguration =
                new FormsAuthenticationConfiguration()
                {
                    RedirectUrl = "~/Login/login",
                    UserMapper = container.Resolve<IUserMapper>(),
                };

            FormsAuthentication.Enable(pipelines, formsAuthConfiguration);

            // This is just for login purpose:
            pipelines.BeforeRequest += (ctx) =>
                {
                    Console.WriteLine(
                        "[{0}] {1}: {2}",
                        DateTime.Now.ToShortTimeString(),
                        ctx.Request.UserHostAddress,
                        ctx.Request.Path);
                    return null; 
                };
        }
    }
}
