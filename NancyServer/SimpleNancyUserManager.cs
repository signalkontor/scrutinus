﻿// // TPS.net TPS8 NancyServer
// // SimpleNancyUserManager.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Linq;
using Nancy.Security;

namespace NancyServer
{
    public class LoginParams
    {
        public string UserName { get; set; }

        public string Password { get; set; }
    }

    public class UserIdentity : IUserIdentity
    {
        private List<string> claims;

        public string Password { get; set; }

        public IEnumerable<string> Claims
        {
            get { return this.claims; }
            set
            {
                this.claims = value.ToList();
            }
        }

        public string UserName { get; set; }
    }

    public class SimpleNancyUserManager : INancyUserManager
    {
        private readonly Dictionary<Guid, IUserIdentity> users;

        public SimpleNancyUserManager()
        {
            this.Users = new List<UserIdentity>();
            this.users = new Dictionary<Guid, IUserIdentity>();
        }

        public List<UserIdentity> Users { get; set; }


        public Guid ValidateUser(string user, string password)
        {
            var dbuser = this.Users.SingleOrDefault(u => u.UserName == user);

            if (dbuser == null)
            {
                return Guid.Empty;
            }

            if (dbuser.Password == password)
            {
                var guid = Guid.NewGuid();
                this.users.Add(guid, dbuser);
                return guid;
            }

            return Guid.Empty;
        }

        public IUserIdentity GetUserIdentity(Guid guid)
        {
            if (this.users.ContainsKey(guid))
            {
                return this.users[guid];
            }

            return null;
        }
    }
}
