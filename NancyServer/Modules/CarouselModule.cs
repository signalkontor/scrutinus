﻿// // TPS.net TPS8 NancyServer
// // CarouselModule.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Globalization;
using System.Linq;
using System.Threading;
using DataModel;
using DataModel.Models;
using Nancy;

namespace NancyServer.Modules
{
    public class CarouselModule : NancyModule
    {
        public CarouselModule()
        {
            this.Get["/Carousel"] = request =>
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo(HttpServer.ApplicationLocale ?? "en");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(HttpServer.ApplicationLocale ?? "en");

                var context = new ScrutinusContext();
                var rounds = context.Competitions.ToList()
                    .Where(c => !c.IsCompetitionNotStarted && c.State != CompetitionState.Finished)
                    .Select(c => c.CurrentRound).ToList();

                if (rounds.Count > 0)
                {
                    return this.Response.AsRedirect($"/QualifiedCarousel/{rounds.First().Id}/1");
                }

                return this.View["Carousel"];
            };

            this.Get["/QualifiedCarousel/{round:int}/{page:int}"] = request =>
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo(HttpServer.ApplicationLocale ?? "en");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(HttpServer.ApplicationLocale ?? "en");

                int roundId = request.round;
                int page = request.page;

                var viewModel = new QualifiedCarouselViewModel(roundId, page);

                if (viewModel.NextRoundId == 0)
                {
                    // no more rounds to show
                    return this.Response.AsRedirect("/Carousel");
                }



                viewModel.NextPageName = viewModel.NextRoundId != roundId
                                    ? (HttpServer.ShowCarouselDroppedOut ? "DroppedOutCarousel" : "QualifiedCarousel") 
                                    : "QualifiedCarousel";

                viewModel.NextRoundId = roundId; // override the next id because we switch to dropped out of this comp.

                return this.View["QualifiedCarousel", viewModel];
            };

            this.Get["/DroppedOutCarousel/{round:int}/{page:int}"] = request =>
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo(HttpServer.ApplicationLocale ?? "en");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(HttpServer.ApplicationLocale ?? "en");

                int roundId = request.round;
                int page = request.page;

                var viewModel = new DroppedOutCarouselViewModel(roundId, page);
                if (viewModel.NextRoundId == 0)
                {
                    // no more rounds to show
                    return this.Response.AsRedirect("/Carousel");
                }
                if (viewModel.DroppedOut == null)
                {
                   return this.Response.AsRedirect($"/QualifiedCarousel/{viewModel.NextRoundId}/1"); 
                }

                viewModel.NextPageName = viewModel.NextRoundId != roundId ? "QualifiedCarousel" : HttpServer.ShowCarouselDroppedOut ? "DroppedOutCarousel" : "QualifiedCarousel";

                return this.View["DroppedOutCarousel", viewModel];
            };
        }
    }
}
