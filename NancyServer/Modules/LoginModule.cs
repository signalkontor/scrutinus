﻿// // TPS.net TPS8 NancyServer
// // LoginModule.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using Nancy;
using Nancy.Authentication.Forms;
using Nancy.ModelBinding;
using Nancy.TinyIoc;

namespace NancyServer.ViewModels
{
    public class DrawParameters
    {
        public int RoundId { get; set; }

        public int Heat { get; set; }
    }

    public class LoginModule : NancyModule
    {
        public LoginModule()
        {
            this.Get["/Login/login"] = parameters => this.View["login"];

            this.Get["/Login/logout"] = parameters =>
            {
                return this.LogoutAndRedirect("/");
            };

            this.Post["/Login/login"] = parameters =>
                {
                    var loginParams = this.Bind<LoginParams>();

                    var userService = TinyIoCContainer.Current.Resolve<INancyUserManager>();

                    var guid = userService.ValidateUser(loginParams.UserName, loginParams.Password);
                    
                    if (guid != Guid.Empty)
                    {
                        return this.LoginAndRedirect(guid);
                    }

                    return this.Response.AsRedirect("/Login/login");
                };
        }
    }
}
