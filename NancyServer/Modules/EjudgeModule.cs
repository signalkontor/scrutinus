﻿// // TPS.net TPS8 NancyServer
// // EjudgeModule.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using DataModel.Models;
using GalaSoft.MvvmLight.Messaging;
using mobileControl.Helper;
using MobileControlLib.EjudgeMessages;
using Nancy;

namespace NancyServer.Modules
{
    public class RoundList
    {
        public RoundList(IEnumerable<EjudgeData> data)
        {
            this.roundNames = data.Select(d => d.Round.Competition.Title + " / " + d.Round.Name).ToArray();
            this.roundIds = data.Select(d => d.Round.Id).ToArray();
        }

        public string[] roundNames { get; set; }

        public int[] roundIds { get; set; }
    }

    public class EjudgeModule : NancyModule
    {
        public EjudgeModule()
        {
            this.Get["/GetData/{Pin}"] = request =>
            {
                string pin = request.Pin.ToString();
                var deviceId = this.Request.Query["device"];

                // Try to get the Judge Data:

                using (var context = new ScrutinusContext())
                {
                    var eJudgeData = context.EjudgeDatas.Where(e => e.Judge.PinCode == pin && e.DeviceId == "" && e.Round.EjudgeEnabled);
                    if (!eJudgeData.Any())
                    {
                        return HttpStatusCode.NotFound;
                    }

                    if (eJudgeData.Count() > 1)
                    {
                        return this.Response.AsJson(new RoundList(eJudgeData));
                    }

                    // Ok, from here we are sure to have only one round:
                    var round = eJudgeData.First().Round;
                    var judge = context.Officials.FirstOrDefault(j => j.PinCode == pin);

                    if (round == null || judge == null)
                    {
                        return HttpStatusCode.NotFound;
                    }

                    var deviceData = round.EjudgeData.First(r => r.Judge.Id == judge.Id);
                    if (deviceData == null)
                    {
                        deviceData = new EjudgeData() { DeviceId = deviceId, Judge = judge, Round = round};
                        context.EjudgeDatas.Add(deviceData);
                    }

                    deviceData.DeviceId = deviceId;

                    context.SaveChanges();
                    // Get Data Data from PIN:
                    var data = ScrutinusCommunicationHandler.GetDataForJudge(round, deviceData).Replace('|', '-');
                    var marking = ScrutinusCommunicationHandler.GetMarksForJudge(round, judge);

                    Messenger.Default.Send(new JudgeLogonMessage() { Sender = deviceId, DeviceId = deviceId, Judge = judge, Round = round });

                    return round.EjudgeName + "|" + data + "|" + marking;
                }
            };

            this.Get["/SetDevice/{PIN}"] = request =>
                {
                    string pin = request.Pin.ToString();
                    var deviceId = this.Request.Query["device"];
                    var roundId = int.Parse((string)this.Request.Query["roundid"]);

                    using (var context = new ScrutinusContext())
                    {
                        var round = context.Rounds.Single(r => r.Id == roundId);
                        var judge = round.Competition.GetJudges().FirstOrDefault(j => j.PinCode == pin);

                        if (judge == null)
                        {
                            return HttpStatusCode.NotFound;
                        }

                        var deviceData = round.EjudgeData.First(r => r.Judge.Id == judge.Id);
                        if (deviceData == null)
                        {
                            deviceData = new EjudgeData() { DeviceId = deviceId, Judge = judge, Round = round };
                            context.EjudgeDatas.Add(deviceData);
                        }

                        deviceData.DeviceId = deviceId;

                            context.SaveChanges();
                        // Get Data Data from PIN:
                        var data = ScrutinusCommunicationHandler.GetDataForJudge(round, deviceData).Replace('|', '-');
                        var marking = ScrutinusCommunicationHandler.GetMarksForJudge(round, judge);

                        Messenger.Default.Send(new JudgeLogonMessage() { Sender = deviceId, DeviceId = deviceId, Judge = judge, Round = round });

                        return round.EjudgeName + "|" + data + "|" + marking;
                    }
                };
        }
    }
}
