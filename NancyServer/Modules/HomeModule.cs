﻿// // TPS.net TPS8 NancyServer
// // HomeModule.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using DataModel;
using DataModel.Models;
using Nancy;

namespace NancyServer.Modules
{
    public class HomeModule : NancyModule
    {
        public HomeModule()
        {
            this.Get["/"] = parameters =>
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo(HttpServer.ApplicationLocale ?? "en");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(HttpServer.ApplicationLocale ?? "en");

                var dataContext = new ScrutinusContext();

                if (Competitions == null)
                {
                    Competitions = dataContext.Competitions.ToList();
                }

                var competitions = dataContext.Competitions.Where(c => c.State != CompetitionState.Finished && c.CurrentRound != null).ToList();

                return this.View["Index", competitions];
            };

            this.Get["/Home/AllCompetitions"] = parameters =>
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo(HttpServer.ApplicationLocale ?? "en");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(HttpServer.ApplicationLocale ?? "en");

                var dataContext = new ScrutinusContext();

                if (Competitions == null)
                {
                    Competitions = dataContext.Competitions.ToList();
                }

                var competitions = dataContext.Competitions.ToList();

                return this.View["AllCompetitions", competitions];
            };

            this.Get["/Home/LeftNavigation"] = parameters =>
            {
                return this.View["LeftNavigation"];
            };

        }

        public static IEnumerable<Competition> Competitions { get; set; }
    }
}
