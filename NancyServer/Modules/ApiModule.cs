﻿// // TPS.net TPS8 NancyServer
// // ApiModule.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Linq;
using System.Text;
using DataModel.JsonModels;
using DataModel.Models;
using General;
using Nancy;
using Nancy.ModelBinding;

namespace NancyServer.Modules
{
    public class ApiModule : NancyModule
    {
        public ApiModule() : base("/api")
        {
            this.Get["/AgeGroups"] = _ =>
            {
                var context = new ScrutinusContext();

                return this.GetJsonReponse(context.AgeGroups.ToList().Select(c => new JsonAgeGroup(c)).ToArray());
            };

            this.Get["/Sections"] = _ =>
            {
                var context = new ScrutinusContext();

                return this.GetJsonReponse(context.Sections.ToList().Select(s => new JsonSection(s)).ToArray());
            };

            this.Get["/Startbuch/{id:int}"] = request =>
                {
                    var context = new ScrutinusContext();
                    int coupleId = request.id;

                    return
                        context.Startbuecher.Where(s => s.Couple.Id == coupleId)
                            .ToList()
                            .Select(s => new JsonStartbuch(s))
                            .ToArray();
                };

            this.Get["/Classes"] = _ =>
            {
                var context = new ScrutinusContext();

                return this.GetJsonReponse(context.Classes.ToList().Select(c => new JsonClass(c)).ToArray());
            };

            this.Get["/Couples"] = _ =>
                {
                    var context = new ScrutinusContext();

                    return this.GetJsonReponse(context.Couples.ToList().Select(c => new JsonCouple(c)).ToArray());
                };

            this.Get["/Competitions"] = request =>
                {
                    var context = new ScrutinusContext();

                    return this.GetJsonReponse(context.Competitions.ToList().Select(c => new JsonCompetition(c)) .ToArray());
                };

            this.Get["/Participants/{id:int}"] = request =>
                {
                    var context = new ScrutinusContext();

                    int competitionId = request.id;

                    return
                        this.GetJsonReponse(
                            context.Participants.Where(p => p.Competition.Id == competitionId)
                                .ToList()
                                .Select(p => new JsonParticipant(p)));
                };

            this.Post["/ParticipantState/"] = request =>
                {
                    var model = this.Bind<ParticipantStateUpdate>();

                    var context = new ScrutinusContext();

                    var participant = context.Participants.SingleOrDefault(p => p.Id == model.ParticipantId);

                    if (participant != null)
                    {
                        participant.State = model.State;
                        context.SaveChanges();

                        return new Response() { StatusCode = HttpStatusCode.OK };
                    }
                    else
                    {
                        return new Response() { StatusCode = HttpStatusCode.NotFound };
                    }
                };
        }

        public Response GetJsonReponse(object obj)
        {
            var json = JsonHelper.SerializeObject(obj);

            var jsonBytes = Encoding.UTF8.GetBytes(json);
            return new Response
            {
                ContentType = "application/json",
                Contents = s => s.Write(jsonBytes, 0, jsonBytes.Length)
            };
        }
    }
}
