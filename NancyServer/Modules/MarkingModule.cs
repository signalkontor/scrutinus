﻿// // TPS.net TPS8 NancyServer
// // MarkingModule.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using DataModel;
using DataModel.Models;
using Nancy;

namespace NancyServer.ViewModels
{


    public class MarkingModule : NancyModule
    {
        public MarkingModule()
        {
            this.Get["/Marking/Marks/{id:int}"] = roundId =>
                {
                    int id = roundId.id;
                    var context = new ScrutinusContext();

                    var round = context.Rounds.Single(r => r.Id == id);

                    if (round.MarksInputType == MarkingTypes.Skating)
                    {
                        return this.Response.AsRedirect("/Marking/Final/" + id);
                    }

                    var marksViewModel = new MarksViewModel()
                                             {
                                                 Round = round,
                                                 Judges = round.Competition.GetJudges(),
                                                 Participants = round.Qualifieds.OrderBy(q => q.Participant.Number).Select(q => q.Participant).ToList(),
                                                 Marks = new List<MarkViewModel>()
                                             };

                    var marks = round.Markings;

                    foreach (var participant in marksViewModel.Participants)
                    {
                        foreach (var judge in marksViewModel.Judges)
                        {
                            var markViewModel = new MarkViewModel()
                                                    {
                                                        Judge = judge,
                                                        Participant = participant,
                                                        MarksCount = marks.Count(m => m.Judge.Id == judge.Id && m.Participant.Id == participant.Id)
                                                    };
                            marksViewModel.Marks.Add(markViewModel);
                        }
                    }

                    return this.View["Marks", marksViewModel];
                };

            this.Get["/Marking/Final/{id:int}"] = roundId =>
            {
                int id = roundId.id;
                var context = new ScrutinusContext();

                var round = context.Rounds.Single(r => r.Id == id);

                if (round.MarksInputType == MarkingTypes.Marking)
                {
                    return this.Response.AsRedirect("/Marking/Marks/" + id);
                }

                var marksViewModel = new MarksViewModel()
                {
                    Round = round,
                    Judges = round.Competition.GetJudges(),
                    Participants = round.Qualifieds.OrderBy(q => q.Participant.Number).Select(q => q.Participant).ToList(),
                    Dances = round.DancesInRound.Select(d => d.Dance),
                    Marks = new List<MarkViewModel>()
                };

                var marks = round.Markings;

                foreach (var participant in marksViewModel.Participants)
                {
                    foreach (var judge in marksViewModel.Judges)
                    {
                        foreach (var dance in marksViewModel.Dances)
                        {
                            var markViewModel = new MarkViewModel()
                            {
                                Judge = judge,
                                Participant = participant,
                                Dance = dance,
                                MarksCount = marks.First(m => m.Judge.Id == judge.Id && m.Participant.Id == participant.Id && m.Dance.Id == dance.Id).Mark,
                            };
                            marksViewModel.Marks.Add(markViewModel);
                        }
                    }
                }

                return this.View["Final", marksViewModel];
            };
        }
    }
}
