﻿// // TPS.net TPS8 NancyServer
// // StaticContentModule.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.IO;
using Nancy;
using Nancy.Responses;

namespace NancyServer.ViewModels
{
    public class StaticContentModule : NancyModule
    {
        public StaticContentModule()
        {
            this.Get["/abc/{file}"] = file =>
                {

                    string path = string.Format("{1}\\content\\{0}", file.file, Directory.GetCurrentDirectory());
                    if (!File.Exists(path))
                    {
                        throw new Exception("File");
                    }

                    var data = File.ReadAllText(path);

                    return new GenericFileResponse(path, "text/css");
                };
        }
    }
}
