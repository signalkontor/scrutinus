﻿// // TPS.net TPS8 NancyServer
// // CompetitionModule.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Globalization;
using System.Linq;
using System.Threading;
using DataModel.Models;
using Nancy;

namespace NancyServer.ViewModels
{

    public class CompetitionModule : NancyModule
    {
        public CompetitionModule()
        {
            this.Get["/Competition/{id:int}"] = request =>
                {
                    Thread.CurrentThread.CurrentCulture = new CultureInfo(HttpServer.ApplicationLocale ?? "en");
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(HttpServer.ApplicationLocale ?? "en");

                    var dataContext = new ScrutinusContext();

                    int id = request.id;

                    var competition = dataContext.Competitions.Single(c => c.Id == id);

                    return this.View["Competition", new CompetitionViewModel(){Competition = competition, RunningCompetitions = ViewModelFactory.GetRunningCompetitions(dataContext)}];
                };

            this.Get["/Competitions/"] = _ =>
                {
                    Thread.CurrentThread.CurrentCulture = new CultureInfo(HttpServer.ApplicationLocale ?? "en");
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(HttpServer.ApplicationLocale ?? "en");

                    var dataContext = new ScrutinusContext();
                    var competitions = dataContext.Competitions.ToList();

                    return this.View["Index", competitions];
                };

            this.Get["/Competitions/Export"] = _ =>
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo(HttpServer.ApplicationLocale ?? "en");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(HttpServer.ApplicationLocale ?? "en");

                var dataContext = new ScrutinusContext();
                var competitions = dataContext.Competitions.ToList();

                return this.View["IndexExport", competitions];
            };

            this.Get["Competition/Participants/{id:int}"] = request =>
                {
                    Thread.CurrentThread.CurrentCulture = new CultureInfo(HttpServer.ApplicationLocale ?? "en");
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(HttpServer.ApplicationLocale ?? "en");

                    var dataContext = new ScrutinusContext();

                    int id = request.id;

                    var competition = dataContext.Competitions.Single(c => c.Id == id);

                    return this.View["Participants", new CompetitionViewModel() { Competition = competition }];
                };
        }
    }
}
