﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModel.Models;
using Nancy;
using NancyServer.ViewModels.Presentation;

namespace NancyServer.Modules
{
    public class ProjectorModule : NancyModule
    {
        public ProjectorModule() : base("/projector")
        {
            Get["/event"] = parameters => GetEventView(parameters);

            Get["/round/{roundId}"] = parameters => GetRoundView(parameters);

            Get["/drawing/{roundId}"] = parameters => GetHeatView(parameters);

            Get["/qualified/{roundId}"] = parameters => GetQualifiedCouplesView(parameters);

            Get["/pricepresentation/{competitionId}"] = parameters => GetPricePresentationView(parameters);
        }

        private dynamic GetRoundView(dynamic parameters)
        {
            int roundId = parameters.roundId;

            var context = new ScrutinusContext();

            return View["Round", PresentationViewModelFactory.GetRoundViewModel(context, roundId)];
        }

        public dynamic GetEventView(dynamic parameters)
        {
            var context = new ScrutinusContext();

            return this.View["Event", PresentationViewModelFactory.GetEventViewModel(context)];
        }

        public dynamic GetHeatView(dynamic parameters)
        {
            ScrutinusContext context = new ScrutinusContext();
            int roundId = parameters.roundId;
            int danceId = this.Request.Query["danceId"];
            int heatIndex = this.Request.Query["heatIndex"];;

            return View["Heats", PresentationViewModelFactory.GetHeatViewModel(context, roundId, danceId, heatIndex)];
        }

        public dynamic GetQualifiedCouplesView(dynamic parameters)
        {
            ScrutinusContext context = new ScrutinusContext();
            int roundId = parameters.roundId;
            int skip = this.Request.Query["skip"];

            return View["Qualified", PresentationViewModelFactory.GetQualifiedViewModel(context, roundId, skip)];
        }

        public dynamic GetPricePresentationView(dynamic parameters)
        {
            ScrutinusContext context = new ScrutinusContext();
            int competitionId = parameters.competitionId;
            int place = this.Request.Query["place"];

            return View["PricePresentation",
                PresentationViewModelFactory.GetPricePresentationViewModel(context, competitionId, place)];
        }
    }
}
