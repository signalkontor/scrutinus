﻿// // TPS.net TPS8 NancyServer
// // ExportModule.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Globalization;
using System.Linq;
using System.Threading;
using DataModel;
using DataModel.Models;
using Nancy;
using NancyServer.Modules;

namespace NancyServer.ViewModels
{
    public class ExportModule : NancyModule
    {
        public ExportModule()
        {
            this.Get["/Export"] = request =>
                {
                    Thread.CurrentThread.CurrentCulture = new CultureInfo(HttpServer.ApplicationLocale ?? "en");
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(HttpServer.ApplicationLocale ?? "en");

                    var idString = this.Request.Query.ids.ToString();

                    var viewModel = ViewModelFactory.GetCompetitionsViewModel(idString.Split(','));

                    HomeModule.Competitions = viewModel;

                    return this.View["Index", viewModel];
                };

            this.Get["/Export/Overview/{competitionId:int}"] = request =>
                {
                    Thread.CurrentThread.CurrentCulture = new CultureInfo(HttpServer.ApplicationLocale ?? "de");
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(HttpServer.ApplicationLocale ?? "en");

                    var competition = ViewModelFactory.GetCompetitionViewModel(request.competitionId);

                    return this.View["Overview", competition];
                };

            this.Get["/Export/RegisteredCouples/{competitionId:int}"] = request =>
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo(HttpServer.ApplicationLocale ?? "de");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(HttpServer.ApplicationLocale ?? "en");

                var viewModel = ViewModelFactory.GetRegisteredCouplesViewModel(request.competitionId);

                return this.View["RegisteredCouples", viewModel];
                //return Negotiate.WithMediaRangeModel("text/html; charset=utf-16", () => { return viewModel; })
                //       .WithView("RegisteredCouples");

            };

            this.Get["/Export/Result/{competitionId:int}"] = request =>
                {
                    Thread.CurrentThread.CurrentCulture = new CultureInfo(HttpServer.ApplicationLocale ?? "de");
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(HttpServer.ApplicationLocale ?? "en");

                    var viewModel = ViewModelFactory.GetResultGroupedViewModel(request.competitionId);

                    return this.View["Result", viewModel];
                };

            this.Get["/Export/MarkingTable/{competitionId:int}"] = request =>
                {
                    Thread.CurrentThread.CurrentCulture = new CultureInfo(HttpServer.ApplicationLocale ?? "de");
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(HttpServer.ApplicationLocale ?? "en");

                    var viewModel = ViewModelFactory.GetTotalMarkingViewModel(request.competitionId);

                    return this.View["MarkingTable", viewModel];
                };

            this.Get["/Export/Officials/{competitionId:int}"] = request =>
                {
                    Thread.CurrentThread.CurrentCulture = new CultureInfo(HttpServer.ApplicationLocale ?? "de");
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(HttpServer.ApplicationLocale ?? "en");

                    var viewModel = ViewModelFactory.GetOfficialsViewModel(request.competitionId);

                    return this.View["Officials", viewModel];
                };

            this.Get["/Export/FinalTable/{competitionId:int}"] = request =>
                {
                    Thread.CurrentThread.CurrentCulture = new CultureInfo(HttpServer.ApplicationLocale ?? "de");
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(HttpServer.ApplicationLocale ?? "en");

                    int competitionId = request.competitionId;
                    var dataContext = new ScrutinusContext();
                    var competition = dataContext.Competitions.Single(c => c.Id == competitionId);
                    var final = competition.Rounds.FirstOrDefault(r => r.RoundType.IsFinal);

                    if (final == null)
                    {
                        return this.View["NoData"];
                    }

                    if (final.MarksInputType == MarkingTypes.Skating)
                    {
                        var viewModel = ViewModelFactory.GetFinalTableViewModel(competitionId);

                        return this.View["FinalTable", viewModel];
                    }

                    if (final.MarksInputType == MarkingTypes.NewJudgingSystemV2)
                    {
                        var viewModel = ViewModelFactory.GetJS2FinalViewModel(request.competitionId);
                        return this.View["JS2Final", viewModel];
                    }

                    return this.View["NotSupported"];
                };
        }
    }
}
