// // TPS.net TPS8 NancyServer
// // QualifiedCarouselViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using DataModel;
using DataModel.Models;

namespace NancyServer.Modules
{
    public class QualifiedCarouselViewModel
    {
        public QualifiedCarouselViewModel(int roundId, int page)
        {
            var context = new ScrutinusContext();

            var pageSize = HttpServer.MaxCouplesPerPageInCarousel;

            var round = context.Rounds.First(r => r.Id == roundId);
            this.Qualifieds = context.Qualifieds.Where(q => q.Round.Id == roundId).Select(q => q.Participant).OrderBy(p => p.Number).Skip((page - 1) * pageSize).Take(pageSize).ToList();
            this.RoundName = round.Name;
            this.CompetitionName = round.Competition.Title;
            this.EventTitle = round.Competition.Event.Title;
            this.DanceShortNames = round.DancesInRound.OrderBy(d => d.SortOrder).Select(d => d.Dance.ShortName);
            this.HeatsOfCouple = new Dictionary<int, IEnumerable<int>>();

            foreach (var qualified in round.Qualifieds)
            {
                var drawing =
                    round.Drawings.Where(d => d.Participant.Number == qualified.Participant.Number)
                        .OrderBy(d => d.DanceRound.SortOrder)
                        .Select(d => d.Heat + 1);

                this.HeatsOfCouple.Add(qualified.Participant.Number, drawing);
            }

            var maxPage = round.Qualifieds.Count/8;
            if (round.Qualifieds.Count%8 > 0)
            {
                maxPage++;
            }

            if (maxPage > page)
            {
                this.NextPage = page + 1;
                this.NextRoundId = roundId;
            }
            else
            {
                this.NextPage = 1;
                var rounds = context.Competitions.ToList().Where(c => !c.IsCompetitionNotStarted && c.State != CompetitionState.Finished)
                    .Select(c => c.CurrentRound).ToList();

                if (!rounds.Any())
                {
                    this.NextRoundId = 0;
                    return;
                }

                var index = rounds.FindIndex(r => r.Id == roundId);
                if (index == rounds.Count - 1)
                {
                    this.NextRoundId = rounds[0].Id;
                }
                else
                {
                    this.NextRoundId = rounds[index + 1].Id;
                }
            }

            this.PageString = $"{page} / {maxPage}";
        }

        public string RoundName { get; set; }

        public string CompetitionName { get; set; }

        public string EventTitle { get; set; }

        public IEnumerable<string> DanceShortNames { get; set; }

        public string PageString { get; set; }

        public IEnumerable<Participant> Qualifieds { get; set; }

        public Dictionary<int, IEnumerable<int>> HeatsOfCouple { get; set; }

        public int NextRoundId { get; set; }

        public int NextPage { get; set; }

        public string NextPageName { get; set; }
    }

    public class DroppedOutCarouselViewModel
    {
        public DroppedOutCarouselViewModel(int roundId, int page)
        {
            var context = new ScrutinusContext();

            var currentRound = context.Rounds.First(r => r.Id == roundId);

            // if this is a redance, do not show any dropped outs
            // as non of them is dropped out...
            if (currentRound.IsRedanceRound)
            {
                this.DroppedOut = null;
                this.SetNextCompetitionToShow(roundId, context);
                return;
            }

            var round =
                context.Rounds.FirstOrDefault(
                    r => r.Competition.Id == currentRound.Competition.Id && r.Number == currentRound.Number - 1);

            if (round == null)
            {
                this.SetNextCompetitionToShow(roundId, context);
                return;
            }

            this.RoundName = round.Name;
            this.CompetitionName = round.Competition.Title;
            this.EventTitle = round.Competition.Event.Title;
            var pageSize = HttpServer.MaxCouplesPerPageInCarousel;

            var allDroppedOuts = round.Qualifieds.Where(q => q.QualifiedNextRound == false).OrderBy(q => q.PlaceFrom).ToList();

            if (currentRound.RoundType.Id == RoundTypes.Redance)
            {
                // redance -> do not show dropped out
                this.DroppedOut = new List<Qualified>();
                allDroppedOuts.Clear();
            }
            else
            {
                this.DroppedOut = allDroppedOuts.Skip((page - 1) * pageSize).Take(pageSize).ToList(); ;
            }

            var maxPage = allDroppedOuts.Count() / pageSize;
            if (allDroppedOuts.Count() % pageSize > 0)
            {
                maxPage++;
            }

            if (maxPage > page)
            {
                this.NextPage = page + 1;
                this.NextRoundId = roundId;
            }
            else
            {
                this.SetNextCompetitionToShow(roundId, context);
            }

            this.PageString = $"{page} / {maxPage}";
        }


        public string RoundName { get; set; }

        public string CompetitionName { get; set; }

        public string EventTitle { get; set; }

        public string PageString { get; set; }

        public IEnumerable<Qualified> DroppedOut { get; set; }

        public int NextRoundId { get; set; }

        public int NextPage { get; set; }

        public string NextPageName { get; set; }

        private void SetNextCompetitionToShow(int roundId, ScrutinusContext context)
        {
            this.NextPage = 1;

            var rounds =
                context.Competitions.ToList().Where(c => !c.IsCompetitionNotStarted && c.State != CompetitionState.Finished)
                    .Select(c => c.CurrentRound).ToList();

            if (!rounds.Any())
            {
                this.NextRoundId = 0;
                return;
            }

            var index = rounds.FindIndex(r => r.Id == roundId);
            if (index == rounds.Count - 1)
            {
                this.NextRoundId = rounds[0].Id;
            }
            else
            {
                this.NextRoundId = rounds[index + 1].Id;
            }
        }
    }
}