﻿// // TPS.net TPS8 NancyServer
// // ScoreBoardModule.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using DataModel.Models;
using Nancy;
using Nancy.ModelBinding;

namespace NancyServer.Modules
{
    public class DrawParameters
    {
        public int RoundId { get; set; }

        public int DanceId {get; set; }

        public int Heat { get; set; }
    }

    public class PricePresentationViewModel
    {
        public IEnumerable<Participant> Participants { get; set; }

        public Round FinalRound { get; set; }
    }

    public class IndexViewModel
    {
        public Competition Competition { get; set; }
    }

    public class PricePresentationModel
    {
        public int CompetitionId { get; set; }

        public int UpToPlace { get; set; }
    }

    public class DrawViewModel
    {
        public int NextRoundId { get; set; }

        public int NextDanceId { get; set; }

        public int NextHeat { get; set; }

        public string Competition { get; set; }

        public string Dance { get; set; }

        public string Round { get; set; }

        public int Heat { get; set; }

        public List<Drawing> Drawings { get; set; }
    }

    public class ScoreBoardModule : NancyModule
    {
        public ScoreBoardModule()
        {
            this.Get["/ScroreBoard"] = _ => this.View["Index"];

            this.Get["/ScoreBoard/PricePresentation/"] = request =>
                {
                    var parameters = this.Bind<PricePresentationModel>();
                    var context = new ScrutinusContext();

                    var final = context.Rounds.FirstOrDefault(r => r.Competition.Id == parameters.CompetitionId && r.RoundType.IsFinal);

                    var viewModel = new PricePresentationViewModel();

                    if (final != null)
                    {
                        viewModel.Participants =
                            final.Qualifieds.Where(q => q.PlaceFrom < parameters.UpToPlace)
                                .OrderBy(q => q.Place)
                                .Select(q => q.Participant);
                        
                    }
                    else
                    {
                        viewModel.Participants = new List<Participant>(); 
                    }

                    viewModel.FinalRound = final;

                    return this.View["PricePresentation", viewModel];
                };

            this.Get["/ScoreBoard/Drawing"] = request =>
                {
                    var parameters = this.Bind<DrawParameters>();

                    var context = new ScrutinusContext();

                    var round = context.Rounds.Single(r => r.Id == parameters.RoundId);

                    if (parameters.DanceId == 0)
                    {
                        parameters.DanceId =
                            context.DancesInRounds.Where(d => d.Round.Id == parameters.RoundId)
                                .OrderBy(r => r.SortOrder)
                                .First().Id;
                    }

                    var dance = context.DancesInRounds.Single(d => d.Id == parameters.DanceId);

                    var viewModel = new DrawViewModel()
                                        {
                                            NextRoundId = parameters.RoundId,
                                            NextDanceId = parameters.DanceId,
                                            NextHeat = parameters.Heat + 1,
                                            Round = round.Name,
                                            Competition = round.Competition.Title,
                                            Dance = dance.Dance.DanceName,
                                            Heat = parameters.Heat + 1
                                        };

                    if (!context.Drawings.Any(
                            d =>
                            d.Heat == viewModel.NextHeat && d.DanceRound.Id == viewModel.NextDanceId
                            && d.Round.Id == viewModel.NextRoundId))
                    {
                        viewModel.NextHeat = 0;
                        viewModel.NextDanceId++;

                        if (
                            !context.Drawings.Any(
                                d =>
                                d.Heat == viewModel.NextHeat && d.DanceRound.Id == viewModel.NextDanceId
                                && d.Round.Id == viewModel.NextRoundId))
                        {
                            viewModel.NextRoundId = -1;
                        }

                    }

                    viewModel.Drawings = context.Drawings.Where(d => d.Heat == parameters.Heat && d.Round.Id == parameters.RoundId && d.DanceRound.Id == parameters.DanceId).OrderBy(d => d.Participant.Number).ToList();

                    return this.View["Heat", viewModel];
                };
        }
    }
}
