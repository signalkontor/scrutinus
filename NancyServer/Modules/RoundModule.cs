﻿// // TPS.net TPS8 NancyServer
// // RoundModule.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using DataModel.ModelHelper;
using DataModel.Models;
using Nancy;

namespace NancyServer.ViewModels
{
    public class RoundViewModel
    {
        public Round Round { get; set; }
    }

    public class QualifiedViewModel : IHasCompetition
    {
        public Round Round { get; set; }

        public IEnumerable<Participant> Participants { get; set; }

        public Competition Competition
        {
            get { return this.Round.Competition; }
        }

        public IEnumerable<Competition> RunningCompetitions { get; set; }
    }

    public class AllRoundsViewModel
    {
        public IEnumerable<Round> Rounds { get; set; }

        public Competition Competition { get; set; }
    }

    public class DrawingViewModel : IHasCompetition
    {
        public Round Round { get; set; }

        public Dictionary<DanceInRound, List<List<Participant>>> Heats { get; set; }

        public Competition Competition
        {
            get { return this.Round.Competition; }
        }

        public IEnumerable<Competition> RunningCompetitions { get; set; }
    }

    public class RoundModule : NancyModule
    {
        public RoundModule()
        {
            this.Get["/Round/{id:int}"] = compid =>
                {
                    Thread.CurrentThread.CurrentCulture = new CultureInfo(HttpServer.ApplicationLocale ?? "en");
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(HttpServer.ApplicationLocale ?? "en");

                    var dataContext = new ScrutinusContext();

                    int id = compid.id;
                    var competition = dataContext.Competitions.Single(c => c.Id == id);

                    if (competition.CurrentRound == null)
                    {
                        return this.Response.AsRedirect("/Competition/" + competition.Id);
                    }

                    return this.View["Index", new RoundViewModel(){Round = competition.CurrentRound}];
                };

            this.Get["/Round/Officials/{id:int}"] = competitionId =>
                {
                    Thread.CurrentThread.CurrentCulture = new CultureInfo(HttpServer.ApplicationLocale ?? "en");
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(HttpServer.ApplicationLocale ?? "en");

                    var dataContext = new ScrutinusContext();
                    int id = competitionId.id;
                    var competition = dataContext.Competitions.SingleOrDefault(c => c.Id == id);

                    return this.View["Officials", new CompetitionViewModel(){Competition = competition, RunningCompetitions = ViewModelFactory.GetRunningCompetitions(dataContext)}];
                };

            this.Get["/Round/Qualified/{id:int}"] = roundId =>
                {
                    Thread.CurrentThread.CurrentCulture = new CultureInfo(HttpServer.ApplicationLocale ?? "en");
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(HttpServer.ApplicationLocale ?? "en");

                    var dataContext = new ScrutinusContext();
                    int id = roundId.id;
                    var round = dataContext.Rounds.Single(r => r.Id == id);
                    var participants = round.Qualifieds.Select(q => q.Participant).OrderBy(p => p.Number).ToList();

                    return this.View["Qualified", new QualifiedViewModel(){Round = round, Participants = participants, RunningCompetitions = ViewModelFactory.GetRunningCompetitions(dataContext) }];
                    
                };

            this.Get["/Rounds/{id:int}"] = competitionId =>
                {
                    Thread.CurrentThread.CurrentCulture = new CultureInfo(HttpServer.ApplicationLocale ?? "en");
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(HttpServer.ApplicationLocale ?? "en");

                    var dataContext = new ScrutinusContext();
                    int id = competitionId.id;
                    var competition = dataContext.Competitions.Single(c => c.Id == id);
                    var rounds = competition.Rounds.OrderBy(r => r.Number).ToList();

                    return this.View["Rounds", new AllRoundsViewModel() { Competition = competition, Rounds = rounds}];
                };

            this.Get["/Round/Drawing/{id:int}"] = roundId =>
                {
                    Thread.CurrentThread.CurrentCulture = new CultureInfo(HttpServer.ApplicationLocale ?? "en");
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(HttpServer.ApplicationLocale ?? "en");

                    var dataContext = new ScrutinusContext();
                    int id = roundId.id;

                    var round = dataContext.Rounds.Single(r => r.Id == id);

                    var heats = HeatsHelper.GetHeats(dataContext, roundId.id);

                    return this.View["Drawing", new DrawingViewModel(){Round = round, Heats = heats, RunningCompetitions = ViewModelFactory.GetRunningCompetitions(dataContext) }];
                };

            this.Get["/Round/DrawingWithNames/{id:int}"] = roundId =>
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo(HttpServer.ApplicationLocale ?? "en");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(HttpServer.ApplicationLocale ?? "en");

                int id = roundId.id;

                var dataContext = new ScrutinusContext();

                var round = dataContext.Rounds.Single(r => r.Id == id);
                var heats = HeatsHelper.GetHeats(dataContext, roundId.id);

                return this.View["DrawingWithNames", new DrawingViewModel() { Round = round, Heats = heats, RunningCompetitions = ViewModelFactory.GetRunningCompetitions(dataContext) }];
            };

            this.Get["/Round/FinalResult/{id:int}"] = competitionId =>
                {
                    Thread.CurrentThread.CurrentCulture = new CultureInfo(HttpServer.ApplicationLocale ?? "en");
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(HttpServer.ApplicationLocale ?? "en");

                    return this.View["FinalResult", ViewModelFactory.GetFinalResultViewModel(competitionId.id)];
                };

            this.Get["/Round/Results/{id:int}"] = request =>
                {
                    Thread.CurrentThread.CurrentCulture = new CultureInfo(HttpServer.ApplicationLocale ?? "en");
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(HttpServer.ApplicationLocale ?? "en");

                    return this.View["Result", ViewModelFactory.GetResultGroupedViewModel(request.id)];
                };

            this.Get["/Round/ResultsByRegion/{id:int}"] = request =>
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo(HttpServer.ApplicationLocale ?? "en");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(HttpServer.ApplicationLocale ?? "en");

                return this.View["ResultByRegion", ViewModelFactory.GetResultGroupedByRegionViewModel(request.id)];
            };
        }
    }
}
