﻿// // TPS.net TPS8 NancyServer
// // HtmlGenerator.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.IO;
using System.Linq;
using System.Threading;
using Nancy;
using Nancy.Testing;

namespace NancyServer
{
    public class HtmlGenerator
    {
        private readonly Browser browser;

        public HtmlGenerator()
        {
            this.browser = new Browser(new DefaultNancyBootstrapper());
        }

        public byte[] GeneratePage(string url, string key, string value)
        {
            // Write list of competitions:

            var response = this.GetBrowserResponse(url, key, value);

            var bytes = response.Body.ToArray();

            return bytes;
        }

        public BrowserResponse GetBrowserResponse(string url, string key, string value)
        {
            BrowserResponse response = null;
            var errorCount = 0;

            while (response == null && errorCount < 20)
            {
                try
                {
                    response = this.browser.Get(url,
                        (with) =>
                        {
                            with.HttpRequest();
                            with.Query(key, value);
                        });
                }
                catch (IOException)
                {
                    Thread.Sleep(500);
                    errorCount++;
                }
            }

            return response;
        }
    }
}
