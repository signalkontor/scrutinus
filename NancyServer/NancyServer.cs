﻿// // TPS.net TPS8 NancyServer
// // NancyServer.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using DataModel.Models;
using Nancy;
using Nancy.Hosting.Self;
using NancyServer.Modules;

namespace NancyServer
{
    public class HttpServer
    {
        private readonly NancyHost host;

        private bool isRunning = false;

        

        public HttpServer(Uri[] uris, string connectionString, INancyUserManager userManager)
        {
            // We call this once to set the datasource globaly:
            ScrutinusContext.CurrentDataSource = connectionString;

            var hostConfigs = new HostConfiguration();
            hostConfigs.UrlReservations.CreateAutomatically = true;
            hostConfigs.RewriteLocalhost = true;

            // this is needed in 
            hostConfigs.AllowChunkedEncoding = false;

            this.host = new NancyHost(new ApplicationBootstrapper(userManager), hostConfigs, uris);
            StaticConfiguration.DisableErrorTraces = false;
        }

        public static string ApplicationLocale { get; set; }

        public static bool ShowCarouselDroppedOut { get; set; }

        public static int MaxCouplesPerPageInCarousel { get; set; }
        public bool IsRunning
        {
            get
            {
                return this.isRunning;
            }
        }

        public void StartHost()
        {
            this.host.Start();
            this.isRunning = true;
        }

        public void StopHost()
        {
            this.host.Stop();
            this.isRunning = false;
        }

        public void SetNewDatabaseConnection(string connectionString)
        {
            ScrutinusContext.CurrentDataSource = connectionString;
            var context = new ScrutinusContext();
            
            HomeModule.Competitions = context.Competitions.AsNoTracking().ToList();
        }

        public static Uri[] GetUriList(int port)
        {
            var uriParams = new List<Uri>();
            var hostName = Dns.GetHostName();

            // Host name URI
            var hostNameUri = string.Format("http://{0}:{1}", Dns.GetHostName(), port);
            uriParams.Add(new Uri(hostNameUri));

            // Host address URI(s)
            var hostEntry = Dns.GetHostEntry(hostName);
            foreach (var ipAddress in hostEntry.AddressList)
            {
                if (ipAddress.AddressFamily == AddressFamily.InterNetwork)  // IPv4 addresses only
                {
                    var addrBytes = ipAddress.GetAddressBytes();
                    var hostAddressUri = string.Format("http://{0}.{1}.{2}.{3}:{4}",
                    addrBytes[0], addrBytes[1], addrBytes[2], addrBytes[3], port);
                    uriParams.Add(new Uri(hostAddressUri));
                }
            }

            // Localhost URI
            uriParams.Add(new Uri(string.Format("http://localhost:{0}", port)));
            return uriParams.ToArray();
        }
    }
}
