﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WDSGJudgesGenerator
{
    class Program
    {
        private string[] chars = {"A", "B", "C", "E", "F", "G", "H", "I", "J", "K"};

        private string getSign(int index)
        {
            var first = index/10;
            var second = index%10;

            return chars[first] + chars[second];
        }

        private void GenerateXML()
        {
            var sr = new System.IO.StreamReader(@"C:\eJudge\Daten Kaohsiung\judges.csv");
            var sw = new System.IO.StreamWriter(@"C:\eJudge\Daten Kaohsiung\judges.xml");
            var counter = 0;
            while (!sr.EndOfStream)
            {
                var data = sr.ReadLine().Split(',');
                counter++;
                sw.WriteLine("<Official Id=\"{0}\" Sign=\"{1}\" ExternalId=\"\" Firstname=\"{2}\" Lastname=\"{3}\" Country=\"{4}\" MIN=\"\" Roles=\"1\"/>",
                    counter, getSign(counter), data[3], data[4], data[7]
                    );
            }

            sr.Close();
            sw.Close();
        }

        static void Main(string[] args)
        {
            var prg = new Program();
            prg.GenerateXML();
        }
    }
}
