﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

using MailClient;

namespace EmailSender
{
    class Program
    {
        private static string text = @"Sehr geehrte Damen und Herren,

TPS geht in die Zukunft und erhält einen völlig neu entwickelten Nachfolger auf Basis modernster Technik. Und ist bereit für die Zukunft ESV.

Entdecken Sie die neuen Möglichkeiten von TPS.net 2015:

- Unterstützt das ESV des DTV
- Unterstützt alle gängigen DTV sowie WDSF Turniere
- Tablet Server für Turnierleitung: iPad/Tablet statt Papier
- eJudge: Android basiertes digitales Wertungssystem
- Netzwerkfähig, elektronischer Check-in
- Beamer / Projektorunterstützung.
- Unterstützt das neue Wertungssystem JS2.1
- Modernes Design, intuitive Nutzung
- Lauffähig ab Windows XP

Die Software wird bereits weltweit eingesetzt, u.a. in Korea, Taiwan, Finnland, Dänemark und Spanien. 

Sie sind herzlich eingeladen, die neue Software zu testen. Den Download finden Sie unter:
http://tps-ejudge.de/?page_id=84

Anbei zu dieser eMail finden Sie einen Flyer mit weiteren Informationen.

Mit freundlichen Grüßen,

Olav Gröhn
";

        static void Main(string[] args)
        {
            var mailClient = new Mailer
            {
                IsAsync = false,
                MessageType = Mailer.MediaType.Plain,
                EncodingType = Encoding.Unicode,
            };

            var mailAddressTo = new MailAddressCollection();
            mailAddressTo.Add("olav.groehn@signalkontor.com");
            mailClient.IsAsync = false;
            mailClient.To = mailAddressTo;

            mailClient.Send("Hello World", text);
        }
    }
}
