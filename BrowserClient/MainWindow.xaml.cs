﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BrowserClient.Model;
using CefSharp;
using NetMQ;
using NetMQ.Sockets;
using Newtonsoft.Json;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, IDisposable
    {
        private bool listenerRunning;
        private NetMQBeacon beacon;
        private NetMQPoller poller;
        private Thread receiverTask;
        private RequestSocket requestSocket;
        private string remoteHostIp;
        private bool running = true;
        private double logWidth = 350d;
        private string myIpAddress = null;
        TextBoxOutputter outputter;

        CancellationTokenSource tokenSource = new CancellationTokenSource();
        
        public MainWindow()
        {
            CefSharpSettings.SubprocessExitIfParentProcessClosed = true;

            InitializeComponent();

            outputter = new TextBoxOutputter(Output);
            Console.SetOut(outputter);
            Console.WriteLine("Toggle Full-Screen With Key F,\nHide/Show this Log Panel by pressing Key L (L like Log)");

            var fadeInAnimation = CreateAnimationFadeIn();
            var fadeOutAnimation = CreateAnimationFadeOut();

            beacon = new NetMQBeacon();
            beacon.Configure(6000);
            beacon.ReceiveReady += BeaconOnReceiveReady;
            beacon.Subscribe("");

            poller = new NetMQPoller { beacon };
            Task.Factory.StartNew(poller.Run);

            Application.Current.Exit += ApplicationOnExit;

            this.KeyUp += (sender, args) =>
            {
                if (args.Key == Key.L)
                {
                    logWidth = logWidth > 0 ? 0 : 350;
                    outputter.LogIsVisible = logWidth > 0;
                }

                if (args.Key == Key.F)
                {
                    if (WindowState == WindowState.Maximized)
                    {
                        WindowState = WindowState.Normal;
                        WindowStyle = WindowStyle.SingleBorderWindow;
                        ResizeMode = ResizeMode.CanResize;
                        Topmost = false;
                    }
                    else
                    {
                        WindowStyle = WindowStyle.None;
                        WindowState = WindowState.Maximized;
                        ResizeMode = ResizeMode.NoResize;
                        Topmost = true;
                    }
                }

                this.LogColumn.Width = new GridLength(logWidth);
            };
        }

        private void ApplicationOnExit(object sender, ExitEventArgs e)
        {
            Cef.Shutdown();
            Dispose();
        }

        private void StartListener(string hostAddress)
        {
            if (listenerRunning)
            {
                return;
            }

            listenerRunning = true;

            receiverTask = new Thread(() =>
            {

                using (var subscriber = new SubscriberSocket())
                {
                    subscriber.Connect(hostAddress);
                    subscriber.Subscribe("opacity");

                    while (running)
                    {
                        try
                        {
                            subscriber.TryReceiveFrameString(TimeSpan.FromSeconds(5), out string commandString);

                            if (commandString == null || commandString == "opacity")
                            {
                                continue;
                            }

                            var command = JsonConvert.DeserializeObject<Command>(commandString);

                            Console.WriteLine($"Received Command {commandString}");

                            Dispatcher.Invoke(() =>
                            {
                                if (command.Opacity >= 0)
                                {
                                    if (Math.Abs(command.Opacity - Browser2.Opacity) < 0.1)
                                    {
                                        Debug.WriteLine(
                                            $"Difference is {Math.Abs(command.Opacity - Browser2.Opacity)}");
                                        Browser2.BeginAnimation(OpacityProperty, null);
                                        Browser2.Opacity = command.Opacity;
                                    }
                                    else
                                    {
                                        DoubleAnimation animation = new DoubleAnimation
                                        {
                                            From = Browser2.Opacity,
                                            To = command.Opacity,
                                            Duration = new Duration(TimeSpan.FromMilliseconds(500)),
                                            AutoReverse = false,
                                            RepeatBehavior = new RepeatBehavior(1)
                                        };

                                        Browser2.BeginAnimation(OpacityProperty, animation);
                                    }
                                }

                                if (!string.IsNullOrWhiteSpace(command.AddressBrowser1))
                                {
                                    Browser1.Address = command.AddressBrowser1.Replace("http://localhost",
                                        $"http://{remoteHostIp}");
                                }

                                if (!string.IsNullOrWhiteSpace(command.AddressBrowser2))
                                {
                                    Browser2.Address = command.AddressBrowser2.Replace("http://localhost",
                                        $"http://{remoteHostIp}");
                                }
                            });
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("Exception in receive thread: " + ex.Message);
                        }
                    }
                    subscriber.Disconnect(hostAddress);
                }
            });
            receiverTask.IsBackground = true;
            receiverTask.Start();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            running = false;
            poller.Stop();
            beacon.Unsubscribe();
            tokenSource.Cancel();         
        }

        private void BeaconOnReceiveReady(object sender, NetMQBeaconEventArgs e)
        {
            var message = e.Beacon.Receive();

            Console.WriteLine($"Received beacon Message: {message.String} from {message.PeerAddress}");
            this.remoteHostIp = message.PeerHost;
            this.StartListener($"tcp://{message.PeerHost}:{message.String}");
            this.StartStatusThread();
        }

        private static DoubleAnimation CreateAnimationFadeIn()
        {
            DoubleAnimation animation = new DoubleAnimation
            {
                From = 0,
                To = 1,
                Duration = new Duration(TimeSpan.FromSeconds(1)),
                AutoReverse = false,
                RepeatBehavior = new RepeatBehavior(1)
            };

            return animation;
        }

        private static DoubleAnimation CreateAnimationFadeOut()
        {
            DoubleAnimation animation = new DoubleAnimation
            {
                From = 1,
                To = 0,
                Duration = new Duration(TimeSpan.FromSeconds(1)),
                AutoReverse = false,
                RepeatBehavior = new RepeatBehavior(1)
            };

            return animation;
        }

        private void StartStatusThread()
        {
            var thread = new Thread(() =>
            {
                while (running)
                {
                    try
                    {
                        Dispatcher.Invoke(() => SendStatus(Browser1.Address, Browser2.Address));
                        Thread.Sleep(5000);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        Thread.Sleep(5000);
                    }
                }
            });

            thread.IsBackground = true;
            thread.Name = "MainWindowsStatusThread";
            thread.Start();
        }

        private void SendStatus(string urlLoaded1, string urlLoaded2)
        {
            using (var requestSocket = new RequestSocket($">tcp://{this.remoteHostIp}:5557"))
            {
                var status = new Status()
                {
                    HostName = Environment.MachineName,
                    Endpoint = GetMyIpAddress(),
                    UrlLoaded1 = urlLoaded1,
                    UrlLoaded2 = urlLoaded2
                };

                requestSocket.SendFrame(JsonConvert.SerializeObject(status));
            }
        }

        private string GetMyIpAddress()
        {
            if (myIpAddress != null)
            {
                return myIpAddress;
            }

            using (Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, 0))
            {
                socket.Connect(remoteHostIp, 65530);
                IPEndPoint endPoint = socket.LocalEndPoint as IPEndPoint;
                var localIp = endPoint.Address.ToString();
                Console.WriteLine($"My IP is {localIp}");
                myIpAddress = endPoint.Address.ToString();
                return myIpAddress;
            }
        }

        private void Browser1_OnFrameLoadEnd(object sender, FrameLoadEndEventArgs e)
        {
            Debug.WriteLine("Frame loaded");
            if (this.remoteHostIp != null)
            {
                Dispatcher.Invoke(() => SendStatus(Browser1.Address, Browser2.Address));
            }
        }

        public void Dispose()
        {
            beacon?.Dispose();
            poller?.Dispose();
            requestSocket?.Dispose();
            outputter?.Dispose();
            tokenSource?.Dispose();
            Browser1?.Dispose();
            Browser2?.Dispose();
            running = false;
            receiverTask?.Abort();
        }
    }

    public class TextBoxOutputter : TextWriter
    {
        readonly TextBox textBox = null;

        public TextBoxOutputter(TextBox output)
        {
            textBox = output;
            LogIsVisible = true;
        }

        public bool LogIsVisible { get; set; }

        public override void Write(char value)
        {
            if (!LogIsVisible)
            {
                return;
            }

            base.Write(value);
            textBox.Dispatcher.BeginInvoke(new Action(() =>
            {
                if (textBox.Text.Length > 1000)
                {
                    textBox.Text = textBox.Text.Substring(500, textBox.Text.Length - 500);
                }

                textBox.AppendText(value.ToString());
            }));
        }

        public override Encoding Encoding => System.Text.Encoding.UTF8;
    }


}
