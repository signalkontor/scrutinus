﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace BrowserClient.Model
{
    public class Status
    {
        public string HostName { get; set; }

        public string UrlLoaded1 { get; set; }

        public string UrlLoaded2 { get; set; }

        public string Endpoint { get; set; }
    }

    public class HostEntry : INotifyPropertyChanged
    {
        private string hostName;

        private string currentUrl1;

        private string currentUrl2;

        public string HostName
        {
            get => hostName;
            set
            {
                hostName = value;
                OnPropertyChanged(nameof(HostName));
            }
        }

        public string CurrentUrl1
        {
            get => currentUrl1;
            set
            {
                currentUrl1 = value;
                OnPropertyChanged(nameof(CurrentUrl1));
            }
        }

        public string CurrentUrl2
        {
            get => currentUrl2;
            set
            {
                currentUrl2 = value;
                OnPropertyChanged(nameof(CurrentUrl2));
            }
        }

        public string Endpoint { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }


    }

    public class Command
    {
        public string AddressBrowser1 { get; set; }

        public string AddressBrowser2 { get; set; }

        public double Opacity { get; set; }

    }
}
