﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data.SqlServerCe;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

using DataModel.Models;

using Microsoft.Win32;

class Program
{
    static void Main()
    {
        CopyDataBase();

        // Retrieve the enumerator instance and then the data.
        SqlDataSourceEnumerator instance = SqlDataSourceEnumerator.Instance;
        System.Data.DataTable table = instance.GetDataSources();

        var list = GetLocalSqlServerInstanceNames();

        var connection = new SqlConnection(@"Data Source=.\SQLEXPRESS;Initial Catalog=Scrutinus;Persist Security Info=True;User ID=scrutinus;Password=scrutinus;MultipleActiveResultSets=True");
        connection.Open();

        // Create a new Database:
        //var createCommand = connection.CreateCommand();
        //createCommand.CommandText = "CREATE DATABASE [TESTDBTEST]";
        //createCommand.ExecuteNonQuery();

        var command = connection.CreateCommand();
        command.CommandText = "select * from sys.databases WHERE name NOT IN ('master', 'tempdb', 'model', 'msdb')";
        var result = command.ExecuteReader();
        while (result.Read())
        {
            for (int i = 0; i < result.FieldCount; i++)
            {
                Console.WriteLine("{0} = {1}", result.GetName(i), result.GetValue(i));
            }
            

            Console.WriteLine("Name: " + result.GetValue(0));
        }
        result.Close();

        var command2 = connection.CreateCommand();
        command2.CommandText = "select * From AgeGroups";
        var result2 = command2.ExecuteReader();

        Console.WriteLine();

        Console.WriteLine("Press any key to continue.");
        Console.ReadKey();
    }

    private static void CopyDataBase()
    {
        var destinationConnection = new SqlConnection(@"Data Source=.\SQLEXPRESS;Initial Catalog=TESTDBTEST;Persist Security Info=True;User ID=sa;Password=nuwanda;MultipleActiveResultSets=True");
        ScrutinusContext.DoNotInitializeDatabase = true;

        var context = new ScrutinusContext(@"Data Source=.\SQLEXPRESS;Initial Catalog=TESTDBTEST;Persist Security Info=True;User ID=sa;Password=nuwanda;MultipleActiveResultSets=True", null);
        var officials = context.Officials.ToList();

        destinationConnection.Open();
        var sourceConnection = new SqlCeConnection(@"DataSource=C:\Users\Olav\Documents\TPS.net\Ostsee tanzt.sdf");
        sourceConnection.Open();

        CopyTable("AgeGroups", sourceConnection, destinationConnection);
        CopyTable("Classes", sourceConnection, destinationConnection);
        CopyTable("Sections", sourceConnection, destinationConnection);
        CopyTable("ClimbUpTables", sourceConnection, destinationConnection);
        CopyTable("RoundType", sourceConnection, destinationConnection);
        CopyTable("CompetitionRule", sourceConnection, destinationConnection);
        CopyTable("CompetitionType", sourceConnection, destinationConnection);
        CopyTable("Events", sourceConnection, destinationConnection);
        CopyTable("Dance", sourceConnection, destinationConnection);
        CopyTable("DanceTemplate", sourceConnection, destinationConnection);
        CopyTable("DrawingType", sourceConnection, destinationConnection);
        CopyTable("JudgingComponents", sourceConnection, destinationConnection);
        CopyTable("JudgingComponentCompetitionTypes", sourceConnection, destinationConnection);
        CopyTable("Clubs", sourceConnection, destinationConnection);
        CopyTable("Official", sourceConnection, destinationConnection);
        CopyTable("Parameters", sourceConnection, destinationConnection);
        CopyTable("CountryCodes", sourceConnection, destinationConnection);
        CopyTable("Role", sourceConnection, destinationConnection);
        CopyTable("RoleOfficials", sourceConnection, destinationConnection);

        CopyTable("Couple", sourceConnection, destinationConnection);
        
        CopyTable("Competitions", sourceConnection, destinationConnection);
        CopyTable("DancesInCompetitions", sourceConnection, destinationConnection);
        CopyTable("JudgingSheetDatas", sourceConnection, destinationConnection);

        CopyTable("Rounds", sourceConnection, destinationConnection);
        CopyTable("DanceInRounds", sourceConnection, destinationConnection);
        
        CopyTable("Participants", sourceConnection, destinationConnection);
        CopyTable("Drawings", sourceConnection, destinationConnection);

        CopyTable("ClimbUps", sourceConnection, destinationConnection);
        CopyTable("OfficialInCompetitions", sourceConnection, destinationConnection);
        CopyTable("Qualifieds", sourceConnection, destinationConnection);
        CopyTable("Startbuches", sourceConnection, destinationConnection);
        CopyTable("SectionAgeGroupClassesMappings", sourceConnection, destinationConnection);
        CopyTable("SectionAgeGroupClassesMappingClasses", sourceConnection, destinationConnection);
        
        CopyTable("Markings", sourceConnection, destinationConnection);

        // ToDo: CurrentRound_ID in Competitions muss anschließend neu gesetzt werden
        UpdateCurrentRoundInCompetition(sourceConnection, destinationConnection);
    }

    private static void UpdateCurrentRoundInCompetition(DbConnection sourceConnection, DbConnection destinationConnection)
    {
        string sql = "Select Id, CurrentRound_Id from Competitions";
        var command = sourceConnection.CreateCommand();
        command.CommandText = sql;
        var sourceReader = command.ExecuteReader();

        while (sourceReader.Read())
        {
            if (sourceReader.IsDBNull(1))
            {
                continue;
            }

            var updateSql = string.Format(
                "Update Competitions Set CurrentRound_Id={1} WHERE Id={0}",
                sourceReader.GetInt32(0),
                sourceReader.GetInt32(1));

            var updateCommand = destinationConnection.CreateCommand();
            updateCommand.CommandText = updateSql;
            updateCommand.ExecuteNonQuery();
        }

        sourceReader.Close();
    }

    private static void CopyTable(string tableName, DbConnection sourceConnection, DbConnection destinatoinConnection)
    {
        Console.WriteLine("Copy Table " + tableName);

        using (var transaction = destinatoinConnection.BeginTransaction())
        {
            string sql;
            DbCommand command;

            if (tableName != "JudgingComponentCompetitionTypes" 
                && tableName != "RoleOfficials"
                && tableName != "SectionAgeGroupClassesMappingClasses")
            {
                sql = string.Format("SET IDENTITY_INSERT [{0}] ON", tableName);
                command = destinatoinConnection.CreateCommand();
                command.CommandText = sql;
                command.Transaction = transaction;
                command.ExecuteNonQuery();
            }

            var sourceCommand = sourceConnection.CreateCommand();
            sourceCommand.CommandText = "select * from " + tableName;
            var reader = sourceCommand.ExecuteReader();

            var sqlInsert = CreateSqlInsert(tableName, reader);

            while (sqlInsert != null)
            {
                var insertCommand = destinatoinConnection.CreateCommand();
                insertCommand.CommandText = sqlInsert;
                insertCommand.Transaction = transaction;
                insertCommand.ExecuteNonQuery();

                sqlInsert = CreateSqlInsert(tableName, reader);
            }

            reader.Close();

            if (tableName != "JudgingComponentCompetitionTypes" 
                && tableName != "RoleOfficials"
                && tableName != "SectionAgeGroupClassesMappingClasses")
            {
                sql = string.Format("SET IDENTITY_INSERT [{0}] OFF", tableName);
                command = destinatoinConnection.CreateCommand();
                command.CommandText = sql;
                command.Transaction = transaction;
                command.ExecuteNonQuery();
            }
            transaction.Commit();
        }
    }

    private static string CreateSqlInsert(string tableName, DbDataReader reader)
    {
        if (!reader.Read())
        {
            return null;
        }

        var names = new List<string>();
        var values = new List<string>();

        for (var i = 0; i < reader.FieldCount; i++)
        {
            if (reader.GetName(i) == "CurrentRound_Id")
            {
                // We cannot set CurrentRound_Id, because we habe not yet importet rounds
                // and we cannot import rounds before we importet competitions
                continue;
            }

            names.Add("[" + reader.GetName(i) + "]");
                

            if (reader.IsDBNull(i))
            {
                values.Add("NULL");
            }
            else
            {
                var value = reader.GetValue(i);
                if (value is bool)
                {
                    var b = (bool)value;
                    values.Add(b ? "1" : "0");
                    continue;
                }
                if (value is DateTime)
                {
                    var d = (DateTime)value;
                    values.Add(string.Format("convert(datetime, '{0}', 20)",d.ToString("yyyy-MM-dd HH:mm:ss")));
                    continue;
                }
                if (value is byte[])
                {
                    values.Add("0x" + BitConverter.ToString((byte[])value).Replace("-", ""));
                    continue;
                }
                if (value is decimal || value is double)
                {
                    var str = value.ToString();
                    values.Add(str.Replace(",", "."));
                    continue;
                }
                // Default. String or number
                values.Add(value is string ? "N'" + reader.GetValue(i).ToString().Replace("'", "''") + "'" : reader.GetValue(i).ToString());        
            }
        }

        var sql = string.Format(
            "Insert into [{2}] ({0}) Values ({1})",
            string.Join(",", names),
            string.Join(",", values),
            tableName);

        return sql;
    }

    private static void DisplayData(System.Data.DataTable table)
    {
        foreach (System.Data.DataRow row in table.Rows)
        {
            foreach (System.Data.DataColumn col in table.Columns)
            {
                Console.WriteLine("{0} = {1}", col.ColumnName, row[col]);
            }
            Console.WriteLine("============================");
        }
    }

    public static IList<string> GetLocalSqlServerInstanceNames()
    {
        RegistryValueDataReader registryValueDataReader = new RegistryValueDataReader();

        string[] instances64Bit = registryValueDataReader.ReadRegistryValueData(RegistryHive.Wow64,
                                                                                Registry.LocalMachine,
                                                                                @"SOFTWARE\Microsoft\Microsoft SQL Server",
                                                                                "InstalledInstances");

        string[] instances32Bit = registryValueDataReader.ReadRegistryValueData(RegistryHive.Wow6432,
                                                                                Registry.LocalMachine,
                                                                                @"SOFTWARE\Microsoft\Microsoft SQL Server",
                                                                                "InstalledInstances");

        //FormatLocalSqlInstanceNames(ref instances64Bit);
        // FormatLocalSqlInstanceNames(ref instances32Bit);

        IList<string> localInstanceNames = new List<string>(instances64Bit);

        localInstanceNames = localInstanceNames.Union(instances32Bit).ToList();

        return localInstanceNames;
    }
}

public enum RegistryHive
{
    Wow64,
    Wow6432
}

public class RegistryValueDataReader
{
    private static readonly int KEY_WOW64_32KEY = 0x200;
    private static readonly int KEY_WOW64_64KEY = 0x100;

    private static readonly UIntPtr HKEY_LOCAL_MACHINE = (UIntPtr)0x80000002;

    private static readonly int KEY_QUERY_VALUE = 0x1;

    [DllImport("advapi32.dll", CharSet = CharSet.Unicode, EntryPoint = "RegOpenKeyEx")]
    static extern int RegOpenKeyEx(
                UIntPtr hKey,
                string subKey,
                uint options,
                int sam,
                out IntPtr phkResult);


    [DllImport("advapi32.dll", SetLastError = true)]
    static extern int RegQueryValueEx(
                IntPtr hKey,
                string lpValueName,
                int lpReserved,
                out uint lpType,
                IntPtr lpData,
                ref uint lpcbData);

    private static int GetRegistryHiveKey(RegistryHive registryHive)
    {
        return registryHive == RegistryHive.Wow64 ? KEY_WOW64_64KEY : KEY_WOW64_32KEY;
    }

    private static UIntPtr GetRegistryKeyUIntPtr(RegistryKey registry)
    {
        if (registry == Registry.LocalMachine)
        {
            return HKEY_LOCAL_MACHINE;
        }

        return UIntPtr.Zero;
    }

    public string[] ReadRegistryValueData(RegistryHive registryHive, RegistryKey registryKey, string subKey, string valueName)
    {
        string[] instanceNames = new string[0];

        int key = GetRegistryHiveKey(registryHive);
        UIntPtr registryKeyUIntPtr = GetRegistryKeyUIntPtr(registryKey);

        IntPtr hResult;

        int res = RegOpenKeyEx(registryKeyUIntPtr, subKey, 0, KEY_QUERY_VALUE | key, out hResult);

        if (res == 0)
        {
            uint type;
            uint dataLen = 0;

            RegQueryValueEx(hResult, valueName, 0, out type, IntPtr.Zero, ref dataLen);

            byte[] databuff = new byte[dataLen];
            byte[] temp = new byte[dataLen];

            List<String> values = new List<string>();

            GCHandle handle = GCHandle.Alloc(databuff, GCHandleType.Pinned);
            try
            {
                RegQueryValueEx(hResult, valueName, 0, out type, handle.AddrOfPinnedObject(), ref dataLen);
            }
            finally
            {
                handle.Free();
            }

            int i = 0;
            int j = 0;

            while (i < databuff.Length)
            {
                if (databuff[i] == '\0')
                {
                    j = 0;
                    string str = Encoding.Default.GetString(temp).Trim('\0');

                    if (!string.IsNullOrEmpty(str))
                    {
                        values.Add(str);
                    }

                    temp = new byte[dataLen];
                }
                else
                {
                    temp[j++] = databuff[i];
                }

                ++i;
            }

            instanceNames = new string[values.Count];
            values.CopyTo(instanceNames);
        }

        return instanceNames;
    }
}
