﻿using System;
using System.Linq;
using System.Threading.Tasks;
using FtsRestAPI;
using FtsRestAPI.Helper;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FtsUnitTests
{
    [TestClass]
    public class ApiTests
    {
        [TestMethod]
        public async Task GetCurrentCompetitions()
        {
            var apiClient = new FtsRestClient("test", "ftstest");
            var competitionList = await apiClient.GetCurrentCompetitions();

            Assert.IsTrue(competitionList.Competitions.Length > 0);
        }

        [TestMethod]
        public async Task GetCurrentCompetitionsAsCompetitionObject()
        {
            var apiClient = new FtsRestClient("test", "ftstest");
            var competitionList = await apiClient.GetCurrentCompetitions();

            var events = ModelHelper.GetEvents(competitionList);

            Assert.IsTrue(events.Any());
            Assert.IsTrue(events.All(e => !string.IsNullOrWhiteSpace(e.Title)));
        }
    }
}
