﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace TPS.Heats.Model
{
    public class Heat
    {
        public int Id { get; set; }

        public virtual Couple Couple { get; set; }

        [JsonIgnore]
        public virtual Round Round { get; set; }

        [MaxLength(100)]
        public string Dance { get; set; }

        public int HeatIndex { get; set; }

        public int DanceIndex { get; set; }
    }
}