﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace TPS.Heats.Model
{
    public class HeatsContext : DbContext
    {
        public HeatsContext() : this("Data Source=.\\SQLEXPRESS;Database=heats.qa;Integrated Security=False;User Id=heats;Password=I$S4V5%VP/V9rS^(3)l1;MultipleActiveResultSets=True")
        {
            
        }

        public HeatsContext(string connectionString) : this(new DbContextOptionsBuilder<HeatsContext>()
            .UseSqlServer(connectionString).Options)
        {
            
        }

        public HeatsContext(DbContextOptions<HeatsContext> options) : base(options)
        {
            
        }


        public DbSet<Couple> Couples { get; set; }

        public DbSet<Competition> Competitions { get; set; }

        public DbSet<Round> Rounds { get; set; }

        public DbSet<Heat> Heats { get; set; }

        public DbSet<Placing> Placings { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Competition>().HasMany(c => c.Rounds).WithOne(r => r.Competition).OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<Round>().HasMany(r => r.Heats).WithOne(h => h.Round).OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<Couple>().HasMany(r => r.Placings).WithOne(p => p.Couple).OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<Competition>().HasMany(c => c.Placings).WithOne(p => p.Competition).OnDelete(DeleteBehavior.Cascade);
        }
    }
}
