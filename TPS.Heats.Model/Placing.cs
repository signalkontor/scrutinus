﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPS.Heats.Model
{
    public class Placing
    {
        [Key]
        public int Id { get; set; }

        [MaxLength(50)]
        public string PlaceInCompetition { get; set; }

        public virtual Couple Couple { get; set; }

        public virtual Competition Competition { get; set; }
    }
}
