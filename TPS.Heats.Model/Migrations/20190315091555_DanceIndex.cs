﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TPS.Heats.Model.Migrations
{
    public partial class DanceIndex : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DanceIndex",
                table: "Heats",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DanceIndex",
                table: "Heats");
        }
    }
}
