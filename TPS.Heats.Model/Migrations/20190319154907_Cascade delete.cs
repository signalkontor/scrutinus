﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TPS.Heats.Model.Migrations
{
    public partial class Cascadedelete : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Placings_Competitions_CompetitionId",
                table: "Placings");

            migrationBuilder.AddForeignKey(
                name: "FK_Placings_Competitions_CompetitionId",
                table: "Placings",
                column: "CompetitionId",
                principalTable: "Competitions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Placings_Competitions_CompetitionId",
                table: "Placings");

            migrationBuilder.AddForeignKey(
                name: "FK_Placings_Competitions_CompetitionId",
                table: "Placings",
                column: "CompetitionId",
                principalTable: "Competitions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
