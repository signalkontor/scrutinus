﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TPS.Heats.Model.Migrations
{
    public partial class PlaceInCompetition : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Placings",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    PlaceInCompetition = table.Column<string>(maxLength: 50, nullable: true),
                    CoupleId = table.Column<int>(nullable: true),
                    CompetitionId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Placings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Placings_Competitions_CompetitionId",
                        column: x => x.CompetitionId,
                        principalTable: "Competitions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Placings_Couples_CoupleId",
                        column: x => x.CoupleId,
                        principalTable: "Couples",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Placings_CompetitionId",
                table: "Placings",
                column: "CompetitionId");

            migrationBuilder.CreateIndex(
                name: "IX_Placings_CoupleId",
                table: "Placings",
                column: "CoupleId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Placings");
        }
    }
}
