﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Newtonsoft.Json;

namespace TPS.Heats.Model
{
    public class Competition
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime Start { get; set; }

        public virtual ICollection<Round> Rounds { get; set; }

        [JsonIgnore]
        public virtual ICollection<Placing> Placings { get; set; }

    }
}
