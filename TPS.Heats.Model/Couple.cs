﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TPS.Heats.Model
{
    public class Couple
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        [MaxLength(250)]
        public string Name { get; set; }

        [MaxLength(20)]
        public string ExternalIdMan { get; set; }

        [MaxLength(20)]
        public string ExternalIdWoman { get; set; }

        public virtual ICollection<Placing> Placings { get; set; }
    }
}
