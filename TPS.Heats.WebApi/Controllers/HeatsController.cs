﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TPS.Heats.Model;
using TPS.Heats.WebApi.Dto;
using TPS.Heats.WebApi.Service;

namespace TPS.Heats.WebApi.Controllers
{
    [Route("api")]
    [ApiController]
    [ApiExceptionFilter]
    public class HeatsController : ControllerBase
    {
        private readonly HeatsService service;

        public HeatsController(HeatsService service)
        {
            this.service = service;
        }

        [Route("couples")]
        [HttpPost]
        public async Task<ActionResult<bool>> SaveCouples([FromBody] IEnumerable<Couple> couples)
        {
            await service.SaveCouples(couples);

            return true;
        }

        [Route("competition")]
        [HttpPost]
        public async Task UpdateCompetition([FromBody] Competition competition)
        {
            await service.UpdateCompetitions(new Competition[] {competition});
        }

        [Route("placings")]
        [HttpPost]
        public async Task UpdatePlacings([FromBody] IEnumerable<PlacingDto> placings)
        {
            await service.UpdatePlacings(placings);
        }

        [Route("competitions")]
        [HttpPost]
        public async Task UpdateCompetitions([FromBody] IEnumerable<Competition> competitions)
        {
            await service.UpdateCompetitions(competitions);
        }

        [Route("login/{dtvId}")]
        [HttpPost]
        public async Task<ActionResult<Couple>> Login(string dtvId)
        {
            return Ok(await service.GetCouple(dtvId));
        }

        [Route("competitions/{coupleId}")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CompetitionDto>>> GetCompetitions(int coupleId)
        {
            return Ok(await service.GetCompetitions(coupleId));
        }

        [Route("competitions/{competitionid}/rounds/{coupleId}")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Round>>> GetRounds(int competitionId, int coupleId)
        {
            return Ok(await service.GetRounds(competitionId, coupleId));
        }

        [Route("rounds/{roundId}/heats/{coupleId}")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Heat>>> getHeats(int roundId, int coupleId)
        {
            return Ok(await service.GetHeats(roundId, coupleId));
        }

        [Route("service/create")]
        [HttpGet]
        public async Task<ActionResult> MigrateDatabase()
        {
            await service.MigrateDb();
            return Ok("SUCCESS");
        }
    }
}