﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TPS.Heats.Model;

namespace TPS.Heats.WebApi.Dto
{
    public class CompetitionDto
    {
        private HeatsContext context;
        private Competition c;

        public CompetitionDto(HeatsContext context, Competition c, int coupleId)
        {
            this.context = context;
            this.c = c;
            var placing = context.Placings.FirstOrDefault(p => p.Competition.Id == c.Id && p.Couple.Id == coupleId);

            this.Id = c.Id;
            this.Name = c.Name;
            this.Start = c.Start;
            this.PlaceInCompetition = placing?.PlaceInCompetition;
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime Start { get; set; }

        public string PlaceInCompetition { get; set; }
    }
}
