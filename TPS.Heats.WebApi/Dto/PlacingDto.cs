﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TPS.Heats.WebApi.Dto
{
    public class PlacingDto
    {
        public int CoupleId { get; set; }

        public int CompetitionId { get; set; }

        public string PlaceInCompetition { get; set; }

    }
}
