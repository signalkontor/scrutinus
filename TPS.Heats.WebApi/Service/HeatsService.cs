﻿using System;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TPS.Heats.Model;
using TPS.Heats.WebApi.Dto;

namespace TPS.Heats.WebApi.Service
{
    public class HeatsService
    {
        private readonly HeatsContext context;

        public HeatsService(HeatsContext context)
        {
            this.context = context;
        }


        public async Task<Couple> GetCouple(string dtvId)
        {
            dtvId = dtvId.ToUpper();

            return await context.Couples.FirstAsync(c => c.ExternalIdMan == dtvId || c.ExternalIdWoman == dtvId);
        }

        public async Task SaveCouples(IEnumerable<Couple> couples)
        {
            // we remove all competitions and couples
            var competitions = context.Competitions.ToList();
            context.Competitions.RemoveRange(competitions);
            await context.SaveChangesAsync();

            // remove couples
            var existing = context.Couples.ToList();
            context.RemoveRange(existing);
            await context.SaveChangesAsync();

            // no save the new couples
            context.Couples.AddRange(couples);
            await context.SaveChangesAsync();
        }

        public async Task UpdateCompetitions(IEnumerable<Competition> competitions)
        {
            foreach (var competition in competitions)
            {
                await UpdateCompetition(competition);
            }
        }

        public async Task UpdatePlacings(IEnumerable<PlacingDto> placings)
        {
            foreach (var placing in placings)
            {
                var existing = await context.Placings.FirstOrDefaultAsync(p =>
                    p.Competition.Id == placing.CompetitionId && p.Couple.Id == placing.CoupleId);

                if (existing != null)
                {
                    existing.PlaceInCompetition = placing.PlaceInCompetition;
                }
                else
                {
                    var newPlacing = new Placing()
                    {
                        Competition = context.Competitions.FirstOrDefault(c => c.Id == placing.CompetitionId),
                        Couple = context.Couples.FirstOrDefault(c => c.Id == placing.CoupleId),
                        PlaceInCompetition = placing.PlaceInCompetition
                    };

                    context.Placings.Add(newPlacing);
                }
            }

            await context.SaveChangesAsync();
        }

        public async Task UpdateCompetition(Competition competition)
        {
            var existing = context.Competitions.FirstOrDefault(c => c.Id == competition.Id);

            if (existing != null)
            {
                context.Competitions.Remove(existing);
            }

            await context.SaveChangesAsync();

            // Fix the reference to the couple in heats:
            foreach (var round in competition.Rounds)
            {
                foreach (var heat in round.Heats)
                {
                    heat.Couple = context.Couples.FirstOrDefault(c => c.Id == heat.Couple.Id);
                }
            }

            context.Competitions.Add(competition);

            await context.SaveChangesAsync();
        }

        public async Task<IEnumerable<CompetitionDto>> GetCompetitions(int coupleId)
        {
            var competitions = await context.Competitions.Where(c => c.Rounds.Any(r => r.Heats.Any(h => h.Couple.Id == coupleId)))
                                .ToListAsync();

            return competitions.Select(c => new CompetitionDto(context, c, coupleId));
        }

        public async Task<IEnumerable<Round>> GetRounds(int competitionId, int coupleId)
        {
            var rounds = await context.Rounds
                .Include(r => r.Competition)
                .Include(r => r.Heats)
                .ThenInclude(h => h.Couple)
                .Where(r => r.Competition.Id == competitionId).ToArrayAsync();

            foreach (var round in rounds)
            {
                round.Competition.Rounds = null;
            }

            return rounds
                .Where(r => r.Heats.Any(h => h.Couple.Id == coupleId));
        }

        public async Task<IEnumerable<Heat>> GetHeats(int roundId, int coupleId)
        {
            return await context.Heats
                .Where(h => h.Round.Id == roundId && h.Couple.Id == coupleId)
                .OrderBy(h => h.DanceIndex).ToListAsync();
        }

        public async Task MigrateDb()
        {
            await this.context.Database.MigrateAsync();
        }
    }
}
