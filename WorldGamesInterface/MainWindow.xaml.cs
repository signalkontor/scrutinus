﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DataModel;
using DataModel.Models;

namespace WorldGamesInterface
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ScrutinusContext _context = new ScrutinusContext(WorldGamesSettings.Default.DataSource, null);

        public MainWindow()
        {
            InitializeComponent();

            Competition.ItemsSource = _context.Competitions.ToList();
        }

        private void BtnImport_OnClick(object sender, RoutedEventArgs e)
        {
            var importHelper = new Helper.ImportHelper();
            importHelper.ImportAtosData(ImportFolder.Text);
            MessageBox.Show("Import done");
        }


        private void Competition_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var competition = Competition.SelectedItem as DataModel.Models.Competition;
            if (competition == null) return;

            Round.ItemsSource = competition.Rounds.ToList();
        }

        private void Round_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // Hier bestimmen wir den DataPatch
            var round = Round.SelectedItem as DataModel.Models.Round;
            var competition = Competition.SelectedItem as Competition;

            if (round == null) return;

            TargetFolder.Text = String.Format(@"T:\DS0000000\DSX{0}000\", competition.WGFolderCode);

            switch (competition.WGFolderCode)
            {
                case "001":
                case "002":
                    switch (round.Number)
                    {
                        case 1:
                            TargetFolder.Text += String.Format(@"\DSX{0}900", competition.WGFolderCode); break;
                        case 2:
                            TargetFolder.Text += String.Format(@"\DSX{0}800", competition.WGFolderCode); break;
                        case 3:
                            TargetFolder.Text += String.Format(@"\DSX{0}200", competition.WGFolderCode); break;
                        case 4:
                            TargetFolder.Text += String.Format(@"\DSX{0}100", competition.WGFolderCode); break;

                    }
                    break;
                case "003":
                    switch (round.Number)
                    {
                        case 1:
                            TargetFolder.Text += String.Format(@"\DSX{0}900", competition.WGFolderCode); break;
                        case 2:
                            TargetFolder.Text += String.Format(@"\DSX{0}800", competition.WGFolderCode); break;
                        case 3:
                            TargetFolder.Text += String.Format(@"\DSX{0}200", competition.WGFolderCode); break;
                        case 4:
                            TargetFolder.Text += String.Format(@"\DSX{0}100", competition.WGFolderCode); break;
                    }
                    break;
            }           
        }

        private void SendQualified_OnClick(object sender, RoutedEventArgs e)
        {
            var round = Round.SelectedItem as DataModel.Models.Round;
            if (round == null)
            {
                MessageBox.Show("Please select Round");
                return;
            }

            if (!Directory.Exists(TargetFolder.Text))
            {
                Directory.CreateDirectory(TargetFolder.Text);

            }
            var writer = new StreamWriter(TargetFolder.Text + "\\result.csv");
            var helper = new Helper.ExportHelper();
            if (round.Number == 1 && round.Competition.FirstRoundType == RoundTypes.Redance)
            {
                helper.WriteQualifiedRedance(writer, round);
            }
            else
            {
                helper.WriteQualifiedWithPlaces(writer, round);    
            }
            
            writer.Close();
            
            if (!round.Qualifieds.Any(q => q.QualifiedNextRound) && !round.RoundType.IsFinal) return;
            if (round.Competition.FirstRoundType == DataModel.RoundTypes.Redance && !round.RoundType.IsFinal) return; // No detailed data in the frist rounds

            if (round.RoundType.IsFinal && round.Competition.State != DataModel.CompetitionState.Finished) return;
            // We export the dance results as well
            
            foreach (var dance_round in round.DancesInRound.OrderBy(d => d.SortOrder))
            {
                writer = new StreamWriter(getResultFolderName(round, dance_round.Dance.DefaultOrder) + "\\result_dance.csv");
                if (round.MarksInputType == MarkingTypes.NewJudgingSystemV2)
                {
                    helper.WriteMarkingOfDance(writer, round, dance_round.Dance.Id, _context);
                }
                else
                {
                    helper.WriteMarkingOfDance_V1(writer, round, dance_round.Dance.Id, _context);
                }

                writer.Close();
            }

            MessageBox.Show("Qualified and Results of Round have been written");
        }

        private string getResultFolderName(DataModel.Models.Round round, int order)
        {
            var root = TargetFolder.Text;
            var folderName = root.Substring(root.LastIndexOf('\\'), root.Length - root.LastIndexOf('\\') - 1);
            var path = root + "\\" + folderName + order.ToString();

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            return path;
        }

        private void SendTotal_OnClick(object sender, RoutedEventArgs e)
        {
            var competition = Competition.SelectedItem as DataModel.Models.Competition;
            if (competition == null)
            {
                MessageBox.Show("Please select the competition");
                return;
            }

            var folder = String.Format(@"T:\DS0000000\DSX{0}000\", competition.WGFolderCode);
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }

            var writer =
                new StreamWriter(String.Format(@"{0}\final_rank.csv", folder));
            var helper = new Helper.ExportHelper();
            helper.WriteFinalRank(writer, competition);
            writer.Close();
            MessageBox.Show("Total Result has been written");
        }

        private void WriteCompetitionTeams(DataModel.Models.Competition competition)
        {
            var filename = String.Format(@"{1}\DSX{0}000\comp_teams.csv", competition.WGFolderCode, ExportFolder.Text);

            var writer = new StreamWriter(filename);

            writer.WriteLine("Athlete_ID;Team_ID");
            foreach (var participant in competition.Participants.Where(p => p.State == DataModel.CoupleState.Dancing || p.State == DataModel.CoupleState.DroppedOut))
            {
                writer.WriteLine($"{participant.Couple.Athlete_Id_Man};{participant.Couple.Team_Id}");
                writer.WriteLine($"{participant.Couple.Athlete_Id_Woman};{participant.Couple.Team_Id}");
            }

            writer.Close();
        }

        private void WriteParticipantsTeams(DataModel.Models.Competition competition)
        {
            // "Team_ID";"NOC_Code";"Team_Name";"Bib_No" 
            var filename = String.Format(@"{1}\DSX{0}000\part_teams.csv", competition.WGFolderCode, ExportFolder.Text);

            var writer = new StreamWriter(filename);

            writer.WriteLine("\"Team_ID\";\"NOC_Code\";\"Team_Name\";\"Bib_No\"");
            foreach (var participant in competition.Participants.Where(p => p.State == DataModel.CoupleState.Dancing || p.State == DataModel.CoupleState.DroppedOut))
            {
                writer.WriteLine(String.Format("\"{0}\";\"{1}\";\"{2}\";\"{3}\"", 
                    participant.Couple.Team_Id,
                    participant.Couple.NOC_Code_Man,
                    participant.Couple.NiceName,
                    participant.Number
                    ));             
            }

            writer.Close();
        }

        private void WriteParticipants(DataModel.Models.Competition competition)
        {
            var filename = String.Format(@"{1}\DSX{0}000\participants.csv", competition.WGFolderCode, ExportFolder.Text);

            var writer = new StreamWriter(filename);

            writer.WriteLine("\"Athlete_ID\";\"Long_Name\";\"Short_Name\";\"NOC_Code\";\"Gender_Code\";\"Date_Of_Birth\"");
            foreach (var participant in competition.Participants.Where(p => p.State == DataModel.CoupleState.Dancing || p.State == DataModel.CoupleState.DroppedOut))
            {
                writer.WriteLine(String.Format("\"{0}\";\"{1}\";\"{2}\";\"{3}\";\"{4}\";\"{5}\"",
                        participant.Couple.Athlete_Id_Man,
                        participant.Couple.FirstMan + " " + participant.Couple.LastMan,
                        participant.Couple.FirstMan.Substring(0, 1) + ". " + participant.Couple.LastMan,
                        participant.Couple.NOC_Code_Man,
                        "M", "0"
                    ));

                writer.WriteLine(String.Format("\"{0}\";\"{1}\";\"{2}\";\"{3}\";\"{4}\";\"{5}\"",
                        participant.Couple.Athlete_Id_Woman,
                        participant.Couple.FirstWoman + " " + participant.Couple.LastWoman,
                        participant.Couple.FirstWoman.Substring(0, 1) + ". " + participant.Couple.LastWoman,
                        participant.Couple.NOC_Code_Woman,
                        "F", "0"
                    ));

            }

            writer.Close();    
        }

        private void WriteOfficials(DataModel.Models.Competition competition)
        {
            var filename = String.Format(@"{1}\DSX{0}000\officials.csv", competition.WGFolderCode, ExportFolder.Text);

            var writer = new StreamWriter(filename);
            writer.WriteLine("OfficialId;Familiy_Name;Given_Name;Country_Code;Gender_Code;Official_Function");

            foreach (var official in competition.Officials)
            {
                var role = _context.Roles.Single(r => r.Id == official.Role.Id);
                writer.WriteLine(String.Format("\"{0}\";\"{1}\";\"{2}\";\"{3}\";\"{4}\";\"{5}\"",
                    official.Official.ExternalId,
                    official.Official.Lastname,
                    official.Official.Firstname,
                    official.Official.Club,
                    official.Official.Gender,
                    official.Role.Id == 1 ? official.Official.Sign : role.RoleLong
                    ));
            }
            writer.Close();
        }

        private void BtnExport_OnClick(object sender, RoutedEventArgs e)
        {
            if (!Directory.Exists(ExportFolder.Text))
            {
                Directory.CreateDirectory(ExportFolder.Text);
            }

            // Check, a competition is selected:
            var competition = Competition.SelectedItem as DataModel.Models.Competition;
            if (competition == null)
            {
                MessageBox.Show("Please select the competition");
                return;
            }

            var folder = String.Format(@"{1}\DSX{0}000", competition.WGFolderCode, ExportFolder.Text);
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
                
            WriteCompetitionTeams(competition);
            WriteParticipantsTeams(competition);
            WriteParticipants(competition);
            WriteOfficials(competition);

            MessageBox.Show("Data export has been written");
        }
    }
}
