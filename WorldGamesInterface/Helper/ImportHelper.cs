﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using DataModel.Models;

namespace WorldGamesInterface.Helper
{
    public class ImportHelper
    {
        private ScrutinusContext _context;
        private XmlDocument _participants;

        public ImportHelper()
        {
            _context = new ScrutinusContext(WorldGamesSettings.Default.DataSource, null);
        }

        public void ImportAtosData(string path)
        {
            this.ImportParticipantsAtos(path + "\\DT_PARTIC.xml");
            this.ImportTeamsAtos(path + "\\DT_PARTIC_TEAMS.xml");
        }

        private void ImportParticipantsAtos(string file)
        {
            this._participants = new XmlDocument();
            this._participants.Load(file);
        }

        private void ImportTeamsAtos(string file)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(file);

            var list = doc.GetElementsByTagName("Team");
            foreach (var nodeObject in list)
            {
                var node = nodeObject as XmlNode;
                var teamId = node.Attributes["Code"].Value;

                var couple = _context.Couples.SingleOrDefault(c => c.Team_Id == teamId);
                if (couple == null)
                {
                    couple = new DataModel.Models.Couple()
                    {
                        Country = node.Attributes["Organisation"].Value,
                        Team_Id = node.Attributes["Code"].Value
                    };

                    _context.Couples.Add(couple);
                }
                // Search the participants:
                var athlet = (XmlNode)node.FirstChild.ChildNodes[0];
                var id = athlet.Attributes["Code"].Value;
                var a = _participants.SelectSingleNode(String.Format("//Competition/Participant[@Code='{0}']", id));
                var gender = a.Attributes["Gender"].Value;
                if (gender == "M")
                {
                    UpdateManAtos(couple, a);
                }
                else
                {
                    UpdateWomanAtos(couple, a);
                }
                    

                athlet = (XmlNode)node.FirstChild.ChildNodes[1];
                id = athlet.Attributes["Code"].Value;
                a = _participants.SelectSingleNode(String.Format("//Competition/Participant[@Code='{0}']", id));

                if (gender == "M")
                {
                    UpdateWomanAtos(couple, a);
                }
                else
                {
                    UpdateManAtos(couple, a);
                }
                // Can we find the competition? If so, add the couple
                var eventid = node.ChildNodes[1].FirstChild.Attributes["Event"].Value;
                var competition =
                    _context.Competitions.SingleOrDefault(c => c.ExternalId == eventid);
                if (competition != null)
                {
                    var participant = competition.Participants.SingleOrDefault(p => p.Couple.Team_Id == couple.Team_Id);
                    if (participant == null)
                    {
                        participant = new DataModel.Models.Participant
                        {
                            Competition = competition,
                            Couple = couple,
                            Number = 0
                        };
                        _context.Participants.Add(participant);
                    }
                }
                _context.SaveChanges();
            }
        }
        

        public void Import(string path)
        {
            // enumerate files:
            var files = System.IO.Directory.GetFiles(path);
            var fileInfos = files.Select(file => new System.IO.FileInfo(file)).ToList();
            //
            var participants = fileInfos.SingleOrDefault(f => f.Name.StartsWith("DS0000000_X01"));
            ImportAthlets(participants);

            var team = fileInfos.SingleOrDefault(f => f.Name.StartsWith("DS0000000_X30"));
            ImportTeams(team);
        }

        private void ImportAthlets(System.IO.FileInfo fileinfo)
        {
            _participants = new XmlDocument();
            _participants.Load(fileinfo.FullName);
            /* -<OVRCRS_Athlete_Lists>-<OVRCRS_Athlete_List Gender="W" Registration_Status="ACCRED" Organization_ID="FIN" Nationality="FIN" Height="" Weight="" Country_Of_Residence="" Country_Of_Birth="FIN" Reporting_Name="S. MERJE" Initial_Name="S. M" TV_Long_Name="M. STUF" TV_Short_Name="M. STUF" WNPA_Family_Name="STUF" WNPA_Given_Name="MERJE" DOB="19830608" Given_Name="MERJE" Family_Name="STUF" Modification_Indicator="N" Registration_Number="952">-<Athlete_Discipline Discipline_ID="DS"><Athlete_Event Event="003" Gender="X"/></Athlete_Discipline></OVRCRS_Athlete_List> */

        }

        private void ImportTeams(System.IO.FileInfo fileinfo)
        {
            System.Xml.XmlDocument doc = new XmlDocument();
            doc.Load(fileinfo.FullName);
            
            /* -<OVRCRS_Team_List Event="002" Gender="X" Delete="N" Team_Name="Pavcek, B./ Sercer, B." NOC="SLO" Team_ID="5048"><Athlete_List ID="3371"/><Athlete_List ID="3513"/> */
            var list = doc.GetElementsByTagName("OVRCRS_Team_List");
            foreach (var nodeObject in list)
            {
                var node = nodeObject as XmlNode;

                if (!node.HasChildNodes)
                {
                    continue;
                }

                var teamId = node.Attributes["Team_Id"].Value;
                var couple = _context.Couples.SingleOrDefault(c => c.Team_Id == teamId);
                if (couple == null)
                {
                    couple = new DataModel.Models.Couple()
                        {
                            Country = node.Attributes["NOC"].Value,
                            Team_Id = node.Attributes["Team_Id"].Value
                        };

                    _context.Couples.Add(couple);
                }
                // Search the participants:
                var athlet = (XmlNode) node.ChildNodes[0];
                var id = athlet.Attributes["ID"].Value;
                var a = _participants.SelectSingleNode(String.Format("//OVRCRS_Athlete_List[@Registration_Number='{0}']", id));
                var gender = a.Attributes["Gender_ID"].Value;
                if(gender == "M")
                    UpdateMan(couple, a);
                else
                    UpdateWoman(couple, a);

                athlet = (XmlNode) node.ChildNodes[1];
                id = athlet.Attributes["ID"].Value;
                a = _participants.SelectSingleNode(String.Format("//OVRCRS_Athlete_List[@Registration_Number='{0}']", id));
                if(gender == "M")
                    UpdateWoman(couple, a); 
                else
                    UpdateMan(couple, a);

                // Can we find the competition? If so, add the couple
                var eventid = node.Attributes["Event"].Value;
                var competition =
                    _context.Competitions.SingleOrDefault(c => c.ExternalId == eventid);
                if (competition != null)
                {
                    var participant = competition.Participants.SingleOrDefault(p => p.Couple.Team_Id == couple.Team_Id);
                    if (participant == null)
                    {
                        participant = new DataModel.Models.Participant
                            {
                                Competition = competition,
                                Couple = couple,
                                Number = 0
                            };
                        _context.Participants.Add(participant);
                    }
                }
                _context.SaveChanges();
            }
        }

        private void UpdateManAtos(Couple couple, XmlNode a)
        {
            couple.Athlete_Id_Man = a.Attributes["Code"].Value;
            couple.FirstMan = a.Attributes["GivenName"].Value;
            couple.LastMan = a.Attributes["FamilyName"].Value;
            couple.NOC_Code_Man = a.Attributes["Nationality"].Value;
            couple.Long_TV_Name_Man = a.Attributes["TVName"].Value;
            couple.Short_TV_Name_Man = a.Attributes["TVInitialName"].Value;
        }

        private void UpdateMan(Couple couple, XmlNode a)
        {
            couple.Athlete_Id_Man = a.Attributes["Registration_Number"].Value;
            couple.FirstMan = a.Attributes["WNPA_Given_Name"].Value;
            couple.LastMan = a.Attributes["WNPA_Family_Name"].Value;
            couple.NOC_Code_Man = a.Attributes["Nationality"].Value;
            couple.Long_TV_Name_Man = a.Attributes["TV_Long_Name"].Value;
            couple.Short_TV_Name_Man = a.Attributes["TV_Short_Name"].Value;
        }

        private void UpdateWoman(Couple couple, XmlNode a)
        {
            couple.Athlete_Id_Woman = a.Attributes["Registration_Number"].Value;
            couple.FirstWoman = a.Attributes["WNPA_Given_Name"].Value;
            couple.LastWoman = a.Attributes["WNPA_Family_Name"].Value;
            couple.NOC_Code_Woman = a.Attributes["Nationality"].Value;
            couple.Long_TV_Name_Woman = a.Attributes["TV_Long_Name"].Value;
            couple.Short_TV_Name_Woman = a.Attributes["TV_Short_Name"].Value;
        }

        private void UpdateWomanAtos(Couple couple, XmlNode a)
        {
            couple.Athlete_Id_Woman = a.Attributes["Code"].Value;
            couple.FirstWoman = a.Attributes["GivenName"].Value;
            couple.LastWoman = a.Attributes["FamilyName"].Value;
            couple.NOC_Code_Woman = a.Attributes["Nationality"].Value;
            couple.Long_TV_Name_Woman = a.Attributes["TVName"].Value;
            couple.Short_TV_Name_Woman = a.Attributes["TVInitialName"].Value;
        }
    }
}
