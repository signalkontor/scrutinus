﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using DataModel;
using DataModel.Models;
using Helpers.NewJudgingSystem;

namespace WorldGamesInterface.Helper
{
    class ExportHelper
    {
        private const bool WriteHeader = true;

        public void WriteFinalRank(StreamWriter writer, Competition competition)
        {
            if(WriteHeader)
                writer.WriteLine("Team_ID;Medal;Place_From;Place_To;Rank;Score");
         
            foreach (var participant in competition.Participants.OrderBy(p => p.Couple.Team_Id))
            {
                var medal = participant.PlaceFrom == 1
                                ? "Gold"
                                : participant.PlaceFrom == 2 ? "Silver" : participant.PlaceFrom == 3 ? "Bronze" : "\"\"";

                writer.WriteLine("{0};{1};{2};{3};{4};{5}", participant.Couple.Team_Id, 
                    medal, 
                    participant.PlaceFrom, 
                    participant.PlaceTo, 
                    participant.PlaceFrom,
                    participant.Points.ToString("#0.00").Replace(",", "."));
            }

        }

        public void WriteQualifiedRedance(StreamWriter writer, Round round)
        {
            if (WriteHeader)
                writer.WriteLine("Team_ID;Bib_No;Qualified;Redance;Place_From;Place_To;IRM");

            foreach (var participant in round.Qualifieds.OrderBy(q => q.Participant.Number))
            {
                if (round.State == DataModel.CompetitionState.Startlist)
                {
                    writer.WriteLine("{0};{1};{2};{3};{4};{5};{6}",
                        participant.Participant.Couple.Team_Id,
                        participant.Participant.Number,
                        "0",
                        "0",
                        "\"\"",
                        "\"\"",
                        participant.Participant.State == CoupleState.Missing ? "DNS" : "\"\"");
                }
                else
                {
                    writer.WriteLine("{0};{1};{2};{3};{4};{5};{6}",
                         participant.Participant.Couple.Team_Id,
                         participant.Participant.Number,
                         participant.QualifiedNextRound ? "\"Q\"" : "0",
                         participant.QualifiedNextRound ? "0" : "\"R\"",
                         "\"\"",
                         "\"\"",
                         participant.Participant.State == CoupleState.Missing || participant.Participant.State == CoupleState.Excused 
                                ? "DNS" : "\"\""); 
                }

            }
        }

        public void WriteQualifiedWithPlaces(StreamWriter writer, Round round)
        {
            WriteQualifiedWithPlaces(writer, round, "\"\"");
        }

        public void WriteQualifiedWithPlaces(StreamWriter writer, Round round, string NotQualifiedSign)
        {
            if (WriteHeader)
            {
                writer.WriteLine("Team_ID;Bib_No;Qualified;Redance;Place_From;Place_To;IRM;Score");
            }
                

            foreach (var participant in round.Qualifieds.OrderBy(q => q.Participant.Number))
            {
                writer.WriteLine("{0};{1};{2};{3};{4};{5};{6};{7}", participant.Participant.Couple.Team_Id,
                    participant.Participant.Number,
                    participant.QualifiedNextRound ? "Q" : NotQualifiedSign,
                    "\"\"",
                    participant.Participant.PlaceFrom.HasValue && !participant.QualifiedNextRound ? participant.Participant.PlaceFrom.Value.ToString() : "\"\"", 
                    participant.Participant.PlaceTo.HasValue   && !participant.QualifiedNextRound ? participant.Participant.PlaceTo.Value.ToString() : "\"\"", 
                    participant.Participant.State == CoupleState.Missing ? "DNS": "\"\"",
                    participant.Participant.Points.ToString("##0.00").Replace(",", "."));
            }
        }

        public void WriteMarkingOfDance(StreamWriter writer, Round round, int danceId, ScrutinusContext context)
        {
            if(WriteHeader)
                writer.WriteLine("Team_ID;Bib_No;Place;Component_A;Component_B;Component_C;Component_D;Total_Points;Order");

            System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

            foreach (var qualified in round.Qualifieds.OrderBy(p => p.Participant.Number))
            {
                var sumAll = 0d;

                var currentRoundId = round.Id;

                var marks = context.Markings.Where(m => m.Round.Id == currentRoundId && m.Participant.Id == qualified.Participant.Id && m.Dance.Id == danceId).ToList();
                var danceInRound = context.DancesInRounds.First(d => d.Dance.Id == danceId && d.Round.Id == round.Id);

                var pointCalculator = new PointCalculatorJs3(1.3);

                var area1 = pointCalculator.CalculateComponent(1, marks, qualified.Participant, danceInRound);
                var area2 = pointCalculator.CalculateComponent(2, marks, qualified.Participant, danceInRound);
                var area3 = pointCalculator.CalculateComponent(3, marks, qualified.Participant, danceInRound);
                var area4 = pointCalculator.CalculateComponent(4, marks, qualified.Participant, danceInRound);

                if (area3 == 0 && area4 == 0)
                {
                    area1 *= 2;
                    area2 *= 2;
                }

                sumAll += area1 + area2 + area3 + area4;

                writer.WriteLine("{0};{7};{1};{2};{3};{4};{5};{6:00.00}", 
                    qualified.Participant.Couple.Team_Id, 
                    qualified.PlaceFrom, 
                    area1 > 0 ? area1.ToString("0.00") : "", 
                    area2 > 0 ? area2.ToString("0.00") : "", 
                    area3 > 0 ? area3.ToString("0.00") : "", 
                    area4 > 0 ? area4.ToString("0.00") : "", 
                    sumAll, 
                    qualified.Participant.Number,
                    "1");
            }
        }

        
        public void WriteMarkingOfDance_V1(StreamWriter writer, Round round, int danceId, ScrutinusContext context)
        {
            if (WriteHeader)
                writer.WriteLine("Team_ID;Bib_No;Place;Component_A;Component_B;Component_C;Component_D;Total_Points");

            System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

            foreach (var qualified in round.Qualifieds.OrderBy(p => p.Participant.Number))
            {
                var sumAll = 0d;

                var currentRoundId = round.Id;

                var qualified1 = qualified;
                var marks = context.Markings.Where(m => m.Round.Id == currentRoundId && m.Participant.Id == qualified1.Participant.Id && m.Dance.Id == danceId);
                var marksCount = marks.Count();
                var area1 = (marks.Sum(m => m.MarkA) - marks.Max(m => m.MarkA) - marks.Min(m => m.MarkA))/
                                (marks.Count() - 2);
                var area2 = (marks.Sum(m => m.MarkB) - marks.Max(m => m.MarkB) - marks.Min(m => m.MarkB)) /
                                (marks.Count() - 2);
                var area3 = (marks.Sum(m => m.MarkC) - marks.Max(m => m.MarkC) - marks.Min(m => m.MarkC)) /
                                 (marks.Count() - 2);
                var area4 = (marks.Sum(m => m.MarkD) - marks.Max(m => m.MarkD) - marks.Min(m => m.MarkD)) /
                                (marks.Count() - 2);

                sumAll = area1 + area2 + area3 + area4;

                writer.WriteLine("{0};{7};{1};{2:0.00};{3:0.00};{4:0.00};{5:0.00};{6:00.00}", qualified.Participant.Couple.Team_Id,
                    qualified.PlaceFrom,
                    area1, area2, area3, area4, sumAll,
                    qualified.Participant.Number);
            }
        }
    }
}
