import { Component, OnInit } from '@angular/core';
import { Competition } from 'src/app/model/competition';
import { MyHeatsService } from 'src/app/services/myHeats.service';

@Component({
  selector: 'app-competition-list',
  templateUrl: './competition-list.component.html',
  styleUrls: ['./competition-list.component.css']
})
export class CompetitionListComponent implements OnInit {

  competitions: Competition[] = [];

  constructor(private myHeatsService: MyHeatsService) { }

  isLoading: boolean = false;

  ngOnInit() {
    this.reload();
  }

  reload() {
    this.isLoading = true;
    this.myHeatsService.getCompetitions().subscribe((comps: Competition[]) => {
      this.isLoading = false;
      this.competitions = comps;
    }, err => {
      this.isLoading = false;
    });
  }
}
