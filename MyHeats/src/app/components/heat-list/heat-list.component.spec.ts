/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { HeatListComponent } from './heat-list.component';

describe('HeatListComponent', () => {
  let component: HeatListComponent;
  let fixture: ComponentFixture<HeatListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeatListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeatListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
