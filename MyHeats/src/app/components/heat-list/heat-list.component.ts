import { Component, OnInit } from '@angular/core';
import { Heat } from 'src/app/model/heat';
import { MyHeatsService } from 'src/app/services/myHeats.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-heat-list',
  templateUrl: './heat-list.component.html',
  styleUrls: ['./heat-list.component.css']
})
export class HeatListComponent implements OnInit {

  heats: Heat[] = [];
  roundId: number;
  isLoading = false;
  error = '';

  constructor(public myHeatsService: MyHeatsService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.isLoading = true;
    this.roundId = Number(this.route.snapshot.paramMap.get('roundId'));
    this.myHeatsService.lastRoundId = this.roundId;
   
    this.myHeatsService.getHeats(this.roundId).subscribe((res: Heat[]) => {
      if(res) {
        this.isLoading = false;
        this.heats = res;
      }
    }, err => {
      this.isLoading = false;
      this.error = 'Fehler beim Laden der Auslosung, probieren Sie es gleich noch einmal';
    });

  }

}
