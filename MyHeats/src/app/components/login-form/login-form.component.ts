import { Component, OnInit } from '@angular/core';
import { MyHeatsService } from 'src/app/services/myHeats.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {

  hasError: boolean = false;

  error: string = '';

  dtvId: string = '';

  constructor(private myHeatsService: MyHeatsService, private router: Router, public translateService: TranslateService) { }

  ngOnInit() {
  
  }

  login() {
    if (this.dtvId == null || this.dtvId === '') {
      this.hasError = true;
      this.error = 'Die DTV ID darf nicht leer sein. Tragen Sie Ihre eigene oder die des/der Partner/in ein';
      return;
    }

    this.hasError = false;
    this.error = '';

    this.myHeatsService.dtvId = this.dtvId;
    this.myHeatsService.login().subscribe(res => {
      if (res) {
        this.router.navigate(['competitions']);
      }
    }, err => {
      this.hasError = true;
      console.log(err);
      this.error = 'Sie konnten leider nicht angemeldet werden ' + err;
    });
  }

  changeValue($event: any) {
    console.log($event);
    this.dtvId = $event.target.value;
  }
}
