import { Component, OnInit } from '@angular/core';
import { MyHeatsService } from 'src/app/services/myHeats.service';
import { Round } from 'src/app/model/round';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-round-list',
  templateUrl: './round-list.component.html',
  styleUrls: ['./round-list.component.css']
})
export class RoundListComponent implements OnInit {

  rounds: Round[] = [];

  isLoading = false;

  error = '';
  constructor(public myHeatsService: MyHeatsService, private route: ActivatedRoute) { }


  ngOnInit() {
    this.reload();
  }

  reload() {
    const competitionId = Number(this.route.snapshot.paramMap.get('competitionId'));
    this.myHeatsService.lastCompetitionId = competitionId;

    this.isLoading = true;
    this.myHeatsService.getRounds(competitionId).subscribe((res: Round[]) => {
      if (res) {
        this.isLoading = false;
        this.rounds = res;
    }},
    err => {
      this.isLoading = false;
      this.error = 'Es ist ein Fehler aufgetreten, bitte probieren Sie es gleich noch einmal'
    });
  }
}
