import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { MatInputModule, MatFormFieldModule, MatButtonModule, MatCardModule, MatListModule, MatProgressSpinnerModule, MatIconModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MyHeatsService } from './services/myHeats.service';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { CompetitionListComponent } from './components/competition-list/competition-list.component';
import { RoundListComponent } from './components/round-list/round-list.component';
import { HeatListComponent } from './components/heat-list/heat-list.component';
import { AuthGuardService } from './services/AuthGuard.service';
import { NgxFlagIconCssModule } from 'ngx-flag-icon-css';
import {TranslateModule, TranslateLoader } from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

const routes: Routes = [
  {
    path: '',
    component: LoginFormComponent
  },
  {
    path: 'competitions',
    component: CompetitionListComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'rounds/:competitionId',
    component: RoundListComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'heats/:roundId',
    component: HeatListComponent,
    canActivate: [AuthGuardService]
  }
];

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    LoginFormComponent,
    CompetitionListComponent,
    RoundListComponent,
    HeatListComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    MatInputModule,
    MatFormFieldModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    MatListModule,
    MatProgressSpinnerModule,
    NgxFlagIconCssModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [
    MyHeatsService,
    HttpClient
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
