import { Competition } from "./competition";

export class Round {
    id: number;

    name: string = null;

    competition: Competition = null;
}
