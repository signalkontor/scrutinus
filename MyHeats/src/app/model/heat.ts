import { Round } from "./round";

export class Heat {
    name: string = null;

    heatIndex: number;

    round: Round = null;

    dance: string = null;
}
