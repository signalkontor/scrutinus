export class Competition {
    id: number;

    name: string = null;

    start: Date = null;

    floor: string = null;

    placeInCompetition: string = null;
}
