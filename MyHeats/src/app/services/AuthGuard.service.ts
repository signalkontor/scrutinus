import { Injectable } from '@angular/core';
import { MyHeatsService } from './myHeats.service';
import { CanActivate, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {



constructor(private myHeatsService: MyHeatsService, private router: Router) { }

canActivate(): boolean {
  if (this.myHeatsService.couple == null || this.myHeatsService.couple == undefined) {
    this.router.navigate(['/']);
    return false;
  }
  return true;
}
}
