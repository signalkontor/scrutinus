import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Couple } from '../model/couple';
import { Competition } from '../model/competition';
import { Round } from '../model/round';
import { Heat } from'../model/heat';
import { map } from 'rxjs/operators';
import { CompetitionListComponent } from '../components/competition-list/competition-list.component';
import { of } from 'rxjs';
import { RoundListComponent } from '../components/round-list/round-list.component';
import { headersToString } from 'selenium-webdriver/http';

@Injectable({
  providedIn: 'root'
})
export class MyHeatsService {

  public dtvId: string;

  public couple: Couple = null;

  lastCompetitionId: number = null;

  lastRoundId: number = null;

constructor(private httpClient: HttpClient) { 
  
  
}

  login(): Observable<Couple> {

    const couple = new Couple();
    couple.club = 'TC Hanseatic Lübeck';
    couple.name = 'Olav Gröhn';
    couple.name = 'Jola Borchert';

    // return of(couple);

    return this.httpClient.post<Couple>(`${environment.baseUrl}/login/${this.dtvId}`, null)
              .pipe(
                map((r: Couple) => {
                  console.log('Returned');
                this.couple = r;
                return r;
              }));
  }

  getCompetitions(): Observable<Competition[]> {

    // return of(this.competitions);

    return this.httpClient.get<Competition[]>(`${environment.baseUrl}/competitions/${this.couple.id}`);
  }

  getRounds(competitionId: number): Observable<Round[]> {

    // return of(this.rounds);

    return this.httpClient.get<Round[]>(`${environment.baseUrl}/competitions/${competitionId}/rounds/${this.couple.id}`);
  }

  getHeats(roundId: number): Observable<Heat[]> {

    // return of(this.heats);
    return this.httpClient.get<Heat[]>(`${environment.baseUrl}/rounds/${roundId}/heats/${this.couple.id}`);
  }

}
