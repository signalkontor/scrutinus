﻿// // TPS.net TPS8 TPSModel
// // TranslationElement.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

namespace TPSModel
{

    public class TranslationElement
    {
        private readonly string _code;
        private readonly string _english;
        private readonly string _german;

        public TranslationElement(string id, string german, string english, string code)
        {
            this.Id = id;
            this._german = german;
            this._english = english;
            this._code = code;
        }

        public string Code { get { return this._code; } }

        public string Id { get; set; }

        public string Text
        {
            get
            {
                switch (Model.Instance.Language)
                {
                    case Model.Languages.English:
                        return this._english;
                    case Model.Languages.German:
                        return this._german;
                }

                return this._english;
            }
        }

        public string German
        {
            get { return this._german; }
        }
    }

}
