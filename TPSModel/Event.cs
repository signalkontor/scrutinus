﻿// // TPS.net TPS8 TPSModel
// // Event.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;

namespace TPSModel
{
    public class Event
    {
        public int WdsfId { get; set; }
        public string Place { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateUntil { get; set; }
    }
}
