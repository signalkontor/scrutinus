﻿// // TPS.net TPS8 TPSModel
// // Person.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

namespace TPSModel
{
    /// <summary>
    /// Represents the Person Object from WDSF
    /// </summary>
    public class Person
    {
        public string Min { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Country { get; set; }
        public string ActiveCoupleId { get; set; }
    }
}
