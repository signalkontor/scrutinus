﻿// // TPS.net TPS8 TPSModel
// // Couple.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;

namespace TPSModel
{
    public class Couple
    {
        public Couple(string data)
        {
            var d = Model.TPSSplitter(data);
            this.Id = Int32.Parse(d[0]);
            this.FirstNameHe = d[2];
            this.LastNameHe = d[3];
            this.FirstNameShe = d[4];
            this.LastNameShe = d[5];
            this.Country = d[7];
            this.MinID = d.Length > 11 ? d[11] : "";
            this.Competitions = new List<Competition>();

            if (d.Length > 11 && !d[11].Contains("_") && !d[11].Contains("-"))
            {
                // Check, if the string is numeric:
                var id = 0;
                if (!int.TryParse(d[11], out id))
                {
                    this.MinID = "";    
                }
            }
        }

        public Couple()
        {
            this.Competitions = new List<Competition>();
        }

        public int Id { get; set; }

        public string FirstNameHe { get; set; }
        public string LastNameHe { get; set; }
        public string MINMan { get; set; }

        public string FirstNameShe { get; set; }
        public string LastNameShe { get; set; }
        public string MINWoman { get; set; }
        public string Country { get; set; }

        public List<Competition> Competitions { get; set; }

        public string MinID
        {
            get
            {
                return string.IsNullOrEmpty(this.MINWoman) ? this.MINMan : this.MINMan + "_" + this.MINWoman;
            }
            
            set
            {
                if (value.Contains("_"))
                {
                    var data = value.Split('_');
                    this.MINMan = data[0];
                    this.MINWoman = data[1];
                }
                else
                {
                    this.MINMan = value;
                    this.MINWoman = "";
                }
            }
        }

        public string WDSFID { get; set; }

        public string NiceName
        {
            get { return this.FirstNameHe + " " + this.LastNameHe + " - " + this.FirstNameShe + " " + this.LastNameShe; }
        }

        public Couple Clone()
        {
            var c = new Couple();
            c.FirstNameHe = this.FirstNameHe;
            c.LastNameHe = this.LastNameHe;
            c.FirstNameShe = this.FirstNameShe;
            c.LastNameShe = this.LastNameShe;
            c.Country = this.Country;
            c.Id = this.Id;

            return c;
        }
    }
}
