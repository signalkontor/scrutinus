﻿// // TPS.net TPS8 TPSModel
// // OfficalWithRole.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

namespace TPSModel
{
    public class OfficalWithRole
    {
        public Offical Offical { get; set; }
        public string Role { get; set; }
        public int Id { get; set; }
        public int EventId { get; set; }
    }
}
