﻿// // TPS.net TPS8 TPSModel
// // Competitor.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using TPSModel.Enumerations;

namespace TPSModel
{
    public class Competitor : INotifyPropertyChanged
    {
        private CoupleState _state;
        public int Star { get; set; }

        public CoupleState State
        {
            get { return this._state; }
            set
            {
                if (this._state != value)
                {
                    this._state = value;
                    this.RaiseEvent("State");
                }
            }
        }

        public Couple Couple { get; set; }

        public Competition Competition { get; set; }

        public object WDSFPartisipant { get; set; }

        public int Number { get; set; }
        public int PlaceFrom { get; set; }
        public int PlaceTo { get; set; }
        public string LastRound { get; set; }
        public double TotalMarking { get; set; }
        public string[] RawData { get; set; }
        // Marks by Round and per Judge
        public Dictionary<string, List<Mark>> Results { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public void SetMarks(string data, string round)
        {
            if (this.Results == null)
            {
                this.Results = new Dictionary<string, List<Mark>>();
            }
            var d = Model.TPSSplitter(data);
            var noJudges = Int32.Parse(d[2]);
            for (var i = 0; i < noJudges; i++)
            {
                var marks = Model.TPSSplitter(d[i * 3 + 4]);

                if (marks.Length == 1 && (marks[0] == "" || marks[0] == "0"))
                {
                    continue;
                }

                var judgeSign = d[i * 3 + 3];
                var judge = Model.Instance.Officals.SingleOrDefault(o => o.Sign == judgeSign);

                List<Mark> marking;
                if (this.Results.ContainsKey(round))
                {
                    marking = this.Results[round];
                }
                else
                {
                    marking = new List<Mark>();
                    this.Results.Add(round, marking);
                }

                for (var j = 0; j < this.Competition.Dances.Count; j++)
                {
                    if (marks.Length > j && marks[j] == "X")
                    {
                        marking.Add(new Mark { Dance = this.Competition.Dances[j], Judge = judge, Value = 1 });
                    }
                    else
                    {
                        var res = 0;
                        if (marks.Length <= j)
                        {
                            continue;
                        }

                        if (Int32.TryParse(marks[j], out res) && round == "F")
                        {
                            marking.Add(new Mark { Dance = this.Competition.Dances[j], Judge = judge, Value = res });
                        }
                    }
                }
            }
        }

        protected void RaiseEvent(string property)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }
    }
}
