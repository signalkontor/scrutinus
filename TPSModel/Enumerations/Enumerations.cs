﻿// // TPS.net TPS8 TPSModel
// // Enumerations.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

namespace TPSModel.Enumerations
{
    public enum CoupleState
    {
        Dancing = 1,
        Excused = 2,
        Missing = 3
    }

    public enum RoundType
    {
        Qualification = 1,
        Redance = 2,
        Final = 3
    }
}
