﻿// // TPS.net TPS8 TPSModel
// // Competition.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using TPSModel.Enumerations;

namespace TPSModel
{
    public class Competition : INotifyPropertyChanged
    {
        private readonly string ageGroup;
        private readonly string dataPath;

        private readonly string section;


        private readonly string startClass;

        private bool transmitToWdsf;

        private readonly string type;

        public Competition(int id)
        {
            this.Id = id;
        }

        public Competition(int id, string[] data, string dataPath)
        {
            this.Id = id;
            this.Title = string.IsNullOrEmpty(data[1]) ? data[0] : data[1];

            if (data[20] != "")
            {
                this.StartTime = DateTime.Parse(data[20]);
            }
            else
            {
                this.StartTime = DateTime.Now;
            }
            this.WDSF_Id = data[24];
            this.type = data[2];
            this.ageGroup = data[5];
            this.startClass = data[6];
            this.section = data[3];
            this.dataPath = dataPath;
            var stars = 0;
            Int32.TryParse(data[9], out stars);
            this.Stars = stars;

            if (String.IsNullOrWhiteSpace(this.Title))
            {
                this.Title = this.Type + " " + this.AgeGroup + " " + this.Section;
            }
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public string State { get; set; }
        public string WDSF_Id { get; set; }

        public bool CheckinAllowed { get; set; }
        public bool TPSAllowed { get; set; }

        public Event Event { get; set; }

        public string Type
        {
            get
            {
                if (Model.Instance.CompetitionTypes.ContainsKey(this.type))
                {
                    return Model.Instance.CompetitionTypes[this.type].Text;
                }
                else
                {
                    return "unknown type";
                }
            }
        }

        public string AgeGroup
        {
            get
            {
                if (Model.Instance.AgeGroups.ContainsKey(this.ageGroup))
                {
                    return Model.Instance.AgeGroups[this.ageGroup].Text;
                }
                else
                {
                    return "unknown Age Group";
                }
            }
        }

        public string ExportAgeGroupCode 
        {
            get
            {
                /* Code Übersetzung analog zu Code aus dem TPS */
                switch (this.ageGroup)
                {
                    case "A":
                        return "KIN I";
                    case "B":
                        return "KIN II";
                    case "C":
                        return "KIN I/II";
                    case "D":
                        return "JUN I";
                    case "E":
                        return "JUN II";
                    case "G":
                        return "JUG";
                    case "I":
                        return "HGR";
                    case "M":
                        return "HGR II";
                    case "N":
                        return "SEN I";
                    case "O":
                        return "SEN II";
                    case "P":
                        return "SEN III";
                    case "S":
                        return "SEN IV";
                    case "Q":
                        return "SEN";
                }

                return "-";
            }
        }

        public string AgeGroupGerman
        {
            get
            {
                if (Model.Instance.AgeGroups.ContainsKey(this.ageGroup))
                {
                    return Model.Instance.AgeGroups[this.ageGroup].Code;
                }
                else
                {
                    return "-";
                }
            }
        }

        public string StartClass
        {
            get
            {
                if (Model.Instance.StartClasses.ContainsKey(this.startClass))
                {
                    return Model.Instance.StartClasses[this.startClass].Text;
                }
                else
                {
                    return "unknown start class";
                }
            }
        }

        public string StartClassGerman
        {
            get
            {
                if (Model.Instance.StartClasses.ContainsKey(this.startClass))
                {
                    return Model.Instance.StartClasses[this.startClass].Code;
                }
                else
                {
                    return "-";
                }
            }
        }

        public string Section
        {
            get
            {
                if (Model.Instance.Sections.ContainsKey(this.section))
                {
                    return Model.Instance.Sections[this.section].Text;
                }
                else
                {
                    return "unknown section";
                }
            }
        }


        public int Stars { get; set; }
        public DateTime StartTime { get; set; }
        public List<Round> Rounds { get; set; }
        public List<OfficalWithRole> Judges { get; set; }
        public List<OfficalWithRole> Officals { get; set; }
        public List<string> Dances { get; set; }
        public List<Competitor> Couples { get; set; }

        public double WeightA { get; set; }
        public double WeightB { get; set; }
        public double WeightC { get; set; }
        public double WeightD { get; set; }

        public bool IsFormation { get; set; }

        public bool TransmitToWDSF
        {
            get { return this.transmitToWdsf; }
            set
            {
                if (this.transmitToWdsf != value)
                {
                    this.transmitToWdsf = value;
                    this.RaiseEvent("TransmitToWDSF");
                }
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        public void LoadData()
        {
            this.LoadDances();
            this.LoadOfficials();
            this.Rounds = new List<Round>();
            this.LoadState();
            this.LoadStartlist();
            this.LoadMarkings();
            this.LoadAllowedState();
            // Try to load a First Round
            var filename = String.Format(this.dataPath + "\\{0:0000}RlV1.dat", this.Id);
            this.LoadRound(filename, "1. Round", RoundType.Qualification, "1");
            filename = String.Format(this.dataPath + "\\{0:0000}RlV2.dat", this.Id);
            if (File.Exists(filename))
            {
                this.LoadRound(filename, "Redance", RoundType.Redance, "R");
            }

            for (var i = 1; i < 200; i++)
            {
                filename = String.Format(this.dataPath + "\\{0:0000}RlZ{1}.dat", this.Id, i);
                if (!this.LoadRound(filename, i + 1 + ". Round", RoundType.Qualification, (i + 1).ToString()))
                {
                    break;
                }
            }
            // try to load a final
            if (this.State == "Finale" || this.State == "Finished" || this.State == "Final")
            {
                this.LoadFinale();
            }
        }

        public void LoadAllowedState()
        {
            var filename = String.Format(this.dataPath + "\\{0:0000}TdCtrl.dat", this.Id);
            if (!File.Exists(filename))
            {
                return;
            }

            var stream = new StreamReader(filename);
            var data = Model.TPSSplitter(stream.ReadLine());
            this.CheckinAllowed = data[8] == ".T.";
            this.TPSAllowed = data[10] == ".T.";
            stream.Close();
        }

        private void LoadStartlist()
        {
            this.Couples = new List<Competitor>();
            var filename = String.Format(this.dataPath + "\\{0:0000}Sl.dat", this.Id);
            if (!File.Exists(filename))
            {
                return;
            }
            var reader = new StreamReader(filename, Encoding.UTF7);
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                var data = Model.TPSSplitter(line);
                var cid = Int32.Parse(data[0]);
                var couple = Model.Instance.Couples.SingleOrDefault(c => c.Id == cid);

                if(couple == null)
                {
                    continue;
                }

                data[13] = data[13].EndsWith(";") ? data[13].Substring(0, data[13].Length - 1) : data[13];
                data[14] = data[14].EndsWith(";") ? data[14].Substring(0, data[14].Length - 1) : data[14];
                var competitor = new Competitor()
                {
                    Couple = couple,
                    Number = Int32.Parse(data[1]),
                    Star = Int32.Parse(data[7]),
                    Competition = this,
                    PlaceFrom = data[13] != "" ? Int32.Parse(data[13]) : 0,
                    PlaceTo = data[14] != "" ? Int32.Parse(data[14]) : 0,
                    LastRound = data[9] + data[10],
                    RawData = data
                };

                couple.Competitions.Add(this);

                var rnd = competitor.LastRound.Split(';');
                if (competitor.LastRound.StartsWith(" ") || rnd.Length < 2)
                {
                    competitor.LastRound = ""; // Still in Competition
                }
                else
                {
                    if (rnd[0] == "V")
                    {
                        competitor.LastRound = "1"; // 1: like WDSF API
                    }
                    else
                    {
                        if (rnd[0] == "E")
                        {
                            competitor.LastRound = "F";
                        }
                        else
                        {
                            var irnd = 0;
                            if (!Int32.TryParse(rnd[1], out irnd))
                            {
                                competitor.LastRound = ""; // Still in Compl?
                            }
                            else
                            {
                                competitor.LastRound = (irnd + 1).ToString();
                            }
                        }
                    }
                }

                switch (data[6])
                {
                    case "-":
                        competitor.State = CoupleState.Dancing;
                        break;
                    case "E": // Excused
                        competitor.State = CoupleState.Excused;
                        break;
                    case "F": // Missing
                        competitor.State = CoupleState.Missing;
                        break;
                }

                this.Couples.Add(competitor);
            }
            reader.Close();
        }

        public void SaveStartList()
        {
            var filename = String.Format(this.dataPath + "\\{0:0000}Sl.dat", this.Id);
            var writer = new StreamWriter(filename, false, Encoding.ASCII);
            foreach (var competitor in this.Couples.OrderBy(c => c.Number))
            {
                for (var i = 0; i < competitor.RawData.Count(); i++)
                {
                    if (i != 6)
                    {
                        var towrite = String.IsNullOrWhiteSpace(competitor.RawData[i])
                                          ? "\"\";"
                                          : competitor.RawData[i] + ";"; 
                        writer.Write(towrite);
                    }
                    else
                    {
                        switch (competitor.State)
                        {
                            case CoupleState.Dancing: writer.Write("-;");
                                break;
                            case CoupleState.Excused: writer.Write("E;");
                                break;
                            case CoupleState.Missing:writer.Write("F;");
                                break;
                        }
                    }
                }
                writer.WriteLine("");
            }
            writer.Close();
            /// Todo: Prüfen, ob Datumsformat kompatibel ist zu TPS ...
            var stream = new StreamWriter(String.Format(this.dataPath + "\\Touch.TuStl.{0:0000}.{1}.dat", this.Id, Environment.MachineName),false, Encoding.ASCII);
            stream.WriteLine("\"{0}\";{1}", DateTime.Now.ToString(), Environment.MachineName);
            stream.Close();
        }

        private void LoadFinale()
        {
            var filename = String.Format(this.dataPath + "\\{0:0000}Sl.dat", this.Id);
            var reader = new StreamReader(filename, Encoding.UTF7);
            var round = new Round();
            round.Title = "Final";
            round.Type = RoundType.Final;
            round.Qualified = new List<Competitor>();
            round.Id = Guid.NewGuid();
            round.Competition = this;
            round.WDSF_Name = "F";

            while (!reader.EndOfStream)
            {
                var data = Model.TPSSplitter(reader.ReadLine());
                if ((data[9].StartsWith("E") || data[9].Equals("") || data[9] == " ;") && data[6] == "-") // still in competition
                {
                    var cid = Int32.Parse(data[0]);
                    round.Qualified.Add(this.Couples.SingleOrDefault(c => c.Couple.Id == cid));
                }
            }

            reader.Close();

            this.Rounds.Add(round);
        }

        private void LoadState()
        {
            var filename = String.Format(this.dataPath + "\\{0:0000}Ckp.dat", this.Id);
            if (!File.Exists(filename))
            {
                this.State = "";
                return;
            }
            var reader = new StreamReader(filename, Encoding.UTF7);
            var line = "";
            var last = "";
            while (!reader.EndOfStream)
            {
                last = line;
                line = reader.ReadLine();
            }
            reader.Close();
            // Line now contains the last line of the file and this is the current state
            if (line == "")
            {
                line = last;
            }

            var data = Model.TPSSplitter(line);
            if (data.Length < 2)
            {
                this.State = "";
                return;
            }
            switch (data[1])
            {
                case "V":
                    this.State = "1. Round";
                    break;
                case "Z":
                    var i = Int32.Parse(data[2]);
                    this.State = i + 1 + ". Round";
                    break;
                case "E":
                    this.State = "Final";
                    break;
                case "#":
                    this.State = "Finished";
                    break;

            }

        }

        private void LoadDances()
        {
            this.Dances = new List<string>();
            var filename = String.Format(this.dataPath + "\\{0:0000}Tanz.dat", this.Id);
            if (!File.Exists(filename))
            {
                return;
            }
            var reader = new StreamReader(filename, Encoding.UTF7);
            while (!reader.EndOfStream)
            {
                var data = Model.TPSSplitter(reader.ReadLine());
                this.Dances.Add(data[0]);
            }
            reader.Close();
        }

        private bool LoadRound(string filename, string roundname, RoundType type, string WDSF_Name)
        {
            if (!File.Exists(filename))
            {
                return false;
            }

            var round = new Round();
            round.Type = type;
            round.Title = roundname;
            round.Couples = new Dictionary<string, List<List<Competitor>>>();
            round.Dances = new List<string>();
            round.Id = Guid.NewGuid();
            round.Qualified = new List<Competitor>();
            round.Competition = this;
            round.WDSF_Name = WDSF_Name;
            this.Rounds.Add(round);
            // We copy the dances of the competition
            foreach (var dance in this.Dances)
            {
                round.Dances.Add(dance);
            }
            var reader = new StreamReader(filename, Encoding.UTF7);
            var index = 0;

            while (!reader.EndOfStream)
            {
                var headsofdance = new List<List<Competitor>>();
                round.Couples.Add(this.Dances[index], headsofdance);
                var data = Model.TPSSplitter(reader.ReadLine());
                var heads = Int32.Parse(data[0]);
                for (var i = 1; i <= heads; i++)
                {
                    var couples = Model.TPSSplitter(data[i]);
                    var head = new List<Competitor>();
                    headsofdance.Add(head);
                    foreach (var couple in couples)
                    {
                        if (!String.IsNullOrEmpty(couple))
                        {
                            var cid = Int32.Parse(couple);
                            var co = this.Couples.SingleOrDefault(c => c.Couple.Id == cid);
                            head.Add(co);
                            if (!round.Qualified.Contains(co))
                            {
                                round.Qualified.Add(co);
                            }
                        }
                    }
                }
                index++;
            }
            reader.Close();
            return true;
        }

        private void LoadOfficials()
        {
            this.Judges = new List<OfficalWithRole>();

            this.Officals = new List<OfficalWithRole>();

            var filename = String.Format(this.dataPath + "\\{0:0000}Wr.dat", this.Id);
            if (!File.Exists(filename))
            {
                goto ReadOtherOfficals;
            }
            var reader = new StreamReader(filename, Encoding.UTF7);
            while (!reader.EndOfStream)
            {
                var data = Model.TPSSplitter(reader.ReadLine());
                if (data[1] == "WR")
                {
                    var judge = Model.Instance.Officals.SingleOrDefault(o => o.Sign == data[0]);
                    this.Judges.Add(new OfficalWithRole() { Offical = judge, Role = "Adjudicator" });
                }
            }
            reader.Close();
        // Hier gehts weiter auch wenn wir keine WR's gelesen hatten
        ReadOtherOfficals:
            filename = String.Format(this.dataPath + "\\{0:0000}Fo.dat", this.Id);
            if (!File.Exists(filename))
            {
                return;
            }
            reader = new StreamReader(filename, Encoding.UTF7);
            while (!reader.EndOfStream)
            {
                var data = Model.TPSSplitter(reader.ReadLine());
                var offical = Model.Instance.Officals.SingleOrDefault(o => o.Sign == data[0]);
                var role = "";
                switch (data[1])
                {
                    case "CM":
                        role = "Chairman";
                        break;
                    case "BS":
                        role = "Associate";
                        break;
                    case "TL":
                        role = "Speaker";
                        break;
                    case "PR":
                        role = "Scrutineer";
                        break;
                }

                this.Officals.Add(new OfficalWithRole() { Offical = offical, Role = role });
            }
            reader.Close();
        }

        private void LoadMarkings(string file, string round)
        {
            // Open file and read line by line    
            var reader = new StreamReader(file, Encoding.UTF7);
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                var data = Model.TPSSplitter(line);
                var cid = Int32.Parse(data[0]);
                var competitor = this.Couples.FirstOrDefault(c => c.Couple.Id == cid);

                if (competitor != null)
                {
                    competitor.SetMarks(line, round);
                }
            }
            reader.Close();
        }

        private void LoadMarkings()
        {
            // Try to load First rounds ...
            // 
            var filename = String.Format(this.dataPath + "\\{0:0000}WtV1.dat", this.Id);
            if (File.Exists(filename))
            {
                this.LoadMarkings(filename, "1");
            }
            // Maybe we have a redance?
            filename = String.Format(this.dataPath + "\\{0:0000}WtV2.dat", this.Id);
            if (File.Exists(filename))
            {
                this.LoadMarkings(filename, "R");
            }

            for (var i = 1; i < 20; i++)
            {
                filename = String.Format(this.dataPath + "\\{0:0000}WtZ{1}.dat", this.Id, i);
                if (File.Exists(filename))
                {
                    this.LoadMarkings(filename, "" + (i + 1).ToString());
                }
                else
                {
                    break;
                }
            }
            // Load the finale
            filename = String.Format(this.dataPath + "\\{0:0000}WtE1.dat", this.Id);
            if (File.Exists(filename))
            {
                this.LoadMarkings(filename, "F");
            }
        }

        protected void RaiseEvent(string Property)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(Property));
            }
        }
    }

}
