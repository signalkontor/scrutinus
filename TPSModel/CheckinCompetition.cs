﻿// // TPS.net TPS8 TPSModel
// // CheckinCompetition.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;

namespace TPSModel
{
    public class CheckinCompetition
    {
        private bool _selected;
        public Competition Competition { get; set; }
        public Competitor Competitor { get; set; }

        public bool Selected
        {
            get { return this._selected; } 
            set
            {
                this._selected = value;
            }
        }
    }

    public class CheckinCompetitionList : List<CheckinCompetition>
    {
        public CheckinCompetitionList(IEnumerable<Competition> list, Couple couple)
        {
            foreach (var competition in list)
            {
                var competitior = competition.Couples.SingleOrDefault(c => c.Couple.Id == couple.Id);
                this.Add(new CheckinCompetition() { Competition = competition, Selected = false, Competitor = competitior });
            }
        }
    }
}
