﻿// // TPS.net TPS8 TPSModel
// // Model.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using TPSModel.Enumerations;

namespace TPSModel
{
 
    public class Model
    {
        public enum Languages
        {
            English = 1,
            German = 2
        }

        private readonly int _codePage;

        private readonly string _dataPath;
        private readonly string _infPath;
        private string _tpsPath;


        public Model(string tpspath, string path, string pathToInfFiles, int CodePage)
        {
            this._dataPath = path;
            this._tpsPath = tpspath;
            this._infPath = pathToInfFiles;
            this._codePage = CodePage;
            Instance = this;
            this.Language = Languages.English;
            this.Competitions = new List<Competition>();

            // we copy the files from the TPS folder to not
            // disturb TPS:
            if (path != null)
            {
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                else
                {
                    var allfiles = Directory.GetFiles(path);
                    foreach (var file in allfiles)
                    {
                        File.Delete(file);
                    }
                }
                var files = Directory.GetFiles(tpspath, "*.dat");
                foreach (var file in files)
                {
                    File.Copy(file, path + "\\" + Path.GetFileName(file), true);
                }
            }
            else
            {
                this._dataPath = tpspath;
            }

            this.LoadInfFiles();
            this.LoadCouples();
            this.LoadOfficials();
            this.LoadCompetitions();
            
        }

        public List<Couple> Couples { get; set; }
        public List<Competition> Competitions { get; set; }
        public List<Offical> Officals { get; set; }

        public Dictionary<string, TranslationElement> CompetitionTypes { get; set; }
        public Dictionary<string, TranslationElement> AgeGroups { get; set; }
        public Dictionary<string, TranslationElement> StartClasses { get; set; }
        public Dictionary<string, TranslationElement> DanceNames { get; set; }
        public Dictionary<string, TranslationElement> Sections { get; set; }

        public static Model Instance { get; set; }

        public Languages Language { get; set; }

        #region Data saving methods

        public void SaveParticipants()
        {
            // Writeback Participants list to use in TPS

        }

        #endregion

        public void UpdateAllowedStates()
        {
            foreach (var competition in this.Competitions)
            {
                competition.LoadAllowedState();
            }
        }

        #region Helper

        public static string[] TPSSplitter(string data)
        {
            var tokens = new List<string>();
            var upmode = false;
            var token = "";

            for (var i = 0; i < data.Length; i++)
            {
                if (data[i] == ';' && !upmode)
                {
                    tokens.Add(token);
                    token = "";
                    continue;
                }

                if (data[i] == '"')
                {
                    upmode = !upmode;
                    continue;
                }

                token += data[i];
            }
            // Add the last one at end of line
            tokens.Add(token);

            return tokens.ToArray();

        }

        #endregion

        public void ExportLine(Competition competition, Competitor couple, StreamWriter fs)
        {
            fs.WriteLine(String.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11}",
                        competition.WDSF_Id,
                        couple.State == CoupleState.Dancing ? "1" : "0",
                        competition.ExportAgeGroupCode,
                        competition.StartClassGerman,
                        couple.Couple.FirstNameHe,
                        couple.Couple.LastNameHe,
                        couple.Couple.FirstNameShe,
                        couple.Couple.LastNameShe,
                        couple.Couple.Country,
                        "", // LTV wird derzeit nicht hier verwendet
                        couple.Couple.MinID,
                        couple.Number.ToString()
                        ));
        }

        public void ExportTPSData(string fileName)
        {
            var fs = new StreamWriter(fileName, false, Encoding.ASCII);
            // Let's Export all Couples of all Competitions:
            // We also export only couples that are dancing or excused.
            // When we set all couples to missing in TPS, this will delete the excused
            // switch the once dancing to dancing and leave the missing so we can report them to
            // WDSF API
            foreach (var competition in this.Competitions)
            {
                foreach (var couple in competition.Couples.Where(c => c.State == CoupleState.Dancing || c.State == CoupleState.Excused))
                {
                    this.ExportLine(competition, couple, fs);
                }    
            }

            fs.Flush();
            fs.Close();
        }

        #region Data loading methods

        private void LoadInfFile(string path)
        {
            var ins = new StreamReader(path, Encoding.UTF7);

            while (!ins.EndOfStream)
            {
                var data = TPSSplitter(ins.ReadLine());
                switch (data[0])
                {
                    case "StKl":
                        if (!this.StartClasses.ContainsKey(data[2]))
                        {
                            this.StartClasses.Add(data[2], new TranslationElement(data[2], data[3], data[5], data[4]));
                        }
                        break;
                    case "TnFm":
                        if (!this.CompetitionTypes.ContainsKey(data[3]))
                        {
                            this.CompetitionTypes.Add(data[3], new TranslationElement(data[3], data[4], data[6], data[5]));
                        }
                        break;
                    case "StGr":
                        if (!this.AgeGroups.ContainsKey(data[2]))
                        {
                            this.AgeGroups.Add(data[2], new TranslationElement(data[2], data[3], data[5], data[4]));
                        }
                        break;
                    case "TnArt":
                        if (!this.Sections.ContainsKey(data[2]))
                        {
                            this.Sections.Add(data[2], new TranslationElement(data[2], data[3], data[5], data[4]));
                        }
                        break;
                    case "Tanz":
                        if (!this.DanceNames.ContainsKey(data[2]))
                        {
                            this.DanceNames.Add(data[2], new TranslationElement(data[2], data[3], data[5], data[4]));
                        }
                        break;
                }
            }
        }

        private void LoadInfFiles()
        {
            this.AgeGroups = new Dictionary<string, TranslationElement>();
            this.CompetitionTypes = new Dictionary<string, TranslationElement>();
            this.DanceNames = new Dictionary<string, TranslationElement>();
            this.StartClasses = new Dictionary<string, TranslationElement>();
            this.Sections = new Dictionary<string, TranslationElement>();

            this.AgeGroups.Add("-", new TranslationElement("-", "", "", "-"));
            this.StartClasses.Add("-", new TranslationElement("-", "", "", "-"));

            this.LoadInfFile(String.Format("{0}\\TPSINF0.dat", this._infPath));
            this.LoadInfFile(String.Format("{0}\\TPSINF1.dat", this._infPath));
            this.LoadInfFile(String.Format("{0}\\TPSINF2.dat", this._infPath));
            this.LoadInfFile(String.Format("{0}\\TPSINF3.dat", this._infPath));
            this.LoadInfFile(String.Format("{0}\\TPSINF4.dat", this._infPath));
        }

        private void LoadCompetitions()
        {
            for (var i = 1; i < 200; i++)
            {
                var file = String.Format("{0}\\{1:0000}Td.dat", this._dataPath, i);
                if (File.Exists(file))
                {
                    this.LoadCompetition(i, file);
                }

            }
        }

        private void LoadCompetition(int id, string file)
        {
            // var ins = new StreamReader(file, Encoding.UTF7);
     
            var ins = new StreamReader(file, Encoding.GetEncoding(this._codePage));
            var data = TPSSplitter(ins.ReadLine());
            ins.Close();

            var comp = new Competition(id, data, this._dataPath);

            comp.LoadData();
            this.Competitions.Add(comp);

        }

        private void LoadCouples()
        {
            this.Couples = new List<Couple>();

            if (!File.Exists(this._dataPath + "\\_VaTn.dat"))
            {
                return;
            }

            var reader = new StreamReader(this._dataPath + "\\_VaTn.dat", Encoding.GetEncoding(this._codePage));
            
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                this.Couples.Add(new Couple(line));
            }

            reader.Close();
        }

        private void LoadOfficials()
        {
            this.Officals = new List<Offical>();
            var filename = this._dataPath + "\\_VaFu.dat";

            if(!File.Exists(filename))
            {
                return;
            }
            var reader = new StreamReader(filename, Encoding.GetEncoding(this._codePage));
            
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                var data = TPSSplitter(line);
                var wdsfid = 0;
                Int32.TryParse(data[4], out wdsfid);
                var official = new Offical()
                    {
                        Club = data[3],
                        FirstName = data[1],
                        LastName = data[2],
                        Sign = data[0],
                        MIN = wdsfid
                    };
                this.Officals.Add(official);
            }
            reader.Close();
        }

        #endregion
    }
}
