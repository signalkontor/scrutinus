﻿// // TPS.net TPS8 TPSModel
// // Round.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using TPSModel.Enumerations;

namespace TPSModel
{
    public class Round
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public RoundType Type { get; set; }
        public string WDSF_Name { get; set; }
        public List<string> Dances { get; set; }

        /// <summary>
        /// Round Drawing of this Round. Null when final
        /// </summary>
        public Dictionary<string, List<List<Competitor>>> Couples { get; set; }

        public List<Competitor> Qualified { get; set; }
        public Competition Competition { get; set; }
    }

}
