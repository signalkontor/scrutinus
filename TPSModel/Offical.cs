﻿// // TPS.net TPS8 TPSModel
// // Offical.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

namespace TPSModel
{
    public class Offical
    {
        public string Sign { get; set; }
        public string WDSFSign { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string Name
        {
            get { return this.FirstName + " " + this.LastName; }

        }

        public string Club { get; set; }
        public int MIN { get; set; }
        private string Type { get; set; }
    }

}
