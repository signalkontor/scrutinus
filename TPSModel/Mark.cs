﻿// // TPS.net TPS8 TPSModel
// // Mark.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

namespace TPSModel
{
    public class Mark
    {
        public Offical Judge { get; set; }
        public int Value { get; set; }
        public string Dance { get; set; }
        // Marking for Grand Slam. Not otherwise used.
        public double MarkA { get; set; }
        public double MarkB { get; set; }
        public double MarkC { get; set; }
        public double MarkD { get; set; }
        public double MarkE { get; set; }
    }
}
