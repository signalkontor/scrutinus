﻿// // TPS.net TPS8 XD Server
// // PingThread.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Threading;

namespace com.signalkontor.XD_Server
{
    internal class PingThread
    {
        public PingThread()
        {
            new Thread(Run) {IsBackground = true}.Start();
        }

        private static void Run()
        {
            ServerCore.WriteLine("PingThread started");
            while (true)
            {
                Thread.Sleep(10000);
                // wir senden an alle User ein Ping und schlafen dann wieder ein
                var users = new List<string>(UserManager.GetAllUser());
                foreach (var user in users)
                {
                    if (ServerCore.Instance.SendPingToMobile(user) == 0)
                    {
                        continue;
                    }

                    var tmqeJsMsg = new TMQeJSMsg
                                        {
                                            TargetId = 3,
                                            DoCmd = "",
                                            MsgData = user,
                                            TimeStamp = DateTime.Now,
                                            Sender = user
                                        };
                    UserManager.Logoff(user);
                    ServerCore.Instance.SendMessageToQueue(tmqeJsMsg);
                }
            }
        }
    }
}
