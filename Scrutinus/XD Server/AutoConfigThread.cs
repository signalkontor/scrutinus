﻿// // TPS.net TPS8 XD Server
// // AutoConfigThread.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace com.signalkontor.XD_Server
{
    internal class AutoConfigThread
    {
        private readonly List<String> _accessPoints = new List<string>();
        private readonly Dictionary<string, string> _deviceList;
        private readonly int _firstDevice = 130;
        private readonly int _localPort;
        private readonly Thread _thread;
        private readonly string devicesFile;
        private int _currentDevice;

        public AutoConfigThread(int autoConfigPort,  string accessPoints, string devicesFile )
        {
            this.devicesFile = devicesFile;
            this._localPort = autoConfigPort;
            var ap = accessPoints.Split(',');
            foreach (var t in ap)
            {
                this._accessPoints.Add(t.Trim().ToLower());
            }

            this._deviceList = new Dictionary<string, string>();
            this._currentDevice = this._firstDevice;
            this._thread = new Thread(this.Run) { IsBackground = true };
            this.LoadDeviceList();
            this._thread.Start();
        }

        private void LoadDeviceList()
        {
            if (!File.Exists(this.devicesFile))
            {
                File.Create(this.devicesFile);
                return;
            }

            var stream = new StreamReader(this.devicesFile);
            while (!stream.EndOfStream)
            {
                var line = stream.ReadLine();
                var data = line.Split(new[] { '=' });
                if (data.Length == 2)
                {
                    this._deviceList.Add(data[0], data[1]);

                    // current Device hochzählen ...
                    int intId;
                    var id = data[1].StartsWith("eJS") ? data[1].Substring(3) : data[1];

                    if (Int32.TryParse(id, out intId))
                    {
                        if (this._currentDevice <= intId)
                        {
                            this._currentDevice = intId + 1;
                        }
                    }
                }
            }
            stream.Close();
        }

        private void SaveDeviceList()
        {
            lock (this._deviceList)
            {
                var writer = new StreamWriter(this.devicesFile);
                foreach (var key in this._deviceList.Keys)
                {
                    writer.WriteLine(key + "=" + this._deviceList[key]);
                }
                writer.Close();
            }
        }

        private string FindNextId()
        {
            var id = this._firstDevice;
            while (true)
            {
                var devId = "eJS" + String.Format("{0}", id);
                if (this._deviceList.ContainsValue(devId))
                {
                    id++;
                }
                else
                {
                    return devId;
                }
            }
        }

        private void Run()
        {
            ServerCore.WriteLine("AutoConfigThread Listening at Port " + this._localPort);
            // UDP Socket aufmachen und auf Verbindungen warten ...
            UdpClient udpClient;
            try
            {
                udpClient = new UdpClient(this._localPort, AddressFamily.InterNetwork) {EnableBroadcast = true};
            }
            catch (Exception ex)
            {
                // todo: do something with the exception
                return;
            }
            var remoteIPEndPoint = new IPEndPoint(0, 0);

            while (true)
            {
                try
                {
                    var buffer = udpClient.Receive(ref remoteIPEndPoint);
                    // Wir brauchen nun einen String
                    var data = Encoding.Unicode.GetString(buffer, 0, buffer.Length);
                    ServerCore.WriteLine("Received data from: {0}: {1}", remoteIPEndPoint, data);
                    
                    var s = data.Split(';');

                    if (s.Length < 2)
                    {
                        throw new Exception("Data not correct: " + data);
                    }
                    
                    // Damit es keine Vertipper gibt vergleichen wir Caseinsensitiv
                    if (this._accessPoints.Contains(s[0].ToLower()) || this._accessPoints.Contains("*"))
                    {
                        if (this._accessPoints.Contains("*"))
                        {
                            ServerCore.WriteLine("* => Accept any Access Points");
                        }

                        lock (this._deviceList)
                        {
                            string deviceName;
                            // OK, wir sind zuständig, also schauen, ob Device bekannt.
                            if (this._deviceList.ContainsKey(s[1]))
                            {   // ja, bekannt ...
                                deviceName = this._deviceList[s[1]];
                                SendConfig(remoteIPEndPoint, deviceName);
                            }
                            else
                            {   // nein, also neues device anlegen
                                deviceName = this.FindNextId();
                                this._deviceList.Add(s[1], deviceName);
                                SendConfig(remoteIPEndPoint, deviceName);
                                this.SaveDeviceList();
                            }
                            UserManager.Logon(deviceName, remoteIPEndPoint.Address);
                        }
                    }
                }
                catch (Exception e)
                {
                    ServerCore.WriteLine(e.Message);
                    ServerCore.WriteLine("Error sending config" + e.Message);
                }
            }
        }

        private static void SendConfig(IPEndPoint remoteIPEndPoint, string device)
        {
            // We give the client a moment to be ready to receive our answer:
            Thread.Sleep(250);

            var client = new UdpClient();
            var enc = new UnicodeEncoding();
            var b = enc.GetBytes(device);

            var endPoint = new IPEndPoint(remoteIPEndPoint.Address, 9095);

            remoteIPEndPoint.Port = 9095;
            client.Send(b, b.Length, endPoint);
            ServerCore.WriteLine("send configuration to: " + remoteIPEndPoint + ": " + device);
        }
    }
}
