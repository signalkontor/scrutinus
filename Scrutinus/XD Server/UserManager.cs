﻿// // TPS.net TPS8 XD Server
// // UserManager.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;

namespace com.signalkontor.XD_Server
{
    public static class UserManager
    {
        private static readonly Dictionary<string, IPAddress> UsersIpAddresses = new Dictionary<string, IPAddress>();

        [MethodImpl(MethodImplOptions.Synchronized)]
        public static void Logon(string user, IPAddress ipAddress)
        {
            if (user == null)
            {
                throw new ArgumentException("user must not be null");
            }
            if (ipAddress == null)
            {
                throw new ArgumentException("ipAddress must not be null");
            }

            var old = UsersIpAddresses.SingleOrDefault(o => o.Value == ipAddress).Key;
            if (old != null)
            {
                UsersIpAddresses.Remove(old); // we don't want duplicate ip addresses
            }

            UsersIpAddresses[user] = ipAddress;

            ServerCore.Instance.SendLogonMessage(user);

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public static void Logoff(string user)
        {
            if(UsersIpAddresses.ContainsKey(user))
            {
                UsersIpAddresses.Remove(user);
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public static IPAddress GetAddress(string user)
        {
            return UsersIpAddresses.ContainsKey(user) ? UsersIpAddresses[user] : null;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public static string GetUser(IPAddress address)
        {
            var result = UsersIpAddresses.Where(o => o.Value == address);
            return result.Any() ? result.Single().Key : null;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public static IEnumerable<string> GetAllUser()
        {
            return UsersIpAddresses.Keys;
        }
    }
}
