﻿// // TPS.net TPS8 XD Server
// // ListenerThreadEx.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using com.signalkontor.Common;
using log4net;

namespace com.signalkontor.XD_Server
{

    public interface IListenerThread
    {
        int SendMessage(COMMessage msg, string user);
        int SendPing(string user);
        void Start();
    }


    public class MessageEventArgs : EventArgs
    {
        public MessageEventArgs(int appId, byte[] data)
        {
            switch (appId)
            {
                case 1:
                    this.Message = COMMessage.FromJson<LogonMessage>(data);
                    break;
                case 2:
                    this.Message = COMMessage.FromJson<StringMessage>(data);
                    break;
                case 120:
                    this.Message = COMMessage.FromJson<TPSMessage>(data);
                    break;
            }
        }

        public MessageEventArgs(COMMessage message)
        {
            this.Message = message;
        }

        public string User { get; set; }
        public COMMessage Message { get; set; }
    }


    public delegate void MessageReceivedDelegate(object sender, MessageEventArgs e);

    internal class ListenerThreadEx : IListenerThread
    {
        private static readonly Encoding Encoding = Encoding.UTF8;
        protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly int port;
        private readonly TcpListener tcpListener;

        public ListenerThreadEx(int port)
        {
            this.port = port;

            var ipAddress = NetworkInterface.GetAllNetworkInterfaces()
                .SelectMany(ni => ni.GetIPProperties().UnicastAddresses)
                .Where(ua => ua.Address.AddressFamily == AddressFamily.InterNetwork && !IPAddress.IsLoopback(ua.Address))
                .Select(ua => ua.Address)
            .First();

            this.tcpListener = new TcpListener(IPAddress.Any, port);

        }

        public void Start()
        {
            try
            {
                this.tcpListener.Start();
            }
            catch (SocketException socket)
            {
                // todo: do something with the exception
                return;
            }

            while (true)
            {    
                var tcpClient = this.tcpListener.AcceptTcpClient();
                new Task(obj => ReceiveMessageMulti(obj as TcpClient), tcpClient).Start();
            }
        }

        public int SendPing(string user)
        {
            var tcpClient = new TcpClient();
            var address = UserManager.GetAddress(user);

            try
            {
                tcpClient.Connect(new IPEndPoint(address, this.port));
                using (var stream = tcpClient.GetStream())
                {
                    
                    // wir senden nun die Länge unserer Daten
                    var len = BitConverter.GetBytes(1);
                    stream.Write(len, 0, len.Length);
                    stream.WriteByte((byte) 1);
                    stream.Flush();

                    // -1 == no acknowlege byte sent => user is offline
                    if (stream.ReadByte() == -1)
                    {
                        return 1;
                    }
                }
            }
            catch(Exception ex)
            {
                ServerCore.WriteLine("Could not Ping User " + user + ": " + ex.Message);
                // something went wrong => assume message did not reach the recipient
                return 1;
            }
            finally
            {
                tcpClient.Close();
            }
            // no errors, got acknowledge byte => all ok
            return 0;
        }

        public int SendMessage(COMMessage msg, string user)
        {
            var tcpClient = new TcpClient();
            var address = UserManager.GetAddress(user);
            if (address == null)
            {
                ServerCore.WriteLine("Could not find address for device " + user + " please restart ejudge on this device");
                return 10;
            }
            try
            {
                tcpClient.Connect(new IPEndPoint(address, this.port));
                using (var stream = tcpClient.GetStream())
                {
                    var bytes = Encoding.GetBytes(msg.ToJson());
                    // wir senden nun die Länge unserer Daten
                    var len = BitConverter.GetBytes(bytes.Length);
                    stream.Write(len, 0, len.Length);
                    stream.Write(bytes, 0, bytes.Length);
                    stream.Flush();

                    // -1 == no acknowlege byte sent => user is offline
                    if (stream.ReadByte() == -1)
                    {
                        return 1;
                    }
                }
            }
            catch(Exception ex)
            {
                // something went wrong => assume message did not reach the recipient
                Log.Error(ex);
                Log.Error(ex.StackTrace);
                return 1;
            }
            finally
            {
                tcpClient.Close();
            }
            // no errors, got acknowledge byte => all ok
            return 0;
        }

        public void Stop()
        {
            lock (this.tcpListener)
            {
                this.tcpListener.Stop();
            }
        }

        private static void ReceiveMessageMulti(TcpClient tcpClient)
        {
            var count = 0;
            while (ReceiveMessage(tcpClient))
            {
                count++;
                // ServerCore.WriteLine(count);
            }
            // Fertig, Schließen
            tcpClient.Close();
        }

        private static bool ReceiveMessage(TcpClient tcpClient)
        {
            try
            {
                var stream = tcpClient.GetStream();
                stream.ReadTimeout = 15000;

                var pos = 0;
                var lenBuffer = new byte[4];

                if (stream.Read(lenBuffer, 0, 4) == 0)
                {
                    return false;
                }
                    
                var dataSize = BitConverter.ToInt32(lenBuffer, 0);
                var buffer = new byte[dataSize];
    
                if (dataSize == 1)
                {
                    // Dies ist ein Ping und wir senden nun eine Bestätigung zurück
                    stream.Write(BitConverter.GetBytes(true), 0, 1);
                    ServerCore.WriteLine("Ping von " + tcpClient.Client.RemoteEndPoint.ToString());
                    stream.Close();                    
                    return false;
                }

                while (pos < buffer.Length)
                {
                    var bytesRead = stream.Read(buffer, pos, buffer.Length - pos);
                    if (bytesRead == -1)
                    {
                        break;
                    }
                    pos += bytesRead;
                }

                var genericMessage = COMMessage.FromJson<COMMessage>(buffer);
                var tmqeJsMsg = new TMQeJSMsg();

                switch (genericMessage.AppID)
                {
                    case 1: // logon message
                        var logonMessage = COMMessage.FromJson<LogonMessage>(buffer);
                        var end = (IPEndPoint)tcpClient.Client.RemoteEndPoint;
                        UserManager.Logon(logonMessage.User, end.Address);
                        // Logon hat bereit die Nachricht an mTPS geschickt, wir sind fertig
                        stream.Write(BitConverter.GetBytes(true), 0, 1);
                        return true;
                    case 2: // string message
                        var stringMessage = COMMessage.FromJson<StringMessage>(buffer);
                        tmqeJsMsg.TargetId = 3; // User Logoff
                        tmqeJsMsg.DoCmd = "";
                        tmqeJsMsg.MsgData = stringMessage.Message;
                        tmqeJsMsg.TimeStamp = DateTime.Now;
                        tmqeJsMsg.Sender = stringMessage.Message;
                        break;

                    case 120: // tps message
                        var tpsMessage = COMMessage.FromJson<TPSMessage>(buffer);
                        tmqeJsMsg.DoCmd = tpsMessage.FDoCmd;
                        tmqeJsMsg.MsgData = tpsMessage.FMsgData;
                        tmqeJsMsg.ResponseId = tpsMessage.FResponseId;
                        tmqeJsMsg.Sender = tpsMessage.FSender;
                        tmqeJsMsg.TargetId = tpsMessage.FTargetId;
                        tmqeJsMsg.TimeStamp = tpsMessage.FTimeStamp;
                        tmqeJsMsg.ReceiverIP = tpsMessage.FReceiverIP;
                        tmqeJsMsg.Receiver = tpsMessage.FReceiver;

                        var endIp = (IPEndPoint)tcpClient.Client.RemoteEndPoint;
                        UserManager.Logon(tmqeJsMsg.Sender, endIp.Address);
                        break;

                    default:
                        tmqeJsMsg.DoCmd = "";
                        tmqeJsMsg.MsgData = "unknown Message: " + genericMessage.AppID;
                        tmqeJsMsg.Receiver = "";
                        tmqeJsMsg.ReceiverIP = "";
                        tmqeJsMsg.ResponseId = 0;
                        tmqeJsMsg.Sender = null; // will be set later
                        tmqeJsMsg.TargetId = 500;
                        tmqeJsMsg.TimeStamp = DateTime.Now;
                        break;
                }

                if (tmqeJsMsg.Sender == null)
                {
                    var endpoint = tcpClient.Client.RemoteEndPoint;
                    string user = null;
                    if (endpoint is IPEndPoint)
                    {
                        user = UserManager.GetUser((endpoint as IPEndPoint).Address);
                    }
                    tmqeJsMsg.Sender = user ?? "";
                }

                ServerCore.Instance.SendMessageToQueue(tmqeJsMsg);

                // wir senden eine Quittung zurück
                stream.Write(BitConverter.GetBytes(true), 0, 1);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
