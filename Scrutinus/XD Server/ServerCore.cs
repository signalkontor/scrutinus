﻿// // TPS.net TPS8 XD Server
// // ServerCore.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.ExceptionServices;
using System.Threading.Tasks;
using com.signalkontor.Common;
using log4net;

namespace com.signalkontor.XD_Server
{
    public class ServerCore
    {
        public delegate void NewLogMessageEventHandler(object sender, string message);

        protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IListenerThread listenerThread;

        private readonly MSQueueThread queueThread;

        private AutoConfigThread autoConfigThread;

        private readonly Dictionary<string, Queue<TPSMessage>> messages;

        private PingThread pingThread;

        public ServerCore(int port, int autoConfigPort, string accessPoints, string devicesFile, string inQueue, string outQueue)
        {
            Instance = this;

            WriteLine("XD Server Version 2.3.230");
            // Wir holen unseren ListenerThread
            
            Log.Error("Start of Server ... Log initialized");
            AppDomain.CurrentDomain.FirstChanceException += new EventHandler<FirstChanceExceptionEventArgs>(this.CurrentDomain_FirstChanceException);
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(this.CurrentDomain_UnhandledException);
            this.messages = new Dictionary<string, Queue<TPSMessage>>();
            this.listenerThread = new ListenerThreadEx(port);
            Task.Factory.StartNew(() => this.listenerThread.Start());
            this.autoConfigThread = new AutoConfigThread(autoConfigPort, accessPoints, devicesFile);
            this.queueThread = new MSQueueThread(inQueue, outQueue);
            this.queueThread.Run();
            this.pingThread = new PingThread();
        }

        public static ServerCore Instance { get; set; }

        public event NewLogMessageEventHandler NewLogMessageEvent;

        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            if (e.ExceptionObject is Exception)
            {
                var ex = (Exception) e.ExceptionObject;
                WriteLine("Unhandlelt Exception: \r\n" + ex.Message + "\r\n" + ex.StackTrace);
                Log.Error("Unhandelt Exception: " + ex.Message);
                Log.Error(ex.StackTrace);
            }
        }

        private void CurrentDomain_FirstChanceException(object sender, FirstChanceExceptionEventArgs e)
        {
            // ServerCore.WriteLine("First Chance Exception: \r\n" + e.Exception.Message + "\r\n" + e.Exception.StackTrace);
            // Log.Error("First Chance Exception: " + e.Exception.Message);
            // Log.Error(e.Exception.StackTrace);
        }

        public void SendMessageToMobile(TPSMessage msg)
        {
            if (this.listenerThread.SendMessage(msg, msg.FReceiver) != 0)
            {
                // wir müssen diese Nachricht für den User zwischenspeichern
                // ToDo: Zwischenspeichern
                lock (this.messages)
                {
                    Queue<TPSMessage> queue;
                    if (!this.messages.ContainsKey(msg.FReceiver))
                    {
                        queue = new Queue<TPSMessage>();
                        this.messages.Add(msg.FReceiver, queue);
                    }
                    else
                    {
                        queue = this.messages[msg.FReceiver];
                    }

                    queue.Enqueue(msg);
                }
            }
        }

        public int SendPingToMobile(string user)
        {
            var ret = this.listenerThread.SendPing(user);
            if (ret == 0)
            {
                // Wir prüfen nun, ob wir daten für diesen User haben
                lock (this.messages)
                {
                    if (this.messages.ContainsKey(user))
                    {
                        var queue = this.messages[user];
                        while (queue.Count > 0)
                        {
                            var msg = queue.Peek();
                            if (this.listenerThread.SendMessage(msg, user) == 0)
                            {
                                queue.Dequeue();
                            }
                            else
                            {
                                break;
                            }
                        }
                        if (queue.Count == 0)
                        {
                            this.messages.Remove(user);
                        }
                    }
                }
            }
            return ret;
        }

        public void SendLogonMessage(string user)
        {
            var tmqeJsMsg = new TMQeJSMsg();
            tmqeJsMsg.TargetId = 2; // User Logon
            tmqeJsMsg.DoCmd = "";
            tmqeJsMsg.MsgData = user;
            tmqeJsMsg.TimeStamp = DateTime.Now;
            tmqeJsMsg.Sender = user;
            this.SendMessageToQueue(tmqeJsMsg);
        }

        public void SendLogoffMessage(string user)
        {
            var tmqeJsMsg = new TMQeJSMsg();
            tmqeJsMsg.TargetId = 3; // User Logoff
            tmqeJsMsg.DoCmd = "";
            tmqeJsMsg.MsgData = user;
            tmqeJsMsg.TimeStamp = DateTime.Now;
            tmqeJsMsg.Sender = user;
            this.SendMessageToQueue(tmqeJsMsg);

        }

        public void SendMessageToQueue(TMQeJSMsg msg)
        {
            this.queueThread.SendMessage(msg);
        }

        public static void WriteLine(string format, params object[] parameter)
        {
            var str = string.Format(format, parameter);

            Console.WriteLine(format, parameter);

            if (Instance.NewLogMessageEvent != null)
            {
                Instance.NewLogMessageEvent(Instance, str);
            }
        }
    }
}
