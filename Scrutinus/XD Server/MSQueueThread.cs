﻿// // TPS.net TPS8 XD Server
// // MSQueueThread.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Messaging;
using System.Threading.Tasks;
using com.signalkontor.Common;

namespace com.signalkontor.XD_Server
{
    public class MSQueueThread
    {
        private MessageQueue outQueue;
        private MessageQueue queue;

        private readonly string inQueueName;

        private readonly object outQueueLock = new object();

        private readonly string outQueueName;

        public MSQueueThread(string inQueue, string outQueue)
        {
            this.inQueueName = inQueue;
            this.outQueueName = outQueue;
        }

        public void Run()
        {
            try
            {
                var queuePath = this.outQueueName;

                this.queue = new MessageQueue(queuePath);
                this.queue.ReceiveCompleted += this.QueueReceiveCompleted;
                this.queue.Formatter = new XmlMessageFormatter(new[] { typeof(TMQeJSMsg) });
                ServerCore.WriteLine("MS Queue Thread started");
                this.queue.BeginReceive();
            }
            catch (Exception e)
            {
                ServerCore.WriteLine(e.Message);
                throw;
            }
        }

        private static void ProcessMessage(TMQeJSMsg tps)
        {
            if (tps == null)
            {
                return;
            }

            if (tps.Sender == null)
            {
                tps.Sender = "";
            }
            if (tps.DoCmd == null)
            {
                tps.DoCmd = "";
            }
            if (tps.MsgData == null)
            {
                tps.MsgData = "";
            }
            if (tps.Receiver == null)
            {
                tps.Receiver = "";
            }

            var t = new TPSMessage
            {
                FDoCmd = tps.DoCmd,
                FMsgData = tps.MsgData,
                FResponseId = tps.ResponseId,
                FSender = tps.Sender,
                FTargetId = tps.TargetId,
                FTimeStamp = tps.TimeStamp,
                FReceiverIP = tps.ReceiverIP,
                FReceiver = tps.Receiver
            };

            if (t.FDoCmd == null)
            {
                t.FDoCmd = "";
            }
            if (t.FMsgData == null)
            {
                t.FMsgData = "";
            }
            if (t.FSender == null)
            {
                t.FSender = "";
            }

            Task.Factory.StartNew(() => ServerCore.Instance.SendMessageToMobile(t));
        }

        private void QueueReceiveCompleted(object sender, ReceiveCompletedEventArgs e)
        {

            try
            {
                var msg = this.queue.EndReceive(e.AsyncResult);

                if (msg.Body is TMQeJSMsg)
                {
                    var tps = (TMQeJSMsg)msg.Body;

                    ProcessMessage(tps);

                    while (tps != null)
                    {
                        msg = this.queue.Receive(new TimeSpan(0, 0, 0, 5));
                        if (msg == null)
                        {
                            break;
                        }

                        tps = msg.Body as TMQeJSMsg;

                        ProcessMessage(tps);

                    }

                }
            }
            catch (Exception ex)
            {
                if (!ex.Message.Contains("Timeout"))
                {
                    ServerCore.WriteLine(ex.Message);
                }
            }

            this.queue.BeginReceive();
        }

        public void SendMessage(TMQeJSMsg msg)
        {
            if (this.outQueue == null)
            {
                this.outQueue = new MessageQueue(this.inQueueName);
                var formatter = new XmlMessageFormatter(new[] { typeof(TMQeJSMsg) });
                this.queue.Formatter = formatter;
            }

            try
            {
                lock (this.outQueueLock)
                {
                    this.outQueue.Send(msg);
                }
            }
            catch (Exception ex)
            {
                ServerCore.WriteLine(ex.Message);
            }
        }
    }
}
