﻿// // TPS.net TPS8 Helpers
// // SecurityHelper.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Security.Cryptography;
using System.Text;

namespace Helpers
{
    public static class SecurityHelper
    {
        /// <summary>
        /// Calculates the MD5 hash.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>A string representing the MD5 hash of the string</returns>
        public static string CalculateMd5Hash(string value)
        {
            using (var md5Hash = MD5.Create())
            {
                // Convert the input string to a byte array and compute the hash.
                var data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(value));

                // Create a new Stringbuilder to collect the bytes
                // and create a string.
                var stringBuilder = new StringBuilder();

                // Loop through each byte of the hashed data 
                // and format each one as a hexadecimal string.
                for (var i = 0; i < data.Length; i++)
                {
                    stringBuilder.Append(data[i].ToString("x2"));
                }

                // Return the hexadecimal string.
                return stringBuilder.ToString();
            }
        }
    }
}
