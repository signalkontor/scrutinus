﻿// // TPS.net TPS8 Helpers
// // SkatingViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Linq;
using DataModel.Models;

namespace Helpers.Skating
{
    public class SkatingViewModel
    {
        private int placeToGive;

        public SkatingViewModel(
            ScrutinusContext context,
            Round round,
            IEnumerable<Participant> participants,
            IEnumerable<DanceInRound> dances,
            IEnumerable<Official> judges)
        {
            this.DanceSkating = new List<DanceSkatingViewModel>();
            this.Rule11ViewModel = new List<CoupleResult>();
            this.Rule10ViewModel = new List<CoupleResult>();

            this.Participants = participants.ToList();
            this.Judges = judges.ToList();
            this.Round = round;

            foreach (var dance in dances)
            {
                this.DanceSkating.Add(new DanceSkatingViewModel(context, round, participants, dance, this.Judges));
            }
        }

        public Round Round { get; set; }

        public List<DanceSkatingViewModel> DanceSkating { get; set; }

        public List<Rule9ViewModel> Rule9ViewModels { get; set; }

        public List<CoupleResult> Rule10ViewModel { get; set; }

        public List<CoupleResult> Rule11ViewModel { get; set; }

        public List<Participant> Participants { get; set; }

        public List<Official> Judges { get; set; }

        public void DoCalculations()
        {
            this.Rule9ViewModels = new List<Rule9ViewModel>();

            foreach (var participant in this.Participants)
            {
                participant.PlaceFrom = null;
                participant.PlaceTo = null;
            }

            foreach (var danceSkatingViewModel in this.DanceSkating)
            {
                if (danceSkatingViewModel.HasAllMarks())
                {
                    danceSkatingViewModel.DoCalculations();
                    danceSkatingViewModel.RaiseChangedEvent();
                }
            }

            this.Rule9ViewModels.Clear();
            this.Rule10ViewModel.Clear();
            this.Rule11ViewModel.Clear();

            this.CalculateRule9();

            this.ApplyPlacesToParticipants();
        }

        private void ApplyPlacesToParticipants()
        {
            // Now apply places to participants.
            var resolvedRule9 = this.Rule9ViewModels.Where(r => r.IsResolved);
            foreach (var rule9ViewModel in resolvedRule9)
            {
                rule9ViewModel.Participant.PlaceFrom = rule9ViewModel.Place;
                rule9ViewModel.Participant.PlaceTo = rule9ViewModel.Place;
                var participant = this.Participants.Single(p => p.Number == rule9ViewModel.Participant.Number);
                participant.PlaceFrom = rule9ViewModel.Place;
                participant.PlaceTo = rule9ViewModel.Place;
            }

            var resolvedRule10 = this.Rule10ViewModel.Where(r => r.IsResolved);
            foreach (var rule10ViewModel in resolvedRule10)
            {
                rule10ViewModel.Participant.PlaceFrom = Convert.ToInt32(rule10ViewModel.PlaceFrom);
                rule10ViewModel.Participant.PlaceTo = Convert.ToInt32(rule10ViewModel.PlaceTo);
            }

            var resolvedRule11 = this.Rule11ViewModel.Where(r => r.IsResolved).ToList();
            foreach (var coupleResult in resolvedRule11)
            {
                coupleResult.Participant.PlaceFrom = coupleResult.PlaceFrom;
                coupleResult.Participant.PlaceTo = coupleResult.PlaceTo;
            }

            foreach (var rule9ViewModel in this.Rule9ViewModels)
            {
                rule9ViewModel.Participant.Points = rule9ViewModel.Sum;
            }
        }

        private void CalculateRule9()
        {
            this.Rule10ViewModel = new List<CoupleResult>();

            foreach (var participant in this.Participants)
            {
                var results = this.DanceSkating.SelectMany(p => p.CoupleResults.Where(c => c.Participant.Id == participant.Id)).Select(r => r.Place).ToList();
                this.Rule9ViewModels.Add(new Rule9ViewModel(participant, results));
            }

            var ordered = this.Rule9ViewModels.OrderBy(r => r.Sum).GroupBy(r => r.Sum);

            this.placeToGive = 1;

            foreach (var group in ordered)
            {
                if (group.Count() == 1)
                {
                    group.First().Place = this.placeToGive;
                    group.First().IsResolved = true;
                    this.placeToGive++;
                }
                else
                {
                    this.CalculateRule10(group);
                }
            }
        }

        private void CalculateRule10(IEnumerable<Rule9ViewModel> group)
        {
            var rule10List = new List<CoupleResult>();

            foreach (var rule9ViewModel in group)
            {
                // Note: For Rule 10 we use the place.
                // a devided place will be count as 2. place
                var places =
                    this.DanceSkating.SelectMany(
                        p =>
                        p.CoupleResults.Where(c => c.Participant.Id == rule9ViewModel.Participant.Id)
                            .Select(r => r.Place)).ToList();

                rule10List.Add(new CoupleResult(rule9ViewModel.Participant, this.Participants.Count(), this.Judges.Count) { Marks = places });
            }

            var candidateCount = int.MaxValue;
            var candidates = rule10List.Where(r => !r.IsResolved).ToList();
           
            // we try to solve the places until we cannot solve anything anymore
            while (candidates.Count < candidateCount && candidates.Count > 0)
            {
                candidateCount = candidates.Count;

                this.placeToGive = SkatingHelper.CalculateRule10(candidates, this.placeToGive, this.Participants.Count());

                if (candidates.Any(r => !r.IsResolved))
                {
                    var rule11Candidates = new List<CoupleResult>();

                    var grouped = rule10List.Where(c => !c.IsResolved).OrderByDescending(o => o.NumberPlaces[placeToGive - 1]).GroupBy(r => r.NumberPlaces[placeToGive - 1]);

                    this.placeToGive = this.HandleRule11(grouped.First().ToList(), rule11Candidates);

                    rule11Candidates.Where(c => c.IsResolved).ToList().ForEach((c) =>
                    {
                        var result = candidates.First(ca => ca.Participant.Number == c.Participant.Number);
                        result.IsResolvedRule11 = true;
                        // Reset Sums and places of the one resolved couple:
                        SkatingHelper.ResetSkatingSumsAndPlaces(this.placeToGive, this.Participants.Count, result);
                    });
                }

                // Find all couples that are still not placed in rule 10 or 11:
                candidates = rule10List.Where(r => !r.IsResolved && !r.IsResolvedRule11).ToList();

                if (candidates.Count == 1)
                {
                    candidates[0].IsResolved = true;
                    candidates[0].PlaceFrom = this.placeToGive;
                    candidates[0].PlaceTo = this.placeToGive;
                    candidates[0].Place = this.placeToGive;
                    candidates[0].IsResolved = true;

                    this.placeToGive++;

                    break;
                }
            }

            this.Rule10ViewModel.AddRange(rule10List);
        }

        private int HandleRule11(List<CoupleResult> candidates, IList<CoupleResult> rule11Candidates)
        {
            var newPlace = this.CalculateRule11(candidates, this.placeToGive, rule11Candidates);

            if (newPlace == 0)
            {
                // we could not devide this, so lets devide places:
                foreach (var coupleResult in candidates)
                {
                    coupleResult.PlaceFrom = this.placeToGive;
                    coupleResult.PlaceTo = this.placeToGive + candidates.Count - 1;
                    coupleResult.Place = this.placeToGive;
                    coupleResult.IsResolved = true;
                }

                return this.placeToGive;
            }

            return newPlace;
        }

        private void RemovePlacedCouplesAfterRule11(List<CoupleResult> rule10List)
        {
            foreach (var rule11ViewModel in this.Rule11ViewModel.Where(r => r.IsResolved))
            {
                var element = rule10List.FirstOrDefault(r => r.Participant.Id == rule11ViewModel.Participant.Id);
                if (element != null)
                {
                    this.Rule10ViewModel.Add(element);
                    rule10List.Remove(element);
                }
            }
        }


        private int CalculateRule11(IEnumerable<CoupleResult> candidates, int placeToGive, IList<CoupleResult> rule11Candidates)
        {
            var majority = this.DanceSkating.Count * this.Judges.Count / 2 + 1;
            var maxPlaceToSearch = this.Participants.Count();
            
            // build up rule 11 viewModel:
            foreach (var coupleResult in candidates)
            {
                var allMarks = this.DanceSkating.SelectMany(d => d.CoupleResults.First(r => r.Participant.Id == coupleResult.Participant.Id).Marks.Select(m => Convert.ToDouble(m.Mark))).ToList();
                var result = new CoupleResult(coupleResult.Participant, this.Participants.Count(), this.Judges.Count) { Marks = allMarks };

                var oldResult = this.Rule11ViewModel.FirstOrDefault(r => r.Participant.Id == coupleResult.Participant.Id);
                if (oldResult != null)
                {
                    this.Rule11ViewModel.Remove(oldResult);
                }

                this.Rule11ViewModel.Add(result);
                
                rule11Candidates.Add(result);

                for (var i = placeToGive; i <= this.Participants.Count; i++)
                {
                    result.NumberPlaces[i - 1] = result.Marks.Count(m => m <= i);
                    result.Sums[i - 1] = result.Marks.Where(m => m <= i).Sum(m => m);
                }
            }

            var newPlace = SkatingHelper.CalculateSkating(majority, placeToGive, maxPlaceToSearch, rule11Candidates.Cast<SkatingModelBase>().ToList(), false, false, true);

            if (rule11Candidates.Count() == 2 && rule11Candidates.Count(c => !c.IsResolved) == 1)
            {
                var unplaced = rule11Candidates.First(c => !c.IsResolved);
                unplaced.IsResolved = true;
                unplaced.Place = newPlace;
                unplaced.PlaceFrom = newPlace;
                unplaced.PlaceTo = newPlace;
                unplaced.Sums[placeToGive - 1] = 0;

                SkatingHelper.ResetSkatingSumsAndPlaces(newPlace + 1, this.Participants.Count, unplaced);

                newPlace++;
            }

            return newPlace;
        }
    }
}
