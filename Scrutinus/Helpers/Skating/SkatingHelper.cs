﻿// // TPS.net TPS8 Helpers
// // SkatingHelper.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using DataModel.Models;

namespace Helpers.Skating
{
    public static class SkatingHelper
    {
        public static SkatingViewModel CreateViewModel(ScrutinusContext context, Round round)
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();

            Debug.WriteLine("CreateViewModel");

            var participants = round.Qualifieds.Select(q => q.Participant).OrderBy(p => p.Number).ToList();

            var skatingViewModel = new SkatingViewModel(context, round, participants.OrderBy(p => p.Number), round.DancesInRound, round.Competition.GetJudges());

            Debug.WriteLine("new SkatingViewModel: " + stopWatch.ElapsedMilliseconds);
            try
            {
                skatingViewModel.DoCalculations();
                Debug.WriteLine("DoCalculation: " + stopWatch.ElapsedMilliseconds);
            }
            catch (Exception)
            {

            }

            return skatingViewModel;
        }

        public static int CalculateRule10(List<CoupleResult> candidates, int placeToFind, int participantsCount)
        {
            CalculatePlacesAndSumsRule10(candidates, placeToFind, participantsCount);

            // order and resolve:
            var newPlace = TryResolvePlace(candidates, placeToFind, placeToFind, participantsCount, true, false);

            if (newPlace.Item1 == placeToFind)
            {
                // we have not been able to resolve this with group 10
                return placeToFind;
            }

            var newCandidates = candidates.Where(c => !c.IsResolved).ToList();

            if (newPlace.Item1 == placeToFind || !newCandidates.Any() || placeToFind == participantsCount)
            {
                return newPlace.Item1;
            }

            var resolvedPlaces = candidates.Count - newCandidates.Count;

            // We try again with the other candidates
            return CalculateRule10(newCandidates, placeToFind + resolvedPlaces, participantsCount);
        }

        private static void CalculatePlacesAndSumsRule10(List<CoupleResult> candidates, int placeToFind, int participantsCount)
        {
            foreach (var coupleResult in candidates)
            {
                if (coupleResult.NumberPlaces == null)
                {
                    coupleResult.NumberPlaces = new List<int>();
                    coupleResult.Sums = new List<double>();

                    for (var i = 0; i < participantsCount; i++)
                    {
                        coupleResult.NumberPlaces.Add(0);
                        coupleResult.Sums.Add(0);
                        coupleResult.CalculationUsed.Add(true);
                    }
                }

                coupleResult.NumberPlaces[placeToFind - 1] = coupleResult.Marks.Count(m => m <= placeToFind);
                coupleResult.Sums[placeToFind - 1] = coupleResult.Marks.Where(m => m <= placeToFind).Sum(m => m);
            }
        }

        public static int CalculateSkating(int maiority, int placeToGive, int maxPlaceToSearch, List<SkatingModelBase> coupleResults, bool devidePlaces, bool calculateRule10, bool calculateRule11)
        {
            var placeStartSearch = placeToGive;
            List<SkatingModelBase> candidates = null;

            while (placeToGive <= maxPlaceToSearch)
            {
                var placeToSearch = placeStartSearch;
                candidates = null;

                while (true)
                {
                    if (candidates == null)
                    {
                        if (coupleResults.All(c => c.Place > 0.1))
                        {
                            return coupleResults.Max(m => Convert.ToInt32(m.Place)) + 1;
                        }

                        candidates =
                            coupleResults.Where(c => c.NumberPlaces[placeToSearch - 1] >= maiority && c.Place < 0.1)
                                .ToList();
                    }

                    if (!candidates.Any())
                    {
                        candidates = null;
                        placeToSearch++;
                        if (placeToSearch > maxPlaceToSearch)
                        {
                            return 0;
                        }

                        continue;
                    }

                    if (candidates.Count() == 1)
                    {
                        candidates.First().Place = placeToGive;
                        candidates.First().PlaceFrom = placeToGive;
                        candidates.First().PlaceTo = placeToGive;
                        candidates.First().IsResolved = true;
                        candidates.First().Sums[placeToSearch - 1] = 0; // We do not need the sum to resolve this place
                        candidates.First().SumsUsed[placeToSearch - 1] = false;
                        placeToGive++;
                        placeStartSearch++;
                        // Clear all sums and places that have not been used:
                        ResetSkatingSumsAndPlaces(placeToSearch + 1, maxPlaceToSearch, candidates.First());

                        if (calculateRule11)
                        {
                            return placeToGive;
                        }

                        break;
                    }

                    // Handle more then one maiority:
                    // Check the sums:
                    var placeResolved = TryResolvePlace(candidates, placeToSearch, placeToGive, maxPlaceToSearch, calculateRule10, calculateRule11);
                    if (placeResolved.Item1 > 0)
                    {
                        placeToGive = placeResolved.Item1;
                        placeStartSearch++;
                        if (calculateRule11)
                        {
                            foreach (var skatingModelBase in coupleResults)
                            {
                                ResetSkatingSumsAndPlaces(placeResolved.Item2 + 1, maxPlaceToSearch, skatingModelBase);
                            }

                            // reset the sums and places of those couples without mayority:
                            foreach (var skatingModel in coupleResults.Where(c => !candidates.Any(c2 => c2.Participant.Id == c.Participant.Id)))
                            {
                                ResetSkatingSumsAndPlaces(placeToSearch + 1, maxPlaceToSearch, skatingModel);
                            }
                            // We resolved one place, so in rule 11 we will exit and continue again with rule 10
                            return placeToGive;
                        }
                        break;
                    }

                    if (placeToSearch == maxPlaceToSearch)
                    {
                        var ordered = candidates.OrderBy(c => c.Sums[placeToSearch - 1]).ToList();
                        var sameSum = candidates.Where(c => c.Sums[placeToSearch - 1] == ordered[0].Sums[placeToSearch - 1]).ToList();

                        if (devidePlaces)
                        {
                            DevidePlaces(sameSum, placeToGive);
                        }
                        else
                        {
                            // So, nothing we can do here, we devide the places:
                            foreach (var coupleResult in sameSum)
                            {
                                coupleResult.Place = placeToGive;
                                coupleResult.PlaceFrom = placeToGive;
                                coupleResult.PlaceTo = placeToGive + candidates.Count - 1;
                                coupleResult.IsResolved = true;
                            }
                            if (coupleResults.All(r => r.Place > 0))
                            {
                                return placeToGive + sameSum.Count;
                            }

                        }
                        placeToGive += sameSum.Count();
                        placeStartSearch++;

                        break;
                    }

                    placeToSearch++;
                }
            }

            return placeToGive;
        }

        private static Tuple<int, int> TryResolvePlace(IEnumerable<SkatingModelBase> candidates, int placeToSearch, int placeToGive, int participantCount, bool calculateRul10, bool calculateRule11)
        {
            if (candidates.Count() == 1)
            {
                candidates.First().PlaceFrom = placeToGive;
                candidates.First().PlaceTo = placeToGive;
                candidates.First().Place = placeToGive;
                candidates.First().IsResolved = true;

                return new Tuple<int, int>(placeToGive + 1, placeToSearch);
            }

            var orderdByPlacings = candidates.OrderByDescending(c => c.NumberPlaces[placeToSearch - 1]).ThenBy(o => o.Sums[placeToSearch - 1]).GroupBy(e => new Tuple<int, double>(e.NumberPlaces[placeToSearch - 1], e.Sums[placeToSearch - 1])).ToList();
            
            var placesResolved = 0;

            if (calculateRul10)
            {
                // we check only the first
                if (orderdByPlacings.Any() && orderdByPlacings.First().Count() == 1)
                {
                    var model = orderdByPlacings.First().First();
                    SetPlaceAndViewModel(placeToSearch, placeToGive, model);

                    placeToGive++;
                    placesResolved++;

                    // only if there is on other couple we set the place for this one
                    // if it would be more then 2, we continue with rule 10 but search one place more
                    //if (orderdByPlacings.Count() == 2 && orderdByPlacings.Last().Count() == 1 && calculateRul10)
                    //{
                    //    model = orderdByPlacings.Last().First();

                    //    SetPlaceAndViewModel(placeToSearch, placeToGive, model);

                    //    placeToGive++;
                    //    placesResolved++;
                    //}
                }

                return new Tuple<int, int>(placeToGive, placeToSearch);
            }

            foreach (var group in orderdByPlacings)
            {
                if (group.Count() == 1)
                {
                    var model = group.First();
                    SetPlaceAndViewModel(placeToSearch, placeToGive, model);

                    placeToGive++;
                    placesResolved++;

                    // in rule 11 we only give one place and then continue with rule 10
                    if (calculateRule11)
                    {
                        return new Tuple<int, int>(placeToGive, placeToSearch);
                    }
                }
                else
                {
                    if (placesResolved > 0 && calculateRule11)
                    {
                        return new Tuple<int, int>(placeToGive, placeToSearch);
                    }

                    if (participantCount == placeToSearch)
                    {
                        if (!calculateRul10)
                        {
                            // Devide places only if no other chance
                            foreach (var skating in group)
                            {
                                skating.IsResolved = true;
                                skating.PlaceFrom = placeToGive;
                                skating.PlaceTo = placeToGive + group.Count() - 1;
                                skating.Place = Convert.ToDouble(skating.PlaceFrom + skating.PlaceTo) / 2;
                            }
                        }
                        else
                        {
                            return new Tuple<int, int>(0, placeToSearch);
                        }
                    }
                    else
                    {
                        var placed = TryResolvePlace(group.ToList(), placeToSearch + 1, placeToGive, participantCount, calculateRul10, calculateRule11);
                        if (calculateRul10 || calculateRule11)
                        {
                            return placed.Item1 > 0 ? placed : new Tuple<int, int>(placeToGive, placeToSearch);
                        }
                    }

                    placeToGive += group.Count();
                }
            }

            
            return new Tuple<int, int>(placeToGive, placeToSearch);
        }

        private static void SetPlaceAndViewModel(int placeToSearch, int placeToGive, SkatingModelBase model)
        {
            model.IsResolved = true;
            model.PlaceFrom = placeToGive;
            model.PlaceTo = placeToGive;
            model.Place = placeToGive;
            for (var i = placeToSearch; i < model.NumberPlaces.Count; i++)
            {
                model.NumberPlaces[i] = 0;
            }
        }

        private static int ApplyPlaces(int placeToGive, int maxPlaceToSearch, List<SkatingModelBase> ordered, int placeToSearch)
        {
            foreach (var coupleResult in ordered)
            {
                coupleResult.Place = placeToGive;
                coupleResult.PlaceFrom = placeToGive;
                coupleResult.PlaceTo = placeToGive;
                coupleResult.IsResolved = true;

                ResetSkatingSumsAndPlaces(placeToSearch + 1, maxPlaceToSearch, coupleResult);
                placeToGive++;
            }

            return placeToGive;
        }

        public static void ResetSkatingSumsAndPlaces(int placeToSearch, int participantCount, SkatingModelBase candidate)
        {
            for (var i = placeToSearch - 1; i < participantCount; i++)
            {
                candidate.NumberPlaces[i] = 0;
                candidate.Sums[i] = 0;
                candidate.CalculationUsed[i] = false;
                candidate.SumsUsed[i] = false;
            }
        }

        private static void DevidePlaces(IEnumerable<SkatingModelBase> candidates, int placeToGive)
        {
            var points = (double)(placeToGive + placeToGive + candidates.Count() - 1) / 2d;

            foreach (var coupleResult in candidates)
            {
                coupleResult.Place = points;
                coupleResult.IsResolved = true;
            }
        }
    }
}
