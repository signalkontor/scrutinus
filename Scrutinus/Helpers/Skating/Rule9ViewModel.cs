﻿// // TPS.net TPS8 Helpers
// // Rule9ViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using DataModel.Models;

namespace Helpers.Skating
{
    public class Rule9ViewModel
    {
        public Rule9ViewModel(Participant participant, IEnumerable<double> points)
        {
            this.PointsInDances = new List<double>(points);
            this.Sum = this.PointsInDances.Sum(p => p);
            this.Participant = participant;
        }

        public Participant Participant { get; private set; }

        public List<double> PointsInDances { get; private set; }

        public double Sum { get; private set; }

        public int Place { get; set; }

        public bool IsResolved { get; set; }
    }
}
