// // TPS.net TPS8 Helpers
// // DanceSkatingViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataModel.Models;
using GalaSoft.MvvmLight;

namespace Helpers.Skating
{
    public class DanceSkatingViewModel : ViewModelBase
    {
        private readonly IEnumerable<Official> judges;
        private readonly IEnumerable<Participant> participants;


        public DanceSkatingViewModel(
            ScrutinusContext context,
            Round round,
            IEnumerable<Participant> participants,
            DanceInRound dance,
            IEnumerable<Official> judges)
        {
            this.participants = participants;
            this.judges = judges;
            this.DanceInRound = dance;
            this.CoupleResults = new List<CoupleResultFinal>();

            foreach (var participant in participants)
            {
                this.CoupleResults.Add(new CoupleResultFinal(context, round, participant, participants.Count(), dance.Dance, judges));
            }
        }

        public List<CoupleResultFinal> CoupleResults { get; set; }

        public DanceInRound DanceInRound { get; set; }

        public bool HasAllMarks()
        {
            return this.CoupleResults.All(c => c.Marks.All(m => m.Mark > 0));
        }

        public void DoCalculations()
        {
            Parallel.ForEach(this.CoupleResults, new Action<CoupleResultFinal>((c) => c.DoCalculations()));

            var maiority = (int)this.judges.Count() / 2 + 1;

            // Find the places based on maiority:
            SkatingHelper.CalculateSkating(maiority, 1, this.participants.Count(), this.CoupleResults.Cast<SkatingModelBase>().ToList(), true, false, false);
            // all place counts < miority:
            foreach (var coupleResultFinal in this.CoupleResults)
            {
                for (var i = 0; i < coupleResultFinal.NumberPlaces.Count; i++)
                {
                    if (coupleResultFinal.NumberPlaces[i] < maiority)
                    {
                        coupleResultFinal.NumberPlaces[i] = 0;
                        coupleResultFinal.CalculationUsed[i] = false;
                    }
                }
            }
        }

        public void RaiseChangedEvent()
        {
            foreach (var coupleResultFinal in this.CoupleResults)
            {
                coupleResultFinal.RaisePropertyChangedEvent();
            }

            this.RaisePropertyChanged(string.Empty);
        }
    }
}