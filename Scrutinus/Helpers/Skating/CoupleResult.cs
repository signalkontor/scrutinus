﻿// // TPS.net TPS8 Helpers
// // CoupleResult.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using DataModel.Models;

namespace Helpers.Skating
{
    public class CoupleResultFinal : SkatingModelBase
    {
        public CoupleResultFinal(ScrutinusContext context, Round round, Participant participant, int totalCouplesCount, Dance dance, IEnumerable<Official> judges)
        {
            this.Participant = participant;
            this.Marks = new List<Marking>(totalCouplesCount);
            this.NumberPlaces = new List<int>(totalCouplesCount);
            this.Sums = new List<double>(totalCouplesCount);
            this.CalculationUsed = new List<bool>();
            this.SumsUsed = new List<bool>();

            var needSave = false;

            for (var i = 0; i < totalCouplesCount; i++)
            {
                this.NumberPlaces.Add(0);
                this.Sums.Add(0);
                this.CalculationUsed.Add(true);
                this.SumsUsed.Add(true);
            }

            if (context == null)
            {
                return;
            }

            var marks = context.Markings.Where(m => m.Round.Id == round.Id).ToList();
            var newMarks = new List<Marking>();

            foreach (var judge in judges)
            {
                var mark = marks.FirstOrDefault(m => m.Dance.Id == dance.Id && m.Judge.Id == judge.Id && m.Participant.Id == participant.Id && m.Round.Id == round.Id);

                if (mark == null)
                {
                    mark = new Marking(participant, round, dance, judge) { Mark = 0 };
                    newMarks.Add(mark);
                    needSave = true;
                }

                this.Marks.Add(mark);
            }

            context.Markings.AddRange(newMarks);

            if (needSave)
            {
                context.SaveChanges();
            }
            
        }

        public List<Marking> Marks { get; set; }

        public CoupleResultFinal ItSelf {
            get
            {
                return this;
            }
        }

        public void DoCalculations()
        {
            for (var i = 1; i <= this.NumberPlaces.Count; i++)
            {
                this.NumberPlaces[i - 1] = this.Marks.Count(p => p.Mark <= i);
                this.Sums[i - 1] = this.Marks.Where(m => m.Mark <= i).Sum(p => p.Mark);
            }
        }

        public string GetSkatingLabel(int index)
        {
            if (this.NumberPlaces[index] == 0)
            {
                return string.Empty;
            }

            if (this.SumsUsed[index])
            {
                return string.Format("{0} ({1}) ", this.NumberPlaces[index], this.Sums[index]);
            }
            else
            {
                return string.Format("{0} ", this.NumberPlaces[index]);
            }
        }

        public void RaisePropertyChangedEvent()
        {
            this.RaisePropertyChanged("ItSelf");
        }
    }

    public class CoupleResult : SkatingModelBase
    {
        public CoupleResult(Participant participant, int totalCouplesCount, int judgesCount)
        {
            this.Participant = participant;
            this.Marks = new List<double>(totalCouplesCount);
            this.NumberPlaces = new List<int>(totalCouplesCount);
            this.Sums = new List<double>(totalCouplesCount);
            this.SumsUsed = new List<bool>();
            this.CalculationUsed = new List<bool>();

            for (var i = 0; i < judgesCount; i++)
            {
                this.Marks.Add(0);
            }

            for (var i = 0; i < totalCouplesCount; i++)
            {
                
                this.NumberPlaces.Add(0);
                this.Sums.Add(0);
                this.CalculationUsed.Add(true);
                this.SumsUsed.Add(true);
            }
        }

        public List<double> Marks { get; set; }

        public void DoCalculations()
        {
            for (var i = 1; i <= this.NumberPlaces.Count; i++)
            {
                this.NumberPlaces[i - 1] = this.Marks.Count(p => p <= i);
                this.Sums[i - 1] = this.Marks.Where(m => m <= i).Sum(p => p);
                this.CalculationUsed[i - 1] = true;
            }
        }
    }
}
