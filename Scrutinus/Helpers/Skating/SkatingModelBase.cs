﻿// // TPS.net TPS8 Helpers
// // SkatingModelBase.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.ComponentModel;
using DataModel.Models;

namespace Helpers.Skating
{
    public class SkatingModelBase : INotifyPropertyChanged
    {
        private bool isResolved;

        private bool isResolvedRule11;

        private double place;
        private int placeFrom;

        private int placeTo;

        public Participant Participant { get; set; }

        public List<int> NumberPlaces { get; set; }

        public List<double> Sums { get; set; }

        public List<bool> CalculationUsed { get; set; }

        public List<bool> SumsUsed { get; set; }

        public int PlaceFrom
        {
            get
            {
                return this.placeFrom;
            }
            set
            {
                this.placeFrom = value;
                this.RaisePropertyChanged(nameof(this.PlaceFrom));
            }
        }

        public int PlaceTo
        {
            get
            {
                return this.placeTo;
            }
            set
            {
                this.placeTo = value;
                this.RaisePropertyChanged(nameof(this.PlaceTo));
            }
        }

        public double Place
        {
            get
            {
                return this.place;
            }
            set
            {
                this.place = value;
                this.RaisePropertyChanged(nameof(this.Place));
            }
        }

        public bool IsResolved
        {
            get
            {
                return this.isResolved;
            }
            set
            {
                this.isResolved = value;
                this.RaisePropertyChanged(nameof(this.IsResolved));
            }
        }

        public bool IsResolvedRule11
        {
            get { return this.isResolvedRule11; }
            set
            {
                this.isResolvedRule11 = value; 
                this.RaisePropertyChanged(nameof(this.IsResolvedRule11));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void RaisePropertyChanged(string propertyName)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
