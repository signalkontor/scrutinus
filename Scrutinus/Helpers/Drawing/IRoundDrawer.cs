﻿// // TPS.net TPS8 Helpers
// // IRoundDrawer.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using DataModel.Models;

namespace General.Drawing
{
    public interface IRoundDrawer
    {
        Dictionary<string, List<List<Participant>>> CreateDrawing(
            List<Participant> Participants,
            List<DanceInRound> dances,
            int HeatCount);
    }
}