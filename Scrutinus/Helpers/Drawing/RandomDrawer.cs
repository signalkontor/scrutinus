﻿// // TPS.net TPS8 Helpers
// // RandomDrawer.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Linq;
using DataModel.Models;

namespace General.Drawing
{
    public class RandomDrawer : IRoundDrawer
    {
        public Dictionary<string, List<List<Participant>>> CreateDrawing(
            List<Participant> participants,
            List<DanceInRound> dances,
            int numberOfHeats)
        {
            // Randomly generate a Round Drawing with required Heats
            if (numberOfHeats > participants.Count)
            {
                numberOfHeats = participants.Count;
            }

            var heatsize = participants.Count / numberOfHeats;
            var numFullGroups = numberOfHeats;

            if (participants.Count % numberOfHeats > 0)
            {
                // We have a modulo so add Heatsize by one
                heatsize += 1;
                numFullGroups = participants.Count % numberOfHeats;
            }

            var random = new System.Random();
            List<Participant> lastHeat = null;

            var result = new Dictionary<string, List<List<Participant>>>();
            if (dances != null)
            {
                List<Participant> list = null;
                foreach (var dance in dances)
                {
                    var DanceList = new List<List<Participant>>();
                    result.Add(dance.Dance.ShortName, DanceList);

                    var shuffleCount = random.Next(5, 15);

                    for (var i = 0; i < shuffleCount; i++)
                    {
                        list = this.Shuffle(participants.ToList(), random).ToList();
                    }

                    var index = 0;

                    var numberOfHeatsInDance = numberOfHeats;
                    if (dance.IsSolo)
                    {
                        foreach (var participant in list)
                        {
                            var heatList = new List<Participant> { participant };
                            DanceList.Add(heatList);
                        }
                    }
                    else
                    {
                        for (var h = 0; h < numberOfHeatsInDance; h++)
                        {
                            var HeatList = new List<Participant>();
                            var heatSize = h < numFullGroups ? heatsize : heatsize - 1;

                            if (h == 0 && lastHeat != null)
                            {
                                var cindex = 0;

                                while (HeatList.Count < heatSize)
                                {
                                    var participant = list[cindex];
                                    if(lastHeat.All(p => p.Number != participant.Number))
                                    {
                                        HeatList.Add(participant);
                                        list.Remove(participant);
                                    }
                                    else
                                    {
                                        cindex++;
                                    }
                                }
                            }
                            else
                            {
                                HeatList.AddRange(list.GetRange(index, heatSize));
                                index += h < numFullGroups ? heatsize : heatsize - 1;
                            }
                            
                            DanceList.Add(HeatList.OrderBy(p => p.Number).ToList());

                            if (h == numberOfHeatsInDance - 1 && numberOfHeatsInDance > 2)
                            {
                                lastHeat = HeatList;
                            }
                        }
                    }

                    
                }
            }
            else
            {
                var DanceList = new List<List<Participant>>();
                result.Add("All Dances", DanceList);

                var list = this.Shuffle(participants.ToList(), new System.Random()).ToList();
                var index = 0;
                for (var h = 0; h < numberOfHeats; h++)
                {
                    var HeatList = new List<Participant>();

                    HeatList.AddRange(list.GetRange(index, h < numFullGroups ? heatsize : heatsize - 1));
                    index += h < numFullGroups ? heatsize : heatsize - 1;

                    DanceList.Add(HeatList.OrderBy(p => p.Number).ToList());
                }
            }
            return result;
        }

        private List<Participant> GetRandomOrder(List<Participant> list)
        {
            var randomOrder = new List<Participant>();
            var rnd = new System.Random((int)DateTime.Now.Ticks);

            while (list.Count > 0)
            {
                if (list.Count == 1)
                {
                    randomOrder.Add(list[0]);
                    foreach (var element in randomOrder)
                    {
                        Console.Write(" " + element.Number);
                    }
                    Console.WriteLine("");
                    return randomOrder;
                }

                var index = rnd.Next(0, list.Count - 1);
                Console.WriteLine(index);
                randomOrder.Add(list[index]);
                list.RemoveAt(index);
            }

            return randomOrder;
        }

        public IEnumerable<T> Shuffle<T>(IEnumerable<T> source, System.Random rng)
        {
            var elements = source.ToArray();
            // Note i > 0 to avoid final pointless iteration
            for (var i = elements.Length - 1; i > 0; i--)
            {
                // Swap element "i" with a random earlier element it (or itself)
                var swapIndex = rng.Next(i + 1);
                var tmp = elements[i];
                elements[i] = elements[swapIndex];
                elements[swapIndex] = tmp;
            }
            // Lazily yield (avoiding aliasing issues etc)
            foreach (var element in elements)
            {
                yield return element;
            }
        }
    }
}