﻿// // TPS.net TPS8 Helpers
// // AscendingDrawer.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using DataModel.Models;

namespace General.Drawing
{
    public class OrderedDrawer : IRoundDrawer
    {
        private readonly bool _orderAscending = true;

        public OrderedDrawer(bool Ascending)
        {
            this._orderAscending = Ascending;
        }

        public Dictionary<string, List<List<Participant>>> CreateDrawing(
            List<Participant> Participants,
            List<DanceInRound> dances,
            int HeatCount)
        {
            var heatsize = Participants.Count / HeatCount;
            var numFullGroups = heatsize;
            if (Participants.Count % HeatCount > 0)
            {
                // We have a modulo so add Heatsize by one
                heatsize += 1;
                numFullGroups = Participants.Count % HeatCount;
            }

            var source = this._orderAscending
                              ? Participants.OrderBy(p => p.Number).ToList()
                              : Participants.OrderByDescending(p => p.Number).ToList();

            var result = new Dictionary<string, List<List<Participant>>>();
            result.Add("All Dances", new List<List<Participant>>());
            var coupleIndex = 0;
            for (var i = 0; i < HeatCount; i++)
            {
                var groupHeatSize = i < numFullGroups ? heatsize : heatsize - 1;
                var list = new List<Participant>();
                while (list.Count < groupHeatSize && coupleIndex < source.Count())
                {
                    list.Add(source[coupleIndex]);
                    coupleIndex++;
                }

                result["All Dances"].Add(list.OrderBy(p => p.Number).ToList());
            }

            return result;
        }
    }
}