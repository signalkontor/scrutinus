﻿// // TPS.net TPS8 Helpers
// // Random.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;

namespace General.Drawing
{
    public class Random
    {
        public static int GetRandomNumbers(int count, byte min, byte max)
        {
            // Zufallszahlen erzeugen
            var csp = new RNGCryptoServiceProvider();
            var numbers = new Byte[count];
            csp.GetBytes(numbers);

            // Die Zahlen umrechnen
            double divisor = 256F / (max - min + 1);
            if (min > 0 || max < 255)
            {
                for (var i = 0; i < count; i++)
                {
                    numbers[i] = (byte)(numbers[i] / divisor + min);
                }
            }

            return (int)numbers[0];
        }

        public static int GetRandom(int max)
        {
            if (max < 256)
            {
                return GetRandomNumbers(1, 0, (byte)max);
            }
            else
            {
                var rnd = new System.Random();
                return rnd.Next(max);
            }
        }

        private static void Shuffle<T>(IList<T> list)
        {
            var provider = new RNGCryptoServiceProvider();
            var n = list.Count;
            while (n > 1)
            {
                var box = new byte[1];
                do
                {
                    provider.GetBytes(box);
                } while (!(box[0] < n * (Byte.MaxValue / n)));
                var k = box[0] % n;
                n--;
                var value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        public static List<T> GetRandomList<T>(IEnumerable<T> list)
        {
            var newList = list.ToList();
            Shuffle(newList);

            return newList;
        }
    }
}