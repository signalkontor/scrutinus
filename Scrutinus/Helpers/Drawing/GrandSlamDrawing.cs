﻿// // TPS.net TPS8 Helpers
// // GrandSlamDrawing.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using DataModel.Models;

namespace General.Drawing
{
    public class GrandSlamDrawing : IRoundDrawer
    {
        public Dictionary<string, List<List<Participant>>> CreateDrawing(
            List<Participant> Participants,
            List<DanceInRound> dances,
            int HeatCount)
        {
            return this.CreateFinalDrawing(Participants, dances);
        }

        private List<DanceInRound> PickSoloDances(List<DanceInRound> dances)
        {
            return dances.Where(d => d.IsSolo).ToList();
        }

        internal Dictionary<string, List<List<Participant>>> CreateFinalDrawing(
            List<Participant> couples,
            List<DanceInRound> dances)
        {
            // we pick the solo dances:
            var solos = this.PickSoloDances(dances);

            var order = new DanceInRound[dances.Count];
            var index = 0;

            foreach (var dance in dances.OrderBy(d => d.SortOrder))
            {
                order[index] = dance;
                index++;
            }

            return order.ToDictionary(
                dance => dance.Dance.ShortName,
                dance =>
                solos.Any(d => d.Dance.ShortName == dance.Dance.ShortName)
                    ? this.Create(couples, couples.Count, dance)
                    : this.Create(couples, 1, dance));
        }

        private List<List<Participant>> Create(List<Participant> couples, int heats, DanceInRound dance)
        {
            var res = new List<List<Participant>>();
            // get a random list
            var random = Random.GetRandomList<Participant>(couples);

            var size = couples.Count / heats;
            var numExtraSized = couples.Count % heats;

            for (var i = 0; i < heats; i++)
            {
                var heat = new List<Participant>();

                res.Add(heat);

                if (numExtraSized > i)
                {
                    heat.AddRange(random.Take(size + 1));
                    random.RemoveRange(0, size + 1);
                }
                else
                {
                    heat.AddRange(random.Take(size));
                    random.RemoveRange(0, size);
                }
                // Sortieren
                heat = heat.OrderBy(c => c.Id).ToList();
            }
            return res;
        }
    }
}