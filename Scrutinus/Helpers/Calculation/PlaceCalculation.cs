﻿// // TPS.net TPS8 Helpers
// // PlaceCalculation.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using DataModel.Models;

namespace Helpers.Calculation
{
    public static class PlaceCalculation
    {
        public static IEnumerable<Qualified> CalculatePlaces(IEnumerable<Qualified> qualifieds, int placeOffset)
        {
            foreach (var qualified in qualifieds)
            {
                qualified.PlaceFrom = 0;
                qualified.PlaceTo = 0;
            }

            // Calculate Places:
            var sorted = qualifieds.OrderByDescending(q => q.Points).ToList();
            
            var placefrom = 1;
            for (var i = 0; i < sorted.Count - 1; i++)
            {
                sorted[i].PlaceFrom = placefrom + placeOffset;
                sorted[i].PlaceTo = placefrom + placeOffset;
                if (sorted[i + 1].Points == sorted[i].Points)
                {
                    var index = i + 1;
                    var placecount = 1;
                    while (index < sorted.Count && sorted[i].Points == sorted[index].Points)
                    {
                        index++;
                        placecount++;
                    }
                    for (var j = i; j < index; j++)
                    {
                        sorted[j].PlaceFrom = placefrom + placeOffset;
                        sorted[j].PlaceTo = placefrom + placecount + placeOffset - 1;
                    }
                    i = index - 1;
                    placefrom += placecount - 1;
                }
                placefrom++;
            }
            if (sorted[sorted.Count - 1].PlaceFrom == 0)
            {
                sorted[sorted.Count - 1].PlaceFrom = placefrom + placeOffset;
                sorted[sorted.Count - 1].PlaceTo = placefrom + placeOffset;
            }

            return sorted;
        }
    }
}
