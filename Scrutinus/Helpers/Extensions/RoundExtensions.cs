﻿// // TPS.net TPS8 Helpers
// // RoundExtensions.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Linq;
using DataModel;
using DataModel.Models;

namespace General.Extensions
{
    public static class RoundExtensions
    {
        public static string PrintTitle(this Round round)
        {
            var title = "";

            switch (round.RoundType.Id)
            {
                case RoundTypes.Final:
                    title = "Final";
                    break;
                case RoundTypes.Redance:
                    title = "Redance";
                    break;
                default:
                    // check: did we had a redance?
                    if (
                        round.Competition.Rounds.Any(
                            r => r.RoundType.Id == RoundTypes.Redance && r.Number < round.Number))
                    {
                        title = round.Number - 1 + ". Round";
                    }
                    else
                    {
                        title = round.Number + ". Round";
                    }

                    break;
            }

            return title;
        }
    }
}