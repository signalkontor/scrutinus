﻿// // TPS.net TPS8 Helpers
// // TaskExtensions.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Helpers.Extensions
{
    public static class TaskExtensions
    {
        public static T WaitWithoutBlockingUI<T>(this Task<T> task, Dispatcher dispatcher)
        {
            while (!task.IsCompleted)
            {
                dispatcher.Invoke(DispatcherPriority.ApplicationIdle, new Action(() => { }));
            }

            return task.Result;
        }

        public static void WaitWithoutBlockingUI(this Task task, Dispatcher dispatcher)
        {
            while (!task.IsCompleted)
            {
                dispatcher.Invoke(DispatcherPriority.ApplicationIdle, new Action(() => { }));
            }
        }
    }
}
