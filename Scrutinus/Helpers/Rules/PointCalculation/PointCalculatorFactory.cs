﻿// // TPS.net TPS8 Helpers
// // PointCalculatorFactory.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using DataModel.Models;

namespace General.Rules.PointCalculation
{
    public static class PointCalculatorFactory
    {
        public static IRankingPointCalculator GetCalculator(Competition competition)
        {
            if (competition.CompetitionType.Federation == "DTV")
            {
                return new DTVPointCalculator();
            }

            return new NoPointsCalculator();
        }
    }
}