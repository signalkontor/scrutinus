﻿// // TPS.net TPS8 Helpers
// // IPointCalculator.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using DataModel.Models;

namespace General.Rules.PointCalculation
{
    public interface IRankingPointCalculator
    {
        void CalculateRankingPoints(IEnumerable<Participant> participants, ScrutinusContext context);
    }
}