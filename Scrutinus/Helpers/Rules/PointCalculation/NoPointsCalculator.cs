﻿// // TPS.net TPS8 Helpers
// // NoPointsCalculator.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using DataModel.Models;

namespace General.Rules.PointCalculation
{
    public class NoPointsCalculator : IRankingPointCalculator
    {
        public void CalculateRankingPoints(IEnumerable<Participant> participants, ScrutinusContext context)
        {
            foreach (var participant in participants)
            {
                participant.RankingPoints = 0;
            }
        }
    }
}