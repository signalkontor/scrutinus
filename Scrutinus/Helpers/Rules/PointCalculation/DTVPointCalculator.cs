﻿// // TPS.net TPS8 Helpers
// // DTVPointCalculator.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Linq;
using DataModel;
using DataModel.Models;

namespace General.Rules.PointCalculation
{
    public class DTVPointCalculator : IRankingPointCalculator
    {
        public void CalculateRankingPoints(IEnumerable<Participant> participants, ScrutinusContext context)
        {
            var droppedOut = participants.Where(p => p.State == CoupleState.DroppedOut).ToList();
          
            // Todo: Point calcuation in combined competitions
            foreach (var participant in droppedOut)
            {
                participant.ClimbUp = false;
                // do we have a climbup for this couple?
                var climbup = context.ClimbUps.FirstOrDefault(c => c.ClimbupType == ClimbupType.Regular && c.Competition.Id == participant.Competition.Id && c.Couple.Id == participant.Couple.Id);
                if(climbup != null)
                {
                    context.ClimbUps.Remove(climbup);
                    climbup = null;
                }

                var startbuch = participant.Couple.Startbooks.SingleOrDefault(s => s.Section.Id == participant.Competition.Section.Id);

                // S-Class does not get points but winner couple from A-Class (if any)
                // BSW Couples get points but cannot climb up
                if (!ParticipantGetsPoints(participant, startbuch))
                {
                    if (participant.Competition.CombinedExternalId != null)
                    {
                        var betterCouples = participants.Count(p =>
                            p.PlaceTo < participant.PlaceFrom &&
                            p.AgeGroup.OrderWhenCombined < participant.AgeGroup.OrderWhenCombined);

                        participant.PlaceFromOwnCompetition = participant.PlaceFrom - betterCouples;
                        participant.PlaceToOwnCompetition = participant.PlaceTo - betterCouples;
                    }
                    participant.RankingPoints = null;
                    continue;
                }
                //if ((startbuch == null || startbuch.Class.Id == 5 || startbuch.NextClass == null || participant.TargetClass == null || participant.TargetClass.ClassShortName == "-") && participant.Class != null && participant.Class.Id != 7)
                //{
                //    participant.RankingPoints = null;
                //    continue;
                //}

                if (participant.Competition.CombinedExternalId == null)
                {
                    CalculatePointsUncombined(participants, participant);
                    participant.PlaceFromOwnCompetition = null;
                    participant.PlaceToOwnCompetition = null;
                }
                else
                {
                    var competition = participant.Competition;

                    var ageGroups = new List<AgeGroup> { competition.AgeGroup, competition.CombinedAgeGroup };
                    ageGroups = ageGroups.OrderBy(a => a.OrderWhenCombined).ToList();
                    //var agegroups = droppedOut.Select(p => p.AgeGroup).OrderBy(a => a.OrderWhenCombined).Distinct().ToList();
                    var classes = new List<Class> { competition.Class, competition.CombinedClass };
                    classes = classes.OrderBy(c => c.OrderWhenCombined).ToList();
                    // var classes = droppedOut.Select(p => p.Class).OrderBy(c => c.OrderWhenCombined).Distinct().ToList();

                    if (competition.CombinedAgeGroup.Id != competition.AgeGroup.Id)
                    {
                        this.CalculatePointsCombinedAgeGroup(participants, participant, ageGroups);
                    }
                    else
                    {
                        this.CalculatePointsCombinedClass(participants, participant, classes);
                    }
                }

                if (!participant.Competition.HasRankingPoints)
                {
                    participant.RankingPoints = 0;
                    participant.NewPoints = participant.OriginalPoints;
                    participant.NewPlacings = participant.OriginalPlacings;
                    UpdateAllParticipants(context, participant.Id, participant.OriginalPoints.HasValue ? participant.OriginalPoints.Value : 0, participant.OriginalPlacings.HasValue ? participant.OriginalPlacings.Value : 0, participant.Class, false, false);

                    continue;
                }

                // Check, if we have a placing:
                if (participant.Competition.HasRankingPoints && participant.RankingPoints >= startbuch.MinimumPoints && participant.PlaceFrom <= startbuch.PlacingsUpto)
                {
                    participant.NewPlacings = participant.OriginalPlacings + 1;
                }
                else
                {
                    participant.NewPlacings = participant.OriginalPlacings;
                }

                participant.NewPoints = participant.OriginalPoints + participant.RankingPoints;

                UpdateAllParticipants(context, participant.Id, 
                    (participant.OriginalPoints.HasValue ? participant.OriginalPoints.Value : 0) + (participant.RankingPoints.HasValue ? participant.RankingPoints.Value : 0), 
                    participant.NewPlacings.HasValue ? participant.NewPlacings.Value : participant.OriginalPlacings ?? 0, 
                    participant.Class, false, false);

                // Check, if we have a climb-up:
                if (participant.NewPlacings >= participant.TargetPlacings
                    && participant.OriginalPoints + participant.RankingPoints >= participant.TargetPoints
                    && participant.TargetClass != null
                    && participant.TargetClass.ClassShortName != "-")
                {
                    // Yes, this is a climb-up:
                    participant.ClimbUp = true;
                    participant.Startbuch.PrintLaufzettel = true;

                    climbup =
                        context.ClimbUps.FirstOrDefault(
                            c => c.Competition.Id == participant.Competition.Id && c.Couple.Id == participant.Couple.Id);

                    if (climbup == null)
                    {
                        climbup = new ClimbUp()
                                          {
                                              ClimbupType = ClimbupType.Regular,
                                              Competition = context.Competitions.Single(c => c.Id == participant.Competition.Id),
                                              Couple = context.Couples.Single(c => c.Id == participant.Couple.Id),
                                              NewClass = participant.TargetClass != null ? context.Classes.Single(s => s.Id == participant.TargetClass.Id) : null,
                                          };

                        context.ClimbUps.Add(climbup);
                    }

                    UpdateAllParticipants(context, participant.Id, 0, 0, climbup.NewClass, false, true);
                }

                context.SaveChanges();
            }
        }

        private void CalculatePointsCombinedClass(IEnumerable<Participant> participants, Participant participant, IEnumerable<Class> classes)
        {
            var betterCouples = participants.Count(p => p.PlaceTo < participant.PlaceFrom && p.Class.OrderWhenCombined > participant.Class.OrderWhenCombined);
            participant.PlaceFromOwnCompetition = participant.PlaceFrom - betterCouples;
            participant.PlaceToOwnCompetition = participant.PlaceTo - betterCouples;

            if (participant.Class.OrderWhenCombined <= classes.First().OrderWhenCombined)
            {
                CalculatePointsUncombined(participants, participant);
            }
            else
            {
                var participansOfSameClass = participants.Where(p => p.Class.OrderWhenCombined == participant.Class.OrderWhenCombined);
                CalculatePointsUncombined(participansOfSameClass, participant);
            }
        }

        private void CalculatePointsCombinedAgeGroup(IEnumerable<Participant> participants, Participant participant, IEnumerable<AgeGroup> agegroups)
        {
            var betterCouples = participants.Count(p => p.PlaceTo < participant.PlaceFrom && p.AgeGroup.OrderWhenCombined < participant.AgeGroup.OrderWhenCombined);
            participant.PlaceFromOwnCompetition = participant.PlaceFrom - betterCouples;
            participant.PlaceToOwnCompetition = participant.PlaceTo - betterCouples;

            if (participant.AgeGroup.OrderWhenCombined == agegroups.First().OrderWhenCombined)
            {
                var participantsOfSameAge = participants.Where(p => p.AgeGroup.OrderWhenCombined == participant.AgeGroup.OrderWhenCombined);
                CalculatePointsUncombined(participantsOfSameAge, participant);
            }
            else
            {
                CalculatePointsUncombined(participants, participant);
            }
        }


        private bool ParticipantGetsPoints(Participant participant, Startbuch startbuch)
        {
            // no points if no Startbuch
            if (startbuch == null)
            {
                return false;
            }

            // no points for non German couples
            if (participant.Couple.Country2 != "GER")
            {
                return false;
            }

            // no points for S-Class Couples
            if (startbuch.Class.ClassShortName == "S" || startbuch.Class.ClassShortName == "-")
            {
                return false;
            }

            if (participant.Class.ClassShortName == "S")
            {
                return false;
            }

            // no points if startbuch class is higher then competition 
            // Couple is YOUTH A but age group JUN II, dancing JUN II B
            if (participant.Competition.AgeGroup.ShortName == "JUG" || participant.Competition.AgeGroup.ShortName == "JUN II")
            {
                if (startbuch.Class.OrderWhenCombined > participant.Competition.Class.OrderWhenCombined)
                {
                    return false;
                }
            }

            return true;
        }

        private static void CalculatePointsUncombined(IEnumerable<Participant> participants, Participant participant)
        {
            var numberOfLowerCouples =
                participants.Count(p => p.PlaceFrom > participant.PlaceFrom && p.State == CoupleState.DroppedOut);

            participant.RankingPoints = numberOfLowerCouples <= 20 ? numberOfLowerCouples : 20;
        }

        public static void UpdateAllParticipants(
            ScrutinusContext context,
            int participantId,
            int points,
            int placings,
            Class newClass,
            bool checkEsvStartbuch,
            bool setLaufzettel = true)
        {
            var participant = context.Participants.Single(p => p.Id == participantId);

            if (participant.Class == null || participant.AgeGroup == null || participant.Startbuch == null)
            {
                return;
            }

            // Check if we need to print the laufzettels:
            if (participant.Startbuch != null && setLaufzettel)
            {
                participant.Startbuch.PrintLaufzettel = true;
            }

            // Step 1: Get Aufstiegstabelle to update targets for next climbup:
            var climbupentry =
                context.ClimbUpTables.FirstOrDefault(
                    c =>
                    c.Class != null && c.Class.Id == newClass.Id && 
                    c.AgeGroup.Id == participant.Startbuch.AgeGroup.Id
                    && c.LTV == participant.Couple.Region
                    && c.Section.Id == participant.Competition.Section.Id);

            var participants =
                context.Participants.Where(
                    p =>
                    p.Id != participantId &&
                    p.Couple.Id == participant.Couple.Id && p.State != CoupleState.Disqualified
                    && p.State != CoupleState.DroppedOut
                    && p.Competition.Section.Id == participant.Competition.Section.Id).ToList();

            foreach (var dancer in participants)
            {
                dancer.OriginalPoints = points;
                dancer.OriginalPlacings = placings;

                if (dancer.Class.Id != newClass.Id)
                {
                    dancer.State = CoupleState.Excused;
                }

                dancer.Class = newClass;
                dancer.TimeStampLastPointUpdate = DateTime.Now;

                if (climbupentry != null)
                {
                    dancer.TargetClass = climbupentry.TargetClass;
                    dancer.MinimumPoints = climbupentry.MinPoints;
                    dancer.PlacingsUpto = climbupentry.MinimumPlace;
                    dancer.TargetPlacings = climbupentry.Placings;
                    dancer.TargetPoints = climbupentry.Points;
                }
            }

            if (checkEsvStartbuch)
            {
                foreach (var startuch in context.Startbuecher.Where(
                            s =>
                            s.Couple.Id == participant.Couple.Id && s.Section.Id == participant.Competition.Section.Id))
                {
                    startuch.IsSelfGenerated = false;
                }
            }
        }
    }
}