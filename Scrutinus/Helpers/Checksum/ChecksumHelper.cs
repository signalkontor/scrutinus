﻿// // TPS.net TPS8 Helpers
// // ChecksumHelper.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using DataModel;
using DataModel.Models;

namespace Helpers.Checksum
{
    public static class ChecksumHelper
    {
        public static string CalculateChecksum(Round round, Official judge)
        {
            switch (round.MarksInputType)
            {
                case MarkingTypes.Marking:
                    return CalculateChecksumMarking(round, judge);
                case MarkingTypes.Skating:
                    return CalculatChecksumSkating(round, judge);
                case MarkingTypes.NewJudgingSystemV2:
                    return CalculateChecksumJs2(round, judge);
            }

            return "";
        }

        private static string CalculateChecksumJs2(Round round, Official judge)
        {
            return "";
        }

        private static string CalculatChecksumSkating(Round round, Official judge)
        {
            var marked = round.Markings.Where(m => m.Judge.Id == judge.Id).GroupBy(g => new { g.Dance.Id });

            var dances = round.Markings.GroupBy(m => m.Dance).Select(d => d.Key).OrderBy(d => d.DanceName);

            var resultBuffer = new List<byte>();

            foreach (var dance in dances)
            {
                var markingsForDance = marked.FirstOrDefault(m => m.Key.Id == dance.Id);

                if (markingsForDance == null)
                {   // No markings? ok, we continue ...
                    continue;
                }

                var markString = string.Join("", markingsForDance.OrderBy(o => o.Mark).Select(m => m.Participant.Number).Distinct());
                resultBuffer.AddRange(Hash(markString));
            }

            return CalculateChecksumHash(resultBuffer).ToString();
        }

        private static string CalculateChecksumMarking(Round round, Official judge)
        {
            var marked = round.Markings.Where(m => m.Mark == 1 && m.Judge.Id == judge.Id).GroupBy(g => new { g.Dance.Id });

            var dances = round.Markings.GroupBy(m => m.Dance).Select(d => d.Key).OrderBy(d => d.DanceName);

            var resultBuffer = new List<byte>();

            foreach (var dance in dances)
            {
                var markingsForDance = marked.FirstOrDefault(m => m.Key.Id == dance.Id);

                if (markingsForDance == null)
                {   // No markings? ok, we continue ...
                    continue;
                }

                var markString = string.Join("", markingsForDance.Select(p => p.Participant.Number).OrderBy(p => p).Distinct());
                resultBuffer.AddRange(Hash(markString));
            }

            return CalculateChecksumHash(resultBuffer).ToString();
        }

        private static byte[] Hash(string input)
        {
            using (var sha1 = new SHA1Managed())
            {
                return sha1.ComputeHash(Encoding.UTF8.GetBytes(input));
            }
        }

        private static int CalculateChecksumHash(List<byte> buffer)
        {
            using (var sha1 = new SHA1Managed())
            {
                var hash = sha1.ComputeHash(buffer.ToArray());

                var checksum = (hash[19] & 0xFF) | (hash[18] & 0xFF) << 8;

                return checksum % 10000;
            }
        }
    }
}
