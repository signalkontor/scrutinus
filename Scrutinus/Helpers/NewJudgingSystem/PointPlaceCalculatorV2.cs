﻿// // TPS.net TPS8 Helpers
// // PointPlaceCalculatorV2.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using DataModel.Models;

namespace General.NewJudgingSystem
{
    public class PointPlaceCalculatorV2 : PointCalculatorAbstract
    {
        protected override void CalculatePoints(
            ScrutinusContext context,
            Round round,
            List<JudgingComponent> components)
        {
            foreach (var qualified in round.Qualifieds)
            {
                var sumAll = 0d;
                foreach (var danceround in round.DancesInRound)
                {
                    var marks =
                        context.Markings.Where(
                            m =>
                            m.Round.Id == round.Id && m.MarkingComponent == 1
                            && m.Participant.Id == qualified.Participant.Id && m.Dance.Id == danceround.Dance.Id);
                    if (!marks.Any())
                    {
                        continue;
                    }
                    var area1 = (marks.Sum(s => s.Mark20) * 2 - marks.Min(s => s.Mark20) - marks.Max(s => s.Mark20)) / 4;
                    marks =
                        context.Markings.Where(
                            m =>
                            m.Round.Id == round.Id && m.MarkingComponent == 2
                            && m.Participant.Id == qualified.Participant.Id && m.Dance.Id == danceround.Dance.Id);
                    var area2 = (marks.Sum(s => s.Mark20) * 2 - marks.Min(s => s.Mark20) - marks.Max(s => s.Mark20)) / 4;
                    marks =
                        context.Markings.Where(
                            m =>
                            m.Round.Id == round.Id && m.MarkingComponent == 3
                            && m.Participant.Id == qualified.Participant.Id && m.Dance.Id == danceround.Dance.Id);
                    var area3 = (marks.Sum(s => s.Mark20) * 2 - marks.Min(s => s.Mark20) - marks.Max(s => s.Mark20)) / 4;
                    marks =
                        context.Markings.Where(
                            m =>
                            m.Round.Id == round.Id && m.MarkingComponent == 4
                            && m.Participant.Id == qualified.Participant.Id && m.Dance.Id == danceround.Dance.Id);
                    var area4 = (marks.Sum(s => s.Mark20) * 2 - marks.Min(s => s.Mark20) - marks.Max(s => s.Mark20)) / 4;

                    sumAll += area1 * (components.Count > 0 ? components[0].Weight : 1d)
                              + area2 * (components.Count > 1 ? components[1].Weight : 1d)
                              + area3 * (components.Count > 2 ? components[2].Weight : 1d)
                              + area4 * (components.Count > 3 ? components[3].Weight : 1d);
                }

                qualified.Points = sumAll;
                qualified.Participant.Points = sumAll;
            }
        }
    }
}