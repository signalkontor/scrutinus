﻿// // TPS.net TPS8 Helpers
// // PointPlaceCalculator2014.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Linq;
using DataModel.Models;

namespace General.NewJudgingSystem
{
    public class PointPlaceCalculator2014 : PointCalculatorAbstract
    {
        private double CalculateSumV3(List<double> marks)
        {
            var distance = marks[1] - marks[0];
            var lowerWeight = 1 / (1 + distance * distance);
            distance = marks[2] - marks[1];
            var upperWeight = 1 / (1 + distance * distance);
            var sum = marks[0] * lowerWeight + marks[1] + marks[2] * upperWeight;
            sum /= 1 + 1 * lowerWeight + 1 * upperWeight;

            return sum;
        }

        protected override void CalculatePoints(
            ScrutinusContext context,
            Round round,
            List<JudgingComponent> components)
        {
            foreach (var qualified in round.Qualifieds)
            {
                qualified.PointsPerDance = new Dictionary<int, double>();

                var sumAll = 0d;

                foreach (var danceround in round.DancesInRound)
                {
                    var marks =
                        context.Markings.Where(
                            m =>
                            m.Round.Id == round.Id && m.MarkingComponent == 1
                            && m.Participant.Id == qualified.Participant.Id && m.Dance.Id == danceround.Dance.Id)
                            .OrderBy(m => m.Mark20)
                            .Select(m => m.Mark20)
                            .ToList();
                    if (!marks.Any())
                    {
                        continue;
                    }
                    var area1 = this.CalculateSumV3(marks);
                    marks =
                        context.Markings.Where(
                            m =>
                            m.Round.Id == round.Id && m.MarkingComponent == 2
                            && m.Participant.Id == qualified.Participant.Id && m.Dance.Id == danceround.Dance.Id)
                            .OrderBy(m => m.Mark20)
                            .Select(m => m.Mark20)
                            .ToList();
                    var area2 = this.CalculateSumV3(marks);
                    marks =
                        context.Markings.Where(
                            m =>
                            m.Round.Id == round.Id && m.MarkingComponent == 3
                            && m.Participant.Id == qualified.Participant.Id && m.Dance.Id == danceround.Dance.Id)
                            .OrderBy(m => m.Mark20)
                            .Select(m => m.Mark20)
                            .ToList();
                    var area3 = this.CalculateSumV3(marks);
                    marks =
                        context.Markings.Where(
                            m =>
                            m.Round.Id == round.Id && m.MarkingComponent == 4
                            && m.Participant.Id == qualified.Participant.Id && m.Dance.Id == danceround.Dance.Id)
                            .OrderBy(m => m.Mark20)
                            .Select(m => m.Mark20)
                            .ToList();
                    var area4 = this.CalculateSumV3(marks);

                    area1 = Math.Round(area1, 3, MidpointRounding.AwayFromZero);
                    area2 = Math.Round(area2, 3, MidpointRounding.AwayFromZero);
                    area3 = Math.Round(area3, 3, MidpointRounding.AwayFromZero);
                    area4 = Math.Round(area4, 3, MidpointRounding.AwayFromZero);

                    var sumDance = area1 * (components.Count > 0 ? components[0].Weight : 1d)
                              + area2 * (components.Count > 1 ? components[1].Weight : 1d)
                              + area3 * (components.Count > 2 ? components[2].Weight : 1d)
                              + area4 * (components.Count > 3 ? components[3].Weight : 1d);

                    qualified.PointsPerDance.Add(danceround.Dance.Id, sumDance);

                    sumAll += sumDance;
                }

                sumAll = Math.Round(sumAll, 3, MidpointRounding.AwayFromZero);
                qualified.Points = sumAll;
                qualified.Participant.Points = sumAll;
            }
        }
    }
}