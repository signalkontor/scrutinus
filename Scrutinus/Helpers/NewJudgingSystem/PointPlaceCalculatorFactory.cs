﻿// // TPS.net TPS8 Helpers
// // PointPlaceCalculatorFactory.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using DataModel;
using Helpers.NewJudgingSystem;

namespace General.NewJudgingSystem
{
    public static class PointPlaceCalculatorFactory
    {
        public static PointCalculatorAbstract GetPointPlaceCalculator(NewJudgingSystemCaluclationType type, double maximumDeviation)
        {
            switch (type)
            {
                case NewJudgingSystemCaluclationType.V2014:
                    return new PointPlaceCalculator2014();
                case NewJudgingSystemCaluclationType.V2:
                    return new PointPlaceCalculatorV2();
                case NewJudgingSystemCaluclationType.JS3:
                    return new PointCalculatorJs3(maximumDeviation); 
            }

            return null;
        }
    }
}