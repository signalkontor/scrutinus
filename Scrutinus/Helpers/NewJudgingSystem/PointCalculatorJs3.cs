﻿// // TPS.net TPS8 Helpers
// // PointCalculatorJs3.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using DataModel.Models;
using General.NewJudgingSystem;

namespace Helpers.NewJudgingSystem
{
    public class PointCalculatorJs3 : PointCalculatorAbstract
    {
        private readonly double maximumDeviation;

        public PointCalculatorJs3(double maximumDeviation)
        {
            this.maximumDeviation = maximumDeviation;
        }

        protected override void CalculatePoints(ScrutinusContext context, Round round, List<JudgingComponent> components)
        {
            foreach (var qualified in round.Qualifieds)
            {
                var sumAll = 0d;

                foreach (var danceround in round.DancesInRound)
                {
                    var isSolo = danceround.IsSolo;
                    var marks = context.Markings.Where(m => m.Round.Id == round.Id && m.Dance.Id  == danceround.Dance.Id && m.Participant.Id == qualified.Participant.Id).ToList();

                    if (isSolo)
                    {
                        sumAll += this.CalculateSoloDance(marks, components, qualified.Participant, danceround);
                    }
                    else
                    {
                        sumAll += this.CalcuateGroupDance(marks, components, qualified.Participant, danceround);
                    }
                }

                qualified.Points = sumAll;

                qualified.Participant.Points = sumAll;
            }
        }

        private double CalcuateGroupDance(IList<Marking> marks, IList<JudgingComponent> components, Participant participant, DanceInRound danceInRound)
        {
            var result = CalculateComponent(1, marks, participant, danceInRound) * 2 * components[0].Weight
                        + CalculateComponent(2, marks, participant, danceInRound) * 2 * components[1].Weight;

            return result;
        }

        private double CalculateSoloDance(IList<Marking> marks, IList<JudgingComponent> components, Participant participant, DanceInRound danceInRound)
        {
            var result = CalculateComponent(1, marks, participant, danceInRound) * components[0].Weight
                         + CalculateComponent(2, marks, participant, danceInRound) * components[1].Weight
                         + CalculateComponent(3, marks, participant, danceInRound) * components[2].Weight
                         + CalculateComponent(4, marks, participant, danceInRound) * components[3].Weight;

            return result;
        }

        public double CalculateComponent(int component, IList<Marking> marks, Participant participant, DanceInRound danceInRound)
        {
            IList<double> allMarks = null;

            switch (component)
            {
                case 1:
                    allMarks = marks.Where(m => m.MarkA > 0).Select(m => m.MarkA).ToList();
                    break;
                case 2:
                    allMarks = marks.Where(m => m.MarkB > 0).Select(m => m.MarkB).ToList();
                    break;
                case 3:
                    allMarks = marks.Where(m => m.MarkC > 0).Select(m => m.MarkC).ToList();
                    break;
                case 4:
                    allMarks = marks.Where(m => m.MarkD > 0).Select(m => m.MarkD).ToList();
                    break;
            }

            if (allMarks == null || allMarks.Count == 0)
            {
                return 0;
            }

            var marksInRange = GetMarksInRange(allMarks, CaluclateMedian(allMarks));

            Debug.Write(component + " / " + participant.Number + " / " + danceInRound.Dance.ShortName + ": ");
            foreach (var mark in marksInRange)
            {
                Debug.Write(" " + mark);
            }
            Debug.WriteLine("");

            return Math.Round(marksInRange.Average(), 3, MidpointRounding.AwayFromZero); ;
        }

        private IEnumerable<double> GetMarksInRange(IList<double> marks, double avg)
        {
            var marksInRange = marks.Where(m => Math.Abs(m - avg) <= this.maximumDeviation).ToList();

            if (!marksInRange.Any())
            {
                marksInRange = marks.ToList();
            }

            return marksInRange;
        }

        private static double CaluclateMedian(IList<double> marks)
        {
            var sorted = marks.OrderBy(o => o).ToList();
            return (sorted[2] + sorted[3]) / 2;

            //return marks.Average();
        }
    }
}
