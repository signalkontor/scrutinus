﻿// // TPS.net TPS8 Helpers
// // PointCalculatorAbstract.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using DataModel.Models;
using Helpers.Calculation;

namespace General.NewJudgingSystem
{
    public abstract class PointCalculatorAbstract
    {
        public IEnumerable<Qualified> CalculatePointsAndPlaces(
            ScrutinusContext context,
            Round round,
            List<JudgingComponent> components)
        {
            this.CalculatePoints(context, round, components);
            
            context.SaveChanges();
            
            var result = PlaceCalculation.CalculatePlaces(round.Qualifieds, round.PlaceOffset);
            
            context.SaveChanges();

            return result;
        }

        protected abstract void CalculatePoints(
            ScrutinusContext context,
            Round round,
            List<JudgingComponent> components);
    }
}