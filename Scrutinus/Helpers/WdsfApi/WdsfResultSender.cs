﻿// // TPS.net TPS8 Helpers
// // WdsfResultSender.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataModel;
using DataModel.Models;
using Wdsf.Api.Client;
using Wdsf.Api.Client.Models;
using Dance = Wdsf.Api.Client.Models.Dance;
using Round = DataModel.Models.Round;

namespace General.WdsfApi
{
    public class WdsfResultSender
    {
        private readonly Client _apiClient;

        private readonly ScrutinusContext _context;

        private readonly StringBuilder _log;

        private readonly IStatusReporter _statusReporter;

        private int _wdsfId;

        public WdsfResultSender(Client apiClient, ScrutinusContext context, IStatusReporter status)
        {
            this._apiClient = apiClient;
            this._context = context;
            this._statusReporter = status;
            this._log = new StringBuilder();
        }

        public string SendResult(int roundId)
        {
            var round = this._context.Rounds.Single(r => r.Id == roundId);

            this._wdsfId = 0;
            if (!Int32.TryParse(round.Competition.ExternalId, out this._wdsfId))
            {
                this._log.AppendLine(
                    "WDSF Competition Id not correctly set in competition data. The value is not numeric");
                return this._log.ToString();
            }

            var participants = this._apiClient.GetCoupleParticipants(this._wdsfId);

            this.CheckOfficialsRegistered(round);
            this.CheckParticipantsRegistered(round, participants);

            this.UpdateResults(round, participants);

            return this._log.ToString();
        }

        private void UpdateResults(Round round, IList<ParticipantCouple> participants)
        {
            // Do we have markings to send? 
            if (!this._context.Markings.Any(p => p.Round.Id == round.Id))
            {
                return;
            }
            var max = participants.Count;
            var count = 0;
            var roundName = this.getWdsfRoundName(round);
            foreach (var qualified in round.Qualifieds)
            {
                this._statusReporter.ReportStatus("Sending Results for couples", count, max);
                count++;

                var WdsfParticipant = participants.Single(p => p.StartNumber == qualified.Participant.Number.ToString());
                var details = this._apiClient.GetCoupleParticipant(WdsfParticipant.Id);
                var r = details.Rounds.SingleOrDefault(a => a.Name == roundName);

                if (r != null)
                {
                    details.Rounds.Remove(r);
                }

                var wdsfRound = new Wdsf.Api.Client.Models.Round() { Name = roundName };

                if (qualified.Participant.PlaceFrom.HasValue && qualified.Participant.PlaceFrom.Value > 0)
                {
                    details.Rank = qualified.Participant.PlaceFrom.Value.ToString();
                }

                foreach (var danceRound in round.DancesInRound)
                {
                    var dance = new Dance() { Name = danceRound.Dance.DanceName };

                    wdsfRound.Dances.Add(dance);

                    switch (round.MarksInputType)
                    {
                        case MarkingTypes.Marking:
                            this.UpdateMarking(dance, round, danceRound, qualified.Participant);
                            break;
                        case MarkingTypes.NewJudgingSystemV1:
                            this.UpdateScoreV1(dance, round, danceRound, qualified.Participant);
                            break;
                        case MarkingTypes.NewJudgingSystemV2:
                            this.UpdateScoreV2(dance, round, danceRound, qualified.Participant);
                            break;
                    }
                }
            }
        }

        private void UpdateScoreV2(Dance wdsfDance, Round round, DanceInRound danceRound, Participant participant)
        {
            foreach (var judge in round.Competition.Officials.Where(o => o.Role.Id == Roles.Judge))
            {
                var marking = this._context.Markings.SingleOrDefault(m => m.Judge.Id == judge.Id && m.Round.Id == round.Id && m.Participant.Id == participant.Id);

                if (marking == null)
                {
                    continue;
                }

                var officialId = Int32.Parse(judge.ExternalId);

                var mark = new OnScale2Score() { OfficialId = officialId, };

                switch (marking.MarkingComponent)
                {
                    case 1:
                        mark.TQ = (decimal)marking.Mark20;
                        break;
                    case 2:
                        mark.MM = (decimal)marking.Mark20;
                        break;
                    case 3:
                        mark.CP = (decimal)marking.Mark20;
                        break;
                    case 4:
                        mark.PS = (decimal)marking.Mark20;
                        break;
                }

                wdsfDance.Scores.Add(mark);
            }
        }

        private void UpdateScoreV1(Dance wdsfDance, Round round, DanceInRound danceRound, Participant participant)
        {
            foreach (var judge in round.Competition.Officials.Where(o => o.Role.Id == Roles.Judge))
            {
                var marking =
                    this._context.Markings.SingleOrDefault(
                        m => m.Judge.Id == judge.Id && m.Round.Id == round.Id && m.Participant.Id == participant.Id);
                var officialId = Int32.Parse(judge.ExternalId);

                var mark = new OnScaleScore()
                               {
                                   OfficialId = officialId,
                                   PB = (decimal)marking.MarkA,
                                   MM = (decimal)marking.MarkB,
                                   PS = (decimal)marking.MarkC,
                                   CP = (decimal)marking.MarkD
                               };

                wdsfDance.Scores.Add(mark);
            }
        }

        private void UpdateMarking(Dance wdsfDance, Round round, DanceInRound danceRound, Participant participant)
        {
            foreach (var judge in round.Competition.Officials.Where(o => o.Role.Id == Roles.Judge))
            {
                var marking =
                    this._context.Markings.SingleOrDefault(
                        m => m.Judge.Id == judge.Id && m.Round.Id == round.Id && m.Participant.Id == participant.Id);
                var officialId = Int32.Parse(judge.ExternalId);

                var mark = new MarkScore() { OfficialId = officialId, IsSet = marking != null && marking.Mark > 0 };

                wdsfDance.Scores.Add(mark);
            }
        }

        private string getWdsfRoundName(Round round)
        {
            if (round.Number == 1)
            {
                return "1";
            }
            if (round.IsRedanceRound)
            {
                return "R";
            }
            if (round.RoundType.IsFinal)
            {
                return "F";
            }

            if (round.Competition.Rounds.Any(r => r.RoundType.Id == RoundTypes.Redance))
            {
                // In redance, our 3rd round is actually called 2nd round ...   
                return (round.Number - 1).ToString();
            }
            else
            {
                return round.Number.ToString();
            }
        }

        private void CheckParticipantsRegistered(
            Round round,
            IList<ParticipantCouple> WdsfParticipants)
        {
            var count = 0;
            var max = round.Qualifieds.Count;

            foreach (var qualified in round.Qualifieds)
            {
                this._statusReporter.ReportStatus("Registering Couples", count, max);
                count++;

                if (string.IsNullOrEmpty(qualified.Participant.ExternalId))
                {
                    this.RegisterParticipant(qualified.Participant);
                }
            }
        }

        private void RegisterParticipant(Participant participant)
        {
            var WdsfParticipant = new ParticipantCoupleDetail()
                                      {
                                          CompetitionId = this._wdsfId,
                                          CoupleId = participant.Couple.ExternalId,
                                          StartNumber = participant.Number
                                      };

            try
            {
                var newParticipantUri = this._apiClient.SaveCoupleParticipant(WdsfParticipant);
                WdsfParticipant = this._apiClient.Get<ParticipantCoupleDetail>(newParticipantUri);
                participant.ExternalId = WdsfParticipant.Id.ToString();
                this._context.SaveChanges();
            }
            catch (Exception Ex)
            {
                this.Log(Ex.Message);
            }
        }

        private void CheckOfficialsRegistered(Round round)
        {
            var count = 0;
            var max = round.Competition.Officials.Count;

            var judges = this._apiClient.GetOfficials(this._wdsfId);
            var officialDetails = new List<OfficialDetail>();

            foreach (var official in judges)
            {
                officialDetails.Add(this._apiClient.GetOfficial(official.Id));
            }


            foreach (var officialCompetition in round.Competition.Officials)
            {
               
                var wdsfJudge = officialDetails.SingleOrDefault(j => j.AdjudicatorChar == officialCompetition.Official.Sign);
                if (wdsfJudge != null)
                {
                    officialCompetition.ExternalId = wdsfJudge.Id.ToString();
                    continue;
                }

                this._statusReporter.ReportStatus("Registering Officials", count, max);
                count++;

                if (officialCompetition.ExternalId == null)
                {
                    this.RegisterOfficial(officialCompetition);
                }
            }
        }

        private void RegisterOfficial(OfficialInCompetition officialCompetition)
        {
            // Check, if the judge is registered:

            var officalDetail = new OfficialDetail()
                                    {
                                        CompetitionId = this._wdsfId,
                                        AdjudicatorChar = officialCompetition.Official.Sign,
                                        Min =
                                            officialCompetition.Official.MIN.HasValue
                                                ? officialCompetition.Official.MIN.Value
                                                : 0,
                                        Nationality = officialCompetition.Official.Nationality.LongName,
                                        Person = officialCompetition.Official.NiceName,
                                    };

            if (officialCompetition.Role.Id == Roles.Judge)
            {
                officalDetail.Task = "Adjudicator";
            }
            else if (officialCompetition.Role.Id == Roles.Chairman)
            {
                officalDetail.Task = "Chairman";
            }

            try
            {
                var uri = this._apiClient.SaveOfficial(officalDetail);
                var official = this._apiClient.Get<OfficialDetail>(uri);
                officialCompetition.ExternalId = official.Id.ToString();
                this._context.SaveChanges();
            }
            catch (Exception ex)
            {
                this.Log(
                    String.Format(
                        "Could not update Official {0}: {1}",
                        officialCompetition.Official.NiceName,
                        ex.Message));
            }
        }

        private void Log(string message)
        {
            this._log.AppendLine(message);
        }
    }
}