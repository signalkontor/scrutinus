﻿using System;

using LibGit2Sharp;
using Microsoft.VisualStudio.TextTemplating;

namespace ProjectMetaData
{
    public abstract class CreateMetaData : TextTransformation
    {
        public string GetBranchName(string path)
        {
            using (var repo = new Repository(path))
            {
                Branch checkedOutBranch = repo.Head;

                return string.Format("git branch: {0} - {2} ({1})", checkedOutBranch.FriendlyName, checkedOutBranch.Tip, DateTime.Now.ToShortDateString());
            }
        }
    }
}
