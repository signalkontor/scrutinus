using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;

using CheckIn.Printing;
using CheckIn.ServiceProvider;
using CheckIn.View;

using DataModel;
using DataModel.Models;

using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

using MahApps.Metro.Controls.Dialogs;

using Scrutinus.Localization;
using System.IO;

namespace CheckIn.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private List<Couple> couples;

        private string searchText;

        private bool searchTextFocused;

        private IServiceProvider serviceProvider;

        private Couple selectedCouple;

        private Competition selectedCompetition;

        private AddCompetitionDialog addCompetitionDialog;

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel()
        {
            selectedCompetition = null;
            searchText = string.Empty;
            this.Competitions = new ObservableCollection<Competition>();

            this.CreateNewDataContext();

            this.Couples = this.GetFilteredList();

            this.ClearSearchCommand = new RelayCommand(this.ClearSearch);
            this.CheckInCommand = new RelayCommand(this.CheckIn);
            this.CheckOutCommand = new RelayCommand(this.CheckOut);
            this.PrintNumbersCommand = new RelayCommand(this.PrintNumbers);
            this.AddNewCoupleCommand = new RelayCommand(this.AddNewCouple);
            this.SaveCommand = new RelayCommand(this.Save);
            this.AddCompetitionCommand = new RelayCommand(AddCompetition);
            this.EditStartbookCommand = new RelayCommand(EditStartbook);
            this.ScanLaufzettelCommand = new RelayCommand(this.ScanLaufzettel);
            this.OpenDatabaseCommand = new RelayCommand(this.SelectDatabase);

            this.SearchTextFocused = true;
        }

        public ObservableCollection<Competition> Competitions { get; set; } 

        public List<Couple> Couples
        {
            get
            {
                return this.couples;
            }
            set
            {
                this.couples = value;
                this.RaisePropertyChanged(() => Couples);
            }
        }

        public ICommand ClearSearchCommand { get; set; }

        public ICommand AddNewCoupleCommand { get; set; }

        public ICommand CheckInCommand { get; set; }

        public ICommand CheckOutCommand { get; set; }

        public ICommand PrintNumbersCommand { get; set; }

        public ICommand SaveCommand { get; set; }

        public ICommand AddCompetitionCommand { get; set; }

        public ICommand EditStartbookCommand { get; set; }

        public ICommand ScanLaufzettelCommand { get; set; }

        public ICommand OpenDatabaseCommand { get; set; }

        public IList SelectedParticipants { get; set; } 

        public string SearchText
        {
            get
            {
                return searchText;
            }
            set
            {
                searchText = value;

                this.Couples = this.GetFilteredList();

                this.RaisePropertyChanged(() => SearchText);

                if (this.Couples.Count == 1)
                {
                    this.SelectedCouple = this.Couples.First();
                }

            }
        }

        public Couple SelectedCouple
        {
            get
            {
                return this.selectedCouple;
            }
            set
            {
                this.selectedCouple = value;

                if (value != null)
                {
                    this.serviceProvider.UpdateEntity(this.selectedCouple);
                    foreach (var participant in this.selectedCouple.Participants)
                    {
                        this.serviceProvider.UpdateEntity(participant);
                        this.serviceProvider.UpdateEntity(participant.Competition);
                    }
                }

                this.RaisePropertyChanged(() => SelectedCouple);

                this.SelectedParticipants.Clear();

                if (this.selectedCouple != null)
                {
                    foreach (
                        var participant in
                            this.selectedCouple.Participants.Where(p => p.Competition.State == CompetitionState.CheckIn && p.State == CoupleState.Missing)
                        )
                    {
                        this.SelectedParticipants.Add(participant);
                    }
                }
            }
        }

        public Competition SelectedCompetition
        {
            get
            {
                return this.selectedCompetition;
            }
            set
            {
                this.selectedCompetition = value;
                this.RaisePropertyChanged(() => SelectedCompetition);
                if (value == null)
                {
                    return;
                }
                // Filter this view by selected Competition:
                this.Couples = this.GetFilteredList();
            }
        }

        public bool SearchTextFocused
        {
            get
            {
                return this.searchTextFocused;
            }
            set
            {
                this.searchTextFocused = value;
                RaisePropertyChanged(() => SearchTextFocused);
            }
        }

        private void ClearSearch()
        {
            this.CreateNewDataContext();
            this.SelectedCompetition = null;
            this.SearchText = String.Empty;
            this.SearchTextFocused = true;
        }

        private void AddNewCouple()
        {
            var newCouple = serviceProvider.CreateNewCouple();
            this.Couples = this.GetFilteredList();
            this.SelectedCouple = newCouple;
        }

        private void CheckIn()
        {
            this.SetStateForSelectedCouples(CoupleState.Dancing, CompetitionState.CheckIn);

            if (CheckInSettings.Default.PrintNumbers)
            {
                this.PrintNumbers();                
            }
        }

        private void CheckOut()
        {
            this.SetStateForSelectedCouples(CoupleState.Excused, null);
            this.ClearSearch();
        }

        private void SetStateForSelectedCouples(CoupleState state, CompetitionState? allowedState)
        {
            if (this.SelectedParticipants != null && this.SelectedParticipants.Count > 0)
            {
                var laufzettel = false;

                foreach (var selectedParticipant in this.SelectedParticipants.Cast<Participant>())
                {
                    this.serviceProvider.UpdateEntity(selectedParticipant);
                    this.serviceProvider.UpdateEntity(selectedParticipant.Competition);

                    if ((!allowedState.HasValue || selectedParticipant.Competition.State == allowedState.Value) && selectedParticipant.Competition.IsCompetitionNotStarted)
                    {
                        selectedParticipant.State = state;
                        serviceProvider.UpdateParticipantState(selectedParticipant.Id, state);
                        if (selectedParticipant.Startbuch != null && selectedParticipant.Startbuch.PrintLaufzettel)
                        {
                            laufzettel = true;
                        }
                    }
                }

                if (laufzettel)
                {
                    MainWindow.Instance.ShowErrorMessage("Laufzettel", "Achtung: Laufzettelpaar!");
                }
            }
        }

        private void PrintNumbers()
        {
            IEnumerable<Participant> participantsToPrint = null;

            try
            {
                participantsToPrint = this.SelectedParticipants.Cast<Participant>().GroupBy(p => p.Number).Select(g => g.First());

                foreach (var selectedParticipant in participantsToPrint)
                {
                    var printer = new NumberPrinter((Participant)selectedParticipant);
                    Scrutinus.Reports.PrintHelper.PrintReport(null, 1, false, printer, "Number", printer.IsLandScape);
                }
            }
            catch (Exception ex)
            {
                MainWindow.Instance.ShowErrorMessage("Could not print number: " + ex.Message);
            }
            
        }

        private void Save()
        {
            this.Couples = this.GetFilteredList();
        }

        private void AddCompetition()
        {
            if (this.SelectedCouple == null)
            {
                MainWindow.Instance.ShowErrorMessage(LocalizationService.Resolve(() => Localization.Text.PleaseSelectCouple));
                return;
            }

            addCompetitionDialog = new AddCompetitionDialog(AddCompetitionClosed, new AddCompetitionsViewModel(this.SelectedCouple));

            MainWindow.Instance.ShowMetroDialog(addCompetitionDialog);
        }

        private void AddCompetitionClosed()
        {
            if (addCompetitionDialog.ViewModel.ResultOk)
            {
                foreach (var externalCompetition in addCompetitionDialog.ViewModel.SelectedCompetitions.OfType<Competition>())
                {
                    var competition = this.Competitions.Single(c => c != null && c.Id == externalCompetition.Id);
                    // Create a new Participant for this competition
                    var number = competition.Participants.Max(p => p.Number) + 1;

                    var startbuch = GetStartBuch(competition.Section);

                    var participant = new Participant()
                                          {
                                              Competition = competition,
                                              AgeGroup = competition.AgeGroup,
                                              Class = startbuch != null ? startbuch.Class : competition.Class,
                                              TargetClass = startbuch?.NextClass,
                                              TargetPlacings = startbuch?.TargetPlacings ?? 0,
                                              TargetPoints = startbuch?.TargetPoints ?? 0,
                                              MinimumPoints = startbuch?.MinimumPoints ?? 0,
                                              Couple = SelectedCouple,
                                              State = CoupleState.Missing,
                                              Star = 0,
                                              Number = number,
                                          };

                    this.SelectedCouple.Participants.Add(participant);
                    this.serviceProvider.CreateNewParticipant(participant);
                }

                var selectedCouple = SelectedCouple;
                // Update SelectedCouple to bring changes to the UI
                SelectedCouple = null;
                SelectedCouple = selectedCouple;

            }
        }

        private Startbuch GetStartBuch(Section section)
        {
            if (this.selectedCouple.Startbooks == null)
            {
                return null;
            }

            return this.selectedCouple.Startbooks.FirstOrDefault(s => s.Section.Id == section.Id);
        }

        private List<Couple> GetFilteredList()
        {
            var result = serviceProvider.GetCouples().AsQueryable();

            if (this.searchText.Length > 2)
            {
                result = result.Where(
                            c =>
                            c.FirstMan.Contains(this.searchText) || c.LastMan.Contains(this.searchText) || c.MINMan.Contains(this.searchText)
                            || c.WdsfIdMan.Contains(this.searchText) || c.WdsfIdWoman.Contains(this.searchText)
                            || c.FirstWoman.Contains(this.searchText) || c.LastWoman.Contains(this.searchText)
                            || c.MINWoman.Contains(this.searchText));
            }

            if (this.selectedCompetition != null)
            {
                result = result.Where(c => c.Participants.Any(p => p.Competition.Id == selectedCompetition.Id));
            }

            var list = result.OrderBy(c => c.FirstMan).ThenBy(c => c.LastMan).ToList();
            
            return list;
        }

        private void CreateNewDataContext()
        {
            try
            {
                this.serviceProvider = ServiceProviderFactory.GetServiceProvider();
                this.Competitions.Clear();

                var competitions = this.serviceProvider.GetCompetitions();

                foreach (var competition in competitions.OrderBy(c => c.StartTime))
                {
                    this.Competitions.Add(competition);
                }
            }
            catch (Exception ex)
            {
                File.AppendAllText("errorlog.log", ex.Message);
                MessageBox.Show(ex.Message);
                if (ex.InnerException != null)
                {
                    File.AppendAllText("errorlog.log", ex.InnerException.Message);
                    MessageBox.Show(ex.InnerException.Message);
                }
            }

            this.Competitions.Add(null);
        }

        private void EditStartbook()
        {
            if (this.SelectedCouple == null)
            {
                return;
            }

            var dialog = new EditStartbuchDialog(this.serviceProvider, this.SelectedCouple);
            MainWindow.Instance.ShowMetroDialog(dialog);
        }

        private void ScanLaufzettel()
        {
            if (this.SelectedCouple == null)
            {
                return;
            }

            var dialog = new ScanBarcodeDialog(this.serviceProvider, this.SelectedCouple);
            MainWindow.Instance.ShowMetroDialog(dialog);
        }

        private void SelectDatabase()
        {
            var selectDatabaseWindow = new SelectDatabaseDialog();
            var result = selectDatabaseWindow.ShowDialog();
            if (result.HasValue && result.Value)
            {
                CheckInSettings.Default.DatabaseName = selectDatabaseWindow.SelectedDatabase;
                CheckInSettings.Default.SQLServer = selectDatabaseWindow.Server;
                CheckInSettings.Default.DatabaseUser = selectDatabaseWindow.User;
                CheckInSettings.Default.DatabasePassword = selectDatabaseWindow.Password;
                CheckInSettings.Default.Save();

                this.CreateNewDataContext();

                this.ClearSearch();
            }
        }
    }
}