﻿// // TPS.net TPS8 CheckIn
// // AddCompetitionsViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using DataModel;
using DataModel.Models;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MahApps.Metro.Controls.Dialogs;

namespace CheckIn.ViewModel
{
    public class AddCompetitionsViewModel : ViewModelBase
    {
        private readonly ScrutinusContext context;

        public AddCompetitionsViewModel(Couple couple)
        {
            this.SaveCommand = new RelayCommand(this.SaveClose);
            this.CancelCommand = new RelayCommand(this.CancelClose);
            this.context = new ScrutinusContext();
            this.Competitions = this.context.Competitions.Where(c => c.State == CompetitionState.CheckIn && c.Participants.All(p => p.Couple.Id != couple.Id)).OrderBy(c => c.Section.Id).ThenBy(c => c.StartTime).ToList();
            }

        public IList SelectedCompetitions { get; set; }

        public List<Competition> Competitions { get; set; }

        public ICommand SaveCommand { get; set; }

        public ICommand CancelCommand { get; set; }

        public Action ClosingAction { get; set; }

        public bool ResultOk { get; private set; }

        public AddCompetitionDialog Dialog { get; set; }

        private void SaveClose()
        {
            this.ResultOk = true;
            MainWindow.Instance.HideMetroDialogAsync(this.Dialog);
            this.ClosingAction();
        }

        private void CancelClose()
        {
            this.ResultOk = false;
            MainWindow.Instance.HideMetroDialogAsync(this.Dialog);
            this.ClosingAction();
        }
    }
}
