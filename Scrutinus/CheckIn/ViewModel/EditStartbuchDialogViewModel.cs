﻿// // TPS.net TPS8 CheckIn
// // EditStartbuchDialogViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using DataModel.Models;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MahApps.Metro.Controls.Dialogs;
using Scrutinus.Dialogs.DialogViewModels;

namespace CheckIn.ViewModel
{
    internal class EditStartbuchDialogViewModel : ViewModelBase
    {
        private readonly IServiceProvider serviceProvider;

        public EditStartbuchDialogViewModel()
        {
            this.CancelCommand = new RelayCommand(this.Cancel);
            this.SaveCommand = new RelayCommand(this.Save);
        }

        public EditStartbuchDialogViewModel(IServiceProvider serviceProvider, Couple couple) : this()
        {
            this.serviceProvider = serviceProvider;
            this.Couple = couple;
            this.Classes = serviceProvider.GetClasses();
            this.Startbucher = this.CloneStartbucher();
        }

        public IEnumerable<StartbuchViewModel> Startbucher { get; set; }

        public IEnumerable<Class> Classes { get; set; }

        public ICommand SaveCommand { get; set; }

        public ICommand CancelCommand { get; set; }

        public Couple Couple { get; set; }

        public BaseMetroDialog Dialog { get; set; }

        private void Cancel()
        {
            MainWindow.Instance.HideMetroDialogAsync(this.Dialog);
        }

        private void Save()
        {
            foreach (var startbuch in this.Startbucher)
            {
                // find the one:
                var book = this.Couple.Startbooks.Single(b => b.Id == startbuch.Id);

                this.serviceProvider.UpdateStartbuchData(startbuch.Couple.Id, startbuch.Section.Id, startbuch.OriginalPoints, startbuch.OriginalPlacings, startbuch.Class.Id);

                //if (book.Class.Id != startbuch.Class.Id || book.OriginalPlacings != startbuch.OriginalPlacings
                //    || book.OriginalPoints != startbuch.OriginalPoints)
                //{
                //    UpdateParticipantHelper.UpdateAllParticipants(serviceProvider, startbuch);
                //    book.PrintLaufzettel = true;
                //    book.OriginalPoints = startbuch.OriginalPoints;
                //    book.OriginalPlacings = startbuch.OriginalPlacings;
                //    book.Class = startbuch.Class;
                //    book.NextClass = startbuch.NextClass;
                //}
            }

            MainWindow.Instance.HideMetroDialogAsync(this.Dialog);
        }

        private IEnumerable<StartbuchViewModel> CloneStartbucher()
        {
            var result = new List<StartbuchViewModel>();
            foreach (var startbook in this.Couple.Startbooks)
            {
                var clonedBook = new StartbuchViewModel()
                                     {
                                         Id = startbook.Id,
                                         AgeGroup = startbook.AgeGroup,
                                         Class = startbook.Class,
                                         Couple = startbook.Couple,
                                         Section = startbook.Section,
                                         OriginalPoints = startbook.OriginalPoints,
                                         OriginalPlacings = startbook.OriginalPlacings,
                                     };

               clonedBook.AvailableClasses = this.Classes.ToList();

                result.Add(clonedBook);
            }

            return result;
        }
    }
}
