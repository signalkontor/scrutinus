﻿// // TPS.net TPS8 CheckIn
// // ScanBarcodeDialogViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Linq;
using System.Windows.Input;
using CheckIn.Helper;
using DataModel.Models;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MahApps.Metro.Controls.Dialogs;
using Scrutinus.Dialogs.DialogViewModels;

namespace CheckIn.ViewModel
{
    public class ScanBarcodeDialogViewModel : ViewModelBase
    {
        private string barcode;

        private string message;
        private readonly IServiceProvider serviceProvider;

        private StartbuchViewModel startbuch;

        public ScanBarcodeDialogViewModel()
        {
            
        }

        public ScanBarcodeDialogViewModel(IServiceProvider serviceProvider, Couple couple)
        {
            this.serviceProvider = serviceProvider;
            this.Couple = couple;

            this.SaveCommand = new RelayCommand(this.Save);
            this.CancelCommand = new RelayCommand(this.Cancel);
        }

        public CustomDialog Dialog { get; set; }

        public string Barcode
        {
            get
            {
                return this.barcode;
            }
            set
            {
                this.barcode = value;
                this.DecodeBarcode();
            }
        }

        public StartbuchViewModel Startbuch
        {
            get
            {
                return this.startbuch;
            }
            set
            {
                this.startbuch = value;
                this.RaisePropertyChanged(() => this.Startbuch);
            }
        }

        public ICommand SaveCommand { get; set; }

        public ICommand CancelCommand { get; set; }

        public Couple Couple { get; set; }

        public string Message
        {
            get
            {
                return this.message;
            }
            set
            {
                this.message = value;
                this.RaisePropertyChanged(() => this.Message);
            }
        }

        private void DecodeBarcode()
        {
            if (this.barcode.StartsWith("$"))
            {
                this.barcode = this.barcode.Substring(1);
            }

            if (this.barcode.EndsWith("$"))
            {
                this.barcode = this.barcode.Substring(0, this.barcode.Length - 1);
            }

            try
            {
                this.Startbuch = new StartbuchViewModel()
                                    {
                                        Couple = this.Couple,
                                        Section = this.GetSection(this.barcode.Substring(0, 1)),
                                        Class = this.GetClass(this.barcode.Substring(1, 1)),
                                        OriginalPoints = int.Parse(this.barcode.Substring(2, 4)),
                                        OriginalPlacings = int.Parse(this.barcode.Substring(6, 2)),
                                        NextClass = this.GetClass(this.barcode.Substring(8, 1)),
                                    };
                var originalStartbook = this.Couple.Startbooks.First(b => b.Section.Id == this.Startbuch.Section.Id);
                this.Startbuch.AgeGroup = originalStartbook.AgeGroup;
                this.Message = "";
            }
            catch (Exception exception)
            {
                this.Message = $"Barcode konnte nicht erkannt werden ({exception.Message})";
            }
        }

        private Section GetSection(string section)
        {
            var sections = this.serviceProvider.GetSections();

            switch(section)
            {
                case "1":
                    return sections.First(s => s.Id == 1);
                case "2":
                    return sections.First(s => s.Id == 2);
            }

            return null;
        }

        private Class GetClass(string classString)
        {
            var classes = this.serviceProvider.GetClasses();

            switch (classString)
            {
                case "0":
                    return classes.Single(c => c.Id == 7);
                case "1":
                    return classes.Single(c => c.Id == 1);
                case "2":
                    return classes.Single(c => c.Id == 2);
                case "3":
                    return classes.Single(c => c.Id == 3);
                case "4":
                    return classes.Single(c => c.Id == 4);
                case "5":
                    return classes.Single(c => c.Id == 5);
            }

            return null;
        }

        private void Save()
        {
            if (this.Startbuch != null)
            {
                UpdateParticipantHelper.UpdateAllParticipants(this.serviceProvider, this.Startbuch);
            }

            MainWindow.Instance.HideMetroDialogAsync(this.Dialog);
        }

        private void Cancel()
        {
            MainWindow.Instance.HideMetroDialogAsync(this.Dialog);
        }
    }
}
