﻿// // TPS.net TPS8 CheckIn
// // MainWindow.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Text;
using System.Threading;
using System.Windows.Input;
using System.Windows.Threading;
using CheckIn.ViewModel;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Scrutinus.Localization;

namespace CheckIn
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public static MainWindow Instance;

        private DateTime keyDownTime = DateTime.Now;

        private readonly StringBuilder mScanData = new StringBuilder();

        private readonly KeyConverter mScanKeyConverter = new KeyConverter();

        private bool mScanning = false;


        public MainWindow()
        {
            this.InitializeComponent();

            Instance = this;
            
            LocalizationService.ApplicationLanguage = Thread.CurrentThread.CurrentCulture;

            var viewModel = this.DataContext as MainViewModel;

            if (viewModel != null)
            {
                viewModel.SelectedParticipants = this.ParticipantDataGrid.SelectedItems;
            }

            // this.SearchTextBox.PreviewKeyDown += this.OnPreviewKeyDown;

            this.PreviewKeyDown += this.OnPreviewKeyDown;
        }

        private void OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            // Dauert die Eingabe zwischen zwei Zeichen länger als 50 Millisekunden, handelt es sich nicht um den Barcodescanner.
            if (this.mScanning && (DateTime.Now - this.keyDownTime).TotalMilliseconds > 50)
            {
                this.mScanning = false;
                this.mScanData.Clear();
            }
            // 11 Zeichen gescannt und Enter oder Tab -> String übernehmen
            if (this.mScanning && this.mScanData.Length == 11 && (e.Key == Key.Enter || e.Key == Key.Tab))
            {
                this.SearchTextBox.Text = this.mScanData.ToString();
                this.mScanning = false;
            } // aktiver Scanvorgang (DE123456789)
            else if (this.mScanning && this.mScanData.Length > 1 || this.mScanning && e.Key == Key.D && this.mScanData.Length == 0 || this.mScanning && e.Key == Key.E && this.mScanData.Length == 1)
            {
                this.keyDownTime = DateTime.Now;
                this.mScanData.Append(this.mScanKeyConverter.ConvertToString(e.Key));
            }  // Scanvorgang startet mit LeftShift (ausgeschlossen der LeftShift vor dem "E")
            else if (!(this.mScanning && this.mScanData.Length == 1 && e.Key == Key.LeftShift))
            {
                this.keyDownTime = DateTime.Now;
                this.mScanning = false;
                if (e.Key == Key.LeftShift)
                {
                    this.mScanning = true;
                }
            }
        }

        public void ShowMetroDialog(BaseMetroDialog dialog)
        {
            var metroWindow = this;
            
            metroWindow.MetroDialogOptions.ColorScheme = MetroDialogColorScheme.Inverted;

            var task = this.ShowMetroDialogAsync(dialog);

            while (!task.IsCompleted)
            {
                Thread.Sleep(100);
                this.Dispatcher.Invoke(DispatcherPriority.ApplicationIdle, new Action(() => { }));
            }
        }

        public void ShowErrorMessage(string message)
        {
            this.ShowMessageAsync("Error", message);
        }

        public void ShowErrorMessage(string title, string message)
        {
            this.ShowMessageAsync(title, message);
        }

        private void SearchTextBox_OnPreviewKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                this.ComboBox.Focus();
            }
        }
    }
}
