﻿// // TPS.net TPS8 CheckIn
// // SQLServerServiceProvider.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Linq;
using DataModel;
using DataModel.Models;

namespace CheckIn.ServiceProvider
{
    public class SQLServerServiceProvider : IServiceProvider
    {
        private readonly ScrutinusContext context;

        public SQLServerServiceProvider(string serverName, string databaseName, string userName, string password)
        {
            var sqlServerConnectionString = string.Format(@"Data Source={0};Initial Catalog={3};Persist Security Info=True;User ID={1};Password={2};MultipleActiveResultSets=True", serverName, userName, password, databaseName);
                // CheckInSettings.Default.CeFile;
            ScrutinusContext.CurrentDataSource = sqlServerConnectionString;

            this.context = new ScrutinusContext();
        }

        public IEnumerable<Class> GetClasses()
        {
            return this.context.Classes;
        }

        public IEnumerable<AgeGroup> GetAgeGroups()
        {
            return this.context.AgeGroups;
        }

        public IEnumerable<Couple> GetCouples()
        {
            return this.context.Couples;
        }

        public IEnumerable<Competition> GetCompetitions()
        {
            var data = this.context.Competitions.ToList();
            return data;
        }

        public IEnumerable<Participant> GetParticipants()
        {
            return this.context.Participants;
        }

        public IEnumerable<Startbuch> GetStartbuchsForCouple(int coupleId)
        {
            return this.context.Startbuecher.Where(s => s.Couple.Id == coupleId);
        }

        public IEnumerable<Section> GetSections()
        {
            return this.context.Sections;
        }

        public void CreateNewParticipant(Participant participant)
        {
            this.context.Participants.Add(participant);
            this.context.SaveChanges();
        }

        public Couple CreateNewCouple()
        {
            throw new NotImplementedException();
        }

        public void UpdateParticipantState(int id, CoupleState state)
        {
            var participant = this.context.Participants.Single(p => p.Id == id);
            participant.State = state;
            this.context.SaveChanges();
        }

        public void UpdateStartbuchData(int coupleId, int sectionId, int newPoints, int newPlacings, int nwClassId)
        {
            ClimbUpTable climbUpData;

            var startbuch = this.context.Startbuecher.First(s => s.Couple.Id == coupleId && s.Section.Id == sectionId);
            startbuch.OriginalPoints = newPoints;
            startbuch.OriginalPlacings = newPlacings;
            
            if (startbuch.Class.Id != nwClassId)
            {
                // Target-Class heraus suchen aus Aufstiegsdaten
                climbUpData = this.context.ClimbUpTables.First(c => c.Section.Id == sectionId && c.AgeGroup.Id == startbuch.AgeGroup.Id && c.LTV == startbuch.Couple.Region && c.Class.Id == nwClassId);
                startbuch.Class = climbUpData.Class;
                startbuch.TargetPlacings = climbUpData.Placings;
                startbuch.TargetPoints = climbUpData.Points;
                startbuch.NextClass = climbUpData.TargetClass;
                startbuch.PlacingsUpto = climbUpData.MinimumPlace;
                startbuch.MinimumPoints = climbUpData.MinPoints;
                startbuch.PrintLaufzettel = true;
            }
            // Now update all participants that did not dance yet:
            var participants = this.context.Participants.Where(p => p.Couple.Id == coupleId && p.Competition.Section.Id == sectionId && (p.State == CoupleState.Dancing || p.State == CoupleState.Excused || p.State == CoupleState.Missing));

            foreach (var participant in participants)
            {
                participant.OriginalPoints = newPoints;
                participant.OriginalPlacings = newPlacings;
                if (participant.Class.Id != nwClassId)
                {
                    participant.Class = this.context.Classes.Single(c => c.Id == nwClassId);
                    participant.TargetClass = startbuch.NextClass;
                    participant.TargetPlacings = startbuch.TargetPlacings;
                    participant.TargetPoints = startbuch.TargetPoints;
                    participant.PlacingsUpto = startbuch.PlacingsUpto;
                    participant.MinimumPoints = startbuch.MinimumPoints;
                }
            }

            this.context.SaveChanges();
        }

        public void UpdateEntity(object entity)
        {
            this.context.Refresh(entity);
        }
    }
}
