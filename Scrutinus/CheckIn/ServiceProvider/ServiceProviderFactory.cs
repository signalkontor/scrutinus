﻿// // TPS.net TPS8 CheckIn
// // ServiceProviderFactory.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

namespace CheckIn.ServiceProvider
{
    public static class ServiceProviderFactory
    {
        public static IServiceProvider GetServiceProvider()
        {
            return new SQLServerServiceProvider(CheckInSettings.Default.SQLServer, CheckInSettings.Default.DatabaseName, CheckInSettings.Default.DatabaseUser, CheckInSettings.Default.DatabasePassword);
        }
    }
}
