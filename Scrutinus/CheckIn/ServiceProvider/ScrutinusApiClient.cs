﻿// // TPS.net TPS8 CheckIn
// // ScrutinusApiClient.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using DataModel.JsonModels;
using DataModel.Models;
using General;

namespace CheckIn.ServiceProvider
{
    public class ScrutinusApiClient
    {
        private IEnumerable<AgeGroup> ageGroups;
        private readonly string baseUrl;

        private IEnumerable<Class> classes;

        private IEnumerable<Competition> competitions;

        private IEnumerable<Couple> couples;

        private IEnumerable<Section> sections;

        public ScrutinusApiClient(string baseurl)
        {
            this.baseUrl = baseurl;
        }

        public IEnumerable<Class> GetClasses()
        {
            var jsonClasses = this.GetResource<JsonClass[]>("/Classes");

            var result = new List<Class>();
            foreach (var jsonClass in jsonClasses)
            {
                result.Add(jsonClass.GetClass());
            }

            return result;
        }

        public IEnumerable<AgeGroup> GetAgeGroups()
        {
            var jsonAgeGroups = this.GetResource<JsonAgeGroup[]>("/AgeGroups");

            return jsonAgeGroups.Select(jsonAgeGroup => jsonAgeGroup.GetAgeGroup()).ToList();
        }

        public IEnumerable<Section> GetSections()
        {
            var jsonSections = this.GetResource<JsonSection[]>("/Sections");

            return jsonSections.Select(s => s.GetSection()).ToList();
        }

        public IEnumerable<Couple> GetCouples()
        {
            var jsonCouples = this.GetResource<JsonCouple[]>("/Couples");

            return jsonCouples.Select(jsonCouple => jsonCouple.GetCouple()).ToList();
        }

        public IEnumerable<Competition> GetCompetitions()
        {
            if (this.ageGroups == null || this.classes == null)
            {
                this.ageGroups = this.GetAgeGroups();
                this.classes = this.GetClasses();
            }

            var jsonCompetitions = this.GetResource<JsonCompetition[]>("/Competitions");

            return jsonCompetitions.Select(comp => comp.GetCompetition(this.classes, this.ageGroups));
        }

        public IEnumerable<Startbuch> GetStartbuchsForCouple(int coupleId)
        {
            this.CheckBaseLists();

            var jsonStartbuchs = this.GetResource<JsonStartbuch[]>("/Startbuches/" + coupleId);

            return jsonStartbuchs.Select(s => s.GetStartbuch(this.sections, this.ageGroups, this.classes, this.couples));
        }

        public IEnumerable<Participant> GetParticipants()
        {
            this.CheckBaseLists();

            var jsonParticipants = this.GetResource<IEnumerable<JsonParticipant>>("/Participants");

            return jsonParticipants.Select(j => j.GetParticipants(this.classes, this.ageGroups, this.competitions, this.couples));
        }

        private void CheckBaseLists()
        {
            if (this.ageGroups == null)
            {
                this.ageGroups = this.GetAgeGroups();
            }

            if (this.classes == null)
            {
                this.classes = this.GetClasses();
            }

            if (this.competitions == null)
            {
                this.competitions = this.GetCompetitions();
            }

            if (this.couples == null)
            {
                this.couples = this.GetCouples();
            }

            if (this.sections == null)
            {
                this.sections = this.GetSections();
            }
        }

        private T GetResource<T>(string path)
        {
            using (var client = new WebClient())
            {
                var urlToCall = this.baseUrl + path;

                var content = client.DownloadString(urlToCall);

                if (Directory.Exists(@"C:\Temp\json"))
                {
                    var filename = urlToCall.Replace("/", "_").Replace(":", "");
                    File.WriteAllText(@"C:\Temp\json\" + filename, content);
                }

                return JsonHelper.DeserializeData<T>(content);
            }
        }

        private void SaveResource(object objectToSave, string path)
        {
            using (var client = new WebClient())
            {
                var urlToCall = this.baseUrl + path;

                var json = JsonHelper.SerializeObject(objectToSave, null);

               

                byte[] returnStr = null;

                try
                {

                }
                catch (WebException ex)
                {
                    throw;
                }

            }
        }
    }
}
