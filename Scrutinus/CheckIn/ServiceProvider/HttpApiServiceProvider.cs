﻿// // TPS.net TPS8 CheckIn
// // HttpApiServiceProvider.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using DataModel;
using DataModel.Models;

namespace CheckIn.ServiceProvider
{
    internal class HttpApiServiceProvider : IServiceProvider
    {
        private readonly ScrutinusApiClient apiClient;

        public HttpApiServiceProvider()
        {
            this.apiClient = new ScrutinusApiClient(CheckInSettings.Default.ApiUrl);
        }

        public IEnumerable<Class> GetClasses()
        {
            return this.apiClient.GetClasses();
        }

        public IEnumerable<AgeGroup> GetAgeGroups()
        {
            return this.apiClient.GetAgeGroups();
        }

        public IEnumerable<Couple> GetCouples()
        {
            return this.apiClient.GetCouples();
        }

        public IEnumerable<Competition> GetCompetitions()
        {
            return this.apiClient.GetCompetitions();
        }

        public IEnumerable<Participant> GetParticipants()
        {
            return this.apiClient.GetParticipants();
        }

        public IEnumerable<Section> GetSections()
        {
            return this.apiClient.GetSections();
        }

        public void UpdateStartbuchData(int coupleId, int sectionId, int newPoints, int newPlacings, int newClassId)
        {
            throw new NotImplementedException();
        }


        public IEnumerable<Startbuch> GetStartbuchsForCouple(int coupleId)
        {
            throw new NotImplementedException();
        }

        public void CreateNewParticipant(Participant participant)
        {
            throw new NotImplementedException();
        }

        public Couple CreateNewCouple()
        {
            throw new NotImplementedException();
        }

        public void UpdateParticipantState(int id, CoupleState state)
        {
            throw new NotImplementedException();
        }

        public void UpdateEntity(object entity)
        {
            
        }
    }
}
