﻿// // TPS.net TPS8 CheckIn
// // ScanBarcodeDialog.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using CheckIn.ViewModel;
using DataModel.Models;
using MahApps.Metro.Controls.Dialogs;

namespace CheckIn.View
{
    /// <summary>
    /// Interaction logic for ScanBarcodeDialog.xaml
    /// </summary>
    public partial class ScanBarcodeDialog : CustomDialog
    {
        public ScanBarcodeDialog(IServiceProvider serviceProvider, Couple couple)
        {
            var viewModel = new ScanBarcodeDialogViewModel(serviceProvider, couple);
            this.DataContext = viewModel;
            viewModel.Dialog = this;

            this.InitializeComponent();
        }
    }
}
