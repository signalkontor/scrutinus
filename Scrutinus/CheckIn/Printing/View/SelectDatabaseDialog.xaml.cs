﻿// // TPS.net TPS8 CheckIn
// // SelectDatabaseDialog.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;

namespace CheckIn.View
{
    /// <summary>
    /// Interaction logic for SelectDatabaseDialog.xaml
    /// </summary>
    public partial class SelectDatabaseDialog : Window
    {
        private readonly string sqlServerConnectionString =
            @"Data Source={0};Initial Catalog={3};Persist Security Info=True;User ID={1};Password={2};MultipleActiveResultSets=True";

        public SelectDatabaseDialog()
        {
            this.Databases = new ObservableCollection<string>();
            this.ConnectCommand = new RelayCommand(this.Connect);

            this.InitializeComponent();
        }

        public ObservableCollection<string> Databases { get; set; }

        public string Server { get; set; } = CheckInSettings.Default.SQLServer;

        public string User { get; set; } = CheckInSettings.Default.DatabaseUser;

        public string Password { get; set; } = CheckInSettings.Default.DatabasePassword;

        public string SelectedDatabase { get; set; } = CheckInSettings.Default.DatabaseName;

        public ICommand ConnectCommand { get; set; }

        public ICommand OkCommand { get; set; }

        public ICommand CancelCommand { get; set; }

        private void Connect()
        {
            this.FillDatabases();
        }

        private void FillDatabases()
        {
            if (this.Server != null && !string.IsNullOrEmpty(this.User)
                && !string.IsNullOrEmpty(this.Password))
            {
                var connectionString = this.GetSqlServerConnectionString("master");

                try
                {
                    this.Databases.Clear();

                    var connection = new SqlConnection(connectionString);
                    connection.Open();
                    var command = connection.CreateCommand();
                    command.CommandText = "Select [name] FROM master.dbo.sysdatabases where dbid > 4";
                    var reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        this.Databases.Add(reader.GetString(0));
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Connection could not established: {ex.Message}\r\nYou must set Servername, user and password");
                }
            }
        }

        private string GetSqlServerConnectionString(string databaseName)
        {
            var connectionString = string.Format(
                this.sqlServerConnectionString,
                this.Server,
                this.User,
                this.Password,
                databaseName);

            return connectionString;
        }

        private void OnOkButtonClicked(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            this.Close();
        }

        private void OnCancelButtonClicked(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }
    }
}
