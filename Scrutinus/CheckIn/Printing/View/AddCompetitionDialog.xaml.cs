﻿// // TPS.net TPS8 CheckIn
// // AddCompetitionDialog.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using CheckIn.ViewModel;
using MahApps.Metro.Controls.Dialogs;

namespace CheckIn
{
    /// <summary>
    /// Interaction logic for AddCompetitionDialog.xaml
    /// </summary>
    public partial class AddCompetitionDialog : CustomDialog 
    {
        private AddCompetitionsViewModel viewModel;

        public AddCompetitionDialog(Action closingAction, AddCompetitionsViewModel viewModel)
        {
            this.InitializeComponent();

            this.ViewModel = viewModel;
            this.DataContext = viewModel;
            this.ViewModel.Dialog = this;
            this.ViewModel.ClosingAction = closingAction;
            this.ViewModel.SelectedCompetitions = this.CompetitionListView.SelectedItems;
        }

        public AddCompetitionsViewModel ViewModel
        {
            get
            {
                return this.viewModel;
            }
            set
            {
                this.viewModel = value;
            }
        }
    }
}
