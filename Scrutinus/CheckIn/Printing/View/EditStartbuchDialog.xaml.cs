﻿// // TPS.net TPS8 CheckIn
// // EditStartbuchDialog.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using CheckIn.ViewModel;
using DataModel.Models;
using MahApps.Metro.Controls.Dialogs;

namespace CheckIn.View
{
    /// <summary>
    /// Interaction logic for EditStartbuchDialog.xaml
    /// </summary>
    public partial class EditStartbuchDialog : CustomDialog
    {
        public EditStartbuchDialog(IServiceProvider serviceProvider, Couple couple)
        {
            var viewModel = new EditStartbuchDialogViewModel(serviceProvider, couple);
            this.DataContext = viewModel;

            this.InitializeComponent();
        }
    }
}
