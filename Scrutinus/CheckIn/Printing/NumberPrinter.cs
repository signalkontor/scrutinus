﻿// // TPS.net TPS8 CheckIn
// // NumberPrinter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing.Imaging;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml;
using DataModel.Models;
using Scrutinus.Localization;
using Scrutinus.Reports.BasicPrinting;
using Image = System.Drawing.Image;

namespace CheckIn.Printing
{
    public enum PrintType
    {
        Number,
        Couple,
        CompetitionTitle,
        CompetitionDate,
        Image
    }

    internal class NumberPrintModel
    {
        public NumberPrintModel()
        {
            this.Top = 1;
            this.Width = 10;
            this.Left = 1;
            this.Height = 1;
            this.FontSize = 12;
            this.HorizontalAlignment = TextAlignment.Center;
            this.VerticalAlignment = VerticalAlignment.Center;
            this.Rotation = 0;
            this.PrintType = PrintType.Number;
            
        }

        public double Top { get; set; }

        public double Width { get; set; }

        public double Left { get; set; }

        public double Height { get; set; }

        public double FontSize { get; set; }

        public double Rotation { get; set; }

        public string FileName { get; set; }

        public TextAlignment HorizontalAlignment { get; set; }

        public VerticalAlignment VerticalAlignment { get; set; }

        public PrintType PrintType { get; set; }
    }

    public class NumberPrinter : AbstractPrinter
    {
        private FixedPage page;

        private readonly Participant participant;

        private List<NumberPrintModel> printModels;

        public NumberPrinter(Participant participant)
            : base(participant.Competition)
        {
            this.IsLandScape = false;
            this.participant = participant;

            this.LoadLayout();

            this.CreateContent();
        }

        public bool IsLandScape { get; set; }

        protected override void CreateCustomContent()
        {
            this.page = this.CreatePage();

            foreach (var printModel in this.printModels)
            {
                if (printModel.PrintType != PrintType.Image)
                {
                    var textBlock = new TextBlock()
                    {
                        Text = this.GetTextForPrintType(printModel.PrintType),
                        FontSize = printModel.FontSize,
                        Width = DotsFromCm(printModel.Width),
                        Height = DotsFromCm(printModel.Height),
                        VerticalAlignment = printModel.VerticalAlignment,
                        TextAlignment = printModel.HorizontalAlignment,
                        RenderTransform = new RotateTransform(printModel.Rotation)
                    };
                    this.AddControlToPage(this.page, textBlock, printModel.Left, printModel.Top);
                }
                else
                {
                    using (Stream bitmapStream = File.Open(printModel.FileName, FileMode.Open))
                    {
                        try
                        {
                            var img = Image.FromStream(bitmapStream);
                            var bitmapImage = this.ConvertBitmapToBitmapImage(img);

                            var image = new System.Windows.Controls.Image
                            {
                                Source = bitmapImage,
                                Width = DotsFromCm(printModel.Width),
                                Height = DotsFromCm(printModel.Height),
                                Stretch = Stretch.Fill
                            };

                            this.AddControlToPage(this.page, image, printModel.Left, printModel.Top);
                        }
                        catch (Exception)
                        {
                            Scrutinus.MainWindow.MainWindowHandle.ShowMessage(
                                $"Error loading image {printModel.FileName}");
                        }
                    }
                }
            }
        }

        private BitmapImage ConvertBitmapToBitmapImage(Image bitmap)
        {
            var memoryStream = new MemoryStream();
            bitmap.Save(memoryStream, ImageFormat.Png);
            var bitmapImage = new BitmapImage();
            bitmapImage.BeginInit();
            bitmapImage.StreamSource = new MemoryStream(memoryStream.ToArray());
            bitmapImage.EndInit();

            return bitmapImage;
        }
        private string GetTextForPrintType(PrintType printType)
        {
            switch (printType)
            {
                case PrintType.Number:
                    return this.participant.Number.ToString();
                case PrintType.Couple:
                    return this.participant.Couple.NiceName;
                case PrintType.CompetitionDate:
                    return this.participant.Competition.StartTime.HasValue
                               ? this.participant.Competition.StartTime.Value.ToShortDateString() + " - "
                                 + this.participant.Competition.StartTime.Value.ToShortTimeString()
                               : "";
                case PrintType.CompetitionTitle:
                    return this.participant.Competition.Title;
            }

            return "Unknown print type";
        }

        protected override void AddCommenContent(FixedPage page)
        {
            
        }

        private void LoadLayout()
        {
            var xmlDoc = new XmlDocument();
            xmlDoc.Load("PrintLayout.xml");

            if (xmlDoc.DocumentElement != null && xmlDoc.DocumentElement.Attributes["Orientation"] != null)
            {
                if (xmlDoc.DocumentElement.Attributes["Orientation"].Value.ToLower() == "landscape")
                {
                    this.IsLandScape = true;
                }
            }

            this.printModels = new List<NumberPrintModel>();

            var elements = xmlDoc.GetElementsByTagName("Element");

            try
            {
                foreach (XmlNode element in elements)
                {
                    this.AddElement(element);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Xml Layout could not be parsed!");
            }
            
        }

        private void AddElement(XmlNode element)
        {
            var model = new NumberPrintModel();
            this.printModels.Add(model);

            if (element.Attributes["Top"] != null)
            {
                model.Top = this.GetDouble(element.Attributes["Top"].Value);
            }
            if (element.Attributes["Width"] != null)
            {
                model.Width = this.GetDouble(element.Attributes["Width"].Value);
            }
            if (element.Attributes["Left"] != null)
            {
                model.Left = this.GetDouble(element.Attributes["Left"].Value);
            }
            if (element.Attributes["Height"] != null)
            {
                model.Height = this.GetDouble(element.Attributes["Height"].Value);
            }
            if (element.Attributes["FontSize"] != null)
            {
                model.FontSize = this.GetDouble(element.Attributes["FontSize"].Value);
            }
            if (element.Attributes["Rotation"] != null)
            {
                model.Rotation = this.GetDouble(element.Attributes["Rotation"].Value);
            }
            if (element.Attributes["HorizontalAlignment"] != null)
            {
                model.HorizontalAlignment = this.GetHorizontalAlignment(element.Attributes["HorizontalAlignment"].Value);
            }
            if (element.Attributes["VerticalAlignment"] != null)
            {
                model.VerticalAlignment = this.GetVerticalAlignment(element.Attributes["VerticalAlignment"].Value);
            }
            if (element.Attributes["FileName"] != null)
            {
                model.FileName = element.Attributes["FileName"].Value;
            }
            if (element.Attributes["Type"] != null)
            {
                model.PrintType = this.GetPrintType(element.Attributes["Type"].Value);
            }
        }

        private VerticalAlignment GetVerticalAlignment(string p)
        {
            return (VerticalAlignment )Enum.Parse(typeof(VerticalAlignment), p);
        }

        private TextAlignment GetHorizontalAlignment(string p)
        {
            return (TextAlignment)Enum.Parse(typeof(TextAlignment), p);
        }

        private PrintType GetPrintType(string p)
        {
            return (PrintType)Enum.Parse(typeof(PrintType), p);
        }

        private double GetDouble(string attribute)
        {
            var seperator = Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator;

            return double.Parse(attribute.Replace(".", seperator).Replace(",", seperator));
        }
    }
}
