﻿// // TPS.net TPS8 CheckIn
// // UpdateParticipantHelper.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using Scrutinus.Dialogs.DialogViewModels;

namespace CheckIn.Helper
{
    public static class UpdateParticipantHelper
    {
        public static void UpdateAllParticipants(IServiceProvider serviceProvider, StartbuchViewModel startbuch)
        {
            serviceProvider.UpdateStartbuchData(startbuch.Couple.Id, startbuch.Section .Id, startbuch.OriginalPoints, startbuch.OriginalPlacings, startbuch.Class.Id);
        }
    }
}
