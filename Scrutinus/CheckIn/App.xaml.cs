﻿// // TPS.net TPS8 CheckIn
// // App.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows;
using MahApps.Metro;

namespace CheckIn
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            // get the theme from the current var
            var appTheme = ThemeManager.DetectAppStyle(Current);

            // now use the custom accent
            ThemeManager.ChangeAppStyle(Current,
                                        ThemeManager.GetAccent("Blue"),
                                        appTheme.Item1);

            // get the theme from the current application
            var theme = ThemeManager.GetAppTheme(CheckInSettings.Default.ApplicationTheme) ?? ThemeManager.GetAppTheme("BaseDark");
            var accent = ThemeManager.GetAccent(CheckInSettings.Default.AccentColor) ?? ThemeManager.GetAccent("Blue");

            // now set the Green accent and dark theme
            ThemeManager.ChangeAppStyle(Current, accent, theme);
        }
    }
}
