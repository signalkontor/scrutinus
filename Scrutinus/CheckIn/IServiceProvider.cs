﻿// // TPS.net TPS8 CheckIn
// // IServiceProvider.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using DataModel;
using DataModel.Models;

namespace CheckIn
{
    public interface IServiceProvider
    {
        IEnumerable<Class> GetClasses();

        IEnumerable<AgeGroup> GetAgeGroups();

        IEnumerable<Couple> GetCouples();

        IEnumerable<Competition> GetCompetitions();

        IEnumerable<Participant> GetParticipants();

        IEnumerable<Startbuch> GetStartbuchsForCouple(int coupleId);

        IEnumerable<Section> GetSections();

        void UpdateEntity(object entity);

        void CreateNewParticipant(Participant participant);

        Couple CreateNewCouple();

        void UpdateParticipantState(int id, CoupleState state);

        void UpdateStartbuchData(int coupleId, int sectionId, int newPoints, int newPlacings, int nwClassId);
    }
}
