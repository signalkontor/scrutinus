﻿// // TPS.net TPS8 DataModel
// // HeatsHelper.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using DataModel.Models;

namespace DataModel.ModelHelper
{
    public static class HeatsHelper
    {
        /// <summary>
        /// Saves the created heats (returned from the helper als Lists of List of List to the database
        /// </summary>
        /// <param name="heatDefinition">Lists returnded from the heat creation helpers</param>
        /// <param name="dances">a List aof dances</param>
        /// <param name="roundId">the RoundId of this round</param>
        public static void SaveHeats(
            ScrutinusContext context,
            Dictionary<string, List<List<Participant>>> heatDefinition,
            ICollection<DanceInRound> dances,
            int roundId)
        {
            // Lets create the dance_round structure that saves what dances are in this round used
            var index = 0;
            foreach (var dance in dances)
            {
                index++;
                // Now, add the heats of this dance:
                List<List<Participant>> heats;
                if (heatDefinition.ContainsKey(dance.Dance.ShortName))
                {
                    heats = heatDefinition[dance.Dance.ShortName];
                }
                else if (heatDefinition.ContainsKey("All Dances"))
                {
                    heats = heatDefinition["All Dances"];
                }
                else
                {
                    // .Show("Something wrong with the heats");
                    continue;
                }
                var heatNo = 0;
                foreach (var heat in heats)
                {
                    foreach (var participant in heat)
                    {
                        var p = new Drawing()
                                    {
                                        Participant = participant,
                                        // Dance_Round = dance,
                                        DanceRound = dance,
                                        Heat = heatNo,
                                        //Participant = participant,
                                        Round = context.Rounds.Single(r => r.Id == roundId)
                                    };
                        context.Drawings.Add(p);
                    }
                    heatNo++;
                }
            }

            context.SaveChanges();
        }

        public static Dictionary<DanceInRound, List<List<Participant>>> GetHeats(
            ScrutinusContext context,
            int roundId)
        {
            // Based on the Heats we have we select the participants
            // At the end we need some sort of dictionary
            var result = new Dictionary<DanceInRound, List<List<Participant>>>();
            
            var source =
                context.Drawings.Where(d => d.Round.Id == roundId)
                    .OrderBy(d => d.DanceRound.SortOrder)
                    .ThenBy(d => d.Heat)
                    .ThenBy(d => d.Participant.Number);
            var lastDanceOrder = -1;
            var lastHeat = -1;
            List<List<Participant>> dance = null;
            List<Participant> heat = null;

            foreach (var drawing in source)
            {
                if (drawing.DanceRound.SortOrder != lastDanceOrder)
                {
                    // Group Change:
                    lastDanceOrder = drawing.DanceRound.SortOrder;
                    lastHeat = drawing.Heat;
                    dance = new List<List<Participant>>();
                    heat = new List<Participant>();
                    dance.Add(heat);
                    result.Add(drawing.DanceRound, dance);
                }

                if (lastHeat != drawing.Heat)
                {
                    heat = new List<Participant>();
                    lastHeat = drawing.Heat;
                    dance.Add(heat);
                }

                heat.Add(drawing.Participant);
            }

            return result;
        }
    }
}