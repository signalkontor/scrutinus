﻿// // TPS.net TPS8 DataModel
// // CompetitionHelper.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Linq;
using DataModel.Models;

namespace DataModel.ModelHelper
{
    public static class CompetitionHelper
    {
        public static void DeleteCompetition(ScrutinusContext context, Competition competition)
        {
            // Delete  the original competitions
            foreach (var participant in competition.Participants.ToList())
            {
                foreach (var qualified in participant.Qualifieds.ToList())
                {
                    context.Qualifieds.Remove(qualified);
                }
                participant.Qualifieds.Clear();

                foreach (var draw in context.Drawings.Where(d => d.Participant.Id == participant.Id).ToList())
                {
                    context.Drawings.Remove(draw);
                }

                context.Participants.Remove(participant);
            }
            competition.Participants.Clear();

            var dances = competition.Dances.ToList();
            foreach (var dancesCompetition in dances)
            {
                context.DancesCompetition.Remove(dancesCompetition);
            }

            competition.Dances.Clear();

            context.Competitions.Remove(competition);

            context.SaveChanges();
        }
    }
}
