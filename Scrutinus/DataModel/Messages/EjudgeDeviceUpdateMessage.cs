﻿// // TPS.net TPS8 DataModel
// // EjudgeDeviceUpdateMessage.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using DataModel.Models;

namespace DataModel.Messages
{
    public class EjudgeDeviceUpdateMessage
    {
        public EjudgeDeviceUpdateMessage(Device device)
        {
            this.Device = device;
        }

        public Device Device { get; set; }
    }
}
