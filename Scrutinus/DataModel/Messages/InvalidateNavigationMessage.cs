﻿// // TPS.net TPS8 DataModel
// // InvalidateNavigationMessage.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using DataModel.Models;

namespace DataModel.Messages
{
    public class InvalidateNavigationMessage
    {
        public Competition Competition { get; set; }
    }
}
