﻿// // TPS.net TPS8 DataModel
// // EjudgeLogMessage.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

namespace DataModel.Messages
{
    public class EjudgeLogMessage
    {
        public EjudgeLogMessage(string message)
        {
            this.LogMessage = message;
        }

        public string LogMessage { get; set; }
    }
}
