﻿// // TPS.net TPS8 DataModel
// // EjudgeJudgementUpdateMessage.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

namespace DataModel.Messages
{
    public class EjudgeJudgementUpdateMessage
    {
        public EjudgeJudgementUpdateMessage(string sender, int targetId, string command, string data)
        {
            this.Sender = sender;
            this.TargetId = targetId;
            this.Command = command;
            this.Data = data;
        }

        public string Command { get; set; }

        public string Data { get; set; }

        public string Sender { get; set; }

        public int TargetId { get; set; }

        public bool IsHandled { get; set; }
    }
}
