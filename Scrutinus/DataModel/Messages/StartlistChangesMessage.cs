﻿// // TPS.net TPS8 DataModel
// // StartlistChangesMessage.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using GalaSoft.MvvmLight.Messaging;

namespace DataModel.Messages
{
    public class StartlistChangesMessage : MessageBase
    {
        public int CompetitionId { get; set; }
    }
}