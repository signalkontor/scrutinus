﻿// // TPS.net TPS8 DataModel
// // ItemValueStarNumber.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH
namespace DataModel
{
    public class ItemValueStarNumber
    {
        public int Number { get; set; }

        public string Display { get; set; }
    }
}