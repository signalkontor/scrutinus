﻿// // TPS.net TPS8 DataModel
// // JsonStartbuch.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Linq;
using DataModel.Models;

namespace DataModel.JsonModels
{
    public class JsonStartbuch
    {
        public JsonStartbuch()
        {
            
        }

        public JsonStartbuch(Startbuch startbuch)
        {
            this.AgeGroupId = startbuch.AgeGroup.Id;
            this.SectionId = startbuch.Section.Id;
            this.CoupleId = startbuch.Couple.Id;
            this.ClassId = startbuch.Class.Id;
            this.NextClassId = startbuch.NextClass.Id;
        }

        public int Id { get; set; }

        public int CoupleId { get; set; }

        public int SectionId { get; set; }

        public int AgeGroupId { get; set; }

        public int ClassId { get; set; }

        public int NextClassId { get; set; }

        public int OriginalPoints { get; set; }

        public int Points { get; set; }

        public int TargetPoints { get; set; }

        public int OriginalPlacings { get; set; }

        public int Placings { get; set; }

        public int TargetPlacings { get; set; }

        public int MinimumPoints { get; set; }

        public int PlacingsUpto { get; set; }

        public bool Sylabus { get; set; }

        public bool PrintLaufzettel { get; set; }

        public bool Blocked { get; set; }

        public bool IsSelfGenerated { get; set; }

        public DateTime? BlockedDate { get; set; }

        public Startbuch GetStartbuch(IEnumerable<Section> sections, IEnumerable<AgeGroup> ageGroups, IEnumerable<Class> classes, IEnumerable<Couple> couples)
        {
            var startBuch = new Startbuch();

            startBuch.AgeGroup = ageGroups.Single(a => a.Id == this.AgeGroupId);
            startBuch.Section = sections.Single(s => s.Id == this.SectionId);
            startBuch.Class = classes.Single(c => c.Id == this.ClassId);
            startBuch.NextClass = classes.Single(c => c.Id == this.NextClassId);
            startBuch.Couple = couples.Single(c => c.Id == this.CoupleId);

            return startBuch;
        }
    }
}
