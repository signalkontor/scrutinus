﻿// // TPS.net TPS8 DataModel
// // ParticipantStateUpdate.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

namespace DataModel.JsonModels
{
    public class ParticipantStateUpdate
    {
        public int ParticipantId { get; set; }

        public CoupleState State { get; set; }
    }
}
