﻿// // TPS.net TPS8 DataModel
// // JsonAgeGroup.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using DataModel.Models;

namespace DataModel.JsonModels
{
    public class JsonAgeGroup
    {
        public JsonAgeGroup()
        {
            
        }

        public JsonAgeGroup(AgeGroup ageGroup)
        {
        }

        public int Id { get; set; }

        public string AgeGroupName { get; set; }

        public string ShortName { get; set; }

        public int OrderWhenCombined { get; set; }

        public string AgeGroupCode { get; set; }

        public AgeGroup GetAgeGroup()
        {
          
            var ageGroup = new AgeGroup();

            return ageGroup;
        }
    }
}
