﻿// // TPS.net TPS8 DataModel
// // JsonCompetition.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using DataModel.Models;

namespace DataModel.JsonModels
{
    public class JsonCompetition
    {
        public JsonCompetition()
        {
            
        }

        public JsonCompetition(Competition c)
        {
            this.Id = c.Id;
            this.Title = c.Title;
            this.StartTime = c.StartTime;
            this.AgeGroupId = c.AgeGroup.Id;
            this.ClassId = c.Class.Id;
        }

        public int Id { get; set; }

        public string Title { get; set; }

        public DateTime? StartTime { get; set; }

        public int AgeGroupId { get; set; }

        public int ClassId { get; set; }

        public Competition GetCompetition(IEnumerable<Class> classes, IEnumerable<AgeGroup> ageGroups)
        {
            return null;
        }
    }
}
