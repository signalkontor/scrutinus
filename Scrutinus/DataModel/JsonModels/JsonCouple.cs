﻿// // TPS.net TPS8 DataModel
// // JsonCouple.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using DataModel.Models;

namespace DataModel.JsonModels
{
    public class JsonCouple
    {
        public JsonCouple()
        {
            
        }

        public JsonCouple(Couple c)
        {
            this.Id = c.Id;
            this.FirstNameMan = c.FirstMan;
            this.LastNameMan = c.LastMan;
            this.FirstNameWoman = c.FirstWoman;
            this.LastNameWoman = c.LastWoman;
            this.Country = c.Country;
            this.MINMan = c.MINMan;
            this.MINWoman = c.MINWoman;
            this.ExternalId = c.ExternalId;
        }

        public int Id { get; set; }

        public string FirstNameMan { get; set; }

        public string LastNameMan { get; set; }

        public string FirstNameWoman { get; set; }

        public string LastNameWoman { get; set; }

        public string MINMan { get; set; }

        public string MINWoman { get; set; }

        public string Country { get; set; }

        public string ExternalId { get; set; }

        public Couple GetCouple()
        {
            var couple = new Couple();

            couple.FirstMan = this.FirstNameMan;
            couple.LastMan = this.LastNameMan;
            couple.FirstWoman = this.FirstNameWoman;
            couple.LastWoman = this.LastNameWoman;

            return couple;
        }
    }
}
