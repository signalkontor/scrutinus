﻿// // TPS.net TPS8 DataModel
// // JsonSection.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using DataModel.Models;

namespace DataModel.JsonModels
{
    public class JsonSection
    {
        public JsonSection()
        {
            
        }

        public JsonSection(Section section)
        {
            this.Id = section.Id;
            this.SectionName = section.SectionName;
        }

        public int Id { get; set; }

        public string SectionName { get; set; }

        public Section GetSection()
        {
            return new Section()
                       {
                           Id = this.Id,
                           SectionName = this.SectionName
                       };
        }
    }
}
