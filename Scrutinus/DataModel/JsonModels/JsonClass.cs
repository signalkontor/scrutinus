﻿// // TPS.net TPS8 DataModel
// // JsonClass.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using DataModel.Models;

namespace DataModel.JsonModels
{
    public class JsonClass
    {
        public JsonClass()
        {
            
        }

        public JsonClass(Class cl)
        {

        }

        public int Id { get; set; }

        public string ClassShortName { get; set; }

        public string ClassLongName { get; set; }

        public int OrderWhenCombined { get; set; }

        public Class GetClass()
        {
            var classObj = new Class();


            return classObj;
        }
    }
}
