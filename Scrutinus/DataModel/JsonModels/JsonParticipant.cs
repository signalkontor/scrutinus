﻿// // TPS.net TPS8 DataModel
// // JsonParticipant.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using DataModel.Models;

namespace DataModel.JsonModels
{
    public class JsonParticipant
    {
        public JsonParticipant(Participant p)
        {
            this.CoupleId = p.Couple.Id;
            this.CompetitionId = p.Competition.Id;
            this.ClassId = p.Class.Id;
            this.AgeGroupId = p.AgeGroup.Id;
            this.Number = p.Number;
            this.State = p.State;
            this.OriginalPoints = p.OriginalPoints;
            this.OriginalPlacings = p.OriginalPlacings;
            if (p.TargetClass != null)
            {
                this.TargetClassId = p.TargetClass.Id;
            }
            else
            {
                this.TargetClassId = null;
            }
        }

        public int CoupleId { get; set; }

        public int CompetitionId { get; set; }

        public int ClassId { get; set; }

        public int AgeGroupId { get; set; }

        public int Number { get; set; }

        public CoupleState State { get; set; }

        public int? OriginalPoints { get; set; }

        public int? OriginalPlacings { get; set; }

        public int? TargetClassId { get; set; }

        public Participant GetParticipants(
            IEnumerable<Class> classes,
            IEnumerable<AgeGroup> ageGroups,
            IEnumerable<Competition> competitions,
            IEnumerable<Couple> couples)
        {
            var participant = new Participant()
                                  {
                                      Class = classes.Single(c => c.Id == this.ClassId),
                                      TargetClass = classes.Single(c => c.Id == this.TargetClassId),
                                      Couple = couples.Single(c => c.Id == this.CoupleId),
                                      Competition = competitions.Single(c => c.Id == this.CompetitionId)
                                  };
            return participant;
        }
    }
}
