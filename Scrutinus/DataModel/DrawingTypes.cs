﻿// // TPS.net TPS8 DataModel
// // DrawingTypes.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH
namespace DataModel
{
    public static class DrawingTypes
    {
        public const int Random = 1;

        public const int FixedHeatsAsc = 2;

        public const int FixedHeatsDesc = 3;

        public const int FixedPerDance = 4;

        public const int GrandSlamFinal = 5;
    }
}