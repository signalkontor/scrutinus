﻿// // TPS.net TPS8 DataModel
// // MarkNumberType.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH
namespace DataModel
{
    public enum MarkNumberType
    {
        Undefined = 0,

        Manual,

        Half,

        TwoThird,

        HalfToTwoThird
    }
}