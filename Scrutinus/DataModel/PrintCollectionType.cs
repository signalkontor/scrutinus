﻿// // TPS.net TPS8 DataModel
// // PrintCollectionType.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH
namespace DataModel
{
    public enum PrintCollectionType
    {
        BeforeFirstRound,
        BeforeQualificationRound,
        BeforeFinal,
        AfterQualificationRound,
        AfterFinal
    }
}