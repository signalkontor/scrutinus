﻿// // TPS.net TPS8 DataModel
// // MigrateDatabase.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Data.Entity.Migrations;
using System.Diagnostics;
using DataModel.Models;

namespace DataModel.Migrations
{
    public class MigrateDatabase : DbMigration
    {
        public static void Migrate(ScrutinusContext context)
        {
            var version = context.GetParameter<int>("DatabaseVersion", 0);

            if (version == 0)
            {
                AddWdsfCodeColumn(context);
                version = 1;
            }

            if (version == 1)
            {
                CreateNewIndexes(context);
                
                version = 2;
            }

            if (version == 2)
            {
                AddIsReadOnlyColumn(context);

                version = 3;
            }

            if (version == 3)
            {
                AddJsCutOff(context);

                version = 4;
            }

            if (version == 4)
            {
                AddNationality(context);

                version = 5;
            }

            context.SetParameter("DatabaseVersion", version);
        }

        private static void AddWdsfCodeColumn(ScrutinusContext context)
        {
            try
            {
                context.Database.ExecuteSqlCommand(GetAlterTableCommand(context, "ALTER TABLE Dance ADD Column WdsfCode NVARCHAR(5) NULL"));
                context.Database.ExecuteSqlCommand("Update Dance Set WdsfCode=[ShortName]");
            }
            catch (Exception) { }
        }

        private static void AddIsReadOnlyColumn(ScrutinusContext context)
        {
            try
            {
                context.Database.ExecuteSqlCommand(GetAlterTableCommand(context, "ALTER TABLE Couple ADD Column IsReadOnly BIT NULL"));
                context.Database.ExecuteSqlCommand("Update Couple Set IsReadOnly = 0");
                context.Database.ExecuteSqlCommand("ALTER TABLE Couple IsReadOnly BIT NOT NULL");
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }

        private static void CreateNewIndexes(ScrutinusContext context)
        {
            try
            {
                context.Database.ExecuteSqlCommand(
                "CREATE UNIQUE INDEX [IX_ParameterName] ON [Parameters] ([ParameterName] ASC);");
            }
            catch (Exception) { }
        }

        private static void AddJsCutOff(ScrutinusContext context)
        {
            try
            {
                context.Database.ExecuteSqlCommand(GetAlterTableCommand(context, "ALTER TABLE Rounds ADD Column Js3CutOffValue float NULL"));
            }
            catch (Exception) { }


            try
            {
                // SQL Server Variante
                context.Database.ExecuteSqlCommand("ALTER TABLE Rounds ADD Column Js3CutOffValue float(53) NULL");
            }
            catch (Exception) { }

            try
            {
                context.Database.ExecuteSqlCommand("Update Rounds Set Js3CutOffValue=1.3");
            }
            catch (Exception) { }
        }

        private static void AddNationality(ScrutinusContext context)
        {
            try
            {
                context.Database.ExecuteSqlCommand(GetAlterTableCommand(context, "ALTER TABLE Official ADD Column Nationality_Id int NULL"));
            }
            catch (Exception)
            {
            }

        }

        public override void Up()
        {
            throw new NotImplementedException();
        }

        private static string GetAlterTableCommand(ScrutinusContext context, string sql)
        {
            if (context.Database.Connection.ConnectionString.Contains(".sdf"))
            {
                return sql.Replace("Column", "");
            }

            return sql;
        }
    }
}
