// // TPS.net TPS8 DataModel
// // Configuration.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System.Data.Entity.Migrations;
using DataModel.Models;

namespace DataModel.Migrations
{
    public sealed class Configuration  : DbMigrationsConfiguration<ScrutinusContext>
    {
        public Configuration()
        {
            this.AutomaticMigrationsEnabled = true;
            this.AutomaticMigrationDataLossAllowed = false;
            this.ContextKey = "DataModel.Models.ScrutinusContext";
        }
    }
}