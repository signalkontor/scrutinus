﻿// // TPS.net TPS8 DataModel
// // NewJudgingSystemCaluclationType.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH
namespace DataModel
{
    public enum NewJudgingSystemCaluclationType
    {
        V2 = 1,

        V2014 = 2,

        Formation = 3,

        V1 = 4,

        JS3 = 5
    }
}