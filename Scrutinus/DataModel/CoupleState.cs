﻿// // TPS.net TPS8 DataModel
// // CoupleState.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH
namespace DataModel
{
    public enum CoupleState
    {
        Dancing = 0,

        Missing = 1,

        Excused = 2,

        DroppedOut = 3,

        Disqualified = 4,

        Unknown = 5
    }
}