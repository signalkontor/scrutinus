﻿// // TPS.net TPS8 DataModel
// // HongKongImporter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.IO;
using System.Linq;
using DataModel.Models;
using OfficeOpenXml;

namespace DataModel.Import
{
    public class HongKongImporter : IFileImporter
    {
        public IEnumerable<ImportViewModel> ImportFile(ScrutinusContext context, string filename, Competition defaultCompetition)
        {
            var result = new List<ImportViewModel>();

            var excelPackage = new ExcelPackage(new FileInfo(filename));

            var sheet = excelPackage.Workbook.Worksheets[1];

            var line = 6;

            while (line < 41)
            {
                var couple = new Couple()
                {
                    FirstMan = sheet.Cells[line, 4].Text,
                    LastMan = sheet.Cells[line, 5].Text,
                    FirstWoman = sheet.Cells[line, 6].Text,
                    LastWoman = sheet.Cells[line, 7].Text,
                    Country = sheet.Cells[line, 8].Text,
                    IsReadOnly = false
                };

                var existing =
                    context.Couples.FirstOrDefault(
                        c =>
                            c.FirstMan == couple.FirstMan && c.LastMan == couple.LastMan &&
                            c.FirstWoman == couple.FirstWoman && c.LastWoman == couple.LastWoman);

                if (existing == null)
                {
                    context.Couples.Add(couple);
                }
                else
                {
                    couple = existing;
                }

                var competitions = this.GetCompetitions(sheet, couple, line, context);

                result.AddRange(competitions);

                line++;
            }

            return result;
        }

        private List<ImportViewModel> GetCompetitions(ExcelWorksheet sheet, Couple couple, int line, ScrutinusContext context)
        {
            var result = new List<ImportViewModel>();

            if (sheet.Cells[line, 10].Text == "1")
            {
                result.Add(new ImportViewModel()
                {
                    Competition = context.Competitions.FirstOrDefault(c => c.ExternalId == "1"),
                    Couple = couple,
                    Number = int.Parse(sheet.Cells[line, 2].Text),
                    RegisterCouple = true,
                    State = 4
                });
            }

            if (sheet.Cells[line, 11].Text == "1")
            {
                result.Add(new ImportViewModel()
                {
                    Competition = context.Competitions.FirstOrDefault(c => c.ExternalId == "2"),
                    Couple = couple,
                    Number = int.Parse(sheet.Cells[line, 2].Text),
                    RegisterCouple = true,
                    State = 4
                });
            }

            if (sheet.Cells[line, 12].Text == "1")
            {
                result.Add(new ImportViewModel()
                {
                    Competition = context.Competitions.FirstOrDefault(c => c.ExternalId == "3"),
                    Couple = couple,
                    Number = int.Parse(sheet.Cells[line, 2].Text),
                    RegisterCouple = true,
                    State = 4
                });
            }

            if (sheet.Cells[line, 13].Text == "1")
            {
                result.Add(new ImportViewModel()
                {
                    Competition = context.Competitions.FirstOrDefault(c => c.ExternalId == "4"),
                    Couple = couple,
                    Number = int.Parse(sheet.Cells[line, 2].Text),
                    RegisterCouple = true,
                    State = 4
                });
            }

            if (sheet.Cells[line, 14].Text == "1")
            {
                result.Add(new ImportViewModel()
                {
                    Competition = context.Competitions.FirstOrDefault(c => c.ExternalId == "5"),
                    Couple = couple,
                    Number = int.Parse(sheet.Cells[line, 2].Text),
                    RegisterCouple = true,
                    State = 4
                });
            }

            if (sheet.Cells[line, 15].Text == "1")
            {
                result.Add(new ImportViewModel()
                {
                    Competition = context.Competitions.FirstOrDefault(c => c.ExternalId == "6"),
                    Couple = couple,
                    Number = int.Parse(sheet.Cells[line, 3].Text),
                    RegisterCouple = true,
                    State = 4
                });
            }

            if (sheet.Cells[line, 16].Text == "1")
            {
                result.Add(new ImportViewModel()
                {
                    Competition = context.Competitions.FirstOrDefault(c => c.ExternalId == "7"),
                    Couple = couple,
                    Number = int.Parse(sheet.Cells[line, 3].Text),
                    RegisterCouple = true,
                    State = 4
                });
            }

            if (sheet.Cells[line, 17].Text == "1")
            {
                result.Add(new ImportViewModel()
                {
                    Competition = context.Competitions.FirstOrDefault(c => c.ExternalId == "8"),
                    Couple = couple,
                    Number = int.Parse(sheet.Cells[line, 3].Text),
                    RegisterCouple = true,
                    State = 4
                });
            }

            if (sheet.Cells[line, 18].Text == "1")
            {
                result.Add(new ImportViewModel()
                {
                    Competition = context.Competitions.FirstOrDefault(c => c.ExternalId == "9"),
                    Couple = couple,
                    Number = int.Parse(sheet.Cells[line, 3].Text),
                    RegisterCouple = true,
                    State = 4
                });
            }

            if (sheet.Cells[line, 19].Text == "1")
            {
                result.Add(new ImportViewModel()
                {
                    Competition = context.Competitions.FirstOrDefault(c => c.ExternalId == "10"),
                    Couple = couple,
                    Number = int.Parse(sheet.Cells[line, 3].Text),
                    RegisterCouple = true,
                    State = 4
                });
            }

            return result;
        }
    }
}
