﻿// // TPS.net TPS8 DataModel
// // TPSImporter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using DataModel.Models;
using General.Interfaces;

namespace DataModel.Import
{
    public class TPSImporter : IFileImporter
    {
        private readonly IReportProgress reportProgress;

        public TPSImporter(IReportProgress reportProgress)
        {
            this.reportProgress = reportProgress;
        }

        public IEnumerable<ImportViewModel> ImportFile(
            ScrutinusContext context,
            string filename,
            Competition defaultCompetition)
        {
            var competitions = context.Competitions.ToList();
            var couples = context.Couples.ToList();

            var list = new List<ImportViewModel>();

            var lines = File.ReadAllLines(filename, Encoding.Default);

            for (var i=0;i<lines.Length;i++)
            {
                this.reportProgress.ReportStatus("Reading file", i, lines.Length);
                ImportViewModel import = null;

                try
                {
                    var line = lines[i];
                    var data = line.Split(';');
                    if (data.Length < 2)
                    {
                        data = line.Split(',');
                    }
                    if (data.Length < 2)
                    {
                        data = line.Split('\t');
                    }

                    if (data.Length < 10)
                    {
                        continue;
                    }

                    var competitionIds = data[0].Split(',');

                    foreach (var competitionId in competitionIds)
                    {
                        import = new ImportViewModel() {State = 0};
                        list.Add(import);

                        // Find Competition by CompetitionID

                        import.Competition = competitions.FirstOrDefault(c => c.ExternalId == competitionId.Trim());
                        if (import.Competition == null)
                        {
                            import.State = 1; // Not found
                        }

                        import.RegisterCouple = data[1] == "1";

                        var minman = "";
                        var minwoman = "";

                        if (data[10].Contains(" and ") || data[10].Contains(" - "))
                        {
                            var mins = data[10].Split(' ');
                            minman = mins[0].Trim();
                            minwoman = mins[2].Trim();
                        }
                        else
                        {
                            if (data[10].Contains("_"))
                            {
                                var mins = data[10].Split('_');
                                minman = mins[0];
                                minwoman = mins[1];
                            }
                        }

                        import.Couple =
                            couples.FirstOrDefault(
                                c =>
                                    c.FirstMan == data[4] && c.LastMan == data[5] && c.FirstWoman == data[6]
                                    && c.LastWoman == data[7]);

                        import.State += import.Couple == null ? 2 : 0;

                        if (import.Couple == null)
                        {
                            import.Couple = new Couple()
                            {
                                FirstMan = data[4],
                                LastMan = data[5],
                                FirstWoman = data[6],
                                LastWoman = data[7],
                                Country = data[8],
                                Region = data[9],
                                MINMan = minman,
                                MINWoman = minwoman
                            };
                        }
                        if (import.Couple.MINMan == "" || import.Couple.MINWoman == "")
                        {
                            import.State += 4; // No MIN
                        }

                        var number = 0;
                        if (Int32.TryParse(data[11], out number))
                        {
                            import.Number = number;
                        }
                        else
                        {
                            import.State += 16;
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (import != null)
                    {
                        import.State = 32; // Error -> wrong format of line ...
                    }
                    
                    Debug.WriteLine(ex.Message);
                }
            }

            this.reportProgress.RemoveStatusReport();

            return list;
        }
    }
}