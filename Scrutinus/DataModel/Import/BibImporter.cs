﻿// // TPS.net TPS8 DataModel
// // BibImporter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DataModel.Models;
using OfficeOpenXml;

namespace DataModel.Import
{
    internal class BibImporter : IFileImporter
    {
        public IEnumerable<ImportViewModel> ImportFile(ScrutinusContext context, string filename, Competition defaultCompetition)
        {
            // Set number of all particiants to 0:
            foreach (var participant in context.Participants)
            {
                participant.Number = 0;
            }

            foreach (var couple in context.Couples)
            {
                couple.NumberInEvent = 0;
            }

            context.SaveChanges();

            var excelPackage = new ExcelPackage(new FileInfo(filename));
            var sheet = excelPackage.Workbook.Worksheets["ListeNomH"];

            var line = 2;
            while (sheet.Cells[line, 1].Value != null)
            {
                var lastName = sheet.Cells[line, 1].Value.ToString();
                var firstName = sheet.Cells[line, 2].Value.ToString();
                var number = Convert.ToInt32(sheet.Cells[line, 6].Value);

                var couples = context.Couples.Where(c => c.LastMan == lastName && c.FirstMan == firstName);
                foreach (var couple in couples)
                {
                    couple.NumberInEvent = number;
                    foreach (var particiant in couple.Participants)
                    {
                        particiant.Number = number;
                    }
                }

                line++;
            }

            context.SaveChanges();

            return new List<ImportViewModel>();
        }
    }
}
