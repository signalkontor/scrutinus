﻿// // TPS.net TPS8 DataModel
// // ExcelImporter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.IO;
using System.Linq;
using DataModel.Models;
using OfficeOpenXml;

namespace DataModel.Import
{
    public class ExcelImporter : IFileImporter
    {
        private List<Competition> competitions;

        public IEnumerable<ImportViewModel> ImportFile(ScrutinusContext context, string filename, Competition defaultCompetition)
        {
            var result = new List<ImportViewModel>();

            var excelPackage = new ExcelPackage(new FileInfo(filename));

            var sheet = excelPackage.Workbook.Worksheets[1];

            this.ReadCompetitions(context, sheet);

            var line = 2;

            while (sheet.Cells[line, 1].Value != null)
            {
                var number = sheet.Cells[line, 1].GetValue<int>();
                var firstNameMan = sheet.Cells[line, 2].GetValue<string>();
                var lastNameMan = sheet.Cells[line, 3].GetValue<string>();
                var minMan = sheet.Cells[line, 4].GetValue<string>();
                var firstNameWoman = sheet.Cells[line, 5].GetValue<string>();
                var lastNameWoman = sheet.Cells[line, 6].GetValue<string>();
                var minWoman = sheet.Cells[line, 7].GetValue<string>();
                var country = sheet.Cells[line, 8].GetValue<string>();

                // Check if we have the couple:
                var couple = context.Couples.FirstOrDefault(c => c.FirstMan == firstNameMan 
                                                                 && c.LastMan == lastNameMan
                                                                 && c.FirstWoman == firstNameWoman 
                                                                 && c.Country == country
                                                                 && c.LastWoman == lastNameWoman);

                if (couple == null)
                {
                    couple = new Couple()
                    {
                        FirstMan = firstNameMan != null ? firstNameMan.Trim() : "",
                        LastMan = lastNameMan != null ? lastNameMan.Trim() : "",
                        FirstWoman = firstNameWoman != null ? firstNameWoman.Trim() : "",
                        LastWoman = lastNameWoman != null ? lastNameWoman.Trim() : "",
                        MINMan = minMan != null ? minMan.Trim() : "",
                        MINWoman = minWoman != null ? minWoman.Trim() : "",
                        Country = country != null ? country.Trim() : ""
                    };

                    context.Couples.Add(couple);
                }
                else
                {
                    couple.MINMan = minMan;
                    couple.MINWoman = minWoman;
                }

                for (var column = 9; column < 9 + this.competitions.Count; column++)
                {
                    if (sheet.Cells[line, column].GetValue<string>()?.ToLower() == "x")
                    {
                        var model = new ImportViewModel()
                                        {
                                            Competition = this.competitions[column - 9],
                                            Couple = couple,
                                            Number = number,
                                            RegisterCouple = true,
                                            State = 4
                                        };

                        result.Add(model);
                    }
                }
                
                line ++;
            }

            context.SaveChanges();

            return result;
        }

        private void ReadCompetitions(ScrutinusContext context, ExcelWorksheet sheet)
        {
            var col = 9;

            this.competitions = new List<Competition>();

            while (sheet.Cells[1, col].Value != null)
            {
                var externalId = sheet.Cells[1, col].Value.ToString();

                var competition = context.Competitions.SingleOrDefault(c => c.ExternalId == externalId);

                if (competition == null)
                {
                    competition = new Competition()
                                      {
                                          Event = context.Events.First(),
                                          AgeGroup = context.AgeGroups.First(),
                                          Class = context.Classes.First(),
                                          CompetitionType = context.CompetitionTypes.First(),
                                          Section = context.Sections.First(),
                                          ExternalId = externalId,
                                          Title = externalId
                                      };
                    context.Competitions.Add(competition);
                    context.SaveChanges();
                }

                this.competitions.Add(competition);

                col++;
            }
        }
    }
}
