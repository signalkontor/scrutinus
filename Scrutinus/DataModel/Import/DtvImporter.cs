﻿// // TPS.net TPS8 DataModel
// // DtvImporter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using DataModel.Models;
using General.Interfaces;

namespace DataModel.Import
{
    internal class DtvImporter : IFileImporter
    {
        private readonly IReportProgress reportProgress;

        public DtvImporter(IReportProgress reportProgress)
        {
            this.reportProgress = reportProgress;
        }

        public IEnumerable<ImportViewModel> ImportFile(ScrutinusContext context, string filename, Competition defaultCompetition)
        {
            var competitions = context.Competitions.ToList();
            var couples = context.Couples.ToList();

            var list = new List<ImportViewModel>();

            var lines = File.ReadAllLines(filename);

            for(var lineIndex=0;lineIndex<lines.Length;lineIndex++)
            {
                this.reportProgress.ReportStatus("Reading file", lineIndex, lines.Length);

                var import = new ImportViewModel() { State = 0 };

                try
                {
                    var line = lines[lineIndex];
                    var data = line.Split(';');
                    if (data.Length < 2)
                    {
                        data = line.Split(',');
                    }
                    if (data.Length < 2)
                    {
                        data = line.Split('\t');
                    }

                    if (data.Length < 12)
                    {
                        continue;
                    }

                    for (var i=0;i<data.Length;i++)
                    {
                        if (data[i].StartsWith("\""))
                        {
                            data[i] = data[i].Substring(1, data[i].Length - 2);
                        }
                    }

                    list.Add(import);

                    import.RegisterCouple = data[2] == "1";

                    // Find Competition by CompetitionID
                    import.Competition = competitions.FirstOrDefault(c => c.ExternalId == data[0]);
                    if (import.Competition == null)
                    {
                        import.State = 1; // Not found
                    }

                    import.Couple =
                        couples.FirstOrDefault(
                            c =>
                            c.FirstMan == data[6] && c.LastMan == data[7] && c.FirstWoman == data[8]
                            && c.LastWoman == data[9]);

                    import.State += import.Couple == null ? 2 : 0;

                    if (import.Couple == null)
                    {
                        import.Couple = new Couple()
                        {
                            FirstMan = data[6],
                            LastMan = data[7],
                            FirstWoman = data[8],
                            LastWoman = data[9],
                            Country = data[10],
                            Region = data[11],
                        };
                    }

                    var number = 0;
                    if (Int32.TryParse(data[12], out number))
                    {
                        import.Number = number;
                    }
                    else
                    {
                        import.State += 16;
                    }
                }
                catch (Exception ex)
                {
                    import.State = 32; // Error -> wrong format of line ...
                    Debug.WriteLine(ex.Message);
                }
            }

            this.reportProgress.RemoveStatusReport();

            return list;
        }
    }
}
