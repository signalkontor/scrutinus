﻿// // TPS.net TPS8 DataModel
// // ImportWDSF.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.IO;
using DataModel.Models;

namespace DataModel.Import
{
    public class CheckImport
    {
        public int TotalItems { get; set; }

        public int NewItems { get; set; }

        public int ExistingItems { get; set; }

        public List<Participant> ToImport { get; set; }

        public List<Couple> NewCouples { get; set; }
    }

    public class ImportWDSF
    {
        private readonly ScrutinusContext _context;

        public ImportWDSF(ScrutinusContext context)
        {
            this._context = context;
        }

        public CheckImport Import(string file, ref string errorMessage)
        {
            var sr = new StreamReader(file, true);

            while (!sr.EndOfStream)
            {
                var data = sr.ReadLine().Split(';');
            }

            return null;
        }

        public bool Commit(CheckImport import, ref string errorMessage)
        {
            try
            {
                foreach (var couple in import.NewCouples)
                {
                    this._context.Couples.Add(couple);
                }
                this._context.SaveChanges();

                foreach (var participan in import.ToImport)
                {
                    this._context.Participants.Add(participan);
                }
                this._context.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                    msg += ": " + ex.Message;
                }
                errorMessage = msg;
            }

            return false;
        }
    }
}