﻿// // TPS.net TPS8 DataModel
// // FileImporterFactory.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.IO;
using General.Interfaces;

namespace DataModel.Import
{
    public class FileImporterFactory
    {
        public static IFileImporter GetFileImporter(string filename, IReportProgress reportProgress)
        {
            var info = new FileInfo(filename);
            switch (info.Extension.ToLower())
            {
                case ".tps":
                    return new TPSImporter(reportProgress);
                case ".txt":
                case ".csv":
                    return new ScrutinusImporter(reportProgress);
                case ".dtv":
                    return new DtvImporter(reportProgress);
                case ".dallas":
                    return new WDSFScoreImporter();
                case ".top":
                    return new TopTurnierImporter();
                case ".xlsx":
                    if (filename.Contains("Athlete list to Olav"))
                    {
                        return new HongKongImporter();
                    }

                    return new ExcelImporter();
                case ".master":
                    return new MasterGamesImporter();
                case ".bib":
                    return new BibImporter();
                case ".heikki":
                    return new HeikkiImporter();
                case ".taipei":
                    return new TaipeiImporter();
            }

            return null;
        }
    }
}