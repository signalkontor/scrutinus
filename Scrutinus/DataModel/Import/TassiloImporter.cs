﻿// // TPS.net TPS8 DataModel
// // TassiloImporter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DataModel.Models;
using OfficeOpenXml;

namespace DataModel.Import
{
    internal class TassiloImporter : IFileImporter
    {
        private Competition competition;
        private ScrutinusContext context;

        public IEnumerable<ImportViewModel> ImportFile(
            ScrutinusContext context,
            string filename,
            Competition defaultCompetition)
        {
            this.context = context;

            var result = new List<ImportViewModel>();

            var excelPackage = new ExcelPackage(new FileInfo(filename));

            var sheet = excelPackage.Workbook.Worksheets[1];

            this.ReadCompetition(sheet);

            var line = 3;

            while (sheet.Cells[line, 1].Value != null)
            {
                var minMan = sheet.Cells[line, 6].Text;
                var minWoman = sheet.Cells[line, 7].Text;

                var couple = context.Couples.FirstOrDefault(c => c.WdsfIdMan == minMan && c.WdsfIdWoman == minWoman);

                if (couple == null)
                {
                    couple = new Couple()
                                 {
                                     WdsfIdMan = minMan,
                                     WdsfIdWoman = minWoman,
                                     LastMan = sheet.Cells[line, 2].Text,
                                     FirstMan = sheet.Cells[line, 3].Text,
                                     LastWoman = sheet.Cells[line, 4].Text,
                                     FirstWoman = sheet.Cells[line, 5].Text,
                                     Country = sheet.Cells[line, 9].Text,
                                     Region = sheet.Cells[line, 8].Text,
                                 };
                    context.Couples.Add(couple);
                }

                result.Add(new ImportViewModel()
                               {
                                   Competition = this.competition,
                                   Couple = couple,
                                   RegisterCouple = true,
                                   State = 0, // Couple exists already, do not create new!
                                   Number = Convert.ToInt32(sheet.Cells[line, 1].Value)
                               });

                line++;
            }

            return result;
        }

        private void ReadCompetition(ExcelWorksheet sheet)
        {
            var title = sheet.Cells[1, 1].Text;

            this.competition = this.context.Competitions.FirstOrDefault(c => c.Title == title);
            if (this.competition == null)
            {
                this.competition = new Competition()
                       {
                           Event = this.context.Events.First(),
                           Title = title,
                           AgeGroup = this.context.AgeGroups.Single(a => a.Id == 10),
                           Section = this.context.Sections.Single(s => s.Id == 1),
                           CompetitionType = this.context.CompetitionTypes.Single(c => c.Id == 23),
                           Class = this.context.Classes.Single(c => c.Id == 6)
                       };

                this.context.Competitions.Add(this.competition);
            }
        }
    }
}
