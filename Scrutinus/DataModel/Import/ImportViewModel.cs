﻿// // TPS.net TPS8 DataModel
// // ImportViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using DataModel.Models;

namespace DataModel.Import
{
    public class ImportViewModel
    {
        public Couple Couple { get; set; }

        public Competition Competition { get; set; }

        public int Number { get; set; }

        /// <summary>
        /// Bitflag:
        /// 1: Competition not found
        /// 2: Couple is new
        /// 4: Missing MIN Id's
        /// 8: Not an valid WDSF Couple
        /// 16: Wrong N umber Format
        /// 32: Wrong line format
        /// </summary>
        public int State { get; set; }

        public bool RegisterCouple { get; set; }
    }
}