﻿// // TPS.net TPS8 DataModel
// // HeikkiImporter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DataModel.Models;
using OfficeOpenXml;
// using System.Collections.Generic;

namespace DataModel.Import
{
    internal class HeikkiImporter : IFileImporter
    {
        public IEnumerable<ImportViewModel> ImportFile(ScrutinusContext context, string filename, Competition defaultCompetition)
        {
            // Set number of all particiants to 0:
            foreach (var participant in context.Participants)
            {
                participant.Number = 0;
            }

            foreach (var couple in context.Couples)
            {
                couple.NumberInEvent = 0;
            }

            context.SaveChanges();

            var excelPackage = new ExcelPackage(new FileInfo(filename));
            var sheet = excelPackage.Workbook.Worksheets[1];
            Competition currentCompetition = null;
            
            var line = 19;
            while (line < 500)
            {
                line++;

                var number = sheet.Cells[line, 2].Value;
                var names = sheet.Cells[line, 3].Value;
                var country = sheet.Cells[line, 4].Value;

                if (number == null && names == null)
                {
                    continue;
                }

                if (number == null)
                {
                    // names contains compeititon title:
                    currentCompetition = context.Competitions.SingleOrDefault(c => c.Title == names.ToString());
                    if (currentCompetition == null)
                    {
                        currentCompetition = new Competition()
                                          {
                                              AgeGroup = context.AgeGroups.First(),
                                              Class = context.Classes.First(),
                                              Section = context.Sections.First(),
                                              CompetitionType = context.CompetitionTypes.First(),
                                              Event = context.Events.First(),
                                              Title = names.ToString()
                                          };

                        context.Competitions.Add(currentCompetition);
                        context.SaveChanges();
                        
                    }

                    continue;
                }

                // Create a new Participant and / or couple:
                var data = names.ToString().Split('-');

                if (data.Count() < 2)
                {
                    continue;
                }

                for (var i = 0; i < data.Length; i++)
                {
                    data[i] = data[i].Trim();
                }

                var mansNames = data[0].Split(' ');

                var firstMan = "";
                var lastMan = "";
                var firstWoman = "";
                var lastWoman = "";

                this.GetNames(mansNames, out firstMan, out lastMan);

                var womanNames = data[1].Split(' ');

                this.GetNames(womanNames, out firstWoman, out lastWoman);

                var couple =
                    context.Couples.FirstOrDefault(
                        c =>
                        c.FirstMan == firstMan && c.LastMan == lastMan && c.FirstWoman == firstWoman
                        && c.LastWoman == lastWoman && c.Country == country.ToString());

                if (couple == null)
                {
                    couple = new Couple()
                                 {
                                     FirstMan = firstMan,
                                     LastMan = lastMan,
                                     FirstWoman = firstWoman,
                                     LastWoman = lastWoman,
                                     Country = country != null ? country.ToString() : ""
                                 };

                    context.Couples.Add(couple);
                    context.SaveChanges();
                }

                var link = sheet.Cells[line, 3].Hyperlink;
                if (link != null)
                {
                    var elements = link.ToString().Split('/');
                    couple.ExternalId = elements.Length > 0 ? elements[elements.Length - 1] : "";
                }

                var backNumber = Convert.ToInt32(number);

                var participant =
                    context.Participants.FirstOrDefault(
                        p => p.Competition.Id == currentCompetition.Id && p.Number == backNumber);

                if (participant == null)
                {
                    participant = new Participant()
                                      {
                                          Competition = currentCompetition,
                                          Couple = couple,
                                          Number = backNumber
                                      };
                    context.Participants.Add(participant);
                    context.SaveChanges();
                }
            }

            context.SaveChanges();

            return new List<ImportViewModel>();
        }

        private bool GetNames(string[] names, out string firstName, out string lastName)
        {
            for (var i = 0; i < names.Length; i++)
            {
                names[i] = names[i].Trim();
            }

            lastName = names[names.Length - 1];

            firstName = string.Join(" ", names, 0, names.Length - 1);

            return true;
        }
    }
}
