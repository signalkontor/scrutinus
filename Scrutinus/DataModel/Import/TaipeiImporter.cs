﻿// // TPS.net TPS8 DataModel
// // TaipeiImporter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.IO;
using System.Linq;
using DataModel.Models;
using OfficeOpenXml;

namespace DataModel.Import
{
    internal class TaipeiImporter : IFileImporter
    {
        private ScrutinusContext context;

        public IEnumerable<ImportViewModel> ImportFile(ScrutinusContext context, string filename, Competition defaultCompetition)
        {
            this.context = context;

            var result = new List<ImportViewModel>();

            var excelPackage = new ExcelPackage(new FileInfo(filename));

            var sheet = excelPackage.Workbook.Worksheets[1];

            var line = 1;

            while (sheet.Cells[line, 1].Value != null)
            {
                var competitionTitle = sheet.Cells[line, 6].Text;
                var competition = this.GetCompetition(competitionTitle);

                var number = sheet.Cells[line, 1].GetValue<int>();
                var nameMan = sheet.Cells[line, 7].GetValue<string>();
                
                var nameWoman = sheet.Cells[line, 8].GetValue<string>();
                
                var country = sheet.Cells[line, 3].GetValue<string>();

                var couple = context.Couples.FirstOrDefault(c => c.FirstMan == nameMan && c.FirstWoman == nameWoman && c.Country == country);

                if (couple == null)
                {
                    couple = new Couple()
                    {
                        FirstMan = nameMan != null ? nameMan.Trim() : "",
                        FirstWoman = nameWoman != null ? nameWoman.Trim() : "",
                        Country = country != null ? country.Trim() : ""
                    };

                    context.Couples.Add(couple);
                }

                var model = new ImportViewModel()
                {
                    Competition = competition,
                    Couple = couple,
                    Number = number,
                    RegisterCouple = true,
                    State = 4
                };

                result.Add(model);

                line++;
            }

            this.context.SaveChanges();

            return result;

        }

        private Competition GetCompetition(string title)
        {
            var competition = this.context.Competitions.FirstOrDefault(c => c.Title == title);

            if (competition == null)
            {
                competition = new Competition()
                {
                    Event = this.context.Events.First(),
                    AgeGroup = this.context.AgeGroups.First(),
                    Class = this.context.Classes.First(),
                    CompetitionType = this.context.CompetitionTypes.First(),
                    Section = this.context.Sections.First(),
                    ExternalId = title,
                    Title = title
                };
                this.context.Competitions.Add(competition);
                this.context.SaveChanges();
            }

            return competition;
        }
    }
}
