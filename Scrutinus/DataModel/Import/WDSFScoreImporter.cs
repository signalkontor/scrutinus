﻿// // TPS.net TPS8 DataModel
// // WDSFScoreImporter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.IO;
using DataModel.Models;

namespace DataModel.Import
{
    public class WDSFScoreImporter : IFileImporter
    {
        public IEnumerable<ImportViewModel> ImportFile(
            ScrutinusContext context,
            string filename,
            Competition defaultCompetition)
        {
            var stream = new StreamReader(filename);

            var result = new List<ImportViewModel>();

            while (!stream.EndOfStream)
            {
                var data = stream.ReadLine().Split(',');

                if (data.Length < 9)
                {
                    continue;
                }

                var MinId = data[3];
                Couple couple = null; // context.Couples.SingleOrDefault(c => c.MINMan == MinId);

                var state = 0;

                if (couple == null)
                {
                    couple = new Couple()
                                 {
                                     MINMan = data[3],
                                     MINWoman = data[6],
                                     Country = data[7],
                                     ExternalId = data[8],
                                     FirstMan = data[1],
                                     LastMan = data[2],
                                     FirstWoman = data[4],
                                     LastWoman = data[5],
                                 };

                    state = 2;
                }

                var number = Int32.Parse(data[0]);

                result.Add(
                    new ImportViewModel()
                        {
                            Couple = couple,
                            Number = number,
                            Competition = defaultCompetition,
                            State = state
                        });
            }

            return result;
        }
    }
}