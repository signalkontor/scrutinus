﻿// // TPS.net TPS8 DataModel
// // MasterGamesImporter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.IO;
using System.Linq;
using DataModel.Models;
using OfficeOpenXml;

namespace DataModel.Import
{
    internal class MasterGamesImporter : IFileImporter
    {
        public IEnumerable<ImportViewModel> ImportFile(ScrutinusContext context, string filename, Competition defaultCompetition)
        {
            var result = new List<ImportViewModel>();

            var excelPackage = new ExcelPackage(new FileInfo(filename));


            for (var index = 1; index <= excelPackage.Workbook.Worksheets.Count; index++)
            {
                var sheet = excelPackage.Workbook.Worksheets[index];

                var line = 2;

                while (sheet.Cells[line, 1].Value != null)
                {
                    var names = sheet.Cells[line, 1].Value.ToString();

                    if (!names.Contains("/"))
                    {
                        line++;
                        continue;
                    }

                    var coupleNames = names.Split('/');

                    for (var i = 0; i < coupleNames.Length; i++)
                    {
                        coupleNames[i] = coupleNames[i].Trim();
                    }

                    var namesMan = coupleNames[0].Split(' ');
                    for (var i = 0; i < namesMan.Length; i++)
                    {
                        namesMan[i] = namesMan[i].Trim();
                    }

                    var namesWoman = coupleNames[1].Split(' ');
                    for (var i = 0; i < namesWoman.Length; i++)
                    {
                        namesWoman[i] = namesWoman[i].Trim();
                    }

                    var firstNameMan = namesMan[0];
                    var lastNameMan = namesMan[namesMan.Length - 1];
                    var firstNameWoman = namesWoman[0];
                    var lastNameWoman = namesWoman[namesWoman.Length - 1];

                    // Check if we have the couple:
                    var state = 0;
                    var couple =
                        context.Couples.FirstOrDefault(
                            c =>
                            c.FirstMan == firstNameMan && c.FirstWoman == firstNameWoman && c.LastMan == lastNameMan
                            && c.LastWoman == lastNameWoman);

                    if (couple == null)
                    {
                        state = 4;
                        couple = new Couple()
                                     {
                                         FirstMan = firstNameMan != null ? firstNameMan.Trim() : "",
                                         FirstWoman = firstNameWoman != null ? firstNameWoman.Trim() : "",
                                         LastMan = lastNameMan != null ? lastNameMan.Trim() : "",
                                         LastWoman = lastNameWoman != null ? lastNameWoman.Trim() : "",
                                     };

                        context.Couples.Add(couple);
                        context.SaveChanges();
                    }
                    var title = sheet.Cells[1, 1].Value.ToString();
                    var competition = context.Competitions.SingleOrDefault(c => c.Title == title);
                    if (competition == null)
                    {
                        competition = new Competition()
                                          {
                                              Title = title,
                                              Event = context.Events.First(),
                                              AgeGroup = context.AgeGroups.First(),
                                              Class = context.Classes.First(c => c.Id == 8),
                                              Section = context.Sections.First(),
                                              CompetitionType =
                                                  context.CompetitionTypes.First(t => t.Id == 14)
                                          };
                        context.Competitions.Add(competition);
                        context.SaveChanges();
                    }

                    var model = new ImportViewModel()
                                    {
                                        Competition = competition,
                                        Couple = couple,
                                        Number = 0,
                                        RegisterCouple = true,
                                        State = state
                                    };

                    result.Add(model);

                    line++;
                }
            }
            return result;
        }
    }
}
