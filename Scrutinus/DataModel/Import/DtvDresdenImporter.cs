﻿// // TPS.net TPS8 DataModel
// // DtvDresdenImporter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.IO;
using System.Linq;
using DataModel.Models;

namespace DataModel.Import
{
    public class DtvDresdenImporter : IFileImporter
    {
        private ScrutinusContext context;

        public IEnumerable<ImportViewModel> ImportFile(ScrutinusContext context, string filename, Competition defaultCompetition)
        {
            this.context = context;

            var stream = new StreamReader(filename);

            stream.ReadLine();

            while (!stream.EndOfStream)
            {
                var data = stream.ReadLine().Split(';');

                var WdsfIdMan = data[2];
                var WdsfIdWoman = data[4];

                var couple = context.Couples.FirstOrDefault(c => c.WdsfIdMan == WdsfIdMan && c.WdsfIdWoman == WdsfIdWoman);
                
                if (couple != null && (couple.Startbooks == null || couple.Startbooks.Count == 0))
                {
                    couple.MINMan = data[1];
                    couple.MINWoman = data[3];

                    if (data[5] == "A")
                    {
                        this.CreateStartBuch(couple, 4, 5, 1);
                    }
                    if (data[5] == "S")
                    {
                        this.CreateStartBuch(couple, 5, 6, 1);
                    }
                    if (data[6] == "A")
                    {
                        this.CreateStartBuch(couple, 4, 5, 2);
                    }
                    if (data[6] == "S")
                    {
                        this.CreateStartBuch(couple, 5, 6, 2);
                    }
                }
            }

            context.SaveChanges();

            return new List<ImportViewModel>();
        }

        private void CreateStartBuch(Couple couple, int classId, int targetClassId, int sectionId)
        {
            var startbuch = new Startbuch()
                                {
                                    AgeGroup = this.context.AgeGroups.Single(a => a.Id == 11),
                                    Class = this.context.Classes.Single(c => c.Id == classId),
                                    Couple = couple,
                                    NextClass = this.context.Classes.Single(c => c.Id == targetClassId),
                                    Points = 0,
                                    TargetPlacings = 10,
                                    TargetPoints = 250,
                                    MinimumPoints = 3,
                                    PlacingsUpto = 3,
                                    Placings = 0,
                                    Blocked = false,
                                    OriginalPlacings = 0,
                                    OriginalPoints = 0,
                                    Section = this.context.Sections.Single(s => s.Id == sectionId)
                                };

            var participants = couple.Participants.Where(p => p.Competition.Section.Id == sectionId);
            foreach(var participant in participants)
            {
                participant.Class = startbuch.Class;
            }

            this.context.Startbuecher.Add(startbuch);
        }
    }
}
