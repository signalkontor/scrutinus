﻿// // TPS.net TPS8 DataModel
// // TopTurnierImporter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using DataModel.Models;

namespace DataModel.Import
{
    internal class TopTurnierImporter : IFileImporter
    {
        public IEnumerable<ImportViewModel> ImportFile(ScrutinusContext context, string filename, Competition defaultCompetition)
        {
            var stream = new StreamReader(filename, Encoding.ASCII);
            var competitions = context.Competitions.ToList();
            var couples = context.Couples.ToList();

            var list = new List<ImportViewModel>();
            while (!stream.EndOfStream)
            {
                var import = new ImportViewModel() { State = 0 };

                try
                {
                    var line = stream.ReadLine();
                    var data = line.Split(';');
                    if (data.Length < 2)
                    {
                        data = line.Split(',');
                    }
                    if (data.Length < 2)
                    {
                        data = line.Split('\t');
                    }

                    if (data.Length < 5)
                    {
                        continue;
                    }

                    for (var i = 0; i < data.Length; i++)
                    {
                        if (data[i].StartsWith("\""))
                        {
                            data[i] = data[i].Substring(1, data[i].Length - 2);
                        }
                        data[i] = data[i].Replace((char)0, '!').Replace("!", "");
                    }

                    list.Add(import);

                  
                    // Find Competition by CompetitionID
                    import.Competition = competitions.FirstOrDefault(c => c.ExternalId == data[0]);
                    if (import.Competition == null)
                    {
                        import.State = 1; // Not found
                    }

                    import.Couple =
                        couples.FirstOrDefault(
                            c =>
                            c.FirstMan == data[1] && c.LastMan == data[2] && c.FirstWoman == data[3]
                            && c.LastWoman == data[4]);

                    import.Competition = defaultCompetition;

                    import.State += import.Couple == null ? 2 : 0;

                    if (import.Couple == null)
                    {
                        import.Couple = new Couple()
                        {
                            FirstMan = data[1],
                            LastMan = data[2],
                            FirstWoman = data[3],
                            LastWoman = data[4],
                            Country = data[5],
                        };
                    }

                    var number = 0;
                    if (Int32.TryParse(data[0], out number))
                    {
                        import.Number = number;
                    }
                    else
                    {
                        import.State += 16;
                    }
                }
                catch (Exception ex)
                {
                    import.State = 32; // Error -> wrong format of line ...
                    Debug.WriteLine(ex.Message);
                }
            }

            stream.Close();

            return list;
        }
    }
}
