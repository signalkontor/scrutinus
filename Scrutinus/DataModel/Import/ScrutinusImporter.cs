﻿// // TPS.net TPS8 DataModel
// // ScrutinusImporter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DataModel.Localization;
using DataModel.Models;
using General.Interfaces;

namespace DataModel.Import
{
    public class ScrutinusImporter : IFileImporter
    {
        private readonly IReportProgress reportProgress;

        public ScrutinusImporter(IReportProgress reportProgress)
        {
            this.reportProgress = reportProgress;
        }

        public IEnumerable<ImportViewModel> ImportFile(
            ScrutinusContext context,
            string filename,
            Competition defaultCompetition)
        {
            var competitions = context.Competitions.ToList();
            var couples = context.Couples.ToList();

            string[] lines;

            try
            {
                lines = File.ReadAllLines(filename, Encoding.UTF8);
            }
            catch (IOException ex)
            {
                ScrutinusContext.WriteLogEntry(ex);
                MessageBox.Show(Text.ImportFile_CouldNotOpenFIle);
                return new List<ImportViewModel>();
            }
            

            var list = new List<ImportViewModel>();

            for(var i=0;i<lines.Length;i++)
            {
                this.reportProgress.ReportStatus("Reading File", i, lines.Length);

                ImportViewModel import = null;

                try
                {
                    var line = lines[i];
                    var data = line.Split(';');
                    if (data.Length < 13)
                    {
                        data = line.Split(',');
                    }
                    if (data.Length < 13)
                    {
                        data = line.Split('\t');
                    }

                    if (data.Length < 13)
                    {
                        continue;
                    }

                    var competitionIds = data[0].Split(',');

                    foreach (var competitionId in competitionIds)
                    {

                        import = new ImportViewModel() {State = 0};

                        list.Add(import);

                        // Find Competition by CompetitionID
                        var competition = competitions.FirstOrDefault(c => c.ExternalId == competitionId.Trim());
                        if (competition == null)
                        {
                            competition = new Competition()
                            {
                                Event = context.Events.FirstOrDefault(),
                                AgeGroup = context.AgeGroups.FirstOrDefault(),
                                Class = context.Classes.FirstOrDefault(c => c.Id == 8),
                                CompetitionType = context.CompetitionTypes.FirstOrDefault(),
                                ExternalId = competitionId.Trim()
                            };
                            competitions.Add(competition);
                            context.Competitions.Add(competition);
                            context.SaveChanges();
                        }

                        import.Competition = competition;

                        if (import.Competition == null)
                        {
                            import.State = 1; // Not found
                        }

                        if (data[4].Contains("/") || data[4].Contains("-")
                            && string.IsNullOrWhiteSpace(data[5])
                            && string.IsNullOrWhiteSpace(data[6])
                            && string.IsNullOrWhiteSpace(data[7]))
                        {
                            var delimiter = data[4].Contains("/") ? '/' : '-';
                            var names = data[4].Split(delimiter);
                            var mansNames = names[0].Trim().Split(' ');
                            var womansNames = names[1].Trim().Split(' ');

                            data[4] = mansNames[0];
                            data[5] = mansNames[mansNames.Count() - 1];
                            data[6] = womansNames[0];
                            data[7] = womansNames[womansNames.Count() - 1];
                        }

                        if (data[4].Contains(","))
                        {
                            var names = data[4].Split(',');
                            data[4] = names[1].Trim();
                            data[5] = names[0].Trim();
                        }
                        if (data[6].Contains(","))
                        {
                            var names = data[6].Split(',');
                            data[6] = names[1].Trim();
                            data[7] = names[0].Trim();
                        }

                        if (data[8].Contains(" and ") || data[8].Contains(" - "))
                        {
                            var mins = data[8].Split(' ');
                            data[8] = mins[0].Trim();
                            data[9] = mins[2].Trim();
                        }
                       

                        import.Couple =
                            couples.FirstOrDefault(
                                c =>
                                    c.FirstMan == data[4] && c.LastMan == data[5] && c.FirstWoman == data[6]
                                    && c.LastWoman == data[7]);

                        import.State += import.Couple == null ? 2 : 0;

                        if (import.Couple == null)
                        {
                            import.Couple = new Couple()
                            {
                                FirstMan = data[4],
                                LastMan = data[5],
                                FirstWoman = data[6],
                                LastWoman = data[7],
                                Country = data[8],
                                Region = data[9],
                                MINMan = data[10],
                                MINWoman = data[11],
                                IsReadOnly = false
                            };
                            couples.Add(import.Couple);
                        }
                        if (import.Couple.MINMan == "" || import.Couple.MINWoman == "")
                        {
                            import.State += 4; // No MIN
                        }

                        int number;
                        if (int.TryParse(data[12], out number))
                        {
                            import.Number = number;
                        }
                        else
                        {
                            import.State += 16;
                        }
                    }
                }
                catch (Exception ex)
                {
                    import.State = 32; // Error -> wrong format of line ...
                    Debug.WriteLine(ex.Message);
                }
            }

            this.reportProgress.RemoveStatusReport();

            return list;
        }
    }
}