﻿// // TPS.net TPS8 DataModel
// // PrintReportTypes.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH
namespace DataModel
{
    public enum PrintReportType
    {
        CorrectedStartList,
        QualifiedCouples,
        Officials,
        WinnerCertificates,
        SkaktingTable,
        FinalTable,
        RoundDraw
    }
}