﻿// // TPS.net TPS8 DataModel
// // Roles.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH
namespace DataModel
{
    public static class Roles
    {
        public const int Judge = 1;

        public const int Chairman = 2;

        public const int Scrutineer = 3;

        public const int Associate = 4;

        public const int Supervisor = 5;

        public const int TechnicalDelegate = 6;
    }
}