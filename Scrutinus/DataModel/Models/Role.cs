// // TPS.net TPS8 DataModel
// // Role.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System.Collections.Generic;

namespace DataModel.Models
{
    public partial class Role
    {
        public Role()
        {
            this.Officials = new List<Official>();
        }

        public int Id { get; set; }

        public string RoleLong { get; set; }

        public string RoleShort { get; set; }

        public virtual ICollection<Official> Officials { get; set; }
    }
}