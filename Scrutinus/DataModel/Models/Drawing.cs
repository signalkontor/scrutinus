// // TPS.net TPS8 DataModel
// // Drawing.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH
namespace DataModel.Models
{
    public partial class Drawing
    {
        public int Id { get; set; }

        public int Heat { get; set; }

        public virtual DanceInRound DanceRound { get; set; }

        public virtual Participant Participant { get; set; }

        public virtual Round Round { get; set; }
    }
}