﻿// // TPS.net TPS8 DataModel
// // ScrutinusDbConfiguration.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Interception;
using System.Diagnostics;

namespace DataModel.Models
{
    public class ScrutinusDbConfiguration : DbConfiguration
    {
        public ScrutinusDbConfiguration()
        {
            // this.AddInterceptor(new MyDbCInterceptor());
        }
    }

    public class MyDbCInterceptor : IDbCommandInterceptor
    {
        public void NonQueryExecuted(DbCommand command, DbCommandInterceptionContext<int> interceptionContext)
        {
            this.LogCommand(command);
        }

        public void NonQueryExecuting(DbCommand command, DbCommandInterceptionContext<int> interceptionContext)
        {
            this.LogCommand(command);
        }

        public void ReaderExecuted(DbCommand command, DbCommandInterceptionContext<DbDataReader> interceptionContext)
        {
            this.LogCommand(command);
        }

        public void ReaderExecuting(DbCommand command, DbCommandInterceptionContext<DbDataReader> interceptionContext)
        {
            this.LogCommand(command);
        }

        public void ScalarExecuted(DbCommand command, DbCommandInterceptionContext<object> interceptionContext)
        {
            this.LogCommand(command);
        }

        public void ScalarExecuting(DbCommand command, DbCommandInterceptionContext<object> interceptionContext)
        {
            this.LogCommand(command);
        }

        private void LogCommand(DbCommand command)
        {
            Debug.WriteLine(command.CommandText);
        }
    }
}
