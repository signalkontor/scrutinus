﻿// // TPS.net TPS8 DataModel
// // Club.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

namespace DataModel.Models
{
    public class Club
    {
        public int Id { get; set; }

        public string ClubName { get; set; }

        public string Region { get; set; }
    }
}
