﻿// // TPS.net TPS8 DataModel
// // ClimbUp.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH
namespace DataModel.Models
{
    public enum ClimbupType
    {
        None = 0,
        Regular = 1,
        Decision = 2
    }

    public class ClimbUp
    {
        public int Id { get; set; }

        public ClimbupType ClimbupType { get; set; }

        public virtual Competition Competition { get; set; }

        public virtual Couple Couple { get; set; }

        public virtual Class NewClass { get; set; }
    }
}