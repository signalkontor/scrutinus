﻿// // TPS.net TPS8 DataModel
// // SimpleStartbuch.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;

namespace DataModel.Models
{
    public class SimpleStartbuch
    {
        public int Id { get; set; }

        public string IdMan { get; set; }

        public string IdWoman { get; set; }

        public int StarterId { get; set; }

        public virtual Section Section { get; set; }

        public virtual AgeGroup AgeGroup { get; set; }

        public virtual Class Class { get; set; }

        public virtual Class NextClass { get; set; }

        public int OriginalPoints { get; set; }

        public int TargetPoints { get; set; }

        public int OriginalPlacings { get; set; }

        public int TargetPlacings { get; set; }

        public int MinimumPoints { get; set; }

        public int PlacingsUpto { get; set; }

        public bool Sylabus { get; set; }

        public bool PrintLaufzettel { get; set; }

        public bool Blocked { get; set; }

        public DateTime? BlockedDate { get; set; }

        public string Club { get; set; }

        public string LTV { get; set; }
    }
}
