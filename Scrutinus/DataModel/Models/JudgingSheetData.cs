﻿// // TPS.net TPS8 DataModel
// // JudgingSheetData.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;

namespace DataModel.Models
{
    public class JudgingSheetData
    {
        public int Id { get; set; }

        public virtual Competition Competition { get; set; }

        public string FileName { get; set; }

        public string Remark { get; set; }

        public bool HasBeenSend { get; set; }

        public DateTime? SendDate { get; set; }
    }
}
