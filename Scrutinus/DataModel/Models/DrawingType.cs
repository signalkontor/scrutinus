// // TPS.net TPS8 DataModel
// // DrawingType.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH
namespace DataModel.Models
{
    public class DrawingType
    {
        public int Id { get; set; }

        public string DrawingName { get; set; }
    }
}