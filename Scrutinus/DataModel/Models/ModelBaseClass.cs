﻿// // TPS.net TPS8 DataModel
// // ModelBaseClass.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using PropertyChanged;

namespace DataModel.Models
{

    [AddINotifyPropertyChangedInterface]
    [Serializable]
    public class ModelBaseClass : INotifyPropertyChanged
    {
        /// <summary>
        /// The errors changed event
        /// </summary>
        

        /// <summary>
        ///     control specific error messages
        /// </summary>
        [NonSerialized]
        protected Dictionary<string, IEnumerable<string>> controlErrorMessage = new Dictionary<string, IEnumerable<string>>();


        /// <summary>
        /// Gets a value that indicates whether the entity has validation errors.
        /// </summary>
        /// <returns>true if the entity currently has validation errors; otherwise, false.</returns>
        public bool HasErrors
        {
            get
            {
                if (this.controlErrorMessage == null)
                {
                    return false;
                }
                // values are a list of error message.
                // we check that we have no error message at all
                return this.controlErrorMessage.Values.Any(v => v.Any());
            }
        }

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Validates this instance.
        /// </summary>
        public void Validate()
        {
            if (this.controlErrorMessage == null)
            {
                this.controlErrorMessage = new Dictionary<string, IEnumerable<string>>();
            }

            this.controlErrorMessage.Clear();

            // Get all Validation Attributes of this class ...
            /*
            var validatedProperties =
                this.GetType()
                    .GetProperties()
                    .Where(p => p.GetCustomAttributes().Any(c => c.GetType().IsSubclassOf(typeof(ValidationAttribute))));


            foreach (var validatedPropery in validatedProperties)
            {
                this.ValidateModelProperty(validatedPropery.GetValue(this), validatedPropery);
            }

            this.RaisePropertyChanged(() => HasErrors);
             * */
        }

        /// <summary>
        /// Gets the validation errors for a specified property or for the entire entity.
        /// </summary>
        /// <param name="propertyName">The name of the property to retrieve validation errors for; or null or <see cref="F:System.String.Empty" />, to retrieve entity-level errors.</param>
        /// <returns>
        /// The validation errors for the property or entity.
        /// </returns>
        public IEnumerable GetErrors(string propertyName)
        {
            if (propertyName == null ||
                this.controlErrorMessage == null ||
                !this.controlErrorMessage.ContainsKey(propertyName))
            {
                return null;
            }

            return this.controlErrorMessage[propertyName];
        }

        /// <summary>
        /// Adds the model error.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        /// <param name="errorMessage">The error message.</param>
        public void AddModelError(string propertyName, string errorMessage)
        {
            if (this.controlErrorMessage.ContainsKey(propertyName))
            {
                this.controlErrorMessage.Remove(propertyName);
            }

            this.controlErrorMessage.Add(propertyName, new[] { errorMessage });
            
            this.RaisePropertyChanged(() => this.HasErrors);
        }

        /// <summary>
        /// Validates the model property.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="propertyInfo">The property information.</param>
        private void ValidateModelProperty(object value, PropertyInfo propertyInfo)
        {
            if (this.controlErrorMessage.ContainsKey(propertyInfo.Name))
            {
                this.controlErrorMessage.Remove(propertyInfo.Name);
            }

            IList<string> validationErrors =
                  (from validationAttribute in propertyInfo.GetCustomAttributes(true).OfType<ValidationAttribute>()
                   where !validationAttribute.IsValid(value)
                   select validationAttribute.FormatErrorMessage(string.Empty))
                   .ToList();

            this.controlErrorMessage.Add(propertyInfo.Name, validationErrors);
            
        }

        public virtual void RaisePropertyChanged<T>(Expression<Func<T>> propertyExpression)
        {
            var changedEventHandler = this.PropertyChanged;
            if (changedEventHandler == null)
            {
                return;
            }

            var propertyName = GetPropertyName<T>(propertyExpression);
            changedEventHandler((object)this, new PropertyChangedEventArgs(propertyName));
        }

        public void RaisePropertyChanged(string propertyName)
        {
            var changedEventHandler = this.PropertyChanged;

            if (changedEventHandler == null)
            {
                return;
            }
            changedEventHandler((object)this, new PropertyChangedEventArgs(propertyName));
        }

        protected static string GetPropertyName<T>(Expression<Func<T>> propertyExpression)
        {
            if (propertyExpression == null)
            {
                throw new ArgumentNullException("propertyExpression");
            }
            var memberExpression = propertyExpression.Body as MemberExpression;
            if (memberExpression == null)
            {
                throw new ArgumentException("Invalid argument", "propertyExpression");
            }
            var propertyInfo = memberExpression.Member as PropertyInfo;
            if (propertyInfo == (PropertyInfo)null)
            {
                throw new ArgumentException("Argument is not a property", "propertyExpression");
            }
            else
            {
                return propertyInfo.Name;
            }
        }
    }
}
