// // TPS.net TPS8 DataModel
// // DancesInCompetition.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH
namespace DataModel.Models
{
    public class DancesInCompetition
    {
        public int Id { get; set; }

        public int DanceOrder { get; set; }

        public bool IsSolo { get; set; }

        public virtual Competition Competition { get; set; }

        public virtual Dance Dance { get; set; }
    }
}