﻿// // TPS.net TPS8 DataModel
// // Printer.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

namespace DataModel.Models
{
    public class Printer
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string PrintQueueName { get; set; }

        public string InputBin { get; set; }

        public bool PrintColors { get; set; }
    }
}
