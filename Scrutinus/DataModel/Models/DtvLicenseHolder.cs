﻿// // TPS.net TPS8 DataModel
// // DtvLicenseHolder.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataModel.Models
{
    public class DtvLicenseHolder
    {
        public int Id { get; set; }

        [MaxLength(20)]
        public string DtvId { get; set; }

        public int? WdsfMin { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Club { get; set; }

        public CountryCode Nationality { get; set; } 

        public string LicensesAsString { get; set; }

        [NotMapped]
        public IEnumerable<string> Licenses {
            get { return this.LicensesAsString.Split(','); }
            set { this.LicensesAsString = string.Join(",", value); }
        }
    }
}
