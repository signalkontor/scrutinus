// // TPS.net TPS8 DataModel
// // AgeGroup.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

namespace DataModel.Models
{
    public class AgeGroup
    {
        public int Id { get; set; }

        public string AgeGroupName { get; set; }

        public string ShortName { get; set; }

        public int OrderWhenCombined { get; set; }

        public string AgeGroupCode { get; set; }
    }
}