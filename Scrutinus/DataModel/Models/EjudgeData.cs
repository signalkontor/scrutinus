﻿// // TPS.net TPS8 DataModel
// // EjudgeData.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using PropertyChanged;

namespace DataModel.Models
{
    [AddINotifyPropertyChangedInterface]
    public class EjudgeData
    {
        public EjudgeData()
        {
            this.SignatureCoordinates = new List<Coordinate>();
        }

        public int Id { get; set; }

        public virtual Official Judge { get; set; }

        public string DeviceId { get; set; }

        public virtual Round Round { get; set; }

        public virtual ICollection<Coordinate> SignatureCoordinates { get; set; }

        [NotMapped] // We do not map this Property because we have the device Id
        public Device Device { get; set; }

        public int Flags { get; set; }

        public string DefaultLanguage { get; set; }
    }
}
