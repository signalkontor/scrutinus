﻿// // TPS.net TPS8 DataModel
// // Person.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

namespace DataModel.Models
{
    public enum Gender
    {
        Male,
        Female,
    }

    public class Person
    {
        public int Id { get; set; }

        public string ExternalId { get; set; }

        public Gender Gender { get; set; }

        public string Title { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string WdsfMin { get; set; }

        public string Nationality { get; set; }

        public bool Dancing { get; set; }

        public virtual Team Team { get; set; }
    }
}
