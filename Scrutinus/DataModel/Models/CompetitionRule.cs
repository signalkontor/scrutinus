﻿// // TPS.net TPS8 DataModel
// // CompetitionRule.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;

namespace DataModel.Models
{
    public class CompetitionRule
    {
        public CompetitionRule()
        {
            this.RedanceRequired = false;
            this.SecondFirstRound = false;
            this.SupervisorRequired = false;
            this.AssosiateRequired = false;
            this.CanChangeOrderOfDances = false;
            this.MinimumJudgeNumber = 1;
            this.MarkingType = MarkingTypes.Marking;
            this.FinalType = MarkingTypes.Skating;
        }

        public int Id { get; set; }

        public string Federation { get; set; }

        public string RuleName { get; set; }

        public bool ChairmanRequired { get; set; }

        public bool SupervisorRequired { get; set; }

        public bool AssosiateRequired { get; set; }

        public bool CanChangeOrderOfDances { get; set; }

        public MarkingTypes MarkingType { get; set; }

        public MarkingTypes FinalType { get; set; }

        public MarkNumberType? DefaultNumberOfMarks { get; set; }

        public bool RedanceRequired { get; set; }

        public bool SecondFirstRound { get; set; }

        public int MinimumJudgeNumber { get; set; }

        public virtual RoundType RoundType { get; set; }

        public virtual ICollection<CompetitionType> CompetitionTypes { get; set; }
    }
}