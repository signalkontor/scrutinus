// // TPS.net TPS8 DataModel
// // DanceInRound.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System.Collections.Generic;

namespace DataModel.Models
{
    public class DanceInRound
    {
        public DanceInRound()
        {
            this.Drawings = new List<Drawing>();
        }

        public int Id { get; set; }

        public int SortOrder { get; set; }

        public bool IsSolo { get; set; }

        public virtual Dance Dance { get; set; }

        public virtual Round Round { get; set; }

        public virtual ICollection<Drawing> Drawings { get; set; }
    }
}