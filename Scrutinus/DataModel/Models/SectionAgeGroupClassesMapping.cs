﻿// // TPS.net TPS8 DataModel
// // SectionAgeGroupClassesMapping.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;

namespace DataModel.Models
{
    public class SectionAgeGroupClassesMapping
    {
        public int Id { get; set; }

        public virtual Section Section { get; set; }

        public virtual AgeGroup AgeGroup { get; set; }

        public virtual ICollection<Class> Classes { get; set; }
    }
}
