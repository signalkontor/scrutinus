// // TPS.net TPS8 DataModel
// // OfficialInCompetition.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System.ComponentModel.DataAnnotations;

namespace DataModel.Models
{
    public class OfficialInCompetition
    {
        public int Id { get; set; }

        public string ExternalId { get; set; }

        [MaxLength(50)]
 
        public string WdsfId { get; set; }

        [Required]
        public virtual Role Role { get; set; }

        [Required]
        public virtual Competition Competition { get; set; }

        [Required]
        public virtual Official Official { get; set; }
    }
}