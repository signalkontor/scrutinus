// // TPS.net TPS8 DataModel
// // ScrutinusContext.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using DataModel.Models.Mapping;
using NLog;

namespace DataModel.Models
{
    public class ScrutinusContext : DbContext
    {
        private static DateTime lastSaveDateTime;

        private static string xmlResource;
        private readonly Logger logger = LogManager.GetCurrentClassLogger();

        public ScrutinusContext()
            : this(CurrentDataSource, "")
        {
            
        }

        public ScrutinusContext(string dataSource, string xmlInitialContent)
        {
            if (dataSource == null)
            {
                throw new ArgumentNullException("dataSource", "CurrentDataSource cannot be null");
            }

            if(!string.IsNullOrEmpty(xmlInitialContent))
            {
                xmlResource = xmlInitialContent;
            }

            CurrentDataSource = dataSource;

            if (!dataSource.StartsWith("Data Source"))
            {
                Database.DefaultConnectionFactory = new SqlCeConnectionFactory("System.Data.SqlServerCe.4.0");
                this.Database.Connection.ConnectionString = @"Data Source=" + dataSource + "; Mode=Read Write";
                
                if (File.Exists(dataSource))
                {
                    Database.SetInitializer<ScrutinusContext>(null);
                }
                else
                {
                    if (DoNotInitializeDatabase || xmlResource == null)
                    {
                        Database.SetInitializer<ScrutinusContext>(new CreateDatabaseIfNotExists<ScrutinusContext>());
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(xmlResource))
                        {
                            throw new ArgumentException("Initial data cannot be empty or null");
                        }
                        Database.SetInitializer<ScrutinusContext>(new DatabaseCreator(xmlResource));
                    }
                }
            }
            else
            {
                Database.DefaultConnectionFactory = new SqlConnectionFactory();
                this.Database.Connection.ConnectionString = dataSource;
                if (!this.Database.Exists())
                {
                    if (string.IsNullOrEmpty(xmlResource) && !DoNotInitializeDatabase)
                    {
                        throw new ArgumentException("Initial data cannot be empty or null");
                    }

                    Database.SetInitializer<ScrutinusContext>(DoNotInitializeDatabase
                        ? new CreateDatabaseIfNotExists<ScrutinusContext>()
                        : new DatabaseCreator(xmlResource));
                }
                else
                {
                    Database.SetInitializer<ScrutinusContext>(null);
                }
            }
        }

        public static string CurrentDataSource { get; set; }

        public static bool DoNotInitializeDatabase { get; set; }

        public DbSet<AgeGroup> AgeGroups { get; set; }

        public DbSet<Class> Classes { get; set; }

        public DbSet<ClimbUp> ClimbUps { get; set; }

        public DbSet<Competition> Competitions { get; set; }

        public DbSet<Couple> Couples { get; set; }

        public DbSet<Team> Teams { get; set; }

        public DbSet<Dance> Dances { get; set; }

        public DbSet<DanceInRound> DancesInRounds { get; set; }

        public DbSet<DancesInCompetition> DancesCompetition { get; set; }

        public DbSet<Drawing> Drawings { get; set; }

        public DbSet<DrawingType> DrawingTypes { get; set; }

        public DbSet<Event> Events { get; set; }

        public DbSet<Marking> Markings { get; set; }

        public DbSet<Official> Officials { get; set; }

        public DbSet<OfficialInCompetition> OfficialCompetition { get; set; }

        public DbSet<Participant> Participants { get; set; }

        public DbSet<Person> People { get; set; }

        public DbSet<Qualified> Qualifieds { get; set; }

        public DbSet<Role> Roles { get; set; }

        public DbSet<Round> Rounds { get; set; }

        public DbSet<RoundType> RoundTypes { get; set; }

        public DbSet<Section> Sections { get; set; }

        public DbSet<SectionAgeGroupClassesMapping> SectionAgeGroupClassesMappings { get; set; }

        public DbSet<CompetitionRule> CompetitionRules { get; set; }

        public DbSet<CompetitionType> CompetitionTypes { get; set; }

        public DbSet<DanceTemplate> DanceTemplates { get; set; }

        public DbSet<JudgingComponent> JudgingComponents { get; set; }

        public DbSet<CountryCode> CountryCodes { get; set; }

        public DbSet<Club> Clubs { get; set; }

        public DbSet<Startbuch> Startbuecher { get; set; }

        public DbSet<ClimbUpTable> ClimbUpTables { get; set; }

        public DbSet<Parameter> Parameters { get; set; }

        public DbSet<JudgingSheetData> JudgingSheets { get; set; }

        public DbSet<SimpleStartbuch> SimpleStartbucher { get; set; }

        public DbSet<LogEntry> LogEntries { get; set; }

        public DbSet<EjudgeData> EjudgeDatas { get; set; }

        public DbSet<Device> Devices { get; set; }

        public DbSet<Printer> Printers { get; set; }

        public DbSet<PrintReportDefinition> PrintReportDefinitions { get; set; }

        public DbSet<DtvLicenseHolder> DtvLicenseHolders { get; set; }

        public static DateTime LastSaveDateTime
        {
            get
            {
                return lastSaveDateTime;
            }
        }

        public override int SaveChanges()
        {
            try
            {
                var result = base.SaveChanges();
                lastSaveDateTime = DateTime.Now;
                return result;
            }
            catch (DbEntityValidationException ex)
            {
                var errorMessages = ex.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                this.logger.Error(exceptionMessage);
                throw;
            }
            catch (Exception ex2)
            {
                this.logger.Error(ex2);
                throw;
            }
        }

        public void SetParameter(string parameterName, object value)
        {
            if (value == null)
            {
                return;
            }

            var ms = new MemoryStream();
            var formatter = new BinaryFormatter();
            try
            {
                formatter.Serialize(ms, value);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Exception serializing object: " + ex.Message);
                return;
            }

            var bytes = ms.GetBuffer();

            var parameter = this.Parameters.FirstOrDefault(p => p.ParameterName == parameterName);
            if (parameter == null)
            {
                parameter = new Parameter {ParameterName = parameterName};
                this.Parameters.Add(parameter);
            }

            parameter.SerializedValue = bytes;

            this.SaveChanges();
        }

        public T GetParameter<T>(string parameterName, T defaultValue = default(T))
        {
            try
            {
                var parameter = this.Parameters.FirstOrDefault(p => p.ParameterName == parameterName);
                if (parameter == null)
                {
                    return defaultValue;
                }

                var stream = new MemoryStream(parameter.SerializedValue);

                var bf = new BinaryFormatter();
                var obj = (T)bf.Deserialize(stream);

                return obj;
            }
            catch (Exception)
            {
                return default(T);
            }
        }

        public static void WriteLogEntry(LogLevel level, string message, string stackTrace)
        {
            try
            {
                using (var context = new ScrutinusContext())
                {
                    if (stackTrace.Length > 4000)
                    {
                        stackTrace = stackTrace.Substring(0, 3999);
                    }

                    if (message.Length > 4000)
                    {
                        message = message.Substring(0, 3999);
                    }

                    var logEntry = new LogEntry()
                                       {
                                           LogLevel = level,
                                           Message = message,
                                           TimeStamp = DateTime.Now,
                                           StackTrace = stackTrace
                                       };
                    context.LogEntries.Add(logEntry);
                    context.SaveChanges();
                }
            }
            catch (DbEntityValidationException validationException)
            {
                foreach (var dbEntityValidationResult in validationException.EntityValidationErrors)
                {
                    foreach (var dbValidationError in dbEntityValidationResult.ValidationErrors)
                    {
                        Debug.WriteLine(dbValidationError.PropertyName + ": " + dbValidationError.ErrorMessage);
                    }
                }
            }
            catch (Exception)
            {
                
            }
        }

        public static void WriteLogEntry(Exception exception)
        {
            while (exception != null)
            {
                WriteLogEntry(LogLevel.Error, exception.Message, exception.StackTrace);
                exception = exception.InnerException;
            }
        }

        public static void WriteLogEntry(LogLevel level, string message)
        {
            WriteLogEntry(level, message, Environment.StackTrace);
        }

        public void DeleteQualified(Qualified qualified)
        {
            this.Qualifieds.Remove(qualified);

            this.SaveChanges();
        }

        public DbEntityValidationResult ValidateEntity<TEntity>(TEntity obj) where TEntity : class
        {
            return this.ValidateEntity(this.Entry(obj), null);
        }

        public void Refresh<TEntity>(TEntity obj) where TEntity : class
        {
            this.Entry(obj).Reload();
        }

        public void RefreshAll()
        {
            foreach (var entity in this.ChangeTracker.Entries())
            {
                entity.Reload();
            }
        }

        public void ResetXMlInitalData()
        {
            xmlResource = null;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<IncludeMetadataConvention>();

            modelBuilder.Configurations.Add(new AgeGroupMap());
            modelBuilder.Configurations.Add(new ClassMap());
            modelBuilder.Configurations.Add(new CompetitionMap());
            modelBuilder.Configurations.Add(new CoupleMap());
            modelBuilder.Configurations.Add(new TeamMap());
            modelBuilder.Configurations.Add(new DanceMap());
            modelBuilder.Configurations.Add(new DanceInRoundMap());
            modelBuilder.Configurations.Add(new DancesCompetitionMap());
            modelBuilder.Configurations.Add(new DrawingMap());
            modelBuilder.Configurations.Add(new DrawingTypeMap());
            modelBuilder.Configurations.Add(new EventMap());
            modelBuilder.Configurations.Add(new MarkingMap());
            modelBuilder.Configurations.Add(new OfficialMap());
            modelBuilder.Configurations.Add(new OfficialInCompetitionMap());
            modelBuilder.Configurations.Add(new ParticipantMap());
            modelBuilder.Configurations.Add(new QualifiedMap());
            modelBuilder.Configurations.Add(new RoleMap());
            modelBuilder.Configurations.Add(new RoundMap());
            modelBuilder.Configurations.Add(new RoundTypeMap());
            modelBuilder.Configurations.Add(new SectionMap());
            modelBuilder.Configurations.Add(new CompetitionRuleMap());
            modelBuilder.Configurations.Add(new CompetitionTypeMap());
            modelBuilder.Configurations.Add(new DanceTemplateMap());
            modelBuilder.Configurations.Add(new JudgingComponentMap());
            modelBuilder.Configurations.Add(new CountryCodeMap());
            modelBuilder.Configurations.Add(new ClimbUpMap());
            modelBuilder.Configurations.Add(new ClubMap());
            modelBuilder.Configurations.Add(new ParameterMap());
            modelBuilder.Configurations.Add(new StartbuchMap());
            modelBuilder.Configurations.Add(new ClimbUpTableMap());
            modelBuilder.Configurations.Add(new SectionAgeGroupClassesMappingMap());
            modelBuilder.Configurations.Add(new LogEntryMap());
        }

        private void WriteLogger(Exception exception)
        {
            this.logger.Error(exception);
        }
    }
}