﻿// // TPS.net TPS8 DataModel
// // Coordinate.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

namespace DataModel.Models
{
    public class Coordinate
    {
        public int Id { get; set; }

        public double X1 { get; set; }

        public double X2 { get; set; }

        public double Y1 { get; set; }

        public double Y2 { get; set; }
    }
}
