﻿// // TPS.net TPS8 DataModel
// // License.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;

namespace DataModel.Models
{
    public enum FeatueLicened
    {
        none,
        eJudge,
        TabletServer,
        eCheckin
    }

    public class License
    {
        public License()
        {
            this.LicenseHolder = "TTC Elmshorn";
            this.ValidFrom = DateTime.Now;
            this.ValidUntil = DateTime.Now;
            this.Ejudge = false;
            this.MaxCompetitions = 20;
            this.MaxCouples = 9999;
        }

        public string LicenseHolder { get; set; }

        public DateTime ValidFrom { get; set; }

        public DateTime ValidUntil { get; set; }

        public bool Ejudge { get; set; }

        public int MaxCompetitions { get; set; }

        public int MaxCouples { get; set; }

        public bool TabletServer { get; set; }

        public bool CheckIn { get; set; }

        public bool SQLServer { get; set; }
    }
}
