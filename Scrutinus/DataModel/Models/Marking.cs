// // TPS.net TPS8 DataModel
// // Marking.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using PropertyChanged;

namespace DataModel.Models
{
    [AddINotifyPropertyChangedInterface]
    public class Marking : INotifyPropertyChanged
    {
        public Marking()
        {
            
        }

        public Marking(Participant participant, Round round, Dance dance, Official judge)
        {
            this.Participant = participant;
            this.Round = round;
            this.Dance = dance;
            this.Judge = judge;
            this.Lift = false;
        }

        public int Id { get; set; }

        // Traditional Marking
        // Qualification-Rounds: 1 = mark set
        // Final: Place 1, 2, ...


        public int Mark { get; set; }

        public bool Lift { get; set; }

        public double Mark20 { get; set; }

        public int HelpCode { get; set; }

        public int MarkingComponent { get; set; }

        // New Judging System Version 1.0

        public double MarkA { get; set; }

        public double MarkB { get; set; }

        public double MarkC { get; set; }

        public double MarkD { get; set; }

        public double MarkE { get; set; }

        public DateTime? Created { get; set; }

        public virtual Participant Participant { get; set; }

        public virtual Round Round { get; set; }

        public virtual Dance Dance { get; set; }


        public virtual Official Judge { get; set; }

        [NotMapped]
        public bool HasError { get; set; }

        [NotMapped]
        public string ScoreWithComponent
        {
            get
            {
                var component = "";

                switch (this.MarkingComponent)
                {
                    case 1: component = "TQ";
                        break;
                    case 2: component = "MM";
                        break;
                    case 3: component = "PS";
                        break;
                    case 4: component = "CP";
                        break;
                }

                return $"{this.Mark20:#0.0} ({component})";
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}