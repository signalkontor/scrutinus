// // TPS.net TPS8 DataModel
// // Qualified.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using PropertyChanged;

namespace DataModel.Models
{
    [AddINotifyPropertyChangedInterface]
    public class Qualified
    {
        public int Id { get; set; }

        public int Sum { get; set; }

        public double Points { get; set; }

        public int? PlaceFrom { get; set; }

        public int? PlaceTo { get; set; }

        public bool QualifiedNextRound { get; set; }

        public virtual Participant Participant { get; set; }

        public virtual Round Round { get; set; }

        [NotMapped]
        public string Place
        {
            get
            {
                return this.PlaceFrom == this.PlaceTo
                           ? this.PlaceFrom + "."
                           : String.Format("{0}. - {1}.", this.PlaceFrom, this.PlaceTo);
            }
        }

        [NotMapped]
        public Dictionary<int, double> PointsPerDance { get; set; }
    }
}