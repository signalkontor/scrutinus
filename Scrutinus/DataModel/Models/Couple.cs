// // TPS.net TPS8 DataModel
// // Couple.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using PropertyChanged;

namespace DataModel.Models
{
    [AddINotifyPropertyChangedInterface]
    public class Couple
    {
        public Couple()
        {
            this.Participants = new List<Participant>();
            this.Startbooks = new List<Startbuch>();
            this.IsReadOnly = false;
        }

        public int Id { get; set; }

        [AlsoNotifyFor("NiceName")]
        public string FirstMan { get; set; }

        [AlsoNotifyFor("NiceName")]
        public string LastMan { get; set; }

        [AlsoNotifyFor("NiceName")]
        public string FirstWoman { get; set; }

        [AlsoNotifyFor("NiceName")]
        public string LastWoman { get; set; }

        public string MINMan { get; set; }

        public string MINWoman { get; set; }

        public string WdsfIdMan { get; set; }

        public string WdsfIdWoman { get; set; }

        public string Country { get; set; }

        public string Country2 { get; set; }

        public string ExternalId { get; set; }

        public string WdsfCoupleId { get; set; }

        public string Region { get; set; }

        // Props for World Games
        public bool IsTeam { get; set; }

        public bool IsSingle { get; set; }

        public string Athlete_Id_Man { get; set; }

        public string Athlete_Id_Woman { get; set; }

        public string Team_Id { get; set; }

        public string Long_TV_Name_Man { get; set; }

        public string Long_TV_Name_Woman { get; set; }

        public string Short_TV_Name_Man { get; set; }

        public string Short_TV_Name_Woman { get; set; }

        public string NOC_Code_Man { get; set; }

        public string NOC_Code_Woman { get; set; }

        public int? NumberInEvent { get; set; }

        public ApiStatus WdsfApiStatus { get; set; }

        public bool IsReadOnly { get; set; }
        //
        public virtual ICollection<Participant> Participants { get; set; }

        public virtual ICollection<Startbuch> Startbooks { get; set; }

        [NotMapped]
        public string MansName
        {
            get
            {
                return this.FirstMan + " " + this.LastMan;
            }
        }

        [NotMapped]
        public string WomansName
        {
            get
            {
                return this.FirstWoman + " " + this.LastWoman;
            }
        }

        [NotMapped]
        public virtual string NiceName
        {
            get
            {
                if (!this.IsTeam && !this.IsSingle)
                {
                    return String.Format(
                        "{0} {1} / {2} {3}",
                        this.FirstMan,
                        this.LastMan,
                        this.FirstWoman,
                        this.LastWoman);
                }
                if (this.IsSingle)
                {
                    return this.FirstMan + " " + this.LastMan;
                }

                return this.FirstMan;
            }
        }
    }
}