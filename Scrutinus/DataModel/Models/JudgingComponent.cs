﻿// // TPS.net TPS8 DataModel
// // JudgingComponent.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;

namespace DataModel.Models
{
    public class JudgingComponent
    {
        public int Id { get; set; }

        public string Code { get; set; }

        public string Description { get; set; }

        public double Weight { get; set; }

        public int Order { get; set; }

        public virtual ICollection<CompetitionType> CompetitionTypes { get; set; }
    }
}