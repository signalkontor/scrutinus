﻿// // TPS.net TPS8 DataModel
// // Team.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataModel.Models
{
    public class Team : Couple
    {
        public string TeamName { get; set; }

        public string TeamCaptn { get; set; }

        public string Trainer { get; set; }

        public string Theme { get; set; }

        public virtual ICollection<Person> TeamMembers { get; set; }

        [NotMapped]
        public override string NiceName
        {
            get
            {
                return string.Format("{0} ({1}, {2})", this.TeamName, this.Theme, this.Trainer);
            }
        }
    }
}
