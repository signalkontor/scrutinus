// // TPS.net TPS8 DataModel
// // Event.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System;
using System.Collections.Generic;

namespace DataModel.Models
{
    public class Event
    {
        public Event()
        {
            this.Competitions = new List<Competition>();
        }

        public int Id { get; set; }

        public string Title { get; set; }

        public DateTime? DateFrom { get; set; }

        public DateTime? DateTo { get; set; }

        public string Place { get; set; }

        public string Organizer { get; set; }

        public string OrgnizerRegion { get; set; }

        public string Executor { get; set; }

        public string ExecutorRegion { get; set; }

        public string Federation { get; set; }

        public string DanceFloor { get; set; }

        public string SizeDanceFloorWidth { get; set; }

        public string SizeDanceFloorLength { get; set; }

        public string ExternalId { get; set; }

        public bool DtvDownloadDataValid { get; set; }

        public virtual ICollection<Competition> Competitions { get; set; }

        public string RuleName { get; set; }
    }
}