// // TPS.net TPS8 DataModel
// // Participant.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using DataModel.Localization;
using PropertyChanged;
using Scrutinus.Localization;

namespace DataModel.Models
{
    public enum ApiStatus
    {
        Unknown,
        CoupleNotFound,
        ManNotFound,
        WomanNotFound,
        NotACouple,
        WrongDevision,
        WrongAgeGroup,
        OK,
        Registered
    }

    public enum DtvDisqualificationFlag
    {
        None = 0,
        WarningSylabus  = 1,
        DisqualificationSylabus = 2,
        Disqualification = 3
    }

    public enum RegistrationFlag
    {
        NotUsed = 0,
        Normal = 1,
        Late = 2,
        Winner = 3,
        ClimbUp = 4
    }

    [AddINotifyPropertyChangedInterface]
    public class Participant
    {
        public Participant()
        {
            this.Markings = new List<Marking>();
            this.Qualifieds = new List<Qualified>();
            this.DisqualificationFlag = DtvDisqualificationFlag.None;
            this.RegistrationFlag = RegistrationFlag.Normal;
        }

        public int Id { get; set; }

        [AlsoNotifyFor(nameof(StateString))]
        public CoupleState State { get; set; }

        public int Number { get; set; }

        [AlsoNotifyFor(nameof(Place))]
        public int? PlaceFrom { get; set; }

        [AlsoNotifyFor(nameof(Place))]
        public int? PlaceTo { get; set; }

        /// <summary>
        /// Place-From in own competition (if competitins combined)
        /// </summary>
        public int? PlaceFromOwnCompetition { get; set; }

        /// <summary>
        /// Place-To in own competition (if combined)
        /// </summary>
        public int? PlaceToOwnCompetition { get; set; }

        [AlsoNotifyFor(nameof(StarString))]
        public int Star { get; set; }

        /// <summary>
        /// Marks, points etc last round
        /// </summary>
        public double Points { get; set; }

        public bool IsWinnerCouple { get; set; }

        public RegistrationFlag RegistrationFlag { get; set; }

        /// <summary>
        /// Ranking points (WDSF ranking or DTV 
        /// </summary>
        public int? RankingPoints { get; set; }

        public string ExternalId { get; set; }

        public string WdsfId { get; set; }

        [AlsoNotifyFor(nameof(Couple), nameof(ApiStatusString))]
        public ApiStatus WdsfApiStatus { get; set; }

        public DtvDisqualificationFlag DisqualificationFlag { get; set; }

        public bool ClimbUp { get; set; }

        public virtual Competition Competition { get; set; }

        public virtual Couple Couple { get; set; }

        // The Age-Group might be different depending on the competition rather then
        // the couple ("Doppeltstarter" / combined competition) ..., JUN I could dance JUN II ...
        public virtual AgeGroup AgeGroup { get; set; }

        public virtual Class Class { get; set; }

        public virtual ICollection<Marking> Markings { get; set; }

        public virtual Round QualifiedRound { get; set; }

        public virtual ICollection<Qualified> Qualifieds { get; set; }

        /// <summary>
        /// DTV original points before competition
        /// </summary>
        public int? OriginalPoints { get; set; }

        /// <summary>
        /// Original placings befor competition
        /// </summary>
        public int? OriginalPlacings { get; set; }

        /// <summary>
        /// DTV points after competition
        /// </summary>
        public int? NewPoints { get; set; }

        /// <summary>
        /// DTV placings after competition
        /// </summary>
        public int? NewPlacings { get; set; }

        /// <summary>
        /// Gets or sets the DTV target points.
        /// </summary>
        /// <value>
        /// The target points.
        /// </value>
        public int? TargetPoints { get; set; }

        public int? MinimumPoints { get; set; }

        public int? PlacingsUpto { get; set; }

        public bool SylabusWarning { get; set; }

        public DateTime? TimeStampLastPointUpdate { get; set; }

        public virtual Class TargetClass { get; set; }

        /// <summary>
        /// Gets or sets the DTV target placings.
        /// </summary>
        /// <value>
        /// The target placings.
        /// </value>
        public int? TargetPlacings { get; set; }

        /// <summary>
        /// DTV new class if climbup
        /// </summary>
        public virtual Class NewClass { get; set; }

        [NotMapped]
        public string Place
        {
            get
            {
                if (this.PlaceFrom.HasValue && this.PlaceFrom.Value > 0)
                {
                    return this.PlaceFrom == this.PlaceTo
                               ? this.PlaceFrom + "."
                               : String.Format("{0}.-{1}.", this.PlaceFrom, this.PlaceTo);
                }
                else
                {
                    return this.StateString;
                }
            }
        }

        [NotMapped]
        public string PlaceOwnCompetition
        {
            get
            {
                if (this.PlaceFromOwnCompetition.HasValue && this.PlaceFromOwnCompetition.Value > 0)
                {
                    return this.PlaceFromOwnCompetition == this.PlaceToOwnCompetition
                               ? this.PlaceFromOwnCompetition + "."
                               : String.Format("{0}.-{1}.", this.PlaceFromOwnCompetition, this.PlaceToOwnCompetition);
                }
                else
                {
                    return this.StateString;
                }
            }
        }

        [NotMapped]
        public string StateString
        {
            get
            {
                switch (this.State)
                {
                    case CoupleState.Dancing:
                        return LocalizationService.Resolve(() => Text.Dancing);
                    case CoupleState.Missing:
                        return LocalizationService.Resolve(() => Text.Missing);
                    case CoupleState.Excused:
                        return LocalizationService.Resolve(() => Text.Excused);
                    case CoupleState.DroppedOut:
                        return LocalizationService.Resolve(() => Text.Placed);
                    case CoupleState.Disqualified:
                        return LocalizationService.Resolve(() => Text.Disqualified);
                }

                return LocalizationService.Resolve(() => Text.UnknowState);
            }
        }

        [NotMapped]
        public string StarString
        {
            get
            {
                switch (this.Star)
                {
                    case 1:
                        return "*";
                    case 2:
                        return "**";
                }
                if (this.RegistrationFlag == RegistrationFlag.Winner)
                {
                    return LocalizationService.Resolve(() => Text.WinnerStarString);
                }
                if (this.RegistrationFlag == RegistrationFlag.ClimbUp)
                {
                    return LocalizationService.Resolve(() => Text.ClimbUpStarString);
                }

                return "";
            }
        }

        [NotMapped]
        public string ApiStatusString
        {
            get
            {
                if (this.Couple == null)
                {
                    return "";
                }

                if (this.WdsfApiStatus == ApiStatus.Unknown && this.Couple.WdsfApiStatus != ApiStatus.Unknown)
                {
                    return this.Couple.WdsfApiStatus.ToString();
                }

                switch (this.WdsfApiStatus)
                {
                    case ApiStatus.CoupleNotFound:
                        return "Not Found";
                    case ApiStatus.OK:
                        return "OK";
                    case ApiStatus.ManNotFound:
                        return "Man not found";
                    case ApiStatus.WomanNotFound:
                        return "Woman not found";
                    case ApiStatus.NotACouple:
                        return "Not a couple";
                    case ApiStatus.Registered:
                        return "Registered";
                    case ApiStatus.Unknown:
                        return "Unknown";
                }

                return "Unknown Status";
            }
        }

        [NotMapped]
        public string NiceNameWithNumber => $"{this.Number} - {this.Couple.NiceName}";

        [NotMapped]
        public string PlaceNumberAndName => $"{this.Place}) {this.Number} - {this.Couple.NiceName}";

        [NotMapped]
        public string ClimbUpString => this.ClimbUp ? "Aufstieg!" : "";

        [NotMapped]
        public string ApiErrorMessage { get; set; }

        [NotMapped]
        public Startbuch Startbuch
        {
            get
            {
                if (this.Competition != null && this.Competition.Section != null)
                {
                    return this.Couple.Startbooks.FirstOrDefault(s => s.Section.Id == this.Competition.Section.Id);
                }
                else
                {
                    return null;
                }
            }
        }
    }
}