﻿// // TPS.net TPS8 DataModel
// // CompetitionType.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataModel.Models
{
    public class CompetitionType
    {
        public CompetitionType()
        {
            this.JudgingComponents = new List<JudgingComponent>();
        }

        public int Id { get; set; }

        public string Federation { get; set; }

        public string TypeString { get; set; }

        public string ShortName { get; set; }

        public string CompetitionKind { get; set; }

        public bool HasRankingPoints { get; set; }

        [NotMapped]
        public bool IsFormation { get; set; }

        [NotMapped]
        public bool IsJMD { get; set; }

        [NotMapped]
        public double Js3CutOff { get; set; }

        public virtual CompetitionRule CompetitionRule { get; set; }

        public virtual ICollection<JudgingComponent> JudgingComponents { get; set; }
    }
}