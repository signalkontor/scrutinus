﻿// // TPS.net TPS8 DataModel
// // Parameter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataModel.Models
{
    /// <summary>
    /// A POCO object to store user application parameters
    /// </summary>
    [Serializable]
    public class Parameter 
    {
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the name of the parameter.
        /// </summary>
        /// <value>
        /// The name of the parameter.
        /// </value>
        [Index(IsUnique = true)]
        [MaxLength(250)]
        [Required]
        public string ParameterName { get; set; }

        /// <summary>
        /// Gets or sets the serialized value.
        /// </summary>
        /// <value>
        /// The serialized value.
        /// </value>
        [MaxLength]
        [Column(TypeName = "image")]
        public byte[] SerializedValue { get; set; }
    }
}
