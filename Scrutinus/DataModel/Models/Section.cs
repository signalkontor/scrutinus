// // TPS.net TPS8 DataModel
// // Section.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System.Collections.Generic;

namespace DataModel.Models
{
    public partial class Section
    {
        public Section()
        {
            this.Competitions = new List<Competition>();
        }

        public int Id { get; set; }

        public string SectionName { get; set; }

        public string ShortName { get; set; }

        public bool IsTeam { get; set; }

        public virtual ICollection<Competition> Competitions { get; set; }

        public virtual ICollection<SectionAgeGroupClassesMapping> AgeGroupClassesMappings { get; set; }
    }
}