// // TPS.net TPS8 DataModel
// // Round.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using PropertyChanged;
using Scrutinus.Localization;

namespace DataModel.Models
{
    [AddINotifyPropertyChangedInterface]
    public class Round : INotifyPropertyChanged
    {
        private int? marksFrom;
        private string name;

        public Round()
        {
            this.DancesInRound = new List<DanceInRound>();
            this.Markings = new List<Marking>();
            this.Qualifieds = new List<Qualified>();
            this.PlaceOffset = 0;
            this.MinimumPointsJs2 = 0.5;
            this.MaximumPointsJs2 = 10;
            this.Intervall = 0.5;
        }

        public int Id { get; set; }

        public MarkNumberType MarksNumberType { get; set; }

        public int Number { get; set; }

        public int? MarksFrom
        {
            get
            {
                return this.marksFrom;
            }
            set
            {
                this.marksFrom = value;
                this.MarksTo = value;
            }
        }

        public int? MarksTo { get; set; }

        public int? DrawingType { get; set; }

        public int? CouplesPerHeat { get; set; }

        public CompetitionState State { get; set; }

        public MarkingTypes MarksInputType { get; set; }

        public bool IsRedanceRound { get; set; }

        public int PlaceOffset { get; set; }

        public double MinimumPointsJs2 { get; set; }

        public double MaximumPointsJs2 { get; set; }

        public double Js3CutOffValue { get; set; }

        public double Intervall { get; set; }

        public bool EjudgeEnabled { get; set; }

        public virtual ICollection<EjudgeData> EjudgeData { get; set; }

        public virtual Competition Competition { get; set; }

        public virtual ICollection<DanceInRound> DancesInRound { get; set; }

        public virtual ICollection<Marking> Markings { get; set; }

        public virtual ICollection<Qualified> Qualifieds { get; set; }

        public virtual ICollection<Drawing> Drawings { get; set; }

        public virtual RoundType RoundType { get; set; }

        [NotMapped]
        public string Name
        {
            get
            {
                if (!string.IsNullOrEmpty(this.name))
                {
                    return this.name;
                }

                if (this.Competition == null)
                {
                    return " - ";
                }

                if (this.RoundType == null)
                {
                    return "";
                }
                
                if (LocalizationService.ApplicationLanguage.TwoLetterISOLanguageName.ToLower() == "de")
                {
                    return this.GetGermanRoundName();
                }

                return this.GetEnglishRoundName();
            }

            set
            {
                this.name = value;
            }
        }

        [NotMapped]
        public string ShortName
        {
            get
            {
                var name = this.Name;

                return name.Replace("Zwischenrunde", "ZR").Replace("Vorrunde", "VR").Replace("Round", "Rnd");
            }
        }

        [NotMapped]
        public string WdsfName
        {
            get
            {
                if (this.IsRedanceRound)
                {
                    return "R";
                }

                if (this.RoundType.IsFinal)
                {
                    return "F";
                }

                var hasRedance = this.Competition.Rounds.Any(r => r.RoundType.Id == RoundTypes.Redance);
                
                if (this.Number > 1 && hasRedance)
                {
                    return string.Format("{0}", this.Number - 1);
                }

                return string.Format("{0}", this.Number);
            }
        }

        [NotMapped]
        public string EjudgeName
        {
            get
            {
                if (this.Competition != null)
                {
                    return string.Format("{0}_##_{1}", this.Competition.Id, this.Id);
                }

                return "unknown";
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private string GetEnglishRoundName()
        {
            if (this.RoundType != null && this.Number == 1)
            {
                return this.RoundType.Id == RoundTypes.Final ? "Final" : "1. Round";
            }

            if (this.RoundType != null && this.RoundType.Id == RoundTypes.Final)
            {
                return "Final";
            }

            var hasRedance = this.Competition.Rounds.Any(r => r.RoundType != null && r.RoundType.Id == RoundTypes.Redance);

            var hasSecondFirstRound =
                this.Competition.Rounds.Any(r => r.RoundType != null && r.RoundType.Id == RoundTypes.Redance);

            if (this.Number == 2)
            {
                if (hasRedance)
                {
                    return "Redance";
                }
                if (hasSecondFirstRound)
                {
                    return "2. First Round";
                }

                return "2. Round";
            }

            if (hasRedance || hasSecondFirstRound)
            {
                return string.Format("{0}. Round", this.Number - 1);
            }
            else
            {
                return string.Format("{0}. Round", this.Number);
            }
        }

        private string GetGermanRoundName()
        {
            if (this.Number == 1)
            {
                return this.RoundType.Id == RoundTypes.Final ? "Finale" : "1. Vorrunde";
            }

            if (this.RoundType.Id == RoundTypes.Final)
            {
                return "Finale";
            }

            var hasRedance = this.Competition.Rounds.Any(r => r.RoundType != null && r.RoundType.Id == RoundTypes.Redance);

            var hasSecondFirstRound = this.Competition.Rounds.Any(
                r => r.RoundType != null && r.RoundType.Id == RoundTypes.SecondFirstRound)
                                      || this.Competition.FirstRoundType == RoundTypes.SecondFirstRound;

            if (this.Number == 2)
            {
                if (hasRedance)
                {
                    return "Redance";
                }

                if (hasSecondFirstRound)
                {
                    return "2. Vorrunde";
                }

                if (this.RoundType.Id == RoundTypes.Final)
                {
                    return "Finale";
                }

                return "1. Zwischenrunde";
            }


            if (hasRedance || hasSecondFirstRound)
            {
                return string.Format("{0}. Zwischenrunde", this.Number - 2);
            }
            else
            {
                return string.Format("{0}. Zwischenrunde", this.Number - 1);
            }
        }
    }
}