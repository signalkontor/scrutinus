// // TPS.net TPS8 DataModel
// // Dance.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DataModel.Models
{
    public class Dance
    {
        public Dance()
        {
            this.DanceInRound = new List<DanceInRound>();
            this.DancesCompetition = new List<DancesInCompetition>();
        }

        public int Id { get; set; }

        public string DanceName { get; set; }

        public string ShortName { get; set; }

        [MaxLength(5)]
        public string WdsfCode { get; set; }

        public int DefaultOrder { get; set; }

        public virtual ICollection<DanceInRound> DanceInRound { get; set; }

        public virtual ICollection<DancesInCompetition> DancesCompetition { get; set; }
    }
}