// // TPS.net TPS8 DataModel
// // Class.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System.Collections.Generic;

namespace DataModel.Models
{
    public class Class
    {
        public Class()
        {
            this.Competitions = new List<Competition>();
        }

        public int Id { get; set; }

        public string ClassShortName { get; set; }

        public string ClassLongName { get; set; }

        public int OrderWhenCombined { get; set; }

        public virtual ICollection<Competition> Competitions { get; set; }

        public virtual ICollection<SectionAgeGroupClassesMapping> AgeGroupMappings { get; set; }
    }
}