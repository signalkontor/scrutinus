// // TPS.net TPS8 DataModel
// // XmlLoader.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using General.Extensions;

namespace DataModel.Models
{
    public class XmlLoader
    {
        /// <summary>
        /// Sets a value by reflection
        /// </summary>
        /// <param name="obj">The objects thats value we want to set</param>
        /// <param name="property">PropteryInfo object of the property to set</param>
        /// <param name="value">the value to set</param>
        protected void SetValue(object obj, PropertyInfo property, string value)
        {
            if (property.PropertyType == typeof(int?) || property.PropertyType == typeof(int))
            {
                if (String.IsNullOrEmpty(value))
                {
                    return;
                }
                property.GetSetMethod().Invoke(obj, new object[] { Int32.Parse(value) });
                return;
            }
            if (property.PropertyType == typeof(string))
            {
                property.GetSetMethod().Invoke(obj, new object[] { value });
                return;
            }
            if (property.PropertyType == typeof(double) || property.PropertyType == typeof(double?))
            {
                property.GetSetMethod().Invoke(obj, new object[] { value.SaveParse() });
                return;
            }
            if (property.PropertyType == typeof(bool) || property.PropertyType == typeof(Boolean))
            {
                property.GetSetMethod().Invoke(obj, new object[] { value.ToLower() == "true" ? true : false });
                return;
            }

            Console.WriteLine("Unknown Type of Proptery: " + property.PropertyType.FullName);
        }

        protected T LoadType<T>(XmlNode node)
        {
            var type = typeof(T);
            var constructor = type.GetConstructor(new Type[] { });
            var obj = (T)constructor.Invoke(null);
            // We try to fill the object now
            foreach (XmlAttribute attr in node.Attributes)
            {
                if (attr.Name.EndsWith("Id") && attr.Name.Length > 2)
                {
                    // we skip this for now:
                    Debug.WriteLine("Skipping {0} of Class {1}", attr.Name, type.ToString());
                    continue;
                }

                var property = type.GetProperty(attr.Name);
                if (property != null)
                {
                    this.SetValue(obj, property, attr.Value);
                }
                else
                {
                    throw new Exception("Unknown Property Name " + attr.Name);
                }
            }

            return obj;
        }

        public void LoadFromXml(ScrutinusContext context, string xmlResourceName)
        {
            if (string.IsNullOrEmpty(xmlResourceName))
            {
                throw new ArgumentException("xmlResourceName may not be null or empty");
            }

            var doc = new XmlDocument();

            Stream stream = null;

            if (File.Exists(xmlResourceName))
            {
                using (stream = new FileStream(xmlResourceName, FileMode.Open))
                {
                    doc.Load(stream);
                }
            }
            else
            {
                using (stream = typeof(XmlLoader).Assembly.GetManifestResourceStream(xmlResourceName))
                {
                    doc.Load(stream);
                }
            }


            // Load all know classes form the XML File

            var sections = doc.GetElementsByTagName("Section");
            foreach (XmlNode section in sections)
            {
                var obj = this.LoadType<Section>(section);
                context.Sections.Add(obj);
            }
            context.SaveChanges();
            var dances = doc.GetElementsByTagName("Dance");
            foreach (XmlNode dance in dances)
            {
                var obj = this.LoadType<Dance>(dance);
                context.Dances.Add(obj);
            }
            context.SaveChanges();

            var roles = doc.GetElementsByTagName("Role");
            foreach (XmlNode role in roles)
            {
                context.Roles.Add(this.LoadType<Role>(role));
            }
            context.SaveChanges();

            var roundTypes = doc.GetElementsByTagName("RoundType");
            foreach (XmlNode roundType in roundTypes)
            {
                context.RoundTypes.Add(this.LoadType<RoundType>(roundType));
            }
            context.SaveChanges();

            var drawingTypes = doc.GetElementsByTagName("DrawingType");
            foreach (XmlNode drawingType in drawingTypes)
            {
                context.DrawingTypes.Add(this.LoadType<DrawingType>(drawingType));
            }
            context.SaveChanges();

            var classes = doc.GetElementsByTagName("Class");
            foreach (XmlNode cl in classes)
            {
                context.Classes.Add(this.LoadType<Class>(cl));
            }
            context.SaveChanges();

            var ageGroups = doc.GetElementsByTagName("AgeGroup");
            foreach (XmlNode ageGroup in ageGroups)
            {
                var obj = this.LoadType<AgeGroup>(ageGroup);
                context.AgeGroups.Add(obj);
            }

            context.SaveChanges();

            var sectionAgeGroupMappings = doc.GetElementsByTagName("SectionAgeGroupClassesMapping");
            foreach (XmlNode mapping in sectionAgeGroupMappings)
            {
                var map = this.LoadType<SectionAgeGroupClassesMapping>(mapping);
                context.SectionAgeGroupClassesMappings.Add(map);
                if (mapping.Attributes["SectionId"] != null)
                {
                    var sectionId = int.Parse(mapping.Attributes["SectionId"].Value);
                    map.Section = context.Sections.Single(s => s.Id == sectionId);
                }

                if (mapping.Attributes["AgeGroupId"] != null)
                {
                    var ageGroupId = int.Parse(mapping.Attributes["AgeGroupId"].Value);
                    map.AgeGroup = context.AgeGroups.Single(a => a.Id == ageGroupId);
                }

                map.Classes = new List<Class>();
                if (mapping.Attributes["Classes"] != null)
                {
                    var classesIds = mapping.Attributes["Classes"].Value.Split(',');
                    foreach (var classesId in classesIds)
                    {
                        var id = Int32.Parse(classesId);
                        var classObj = context.Classes.Single(c => c.Id == id);
                        map.Classes.Add(classObj);
                    }
                }
            }

            context.SaveChanges();

            var competitionRules = doc.GetElementsByTagName("CompetitionRule");
            foreach (XmlNode cr in competitionRules)
            {
                var competitionRule = this.LoadType<CompetitionRule>(cr);
                var firstRoundTypeId = Int32.Parse(cr.Attributes["FirstRoundTypeId"].Value);
                if (cr.Attributes["DefaultNumberOfMarks"] != null)
                {
                    var markNumberType = MarkNumberType.Undefined;
                    Enum.TryParse(
                        cr.Attributes["DefaultNumberOfMarks"].Value,
                        out markNumberType);

                    competitionRule.DefaultNumberOfMarks = markNumberType;
                }
                competitionRule.RoundType = context.RoundTypes.Single(r => r.Id == firstRoundTypeId);
                context.CompetitionRules.Add(competitionRule);
            }
            context.SaveChanges();

            var competitionTypes = doc.GetElementsByTagName("CompetitionType");
            foreach (XmlNode competitionType in competitionTypes)
            {
                var type = this.LoadType<CompetitionType>(competitionType);
                var competitionRuleId = Int32.Parse(competitionType.Attributes["CompetitionRuleId"].Value);
                type.CompetitionRule = context.CompetitionRules.Single(r => r.Id == competitionRuleId);
                context.CompetitionTypes.Add(type);

                context.SaveChanges();
            }

            var judgingComponents = doc.GetElementsByTagName("JudgingComponent");
            foreach (XmlNode judgingComponent in judgingComponents)
            {
                context.JudgingComponents.Add(this.LoadType<JudgingComponent>(judgingComponent));
            }
            context.SaveChanges();

            var JudgingComponentsCompetitionTypes = doc.GetElementsByTagName("JudgingComponentsCompetitionType");
            foreach (XmlNode element in JudgingComponentsCompetitionTypes)
            {
                var componentId = Int32.Parse(element.Attributes["JudgingComponent"].Value);
                var typeId = Int32.Parse(element.Attributes["CompetitionType"].Value);
                var component = context.JudgingComponents.Single(c => c.Id == componentId);
                var type = context.CompetitionTypes.Single(t => t.Id == typeId);
                type.JudgingComponents.Add(component);
            }
            context.SaveChanges();

            var danceTemplates = doc.GetElementsByTagName("DanceTemplate");
            foreach (XmlNode danceTemplateXml in danceTemplates)
            {
                var danceTemplate = this.LoadType<DanceTemplate>(danceTemplateXml);

                var sectionId = int.Parse(danceTemplateXml.Attributes["SectionId"].Value);
                danceTemplate.Section = context.Sections.Single(s => s.Id == sectionId);

                if (danceTemplateXml.Attributes["ClassId"] != null)
                {
                    var classId = Int32.Parse(danceTemplateXml.Attributes["ClassId"].Value);
                    danceTemplate.Class = context.Classes.Single(c => c.Id == classId);
                }
                else
                {
                    danceTemplate.Class = null;
                }

                if (danceTemplateXml.Attributes["AgeGroupId"] != null)
                {
                    var ageGroupId = Int32.Parse(danceTemplateXml.Attributes["AgeGroupId"].Value);
                    danceTemplate.AgeGroup = context.AgeGroups.Single(c => c.Id == ageGroupId);
                }
                else
                {
                    danceTemplate.AgeGroup = null;
                }

                context.DanceTemplates.Add(danceTemplate);

                context.SaveChanges();
            }

            var officials = doc.GetElementsByTagName("Official");
            foreach (XmlNode official in officials)
            {
                var obj = this.LoadType<Official>(official);

                var roleIds = official.Attributes["Roles"].Value.Split(',');
                for (var i = 0; i < roleIds.Length; i++)
                {
                    int idOut;
                    if (Int32.TryParse(roleIds[i], out idOut))
                    {
                        var roletoadd = context.Roles.SingleOrDefault(r => r.Id == idOut);
                        if (roletoadd != null)
                        {
                            obj.Roles.Add(roletoadd);
                        }
                    }
                }

                context.Officials.Add(obj);
                context.SaveChanges();
            }
            var events = doc.GetElementsByTagName("Event");
            foreach (XmlNode ev in events)
            {
                context.Events.Add(this.LoadType<Event>(ev));
            }
            context.SaveChanges();
            var competitions = doc.GetElementsByTagName("Competition");
            foreach (XmlNode competitionXml in competitions)
            {
                var competition = this.LoadType<Competition>(competitionXml);
                var sectionId = Int32.Parse(competitionXml.Attributes["SectionId"].Value);
                var competitionTypeId = Int32.Parse(competitionXml.Attributes["CompetitionTypeId"].Value);
                var AgeGroupId = Int32.Parse(competitionXml.Attributes["AgeGroupId"].Value);
                var classId = Int32.Parse(competitionXml.Attributes["ClassId"].Value);

                competition.Section = context.Sections.Single(s => s.Id == sectionId);
                competition.Event = context.Events.First();
                competition.CompetitionType = context.CompetitionTypes.Single(c => c.Id == competitionTypeId);
                competition.AgeGroup = context.AgeGroups.Single(a => a.Id == AgeGroupId);
                competition.Class = context.Classes.Single(c => c.Id == classId);

                competition.ExternalId = competitionXml.Attributes["ExternalId"].Value;

                context.Competitions.Add(competition);
            }
            context.SaveChanges();
        }
    }
}