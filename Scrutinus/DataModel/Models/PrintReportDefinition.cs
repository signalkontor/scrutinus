﻿// // TPS.net TPS8 DataModel
// // PrintReportDefinition.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH
namespace DataModel.Models
{
    public class PrintReportDefinition
    {
        public int Id { get; set; }

        public string ReportName { get; set; }

        public string PrintReportType { get; set; }

        public PrintCollectionType PrintCollectionType { get; set; }

        public virtual Printer Printer { get; set; }

        public int Copies { get; set; }

        public bool PrintColor { get; set; }

        public override bool Equals(object obj)
        {
            var report = obj as PrintReportDefinition;
            if (report == null)
            {
                return false;
            }

            return this.ReportName == report.ReportName;
        }

        public override int GetHashCode()
        {
            return this.ReportName.GetHashCode();
        }
    }
}