﻿// // TPS.net TPS8 DataModel
// // SerialiazableOfficial.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.ComponentModel;

namespace DataModel.Models
{
    [Serializable]
    public class SerialiazableOfficial : INotifyPropertyChanged
    {
        private string country;
        private string firstName;

        private string lastName;

        private string min;

        private string region;

        public string FirstName
        {
            get
            {
                return this.firstName;
            }
            set
            {
                this.firstName = value;
                this.RaisePropertyChanged("FirstName");
            }
        }

        public string LastName
        {
            get
            {
                return this.lastName;
            }
            set
            {
                this.lastName = value;
                this.RaisePropertyChanged("LastName");
            }
        }

        public string Country
        {
            get
            {
                return this.country;
            }
            set
            {
                this.country = value;
                this.RaisePropertyChanged("Country");
            }
        }

        public string Region
        {
            get
            {
                return this.region;
            }
            set
            {
                this.region = value;
                this.RaisePropertyChanged("Region");
            }
        }

        public string Min
        {
            get
            {
                return this.min;
            }
            set
            {
                this.min = value;
                this.RaisePropertyChanged("Min");
            }
        }

        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string property)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }
    }
}
