// // TPS.net TPS8 DataModel
// // RoundType.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System.Collections.Generic;

namespace DataModel.Models
{
    public class RoundType
    {
        public RoundType()
        {
            this.Rounds = new List<Round>();
        }

        public int Id { get; set; }

        public bool IsFinal { get; set; }

        public bool HasDrawing { get; set; }

        public string Name { get; set; }

        public string AllowedMarkingTypes { get; set; }

        public virtual ICollection<Round> Rounds { get; set; }
    }
}