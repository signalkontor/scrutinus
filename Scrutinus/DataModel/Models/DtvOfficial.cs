﻿// // TPS.net TPS8 DataModel
// // DtvOfficial.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

namespace DataModel.Models
{
    public class DtvOfficial
    {
        public int Id { get; set; }

        public string DtvId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Club { get; set; }

        public string Ltv { get; set; }

        public string Licenses { get; set; }
    }
}
