// // TPS.net TPS8 DataModel
// // DeviceStates.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH
namespace mobileControl.Model
{
    public enum DeviceStates
    {
        StartNoData = 0,
        StartDataLoaded,
        StartRelease,
        StartOldData,
        MarkingFinal,
        FinalAllPlacesSet,
        FinalDanceConfirmed,
        MarkingQualification,
        QualfificationHelpmarks,
        LanguageSelection,
        Message,
        Signature,
        CompleteMarking
    }
}