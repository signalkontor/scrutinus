// // TPS.net TPS8 DataModel
// // DatabaseCreator.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System.Collections.Generic;
using System.Data.Entity;
using System.IO;

namespace DataModel.Models
{
    public class DatabaseCreator : CreateDatabaseIfNotExists<ScrutinusContext>
    {
        private const int DatabaseVersion = 5;
        private string xmlResourcename;

        public DatabaseCreator(string xmlRuleFile)
        {
            this.xmlResourcename = xmlRuleFile;
        }

        public void LoadFromXml(ScrutinusContext context, string initialContent)
        {
            this.xmlResourcename = initialContent;
            var loader = new XmlLoader();
            loader.LoadFromXml(context, initialContent);
        }

        protected override void Seed(ScrutinusContext context)
        {
            base.Seed(context);

            if (this.xmlResourcename == null)
            {
                return;
            }

            // Load initial Content from InitialContent.xml
            this.LoadFromXml(context, this.xmlResourcename);

            context.SaveChanges();

            this.LoadCounties(context);

            context.SaveChanges();

            context.Events.Add(new Event()
                                   {
                                       Competitions = new List<Competition>(),
                                       Title = ""
                                   });

            context.SaveChanges();

            context.SetParameter("DatabaseVersion", DatabaseVersion);
        }

        private void LoadCounties(ScrutinusContext context)
        {
            using (var stream = typeof(XmlLoader).Assembly.GetManifestResourceStream("DataModel.InitialData.counties.txt"))
            {
                var inStr = new StreamReader(stream);

                while (!inStr.EndOfStream)
                {
                    var data = inStr.ReadLine().Split('\t');
                    context.CountryCodes.Add(new CountryCode() { LongName = data[0], IocCode = data[1] });
                }
            }
            
            context.SaveChanges();
        }
    }
}