// // TPS.net TPS8 DataModel
// // Official.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading;
using PropertyChanged;

namespace DataModel.Models
{
    [AddINotifyPropertyChangedInterface]
    [Serializable]
    public class Official : ModelBaseClass
    {
        [NonSerialized]
        private ICollection<OfficialInCompetition> officialCompetition;

        [NonSerialized]
        private ICollection<Role> roles;

        public Official()
        {
            this.OfficialCompetition = new List<OfficialInCompetition>();
            this.Roles = new List<Role>();
            // default language depends on our language
            this.DefaultLanguage = Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName.ToLower() == "de" ? 0 : 1;
        }

        public int Id { get; set; }

        public string Sign { get; set; }

        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public string Club { get; set; }

        public string Region { get; set; }

        public virtual CountryCode Nationality { get; set; }

        public string PinCode { get; set; }

        public int? MIN { get; set; }

        public string DeviceName { get; set; }

        [MaxLength(50)]
        public string ExternalId { get; set; }

        [MaxLength(50)]
        public string WdsfId { get; set; }

        public string Licenses { get; set; }

        public int DefaultLanguage { get; set; }

        [AlsoNotifyFor("ApiStatusText")]
        public ApiStatus WdsfApiStatus { get; set; }

        [MaxLength(10)]
        public string Gender { get; set; }

        public virtual ICollection<OfficialInCompetition> OfficialCompetition { 
            get
            {
                return this.officialCompetition;
            } 
            
            set
            {
                this.officialCompetition = value;
                this.RaisePropertyChanged(() => this.OfficialCompetition);
            } 
        }

        public virtual ICollection<Role> Roles
        {
            get
            {
                return this.roles;
            }
            set
            {
                this.roles = value;
                this.RaisePropertyChanged(() => this.Roles);
            }
        }

        [NotMapped]
        public string NiceName
        {
            get
            {
                return String.Format("{0} {1}", this.Firstname, this.Lastname);
            }
        }

        [NotMapped]
        public string RolesAsString
        {
            get
            {
                var str = this.Roles.OrderBy(r => r.RoleShort).Aggregate("", (current, role) => current + ", " + role.RoleShort);
                if (str.StartsWith(", "))
                {
                    str = str.Substring(2);
                }
                return str;
            }
        }

        [NotMapped]
        public string ApiStatusText
        {
            get
            {
                switch (this.WdsfApiStatus)
                {
                    case ApiStatus.CoupleNotFound:
                        return "Not Found";
                    case ApiStatus.OK:
                        return "OK";
                    case ApiStatus.Registered:
                        return "Registered";
                    case ApiStatus.Unknown:
                        return "Unknown";
                }

                return "Unknown Status";
            }
        }
    }
}