﻿// // TPS.net TPS8 DataModel
// // SimpleStartbuchMap.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Data.Entity.ModelConfiguration;

namespace DataModel.Models.Mapping
{
    internal class SimpleStartbuchMap : EntityTypeConfiguration<SimpleStartbuch>
    {
        public SimpleStartbuchMap()
        {
            this.HasKey(p => p.Id);
        }
    }
}
