﻿// // TPS.net TPS8 DataModel
// // SectionAgeGroupClassesMappingMap.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Data.Entity.ModelConfiguration;

namespace DataModel.Models.Mapping
{
    internal class SectionAgeGroupClassesMappingMap : EntityTypeConfiguration<SectionAgeGroupClassesMapping>
    {
        public SectionAgeGroupClassesMappingMap()
        {
            this.HasKey(k => k.Id);

            this.HasRequired(t => t.Section).WithMany(t => t.AgeGroupClassesMappings);

            this.HasRequired(t => t.AgeGroup);

            this.HasMany(t => t.Classes).WithMany(p => p.AgeGroupMappings);
        }
    }
}
