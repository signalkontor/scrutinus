﻿// // TPS.net TPS8 DataModel
// // CompetitionRuleMap.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DataModel.Models.Mapping
{
    internal class CompetitionRuleMap : EntityTypeConfiguration<CompetitionRule>
    {
        public CompetitionRuleMap()
        {
            this.HasKey(c => c.Id);
            this.Property(p => p.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            
            this.ToTable("CompetitionRule");
        }
    }
}