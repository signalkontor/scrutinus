// // TPS.net TPS8 DataModel
// // DtvOfficialMap.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System.Data.Entity.ModelConfiguration;

namespace DataModel.Models.Mapping
{
    public class DtvOfficialMap : EntityTypeConfiguration<DtvOfficial>
    {
        public DtvOfficialMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);
        }
    }
}