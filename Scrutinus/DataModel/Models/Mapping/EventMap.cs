// // TPS.net TPS8 DataModel
// // EventMap.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System.Data.Entity.ModelConfiguration;

namespace DataModel.Models.Mapping
{
    public class EventMap : EntityTypeConfiguration<Event>
    {
        public EventMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Title).IsRequired().HasMaxLength(500);

            this.Property(t => t.Place).HasMaxLength(150);

            this.Property(t => t.Organizer).HasMaxLength(250);

            this.Property(t => t.Federation).HasMaxLength(150);
        }
    }
}