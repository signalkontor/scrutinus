﻿// // TPS.net TPS8 DataModel
// // CountryCodeMap.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Data.Entity.ModelConfiguration;

namespace DataModel.Models.Mapping
{
    internal class CountryCodeMap : EntityTypeConfiguration<CountryCode>
    {
        public CountryCodeMap()
        {
            this.HasKey(t => t.Id);
        }
    }
}