﻿// // TPS.net TPS8 DataModel
// // JudgingComponentMap.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DataModel.Models.Mapping
{
    internal class JudgingComponentMap : EntityTypeConfiguration<JudgingComponent>
    {
        public JudgingComponentMap()
        {
            this.HasKey(k => k.Id);
            this.Property(p => p.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
        }
    }
}