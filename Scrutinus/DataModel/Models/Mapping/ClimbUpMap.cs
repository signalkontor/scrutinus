﻿// // TPS.net TPS8 DataModel
// // ClimbUpMap.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Data.Entity.ModelConfiguration;

namespace DataModel.Models.Mapping
{
    public class ClimbUpMap : EntityTypeConfiguration<ClimbUp>
    {
        public ClimbUpMap()
        {
            this.HasKey(p => p.Id);

            this.HasRequired(r => r.Competition);
        }
    }
}