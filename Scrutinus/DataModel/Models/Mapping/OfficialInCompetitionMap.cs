// // TPS.net TPS8 DataModel
// // OfficialInCompetitionMap.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System.Data.Entity.ModelConfiguration;

namespace DataModel.Models.Mapping
{
    public class OfficialInCompetitionMap : EntityTypeConfiguration<OfficialInCompetition>
    {
        public OfficialInCompetitionMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Relationships
            this.HasRequired(t => t.Competition).WithMany(t => t.Officials);

            this.HasRequired(t => t.Official).WithMany(t => t.OfficialCompetition);
        }
    }
}