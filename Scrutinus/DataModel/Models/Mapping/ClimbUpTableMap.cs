﻿// // TPS.net TPS8 DataModel
// // ClimbUpTableMap.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Data.Entity.ModelConfiguration;

namespace DataModel.Models.Mapping
{
    public class ClimbUpTableMap : EntityTypeConfiguration<ClimbUpTable>
    {
        public ClimbUpTableMap()
        {
            this.HasKey(p => p.Id);
        }
    }
}
