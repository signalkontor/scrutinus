// // TPS.net TPS8 DataModel
// // DanceMap.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DataModel.Models.Mapping
{
    public class DanceMap : EntityTypeConfiguration<Dance>
    {
        public DanceMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);
            this.Property(p => p.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Properties
            this.Property(t => t.DanceName).IsRequired().HasMaxLength(50);

            this.Property(t => t.ShortName).IsRequired().HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Dance");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.DanceName).HasColumnName("DanceName");
            this.Property(t => t.ShortName).HasColumnName("ShortName");
        }
    }
}