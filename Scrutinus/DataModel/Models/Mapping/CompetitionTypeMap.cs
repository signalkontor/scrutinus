﻿// // TPS.net TPS8 DataModel
// // CompetitionTypeMap.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DataModel.Models.Mapping
{
    internal class CompetitionTypeMap : EntityTypeConfiguration<CompetitionType>
    {
        public CompetitionTypeMap()
        {
            this.HasKey(t => t.Id);
            this.Property(p => p.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.ToTable("CompetitionType");

            this.Property(t => t.TypeString).IsRequired().HasMaxLength(50);
            this.Property(t => t.Federation).IsOptional().HasMaxLength(50);

            this.HasRequired(t => t.CompetitionRule).WithMany(t => t.CompetitionTypes);
        }
    }
}