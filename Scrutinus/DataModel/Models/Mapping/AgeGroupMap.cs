// // TPS.net TPS8 DataModel
// // AgeGroupMap.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DataModel.Models.Mapping
{
    public class AgeGroupMap : EntityTypeConfiguration<AgeGroup>
    {
        public AgeGroupMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);
            this.Property(p => p.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Properties
            this.Property(t => t.AgeGroupName).IsRequired().HasMaxLength(50);
        }
    }
}