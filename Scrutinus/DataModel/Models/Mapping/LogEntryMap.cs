﻿// // TPS.net TPS8 DataModel
// // LogEntryMap.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Data.Entity.ModelConfiguration;

namespace DataModel.Models.Mapping
{
    internal class LogEntryMap : EntityTypeConfiguration<LogEntry>
    {
        public LogEntryMap()
        {
            this.HasKey(p => p.Id);
        }
    }
}
