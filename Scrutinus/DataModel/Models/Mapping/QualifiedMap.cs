// // TPS.net TPS8 DataModel
// // QualifiedMap.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System.Data.Entity.ModelConfiguration;

namespace DataModel.Models.Mapping
{
    public class QualifiedMap : EntityTypeConfiguration<Qualified>
    {
        public QualifiedMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Relationships
            this.HasRequired(t => t.Participant).WithMany(t => t.Qualifieds);

            this.HasRequired(t => t.Round).WithMany(t => t.Qualifieds);
        }
    }
}