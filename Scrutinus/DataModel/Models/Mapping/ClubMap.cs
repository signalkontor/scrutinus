﻿// // TPS.net TPS8 DataModel
// // ClubMap.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Data.Entity.ModelConfiguration;

namespace DataModel.Models.Mapping
{
    public class ClubMap : EntityTypeConfiguration<Club>
    {
        public ClubMap()
        {
            this.HasKey(k => k.Id);

            this.Property(p => p.ClubName).HasMaxLength(100);

            this.Property(p => p.Region).HasMaxLength(100);
        }
    }
}
