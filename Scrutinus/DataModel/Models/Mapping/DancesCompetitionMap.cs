// // TPS.net TPS8 DataModel
// // DancesCompetitionMap.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System.Data.Entity.ModelConfiguration;

namespace DataModel.Models.Mapping
{
    public class DancesCompetitionMap : EntityTypeConfiguration<DancesInCompetition>
    {
        public DancesCompetitionMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Relationships
            this.HasRequired(t => t.Competition).WithMany(t => t.Dances).WillCascadeOnDelete(false);

            this.HasRequired(t => t.Dance).WithMany(t => t.DancesCompetition).WillCascadeOnDelete(false);
        }
    }
}