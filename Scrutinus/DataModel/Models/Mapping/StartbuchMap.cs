﻿// // TPS.net TPS8 DataModel
// // StartbuchMap.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Data.Entity.ModelConfiguration;

namespace DataModel.Models.Mapping
{
    public class StartbuchMap : EntityTypeConfiguration<Startbuch>
    {
        public StartbuchMap()
        {
            this.HasKey(t => t.Id);
        }
    }
}
