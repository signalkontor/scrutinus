// // TPS.net TPS8 DataModel
// // RoundTypeMap.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DataModel.Models.Mapping
{
    public class RoundTypeMap : EntityTypeConfiguration<RoundType>
    {
        public RoundTypeMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);
            this.Property(p => p.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Properties
            this.Property(t => t.Name).IsRequired().HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("RoundType");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.IsFinal).HasColumnName("IsFinal");
            this.Property(t => t.Name).HasColumnName("Name");
        }
    }
}