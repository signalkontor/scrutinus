// // TPS.net TPS8 DataModel
// // CompetitionMap.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System.Data.Entity.ModelConfiguration;

namespace DataModel.Models.Mapping
{
    public class CompetitionMap : EntityTypeConfiguration<Competition>
    {
        public CompetitionMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Title).HasMaxLength(200);

            // Relationships
            this.HasOptional(t => t.Class).WithMany(t => t.Competitions);

            this.HasRequired(t => t.Event).WithMany(t => t.Competitions);

            this.HasOptional(t => t.Section).WithMany(t => t.Competitions);

            this.HasMany(t => t.JudgingSheets);
        }
    }
}