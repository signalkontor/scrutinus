// // TPS.net TPS8 DataModel
// // CoupleMap.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System.Data.Entity.ModelConfiguration;

namespace DataModel.Models.Mapping
{
    public class CoupleMap : EntityTypeConfiguration<Couple>
    {
        public CoupleMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.FirstMan).HasMaxLength(100);

            this.Property(t => t.LastMan).HasMaxLength(100);

            this.Property(t => t.FirstWoman).HasMaxLength(100);

            this.Property(t => t.LastWoman).HasMaxLength(100);

            this.Property(t => t.MINMan).HasMaxLength(20);

            this.Property(t => t.MINWoman).HasMaxLength(20);

            this.Property(t => t.Country).HasMaxLength(250);

            this.Property(t => t.ExternalId).HasMaxLength(50);

            this.Property(t => t.Athlete_Id_Man).HasMaxLength(30);
            this.Property(t => t.Athlete_Id_Woman).HasMaxLength(30);
            this.Property(t => t.Team_Id).HasMaxLength(30);
            this.Property(t => t.Long_TV_Name_Man).HasMaxLength(50);
            this.Property(t => t.Long_TV_Name_Woman).HasMaxLength(50);
            this.Property(t => t.Short_TV_Name_Man).HasMaxLength(50);
            this.Property(t => t.Short_TV_Name_Woman).HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Couple");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.FirstMan).HasColumnName("FirstMan");
            this.Property(t => t.LastMan).HasColumnName("LastMan");
            this.Property(t => t.FirstWoman).HasColumnName("FirstWoman");
            this.Property(t => t.LastWoman).HasColumnName("LastWoman");
            this.Property(t => t.MINMan).HasColumnName("MINMan");
            this.Property(t => t.MINWoman).HasColumnName("MINWoman");
            
            this.Property(t => t.Country).HasColumnName("Country");
            this.Property(t => t.ExternalId).HasColumnName("ExternalId");
        }
    }
}