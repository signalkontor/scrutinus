﻿// // TPS.net TPS8 DataModel
// // DanceTemplateMap.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Data.Entity.ModelConfiguration;

namespace DataModel.Models.Mapping
{
    internal class DanceTemplateMap : EntityTypeConfiguration<DanceTemplate>
    {
        public DanceTemplateMap()
        {
            this.HasKey(t => t.Id);
            this.Property(p => p.Dances).HasMaxLength(255);
            this.ToTable("DanceTemplate");
        }
    }
}