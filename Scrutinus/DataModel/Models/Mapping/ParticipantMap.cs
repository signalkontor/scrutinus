// // TPS.net TPS8 DataModel
// // ParticipantMap.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System.Data.Entity.ModelConfiguration;

namespace DataModel.Models.Mapping
{
    public class ParticipantMap : EntityTypeConfiguration<Participant>
    {
        public ParticipantMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Relationships
            this.HasRequired(t => t.Competition).WithMany(t => t.Participants);

            this.HasRequired(t => t.Couple).WithMany(t => t.Participants);

        }
    }
}