// // TPS.net TPS8 DataModel
// // RoundMap.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System.Data.Entity.ModelConfiguration;

namespace DataModel.Models.Mapping
{
    public class RoundMap : EntityTypeConfiguration<Round>
    {
        public RoundMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Relationships
            this.HasRequired(t => t.Competition)
                .WithMany(t => t.Rounds)
                .WillCascadeOnDelete(false);

            this.HasMany(q => q.Qualifieds);

            this.HasMany(p => p.EjudgeData);

            this.HasRequired(t => t.RoundType).WithMany(t => t.Rounds);
        }
    }
}