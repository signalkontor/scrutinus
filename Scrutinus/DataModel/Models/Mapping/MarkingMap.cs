// // TPS.net TPS8 DataModel
// // MarkingMap.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System.Data.Entity.ModelConfiguration;

namespace DataModel.Models.Mapping
{
    public class MarkingMap : EntityTypeConfiguration<Marking>
    {
        public MarkingMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Relationships
            this.HasRequired(t => t.Participant).WithMany(t => t.Markings);
            this.HasRequired(t => t.Round).WithMany(t => t.Markings);
            this.HasRequired(t => t.Judge);
            this.HasOptional(t => t.Dance);
        }
    }
}