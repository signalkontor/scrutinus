// // TPS.net TPS8 DataModel
// // OfficialMap.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System.Data.Entity.ModelConfiguration;

namespace DataModel.Models.Mapping
{
    public class OfficialMap : EntityTypeConfiguration<Official>
    {
        public OfficialMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Sign).IsRequired().HasMaxLength(10);

            this.Property(t => t.Firstname).HasMaxLength(50);

            this.Property(t => t.Lastname).HasMaxLength(50);

            this.Property(t => t.Club).HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("Official");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Sign).HasColumnName("Sign");
            this.Property(t => t.Firstname).HasColumnName("Firstname");
            this.Property(t => t.Lastname).HasColumnName("Lastname");
            this.Property(t => t.Club).HasColumnName("Country");
            this.Property(t => t.MIN).HasColumnName("MIN");
        }
    }
}