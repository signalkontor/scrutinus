﻿// // TPS.net TPS8 DataModel
// // AttachedJudgingSheetMap.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Data.Entity.ModelConfiguration;

namespace DataModel.Models.Mapping
{
    public class AttachedJudgingSheetMap : EntityTypeConfiguration<AttachedJudgingSheet>
    {
        public AttachedJudgingSheetMap()
        {
            this.HasKey(i => i.Id);


        }
    }
}
