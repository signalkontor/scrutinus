﻿// // TPS.net TPS8 DataModel
// // DanceTemplate.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Linq;

namespace DataModel.Models
{
    public class DanceTemplate
    {
        public int Id { get; set; }

        public string Dances { get; set; }

        public virtual Section Section { get; set; }

        public virtual Class Class { get; set; }

        public virtual AgeGroup AgeGroup { get; set; }

        public IEnumerable<Dance> GetDances(ScrutinusContext context)
        {
            var list = new List<Dance>();
            var data = this.Dances.Split(',');
            foreach (var s in data)
            {
                int val;
                if (Int32.TryParse(s, out val))
                {
                    var dance = context.Dances.SingleOrDefault(d => d.Id == val);
                    if (dance != null)
                    {
                        list.Add(dance);
                    }
                }
            }
            // Set Dance Order based on 1:
            for (var i = 0; i < list.Count; i++)
            {
                list[i].DefaultOrder = i + 1;
            }

            return list;
        }
    }
}