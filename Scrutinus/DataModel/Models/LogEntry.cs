﻿// // TPS.net TPS8 DataModel
// // LogEntry.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;

namespace DataModel.Models
{
    public enum LogLevel
    {
        Error,
        Warning,
        Info,
        Debug,
        Verbos
    }

    public class LogEntry
    {
        public int Id { get; set; }

        public LogLevel LogLevel { get; set; }

        public DateTime TimeStamp { get; set; }

        public string Message { get; set; }

        public string StackTrace { get; set; }
    }
}
