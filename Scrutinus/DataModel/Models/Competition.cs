// // TPS.net TPS8 DataModel
// // Competition.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using PropertyChanged;

namespace DataModel.Models
{
    [AddINotifyPropertyChangedInterface]
    public class Competition : INotifyPropertyChanged
    {
        #region Constructors and Destructors

        public Competition()
        {
            this.Participants = new List<Participant>();
            this.Dances = new List<DancesInCompetition>();
            this.Officials = new List<OfficialInCompetition>();
            this.Rounds = new List<Round>();
            this.ClimbUps = new List<ClimbUp>();
            this.JudgingSheets = new List<JudgingSheetData>();
            this.LateEntriesAllowed = true;
            this.Canceled = false;
            this.FirstStartNumber = 1;
            this.numberParticipantsDancing = -1;
        }

        #endregion

        #region Public Events

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Methods

        private void RaiseEvent(string property)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        #endregion

        #region Fields

        private string title;

        private int? numberParticipantsDancing;

        #endregion

        #region Public Properties

        public int Id { get; set; }

        public virtual AgeGroup AgeGroup { get; set; }

        public virtual AgeGroup CombinedAgeGroup { get; set; }

        public virtual Class Class { get; set; }

        public virtual Class CombinedClass { get; set; }

        public virtual ICollection<ClimbUp> ClimbUps { get; set; }

        public virtual CompetitionType CompetitionType { get; set; }

        public virtual Round CurrentRound { get; set; }

        public virtual ICollection<DancesInCompetition> Dances { get; set; }

        public int DefaultCouplesPerHeat { get; set; }

        public DateTime? EndTime { get; set; }

        public virtual Event Event { get; set; }

        public string ExternalId { get; set; }

        public string CombinedExternalId { get; set; }

        public MarkingTypes FinalType { get; set; }

        public MarkingTypes MarkingType { get; set; }

        public int FirstRoundType { get; set; }

        public string Floor { get; set; }

        public DateTime? LastDtvDownload { get; set; }

        public DateTime? LastDtvUpload { get; set; }

        public bool HasRankingPoints { get; set; }

        public bool DtvResultUploaded { get; set; }

        public bool LateEntriesAllowed { get; set; }

        public bool ResultSend { get; set; }

        public bool Canceled { get; set; }

        public int? PlaceOffset { get; set; }

        public virtual Competition ParentCompetition { get; set; }

        public string WinnerCertificateTemplate { get; set; }

        public virtual ICollection<OfficialInCompetition> Officials { get; set; }

        public virtual ICollection<Participant> Participants { get; set; }

        public virtual ICollection<JudgingSheetData> JudgingSheets { get; set; }

        public int PlanedNumberOfRounds { get; set; }

        public int Visitors { get; set; }

        [MaxLength]
        public string DtvJson { get; set; }

        [MaxLength(40)]
        public string DtvChecksum { get; set; }

        [MaxLength]
        public string Report { get; set; }

        public virtual ICollection<Round> Rounds { get; set; }

        public virtual Section Section { get; set; }

        public int Stars { get; set; }

        public DateTime? StartTime { get; set; }

        public DateTime? ActualStartTime{ get; set; }

        public CompetitionState State { get; set; }

        public int FirstStartNumber { get; set; }

        [NotMapped]
        public int? MinStarters { get; set; }

        [NotMapped]
        public int? MaxStarters { get; set; }

        [NotMapped]
        public int? MaxForeingnStarters { get; set; }

        public string Title
        {
            get
            {
                if (!String.IsNullOrWhiteSpace(this.title))
                {
                    return this.title;
                }

                var ret = this.CompetitionType != null ? this.CompetitionType.TypeString : "";
                ret += " " + (this.AgeGroup != null ? this.AgeGroup.AgeGroupName : "");
                ret += this.Class != null && this.Class.ClassShortName != "-" ? " " + this.Class.ClassShortName : "";
                ret += " " + (this.Section != null ? this.Section.SectionName : "");

                return ret;
            }
            set
            {
                this.title = value;
            }
        }

        [NotMapped]
        public string ShortName => this.Title + " / " + this.AgeGroup?.ShortName + " " + this.Section?.ShortName;

        [NotMapped]
        public string ShortNameWithDate => $"{ShortName} ({this.StartTime?.ToString("dd.MM HH:mm")})";

        public string WGFolderCode { get; set; }

        [NotMapped]
        public bool IsCompetitionNotStarted
        {
            get
            {
                return this.CurrentRound == null;
            }
        }

        [NotMapped]
        public string StartGroup
        {
            get
            {
                if (this.CombinedAgeGroup != null)
                {
                    return $"{this.AgeGroup.ShortName} / {this.CombinedAgeGroup.ShortName} {this.Class.ClassShortName}";
                }

                if (this.CombinedClass != null)
                {
                    return
                        $"{this.AgeGroup.ShortName} {this.Class.ClassShortName} / {this.CombinedClass.ClassShortName}";
                }

                if (this.AgeGroup != null && this.Class != null)
                {
                    return this.AgeGroup.ShortName + " " + this.Class.ClassShortName;
                }

                return "";
            }
        }

        [NotMapped]
        [AlsoNotifyFor(nameof(Statistic))]
        public int? NumberParticipantsDancing
        {
            get
            {
                if (this.numberParticipantsDancing < 0)
                {
                    this.numberParticipantsDancing = this.Participants?.Count(p => p.State == CoupleState.Dancing) ?? 0;
                }
                return this.numberParticipantsDancing;
            }
            set
            {
                this.numberParticipantsDancing = value;
                this.RaiseEvent(nameof(this.NumberParticipantsDancing));
                this.RaiseEvent(nameof(this.Statistic));
            }
        }

        [NotMapped]
        [AlsoNotifyFor(nameof(Statistic))]
        public int? NumberCouplesRegistered { get; set; }

        [NotMapped]
        public string Statistic
        {
            get
            {
                return $"{this.NumberCouplesRegistered ?? this.Participants.Count} / {this.NumberParticipantsDancing ?? this.Participants.Count(p => p.State == CoupleState.Dancing)}";
            }
        }

        [NotMapped]
        public string TitleAndRound
        {
            get
            {
                if (this.CurrentRound != null)
                {
                    return $"{this.Title} / {this.CurrentRound.Name}";
                }
                else
                {
                    return this.Title;
                }
            }
        }

        #endregion

        #region Public Methods and Operators

        public override bool Equals(object obj)
        {
            var comp = obj as Competition;

            return comp?.Id == this.Id;
        }

        public override int GetHashCode()
        {
            // ReSharper disable once NonReadonlyMemberInGetHashCode
            return this.Id.GetHashCode();
        }

        public List<Official> GetJudges()
        {
            return this.Officials.Where(o => o.Role != null && o.Role.Id == Roles.Judge).Select(o => o.Official).OrderBy(j => j.Sign).ToList();
        }

        public void RaiseDancesChanges()
        {
            this.RaiseEvent("Dances");
        }

        #endregion
    }
}