﻿// // TPS.net TPS8 DataModel
// // CountryCode.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH
namespace DataModel.Models
{
    public class CountryCode
    {
        public int Id { get; set; }

        public string LongName { get; set; }

        public string IocCode { get; set; }
    }
}