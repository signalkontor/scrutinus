﻿// // TPS.net TPS8 DataModel
// // Device.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.ComponentModel.DataAnnotations.Schema;
using DataModel.Localization;
using mobileControl.Model;
using PropertyChanged;
using Scrutinus.Localization;

namespace DataModel.Models
{
    [AddINotifyPropertyChangedInterface]
    public class Device
    {
        public int Id { get; set; }

        public string DeviceId { get; set; }

        public DeviceStates DeviceState { get; set; }

        public int CompetitionState { get; set; }

        public string CompetitionId { get; set; }

        [NotMapped]
        public Round Round { get; set; }

        public int BatteryLevel { get; set; }

        public bool IsCharging { get; set; }

        public bool IsOnline { get; set; }

        public string CompetitionLoaded { get; set; }

        public DateTime TimeStamp { get; set; }

        public int Heat { get; set; }

        [NotMapped]
        public Dance Dance { get; set; }

        public int Marks { get; set; }

        [NotMapped]
        public string DeviceStateString
        {
            get
            {
                if (this.DeviceState == DeviceStates.MarkingQualification
                    || this.DeviceState == DeviceStates.MarkingFinal)
                {
                    return string.Format(
                        LocalizationService.Resolve(() => Text.DeviceStateString),
                        this.Dance != null ? this.Dance.DanceName : " - ",
                        this.Heat,
                        this.Marks);
                }
                else
                {
                    return LocalizationService.Resolve("DataModel:Text:DeviceStates_" + this.DeviceState.ToString());
                } 
            }
        }
    }
}
