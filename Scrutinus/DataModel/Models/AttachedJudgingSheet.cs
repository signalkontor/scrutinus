﻿// // TPS.net TPS8 DataModel
// // AttachedJudgingSheet.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;

namespace DataModel.Models
{
    public class AttachedJudgingSheet
    {
        public int Id { get; set; }

        public string File { get; set; }

        public string Remark { get; set; }

        public bool HasBenSend { get; set; }

        public DateTime? SendDate { get; set; }
    }
}
