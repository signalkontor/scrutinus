﻿// // TPS.net TPS8 DataModel
// // ClimbUpTable.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

namespace DataModel.Models
{
    public class ClimbUpTable
    {
        public int Id { get; set; }

        public int MinPoints { get; set; }

        public virtual Section Section { get; set; }

        public virtual AgeGroup AgeGroup { get; set; }

        public virtual Class Class { get; set; }

        public virtual Class TargetClass { get; set; }

        public string LTV { get; set; }

        public int Points { get; set; }

        public int Placings { get; set; }

        public int MinimumPlace { get; set; }

        public bool TargetInDifferentAgeGroup { get; set; }
    }
}
