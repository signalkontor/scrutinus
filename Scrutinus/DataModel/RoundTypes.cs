﻿// // TPS.net TPS8 DataModel
// // RoundTypes.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH
namespace DataModel
{
    public static class RoundTypes
    {
        public const int QualificationRound = 1;

        public const int Redance = 2;

        public const int SecondFirstRound = 3;

        public const int Final = 4;

        public const int ABFinal = 5;

        public const int ABCFinal = 6;

        public const int SeparationRound = 7;
    }
}