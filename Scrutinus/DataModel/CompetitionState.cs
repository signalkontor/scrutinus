﻿// // TPS.net TPS8 DataModel
// // CompetitionState.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH
namespace DataModel
{
    public enum CompetitionState
    {
        Startlist = 0,

        RoundDrawing = 1,

        RoundDrawingResult = 2,

        Printing = 3,

        EnterMarking = 4,

        SelectQualified = 5,

        QualifiedList = 6,

        Finished = 7,

        CheckIn = 8,

        Closed = 9,

        Splitted = 10,
    }
}