﻿// // TPS.net TPS8 DataModel
// // HeatDrawing.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using DataModel.Models;

namespace DataModel.ReportModels
{
    public class Heat
    {
        public string Dance { get; set; }

        public int Order { get; set; }

        public int HeatNo { get; set; }

        public string NumbersAsString
        {
            get
            {
                return string.Join("     ", this.Participants.Select(i => i.Number.ToString()).ToArray());
            }
        }

        public List<Participant> Participants { get; set; }

        // This is not realy nice but report bindung to a variable list seams not working
        public string P1
        {
            get
            {
                return this.Participants.Count > 0 ? this.Participants[0].Number.ToString() : "";
            }
        }

        public string P2
        {
            get
            {
                return this.Participants.Count > 1 ? this.Participants[1].Number.ToString() : "";
            }
        }

        public string P3
        {
            get
            {
                return this.Participants.Count > 2 ? this.Participants[2].Number.ToString() : "";
            }
        }

        public string P4
        {
            get
            {
                return this.Participants.Count > 3 ? this.Participants[3].Number.ToString() : "";
            }
        }

        public string P5
        {
            get
            {
                return this.Participants.Count > 4 ? this.Participants[4].Number.ToString() : "";
            }
        }

        public string P6
        {
            get
            {
                return this.Participants.Count > 5 ? this.Participants[5].Number.ToString() : "";
            }
        }

        public string P7
        {
            get
            {
                return this.Participants.Count > 6 ? this.Participants[6].Number.ToString() : "";
            }
        }

        public string P8
        {
            get
            {
                return this.Participants.Count > 7 ? this.Participants[7].Number.ToString() : "";
            }
        }

        public string P9
        {
            get
            {
                return this.Participants.Count > 8 ? this.Participants[8].Number.ToString() : "";
            }
        }

        public string P10
        {
            get
            {
                return this.Participants.Count > 9 ? this.Participants[9].Number.ToString() : "";
            }
        }

        public string P11
        {
            get
            {
                return this.Participants.Count > 10 ? this.Participants[10].Number.ToString() : "";
            }
        }

        public string P12
        {
            get
            {
                return this.Participants.Count > 11 ? this.Participants[11].Number.ToString() : "";
            }
        }

        public string P13
        {
            get
            {
                return this.Participants.Count > 12 ? this.Participants[12].Number.ToString() : "";
            }
        }

        public string P14
        {
            get
            {
                return this.Participants.Count > 13 ? this.Participants[13].Number.ToString() : "";
            }
        }

        public string P15
        {
            get
            {
                return this.Participants.Count > 14 ? this.Participants[14].Number.ToString() : "";
            }
        }

        public string P16
        {
            get
            {
                return this.Participants.Count > 15 ? this.Participants[15].Number.ToString() : "";
            }
        }

        public string P17
        {
            get
            {
                return this.Participants.Count > 16 ? this.Participants[16].Number.ToString() : "";
            }
        }

        public string P18
        {
            get
            {
                return this.Participants.Count > 17 ? this.Participants[17].Number.ToString() : "";
            }
        }

        public string P19
        {
            get
            {
                return this.Participants.Count > 18 ? this.Participants[18].Number.ToString() : "";
            }
        }

        public string P20
        {
            get
            {
                return this.Participants.Count > 19 ? this.Participants[19].Number.ToString() : "";
            }
        }

        public static List<Heat> GenerateSampleData(ScrutinusContext data)
        {
            var res = new List<Heat>();

            AddCouples(res, "Samba", data, 1);
            AddCouples(res, "Cha Cha", data, 2);
            AddCouples(res, "Jive", data, 3);

            return res;
        }

        private static void AddCouples(List<Heat> result, string dance, ScrutinusContext data, int order)
        {
            Heat heat = null;
            var heatNo = 0;

            for (var i = 0; i < 30; i++)
            {
                if (i % 14 == 0)
                {
                    heatNo++;
                    heat = new Heat()
                               {
                                   Dance = dance,
                                   HeatNo = heatNo,
                                   Participants = new List<Participant>(),
                                   Order = order
                               };

                    result.Add(heat);
                }

                var part = new Participant() { Couple = data.Couples.First(), Number = i };
                heat.Participants.Add(part);
            }
        }
    }
}