﻿// // TPS.net TPS8 DataModel
// // PrintModelV2.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;

namespace DataModel.ReportModels
{
    public class MarkingV2PrintModel
    {
        public int Couple { get; set; }

        public string JudgeArea1_1 { get; set; }

        public string JudgeArea1_2 { get; set; }

        public string JudgeArea1_3 { get; set; }

        public string JudgeArea2_1 { get; set; }

        public string JudgeArea2_2 { get; set; }

        public string JudgeArea2_3 { get; set; }

        public string JudgeArea3_1 { get; set; }

        public string JudgeArea3_2 { get; set; }

        public string JudgeArea3_3 { get; set; }

        public string JudgeArea4_1 { get; set; }

        public string JudgeArea4_2 { get; set; }

        public string JudgeArea4_3 { get; set; }

        public double Mark1_1 { get; set; }

        public double Mark1_2 { get; set; }

        public double Mark1_3 { get; set; }

        public double Mark2_1 { get; set; }

        public double Mark2_2 { get; set; }

        public double Mark2_3 { get; set; }

        public double Mark3_1 { get; set; }

        public double Mark3_2 { get; set; }

        public double Mark3_3 { get; set; }

        public double Mark4_1 { get; set; }

        public double Mark4_2 { get; set; }

        public double Mark4_3 { get; set; }

        public double Total { get; set; }

        public string Place { get; set; }
    }

    public class MarkingV2Model
    {
        public Dictionary<string, List<MarkingV2PrintModel>> Markings { get; set; }
    }
}