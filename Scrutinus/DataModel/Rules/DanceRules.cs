﻿// // TPS.net TPS8 DataModel
// // DanceRules.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Linq;
using DataModel.Models;

namespace DataModel.Rules
{
    public class DanceRules
    {
        private readonly ScrutinusContext context;

        public DanceRules(ScrutinusContext context)
        {
            this.context = context;
        }

        public IEnumerable<Dance> GetDances(int sectionId, int agegroupId, int classId)
        {
            var template = this.GetRule(sectionId, agegroupId, classId);
            if (template == null)
            {
                throw new Exception("No dances configured for Section!");
            }

            return template.GetDances(this.context);
        }

        private DanceTemplate GetRule(int sectionId, int agegroupid, int classId)
        {
            // Try To find from most specific to lowest specific:
            var template =
                this.context.DanceTemplates.SingleOrDefault(
                    t => t.Section.Id == sectionId && t.Class.Id == classId && t.AgeGroup.Id == agegroupid);
            if (template != null)
            {
                return template;
            }

            template =
                this.context.DanceTemplates.SingleOrDefault(
                    t => t.Section.Id == sectionId && t.Class != null && t.Class.Id == classId && t.AgeGroup == null);
            if (template != null)
            {
                return template;
            }

            template =
                this.context.DanceTemplates.SingleOrDefault(
                    t => t.Section.Id == sectionId && t.AgeGroup != null && t.AgeGroup.Id == agegroupid && t.Class == null);
            if (template != null)
            {
                return template;
            }

            template =
                this.context.DanceTemplates.SingleOrDefault(
                    t => t.Section.Id == sectionId && t.Class == null && t.AgeGroup == null);

            return template; // Could still be null, this would be an exception
        }
    }
}