﻿// // TPS.net TPS8 DataModel
// // ExportWDSG.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using DataModel.Models;

namespace DataModel.Export
{
    public class ExportWDSG
    {
        public static void ExportWDSGFile(ScrutinusContext context, int roundId, string baseFolder)
        {
            var round = context.Rounds.Single(r => r.Id == roundId);
            var path = "";

            Round previousRound = null;
            if (round.Number > 1)
            {
                previousRound =
                    context.Rounds.SingleOrDefault(
                        r => r.Competition.Id == round.Competition.Id && r.Number == round.Number - 1);
            }

            if (round.RoundType.Id == RoundTypes.Final)
            {
                if (previousRound != null && previousRound.RoundType.Id == RoundTypes.ABFinal)
                {
                    path = String.Format("{0}\\{1}\\Final A", baseFolder, round.Competition.WGFolderCode);
                }
                else
                {
                    path = String.Format("{0}\\{1}\\Final", baseFolder, round.Competition.WGFolderCode);
                }
            }
            else if (round.RoundType.Id == RoundTypes.ABFinal)
            {
                path = String.Format("{0}\\{1}\\Final B", baseFolder, round.Competition.WGFolderCode);
            }
            else
            {
                path = String.Format("{0}\\{1}\\Round {2}", baseFolder, round.Competition.WGFolderCode, round.Number);
            }

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            // Write Result of dances:
            foreach (var dance in round.DancesInRound)
            {
                var dancePath = path + "\\" + dance.Dance.DanceName;
                if (!Directory.Exists(dancePath))
                {
                    Directory.CreateDirectory(dancePath);
                }

                dancePath += "\\data.csv";

                switch (round.MarksInputType)
                {
                    case MarkingTypes.NewJudgingSystemV1:
                        ExportJudgingSystem1(dancePath, round, dance.Dance.Id, context);
                        break;
                    case MarkingTypes.NewJudgingSystemV2:
                        ExportJudgingSystem2(dancePath, round, dance.Dance.Id, context);
                        break;
                    case MarkingTypes.Marking:
                        ExportQualificationRound(dancePath, round, context);
                        break;
                    case MarkingTypes.Skating:
                        ExportFinal(dancePath, round, context);
                        break;
                }

                ExportCurrent(path, round, context);
                ExportOverall(String.Format("{0}", baseFolder), round, context);
            }
        }

        private static void ExportFinal(string dancePath, Round round, ScrutinusContext context)
        {
            var outStr = new StreamWriter(dancePath, false, Encoding.UTF8);
            outStr.WriteLine("Start number,Rank,Name,Country,Score");
            foreach (var qualified in round.Qualifieds.OrderBy(p => p.Participant.Number))
            {
                var country =
                    context.CountryCodes.SingleOrDefault(c => c.LongName == qualified.Participant.Couple.Country);

                outStr.WriteLine(
                    "{0},{1},{2},{3},{4}",
                    qualified.Participant.Number,
                    qualified.PlaceFrom,
                    getName(qualified.Participant),
                    country != null ? country.IocCode : qualified.Participant.Couple.Country,
                    qualified.Points);
            }
            outStr.Close();
        }

        private static void ExportQualificationRound(
            string dancePath,
            Round round,
            ScrutinusContext context)
        {
            var outStr = new StreamWriter(dancePath, false, Encoding.UTF8);
            outStr.WriteLine("Start number,Rank,Name,Country,Score");
            foreach (var qualified in round.Qualifieds.OrderBy(p => p.Participant.Number))
            {
                var country =
                    context.CountryCodes.SingleOrDefault(c => c.LongName == qualified.Participant.Couple.Country);

                outStr.WriteLine(
                    "{0},{1},{2},{3},{4}",
                    qualified.Participant.Number,
                    qualified.PlaceFrom,
                    getName(qualified.Participant),
                    country != null ? country.IocCode : qualified.Participant.Couple.Country,
                    qualified.Points);
            }
            outStr.Close();
        }

        private static void ExportJudgingSystem2(
            string dancePath,
            Round round,
            int DanceId,
            ScrutinusContext context)
        {
            var outStr = new StreamWriter(dancePath, false, Encoding.UTF8);
            outStr.WriteLine("Start number,Rank,Name,Country,Score,TQ,MM,PS,CP");

            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

            var components = round.Competition.CompetitionType.JudgingComponents.OrderBy(o => o.Order).ToList();

            foreach (var qualified in round.Qualifieds.OrderByDescending(p => p.Points))
            {
                var sumAll = 0d;

                var CurrentRoundId = round.Id;

                var marks =
                    context.Markings.Where(
                        m =>
                        m.Round.Id == CurrentRoundId && m.MarkingComponent == 1
                        && m.Participant.Id == qualified.Participant.Id && m.Dance.Id == DanceId);

                var area1 = (marks.Sum(s => s.Mark20) * 2 - marks.Min(s => s.Mark20) - marks.Max(s => s.Mark20)) / 4;
                marks =
                    context.Markings.Where(
                        m =>
                        m.Round.Id == CurrentRoundId && m.MarkingComponent == 2
                        && m.Participant.Id == qualified.Participant.Id && m.Dance.Id == DanceId);
                var area2 = (marks.Sum(s => s.Mark20) * 2 - marks.Min(s => s.Mark20) - marks.Max(s => s.Mark20)) / 4;
                marks =
                    context.Markings.Where(
                        m =>
                        m.Round.Id == CurrentRoundId && m.MarkingComponent == 3
                        && m.Participant.Id == qualified.Participant.Id && m.Dance.Id == DanceId);
                var area3 = (marks.Sum(s => s.Mark20) * 2 - marks.Min(s => s.Mark20) - marks.Max(s => s.Mark20)) / 4;
                marks =
                    context.Markings.Where(
                        m =>
                        m.Round.Id == CurrentRoundId && m.MarkingComponent == 4
                        && m.Participant.Id == qualified.Participant.Id && m.Dance.Id == DanceId);
                var area4 = (marks.Sum(s => s.Mark20) * 2 - marks.Min(s => s.Mark20) - marks.Max(s => s.Mark20)) / 4;

                area1 *= components.Count > 0 ? components[0].Weight : 1d;
                area2 *= components.Count > 1 ? components[1].Weight : 1d;
                area3 *= components.Count > 2 ? components[2].Weight : 1d;
                area4 *= components.Count > 3 ? components[3].Weight : 1d;

                sumAll += area1 + area2 + area3 + area4;

                var country =
                    context.CountryCodes.SingleOrDefault(c => c.LongName == qualified.Participant.Couple.Country);

                outStr.WriteLine(
                    "{0},{1},{2},{3},{4:0.000},{5:0.000},{6:0.000},{7:0.000},{8:0.000}",
                    qualified.Participant.Number,
                    qualified.Participant.PlaceFrom ?? 0,
                    getName(qualified.Participant),
                    country != null ? country.IocCode : qualified.Participant.Couple.Country,
                    sumAll,
                    area1,
                    area2,
                    area3,
                    area4);
            }
            outStr.Close();
        }

        private static void ExportJudgingSystem1(
            string dancePath,
            Round round,
            int danceId,
            ScrutinusContext context)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            var outStr = new StreamWriter(dancePath, false, Encoding.UTF8);
            outStr.Write("Start number,Rank,Name,Country,Score");
            // Write the judging components:
            var components = round.Competition.CompetitionType.JudgingComponents.OrderBy(o => o.Order).ToList();
            foreach (var judgingComponent in components)
            {
                outStr.Write(",{0}", judgingComponent.Code);
            }
            outStr.WriteLine("");

            foreach (var qualified in round.Qualifieds.OrderBy(p => p.Participant.Number))
            {
                var sumAll = 0d;

                var CurrentRoundId = round.Id;
                var area1 = 0.0;
                var area2 = 0.0;
                var area3 = 0.0;
                var area4 = 0.0;

                var qualified1 = qualified;
                var marks =
                    context.Markings.Where(
                        m =>
                        m.Round.Id == CurrentRoundId && m.Participant.Id == qualified1.Participant.Id
                        && m.Dance.Id == danceId);
                if (marks.Any())
                {
                    area1 = (marks.Sum(m => m.MarkA) - marks.Max(m => m.MarkA) - marks.Min(m => m.MarkA))
                            / (marks.Count() - 2);
                    area2 = (marks.Sum(m => m.MarkB) - marks.Max(m => m.MarkB) - marks.Min(m => m.MarkB))
                            / (marks.Count() - 2);
                    area3 = (marks.Sum(m => m.MarkC) - marks.Max(m => m.MarkC) - marks.Min(m => m.MarkC))
                            / (marks.Count() - 2);
                    area4 = (marks.Sum(m => m.MarkD) - marks.Max(m => m.MarkD) - marks.Min(m => m.MarkD))
                            / (marks.Count() - 2);

                    area1 *= components.Count > 0 ? components[0].Weight : 1d;
                    area2 *= components.Count > 1 ? components[1].Weight : 1d;
                    area3 *= components.Count > 2 ? components[2].Weight : 1d;
                    area4 *= components.Count > 3 ? components[3].Weight : 1d;

                    sumAll += area1 + area2 + area3 + area4;
                }

                var country =
                    context.CountryCodes.SingleOrDefault(c => c.LongName == qualified.Participant.Couple.Country);

                outStr.WriteLine(
                    "{0},{1},{2},{3},{4:0.000},{5:0.000},{6:0.000},{7:0.000},{8:0.000}",
                    qualified.Participant.Number,
                    qualified.Participant.PlaceFrom ?? 0,
                    getName(qualified.Participant),
                    country != null ? country.IocCode : qualified.Participant.Couple.Country,
                    sumAll,
                    area1,
                    area2,
                    area3,
                    area4);
            }
            outStr.Close();
        }

        public static void ExportOverall(string path, Round round, ScrutinusContext context)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            var targetPath = String.Format("{0}\\{1}\\Overall", path, round.Competition.WGFolderCode);
            if (!Directory.Exists(targetPath))
            {
                // Todo: Exchange messagebox
                MessageBox.Show("Please configure WDSG Export Directory");
                return;
            }
            var outStr = new StreamWriter(targetPath + "\\data.csv", false, Encoding.UTF8);
            outStr.WriteLine("Start number,Rank,Name,Country,Score");

            var participants = context.Participants.Where(p => p.Competition.Id == round.Competition.Id);

            foreach (var participant in participants.OrderByDescending(o => o.Points).ThenBy(o => o.Number))
            {
                var country = context.CountryCodes.SingleOrDefault(c => c.LongName == participant.Couple.Country);
                outStr.WriteLine(
                    "{0},{1},{2},{3},{4:0.000}",
                    participant.Number,
                    participant.PlaceFrom.HasValue ? participant.PlaceFrom.Value : 0,
                    getName(participant),
                    country != null ? country.IocCode : participant.Couple.Country,
                    participant.Points);
            }
            outStr.Close();
        }

        private static void ExportCurrent(string path, Round round, ScrutinusContext context)
        {
        }

        private static string getName(Participant participant)
        {
            if (!participant.Couple.IsTeam)
            {
                return String.Format(
                    "{0}_{1}/{2}_{3}",
                    participant.Couple.FirstMan,
                    participant.Couple.LastMan,
                    participant.Couple.FirstWoman,
                    participant.Couple.LastWoman);
            }
            else
            {
                if (String.IsNullOrEmpty(participant.Couple.LastMan))
                {
                    return participant.Couple.FirstMan;
                }
                else
                {
                    return participant.Couple.FirstMan + "_" + participant.Couple.LastMan;
                }
            }
        }
    }
}