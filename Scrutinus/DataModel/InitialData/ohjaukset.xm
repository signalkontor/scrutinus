<?xml version="1.0" encoding="utf-8" ?>
<Content RuleName="FDSF National">
  <Sections>
    <Section Id="1" SectionName="Vakio"></Section>
    <Section Id="2" SectionName="Latin"/>
    <Section Id="3" SectionName="10 tanssi" />
    <Section Id="4" SectionName="Caribian" />
    <Section Id="5" SectionName="Hip Hop" />
    <Section Id="6" SectionName="Formation Latin" />
    <Section Id="7" SectionName="Formation Standard" />
    <Section Id="8" SectionName="Showdance Latin" />
    <Section Id="9" SectionName="Showdance Standard" />
    <Section Id="10" SectionName="Taike" />
  </Sections>
  <Dances>
    <Dance Id="1" DanceName="Slow Waltz" ShortName="SW" DefaultOrder="1" />
    <Dance Id="2" DanceName="Tango" ShortName="TG" DefaultOrder="2" />
    <Dance Id="3" DanceName="Viennes Waltz" ShortName="VW" DefaultOrder="3" />
    <Dance Id="4" DanceName="Slowfox" ShortName="SF" DefaultOrder="4" />
    <Dance Id="5" DanceName="Quickstep" ShortName="QS" DefaultOrder="5" />
    <Dance Id="6" DanceName="Samba" ShortName="SB" DefaultOrder="6" />
    <Dance Id="7" DanceName="Cha Cha Cha" ShortName="CC" DefaultOrder="7" />
    <Dance Id="8" DanceName="Rumba" ShortName="RB" DefaultOrder="8" />
    <Dance Id="9" DanceName="Paso Doble" ShortName="PD" DefaultOrder="9" />
    <Dance Id="10" DanceName="Jive" ShortName="JV" DefaultOrder="10" />
    <Dance Id="11" DanceName="Discofox" ShortName="DF" DefaultOrder="4" />
    <Dance Id="12" DanceName="Salsa" ShortName="SA" DefaultOrder="1" />
    <Dance Id="13" DanceName="Bachata" ShortName="BA" DefaultOrder="2" />
    <Dance Id="14" DanceName="Merengue" ShortName="ME" DefaultOrder="3" />
    <Dance Id="15" DanceName="Showdance Latin" ShortName="SL" DefaultOrder="1" />
    <Dance Id="16" DanceName="Showdance Standard" ShortName="SS" DefaultOrder="1" />
    <Dance Id="17" DanceName="Formation Latin" ShortName="FL" DefaultOrder="1" />
    <Dance Id="18" DanceName="Formation Standard" ShortName="FS" DefaultOrder="1" />
    <Dance Id="19" DanceName="Hip Hop" ShortName="HH" DefaultOrder="1" />
    <Dance Id="20" DanceName="Caribbean Show" ShortName="SSH" DefaultOrder="4" />
    <Dance Id="21" DanceName="Taike" ShortName="TA" DefaultOrder="1" />
  </Dances>
  <Roles>
    <Role Id="1" RoleLong="Tuomari" RoleShort="Adj" />
    <Role Id="2" RoleLong="Kilpailunjohtaja" RoleShort="CM" />
    <Role Id="3" RoleLong="Tuloslaskenta" RoleShort="Scr" />
    <Role Id="4" RoleLong="Associate" RoleShort="Ass" />
    <Role Id="5" RoleLong="Supervisor" RoleShort="Sup" />
    <Role Id="6" RoleLong="Technical Delegate" RoleShort="TG" />
    <Role Id="7" RoleLong="Jury" RoleShort="Jury" />
  </Roles>
  <RoundTypes>
    <RoundType Id="1" IsFinal="False" HasDrawing="True" Name="Karsintakierros" AllowedMarkingTypes="1,2,3,5,6"/>
    <RoundType Id="2" IsFinal="False" HasDrawing="True" Name="Redance" AllowedMarkingTypes="1,3,6"/>
    <RoundType Id="3" IsFinal="False" HasDrawing="True" Name="Two Added Rounds" AllowedMarkingTypes="1"/>
    <RoundType Id="4" IsFinal="True" HasDrawing="False" Name="Finaali" AllowedMarkingTypes="2,3,4,5"/>
    <RoundType Id="5" IsFinal="True" HasDrawing="False" Name="A/B Final" AllowedMarkingTypes="2,3,4"/>
    <RoundType Id="6" IsFinal="True" HasDrawing="False" Name="A/B/C Final" AllowedMarkingTypes="2,3,4"/>
  </RoundTypes>
  <DrawingTypes>
    <DrawingType Id="1" DrawingName="Arvottu" />
    <DrawingType Id="2" DrawingName="Kiinte�t er�t laskeva j�rjestys" />
    <DrawingType Id="3" DrawingName="Kiinte�t er�t nouseva j�rjestys" />
    <DrawingType Id="4" DrawingName="Arvottu, kiinte�t er�t" />
    <DrawingType Id="5" DrawingName="Grand Slam Final" />
  </DrawingTypes>
  <AgeGroups>
    <AgeGroup Id="1" AgeGroupName="Lapsi 1" ShortName="La1" OrderWhenCombined="5" Classes="1,2,3,4,5,6,7,8"/>
    <AgeGroup Id="2" AgeGroupName="Lapsi 2" ShortName="La2" OrderWhenCombined="4" Classes="1,2,3,4,5,6,7,8"/>
    <AgeGroup Id="3" AgeGroupName="Juniori 1" ShortName="Jun 1" OrderWhenCombined="3" Classes="1,2,3,4,5,6,7,8"/>
    <AgeGroup Id="4" AgeGroupName="Juniori 2" ShortName="Jun 2" OrderWhenCombined="2" Classes="1,2,3,4,5,6,7,8"/>
    <AgeGroup Id="5" AgeGroupName="Nuoriso" ShortName="Nuor" OrderWhenCombined="1" Classes="1,2,3,4,5,6,7,8"/>
    <AgeGroup Id="6" AgeGroupName="Yleinen" ShortName="Yl" OrderWhenCombined="0" Classes="1,2,3,4,5,6,7,8"/>
    <AgeGroup Id="7" AgeGroupName="Champion" ShortName="Ch" OrderWhenCombined="0" Classes="1,2,3,4,5,6,7,8"/>
    <AgeGroup Id="8" AgeGroupName="Seniori 1" ShortName="Sen 1" OrderWhenCombined="1" Classes="1,2,3,4,5,6,7,8"/>
    <AgeGroup Id="9" AgeGroupName="Seniori 2" ShortName="Sen 2" OrderWhenCombined="2" Classes="1,2,3,4,5,6,7,8"/>
    <AgeGroup Id="10" AgeGroupName="Seniori 3" ShortName="Sen 3" OrderWhenCombined="3" Classes="1,2,3,4,5,6,7,8"/>
    <AgeGroup Id="11" AgeGroupName="Seniori 4" ShortName="Sen 4" OrderWhenCombined="4" Classes="1,2,3,4,5,6,7,8"/>
  </AgeGroups>
  <Classes>
    <Class Id="1" ClassLongName="E Luokka" ClassShortName="E" OrderWhenCombined="1" />
    <Class Id="2" ClassLongName="D Luokka" ClassShortName="D" OrderWhenCombined="1"/>
    <Class Id="3" ClassLongName="C Luokka" ClassShortName="C" OrderWhenCombined="2"/>
    <Class Id="4" ClassLongName="B Luokka" ClassShortName="B" OrderWhenCombined="2"/>
    <Class Id="5" ClassLongName="A-luokka" ClassShortName="A" OrderWhenCombined="3"/>
    <Class Id="6" ClassLongName="C-A luokka" ClassShortName="C-A" OrderWhenCombined="4"/>
    <Class Id="7" ClassLongName="Avoin luokka" ClassShortName="Av" OrderWhenCombined="5"/>
    <Class Id="8" ClassLongName="None" ClassShortName="-" />
  </Classes>
  <CompetitionRules>
    <CompetitionRule Id="1" Federation="FDSF" RuleName="STUL SAV-kilpailu" FirstRoundTypeId="1" MarkingType="1" ChairmanRequired="True" MinimumJudgeNumber="3" SupervisorRequired="False" AssosiateRequired="False" RedanceRequired="False"/>
    <CompetitionRule Id="2" Federation="FDSF" RuleName="STUL IKM-kilpailu" FirstRoundTypeId="1" MarkingType="1" ChairmanRequired="True" MinimumJudgeNumber="7"  SupervisorRequired="False" AssosiateRequired="False" RedanceRequired="False"/>
    <CompetitionRule Id="3" Federation="FDSF" RuleName="STUL GP-kilpailu" FirstRoundTypeId="1" MarkingType="1" ChairmanRequired="True" MinimumJudgeNumber="5"  SupervisorRequired="False" AssosiateRequired="False" RedanceRequired="False"/>
    <CompetitionRule Id="4" Federation="FDSF" RuleName="STUL SM-kilpailu" FirstRoundTypeId="1" MarkingType="1" ChairmanRequired="True" MinimumJudgeNumber="7" SupervisorRequired="False" AssosiateRequired="False" RedanceRequired="False"/>
    <CompetitionRule Id="5" Federation="WDSF" RuleName="WDSF Traditional" FirstRoundTypeId="1" MarkingType="1" ChairmanRequired="True" SupervisorRequired="False" AssosiateRequired="False" RedanceRequired="True"/>
    <CompetitionRule Id="6" Federation="WDSF" RuleName="WDSF JS2" FirstRoundTypeId="1" MarkingType="3" ChairmanRequired="True" SupervisorRequired="False" AssosiateRequired="False" RedanceRequired="True"/>
    <CompetitionRule Id="7" Federation="IDO"  RuleName="IDO Salsa" FirstRoundTypeId="1" MarkingType="1" ChairmanRequired="True" SupervisorRequired="False" AssosiateRequired="False" RedanceRequired="False"/>
  </CompetitionRules>
  <CompetitionTypes>
    <CompetitionType Id="1" Federation="WDSF" TypeString="WDSF Open" CompetitionKind="Single Competition" CompetitionRuleId="5"/>
    <CompetitionType Id="2" Federation="WDSF" TypeString="WDSF International Open" CompetitionKind="Single Competition" CompetitionRuleId="5"/>
    <CompetitionType Id="3" Federation="WDSF" TypeString="WDSF Grand Slam" CompetitionKind="Single Competition" CompetitionRuleId="6"/>
    <CompetitionType Id="4" Federation="IDO" TypeString="WDSG IDO" CompetitionKind="Single Competition" CompetitionRuleId="6"/>
    <CompetitionType Id="5" Federation="WDSF" TypeString="Formation" CompetitionKind="Formation" CompetitionRuleId="2"/>
    <CompetitionType Id="6" Federation="WDSF" TypeString="Showdance" CompetitionKind="Single Competition" CompetitionRuleId="2"/>
    <CompetitionType Id="7" Federation="WDSF" TypeString="Wheelchair" CompetitionKind="Single Competition" CompetitionRuleId="2"/>
    <CompetitionType Id="8" Federation="STUL" TypeString="SAV-kilpailu" CompetitionKind="Single Competition" CompetitionRuleId="1"/>
    <CompetitionType Id="9" Federation="STUL" TypeString="IKM-kilpailu" CompetitionKind="Single Competition" CompetitionRuleId="2"/>
    <CompetitionType Id="10" Federation="STUL" TypeString="GP-kilpailu" CompetitionKind="Single Competition" CompetitionRuleId="3"/>
    <CompetitionType Id="11" Federation="STUL" TypeString="SM-kilpailu" CompetitionKind="Single Competition" CompetitionRuleId="4"/>
  </CompetitionTypes>
  <JudgingComponents>
    <JudgingComponent Id="1" Code="TQ" Order="1" Weight="1" Description="* Posture and Poise\r\n* Body line, shape, design\r\n* Hold or Holds\r\n* Positions and transitions\r\n* Balance \r\n - Static- Dynamic\r\n - Individual- Couple\r\n* Coordination of movement\r\n* Actions: General, Style specific\r\n* Dynamics: Flow, Weight, Time, Space"></JudgingComponent>
    <JudgingComponent Id="2" Code="MM" Order="2" Weight="1" Description="* Time\r\n* Tempo\r\n* Rhytmical structure\r\n* Phrasing\r\n* Timing\r\n* Musicality"></JudgingComponent>
    <JudgingComponent Id="3" Code="PS" Order="3" Weight="1" Description="* Physical connection\r\n* Communication without physical connection\r\n* Appropriateness\r\n* Effectiveness\r\n* Consistency"></JudgingComponent>
    <JudgingComponent Id="4" Code="CP" Order="4" Weight="1" Description="* Well balanced choreography\r\n - Content\r\n - Space\r\n - Partnering\r\n - Level of difficulty\r\n* Atmosphere\r\n* Creativity\r\n* Expression\r\n* Interpretation"></JudgingComponent>
    <JudgingComponent Id="5" Code="CP" Order="4" Weight="1,5" Description="* Well balanced choreography\r\n - Content\r\n - Space\r\n - Partnering\r\n - Level of difficulty\r\n* Atmosphere\r\n* Creativity\r\n* Expression\r\n* Interpretation"></JudgingComponent>
    <JudgingComponent Id="6" Code="T" Order="1" Weight="1" Description="Hier kommt die Beschreibung rein f�r diese Komponente"></JudgingComponent>
    <JudgingComponent Id="7" Code="C" Order="2" Weight="1" Description="Hier kommt die Beschreibung rein f�r diese Komponente"></JudgingComponent>
    <JudgingComponent Id="8" Code="I" Order="3" Weight="1" Description="Hier kommt die Beschreibung rein f�r diese Komponente"></JudgingComponent>
    <JudgingComponent Id="9" Code="S" Order="4" Weight="3" Description="Show"></JudgingComponent>
    <JudgingComponent Id="10" Code="TQ" Order="1" Weight="1" Description="* TQ Formation"></JudgingComponent>
    <JudgingComponent Id="11" Code="MM" Order="2" Weight="1" Description="* MM Formation"></JudgingComponent>
    <JudgingComponent Id="12" Code="PS" Order="3" Weight="1" Description="* PS Formation"></JudgingComponent>
    <JudgingComponent Id="13" Code="CP" Order="4" Weight="1" Description="* CP Formation"></JudgingComponent>
  </JudgingComponents>
  <JudgingComponentsCompetitionTypes>
    <!-- Grand Slam -->
    <JudgingComponentsCompetitionType JudgingComponent="1" CompetitionType="3"></JudgingComponentsCompetitionType>
    <JudgingComponentsCompetitionType JudgingComponent="2" CompetitionType="3"></JudgingComponentsCompetitionType>
    <JudgingComponentsCompetitionType JudgingComponent="3" CompetitionType="3"></JudgingComponentsCompetitionType>
    <JudgingComponentsCompetitionType JudgingComponent="4" CompetitionType="3"></JudgingComponentsCompetitionType>
    <!-- Formation -->
    <JudgingComponentsCompetitionType JudgingComponent="10" CompetitionType="5"></JudgingComponentsCompetitionType>
    <JudgingComponentsCompetitionType JudgingComponent="11" CompetitionType="5"></JudgingComponentsCompetitionType>
    <JudgingComponentsCompetitionType JudgingComponent="12" CompetitionType="5"></JudgingComponentsCompetitionType>
    <JudgingComponentsCompetitionType JudgingComponent="13" CompetitionType="5"></JudgingComponentsCompetitionType>
    <!--- Show Dance -->
    <JudgingComponentsCompetitionType JudgingComponent="1" CompetitionType="6"></JudgingComponentsCompetitionType>
    <JudgingComponentsCompetitionType JudgingComponent="2" CompetitionType="6"></JudgingComponentsCompetitionType>
    <JudgingComponentsCompetitionType JudgingComponent="3" CompetitionType="6"></JudgingComponentsCompetitionType>
    <JudgingComponentsCompetitionType JudgingComponent="5" CompetitionType="6"></JudgingComponentsCompetitionType>
    <!-- IDO -->
    <JudgingComponentsCompetitionType JudgingComponent="6" CompetitionType="4"></JudgingComponentsCompetitionType>
    <JudgingComponentsCompetitionType JudgingComponent="7" CompetitionType="4"></JudgingComponentsCompetitionType>
    <JudgingComponentsCompetitionType JudgingComponent="8" CompetitionType="4"></JudgingComponentsCompetitionType>
    <JudgingComponentsCompetitionType JudgingComponent="9" CompetitionType="4"></JudgingComponentsCompetitionType>
    <!-- Wheelchair -->
    <JudgingComponentsCompetitionType JudgingComponent="1" CompetitionType="7"></JudgingComponentsCompetitionType>
    <JudgingComponentsCompetitionType JudgingComponent="2" CompetitionType="7"></JudgingComponentsCompetitionType>
    <JudgingComponentsCompetitionType JudgingComponent="3" CompetitionType="7"></JudgingComponentsCompetitionType>
    <JudgingComponentsCompetitionType JudgingComponent="4" CompetitionType="7"></JudgingComponentsCompetitionType>
  </JudgingComponentsCompetitionTypes>
  <DanceTemplates>
    <DanceTemplate SectionId="1" Dances="1,2,3,4,5"/>
    <DanceTemplate SectionId="2" Dances="6,7,8,9,10" />
    <DanceTemplate SectionId="3" Dances="1,2,3,4,5,6,7,8,9,10" />

    <DanceTemplate SectionId="3" ClassId="1" AgeGroupId="1"  Dances="1,2,5,6,7,10" />
    <DanceTemplate SectionId="3" ClassId="1" AgeGroupId="2"  Dances="1,2,5,6,7,10" />
    <DanceTemplate SectionId="1" ClassId="1" AgeGroupId="10"  Dances="1,2,5" />
    <DanceTemplate SectionId="2" ClassId="1" AgeGroupId="10"  Dances="6,7,10" />
    <DanceTemplate SectionId="1" ClassId="1" AgeGroupId="11"  Dances="1,2,5" />
    <DanceTemplate SectionId="2" ClassId="1" AgeGroupId="11"  Dances="6,7,10" />

    <DanceTemplate SectionId="3" ClassId="2" Dances="1,2,4,5,6,7,9,10" />
    <DanceTemplate SectionId="1" ClassId="2" AgeGroupId="8" Dances="1,2,4,5" />
    <DanceTemplate SectionId="1" ClassId="2" AgeGroupId="9" Dances="1,2,4,5" />
    <DanceTemplate SectionId="1" ClassId="2" AgeGroupId="10" Dances="1,2,4,5" />
    <DanceTemplate SectionId="1" ClassId="2" AgeGroupId="11" Dances="1,2,4,5" />
    <DanceTemplate SectionId="2" ClassId="2" AgeGroupId="8" Dances="6,7,9,10" />
    <DanceTemplate SectionId="2" ClassId="2" AgeGroupId="9" Dances="6,7,9,10" />
    <DanceTemplate SectionId="2" ClassId="2" AgeGroupId="10" Dances="6,7,9,10" />
    <DanceTemplate SectionId="2" ClassId="2" AgeGroupId="11" Dances="6,7,9,10" />

    <DanceTemplate SectionId="3" ClassId="3" Dances="1,2,3,4,5,6,7,8,9,10" />
    <DanceTemplate SectionId="1" ClassId="3" Dances="1,2,3,4,5" />
    <DanceTemplate SectionId="2" ClassId="3" Dances="6,7,8,9,10" />

    <DanceTemplate SectionId="3" ClassId="4" Dances="1,2,3,4,5,6,7,8,9,10" />
    <DanceTemplate SectionId="1" ClassId="4" Dances="1,2,3,4,5" />
    <DanceTemplate SectionId="2" ClassId="4" Dances="6,7,8,9,10" />

    <DanceTemplate SectionId="3" ClassId="5" Dances="1,2,3,4,5,6,7,8,9,10" />
    <DanceTemplate SectionId="1" ClassId="5" Dances="1,2,3,4,5" />
    <DanceTemplate SectionId="2" ClassId="5" Dances="6,7,8,9,10" />

    <DanceTemplate SectionId="3" ClassId="6" Dances="1,2,3,4,5,6,7,8,9,10" />
    <DanceTemplate SectionId="1" ClassId="6" Dances="1,2,3,4,5" />
    <DanceTemplate SectionId="2" ClassId="6" Dances="6,7,8,9,10" />

    <DanceTemplate SectionId="3" ClassId="6" Dances="1,2,3,4,5,6,7,8,9,10" />
    <DanceTemplate SectionId="1" ClassId="6" Dances="1,2,3,4,5" />
    <DanceTemplate SectionId="2" ClassId="6" Dances="6,7,8,9,10" />

    <DanceTemplate SectionId="4" Dances="12,13,14,20" />
    <DanceTemplate SectionId="5" Dances="19" />
    <DanceTemplate SectionId="6" Dances="17" />
    <DanceTemplate SectionId="7" Dances="18" />
    <DanceTemplate SectionId="8" Dances="15" />
    <DanceTemplate SectionId="9" Dances="16" />
    <DanceTemplate SectionId="10" Dances="21" />
  </DanceTemplates>
  <Officials>
    <Official Id="1" Sign="A" ExternalId="" Firstname="Li" Lastname="Janan" Country="CHN" MIN="" Roles="1"/>
    <Official Id="2" Sign="B" ExternalId="" Firstname="Mei Li" Lastname="Yeh Liu" Country="TPE" MIN="" Roles="1"/>
    <Official Id="3" Sign="C" ExternalId="" Firstname="Sutu" Lastname="Ok" Country="KOR" MIN="" Roles="1"/>
    <Official Id="4" Sign="D" ExternalId="" Firstname="Takeshi" Lastname="Nakanishi" Country="JPN" MIN="" Roles="1"/>
    <Official Id="5" Sign="E" ExternalId="" Firstname="Tin Hung" Lastname="Ho" Country="HKG" MIN="" Roles="1"/>
    <Official Id="6" Sign="F" ExternalId="" Firstname="Luis" Lastname="Pereira" Country="MAC" MIN="" Roles="1"/>
    <Official Id="7" Sign="G" ExternalId="" Firstname="Anantapat" Lastname="Siripatnapakul" Country="THA" MIN="" Roles="1"/>
    <Official Id="8" Sign="H" ExternalId="" Firstname="Gloria T." Lastname="Arcala" Country="PHI" MIN="" Roles="1"/>
    <Official Id="9" Sign="I" ExternalId="" Firstname="Hap Seng" Lastname="Tan" Country="MAS" MIN="" Roles="1"/>
    <Official Id="10" Sign="CM" ExternalId="" Firstname="Keiji" Lastname="Ukai" Country="JPN" MIN="" Roles="2"/>
  </Officials>
  <Events>
    <Event Id="1" DanceFloor="N" Federation="WDSF" Organizer="CDSA" Place="Testhausen" Title="6th Test Dance Games"></Event>
  </Events>
  <Competitions>

  </Competitions>
</Content>

