﻿// // TPS.net TPS8 DataModel
// // MarkingTypes.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH
namespace DataModel
{
    public enum MarkingTypes
    {
        Undefinied = 0,

        Marking = 1,

        NewJudgingSystemV1 = 2,

        NewJudgingSystemV2 = 3,

        Skating = 4,

        TeamMatch = 5,

        MarksSumOnly = 6,

        PointsGeneral = 7
    }
}