// // TPS.net TPS8 Common
// // StatusMessage.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System.Text;

namespace com.signalkontor.Common
{
    public class StatusMessage : StringMessage
    {
        private static readonly int _thisAppId = 6;
        private GeneralStatus _status;

        public StatusMessage(string message)
            : base(message, _thisAppId)
        {
            this.StatusString = message;
        }

        public StatusMessage(GeneralStatus status) 
            : base(status.ToString(), _thisAppId)
        {
            this._status = status;
        }

#if !Android
        public StatusMessage(byte[] buffer)
            : base(buffer)
        {
            this.StatusString = Encoding.ASCII.GetString(this._payload);
        }
#endif

        public GeneralStatus Status 
        {
            get { return this._status; }
            set { this._status = value; }
        }

        public int StatusInt
        {
            get { return (int) this._status; }
            set { this._status = (GeneralStatus)value; }
        }

        public string StatusString
        {
            get { return this._status.ToString(); }
            set
            {
                switch (value)
                {
                    /*case "Loading": _status = GeneralStatus.Loading;
                        break;
                    case "Unloading": _status = GeneralStatus.Unloading;
                        break;*/
                    case "Pause":
                        this._status = GeneralStatus.Pause;
                        break;
                    case "TrafficJam":
                        this._status = GeneralStatus.TrafficJam;
                        break;
                    case "Abwesend":
                        this._status = GeneralStatus.Abwesend;
                        break;
                    case "Freigemeldet":
                        this._status = GeneralStatus.Freigemeldet;
                        break;
                    case "Unterwegs":
                        this._status = GeneralStatus.Unterwegs;
                        break;
                    default:
                        this._status = GeneralStatus.Pause;
                        break;
                }
            }
        }
    }
}
