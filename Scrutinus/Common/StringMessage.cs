// // TPS.net TPS8 Common
// // StringMessage.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System.Runtime.Serialization;
using System.Text;

namespace com.signalkontor.Common
{
    [DataContract]
    public class StringMessage : COMMessage
    {
        protected string _message;

        public StringMessage()
        {
            this._message = "";
        }

#if !Android
        public StringMessage(byte[] buffer)
            : base(buffer)
        {
            // this.message = new string(AdressOf(_payload), 0, _payload.Length);
            this._message = Encoding.ASCII.GetString(this._payload, 0, this._payload.Length);
        }
#endif

        public StringMessage(string message, int appId)
            : base(message, appId)
        {
            this._message = message;
            this.AppID = appId;
        }

        [DataMember]
        public string Message
        {
            get { return this._message; }
            set {
                this._message = value;
                this.setPayload(); }
        }

        protected override void setPayload()
        {
            var enc = new ASCIIEncoding();
            this._payload = enc.GetBytes(this._message);
        }
    }
}
