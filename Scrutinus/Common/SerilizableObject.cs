// // TPS.net TPS8 Common
// // SerilizableObject.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

namespace com.signalkontor.Common
{
    public class SerializeableObject
    {
        private int _appID;

        public SerializeableObject(int appID)
        {
            this._appID = appID;
        }

        public int AppID
        {
            get { return this._appID; }
            set { this._appID = value; }
        }

#if !Android
        public SerializeableObject(byte[] buffer)
        {
            var ser = new ObjectSerializer();
            ser.deserialize(buffer, this);
        }

        public byte[] getBytes()
        {
            var ser = new ObjectSerializer();
            return ser.serialize(this, this._appID);
        }
#endif
    }
}
