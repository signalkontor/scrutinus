// // TPS.net TPS8 Common
// // ObjectSerializer.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH
#if !Android
using System;
using System.Globalization;
using System.Reflection;
using System.Text;

namespace com.signalkontor.Common
{
    internal class ObjectSerializer
    {
        private byte[] _buffer;


        private readonly int _bufferSize;
        private readonly CultureInfo _ci;
        private int _index;

        public ObjectSerializer()
        {
            this._buffer = new byte[1024];
            this._bufferSize = 1024;
            this._index = 4;
            this._ci = new CultureInfo("de-DE");
        }

        protected void addToBuffer(object obj, object target)
        {


            if (obj == null)
            {
                // Wir machen mal einen leeren String draus?
                obj = "";
            }

            byte[] buffer = null;

            var type = obj.GetType();


            if (obj is int)
            {
                buffer = this.getByte((int)obj);
            }
            if (obj is double)
            {
                buffer = this.getByte((double)obj);
            }
            if (obj is string)
            {
                buffer = this.getByte((string)obj);
            }
            if (obj is DateTime)
            {
                buffer = this.getByte((DateTime)obj);
            }

            // wir pr�fen, ob wir noch genug platz haben
            if (this._bufferSize - this._index <= buffer.Length)
            {
                var newArray = new byte[this._index + buffer.Length + 1];
                this._buffer.CopyTo(newArray, 0);
                this._buffer = newArray;
            }

            buffer.CopyTo(this._buffer, this._index);
            this._index += buffer.Length;

        }


        public byte[] serialize(object obj, int AppID)
        {
            var props = obj.GetType().GetProperties();
            foreach (var prop in props)
            {
                if (prop.CanRead && prop.CanWrite)
                {
                    if (prop.Name != "AppID")
                    {
                        this.addToBuffer(prop.GetValue(obj, null), obj);
                    }
                }
            }

            var fields = obj.GetType().GetFields();
            foreach (var info in fields)
            {
                if (info.IsPublic)
                {
                    this.addToBuffer(info.GetValue(obj), obj);
                }
            }

            var ret = new byte[this._index];
            for (var i = 0; i < this._index; i++)
            {
                ret[i] = this._buffer[i];
            }

            ret[0] = this.getHighByte(ret.Length);
            ret[1] = this.getLowByte(ret.Length);
            ret[2] = this.getHighByte(AppID);
            ret[3] = this.getLowByte(AppID);

            return ret;
        }

        public void setValue(PropertyInfo info, object target)
        {
            if (info.PropertyType == typeof(int))
            {
                info.SetValue(target, this.getInt(this._index), null);
            }
            if (info.PropertyType == typeof(string))
            {
                info.SetValue(target, this.getString(this._index), null);
            }
            if (info.PropertyType == typeof(double))
            {
                info.SetValue(target, this.getDouble(this._index), null);
            }
            if (info.PropertyType == typeof(DateTime))
            {
                info.SetValue(target, this.getDateTime(this._index), null);
            }
        }

        public void setValue(FieldInfo info, object target)
        {
            if (info.FieldType == typeof(int))
            {
                info.SetValue(target, this.getInt(this._index));
            }
            if (info.FieldType == typeof(string))
            {
                info.SetValue(target, this.getString(this._index));
            }
            if (info.FieldType == typeof(double))
            {
                info.SetValue(target, this.getDouble(this._index));
            }
            if (info.FieldType == typeof(DateTime))
            {
                info.SetValue(target, this.getDateTime(this._index));
            }
        }

        public object deserialize(byte[] buffer, object obj)
        {
            this._buffer = buffer;
            this._index = 4;

            var props = obj.GetType().GetProperties();
            foreach (var prop in props)
            {
                if (prop.CanRead && prop.CanWrite)
                {
                    if (prop.Name != "AppID")
                    {
                        this.setValue(prop, obj);
                    }
                }
            }

            var fields = obj.GetType().GetFields();
            foreach (var info in fields)
            {
                if (info.IsPublic)
                {
                    this.setValue(info, obj);
                }
            }
            return obj;
        }

        protected byte[] getByte(string val)
        {
            var enc = new UnicodeEncoding();
            var b = enc.GetBytes(val);
            var ret = new byte[b.Length + 1];
            ret[b.Length] = 0;
            b.CopyTo(ret, 0);
            return ret;
        }

        protected byte[] getByte(int val)
        {
            var ret = new byte[2];
            ret[0] = this.getHighByte(val);
            ret[1] = this.getLowByte(val);
            return ret;
        }

        protected byte[] getByte(double val)
        {
            return this.getByte(val.ToString());
        }

        protected byte[] getByte(DateTime val)
        {
            var ret = new byte[6];

            ret[0] = (byte) (val.Year - 2000);
            ret[1] = (byte)val.Month;
            ret[2] = (byte)val.Day;
            ret[3] = (byte)val.Hour;
            ret[4] = (byte)val.Minute;
            ret[5] = (byte)val.Second;

            return ret;
            
            //return getByte(val.ToString());
        }

        protected byte getHighByte(int value)
        {
            return (byte)((value & 65280) >> 8);
        }

        protected byte getLowByte(int value)
        {
            return (byte)(value & 255);
        }

        protected int getInt(int index)
        {
            this._index += 2;
            return this._buffer[index] * 256 + this._buffer[index + 1];
        }

        protected double getDouble(int index)
        {

            var str = this.getString(index);

            try
            {
                var ret = Double.Parse(str, this._ci);
                return ret;
            }
            catch (Exception)
            {
                return 0.0;
            }
        }

        protected string getString(int index)
        {

            int i;
            for (i = index; i < this._buffer.Length; i += 2)
            {
                if (this._buffer[i] == 0)
                {
                    break;
                }
            }

            if (i >= this._buffer.Length)
            {
                throw new Exception("Could not find terminating <0> while deserilizing string");
            }

            var count = i - index;

            var strbuf = new byte[count];
            for (var j = 0; j < count; j++)
            {
                strbuf[j] = this._buffer[j + index];
            }

            var enc = new UnicodeEncoding();
            var str = enc.GetString(strbuf, 0, strbuf.Length);

            this._index = i + 1;

            return str;
        }

        protected DateTime getDateTime(int index)
        {
            var year = (int)(this._buffer[index] + 2000);
            var month = (int)this._buffer[index + 1];
            var day = (int)this._buffer[index + 2];
            var hour = (int)this._buffer[index + 3];
            var min = (int)this._buffer[index + 4];
            var sek = (int)this._buffer[index + 5];

            var ret = new DateTime(year, month, day, hour, min, sek, 0);

            this._index += 6;

            return ret;

        }
    }
}
#endif
