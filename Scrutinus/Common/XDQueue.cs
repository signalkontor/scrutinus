// // TPS.net TPS8 Common
// // XDQueue.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System.Collections.Generic;

namespace com.signalkontor.Common
{
    public class XDQueue<T>
    {
        private readonly List<T> _buffer;

        public XDQueue()
        {
            this._buffer = new List<T>();
        }

        public int Count
        {
            get
            {
                lock (this._buffer)
                {
                    return this._buffer.Count;
                }
            }
        }

        public void Enqueue(T msg)
        {
            lock (this._buffer)
            {
                this._buffer.Add(msg);
            }
        }

        public T Peak()
        {
            lock(this._buffer)
            {
                if (this._buffer.Count > 0)
                {
                    return this._buffer[0];
                }
                else
                {
                    return default(T);
                }
            }
        }

        public void Remove(T msg)
        {
            lock (this._buffer)
            {
                this._buffer.Remove(msg);
            }
        }

        public T Dequeue()
        {
            lock (this._buffer)
            {
                if (this._buffer.Count > 0)
                {
                    var msg = this._buffer[0];
                    this._buffer.RemoveAt(0);
                    return msg;
                }
                else
                {
                    return default(T);
                }
            }
        }
    }
}
