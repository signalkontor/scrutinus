﻿// // TPS.net TPS8 Common
// // LocalizationService.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Diagnostics;
using System.Globalization;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using WPFLocalizeExtension.Engine;
using WPFLocalizeExtension.Extensions;

namespace Scrutinus.Localization
{
    /// <summary>
    /// This class is used to retrieve localized keys from a localization dictionary in the source code.
    /// </summary>
    public static class LocalizationService
    {
        private static readonly object Locker = new object();

        private static readonly LocExtension LocExtension = new LocExtension();

        /// <summary>
        /// Gets or sets the application language.
        /// </summary>
        /// <value>
        /// The application language.
        /// </value>
        public static CultureInfo ApplicationLanguage
        {
            get
            {
                return LocalizeDictionary.Instance.Culture;
            }

            set
            {
                if (value != null && !value.Equals(LocalizeDictionary.Instance.Culture))
                {
                    LocalizeDictionary.Instance.Culture = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the application region.
        /// </summary>
        /// <value>
        /// The application region.
        /// </value>
        public static CultureInfo ApplicationRegion
        {
            get
            {
                return Thread.CurrentThread.CurrentCulture;
            }

            set
            {
                var currentCulture = Thread.CurrentThread.CurrentCulture;
                if (value != null && !value.Equals(currentCulture))
                {
                    Thread.CurrentThread.CurrentCulture = value;
                    Thread.CurrentThread.CurrentCulture = value;
                }
            }
        }

        /// <summary>
        /// Resolves the given key to a localized value and formats it with the given parameters.
        /// </summary>
        /// <typeparam name="T">The type parameter</typeparam>
        /// <param name="expr">The path to the key to use in format (path.to.key.to.use)</param>
        /// <param name="parameters">The parameters to be used for the formatting.</param>
        /// <returns>
        /// The localized string value associated with the given key, based on the current language, formatted using
        /// the given parameters.
        /// </returns>
        public static string FormatLocalized<T>(Expression<Func<T>> expr, params object[] parameters)
        {
            var resolvedText = Resolve(expr);

            return string.Format(CultureInfo.CurrentCulture, resolvedText, parameters);
        }

        /// <summary>
        /// Resolves the given key to a localized value.
        /// </summary>
        /// <typeparam name="T">
        /// The type parameter
        /// </typeparam>
        /// <param name="expr">
        /// The path to the key to use in format (path.to.key.to.use)
        /// </param>
        /// <returns>
        /// The localized string value associated with the given key, based on the current language.
        /// </returns>
        /// <exception cref="System.InvalidOperationException">
        /// Thrown if the value could not be resolved.
        /// </exception>
        public static string Resolve<T>(Expression<Func<T>> expr)
        {
            string localizedString;

            lock (Locker)
            {
                LocExtension.ResourceIdentifierKey = GetLocalizationEngineStringRepresentation(expr);

                if (!LocExtension.ResolveLocalizedValue(out localizedString))
                {
                    if (string.IsNullOrEmpty(localizedString))
                    {
                        var key = LocExtension.ResourceIdentifierKey;
                        var exMessage = string.Format("Error localizing key '{0}'", key);
                        throw new InvalidOperationException(exMessage);
                    }
                }
            }

            // Make sure to replace line breaks (they get escaped automatically)
            return localizedString.Replace("\\n", "\n");
        }

        public static string Resolve(string keyName)
        {
            string localizedString;

            lock (Locker)
            {
                LocExtension.ResourceIdentifierKey = keyName;

                if (!LocExtension.ResolveLocalizedValue(out localizedString))
                {
                    if (string.IsNullOrEmpty(localizedString))
                    {
                        var key = LocExtension.ResourceIdentifierKey;
                        var exMessage = string.Format("Error localizing key '{0}'", key);
                        Debug.WriteLine(exMessage);
                        return exMessage;
                        throw new InvalidOperationException(exMessage);
                    }
                }
            }

            // Make sure to replace line breaks (they get escaped automatically)
            return localizedString.Replace("\\n", "\n");
        }

        /// <summary>
        /// Takes a LINQ expression and returns a string formatted for use with the localization resource files
        /// (assembly:dictionary:key) based upon the given expression.
        /// </summary>
        /// <typeparam name="T">
        /// The type parameter
        /// </typeparam>
        /// <param name="expr">
        /// The path to the key to use in format (path.to.key.to.use)
        /// </param>
        /// <returns>
        /// A string formatted for use with the localization resource files (assembly:dictionary:key) based upon the
        /// given expression.
        /// </returns>
        private static string GetLocalizationEngineStringRepresentation<T>(Expression<Func<T>> expr)
        {
            var memberExpression = (MemberExpression)expr.Body;

            var fullAssemblyName = memberExpression.Member.ReflectedType.Assembly.FullName;
            var assembly = fullAssemblyName.Substring(
                0,
                fullAssemblyName.IndexOf(", ", StringComparison.InvariantCulture));
            var fullDictionaryName = memberExpression.Member.ReflectedType.FullName;
            var dictionary =
                fullDictionaryName.Substring(fullDictionaryName.LastIndexOf(".", StringComparison.InvariantCulture) + 1);
            var key = memberExpression.Member.Name;

            var sb = new StringBuilder();
            sb.Append(assembly);
            sb.Append(":");
            sb.Append(dictionary);
            sb.Append(":");
            sb.Append(key);

            return sb.ToString();
        }
    }
}