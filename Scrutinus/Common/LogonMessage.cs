// // TPS.net TPS8 Common
// // LogonMessage.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System.Runtime.Serialization;

namespace com.signalkontor.Common
{
    [DataContract]
    public class LogonMessage : COMMessage
    {
#if !Android
        public LogonMessage(byte[] buffer) : base(buffer)
        {

        }
#endif

        public LogonMessage(string user, string password) : base(1)
        {
            this.User = user;
            this.Password = password;
        }

        [DataMember]
        public string User { get; set; }

        [DataMember]
        public string Password { get; set; }
    }
}
