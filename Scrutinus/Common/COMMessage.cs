// // TPS.net TPS8 Common
// // COMMessage.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;

namespace com.signalkontor.Common
{
    [DataContract]
    public class COMMessage
    {
        protected byte[] _buffer;

        private string _json;
        protected byte[] _payload = null;

        public COMMessage()
        {
            this.AppID = 0;
        }

        public COMMessage(int appId)
            : base()
        {
            this.AppID = appId;
        }

        public COMMessage(string str, int appId)
        {
            var enc = new ASCIIEncoding();
            this._payload = enc.GetBytes(str);
            this.AppID = appId;
        }

#if !Android
        public COMMessage(byte[] buffer)
        {
            this._buffer = buffer;
            this.AppID = buffer[2] * 256 + buffer[3];
            /*
                        _payload = new byte[buffer.Length - 4];
                        for (int i=0; i<_payload.Length; i++)
                        {
                            _payload[i] = buffer[i+4];
                        }
            */
            var ser = new ObjectSerializer();
            ser.deserialize(buffer, this);
        }
#endif

        [DataMember]
        public int AppID { get; set; }

        public static T FromJson<T>(byte[] json) where T : class
        {
            var dataContractJsonSerializer = new DataContractJsonSerializer(typeof(T));
            var memoryStream = new MemoryStream(json);
            return dataContractJsonSerializer.ReadObject(memoryStream) as T;
        }

        public static T FromJson<T>(string json) where T : class
        {
            return FromJson<T>(Encoding.UTF8.GetBytes(json));
        }

        public string ToJson()
        {
            var memoryStream = new MemoryStream();
            var dataContractJsonSerializer = new DataContractJsonSerializer(this.GetType());
            dataContractJsonSerializer.WriteObject(memoryStream, this);
            memoryStream.Position = 0;
            var streamReader = new StreamReader(memoryStream);
            return streamReader.ReadToEnd();
        }

        public string GetJson()
        {
            return this._json ?? (this._json = this.ToJson());
        }

        protected virtual void setPayload() { }

#if !Android
        public byte[] getBuffer()
        {

            if (this._buffer != null)
            {
                // Sicherheitshalber noch die Buffer - Length sezen
                this._buffer[0] = this.getHighByte(this._buffer.Length);
                this._buffer[1] = this.getLowByte(this._buffer.Length);
                return this._buffer;
            }
            var ser = new ObjectSerializer();
            return ser.serialize(this, this.AppID);
        }
#endif

        public byte[] getSizeArray()
        {
            var buffer = new byte[2];
            var size = this._payload.Length + 2 + 2;
            buffer[0] = this.getHighByte(size);
            buffer[1] = this.getLowByte(size);

            return buffer;
        }


        protected byte getHighByte(int value)
        {
            var test1 = value >> 8;
            var test2 = value >> 4;
            return (byte) ((value & 65280) >> 8);
        }

        protected byte getLowByte(int value)
        {
            return (byte)(value & 255);
        }

        public override string ToString()
        {
            return "\nApplication ID: " + this.AppID.ToString() + "\nPayload: " + Encoding.ASCII.GetString(this._payload, 0, this._payload.Length) + "\n";
        }
    }
}
