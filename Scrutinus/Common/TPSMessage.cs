// // TPS.net TPS8 Common
// // TPSMessage.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Runtime.Serialization;

namespace com.signalkontor.Common
{
    [DataContract]
    public class TPSMessage : COMMessage
    {
        // sonst 0
        [DataMember]
        public string FDoCmd;          // Auszuführendes Kommando

        [DataMember]
        public string FMsgData;        // Die eigentliche Nachricht

        [DataMember]
        public string FReceiver;       // Adresse des Empfängers

        [DataMember]
        public string FReceiverIP;

        [DataMember]
        public int FResponseId;        // Im Falle einer Antwort die TargetId der empfangenen Nachricht,

        [DataMember]
        public string FSender;         // Sender der Nachricht

        [DataMember]
        public int FTargetId;          // Art der Nachricht

        [DataMember]
        public DateTime FTimeStamp;    // Lokaler Timestamp beim Erstellen der Message

#if !Android
        public TPSMessage(byte[] data)
            : base(data)
        {

        }
#endif

        public TPSMessage(int target, string data)
            : base(1001)
        {
            this.FTargetId = target;
            this.FMsgData = data;
        }

        public TPSMessage()
            : base(120)
        { }
    }
}
