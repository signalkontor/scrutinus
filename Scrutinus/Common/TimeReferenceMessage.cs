// // TPS.net TPS8 Common
// // TimeReferenceMessage.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System;
using System.Runtime.Serialization;

namespace com.signalkontor.Common
{
    [DataContract]
    public class TimeReferenceMessage : COMMessage
    {
        [DataMember]
        public DateTime ReferenceTime;

        public TimeReferenceMessage()
            : base(10)
        {
            this.ReferenceTime = DateTime.Now;
        }

#if !Android
        public TimeReferenceMessage(byte[] data)
            : base(data)
        {

        }
#endif
    }
}
