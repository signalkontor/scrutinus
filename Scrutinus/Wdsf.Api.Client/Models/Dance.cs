﻿// // TPS.net TPS8 Wdsf.Api.Client
// // Dance.cs
// // Last Changed: 2017/06/09
// // (c) 2017 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Wdsf.Api.Client.Models
{
    [XmlType("dance", Namespace = "http://services.worlddancesport.org/api")]
    public class Dance
    {
        private List<Score> scores = new List<Score>();

        [XmlAttribute("name")]
        public string Name { get; set; }

        /// <summary>
        /// <para>Do not use this array to process scores.</para>
        /// <para>It is used only as a workaround for .NET's XmlSerializer limitations on deserializing lists.</para>
        /// </summary>
        [XmlArray("scores")]
        [XmlArrayItem("mark", typeof(MarkScore))]
        [XmlArrayItem("final", typeof(FinalScore))]
        [XmlArrayItem("onScale", typeof(OnScaleScore))]
        [XmlArrayItem("onScaleIdo", typeof(OnScaleIdoScore))]
        [XmlArrayItem("onScale2", typeof(OnScale2Score))]
        public Score[] ScoresForSerialization
        {
            get
            {
                return this.scores.Count == 0 ? null : this.scores.ToArray();
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("value");
                }

                this.scores = new List<Score>(value);
            }
        }

        [XmlIgnore]
        public IList<Score> Scores
        {
            get
            {
                return this.scores;
            }
        }
    }
}
