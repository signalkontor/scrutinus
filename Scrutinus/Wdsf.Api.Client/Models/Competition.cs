﻿// // TPS.net TPS8 Wdsf.Api.Client
// // Competition.cs
// // Last Changed: 2017/06/09
// // (c) 2017 Olav Gröhn / signalkontor GmbH

using System;
using System.Xml.Serialization;

namespace Wdsf.Api.Client.Models
{
    [XmlType("competition", Namespace = "http://services.worlddancesport.org/api")]
    [XmlRoot("competition", Namespace = "http://services.worlddancesport.org/api")]
    public class Competition
    {
        [XmlElement("link")]
        public Link[] Links { get; set; }

        [XmlElement("id")]
        public int Id { get; set; }

        [XmlElement("name")]
        public string Name { get; set; }

        [XmlElement("lastmodifiedDate")]
        public DateTime LastModifiedDate { get; set; }
    }
}