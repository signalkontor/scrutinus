﻿// // TPS.net TPS8 Wdsf.Api.Client
// // PersonDetail.cs
// // Last Changed: 2017/06/09
// // (c) 2017 Olav Gröhn / signalkontor GmbH

using System.Xml.Serialization;
using Wdsf.Api.Client.Attributes;

namespace Wdsf.Api.Client.Models
{
    [XmlType("person", Namespace = "http://services.worlddancesport.org/api")]
    [XmlRoot("person", Namespace = "http://services.worlddancesport.org/api")]
    [MediaType("application/vnd.worlddancesport.person")]
    public class PersonDetail 
    {
        [XmlElement("link")]
        public Link[] Link  { get; set; }

        [XmlElement("id")]
        public int Min { get; set; }

        [XmlElement("name")]
        public string Name { get; set; }

        [XmlElement("surname")]
        public string Surname { get; set; }

        [XmlElement("title")]
        public string Title { get; set; }

        [XmlElement("nationality")]
        public string Nationality { get; set; }

        [XmlElement("country")]
        public string Country { get; set; }

        [XmlElement("ageGroup")]
        public string AgeGroup { get; set; }

        [XmlArray("licenses")]
        public License[] Licenses { get; set; }
    }
}
