﻿// // TPS.net TPS8 Wdsf.Api.Client
// // TeamBase.cs
// // Last Changed: 2017/06/09
// // (c) 2017 Olav Gröhn / signalkontor GmbH

using System.Xml.Serialization;

namespace Wdsf.Api.Client.Models
{
    public class TeamBase
    {
        [XmlElement("link")]
        public virtual Link[] Links { get; set; }

        [XmlElement("id")]
        public virtual int Id { get; set; }

        [XmlElement("name")]
        public string Name { get; set; }

        [XmlElement("country")]
        public string Country { get; set; }
    }
}
