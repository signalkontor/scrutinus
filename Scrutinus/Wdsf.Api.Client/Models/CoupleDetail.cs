﻿// // TPS.net TPS8 Wdsf.Api.Client
// // CoupleDetail.cs
// // Last Changed: 2017/06/09
// // (c) 2017 Olav Gröhn / signalkontor GmbH

using System.Xml.Serialization;
using Wdsf.Api.Client.Attributes;

namespace Wdsf.Api.Client.Models
{
    [XmlType("couple", Namespace = "http://services.worlddancesport.org/api")]
    [XmlRoot(Namespace = "http://services.worlddancesport.org/api")]
    [MediaType("application/vnd.worlddancesport.couple")]
    public class CoupleDetail : CoupleBase
    {
        [XmlElement("name")]
        public string Name { get; set; }

        [XmlElement("country")]
        public string Country { get; set; }

        [XmlElement("age")]
        public string AgeGroup { get; set; }

        [XmlElement("division")]
        public string Division { get; set; }

        [XmlElement("status")]
        public string Status { get; set; }

        [XmlElement("retiredOn")]
        public string RetiredOn { get; set; }

        [XmlElement("wrlBLockedUntil")]
        public string WrlBlockedUntil { get; set; }

        [XmlElement("cupOrChampionshipBlockedUntil")]
        public string CupOrChampionshipBlockedUntil { get; set; }

        [XmlElement("man")]
        public int ManMin { get; set; }

        [XmlElement("customManName")]
        public string UnknownManName { get; set; }

        [XmlElement("woman")]
        public int WomanMin { get; set; }

        [XmlElement("customWomanName")]
        public string UnknownWomanName { get; set; }

        [XmlIgnore]
        public bool UnknownWomanNameSpecified { get { return !string.IsNullOrEmpty(this.UnknownWomanName); } set { } }

        [XmlIgnore]
        public bool UnknownManNameSpecified { get { return !string.IsNullOrEmpty(this.UnknownManName); } set { } }

        [XmlIgnore]
        public bool ManMinSpecified { get { return this.ManMin != 0; } set { } }

        [XmlIgnore]
        public bool WomanMinSpecified { get { return this.WomanMin != 0; } set { } }
    }
}
