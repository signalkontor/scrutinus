﻿// // TPS.net TPS8 Wdsf.Api.Client
// // ParticipantCouple.cs
// // Last Changed: 2017/06/09
// // (c) 2017 Olav Gröhn / signalkontor GmbH

using System.Xml.Serialization;

namespace Wdsf.Api.Client.Models
{
    [XmlType("participant", Namespace = "http://services.worlddancesport.org/api")]
    [XmlRoot("participant", Namespace = "http://services.worlddancesport.org/api")]
    public class ParticipantCouple : ParticipantBase
    {
        [XmlElement("link")]
        public override Link[] Link { get; set; }

        [XmlElement("name")]
        public string Couple { get; set; }

        [XmlElement("country")]
        public string Country { get; set; }

        [XmlElement("number")]
        public string StartNumber { get; set; }
    }
}
