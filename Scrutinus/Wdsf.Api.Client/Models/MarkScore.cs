﻿// // TPS.net TPS8 Wdsf.Api.Client
// // MarkScore.cs
// // Last Changed: 2017/06/09
// // (c) 2017 Olav Gröhn / signalkontor GmbH

using System.Xml.Serialization;

namespace Wdsf.Api.Client.Models
{
    [XmlType("mark", Namespace = "http://services.worlddancesport.org/api")]
    public class MarkScore : Score
    {
        public MarkScore()
            :base()
        {

        }

        public MarkScore(int officialId, bool isSet)
            :base(officialId)
        {
            this.IsSet = isSet;
        }

        [XmlAttribute("set")]
        public bool IsSet { get; set; }

        [XmlIgnore]
        public bool IsSetSpecified { get { return this.IsSet; } set { ;} }
    }
}
