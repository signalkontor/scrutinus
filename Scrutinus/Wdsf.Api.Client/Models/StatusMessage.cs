﻿// // TPS.net TPS8 Wdsf.Api.Client
// // StatusMessage.cs
// // Last Changed: 2017/06/09
// // (c) 2017 Olav Gröhn / signalkontor GmbH

using System.Xml.Serialization;
using Wdsf.Api.Client.Attributes;

namespace Wdsf.Api.Client.Models
{
    [XmlType("status", Namespace = "http://services.worlddancesport.org/api")]
    [XmlRoot(Namespace = "http://services.worlddancesport.org/api")]
    [MediaType("application/vnd.worlddancesport.status")]
    public class StatusMessage
    {
        [XmlElement("code")]
        public int Code { get; set; }

        [XmlElement("subcode")]
        public int SubCode { get; set; }

        [XmlElement("message")]
        public string Message { get; set; }

        [XmlElement("id")]
        public string Id { get; set; }

        [XmlElement("link")]
        public Link Link { get; set; }
    }
}
