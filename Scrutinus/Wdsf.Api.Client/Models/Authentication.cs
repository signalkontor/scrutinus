﻿// // TPS.net TPS8 Wdsf.Api.Client
// // Authentication.cs
// // Last Changed: 2017/06/09
// // (c) 2017 Olav Gröhn / signalkontor GmbH

using System.Xml.Serialization;

namespace Wdsf.Api.Client.Models
{
    [XmlType("authentication", Namespace = "http://services.worlddancesport.org/api")]
    public class Authentication
    {
        [XmlElement("isValid")]
        public bool IsValid { get; set; }
    }
}
