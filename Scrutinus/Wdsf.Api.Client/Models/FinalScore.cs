﻿// // TPS.net TPS8 Wdsf.Api.Client
// // FinalScore.cs
// // Last Changed: 2017/06/09
// // (c) 2017 Olav Gröhn / signalkontor GmbH

using System.Xml.Serialization;

namespace Wdsf.Api.Client.Models
{
    [XmlType("final", Namespace = "http://services.worlddancesport.org/api")]
    public class FinalScore : Score
    {
        public FinalScore():
            base()
        {

        }

        public FinalScore(int officialId, int rank):
            base(officialId)
        {
            this.Rank = rank;
        }

        [XmlAttribute("rank")]
        public int Rank { get; set; }
    }
}
