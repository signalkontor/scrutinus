﻿// // TPS.net TPS8 Wdsf.Api.Client
// // ListOfRanking.cs
// // Last Changed: 2017/06/09
// // (c) 2017 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Wdsf.Api.Client.Attributes;

namespace Wdsf.Api.Client.Models
{
    [XmlType("ranking", Namespace = "http://services.worlddancesport.org/api")]
    [XmlRoot("ranking", Namespace = "http://services.worlddancesport.org/api")]
    [MediaType("application/vnd.worlddancesport.ranking", IsCollection = true)]
    public class ListOfRanking : List<Ranking>
    {
        [XmlElement("validDate")]
        public DateTime RankingDate { get; set; }
    }
}
