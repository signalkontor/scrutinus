﻿// // TPS.net TPS8 Wdsf.Api.Client
// // CoupleBase.cs
// // Last Changed: 2017/06/09
// // (c) 2017 Olav Gröhn / signalkontor GmbH

using System.Xml.Serialization;

namespace Wdsf.Api.Client.Models
{
    public class CoupleBase
    {
        [XmlElement("link")]
        public virtual Link[] Links { get; set; }

        [XmlElement("id")]
        public virtual string Id { get; set; }
    }
}
