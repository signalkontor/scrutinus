﻿// // TPS.net TPS8 Wdsf.Api.Client
// // Round.cs
// // Last Changed: 2017/06/09
// // (c) 2017 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Wdsf.Api.Client.Models
{
    [XmlType("round", Namespace = "http://services.worlddancesport.org/api")]
    public class Round
    {
        private List<Dance> dances = new List<Dance>();

        [XmlAttribute("name")]
        public string Name { get; set; }

        /// <summary>
        /// <para>Do not use this array to process dances.</para>
        /// <para>It is used only as a workaround for .NET's XmlSerializer limitations on deserializing lists.</para>
        /// </summary>
        [XmlArray("dances")]
        public Dance[] DancesForSerialization
        {
            get
            {
                return this.dances.Count == 0 ? null : this.dances.ToArray();
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("value");
                }

                this.dances = new List<Dance>(value);
            }
        }

        [XmlIgnore]
        public IList<Dance> Dances
        {
            get
            {
                return this.dances;
            }
        }
    }
}
