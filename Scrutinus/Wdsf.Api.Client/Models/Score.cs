﻿// // TPS.net TPS8 Wdsf.Api.Client
// // Score.cs
// // Last Changed: 2017/06/09
// // (c) 2017 Olav Gröhn / signalkontor GmbH

using System.Xml.Serialization;

namespace Wdsf.Api.Client.Models
{
    public class Score
    {
        public Score()
        {

        }

        public Score(int officialId)
        {
            this.OfficialId = officialId;
        }

        [XmlElement("link")]
        //[XmlIgnore]
        public Link[] Link { get; set; }

        [XmlAttribute("adjudicator")]
        public int OfficialId { get; set; }
    }
}
