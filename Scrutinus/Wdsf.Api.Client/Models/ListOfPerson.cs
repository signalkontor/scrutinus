﻿// // TPS.net TPS8 Wdsf.Api.Client
// // ListOfPerson.cs
// // Last Changed: 2017/06/09
// // (c) 2017 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Xml.Serialization;
using Wdsf.Api.Client.Attributes;

namespace Wdsf.Api.Client.Models
{
    [XmlType("persons", Namespace = "http://services.worlddancesport.org/api")]
    [XmlRoot("persons", Namespace = "http://services.worlddancesport.org/api")]
    [MediaType("application/vnd.worlddancesport.persons", IsCollection = true)]
    public class ListOfPerson : List<Person>
    {}
}
