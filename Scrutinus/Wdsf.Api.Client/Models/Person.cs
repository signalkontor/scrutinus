﻿// // TPS.net TPS8 Wdsf.Api.Client
// // Person.cs
// // Last Changed: 2017/06/09
// // (c) 2017 Olav Gröhn / signalkontor GmbH

using System;
using System.Xml.Serialization;

namespace Wdsf.Api.Client.Models
{
    [XmlType("person", Namespace = "http://services.worlddancesport.org/api")]
    [XmlRoot("person", Namespace = "http://services.worlddancesport.org/api")]
    public class Person
    {
        [XmlElement("link")]
        public Link[] Link  { get; set; }

        [XmlElement("id")]
        public int Min { get; set; }

        [XmlElement("name")]
        public string Name { get; set; }

        [XmlElement("sex")]
        public string Sex { get; set; }

        public bool IsMale
        {
            get
            {
                if (this.Sex.Equals(Gender.Male, StringComparison.OrdinalIgnoreCase))
                {
                    return true;
                }
                else if (this.Sex.Equals(Gender.Female, StringComparison.OrdinalIgnoreCase))
                {
                    return false;
                }
                else
                {
                    throw new ApplicationException("The person's sex not specified.");
                }
            }
            set
            {
                this.Sex = value ? Gender.Male : Gender.Female;
            }
        }

        [XmlElement("country")]
        public string Country { get; set; }

        [XmlElement("activePartner")]
        public string ActivePartner { get; set; }

        [XmlElement("activeCoupleId")]
        public string ActiveCoupleId { get; set; }

        [XmlElement("activeCoupleAgeGroup")]
        public string AgeGroup { get; set; }
    }
}
