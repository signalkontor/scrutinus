﻿// // TPS.net TPS8 Wdsf.Api.Client
// // Official.cs
// // Last Changed: 2017/06/09
// // (c) 2017 Olav Gröhn / signalkontor GmbH

using System.Xml.Serialization;

namespace Wdsf.Api.Client.Models
{
    [XmlType("official", Namespace = "http://services.worlddancesport.org/api")]
    public class Official
    {
        [XmlElement("link")]
        public Link[] Link { get; set; }

        [XmlElement("id")]
        public int Id { get; set; }

        [XmlElement("name")]
        public string Name { get; set; }

        [XmlElement("country")]
        public string Nationality { get; set; }
    }
}
