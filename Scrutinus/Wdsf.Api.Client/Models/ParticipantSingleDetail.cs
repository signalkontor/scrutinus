﻿// // TPS.net TPS8 Wdsf.Api.Client
// // ParticipantSingleDetail.cs
// // Last Changed: 2017/06/09
// // (c) 2017 Olav Gröhn / signalkontor GmbH

using System.Xml.Serialization;
using Wdsf.Api.Client.Attributes;

namespace Wdsf.Api.Client.Models
{
    [XmlType("participant", Namespace = "http://services.worlddancesport.org/api")]
    [XmlRoot("participant", Namespace = "http://services.worlddancesport.org/api")]
    [MediaType("application/vnd.worlddancesport.participant.single")]
    public class ParticipantSingleDetail : ParticipantBaseDetail
    {
        [XmlElement("link")]
        public override Link[] Link { get; set; }

        [XmlElement("personId")]
        public string PersonId { get; set; }

        [XmlElement("name")]
        public string Name { get; set; }

        [XmlElement("country")]
        public string Country { get; set; }
    }
}
