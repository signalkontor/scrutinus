﻿// // TPS.net TPS8 Wdsf.Api.Client
// // Couple.cs
// // Last Changed: 2017/06/09
// // (c) 2017 Olav Gröhn / signalkontor GmbH

using System.Xml.Serialization;

namespace Wdsf.Api.Client.Models
{
    [XmlType("couple", Namespace = "http://services.worlddancesport.org/api")]
    public class Couple : CoupleBase
    {
        [XmlElement("name")]
        public string Name { get; set; }

        [XmlElement("country")]
        public string Country { get; set; }
    }
}
