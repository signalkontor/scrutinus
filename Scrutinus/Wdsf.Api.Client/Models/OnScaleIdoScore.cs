﻿// // TPS.net TPS8 Wdsf.Api.Client
// // OnScaleIdoScore.cs
// // Last Changed: 2017/06/09
// // (c) 2017 Olav Gröhn / signalkontor GmbH

using System.Xml.Serialization;

namespace Wdsf.Api.Client.Models
{
    [XmlType("onScaleIdo", Namespace = "http://services.worlddancesport.org/api")]
    public class OnScaleIdoScore : Score
    {
        /// <summary>
        /// Technique
        /// </summary>
        [XmlAttribute("t")]
        public virtual decimal T { get; set; }

        /// <summary>
        /// Composition
        /// </summary>
        [XmlAttribute("c")]
        public virtual decimal C { get; set; }

        /// <summary>
        /// Image
        /// </summary>
        [XmlAttribute("i")]
        public virtual decimal I { get; set; }

        /// <summary>
        /// Show
        /// </summary>
        [XmlAttribute("s")]
        public virtual decimal S { get; set; }

        /// <summary>
        /// Reduction by Chairman
        /// </summary>
        [XmlAttribute("reduction")]
        public virtual decimal Reduction { get; set; }
    }
}
