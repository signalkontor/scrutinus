﻿// // TPS.net TPS8 Wdsf.Api.Client
// // Team.cs
// // Last Changed: 2017/06/09
// // (c) 2017 Olav Gröhn / signalkontor GmbH

using System.Xml.Serialization;

namespace Wdsf.Api.Client.Models
{
    [XmlType("team", Namespace = "http://services.worlddancesport.org/api")]
    public class Team : TeamBase
    {}
}