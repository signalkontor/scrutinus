﻿// // TPS.net TPS8 Wdsf.Api.Client
// // ListOfAgeClass.cs
// // Last Changed: 2017/06/09
// // (c) 2017 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Xml.Serialization;
using Wdsf.Api.Client.Attributes;

namespace Wdsf.Api.Client.Models
{
    [XmlType("ages", Namespace = "http://services.worlddancesport.org/api")]
    [XmlRoot("ages", Namespace = "http://services.worlddancesport.org/api")]
    [MediaType("application/vnd.worlddancesport.ages", IsCollection = true)]
    public class ListOfAgeClass : List<AgeClass>
    {}
}
