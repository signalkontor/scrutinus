﻿// // TPS.net TPS8 Wdsf.Api.Client
// // Ranking.cs
// // Last Changed: 2017/06/09
// // (c) 2017 Olav Gröhn / signalkontor GmbH

using System.Xml.Serialization;
using Wdsf.Api.Client.Attributes;

namespace Wdsf.Api.Client.Models
{
    [XmlType("rank", Namespace = "http://services.worlddancesport.org/api")]
    [MediaType("application/vnd.worlddancesport.rank")]
    public class Ranking
    {
        [XmlElement("link")]
        public Link[] Links { get; set; }

        [XmlElement("name")]
        public string Couple { get; set; }

        [XmlElement("country")]
        public string Country { get; set; }

        [XmlElement("rank")]
        public int Rank { get; set; }

        [XmlElement("points")]
        public int Points { get; set; }

        [XmlElement("manMin")]
        public string ManMin { get; set; }

        [XmlElement("womanMin")]
        public string WomanMin { get; set; }

        [XmlElement("coupleId")]
        public string Id { get; set; }
    }
}
