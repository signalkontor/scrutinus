﻿// // TPS.net TPS8 Wdsf.Api.Client
// // Country.cs
// // Last Changed: 2017/06/09
// // (c) 2017 Olav Gröhn / signalkontor GmbH

using System.Xml.Serialization;

namespace Wdsf.Api.Client.Models
{
    [XmlType("country", Namespace = "http://services.worlddancesport.org/api")]
    public class Country
    {
        [XmlText]
        public string Name { get; set; }
    }
}
