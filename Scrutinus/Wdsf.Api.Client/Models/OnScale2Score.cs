﻿// // TPS.net TPS8 Wdsf.Api.Client
// // OnScale2Score.cs
// // Last Changed: 2017/06/09
// // (c) 2017 Olav Gröhn / signalkontor GmbH

using System.Xml.Serialization;

namespace Wdsf.Api.Client.Models
{
    [XmlType("onScale2", Namespace = "http://services.worlddancesport.org/api")]
    public class OnScale2Score : Score
    {
        /// <summary>
        /// Techincal Quality
        /// </summary>
        [XmlAttribute("tq")]
        public virtual decimal TQ { get; set; }

        /// <summary>
        /// Movement to music
        /// </summary>
        [XmlAttribute("mm")]
        public virtual decimal MM { get; set; }

        /// <summary>
        /// Partnering skill
        /// </summary>
        [XmlAttribute("ps")]
        public virtual decimal PS { get; set; }

        /// <summary>
        /// Choreography and Presentation
        /// </summary>
        [XmlAttribute("cp")]
        public virtual decimal CP { get; set; }

        /// <summary>
        /// Reduction by Chairman
        /// </summary>
        [XmlAttribute("reduction")]
        public virtual decimal Reduction { get; set; }

        /// <summary>
        /// This participant was allowed to skip the round it set to true
        /// </summary>
        [XmlAttribute("set")]
        public virtual bool IsSet { get; set; }
    }
}
