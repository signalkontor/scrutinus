﻿// // TPS.net TPS8 Wdsf.Api.Client
// // ListOfCountry.cs
// // Last Changed: 2017/06/09
// // (c) 2017 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Xml.Serialization;
using Wdsf.Api.Client.Attributes;

namespace Wdsf.Api.Client.Models
{
    [XmlType("countries", Namespace = "http://services.worlddancesport.org/api")]
    [XmlRoot("countries", Namespace = "http://services.worlddancesport.org/api")]
    [MediaType("application/vnd.worlddancesport.countries", IsCollection = true)]
    public class ListOfCountry : List<Country>
    {}
}
