﻿// // TPS.net TPS8 Wdsf.Api.Client
// // ParticipantBase.cs
// // Last Changed: 2017/06/09
// // (c) 2017 Olav Gröhn / signalkontor GmbH

using System.Xml.Serialization;

namespace Wdsf.Api.Client.Models
{
    public class ParticipantBase
    {
        [XmlElement("link")]
        public virtual Link[] Link { get; set; }

        [XmlIgnore()]
        public int CompetitionId { get; set; }

        [XmlElement("id")]
        public int Id { get; set; }
    }
}
