﻿// // TPS.net TPS8 Wdsf.Api.Client
// // Link.cs
// // Last Changed: 2017/06/09
// // (c) 2017 Olav Gröhn / signalkontor GmbH

using System.Xml.Serialization;

namespace Wdsf.Api.Client.Models
{
    [XmlType("link")]
    public class Link
    {
        [XmlAttribute("href", Namespace = "http://www.w3.org/1999/xlink")]
        public string HRef { get; set; }

        [XmlAttribute("rel")]
        public string Rel { get; set; }

        [XmlAttribute("type")]
        public string Type { get; set; }
    }
}
