﻿// // TPS.net TPS8 Wdsf.Api.Client
// // AssemblyInfo.cs
// // Last Changed: 2017/06/09
// // (c) 2017 Olav Gröhn / signalkontor GmbH

using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Wdsf.Api.Client")]
[assembly: AssemblyDescription("WDSF API Client")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("JayKay-Designs S.C.")]
[assembly: AssemblyProduct("Wdsf.Api.Client")]
[assembly: AssemblyCopyright("Copyright © JayKay-Designs S.C. 2011 (LGPL3.0)")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(true)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("a706fc20-2fbd-4c71-9297-98b16a4611e6")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.*")]
