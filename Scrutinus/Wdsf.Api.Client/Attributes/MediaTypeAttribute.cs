﻿// // TPS.net TPS8 Wdsf.Api.Client
// // MediaTypeAttribute.cs
// // Last Changed: 2017/06/09
// // (c) 2017 Olav Gröhn / signalkontor GmbH

using System;

namespace Wdsf.Api.Client.Attributes
{
    public class MediaTypeAttribute : Attribute
    {
        public MediaTypeAttribute(string mediaType)
            : base()
        {
            this.MediaType = mediaType;
            this.IsCollection = false;
        }

        public bool IsCollection { get; set; }
        public string MediaType { get; set; }
    }
}
