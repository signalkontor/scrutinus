﻿// // TPS.net TPS8 Wdsf.Api.Client
// // RestAdapter.cs
// // Last Changed: 2017/06/09
// // (c) 2017 Olav Gröhn / signalkontor GmbH

using System;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Runtime.InteropServices;
using System.Security;
using System.Xml.Serialization;
using Wdsf.Api.Client.Exceptions;
using Wdsf.Api.Client.Models;

namespace Wdsf.Api.Client
{
    internal class RestAdapter : IDisposable
    {
        private readonly WebClient client = new WebClient();
        private readonly ICredentials credentials;
        private readonly object busyLock = new object();

        public RestAdapter(string username, SecureString password)
        {
            var strPointer = IntPtr.Zero;
            try
            {
                strPointer = Marshal.SecureStringToBSTR(password);
                this.credentials = new NetworkCredential(username, Marshal.PtrToStringBSTR(strPointer));
                // this.client.Proxy = new WebProxy("ddcrdtm200v1.corp.draeger.global:8080") { Credentials = new NetworkCredential("groehnol", "Sk2002hh2") };
            }
            finally
            {
                if (IntPtr.Zero != strPointer)
                {
                    Marshal.ZeroFreeBSTR(strPointer);
                    strPointer = IntPtr.Zero;
                }
            }

        }

        public bool IsAssigned { get; set; }
        public bool IsBusy { get; private set; }

        #region IDisposable Members

        public void Dispose()
        {
 	        this.client.Dispose();
        }

        #endregion

        /// <summary>
        /// Gets a resource of unknown type.
        /// </summary>
        /// <param name="resourceUri">The Uri to the resource.</param>
        /// <exception cref="UnknownMediaTypeException">The received resource type was not recognized.</exception>
        /// <exception cref="BusyException">The adapter is busy, use a new instance or check the IsBusy property.</exception>
        /// <exception cref="HttpWebException">A general failure.</exception>
        /// <returns>The resource.</returns>
        internal object Get(Uri resourceUri)
        {
            this.CheckAndSetBusy();

            var request = this.GetRequest(resourceUri);

            var response = this.GetResponse(request);
            this.IsBusy = false;
            var receivedType = TypeHelper.GetApiModelType(response.ContentType);
            if (receivedType == null)
            {
                throw new UnknownMediaTypeException(response.ContentType);
            }

            var serializer = new XmlSerializer(receivedType);
            var result =  serializer.Deserialize(response.GetResponseStream());
            response.Close();

            return result;
        }

        /// <summary>
        /// Gets a resource.
        /// </summary>
        /// <typeparam name="T">The expected resource type.</typeparam>
        /// <param name="resourceUri">The Uri to the resource.</param>
        /// <exception cref="UnknownMediaTypeException">The received resource type was not recognized.</exception>
        /// <exception cref="UnexpectedMediaTypeException">The received resource type was not expected.</exception>
        /// <exception cref="RestException">The API returned a message instead of a resource.</exception>
        /// <exception cref="BusyException">The adapter is busy, use a new instance or check the IsBusy property.</exception>
        /// <exception cref="HttpWebException">A general failure.</exception>
        /// <returns>The resource.</returns>
        public T Get<T>(Uri resourceUri) where T : class
        {
            this.CheckAndSetBusy();

            var request = this.GetRequest(resourceUri);
            
            var response = this.GetResponse(request);
            this.IsBusy = false;
            
            if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                throw new UnauthorizedException("GET", resourceUri);
            }

            this.CheckResourceType<T>(response);
            var result = this.ReadReponseBody<T>(response);
            response.Close();

            return result;
        }

        /// <summary>
        /// Updates a resource.
        /// </summary>
        /// <typeparam name="T">The resource type.</typeparam>
        /// <param name="resourceUri">The Uri to the resource.</param>
        /// <exception cref="UnknownMediaTypeException">The received message type was not recognized.</exception>
        /// <exception cref="UnexpectedMediaTypeException">The received message type was not expected.</exception>
        /// <exception cref="BusyException">The adapter is busy, use a new instance or check the IsBusy property.</exception>
        /// <exception cref="HttpWebException">A general failure.</exception>
        /// <returns>A status message.</returns>
        public StatusMessage Put<T>(Uri resourceUri, T model) where T : class
        {
            this.CheckAndSetBusy();

            var request = this.GetRequestForSending(resourceUri);
            request.Method = "PUT";
            request.ContentType = string.Format("{0}+xml", TypeHelper.GetHttpContentType(typeof(T)));
            this.WriteRequestBody<T>(model, request);

            var response = this.GetResponse(request);
            this.IsBusy = false;

            if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                throw new UnauthorizedException("PUT", resourceUri);
            }

            this.CheckResourceType<StatusMessage>(response);

            var message = this.ReadReponseBody<StatusMessage>(response);
            response.Close();

            return message;
        }

        /// <summary>
        /// Saves a new resource.
        /// </summary>
        /// <typeparam name="T">The resource type.</typeparam>
        /// <param name="resourceUri">The Uri to the resource.</param>
        /// <exception cref="UnknownMediaTypeException">The received message type was not recognized.</exception>
        /// <exception cref="UnexpectedMediaTypeException">The received message type was not expected.</exception>
        /// <exception cref="BusyException">The adapter is busy, use a new instance or check the IsBusy property.</exception>
        /// <exception cref="HttpWebException">A general failure.</exception>
        /// <returns>A status message containing the Uri to the new resource.</returns>
        public StatusMessage Post<T>(Uri resourceUri, T model) where T : class
        {
            this.CheckAndSetBusy();

            var request = this.GetRequestForSending(resourceUri);
            request.Method = "POST";
            request.ContentType = string.Format("{0}+xml", TypeHelper.GetHttpContentType(typeof(T)));
            this.WriteRequestBody<T>(model, request);

            var response = this.GetResponse(request);
            this.IsBusy = false;

            if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                throw new UnauthorizedException("POST", resourceUri);
            }

            this.CheckResourceType<StatusMessage>(response);
            var message = this.ReadReponseBody<StatusMessage>(response);
            response.Close();

            return message;
        }

        /// <summary>
        /// Deletes a resource.
        /// </summary>
        /// <param name="resourceUri">The Uri to the resource.</param>
        /// <exception cref="UnknownMediaTypeException">The received message type was not recognized.</exception>
        /// <exception cref="UnexpectedMediaTypeException">The received message type was not expected.</exception>
        /// <exception cref="BusyException">The adapter is busy, use a new instance or check the IsBusy property.</exception>
        /// <exception cref="HttpWebException">A general failure.</exception>
        /// <returns>A status message.</returns>
        public StatusMessage Delete(Uri resourceUri)
        {
            this.CheckAndSetBusy();

            var request = this.GetRequestForSending(resourceUri);
            request.Method = "DELETE";

            var response = this.GetResponse(request);
            this.IsBusy = false;

            if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                throw new UnauthorizedException("DELETE", resourceUri);
            }

            this.CheckResourceType<StatusMessage>(response);

            var message = this.ReadReponseBody<StatusMessage>(response);
            response.Close();

            return message;
        }

        /// <summary>
        /// Verifies if the received content-type corresponds to the requested one.
        /// </summary>
        /// <typeparam name="T">The type expected</typeparam>
        /// <param name="response">The response containing a content-type header</param>
        private void CheckResourceType<T>(HttpWebResponse response) where T : class
        {
            var receivedType = TypeHelper.GetApiModelType(response.ContentType);

            if (receivedType == null)
            {
                throw new UnknownMediaTypeException(response.ContentType);
            }

            if(receivedType != typeof(T))
            {
                if (receivedType == typeof(StatusMessage))
                {
                    var message = this.ReadReponseBody<StatusMessage>(response);
                    throw new RestException(message);
                }
                else
                {
                    throw new UnexpectedMediaTypeException(typeof(T), receivedType);
                }
            }
        }

        private void CheckAndSetBusy()
        {
            lock (this.busyLock)
            {
                if (this.IsBusy)
                {
                    throw new BusyException();
                }

                this.IsBusy = true;
            }
        }

        private HttpWebResponse GetResponse(HttpWebRequest request)
        {
            WebResponse response;
            try
            {
                response = request.GetResponse();
            }
            catch (WebException ex)
            {
                response = ex.Response;
            }

            return response as HttpWebResponse;
        }

        private T ReadReponseBody<T>(HttpWebResponse response) where T:class
        {
            var serializer = new XmlSerializer(typeof(T));
            var result = serializer.Deserialize(response.GetResponseStream()) as T;

            return result;
        }

        private HttpWebRequest GetRequest(Uri resourceUri)
        {
            var request = WebRequest.Create(resourceUri) as HttpWebRequest;

            request.Credentials = this.credentials;
            request.PreAuthenticate = true;
            request.Accept = "application/xml";
            request.UserAgent = "WDSF API Client";
            request.AutomaticDecompression = DecompressionMethods.GZip;

            return request;
        }

        private HttpWebRequest GetRequestForSending(Uri resourceUri)
        {
            var request = WebRequest.Create(resourceUri) as HttpWebRequest;

            request.Credentials = this.credentials;
            request.PreAuthenticate = true;
            request.Accept = "application/xml";
            request.UserAgent = "WDSF API Client";
            request.Headers.Add("Content-Encoding", "gzip");

            request.AutomaticDecompression = DecompressionMethods.GZip;

            return request;
        }

        private void WriteRequestBody<T>(T model, HttpWebRequest request) where T : class
        {
            var serializer = new XmlSerializer(typeof(T));
            var ns = new XmlSerializerNamespaces();
            ns.Add("xlink", "http://www.w3.org/1999/xlink");

            using (var data = new MemoryStream())
            {
                if (request.Headers["Content-Encoding"] == "gzip")
                {
                    using (var compressor = new GZipStream(data, CompressionMode.Compress, true))
                    {
                        serializer.Serialize(compressor, model, ns);
                    }
                }
                else
                {
                    serializer.Serialize(data, model, ns);
                }

                data.Position = 0;

                var requestStream = request.GetRequestStream();
                data.WriteTo(requestStream);
                requestStream.Flush();
                requestStream.Close();
            }

        }
    }
}
