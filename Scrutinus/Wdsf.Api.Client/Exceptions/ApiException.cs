﻿// // TPS.net TPS8 Wdsf.Api.Client
// // ApiException.cs
// // Last Changed: 2017/06/09
// // (c) 2017 Olav Gröhn / signalkontor GmbH

using System;

namespace Wdsf.Api.Client.Exceptions
{
    public class ApiException : Exception
    {
        public ApiException(Exception innerException)
            : base("The API call failed. See inner exception for further details.", innerException)
        {

        }
    }
}
