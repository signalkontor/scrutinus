﻿// // TPS.net TPS8 Wdsf.Api.Client
// // UnknownMediaTypeException.cs
// // Last Changed: 2017/06/09
// // (c) 2017 Olav Gröhn / signalkontor GmbH

using System;

namespace Wdsf.Api.Client.Exceptions
{
    public class UnknownMediaTypeException : Exception
    {
        public UnknownMediaTypeException(string receivedType) :
            base()
        {
            this.ReceivedType = receivedType;
        }

        public override string Message
        {
            get
            {
                return string.Format("An unknown media type was received: {0}", this.ReceivedType);
            }
        }

        public string ReceivedType { get; set; }
    }
}
