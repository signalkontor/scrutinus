﻿// // TPS.net TPS8 Wdsf.Api.Client
// // UnexpectedMediaTypeException.cs
// // Last Changed: 2017/06/09
// // (c) 2017 Olav Gröhn / signalkontor GmbH

using System;

namespace Wdsf.Api.Client.Exceptions
{
    public class UnexpectedMediaTypeException : Exception
    {
        public UnexpectedMediaTypeException(Type expectedType, Type receivedType) :
            base()
        {
            this.ReceivedType = receivedType;
            this.ExpectedType = expectedType;
        }

        public override string Message
        {
            get
            {
                return string.Format("Type \"{0}\" was expected but \"{1}\" was received", this.ExpectedType.FullName, this.ReceivedType.FullName);
            }
        }

        public Type ExpectedType { get; set; }
        public Type ReceivedType { get; set; }
    }
}
