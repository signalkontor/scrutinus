﻿// // TPS.net TPS8 Wdsf.Api.Client
// // UnauthorizedException.cs
// // Last Changed: 2017/06/09
// // (c) 2017 Olav Gröhn / signalkontor GmbH

using System;

namespace Wdsf.Api.Client.Exceptions
{
    public class UnauthorizedException : Exception
    {
        public UnauthorizedException(string method, Uri uri)
            : base(string.Format("Access to {0} {1} was denied.", method, uri))
        {
        }
    }
}