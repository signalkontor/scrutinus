﻿// // TPS.net TPS8 Wdsf.Api.Client
// // BusyException.cs
// // Last Changed: 2017/06/09
// // (c) 2017 Olav Gröhn / signalkontor GmbH

using System;

namespace Wdsf.Api.Client.Exceptions
{
    public class BusyException:Exception
    {
        public BusyException() :
            base("The REST client ist busy communicating with the API")
        {
        }
    }
}
