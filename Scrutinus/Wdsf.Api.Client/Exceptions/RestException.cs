﻿// // TPS.net TPS8 Wdsf.Api.Client
// // RestException.cs
// // Last Changed: 2017/06/09
// // (c) 2017 Olav Gröhn / signalkontor GmbH

using System;
using Wdsf.Api.Client.Models;

namespace Wdsf.Api.Client.Exceptions
{
    public class RestException : Exception
    {
        public RestException(StatusMessage message)
            : base(message.Message)
        {
            this.Code = message.Code;
            this.SubCode = message.SubCode;
        }

        public RestException(string message, int code, int subcode)
            : base(message)
        {
            this.Code = code;
            this.SubCode = subcode;
        }

        public int Code { get; set; }
        public int SubCode { get; set; }
    }
}
