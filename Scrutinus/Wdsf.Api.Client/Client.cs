﻿// // TPS.net TPS8 Wdsf.Api.Client
// // Client.cs
// // Last Changed: 2017/06/09
// // (c) 2017 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security;
using Wdsf.Api.Client.Exceptions;
using Wdsf.Api.Client.Interfaces;
using Wdsf.Api.Client.Models;

namespace Wdsf.Api.Client
{
    /// <summary>
    /// <para>Provides access to the WDSF API throught stongly typed models.</para>
    /// <para>This class is threadsafe and multiple request can be made to the same instance.</para>
    /// </summary>
    public class Client : IDisposable, IClient
    {
        private readonly object adapterLock = new object();
        private readonly string apiUriBase;
        private readonly SecureString password;
        private readonly string username;

        private List<RestAdapter> adapters;

        public Client(string username, string password, WdsfEndpoint endPoint) :
            this(username, MakeSecureString(password), endPoint)
        {
        }

        public Client(string username, SecureString password, WdsfEndpoint endPoint) :
            this(username, password, endPoint == WdsfEndpoint.Services ? "https://services.worlddancesport.org/API/1/" : "https://sandbox.worlddancesport.org/API/1/")
        {
        }

        public Client(string username, string password, string baseUrl) :
            this(username, MakeSecureString(password), baseUrl)
        {
        }

        public Client(string username, SecureString password, string baseUrl)
        {
            if (username == null)
            {
                throw new ArgumentNullException("username");
            }
            if (password == null)
            {
                throw new ArgumentNullException("password");
            }
            if (baseUrl == null)
            {
                throw new ArgumentNullException("baseUrl");
            }

            this.apiUriBase = baseUrl;

            this.username = username;
            this.password = password;

            this.adapters = new List<RestAdapter>() { new RestAdapter(username, password) };

#if DEBUG
            ServicePointManager.ServerCertificateValidationCallback = CertificatePolicy.ValidateSSLCertificate;
#endif
        }

        /// <summary>
        /// Contains the last message returned from the API
        /// </summary>
        public string LastApiMessage { get; private set; }


        public IList<Person> GetPersons(IDictionary<string, string> filter)
        {
            if (filter == null)
            {
                throw new ArgumentNullException("filter");
            }

            var query = string.Join("&", filter.Select(e => string.Format("{0}={1}", e.Key, e.Value)).ToArray());

            return this.GetResourceList<ListOfPerson, Person>(string.Format("person?{0}", query));
        }

        public PersonDetail GetPerson(int min)
        {
            return this.GetResource<PersonDetail>(string.Format("person/{0}", min));
        }

        public bool UpdatePerson(PersonDetail person)
        {
            if (person == null)
            {
                throw new ArgumentNullException("person");
            }

            return this.UpdateResource<PersonDetail>(person, string.Format("person/{0}", person.Min));
        }


        public IList<Competition> GetCompetitions(IDictionary<string, string> filter)
        {
            if (filter == null)
            {
                throw new ArgumentNullException("filter");
            }

            var query = string.Join("&", filter.Select(e => string.Format("{0}={1}", e.Key, e.Value)).ToArray());

            return this.GetResourceList<ListOfCompetition, Competition>(string.Format("competition?{0}", query));
        }

        public CompetitionDetail GetCompetition(int id)
        {
            return this.GetResource<CompetitionDetail>(string.Format("competition/{0}", id));
        }

        public bool UpdateCompetition(CompetitionDetail competition)
        {
            if (competition == null)
            {
                throw new ArgumentNullException("competition");
            }

            // not used for updating
            competition.Links = null;

            return this.UpdateResource<CompetitionDetail>(competition, string.Format("competition/{0}", competition.Id));
        }

        public ParticipantCoupleDetail GetCoupleParticipant(int id)
        {
            return this.GetResource<ParticipantCoupleDetail>(string.Format("participant/{0}", id));
        }

        public IList<ParticipantCouple> GetCoupleParticipants(int competitionId)
        {
            return this.GetResourceList<ListOfCoupleParticipant, ParticipantCouple>(
                string.Format("participant?competitionId={0}", competitionId)
            );
        }

        public bool UpdateCoupleParticipant(ParticipantCoupleDetail participant)
        {
            if (participant == null)
            {
                throw new ArgumentNullException("participant");
            }

            this.ClearLinks(participant);

            return this.UpdateResource<ParticipantCoupleDetail>(participant, string.Format("participant/{0}", participant.Id));
        }

        public Uri SaveCoupleParticipant(ParticipantCoupleDetail participant)
        {
            if (participant == null)
            {
                throw new ArgumentNullException("participant");
            }

            this.ClearLinks(participant);

            return this.SaveResource<ParticipantCoupleDetail>(participant, "participant");
        }

        public bool DeleteCoupleParticipant(int id)
        {
            return this.DeleteResource(string.Format("participant/{0}", id));
        }


        public ParticipantTeamDetail GetTeamParticipant(int id)
        {
            return this.GetResource<ParticipantTeamDetail>(string.Format("participant/{0}", id));
        }

        public IList<ParticipantTeam> GetTeamParticipants(int competitionId)
        {
            return this.GetResourceList<ListOfTeamParticipant, ParticipantTeam>(
                string.Format("participant?competitionId={0}", competitionId)
            );
        }

        public bool UpdateTeamParticipant(ParticipantTeamDetail participant)
        {
            if (participant == null)
            {
                throw new ArgumentNullException("participant");
            }

            this.ClearLinks(participant);

            return this.UpdateResource<ParticipantTeamDetail>(participant, string.Format("participant/{0}", participant.Id));
        }

        public Uri SaveTeamParticipant(ParticipantTeamDetail participant)
        {
            if (participant == null)
            {
                throw new ArgumentNullException("participant");
            }

            this.ClearLinks(participant);

            return this.SaveResource<ParticipantTeamDetail>(participant, "participant");
        }

        public bool DeleteTeamParticipant(int id)
        {
            return this.DeleteResource(string.Format("participant/{0}", id));
        }


        public ParticipantSingleDetail GetSingleParticipant(int id)
        {
            return this.GetResource<ParticipantSingleDetail>(string.Format("participant/{0}", id));
        }

        public IList<ParticipantSingle> GetSingleParticipants(int competitionId)
        {
            return this.GetResourceList<ListOfSingleParticipant, ParticipantSingle>(
                string.Format("participant?competitionId={0}", competitionId)
            );
        }

        public bool UpdateSingleParticipant(ParticipantSingleDetail participant)
        {
            if (participant == null)
            {
                throw new ArgumentNullException("participant");
            }

            this.ClearLinks(participant);

            return this.UpdateResource<ParticipantSingleDetail>(participant, string.Format("participant/{0}", participant.Id));
        }

        public Uri SaveSingleParticipant(ParticipantSingleDetail participant)
        {
            if (participant == null)
            {
                throw new ArgumentNullException("participant");
            }

            this.ClearLinks(participant);

            return this.SaveResource<ParticipantSingleDetail>(participant, "participant");
        }

        public bool DeleteSingleParticipant(int id)
        {
            return this.DeleteResource(string.Format("participant/{0}", id));
        }


        public OfficialDetail GetOfficial(int id)
        {
            return this.GetResource<OfficialDetail>(string.Format("official/{0}", id));
        }

        public IList<Official> GetOfficials(int competitionId)
        {
            return this.GetResourceList<ListOfOfficial, Official>(
                string.Format("official?competitionId={0}", competitionId)
            );
        }

        public bool UpdateOfficial(OfficialDetail official)
        {
            if (official == null)
            {
                throw new ArgumentNullException("official");
            }

            return this.UpdateResource<OfficialDetail>(official, string.Format("official/{0}", official.Id));
        }

        public Uri SaveOfficial(OfficialDetail official)
        {
            if (official == null)
            {
                throw new ArgumentNullException("official");
            }

            return this.SaveResource<OfficialDetail>(official, "official");
        }

        public bool DeleteOfficial(int id)
        {
            return this.DeleteResource(string.Format("official/{0}", id));
        }


        public IList<CoupleExport> GetCouples()
        {
            return this.GetResourceList<ListOfCoupleExport, CoupleExport>("couple/export");
        }

        public IList<Couple> GetCouples(IDictionary<string, string> filter)
        {
            if (filter == null)
            {
                throw new ArgumentNullException("filter");
            }

            var query = string.Join("&", filter.Select(e => string.Format("{0}={1}", e.Key, e.Value)).ToArray());

            return this.GetResourceList<ListOfCouple, Couple>(string.Format("couple?{0}", query));
        }

        public CoupleDetail GetCouple(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentException("Couple ID must be specified.", "id");
            }

            return this.GetResource<CoupleDetail>(string.Format("couple/{0}", id));
        }

        public bool UpdateCouple(CoupleDetail couple)
        {
            if (couple == null)
            {
                throw new ArgumentNullException("couple");
            }

            return this.UpdateResource<CoupleDetail>(couple, string.Format("couple/{0}", couple.Id));
        }

        public Uri SaveCouple(CoupleDetail couple)
        {
            if (couple == null)
            {
                throw new ArgumentNullException("couple");
            }

            return this.SaveResource<CoupleDetail>(couple, "couple");
        }


        public IList<Ranking> GetWorldRanking(string discipline, string ageGroup, string division)
        {
            return this.GetWorldRanking(discipline, ageGroup, division, DateTime.MinValue);
        }

        public IList<Ranking> GetWorldRanking(string discipline, string ageGroup, string division, DateTime date)
        {
            if (string.IsNullOrEmpty(discipline))
            {
                throw new ArgumentException("Discipline must be set.", "discipline");
            }

            if (string.IsNullOrEmpty(ageGroup))
            {
                throw new ArgumentException("Age group must be set.", "ageGroup");
            }

            if (string.IsNullOrEmpty(division))
            {
                throw new ArgumentException("Division must be set.", "division");
            }

            var resourceUri = 
                date != DateTime.MinValue ?
                string.Format("ranking?agegroup={0}&discipline={1}&division={2}&date={3:yyyy/MM/dd}", ageGroup, discipline, division, date) :
                string.Format("ranking?agegroup={0}&discipline={1}&division={2}", ageGroup, discipline, division);

            return this.GetResourceList<ListOfRanking, Ranking>(resourceUri);
        }

        public IList<AgeClass> GetAges()
        {
            return this.GetResourceList<ListOfAgeClass, AgeClass>("age");
        }

        public IList<Country> GetCountries()
        {
            return this.GetResourceList<ListOfCountry, Country>("country");
        }


        /// <summary>
        /// Gets a resource.
        /// </summary>
        /// <typeparam name="T">The expected resource type</typeparam>
        /// <param name="resourceUri">The resourceUri</param>
        /// <exception cref="ApiException">The request failed. See inner exception for details.</exception>
        /// <returns>The resource</returns>
        public T Get<T>(Uri resourceUri) where T:class
        {
            if (null == resourceUri)
            {
                throw new ArgumentNullException("resourceUri");
            }

            var adapter = this.GetAdapter();
            try
            {
                return adapter.Get<T>(resourceUri);
            }
            catch (Exception ex)
            {
                throw new ApiException(ex);
            }
            finally
            {
                this.ReleaseAdapter(adapter);
            }
        }

        /// <summary>
        /// Gets a resource where the expected type is not know.
        /// </summary>
        /// <param name="resourceUri">The resourceUri</param>
        /// <exception cref="ApiException">The request failed. See inner exception for details.</exception>
        /// <returns>The resource</returns>
        public object Get(Uri resourceUri)
        {
            if (null == resourceUri)
            {
                throw new ArgumentNullException("resourceUri");
            }

            var adapter = this.GetAdapter();
            try
            {
                return adapter.Get(resourceUri);
            }
            catch (Exception ex)
            {
                throw new ApiException(ex);
            }
            finally
            {
                this.ReleaseAdapter(adapter);
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion

        private static SecureString MakeSecureString(string val)
        {
            if (val == null)
            {
                return null;
            }

            var retVal = new SecureString();

            if (val.Length == 0)
            {
                return retVal;
            }

            for (var i = 0; i < val.Length; i++)
            {
                retVal.AppendChar(val[i]);
            }

            retVal.MakeReadOnly();
            return retVal;
        }

        private T Get<T>(string resourceUri) where T : class
        {
            if (null == resourceUri)
            {
                throw new ArgumentNullException("resourceUri");
            }

            return this.GetResource<T>(resourceUri);
        }

        private object Get(string resourceUri)
        {
            if (null == resourceUri)
            {
                throw new ArgumentNullException("resourceUri");
            }

            var adapter = this.GetAdapter();
            try
            {
                return adapter.Get(new Uri(this.apiUriBase + resourceUri));
            }
            catch (Exception ex)
            {
                throw new ApiException(ex);
            }
            finally
            {
                this.ReleaseAdapter(adapter);
            }

        }

        private T GetResource<T>(string resourceUri) where T : class
        {
            var adapter = this.GetAdapter();
            try
            {
                return adapter.Get<T>(new Uri(this.apiUriBase + resourceUri));
            }
            catch (Exception ex)
            {
                throw new ApiException(ex);
            }
            finally
            {
                this.ReleaseAdapter(adapter);
            }

        }

        private IList<TItem> GetResourceList<TContainer, TItem>(string resourceUri)
            where TContainer : class, IEnumerable<TItem>
            where TItem : class
        {

            var adapter = this.GetAdapter();
            try
            {
                var items = adapter.Get<TContainer>(new Uri(this.apiUriBase + resourceUri));
                return new List<TItem>(items);
            }
            catch (Exception ex)
            {
                throw new ApiException(ex);
            }
            finally
            {
                this.ReleaseAdapter(adapter);
            }

        }

        private bool UpdateResource<T>(T competition, string resourceUri) where T : class
        {
            StatusMessage message;
            var adapter = this.GetAdapter();
            try
            {
                message = adapter.Put<T>(new Uri(this.apiUriBase + resourceUri), competition);
            }
            catch (Exception ex)
            {
                throw new ApiException(ex);
            }
            finally
            {
                this.ReleaseAdapter(adapter);
            }


            this.LastApiMessage = message.Message;
            return message.Code == (int)HttpStatusCode.OK;
        }

        private Uri SaveResource<T>(T participant, string resourceUri) where T : class
        {
            StatusMessage message;
            var adapter = this.GetAdapter();
            try
            {
                message = adapter.Post<T>(new Uri(this.apiUriBase + resourceUri), participant);
            }
            catch (Exception ex)
            {
                throw new ApiException(ex);
            }
            finally
            {
                this.ReleaseAdapter(adapter);
            }


            if (message.Code != (int)HttpStatusCode.Created)
            {
                throw new ApiException(new RestException(message));
            }

            this.LastApiMessage = message.Message;
            return new Uri(message.Link.HRef);
        }

        private bool DeleteResource(string resourceUri)
        {
            StatusMessage message;
            var adapter = this.GetAdapter();
            try
            {
                message = adapter.Delete(new Uri(this.apiUriBase + resourceUri));
            }
            catch (Exception ex)
            {
                throw new ApiException(ex);
            }
            finally
            {
                this.ReleaseAdapter(adapter);
            }


            this.LastApiMessage = message.Message;
            return message.Code == (int)HttpStatusCode.OK;
        }

        /// <summary>
        /// Removes the Link property of all Score types to reduce the model's overall size.
        /// </summary>
        /// <param name="participant"></param>
        private void ClearLinks(ParticipantBaseDetail participant)
        {
            participant.Link = null;

            if (participant.Rounds == null)
            {
                return;
            }

            foreach (var round in participant.Rounds)
            {
                foreach (var dance in round.Dances)
                {
                    foreach (var score in dance.Scores)
                    {
                        score.Link = null;
                    }
                }
            }
        }

        private RestAdapter GetAdapter()
        {
            lock (this.adapterLock)
            {
                if (null == this.adapters)
                {
                    throw new ObjectDisposedException(this.GetType().FullName);
                }

                var adapter = this.adapters.FirstOrDefault(a => a.IsBusy == false && a.IsAssigned == false);
                if (adapter == null)
                {
                    adapter = new RestAdapter(this.username, this.password);
                    this.adapters.Add(adapter);
                }

                adapter.IsAssigned = true;
                return adapter;
            }
        }

        private void ReleaseAdapter(RestAdapter adapter)
        {
            lock (this.adapterLock)
            {
                adapter.IsAssigned = false;
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                lock (this.adapterLock)
                {
                    if (this.adapters != null)
                    {
                        foreach (var adapter in this.adapters)
                        {
                            adapter.Dispose();
                        }

                        this.adapters = null;
                    }
                }
            }
        }
    }
}
