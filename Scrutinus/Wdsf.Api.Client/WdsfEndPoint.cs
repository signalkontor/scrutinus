﻿// // TPS.net TPS8 Wdsf.Api.Client
// // WdsfEndPoint.cs
// // Last Changed: 2017/06/09
// // (c) 2017 Olav Gröhn / signalkontor GmbH
namespace Wdsf.Api.Client
{
    public enum WdsfEndpoint
    {
        /// <summary>
        /// Use this endpoint for testing
        /// </summary>
        Sandbox,
        /// <summary>
        /// This endpoint goes to the live website
        /// </summary>
        Services
    }
}
