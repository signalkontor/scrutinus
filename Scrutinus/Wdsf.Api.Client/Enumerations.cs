﻿// // TPS.net TPS8 Wdsf.Api.Client
// // Enumerations.cs
// // Last Changed: 2017/06/09
// // (c) 2017 Olav Gröhn / signalkontor GmbH

namespace Wdsf.Api.Client
{
    public static class Gender
    {
        /// <summary>
        /// 
        /// </summary>
        public const string Male = "Male";

        /// <summary>
        /// 
        /// </summary>
        public const string Female = "Female";
    }
}
