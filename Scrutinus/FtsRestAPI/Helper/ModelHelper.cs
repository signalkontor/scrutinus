﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using DataModel.Models;
using FtsRestAPI.DTO;

namespace FtsRestAPI.Helper
{
    public static class ModelHelper
    {
        public static IEnumerable<Event> GetEvents(CompetitionsList competitionList)
        {
            return competitionList.Competitions.Select(c => new Event()
            {
                DateFrom = DateTime.Parse(c.Date_start.date),
                DateTo = DateTime.Parse(c.Date_end.date),
                Organizer = c.Organizer.FirstOrDefault()?.full_name,
                Place = c.city,
                Title = c.name
            });
        }
    }
}
