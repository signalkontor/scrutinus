﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using FtsRestAPI.DTO;
using General;

namespace FtsRestAPI
{
    public class FtsRestClient
    {
        private HttpClient httpClient;

        private string user;

        private string password;

        private string baseUrl = "https://baza.fts-taniec.pl";

        public FtsRestClient(string user, string password)
        {
            this.user = user;
            this.password = password;
        }

        public async Task<AthletList> GetActiveAthlets()
        {
            return await this.GetResource<AthletList>(this.baseUrl + "/rest/athlete/active");
        }

        public async Task<CompetitionsList> GetCurrentCompetitions()
        {
            return await this.GetResource<CompetitionsList>(this.baseUrl + "/rest/competitionList/current");
        }

        public async Task<CompetitionDetails> GetCompetitionDetails (int id)
        {
            return await this.GetResource<CompetitionDetails>(this.baseUrl + "/rest/competition/id/" + id);
        }

        private async Task<T> GetResource<T>(string url)
        {
            var credentials = new NetworkCredential(this.user, this.password);
            var handler = new HttpClientHandler { Credentials = credentials };

            using (var httpClient = new HttpClient(handler))
            {
                var content = await httpClient.GetStringAsync(url);
                return JsonHelper.DeserializeData<T>(content);
            }  
        }
    }
}
