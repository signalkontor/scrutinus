﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FtsRestAPI.DTO
{

    public class CompetitionDetails
    {
        public string type { get; set; }
        public CompetitionDetailsContext Context { get; set; }
        public Competition Competition { get; set; }
        public Club Club { get; set; }
        public Regionalfederation RegionalFederation { get; set; }
        public Venue[] Venue { get; set; }
        public Organizer[] Organizer { get; set; }
        public Block[] Blocks { get; set; }
        public Judges Judges { get; set; }
        public Scrutineer[] Scrutineers { get; set; }
        public Category[] Categories { get; set; }
    }

    public class CompetitionDetailsContext
    {
        public string id { get; set; }
    }

    public class Competition
    {
        public int id { get; set; }
        public string name { get; set; }
        public string city { get; set; }
        public Date_Start Date_start { get; set; }
        public Date_End Date_end { get; set; }
    }



    public class CompetitionsList
    {
        public string type { get; set; }
        public Context Context { get; set; }
        public CompetitionSimple[] Competitions { get; set; }
    }


    public class Datefrom
    {
        public string date { get; set; }
        public int timezone_type { get; set; }
        public string timezone { get; set; }
    }

    public class Dateto
    {
        public string date { get; set; }
        public int timezone_type { get; set; }
        public string timezone { get; set; }
    }

    public class CompetitionSimple
    {
        public int id { get; set; }
        public string name { get; set; }
        public string city { get; set; }
        public Date_Start Date_start { get; set; }
        public Date_End Date_end { get; set; }
        public Club Club { get; set; }
        public Regionalfederation RegionalFederation { get; set; }
        public object[] Venue { get; set; }
        public Organizer[] Organizer { get; set; }
    }

    public class Date_Start
    {
        public string date { get; set; }
        public int timezone_type { get; set; }
        public string timezone { get; set; }
    }

    public class Date_End
    {
        public string date { get; set; }
        public int timezone_type { get; set; }
        public string timezone { get; set; }
    }

    public class Club
    {
        public int id { get; set; }
        public string short_name { get; set; }
        public string full_name { get; set; }
        public string city { get; set; }
    }

    public class Regionalfederation
    {
        public int id { get; set; }
        public string short_name { get; set; }
        public string full_name { get; set; }
        public object[] TPS_settings { get; set; }
    }

    public class Organizer
    {
        public int id { get; set; }
        public string short_name { get; set; }
        public string full_name { get; set; }
    }


    public class OfficialsList
    {
        public string type { get; set; }
        public Context Context { get; set; }
        public Judge[] Judge { get; set; }
    }


    public class Judge
    {
        public int id { get; set; }
        public int? min { get; set; }
        public string last_name { get; set; }
        public string first_name { get; set; }
        public string city { get; set; }
        public LicencesOfficial Licences { get; set; }
    }

    public class LicencesOfficial
    {
        public bool judge_pl { get; set; }
        public bool judge_wdsf { get; set; }
        public bool judge_chairperson_pl { get; set; }
        public string[] Category { get; set; }
        public Valid_Date valid_date { get; set; }
    }

    public class Valid_Date
    {
        public string date { get; set; }
        public int timezone_type { get; set; }
        public string timezone { get; set; }
    }


    public class AthletList
    {
        public string type { get; set; }
        public Context Context { get; set; }
        public Athlete[] Athlete { get; set; }
    }

    public class Context
    {
        public string type { get; set; }
        public Datefrom dateFrom { get; set; }
        public Dateto dateTo { get; set; }
        public int licenceForYear { get; set; }
    }

    public class Athlete
    {
        public int id { get; set; }
        public int? min { get; set; }
        public string last_name { get; set; }
        public string first_name { get; set; }
        public Birth_Date birth_date { get; set; }
        public Class1 Class { get; set; }
        public Club Club { get; set; }
        public Regionalfederation RegionalFederation { get; set; }
        public Licences Licences { get; set; }
    }

    public class Birth_Date
    {
        public string date { get; set; }
        public int timezone_type { get; set; }
        public string timezone { get; set; }
    }

    public class Class1
    {
        public string standard { get; set; }
        public string latin { get; set; }
    }

    public class Licences
    {
        public string[] Valid_classes { get; set; }
        public object valid_date { get; set; }
    }

}
