﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FtsRestAPI.DTO
{

    public class FtsEvent
    {
        public string type { get; set; }
        public Context Context { get; set; }
        public Competition Competition { get; set; }
        public Club Club { get; set; }
        public Regionalfederation RegionalFederation { get; set; }
        public Venue[] Venue { get; set; }
        public Organizer[] Organizer { get; set; }
        public Block[] Blocks { get; set; }
        public Judges Judges { get; set; }
        public Scrutineer[] Scrutineers { get; set; }
        public Category[] Categories { get; set; }
    }



    public class Judges
    {
        public Polish[] Polish { get; set; }
        public object[] International { get; set; }
    }

    public class Polish
    {
        public int id { get; set; }
        public int? MIN { get; set; }
        public string last_name { get; set; }
        public string first_name { get; set; }
        public string city { get; set; }
        public string licence { get; set; }
        public Regionalfederation1 RegionalFederation { get; set; }
    }

    public class Regionalfederation1
    {
        public string id { get; set; }
        public string short_name { get; set; }
        public string full_name { get; set; }
    }

    public class Venue
    {
        public int id { get; set; }
        public string name { get; set; }
        public string address { get; set; }
    }


    public class Block
    {
        public int id { get; set; }
        public int number { get; set; }
        public string name { get; set; }
        public Start_Time start_time { get; set; }
    }

    public class Start_Time
    {
        public string date { get; set; }
        public int timezone_type { get; set; }
        public string timezone { get; set; }
    }

    public class Scrutineer
    {
        public int id { get; set; }
        public object MIN { get; set; }
        public string last_name { get; set; }
        public string first_name { get; set; }
        public string city { get; set; }
        public string scrutineer_licence { get; set; }
        public Regionalfederation2 RegionalFederation { get; set; }
    }

    public class Regionalfederation2
    {
        public string id { get; set; }
        public string short_name { get; set; }
        public string full_name { get; set; }
    }

    public class Category
    {
        public int id { get; set; }
        public string name { get; set; }
        public Start_Time1 Start_time { get; set; }
        public Style Style { get; set; }
        public Class Class { get; set; }
        public Age_Category Age_category { get; set; }
        public Block1 Block { get; set; }
        public Entry[] Entries { get; set; }
    }

    public class Start_Time1
    {
        public string date { get; set; }
        public int timezone_type { get; set; }
        public string timezone { get; set; }
    }

    public class Style
    {
        public int id { get; set; }
        public string name { get; set; }
        public string short_name { get; set; }
    }

    public class Class
    {
        public int id { get; set; }
        public string _class { get; set; }
    }

    public class Age_Category
    {
        public int id { get; set; }
        public int min_age { get; set; }
        public int min_age_open_competition { get; set; }
        public object max_age { get; set; }
    }

    public class Block1
    {
        public int id { get; set; }
        public int number { get; set; }
        public string name { get; set; }
        public Start_Time2 start_time { get; set; }
    }

    public class Start_Time2
    {
        public string date { get; set; }
        public int timezone_type { get; set; }
        public string timezone { get; set; }
    }

    public class Entry
    {
        public int id { get; set; }
        public int category_id { get; set; }
        public int start_number { get; set; }
        public int couple_pl_id { get; set; }
        public string country { get; set; }
        public Club1 Club { get; set; }
        public Male Male { get; set; }
        public Female Female { get; set; }
    }

    public class Club1
    {
        public int? id { get; set; }
        public string name { get; set; }
        public string city { get; set; }
    }

    public class Male
    {
        public int? pl_id { get; set; }
        public int? min { get; set; }
        public string last_name { get; set; }
        public string first_name { get; set; }
    }

    public class Female
    {
        public int? pl_id { get; set; }
        public int? min { get; set; }
        public string last_name { get; set; }
        public string first_name { get; set; }
    }

}
