﻿// // TPS.net TPS8 Scrutinus
// // MainWindow.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Messaging;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Navigation;
using System.Windows.Threading;
using System.Xml;
using com.signalkontor.XD_Server;
using DataModel;
using DataModel.Messages;
using DataModel.Migrations;
using DataModel.Models;
using Exceptionless;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using General;
using General.Interfaces;
using Helpers;
using Helpers.Extensions;
using mobileControl.Model;
using MahApps.Metro;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.SimpleChildWindow;
using MobileControlLib.EjudgeMessages;
using MobileControlLib.Helper;
using NLog;
using Scrutinus.Behaviours;
using Scrutinus.Dialogs;
using Scrutinus.Dialogs.DialogViewModels;
using Scrutinus.Localization;
using Scrutinus.Navigation;
using Scrutinus.Pages;
using Scrutinus.ViewModels;
using License = DataModel.Models.License;
using LogLevel = NLog.LogLevel;
using Printing = Scrutinus.Pages.Printing;

namespace Scrutinus
{
	/// <summary>
	///     Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : MetroWindow, IReportProgress, IStatusReporter, INotifyPropertyChanged
	{
	    #region Constructors and Destructors

	    /// <summary>
		///     Initializes a new instance of the <see cref="MainWindow" /> class.
		/// </summary>
		public MainWindow()
		{
		    this.logger.Log(LogLevel.Debug, "Initializing");

            ExceptionlessClient.Default.Register(false);

		    var attributes =
		        Assembly.GetExecutingAssembly()
		            .GetCustomAttributes(false)
		            .Where(t => t is AssemblyFileVersionAttribute)
		            .Cast<AssemblyFileVersionAttribute>();

		    if (attributes.Any())
		    {
#if DTV
                this.Version = "Version DTV 2019 / " + attributes.First().Version;
#else
                this.Version = "Version Int. 2019 / " + attributes.First().Version;
#endif
            }

            try
		    {
		        this.logger.Log(LogLevel.Debug, "Initializing Components");
                this.InitializeComponent();
		        this.logger.Log(LogLevel.Debug, "Initializing Application");
                this.InitializeApplication();

		        this.logger.Log(LogLevel.Debug, "Initialized");
            }
		    catch (Exception ex)
		    {
		        this.logger.Log(LogLevel.Error, ex);
		    }
		}

#endregion

#region Public Events

	    public event PropertyChangedEventHandler PropertyChanged;

#endregion

	    private void ShowHelp()
	    {
	        var url = "http://www.google.com";

	        if (this.ContentFrame.Content is DependencyObject)
	        {
	            var dependencyObject = this.ContentFrame.Content as DependencyObject;
	            var help = HelpFileExtension.GetHelpFile(dependencyObject);

	            while (help == null && dependencyObject != null)
	            {
	                dependencyObject = dependencyObject.GetParentObject();
                    help = HelpFileExtension.GetHelpFile(dependencyObject);
                }

	            var applicationPath = Environment.CurrentDirectory.Replace("\\", "/");
	            var language = Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName;
	            if (language != "de" && language != "en")
	            {
	                language = "en";
	            }

	            url = $"file:///{applicationPath}/HelpFiles/{language}/{help}";
	        }

	        var dlg = new HelpDialog(url);
            dlg.CloseButtonCommand = new RelayCommand<object>(this.CloseHelp);
	        dlg.CloseButtonCommandParameter = dlg;

            this.ShowChildWindow(dlg);
	    }

	    private void CloseHelp(object parameter)
	    {
	        
	    }

	    private void DialogWindowFrame_OnNavigated(object sender, NavigationEventArgs e)
	    {
	        var frame = sender as Frame;
	        if (frame == null)
	        {
	            return;
	        }

	        if (frame.JournalOwnership != JournalOwnership.OwnsJournal)
	        {
	            return;
	        }

            var entry = frame.RemoveBackEntry();
            while (entry != null)
            {
                entry = frame.RemoveBackEntry();
            }
	    }

	    private void OnHelpCommandExecuted(object sender, ExecutedRoutedEventArgs e)
	    {
	        this.ShowHelp();
	    }

#region Static Fields

	    /// <summary>
		///     The move back command.
		/// </summary>
		public static RoutedCommand MoveBackCommand = new RoutedCommand();

	    /// <summary>
		///     The move next command.
		/// </summary>
		public static RoutedCommand MoveNextCommand = new RoutedCommand();

	    /// <summary>
		///     The open fly out command
		/// </summary>
		public static RoutedCommand OpenFlyOutCommand = new RoutedCommand();

	    /// <summary>
		///     The print command.
		/// </summary>
		public static RoutedCommand PrintCommand = new RoutedCommand();

	    public static RoutedCommand HelpCommand = new RoutedCommand();

	    /// <summary>
		///     The _context.
		/// </summary>
		private static ScrutinusContext context;

#endregion

#region Fields

	    /// <summary>
		///     The _view model.
		/// </summary>
		private MainWindowViewModel viewModel;

	    private Thread backupThread;

        private Action<object> dialogClosingAction;

	    private bool initialized;

	    private bool markingInputEnabled;

	    private ProgressDialogController progressDialog;

	    private bool selectInitialRuleDialogOpen;

	    private MessageBoxResult showMessageResult;

	    private AutoResetEvent waitHandle;

	    private ServerCore serverCore;

	    private MessageQueue _inQueue;

	    private readonly Logger logger = LogManager.GetCurrentClassLogger();

#endregion

#region Public Properties

	    public static License License { get; set; }

	    public static bool IsInNetworkMode { get; set; }

	    /// <summary>
		///     Gets or sets the main window handle.
		/// </summary>
		public static MainWindow MainWindowHandle { get; private set; }

	    /// <summary>
		///     Gets the view model.
		/// </summary>
		public MainWindowViewModel ViewModel
		{
			get
			{
				return this.viewModel;
			}
		}

	    public ServerCore ServerCore { get { return this.serverCore;} }

        public string Version { get; set; }

#if DTV
        public bool IsDtv => true;
#else
        public bool IsDtv => false;
#endif

        #endregion

        #region Public Methods and Operators

        public void AddCompetitionToNavigationList(Competition competition)
		{
			var context = new ScrutinusContext();

            var currentPage = this.viewModel.ActivePage;

            this.viewModel.AddCompetition(
				new CompetitionElement
					{
						Competition = competition,
						CurrentRound =
							competition.CurrentRound != null
								? context.Rounds.Single(r => r.Id == competition.CurrentRound.Id)
								: null,
					});

			if (this.viewModel.ActivePage != currentPage)
			{
				this.viewModel.ActivePage = currentPage;
			}
		}

	    public void ForceUpdateNavigationList()
	    {
	        this.ViewModel.UpdateNavigationList();
	    }

	    public void CloseDialog()
		{
			this.Dispatcher.Invoke(
				new Action(
					() =>
						{
							this.DialogWindowPanel.Visibility = Visibility.Collapsed;

							if (this.dialogClosingAction != null)
							{
								this.dialogClosingAction.Invoke(this.DialogWindowFrame.Content);
							}
						}));
		}

	    public void DeleteCompetitionFromNavigationList(Competition competition)
		{
			var navigationElement =
				this.viewModel.Competitions.SingleOrDefault(c => c.Competition.Id == competition.Id);
			if (navigationElement != null)
			{
				this.viewModel.Competitions.Remove(navigationElement);
			}
		}

	    public void NextState()
		{
			var obj = (CompetitionElement)this.CompetitionNavigationListHamburgerMenu.SelectedItem;

			if (obj == null)
			{
			    if (this.ContentFrame.Content is PrintingGeneral)
			    {
                    // we return to the competition
                    this.GotoActiveCompetition();
			        return;
			    }

				// Check the active page:
				if (this.viewModel.ActivePage is ScrutinusPage)
				{
					var scrutinusPage = this.viewModel.ActivePage as ScrutinusPage;
					if (scrutinusPage.Competition != null)
					{
						this.NextState(scrutinusPage.Competition.Id);
						return;
					}
				}

				this.ShowMessage(LocalizationService.Resolve(() => Text.NoActiveCompetition));

				return;
			}

			this.NextState(obj.Competition.Id);
		}

	    /// <summary>
		///     The next state.
		/// </summary>
		/// <param name="competitionId">
		///     The competition id.
		/// </param>
		public void NextState(int competitionId)
		{
		    if (MainWindowHandle.ContentFrame.Content is PrintingGeneral)
		    {
		        var printing = MainWindowHandle.ContentFrame.Content as PrintingGeneral;
                var currentCompetition =
                this.ViewModel.Competitions.SingleOrDefault(c => c.Competition.Id == printing.Competition.Id);

		        if (currentCompetition != null)
		        {
		            this.ContentFrame.Content = currentCompetition.CurrentPage;
                    return;
		        }
            }

			var competition =
				this.ViewModel.Competitions.SingleOrDefault(c => c.Competition.Id == competitionId);

			if (competition == null)
			{
				return;
			}

			var oldPage = competition.CurrentPage as ScrutinusPage;
			if (oldPage != null)
			{
				if (oldPage.CanMoveNext())
				{
				    try
				    {
				        oldPage.OnNext();
				    }
				    catch (AccessViolationException ex)
				    {
				        Debug.WriteLine(ex);   
				    }
				    competition.CurrentPage = null;
 
				    oldPage.Dispose();
				}
				else
				{
					return;
				}
			}

			context = new ScrutinusContext();
			competition.Competition = context.Competitions.Single(c => c.Id == competition.Competition.Id);
			competition.CurrentRound = competition.Competition.CurrentRound;

			if (competition.CurrentRound == null)
			{
				this.ShowMessage(LocalizationService.Resolve(() => Text.NoCurrentRoundToMoveForward));
				return;
			}

			if (competition.CurrentRound.Competition.State == CompetitionState.Finished)
			{
				competition.CurrentPage = new CompetitionResult(competition.Competition.Id, competition.CurrentRound.Id);
				this.viewModel.ActivePage = competition.CurrentPage;
                this.ViewModel.UpdateNavigationList();
				return;
			}

			switch (competition.CurrentRound.State)
			{
				case CompetitionState.Startlist:
				case CompetitionState.QualifiedList:
					competition.CurrentRound.State = CompetitionState.RoundDrawing;
					competition.CurrentPage = new DefineHeat(competition.Competition.Id, competition.CurrentRound.Id);
					this.viewModel.ActivePage = competition.CurrentPage;
					break;
				case CompetitionState.RoundDrawing:
					if (!competition.CurrentRound.RoundType.HasDrawing)
					{
						competition.CurrentRound.State = CompetitionState.Printing;
					    if (context.PrintReportDefinitions.Any())
					    {
                            competition.CurrentPage = new PrintingPrintManager(competition.Competition.Id, competition.CurrentRound.Id);
                        }
					    else
					    {
					        competition.CurrentPage = new Printing(competition.Competition.Id, competition.CurrentRound.Id);
					    }
					}
					else
					{
						competition.CurrentRound.State = CompetitionState.RoundDrawingResult;
						competition.CurrentPage = new RoundDrawingResult(
							competition.Competition.Id,
							competition.CurrentRound.Id);
					}

					this.viewModel.ActivePage = competition.CurrentPage;
					break;
				case CompetitionState.RoundDrawingResult:
					competition.CurrentRound.State = CompetitionState.Printing;
                    if (context.PrintReportDefinitions.Any())
                    {
                        competition.CurrentPage = new PrintingPrintManager(competition.Competition.Id, competition.CurrentRound.Id);
                    }
                    else
                    {
                        competition.CurrentPage = new Printing(competition.Competition.Id, competition.CurrentRound.Id);
                    }
                    this.viewModel.ActivePage = competition.CurrentPage;
					break;
				case CompetitionState.Printing:
					competition.CurrentRound.State = CompetitionState.EnterMarking;
					// this.ShowWaitingSpinner("Bitte warten", "Die Ansicht wird aufgebaut");
					competition.CurrentPage = EnterMarkingPageFactory.MarkingPage(competition);
					// this.RemoveWaitingSpinner();
					this.viewModel.ActivePage = competition.CurrentPage;
					break;
				case CompetitionState.EnterMarking:

					NavigationHelper.GetNextAfterEnterMarking(context, competition);

					this.viewModel.ActivePage = competition.CurrentPage;
					break;
				case CompetitionState.SelectQualified:
					competition.CurrentRound.State = CompetitionState.QualifiedList;
					competition.CurrentPage = new QualifiedList(competition.Competition.Id, competition.CurrentRound.Id);
					this.viewModel.ActivePage = competition.CurrentPage;
					break;
			}

			// we do not need the old page anymore so we can tell it we left the page:
			if (oldPage != null && this.viewModel.ActivePage != oldPage)
			{
				oldPage.OnLeft();
			}

			// Save States
			var round = context.Rounds.Single(r => r.Id == competition.CurrentRound.Id);
			round.State = competition.CurrentRound.State;
			context.SaveChanges();

			// Invalidate the last page in viewmodel
			// so the user can not go back with the left button.
			this.ViewModel.InvalidateLastPage();

			this.ViewModel.RaiseNewState();

		    this.Dispatcher.BeginInvoke(new Action(() => this.ViewModel.UpdateNavigationList()), DispatcherPriority.ApplicationIdle);
        }

	    public void OpenEvent(string file, bool updateNavigation, string ruleset = null)
		{
			Settings.Default.DataSource = file;
			Settings.Default.Save();

            IsInNetworkMode = !file.ToLower().EndsWith(".sdf");

			context = ruleset != null ? new ScrutinusContext(file, ruleset) : new ScrutinusContext(file, null);

            this.Dispatcher.Invoke(() => this.ContentFrame.Content = null);

		    try
		    {
		        MigrateDatabase.Migrate(context);
		    }
		    catch (Exception ex)
		    {
		        this.logger.Log(LogLevel.Error, ex);
		    }

		    if (this.viewModel.NancyServer != null)
		    {
		        this.viewModel.NancyServer.SetNewDatabaseConnection(file);
		    }

			// Just to make sure the database is created:
			try
			{
				this.ViewModel.MarkingInputEnabled = context.GetParameter("MarkingInputEnables", true);
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error opening Database: " + ex.Message);
				return;
			}

		    if (updateNavigation)
		    {
                this.UpdateNavigationList();
            }
			
			if (Settings.Default.UseSQLCompact)
			{
				var fileInfo = new FileInfo(file);
				this.viewModel.CurrentOpenDatabase = fileInfo.Name;
			}
			else
			{
				var start = Settings.Default.DataSource.IndexOf("Initial Catalog=");
				if (start > -1)
				{
					var end = Settings.Default.DataSource.IndexOf(";", start);
					if (end > -1)
					{
						this.viewModel.CurrentOpenDatabase = Settings.Default.DataSource.Substring(
							start + 16,
							end - start - 16);
					}
					else
					{
						this.viewModel.CurrentOpenDatabase = "SQl Server Database";
					}
				}
				else
				{
					this.viewModel.CurrentOpenDatabase = "SQl Server Database";
				}
			}
			
			try
			{
				this.viewModel.Event = context.Events.FirstOrDefault();
				
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error loading SQL Data: " + ex.Message);
			    if (ex.InnerException != null)
			    {
			        MessageBox.Show(ex.InnerException.Message + " " + ex.InnerException.StackTrace);
			    }
			}
		}

	    public void RemoveStatusReport()
		{
			this.ReportStatus(string.Empty, 0, 1000);
		}


	    /// <summary>
		///     The report status.
		/// </summary>
		/// <param name="message">
		///     The message.
		/// </param>
		/// <param name="progress">
		///     The progress.
		/// </param>
		/// <param name="max">
		///     The max.
		/// </param>
		public void ReportStatus(string message, int progress, int max)
		{
			if (this.Dispatcher.CheckAccess())
			{
				this.UpdateStatusMessage(message, progress, max);
			}
			else
			{
				this.Dispatcher.Invoke(new Action(() => this.UpdateStatusMessage(message, progress, max)));
			}
		}

	    /// <summary>
		///     The set active page.
		/// </summary>
		/// <param name="newPage">
		///     The new page.
		/// </param>
		public void SetActivePage(Page newPage)
		{
			this.viewModel.ActivePage = newPage;
		}

	    public void SetApplicationTheme(string accent, string theme)
		{
			ThemeManager.ChangeAppStyle(
				Application.Current,
				ThemeManager.GetAccent(accent),
				ThemeManager.GetAppTheme(theme));
		}

	    public void ShowDialog(Page content, Action<object> onCloseAction, double planedHeight, double planedWidth)
		{
			this.dialogClosingAction = onCloseAction;
			this.DialogWindowFrame.Content = content;
			this.DialogWindowPanel.Visibility = Visibility.Visible;

			// content.Height = planedHeight;
			// content.Width = planedWidth;
		}

	    /// <summary>
		///     The show message.
		/// </summary>
		/// <param name="message">
		///     The message.
		/// </param>
		/// <returns>
		///     The <see cref="MessageBoxResult" />.
		/// </returns>
		public MessageDialogResult ShowMessage(string message)
		{
			return this.ShowMessage(message, string.Empty, MessageDialogStyle.Affirmative);
        }

	    public Task<MessageDialogResult> ShowMessageAsync(string message)
        {
            return this.ShowMessageAsync(message, string.Empty, MessageDialogStyle.Affirmative);
        }

	    /// <summary>
        /// Shows the message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="caption">The caption.</param>
        /// <returns></returns>
        public MessageDialogResult ShowMessage(string message, string caption)
		{
			return this.ShowMessage(message, caption, MessageDialogStyle.Affirmative);
		}

	    public Task<MessageDialogResult> ShowMessageAsync(string message, string caption)
        {
            return this.ShowMessageAsync(message, caption, MessageDialogStyle.Affirmative);
        }

	    public MessageDialogResult ShowMessage(string message, string title, MessageDialogStyle dialogStyle)
        {
            var task = this.ShowMessageAsync(message, title, dialogStyle);

            task.WaitWithoutBlockingUI(this.Dispatcher);

            return task.Result;
        }

	    private Task<MessageDialogResult> ShowMessageAsync(string message, string caption, MessageDialogStyle dialogStyle)
        {
            Task<MessageDialogResult> returnValue = null;

            this.Dispatcher.Invoke(
                new Action(
                    () =>
                    {
                        var metroWindow = this;
                        metroWindow.MetroDialogOptions.ColorScheme = MetroDialogColorScheme.Accented;

                        returnValue = metroWindow.ShowMessageAsync(
                            caption,
                            message,
                            dialogStyle,
                            metroWindow.MetroDialogOptions);
                    }));

            return returnValue;
        }


	    public void ShowChildWindow(FrameworkElement content, string title = "")
        {
            ChildWindow childWindow;

            if (content is ChildWindow)
            {
                childWindow = (ChildWindow)content;
            }
            else
            {
                childWindow = new ChildWindow() { Content = content, IsModal = true, Title = title, };
            }
            // opens a cool child window
            this.ShowChildWindowAsync(childWindow);
        }

	    /// <summary>
        ///     The update navigation list.
        /// </summary>
        public void UpdateNavigationList()
		{
			this.CreateNavigateList();
		}

	    public void ShowPrintDialog()
	    {
	        this.BtnPrint_OnClick(null, null);
	    }

#endregion

#region Methods

	    internal void GotoActiveCompetition()
		{
			if (this.viewModel.SelectedCompetition != null)
			{
				var competition = this.viewModel.SelectedCompetition;
			    this.ContentFrame.Content = competition.CurrentPage;
			}
		}

	    /// <summary>
		///     The goto last page.
		/// </summary>
		internal void GotoLastPage()
		{
			if (this.viewModel.LastPage != null)
			{
				this.viewModel.ActivePage = this.viewModel.LastPage;
			}
		}


	    private void StartMessageQueue()
        {
            try
            {
                this._inQueue = new MessageQueue(@".\private$\TPS.eJudge.in");
                this._inQueue.ReceiveCompleted += this.QueueReceiveCompleted;
                this._inQueue.Formatter = new XmlMessageFormatter(new[] { typeof(TMQeJSMsg) });
                this._inQueue.BeginReceive();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }


	    private void QueueReceiveCompleted(object sender, ReceiveCompletedEventArgs e)
        {
            try
            {
                var msg = this._inQueue.EndReceive(e.AsyncResult);

                if (msg.Body is TMQeJSMsg)
                {
                    var tps = (TMQeJSMsg)msg.Body;

                    this.Dispatcher.Invoke(
                        DispatcherPriority.Normal,
                        (Action)(() => this.ProcessMessage(tps))
                        );

                }
            }
            catch (MessageQueueException)
            {

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }

            this._inQueue.BeginReceive();
        }

	    private void ProcessMessage(TMQeJSMsg tps)
        {
            var dev = this.ViewModel.Devices.SingleOrDefault(d => d.DeviceId == tps.Sender);

            if (dev == null)
            {
                dev = this.CreateNewDevice(tps);
            }
            else
            {
                dev.TimeStamp = tps.TimeStamp;
            }

            if (tps.TargetId == 500)
            {
                Console.WriteLine(tps.MsgData);
            }

            if (tps.TargetId != 2)
            {
                dev.CompetitionLoaded = tps.DoCmd;
            }
                
            
            if (tps.DoCmd != null)
            {
                var message = MessageHelper.EjudgeMessageFactory(tps);

                if (message != null)
                {
                    Messenger.Default.Send(message);

                    if (!message.IsHandled)
                    {
                        this.HandleEjudgeMessage(message);
                    }
                }
            }

            switch (tps.TargetId)
            {
                case 1:
                    DeviceHelper.UpdateDeviceState(dev, tps.MsgData);
                    dev.IsOnline = true;
                    break;
                case 2:
                    dev.IsOnline = true;
                    break;
                case 3:
                    dev.IsOnline = false;
                    break;

            }
        }

	    private Device CreateNewDevice(TMQeJSMsg tps)
	    {
	        Device dev;
	        dev = new Device
	                  {
	                      BatteryLevel = 0,
	                      DeviceId = tps.Sender,
	                      DeviceState = DeviceStates.StartNoData,
	                      IsOnline = true,
	                      IsCharging = false,
	                      CompetitionLoaded = "",
	                      TimeStamp = tps.TimeStamp
	                  };

	        this.ViewModel.Devices.Add(dev);
	        context.Devices.Add(dev);
	        context.SaveChanges();

	        Messenger.Default.Send(new NewDeviceMessage(tps) { Device = dev });

            return dev;
	    }

	    private void HandleEjudgeMessage(EjudgeMessage message)
        {
	        using (var context = new ScrutinusContext())
	        {
	            if (message is MarkMessage)
	            {
	                var markMessage = message as MarkMessage;

                    var round = context.Rounds.ToList().First(r => r.EjudgeName == markMessage.RoundCode);
                    var eJudgeData = round.EjudgeData.FirstOrDefault(e => e.DeviceId == message.Sender);

	                if (eJudgeData == null)
	                {
	                    return;
	                }

                    var participant = round.Qualifieds.First(q => q.Participant.Number == markMessage.Number).Participant;
                    var dance = round.DancesInRound.ToList()[markMessage.DanceIndex - 1].Dance;

                    if (markMessage.Mark == "X")
	                {
	                   
	                    var mark = new Marking(participant, round, dance, eJudgeData.Judge)
	                                   {
	                                       Created = DateTime.Now,
	                                       Mark = 1,
	                                   };
	                    context.Markings.Add(mark);
	                }
	                else
                    {
                        var mark =
                            context.Markings.FirstOrDefault(
                                m =>
                                m.Judge.Id == eJudgeData.Judge.Id && m.Dance.Id == dance.Id
                                && m.Participant.Id == participant.Id && m.Round.Id == round.Id);

                        if (mark != null)
                        {
                            context.Markings.Remove(mark);
                        }
                    }

	                context.SaveChanges();
	            }

                if (message is SignatureMessage)
                {
                    var signatureMessage = (SignatureMessage)message;
                    var round = context.Rounds.ToList().First(r => r.EjudgeName == message.RoundCode);

                    var ejudgeData = round.EjudgeData.FirstOrDefault(e => e.DeviceId == message.Sender);

                    if (ejudgeData == null)
                    {
                        return;
                    }

                    foreach (var tuple in signatureMessage.Coordinates)
                    {
                        ejudgeData.SignatureCoordinates.Add(new Coordinate() { X1 = tuple.Item1, Y1 = tuple.Item2, X2 = tuple.Item3, Y2 = tuple.Item4 });
                    }

                    context.SaveChanges();
                }

	            if (message is CompleteMarkingMessage)
	            {
	                var markingMessage = (CompleteMarkingMessage)message;
                    var round = context.Rounds.ToList().First(r => r.EjudgeName == message.RoundCode);
                    var eJudgeData = round.EjudgeData.FirstOrDefault(j => j.DeviceId == message.Sender);
	                if (eJudgeData == null)
	                {
	                    return;
	                }

                    // Wew remove all judgements that we might have until now:
                    var judgements = context.Markings.Where(m => m.Round.Id == round.Id && m.Judge.Id == eJudgeData.Judge.Id).ToList();
	                context.Markings.RemoveRange(judgements);

	                var dances = round.DancesInRound.OrderBy(o => o.SortOrder).Select(s => s.Dance).ToList();
                    foreach (var number in markingMessage.Marks.Keys)
                    {
                        var participant = round.Qualifieds.First(q => q.Participant.Number == number).Participant;
                        var list = markingMessage.Marks[number];

                        for (var index = 1; index < list.Count; index++)
                        {
                            if (list[index - 1] == "X")
                            {
                                var mark = new Marking(participant, round, dances[index - 1], eJudgeData.Judge)
                                               {
                                                   Created = DateTime.Now,
                                                   Mark = 1,
                                               };
                                context.Markings.Add(mark);
                            }
                            else
                            {
                                if (!string.IsNullOrWhiteSpace(list[index - 1]))
                                {
                                    var mark = new Marking(participant, round, dances[index - 1], eJudgeData.Judge)
                                    {
                                        Created = DateTime.Now,
                                        Mark = int.Parse(list[index - 1]),
                                    };
                                    context.Markings.Add(mark);
                                }
                            }
                        }
                    }
	                context.SaveChanges();
	            }

                if (message is DeviceStateMessage)
                {
                    var deviceStateMessage = message as DeviceStateMessage;
                    var device = this.ViewModel.Devices.First(d => d.DeviceId == deviceStateMessage.Sender);
                    device.Heat = deviceStateMessage.Heat;
                    device.Marks = deviceStateMessage.Marks;

                    if (device.Round == null)
                    {
                        device.Round = context.Rounds.AsNoTracking().ToList().FirstOrDefault(r => r.EjudgeName == message.RoundCode);
                    }

                    if (device.Round != null && deviceStateMessage.DanceIndex > 0)
                    {
                        var round = context.Rounds.Single(r => r.Id == device.Round.Id);
                        device.Dance = round.DancesInRound.OrderBy(d => d.SortOrder).ToList()[deviceStateMessage.DanceIndex - 1].Dance;    
                    }
                }
            }            
        }


	    /// <summary>
        ///     Determines whether the user has write permissions to the desination folder.
        /// </summary>
        /// <param name="destDir">The dest dir.</param>
        /// <returns></returns>
        private static bool HasFolderWritePermission(string destDir)
		{
			try
			{
				using (
					var fs = File.Create(
						Path.Combine(destDir, Path.GetRandomFileName()),
						1,
						FileOptions.DeleteOnClose))
				{
				}
				return true;
			}
			catch
			{
				return false;
			}
		}


	    /// <summary>
		///     The btn back_ on click.
		/// </summary>
		/// <param name="sender">
		///     The sender.
		/// </param>
		/// <param name="e">
		///     The e.
		/// </param>
		private void BtnBack_OnClick(object sender, RoutedEventArgs e)
		{
			var obj = (CompetitionElement)this.CompetitionNavigationListHamburgerMenu.SelectedItem;

			if (obj == null)
			{
			    if (this.ViewModel.ActivePage is ScrutinusPage)
			    {
			        var page = (ScrutinusPage)this.ViewModel.ActivePage;
			        if (page.Competition != null)
			        {
			            obj = this.ViewModel.Competitions.FirstOrDefault(c => c.Competition.Id == page.Competition.Id);
			            if (obj == null)
			            {
			                return;
			            }
			        }
                }
			}

			this.PreviousState(obj);
		}

	    /// <summary>
		///     The btn competitions_ on click.
		/// </summary>
		/// <param name="sender">
		///     The sender.
		/// </param>
		/// <param name="e">
		///     The e.
		/// </param>
		private void BtnCompetitions_OnClick(object sender, RoutedEventArgs e)
		{
			this.CompetitionNavigationListHamburgerMenu.SelectedItem = null;
			this.viewModel.ActivePage = new Competitions();
		}

	    /// <summary>
		///     The btn couples_ on click.
		/// </summary>
		/// <param name="sender">
		///     The sender.
		/// </param>
		/// <param name="e">
		///     The e.
		/// </param>
		private void BtnCouples_OnClick(object sender, RoutedEventArgs e)
		{
			this.CompetitionNavigationListHamburgerMenu.SelectedItem = null;
			this.viewModel.ActivePage = new EditParticipants(0, 0);
		}

	    private void BtnExport_OnClick(object sender, RoutedEventArgs e)
		{
			this.CompetitionNavigationListHamburgerMenu.SelectedItem = null;
			this.viewModel.ActivePage = new EventExport();
		}

	    /// <summary>
		///     The btn forward_ on click.
		/// </summary>
		/// <param name="sender">
		///     The sender.
		/// </param>
		/// <param name="e">
		///     The e.
		/// </param>
		private void BtnForward_OnClick(object sender, RoutedEventArgs e)
		{
			var obj = (CompetitionElement)this.CompetitionNavigationListHamburgerMenu.SelectedItem;

			if (obj == null)
			{
				return;
			}

			this.NextState(obj.Competition.Id);
		}

	    /// <summary>
		///     The btn import_ on click.
		/// </summary>
		/// <param name="sender">
		///     The sender.
		/// </param>
		/// <param name="e">
		///     The e.
		/// </param>
		private void BtnImport_OnClick(object sender, RoutedEventArgs e)
		{
			this.CompetitionNavigationListHamburgerMenu.SelectedItem = null;
			this.viewModel.ActivePage = new ImportData();
		}

	    private void BtnNavigateLastPage_Click(object sender, RoutedEventArgs e)
		{
			this.GotoLastPage();
		}

	    /// <summary>
		///     The btn officials_ on click.
		/// </summary>
		/// <param name="sender">
		///     The sender.
		/// </param>
		/// <param name="e">
		///     The e.
		/// </param>
		private void BtnOfficials_OnClick(object sender, RoutedEventArgs e)
		{
			this.CompetitionNavigationListHamburgerMenu.SelectedItem = null;
			this.viewModel.ActivePage = new EditOfficials(0, 0);
		}

	    /// <summary>
		///     The btn open event_ on click.
		/// </summary>
		/// <param name="sender">
		///     The sender.
		/// </param>
		/// <param name="e">
		///     The e.
		/// </param>
		private void BtnOpenEvent_OnClick(object sender, RoutedEventArgs e)
		{
			var dlg = new OpenEventDialog();

			this.ShowDialog(dlg, null, 300, 300);
		}

	    /// <summary>
		///     The btn print_ on click.
		/// </summary>
		/// <param name="sender">
		///     The sender.
		/// </param>
		/// <param name="e">
		///     The e.
		/// </param>
		private void BtnPrint_OnClick(object sender, RoutedEventArgs e)
		{
			var obj = (CompetitionElement)this.CompetitionNavigationListHamburgerMenu.SelectedItem;

			if (obj == null)
			{
				this.viewModel.ActivePage = new PrintingGeneral(0, 0);
				return;
			}

			this.viewModel.ActivePage = new PrintingGeneral(
				obj.Competition.Id,
				obj.CurrentRound != null ? obj.CurrentRound.Id : 0);
		}

	    /// <summary>
		///     The btn settings_ on click.
		/// </summary>
		/// <param name="sender">
		///     The sender.
		/// </param>
		/// <param name="e">
		///     The e.
		/// </param>
		private void BtnSettings_OnClick(object sender, RoutedEventArgs e)
		{
			this.CompetitionNavigationListHamburgerMenu.SelectedItem = null;
			this.viewModel.ActivePage = new ProgramSettings();
		}


	    /// <summary>
		///     The competition navigation list_ selection changed.
		/// </summary>
		/// <param name="sender">
		///     The sender.
		/// </param>
		/// <param name="e">
		///     The e.
		/// </param>
		private void CompetitionNavigationList_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
		    if (this.ViewModel.DoNotRaiseSelectionChangedEvent)
		    {
		        return;
		    }

			// We would show here the new page
			if (sender is ListView)
			{
				var list = (ListView)sender;
				var competitionElement = (CompetitionElement)list.SelectedItem;

				var context = new ScrutinusContext();

				this.viewModel.CurrentCompetition = competitionElement;

				if (competitionElement == null)
				{
					return;
				}

				var competition = context.Competitions.Single(c => c.Id == competitionElement.Competition.Id);

				if (this.CompetitionHasChanged(competition, competitionElement))
				{
					competitionElement.CurrentPage = null;
					competitionElement.Competition.State = competition.State;
					competitionElement.CurrentRound = competition.CurrentRound;
				}

				this.viewModel.FlyoutIsOpen = false;

				if (competitionElement.CurrentPage == null)
				{
					if (competitionElement.CurrentRound == null)
					{
						competitionElement.CurrentPage = new StartList(competitionElement.Competition.Id, 0);
						this.viewModel.ActivePage = competitionElement.CurrentPage;
						return;
					}

					if (competitionElement.Competition.State == CompetitionState.Finished)
					{
						competitionElement.CurrentPage = new CompetitionResult(
							competitionElement.Competition.Id,
							competitionElement.CurrentRound.Id);
					}
					else
					{
						switch (competitionElement.CurrentRound.State)
						{
							case CompetitionState.CheckIn:
							case CompetitionState.Closed:
							case CompetitionState.Startlist:
								competitionElement.CurrentPage = new StartList(
									competitionElement.Competition.Id,
									competitionElement.CurrentRound.Id);
								break;
							case CompetitionState.RoundDrawing:
								competitionElement.CurrentPage = new DefineHeat(
									competitionElement.Competition.Id,
									competitionElement.CurrentRound.Id);
								break;
							case CompetitionState.RoundDrawingResult:
								competitionElement.CurrentPage =
									new RoundDrawingResult(
										competitionElement.Competition.Id,
										competitionElement.CurrentRound.Id);
								break;
							case CompetitionState.Printing:
								competitionElement.CurrentPage = new PrintingPrintManager(
									competitionElement.Competition.Id,
									competitionElement.CurrentRound.Id);
								break;
							case CompetitionState.EnterMarking:
								competitionElement.CurrentPage = EnterMarkingPageFactory.MarkingPage(competitionElement);
								break;
							case CompetitionState.SelectQualified:
								competitionElement.CurrentPage = new SelectQualified(
									competitionElement.Competition.Id,
									competitionElement.CurrentRound.Id);
								break;
							case CompetitionState.QualifiedList:
								competitionElement.CurrentPage = new QualifiedList(
									competitionElement.Competition.Id,
									competitionElement.CurrentRound.Id);
								break;
                            case CompetitionState.Finished:
						        var round = competition.Rounds.FirstOrDefault(r => r.RoundType.IsFinal);
						        if (round != null)
						        {
                                    competitionElement.CurrentPage = new CompetitionResult(competitionElement.Competition.Id, round.Id);
                                }
                                
						        break;
						}
					}
				}

				this.viewModel.ActivePage = competitionElement.CurrentPage;
			}
		}

	    private bool CompetitionHasChanged(Competition competition, CompetitionElement competitionElement)
	    {
	        if (competition.CurrentRound == null)
	        {
                // negative logic, true when changed
	            return competition.State != competitionElement.Competition.State;
	        }

	        if (competitionElement.Competition.CurrentRound == null)
	        {
	            return true;
	        }

	        return competition.CurrentRound.Id != competitionElement.CurrentRound.Id
	               || competition.CurrentRound.State != competitionElement.CurrentRound.State
	               || competition.State != competitionElement.Competition.State;

	    }

	    private void ContentFrame_OnPreviewMouseUp(object sender, MouseButtonEventArgs e)
		{
			if (this.viewModel.FlyoutIsOpen)
			{
				this.viewModel.FlyoutIsOpen = false;
			}
		}

	    /// <summary>
		///     The create navigate list.
		/// </summary>
		private void CreateNavigateList()
		{
			try
			{
				if (Settings.Default.DataSource == string.Empty)
				{
					return;
				}

                var context = new ScrutinusContext(Settings.Default.DataSource, null);
                this.viewModel.Competitions.Clear();
			    this.viewModel.ClearCompetitions();

			    var comps = context.Competitions.ToList();
			    foreach (var competition in comps)
			    {
			        this.AddCompetitionToNavigationList(competition);
			    }
			    
			}
			catch (Exception)
			{
				//MessageBox.Show(
				//    LocalizationService.Resolve(() => Text.ErrorCouldNotCreateNavigationList) + " ("
				//    + Settings.Default.DataSource + ")");
			}
		}

	    private void CreateNewSqlServerDatabase(string connectionString)
		{
			try
			{
				var xmlRuleFile = Settings.Default.SQLServerInitialRuleFile;

				var context = new ScrutinusContext(connectionString, xmlRuleFile);

				// List<CompetitionRule> competitionRules = context.CompetitionRules.ToList();
			}
			catch (Exception ex)
			{
				MessageBox.Show(LocalizationService.Resolve(() => Text.ErrorCreatingSQLServerDatabase) + " " + ex.Message);
			}
		}

	    private void InitializeApplication()
	    {
            var locale = Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName;

            if (!Settings.Default.SettingsAreInitialized)
            {
                Settings.Default.Upgrade();
                Settings.Default.SettingsAreInitialized = true;
                Settings.Default.Save();
            }

            // Todo: replace by a generated list of supported locales.
            if (locale != "de" && locale != "en")
            {
                LocalizationService.ApplicationLanguage = new CultureInfo("en");
                if (string.IsNullOrEmpty(Settings.Default.LanguageLocale))
                {
                    Settings.Default.LanguageLocale = "en";
                    Settings.Default.Save();
                }
            }
            else
            {
                LocalizationService.ApplicationLanguage = Thread.CurrentThread.CurrentCulture;
                if (string.IsNullOrEmpty(Settings.Default.LanguageLocale))
                {
                    Settings.Default.LanguageLocale = locale;
                    Settings.Default.Save();
                }
            }

            AppDomain.CurrentDomain.UnhandledException += (sender, args) =>
            {
                var exception = args.ExceptionObject as Exception;

                if (exception != null)
                {
                    while (exception != null)
                    {
                        // MessageBox.Show(string.Format("{0}\r\n{1}", exception.Message, exception.StackTrace));
                        // ScrutinusContext.WriteLogEntry(exception);
                        logger.Error(exception);
                        exception = exception.InnerException;                    
                    }
                }
            };

            Application.Current.DispatcherUnhandledException += (sender, args) =>
            {
                var exception = args.Exception;
                while (exception != null)
                {
                    logger.Error(exception);
                    // ScrutinusContext.WriteLogEntry(exception);
                    exception = exception.InnerException;
                }

                args.Handled = true;
            };



            this.Title = "TPS.net 2016";
#if DEBUG
            this.Title += " DEBUG";
#endif
            MainWindowHandle = this;

            License = new License
            {
                Ejudge = true,
                TabletServer = true,
                CheckIn = true,
                SQLServer = true,
                LicenseHolder = "Demoversion",
                MaxCompetitions = 10,
                MaxCouples = 1000,
                ValidFrom = DateTime.Now,
                ValidUntil = DateTime.Now.AddDays(10)
            };

            if (File.Exists("license.lic"))
            {
                this.ReadLicenseFile();
            }

            if (Settings.Default.UseSQLCompact == false)
            {
                var connectionString = Settings.Default.DataSource;

                this.CreateNewSqlServerDatabase(connectionString);
            }
            else
            {
                if (!File.Exists(Settings.Default.DataSource))
                {
                    Settings.Default.DataSource = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
                                                  + "\\TPS.net\\TPS8_Sample_Database.sdf";
                    var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\TPS.net";
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }

                    var context = new ScrutinusContext(
                        Settings.Default.DataSource,
                        "DataModel.InitialData.InitialContent.xml");

                    if (!context.Events.Any())
                    {
                        var eventObj = new Event { Title = "Demo Event", DateFrom = DateTime.Now };
                        context.Events.Add(eventObj);
                        context.SaveChanges();
                    }
                }
                else
                {
                    ScrutinusContext.CurrentDataSource = Settings.Default.DataSource;
                }
            }

            this.viewModel = new MainWindowViewModel();

            this.viewModel.PropertyChanged += this.ViewModel_PropertyChanged;

            this.DataContext = this.viewModel;

            logger.Info("Application initialized");
        }

	    private void InitializeDatabase()
		{
            logger.Info("Initializing Database");
			var locale = !string.IsNullOrEmpty(Settings.Default.LanguageLocale)
								? Settings.Default.LanguageLocale
								: Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName;

			LocalizationService.ApplicationLanguage = new CultureInfo(locale);

			LanguageProperty.OverrideMetadata(
				typeof(FrameworkElement),
				new FrameworkPropertyMetadata(XmlLanguage.GetLanguage(CultureInfo.CurrentCulture.IetfLanguageTag)));

			if (Settings.Default.UseSQLCompact)
			{
				if (File.Exists(Settings.Default.DataSource))
				{
					var fileInfo = new FileInfo(Settings.Default.DataSource);
					this.viewModel.CurrentOpenDatabase = fileInfo.Name;
				}
				else
				{
					this.BtnOpenEvent_OnClick(null, null);
				}
			}
			else
			{
				this.viewModel.CurrentOpenDatabase = this.ParseConnectionString();
			}

			try
			{
				this.initialized = true;
			}
			catch (Exception)
			{
				// MessageBox.Show(LocalizationService.Resolve(() => Text.ErrorCouldNotCreateNavigationList) + ": " + ex.Message);
			}

			if (this.initialized)
			{
				Messenger.Default.Register<InvalidateNavigationMessage>(this, this.HandleInvalidateNavigationMessage);

			    if (Settings.Default.EnableAutoBackup)
			    {
			        this.backupThread = new Thread(this.BackupTimerThread);
			        this.backupThread.IsBackground = true;
			        this.backupThread.Start();
			    }

			    this.Dispatcher.Invoke(new Action(() => { this.viewModel.ActivePage = new ProgramSettings(); }));

			}

            logger.Info("Database Initialized");
		}

	    private string ParseConnectionString()
        {
            var start = Settings.Default.DataSource.IndexOf("Initial Catalog");
            if (start < 0)
            {
                return "Unknown Data Source";
            }
            start += 16;

            var end = Settings.Default.DataSource.IndexOf(";", start);
            if (end < 0)
            {
                return "Unknown Data Source";
            }

            var database = Settings.Default.DataSource.Substring(start, end - start);

            start = Settings.Default.DataSource.IndexOf("Data Source=");
            if (start < 0)
            {
                return database;
            }

            start += 12;

            end = Settings.Default.DataSource.IndexOf(";", start);
            if (end < 0)
            {
                return database;
            }

            var server = Settings.Default.DataSource.Substring(start, end - start);

            return server + "\\" + database;
        }

	    /// <summary>
        ///     The move back.
        /// </summary>
        private void MoveBack()
		{
			this.BtnBack_OnClick(null, null);
		}

	    /// <summary>
		///     The move back_ can execute.
		/// </summary>
		/// <param name="sender">
		///     The sender.
		/// </param>
		/// <param name="e">
		///     The e.
		/// </param>
		private void MoveBack_CanExecute(object sender, CanExecuteRoutedEventArgs e)
		{
			e.CanExecute = true;
			e.Handled = true;
		}

	    /// <summary>
		///     The move back_ executed.
		/// </summary>
		/// <param name="sender">
		///     The sender.
		/// </param>
		/// <param name="e">
		///     The e.
		/// </param>
		private void MoveBack_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			this.MoveBack();
			e.Handled = true;
		}

	    /// <summary>
		///     The move next_ can execute.
		/// </summary>
		/// <param name="sender">
		///     The sender.
		/// </param>
		/// <param name="e">
		///     The e.
		/// </param>
		private void MoveNext_CanExecute(object sender, CanExecuteRoutedEventArgs e)
		{
			e.CanExecute = true;
			e.Handled = true;
		}

	    /// <summary>
		///     The move next_ executed.
		/// </summary>
		/// <param name="sender">
		///     The sender.
		/// </param>
		/// <param name="e">
		///     The e.
		/// </param>
		private void MoveNext_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			this.NextState();
			e.Handled = true;
		}

	    private void OnContentRendered(object sender, EventArgs e)
	    {

            if (!this.initialized)
            {
                var metroDialogSettings = new MetroDialogSettings()
                                              {
                                                  ColorScheme = MetroDialogColorScheme.Inverted
                                              };

                var progressManager = this.ShowProgressAsync(LocalizationService.Resolve(() => Text.InitializingApplication), LocalizationService.Resolve(() => Text.InitializingDatabase), false, metroDialogSettings).WaitWithoutBlockingUI<ProgressDialogController>(this.Dispatcher);

                var stopwatch = new Stopwatch();
                stopwatch.Start();

                Task.Factory.StartNew(
                    () =>
                        {
                            try
                            {
                                string error = null;
                                this.initialized = true;
                                this.OpenEvent(Settings.Default.DataSource, false);

                                this.InitializeDatabase();

                                if (Settings.Default.UseInternalXdServer)
                                {
                                    progressManager.SetMessage(LocalizationService.Resolve(() => Text.StartingXdServer));

                                    this.InitializeXdServer();
                                }

                                if (Settings.Default.AutoStartNancy)
                                {
                                    progressManager.SetMessage(LocalizationService.Resolve(() => Text.StartingWebServer));
                                    try
                                    {
                                        NancyControlViewModel.StartNancyServer(false);
                                    }
                                    catch (Exception ex)
                                    {
                                        error = "Could not start web server: " + ex.Message;
                                    }
                                    
                                }

                                this.ViewModel.CheckForUpdates();

                                var closing = progressManager.CloseAsync();

                                if (error != null)
                                {
                                    while (!closing.IsCompleted)
                                    {
                                        Thread.Sleep(300);
                                    }
                                    this.ShowMessageAsync(error);
                                }

                                if (context == null)
                                {
                                    // Something went wrong with the context ...
                                    return;
                                }

                                this.Dispatcher.Invoke(new Action(() => { this.CreateNavigateList(); }));
#if DTV
                                // Check for valid data:
                                var eventData = context.Events.FirstOrDefault();
                                if (eventData != null && !eventData.DtvDownloadDataValid && eventData.DateFrom.HasValue)
                                {
                                    // only show this message the day before or same day of event:
                                    var dateDiff = DateTime.Today.AddDays(+2);
                                    if (dateDiff > eventData.DateFrom.Value)
                                    {
                                        MainWindowHandle.ShowMessage(
                                            "Die Aufstiegsdaten sind nicht gültig, bitte führen Sie einen Download vom DTV Portal durch.",
                                            "Warnung!", MessageDialogStyle.Affirmative);
                                    }
                                }

#endif
                            }
                            catch (Exception ex)
                            {
                                Trace.WriteLine(ex.Message);
                            }
                        });

                this.viewModel.ActivePage = null;

                stopwatch.Stop();
                Debug.WriteLine("WindowActivated: {0}", stopwatch.ElapsedMilliseconds);
            }


		}

	    private void InitializeXdServer()
        {
	        if (License.Ejudge && Settings.Default.UseInternalXdServer)
            {
                try
                {
                    if (!MessageQueue.Exists(@".\private$\tps.ejudge.in"))
                    {
                        MessageQueue.Create(@".\private$\tps.ejudge.in", false);
                    }

                    if (!MessageQueue.Exists(@".\private$\tps.ejudge.out"))
                    {
                        MessageQueue.Create(@".\private$\tps.ejudge.out", false);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Could not initialize Microsoft Message Queuing, is it installed on this PC? " + ex.Message);
                }
                try
                {
                    this.StartMessageQueue();

                    var path = Settings.Default.XdServerDevicesFile;

                    if (path.StartsWith("%Documents%"))
                    {
                        path = path.Replace("%Documents%", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments));
                    }

                    this.serverCore = new ServerCore(Settings.Default.XdServerPort, 
                                                     Settings.Default.XdServerConfigPort,
                                                     Settings.Default.XdServerAccessPoint,
                                                     path,
                                                     Settings.Default.XdServerInQueue,
                                                     Settings.Default.XdServerOutQueue);

                    this.serverCore.NewLogMessageEvent += (sender, message) => Messenger.Default.Send(new EjudgeLogMessage(message));
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Could not initialize XD Server Services: " + ex.Message);
                }
            }
        }

	    private void OnOpenFlyoutCommandExecuted(object sender, ExecutedRoutedEventArgs e)
		{
			this.ViewModel.ToggleFlyOutCommand.Execute(null);
		}

	    /// <summary>
		///     The previous state.
		/// </summary>
		/// <param name="competition">
		///     The competition.
		/// </param>
		private void PreviousState(CompetitionElement competition)
		{
			if (competition?.CurrentRound == null)
			{
				return; // nothing to go back ,,,
			}

            if (this.ContentFrame.Content is PrintingGeneral)
            {
                // we return to the competition
                this.GotoActiveCompetition();
                return;
            }

            var oldPage = competition.CurrentPage;

			context = new ScrutinusContext();

			competition.CurrentRound = context.Rounds.Single(r => r.Id == competition.CurrentRound.Id);
			competition.Competition = context.Competitions.Single(c => c.Id == competition.Competition.Id);

			switch (competition.CurrentRound.State)
			{
				case 0:
					if (competition.Competition.State == CompetitionState.Finished)
					{
						competition.Competition.State = CompetitionState.RoundDrawing;
						competition.CurrentRound.State = CompetitionState.RoundDrawing;
						competition.CurrentPage = new DefineHeat(
							competition.Competition.Id,
							competition.CurrentRound.Id);
						this.viewModel.ActivePage = competition.CurrentPage;
					}
					break;
				case CompetitionState.SelectQualified:
					competition.CurrentRound.State = CompetitionState.EnterMarking;
					competition.CurrentPage = EnterMarkingPageFactory.MarkingPage(competition);
					this.viewModel.ActivePage = competition.CurrentPage;
					break;
				case CompetitionState.EnterMarking:
				case CompetitionState.Printing:
				case CompetitionState.RoundDrawingResult:
					competition.CurrentRound.State = CompetitionState.RoundDrawing;
					competition.CurrentPage = new DefineHeat(competition.Competition.Id, competition.CurrentRound.Id);
					this.viewModel.ActivePage = competition.CurrentPage;
					break;
				case CompetitionState.QualifiedList:
				case CompetitionState.RoundDrawing:

					// Go back one round to the Select Qualified Page
					competition.CurrentRound.State = CompetitionState.SelectQualified;
						// once we come back, show qualified
					var round =
						context.Rounds.SingleOrDefault(
							r =>
							r.Competition.Id == competition.Competition.Id
							&& r.Number == competition.CurrentRound.Number - 1);
					if (round == null)
					{
						competition.CurrentRound.State = 0;
						competition.CurrentPage = new StartList(competition.Competition.Id, 0);
						competition.CurrentRound = null;
						competition.Competition.CurrentRound = null;
					}
					else
					{
						competition.CurrentRound = round;
						competition.CurrentRound.State = CompetitionState.SelectQualified;
						competition.Competition.CurrentRound = round;
						competition.CurrentPage = new SelectQualified(
							competition.Competition.Id,
							competition.CurrentRound.Id);
					}

					this.viewModel.ActivePage = competition.CurrentPage;
					break;
				case CompetitionState.Finished:
					competition.Competition.State = CompetitionState.RoundDrawing;
					competition.CurrentRound.State = CompetitionState.RoundDrawing;
					competition.CurrentPage = new DefineHeat(competition.Competition.Id, competition.CurrentRound.Id);
					this.viewModel.ActivePage = competition.CurrentPage;
					break;
			}

			if (oldPage != competition.CurrentPage && oldPage is ScrutinusPage)
			{
				((ScrutinusPage)oldPage).OnLeft();
			}

			// Save States. Load object again just to make sure we have the right context
			if (competition.CurrentRound != null)
			{
				var r2 = context.Rounds.Single(r => r.Id == competition.CurrentRound.Id);
				r2.State = competition.CurrentRound.State;
				r2.Competition.CurrentRound = competition.CurrentRound;
			}

			context.SaveChanges();

			// Invalidate the last page in viewmodel
			// so the user can not go back with the left button.
			this.ViewModel.InvalidateLastPage();
		}

	    /// <summary>
		///     The print_ can execute.
		/// </summary>
		/// <param name="sender">
		///     The sender.
		/// </param>
		/// <param name="e">
		///     The e.
		/// </param>
		private void Print_CanExecute(object sender, CanExecuteRoutedEventArgs e)
		{
			e.CanExecute = true;
			e.Handled = true;
		}

	    /// <summary>
		///     The print_ executed.
		/// </summary>
		/// <param name="sender">
		///     The sender.
		/// </param>
		/// <param name="e">
		///     The e.
		/// </param>
		private void Print_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			this.BtnPrint_OnClick(null, null);
			e.Handled = true;
		}

	    private void ReadLicenseFile()
		{
			try
			{
				var encrypted = File.ReadAllText("license.lic");

				var licenseXml = EncryptionHelper.Decrypt(encrypted, "jhfeihi corjwoh h ercwer");

				var xmlDoc = new XmlDocument();
				xmlDoc.LoadXml(licenseXml);

				License.LicenseHolder = xmlDoc.GetElementsByTagName("LicenseFor")[0].Attributes["Name"].Value;

				var features = xmlDoc.GetElementsByTagName("Features")[0];

				License.TabletServer = features.Attributes["TabletServer"].InnerText == "True" ? true : false;
				License.Ejudge = features.Attributes["Ejudge"].InnerText == "True" ? true : false;
				License.CheckIn = features.Attributes["CheckIn"].InnerText == "True" ? true : false;
				License.SQLServer = features.Attributes["SQLServer"].InnerText == "True" ? true : false;
			}
			catch (Exception)
			{
				License.LicenseHolder = "Demoversion";
				License.TabletServer = true;
				License.Ejudge = true;
				License.CheckIn = true;
				License.SQLServer = true;
			}
		}


	    private void UpdateStatusMessage(string message, int progress, int max)
		{
			if (string.IsNullOrEmpty(message))
			{
				this.ProgressBar.Visibility = Visibility.Hidden;
				this.ShowStatusLabel.Content = string.Empty;
			}
			else
			{
				this.ProgressBar.Visibility = Visibility.Visible;
				this.ProgressBar.Maximum = max;
				this.ProgressBar.Value = progress;
				this.ShowStatusLabel.Content = message;
			}
		}

	    /// <summary>
		///     The view model_ property changed.
		/// </summary>
		/// <param name="sender">
		///     The sender.
		/// </param>
		/// <param name="e">
		///     The e.
		/// </param>
		private void ViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			if (e.PropertyName == "ActivePage")
			{
				if (this.viewModel.ActivePage == null)
				{
					this.ContentFrame.Content = new Page();
					return;
				}

                if (this.ViewModel.ActivePage is ScrutinusPage)
                {
                    var scrutinusPage = (ScrutinusPage)this.ViewModel.ActivePage;
                    this.ContentFrame.VerticalContentAlignment = scrutinusPage.FrameVerticalContentAllignment;
                    this.ContentFrame.VerticalAlignment = scrutinusPage.FrameVerticalContentAllignment;
                }

                this.ContentFrame.Content = this.viewModel.ActivePage;

				var windowTitle = "";

                if (this.viewModel.ActivePage != null && this.viewModel.ActivePage.Title != null)
				{
					windowTitle = this.viewModel.ActivePage.Title;
				}
				var eventTitle = "";
				if (this.viewModel.Event != null && this.viewModel.Event.Title != null)
				{
					eventTitle = this.viewModel.Event.Title;
				}
				var competitionTitle = "";
				var roundTitle = "";
				if (this.viewModel.CurrentCompetition != null)
				{
					competitionTitle = this.viewModel.CurrentCompetition.Competition.Title;
					roundTitle = this.viewModel.CurrentCompetition.CurrentRound != null
									 ? this.viewModel.CurrentCompetition.CurrentRound.Name
									 : "";
				}

				this.Title = string.Format(
					"{0} | {1} | {3} | {2}",
					eventTitle,
					competitionTitle,
					windowTitle,
					roundTitle);
				this.viewModel.NextStep_Enabled = true;
			}
		}

	    private void BackupTimerThread(object obj)
		{
			var lastBackup = DateTime.Now;

			while (true)
			{
				try
				{ 
					if (Settings.Default.AutoBackupIntervallInMinutes <= 0)
					{
						return;
					}

					Thread.Sleep(Settings.Default.AutoBackupIntervallInMinutes * 60 * 1000);
					
					if (Settings.Default.EnableAutoBackup && File.Exists(Settings.Default.DataSource))
					{
						if (lastBackup < ScrutinusContext.LastSaveDateTime)
						{
							var fileInfo = new FileInfo(Settings.Default.DataSource);
							File.Copy(
								Settings.Default.DataSource,
								Settings.Default.BackupDirectory + "\\"
								+ fileInfo.Name.Replace(
									".sdf",
									string.Format(
										"_{3:0000}{4:00}{5:00}_{2:00}{1:00}{0:00}.sdf",
										DateTime.Now.Second,
										DateTime.Now.Minute,
										DateTime.Now.Hour,
										DateTime.Now.Year,
										DateTime.Now.Month,
										DateTime.Now.Day)));
						}
					}
				}
				catch (Exception ex)
				{
					ScrutinusContext.WriteLogEntry(ex);
				}
			}
		}

	    private void HandleInvalidateNavigationMessage(InvalidateNavigationMessage invalidateMessage)
		{
			// reset current page of this competition, just in case ...
			var element = this.viewModel.Competitions.SingleOrDefault(c => c.Competition.Id == invalidateMessage.Competition.Id);
			if (element != null)
			{
				element.CurrentPage = null;
				element.CurrentRound = invalidateMessage.Competition.CurrentRound;
			}
		}

	    public Task<ProgressDialogController> OpenProgressBarAsync(string title, string message)
        {
            return (Task<ProgressDialogController>) this.Dispatcher.Invoke(new Func<Task<ProgressDialogController>>(() => this.ShowProgressAsync(title, message)));
        }

	    public ProgressDialogController OpenProgressBar(string title, string message)
	    {
	        var task = this.OpenProgressBarAsync(title, message);
            task.WaitWithoutBlockingUI(this.Dispatcher);

	        return task.Result;
	    }

#endregion
	}
}