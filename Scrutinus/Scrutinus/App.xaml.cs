﻿// // TPS.net TPS8 Scrutinus
// // App.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using MahApps.Metro;

namespace Scrutinus
{
    /// <summary>
    ///     Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        #region Methods

        protected override void OnStartup(StartupEventArgs e)
        {
            // get the theme from the current var
            var appTheme = ThemeManager.DetectAppStyle(Current);

            // now use the custom accent
            ThemeManager.ChangeAppStyle(Current, ThemeManager.GetAccent("Blue"), appTheme.Item1);

            // get the theme from the current application
            var theme = ThemeManager.GetAppTheme(Settings.Default.ApplicationTheme)
                             ?? ThemeManager.GetAppTheme("BaseDark");
            var accent = ThemeManager.GetAccent(Settings.Default.AccentColor) ?? ThemeManager.GetAccent("Blue");

            // now set the Green accent and dark theme
            ThemeManager.ChangeAppStyle(Current, accent, theme);

            base.OnStartup(e);

            //  EventManager.RegisterClassHandler(typeof(TextBox), TextBox.GotFocusEvent, new RoutedEventHandler(TextBox_SelectAllText));
            EventManager.RegisterClassHandler(
                typeof(TextBox),
                UIElement.PreviewMouseDownEvent,
                new MouseButtonEventHandler(this.TextBox_SelectivelyIgnoreMouseButton));
        }

        private void TextBox_SelectAllText(object sender, RoutedEventArgs e)
        {
            ((TextBox)sender).SelectAll();
        }

        private void TextBox_SelectivelyIgnoreMouseButton(object sender, MouseButtonEventArgs e)
        {
            // If its a triple click, select all text for the user.
            if (e.ClickCount == 3)
            {
                this.TextBox_SelectAllText(sender, new RoutedEventArgs());
                return;
            }

            // Find the TextBox
            DependencyObject parent = e.OriginalSource as UIElement;
            while (parent != null && !(parent is TextBox))
            {
                parent = VisualTreeHelper.GetParent(parent);
            }

            if (parent != null)
            {
                if (parent is TextBox)
                {
                    var textBox = (TextBox)parent;
                    if (!textBox.IsKeyboardFocusWithin)
                    {
                        // If the text box is not yet focussed, give it the focus and
                        // stop further processing of this click event.
                        textBox.Focus();
                        e.Handled = true;
                    }
                }
            }
        }

        #endregion
    }
}