﻿// // TPS.net TPS8 Scrutinus
// // AttachJudgingSheets.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows.Controls;
using Scrutinus.Dialogs.DialogViewModels;

namespace Scrutinus.Dialogs
{
    /// <summary>
    ///     Interaction logic for AttachJudgingSheets.xaml
    /// </summary>
    public partial class AttachJudgingSheets : Page
    {
        #region Constructors and Destructors

        public AttachJudgingSheets(int competitionId)
        {
            this.InitializeComponent();

            this.DataContext = new AttachJudgingSheetsViewModel(competitionId);
        }

        #endregion
    }
}