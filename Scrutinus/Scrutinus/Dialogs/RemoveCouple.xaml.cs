﻿// // TPS.net TPS8 Scrutinus
// // RemoveCouple.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows.Controls;
using Scrutinus.Dialogs.DialogViewModels;

namespace Scrutinus.Dialogs
{
    /// <summary>
    ///     Interaction logic for RemoveCouple.xaml
    /// </summary>
    public partial class RemoveCouple : Page
    {
        #region Constructors and Destructors

        public RemoveCouple(RemoveCoupleViewModel viewModel)
        {
            this.InitializeComponent();
            this.DataContext = viewModel;
        }

        #endregion
    }
}