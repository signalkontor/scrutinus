﻿// // TPS.net TPS8 Scrutinus
// // ProgramSettings.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Windows.Input;
using DataModel.Models;
using GalaSoft.MvvmLight.Command;
using Helpers;
using Scrutinus.Localization;
using Scrutinus.Pages;
using Scrutinus.ViewModels;

namespace Scrutinus.Dialogs.SettingsDialogs
{
    /// <summary>
    ///     Interaction logic for ProgramSettings.xaml
    /// </summary>
    public partial class ProgramSettings : ScrutinusPage
    {
        #region Constructors and Destructors

        public ProgramSettings()
            : base(0, 0)
        {
            this.InitializeComponent();

            this.Languages.ItemsSource = ComboBoxItemSources.GetLocales();
            try
            {
                // this.Competitions = new ObservableCollection<Competition>(context.Competitions);
                this.RoundsOfCompetition = new ObservableCollection<Round>();
            }
            catch (SqlException)
            {
                // this.Competitions = new ObservableCollection<Competition>();
                this.RoundsOfCompetition = new ObservableCollection<Round>();
            }

            this.SaveCommand = new RelayCommand(this.Save);
            this.CloseCommand = new RelayCommand(this.Close);

            var colors = new List<string>
                             {
                                 "Red",
                                 "Green",
                                 "Blue",
                                 "Purple",
                                 "Orange",
                                 "Lime",
                                 "Emerald",
                                 "Teal",
                                 "Cyan",
                                 "Cobalt",
                                 "Indigo",
                                 "Violet",
                                 "Pink",
                                 "Magenta",
                                 "Crimson",
                                 "Amber",
                                 "Yellow",
                                 "Brown",
                                 "Olive",
                                 "Steel",
                                 "Mauve",
                                 "Taupe",
                                 "Sienna"
                             };
            var themes = new List<string> { "BaseLight", "BaseDark" };

            this.AvailableAccentColors = new ObservableCollection<string>(colors);
            this.AvailableApplicationThemes = new ObservableCollection<string>(themes);
        }

        #endregion

        #region Public Properties

        public string AccentColor
        {
            get
            {
                return Settings.Default.AccentColor;
            }

            set
            {
                Settings.Default.AccentColor = value;
                MainWindow.MainWindowHandle.SetApplicationTheme(
                    Settings.Default.AccentColor,
                    Settings.Default.ApplicationTheme);
            }
        }

        public string ApplicationTheme
        {
            get
            {
                return Settings.Default.ApplicationTheme;
            }

            set
            {
                Settings.Default.ApplicationTheme = value;
                MainWindow.MainWindowHandle.SetApplicationTheme(
                    Settings.Default.AccentColor,
                    Settings.Default.ApplicationTheme);
            }
        }

        public ObservableCollection<string> AvailableAccentColors { get; set; }

        public ObservableCollection<string> AvailableApplicationThemes { get; set; }

        public ICommand CloseCommand { get; set; }

        public ObservableCollection<Round> RoundsOfCompetition { get; set; }

        public ICommand SaveCommand { get; set; }

        #endregion

        #region Public Methods and Operators

        public override bool AllowToLeave()
        {
            return true;
        }

        public override bool CanMoveNext()
        {
            return false;
        }

        protected override void OnNextInternal()
        {
            // nothing to do
        }

        #endregion

        #region Methods

        protected override void SetViewModel()
        {
            if (string.IsNullOrEmpty(Settings.Default.BackupDirectory))
            {
                Settings.Default.BackupDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
                                                   + "\\TPS\\Backup";
                try
                {
                    Directory.CreateDirectory(Settings.Default.BackupDirectory);
                }
                catch
                {
                }
            }
            this.DataContext = Settings.Default;
        }

        private void Close()
        {
            MainWindow.MainWindowHandle.CloseDialog();
        }

        private void Save()
        {
            var context = new ScrutinusContext();

            Settings.Default.WDSFApiPassword = this.plain.Text;

            var inputEnabled = context.GetParameter("MarkingInputEnabled", true);
            if (!inputEnabled)
            {
                // We have to check the password
                var passwordHash = context.GetParameter<string>("StoredPassword");
                if (passwordHash != SecurityHelper.CalculateMd5Hash(this.ChairmanPassword.Text))
                {
                    MainWindow.MainWindowHandle.ShowMessage("Password cannot be changed");
                    // return;
                }
            }

            context.SetParameter("StoredPassword", SecurityHelper.CalculateMd5Hash(this.ChairmanPassword.Text));
            context.SetParameter("MarkingInputEnabled", !Settings.Default.ChairmanPasswordEnabled);

            MainWindow.MainWindowHandle.ViewModel.MarkingInputEnabled = !Settings.Default.ChairmanPasswordEnabled;

            Settings.Default.ChairmanPassword = "";

            Settings.Default.Save();

            if (Settings.Default.LanguageLocale != null)
            {
                LocalizationService.ApplicationLanguage = new CultureInfo(Settings.Default.LanguageLocale);
            }

            MainWindow.MainWindowHandle.CloseDialog();
        }

        #endregion
    }
}