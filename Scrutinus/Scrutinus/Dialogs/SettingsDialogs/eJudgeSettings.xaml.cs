﻿// // TPS.net TPS8 Scrutinus
// // eJudgeSettings.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using Scrutinus.Localization;
using Scrutinus.Pages;

namespace Scrutinus.Dialogs.SettingsDialogs
{
    /// <summary>
    ///     Interaction logic for eJudgeSettings.xaml
    /// </summary>
    public partial class eJudgeSettings : ScrutinusPage
    {
        private readonly bool originalUseInternalXdServer;

        #region Constructors and Destructors

        public eJudgeSettings()
            : base(0, 0)
        {
            this.InitializeComponent();

            this.originalUseInternalXdServer = Settings.Default.UseInternalXdServer;

            this.SaveCommand = new RelayCommand(this.Save);
            this.CloseCommand = new RelayCommand(this.Close);
        }

        #endregion

        #region Public Properties

        public ICommand CloseCommand { get; set; }

        public ICommand SaveCommand { get; set; }

        #endregion

        #region Public Methods and Operators

        public static int GetEjudgeFlags()
        {
            var flags = 0;

            flags += Settings.Default.eJudgeHelpmarks1 ? 1 : 0;
            flags += Settings.Default.eJudgetshowHelpmarkOverview ? 2 : 0;
            flags += Settings.Default.eJudgeallowMultiLastPlaces ? 4 : 0;
            flags += Settings.Default.eJudgeshowFinalResults ? 8 : 0;
            flags += Settings.Default.eJudgeshowLift ? 16 : 0;
            flags += Settings.Default.eJudgeHelpmarks2 ? 32 : 0;
            flags += Settings.Default.eJudgeshowLanguageSelection ? 64 : 0;
            flags += Settings.Default.eJudgeshowLastPlaceButtons ? 128 : 0;

            return flags;
        }

        public override bool AllowToLeave()
        {
            return true;
        }

        public override bool CanMoveNext()
        {
            return false;
        }

        protected override void OnNextInternal()
        {
        }

        #endregion

        #region Methods

        protected override void SetViewModel()
        {
            this.DataContext = Settings.Default;
        }

        private void Close()
        {
            MainWindow.MainWindowHandle.CloseDialog();
        }

        private void Save()
        {
            Settings.Default.Save();
            MainWindow.MainWindowHandle.CloseDialog();

            // User changed settings to start XD Server
            if (Settings.Default.UseInternalXdServer != this.originalUseInternalXdServer)
            {
                MainWindow.MainWindowHandle.ShowMessageAsync(
                    LocalizationService.Resolve(() => Text.RestartTPSBecauseOfXdServer),
                    LocalizationService.Resolve(() => Text.RestartTPS));
            }
        }

        #endregion
    }
}