﻿// // TPS.net TPS8 Scrutinus
// // EditCoupleDialog.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows.Input;
using DataModel.Models;
using GalaSoft.MvvmLight.Command;
using MahApps.Metro.SimpleChildWindow;

namespace Scrutinus.Dialogs
{
    /// <summary>
    /// Interaction logic for EditCoupleDialog.xaml
    /// </summary>
    public partial class EditCoupleDialog : ChildWindow
    {
        private readonly ScrutinusContext context;

        public EditCoupleDialog(Couple couple, ScrutinusContext context)
        {
            this.CancelCommand = new RelayCommand(this.Cancel);
            this.SaveCommand = new RelayCommand(this.Save);

            this.context = context;
            this.Couple = couple;

            this.InitializeComponent();
        }

        public Couple Couple { get; set; }

        public ICommand SaveCommand { get; set; }

        public ICommand CancelCommand { get; set; }

        private void Save()
        {
            this.context.SaveChanges();
            this.Close();
        }

        private void Cancel()
        {
            this.context.Refresh(this.Couple);
            this.Close();
        }
    }
}
