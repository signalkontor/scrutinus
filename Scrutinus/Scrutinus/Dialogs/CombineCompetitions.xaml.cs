﻿// // TPS.net TPS8 Scrutinus
// // CombineCompetitions.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows.Controls;

namespace Scrutinus.Dialogs
{
    /// <summary>
    ///     Interaction logic for CombineCompetitions.xaml
    /// </summary>
    public partial class CombineCompetitions : Page
    {
        #region Constructors and Destructors

        public CombineCompetitions()
        {
            this.InitializeComponent();
        }

        #endregion
    }
}