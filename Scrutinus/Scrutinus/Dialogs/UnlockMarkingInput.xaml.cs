﻿// // TPS.net TPS8 Scrutinus
// // UnlockMarkingInput.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows.Controls;
using System.Windows.Input;
using DataModel.Models;
using GalaSoft.MvvmLight.Command;
using Helpers;

namespace Scrutinus.Dialogs
{
    /// <summary>
    ///     Interaction logic for UnlockMarkingInput.xaml
    /// </summary>
    public partial class UnlockMarkingInput : Page
    {
        #region Constructors and Destructors

        public UnlockMarkingInput()
        {
            this.SaveCommand = new RelayCommand(this.Save);
            this.CancelCommand = new RelayCommand(this.Cancel);

            this.InitializeComponent();
        }

        #endregion

        #region Public Properties

        public ICommand CancelCommand { get; set; }

        public ICommand SaveCommand { get; set; }

        #endregion

        #region Methods

        private void Cancel()
        {
            MainWindow.MainWindowHandle.CloseDialog();
        }

        private void Save()
        {
#if DTV
            MainWindow.MainWindowHandle.ViewModel.MarkingInputEnabled = true;
            MainWindow.MainWindowHandle.CloseDialog();
#endif

            var context = new ScrutinusContext();

            var passwordHash = SecurityHelper.CalculateMd5Hash(this.PasswordBox.Password);

            var storedPassword = context.GetParameter<string>("StoredPassword");

            if (passwordHash == storedPassword)
            {
                MainWindow.MainWindowHandle.ViewModel.MarkingInputEnabled = true;
                MainWindow.MainWindowHandle.CloseDialog();
            }
        }

        #endregion
    }
}