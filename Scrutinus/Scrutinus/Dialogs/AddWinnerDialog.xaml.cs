﻿// // TPS.net TPS8 Scrutinus
// // AddWinnerDialog.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows.Controls;
using DataModel.Models;
using Scrutinus.Dialogs.DialogViewModels;

namespace Scrutinus.Dialogs
{
    /// <summary>
    ///     Interaction logic for AddWinnerDialog.xaml
    /// </summary>
    public partial class AddWinnerDialog : Page
    {
        #region Constructors and Destructors

        public AddWinnerDialog(Competition competition)
        {
            this.InitializeComponent();

            var viewModel = this.DataContext as AddWinnerViewModel;

            viewModel?.SetCompetition(competition.Id);
        }

        #endregion
    }
}