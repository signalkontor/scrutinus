﻿// // TPS.net TPS8 Scrutinus
// // FindWdsfStarCouples.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows.Controls;
using DataModel.Models;
using Scrutinus.Dialogs.DialogViewModels;

namespace Scrutinus.Dialogs
{
    /// <summary>
    /// Interaction logic for FindWdsfStarCouples.xaml
    /// </summary>
    public partial class FindWdsfStarCouples : Page
    {
        public FindWdsfStarCouples(Competition competition)
        {
            this.InitializeComponent();

            var model = this.DataContext as FindWdsfStarCouplesViewModel;
            if (model != null)
            {
                model.Competition = competition;
            }
        }
    }
}
