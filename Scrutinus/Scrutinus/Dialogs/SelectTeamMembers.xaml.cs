﻿// // TPS.net TPS8 Scrutinus
// // SelectTeamMembers.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows.Controls;
using Scrutinus.Dialogs.DialogViewModels;

namespace Scrutinus.Dialogs
{
    /// <summary>
    /// Interaktionslogik für SelectTeamMembers.xaml
    /// </summary>
    public partial class SelectTeamMembers : Page
    {
        public SelectTeamMembers(SelectTeamMembersViewModel viewModel)
        {
            this.InitializeComponent();
            this.DataContext = viewModel;
        }
    }
}
