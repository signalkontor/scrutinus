﻿// // TPS.net TPS8 Scrutinus
// // AddDtvCoupleToCompetition.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows.Controls;
using DataModel.Models;
using Scrutinus.Dialogs.DialogViewModels;

namespace Scrutinus.Dialogs
{
    /// <summary>
    ///     Interaction logic for AddDtvCoupleToCompetition.xaml
    /// </summary>
    public partial class AddDtvCoupleToCompetition : Page
    {
        #region Constructors and Destructors

        public AddDtvCoupleToCompetition(ScrutinusContext context, Competition competition)
        {
            this.InitializeComponent();

            var viewModel = new AddDtvCoupleToCompetitionViewModel(context, competition);
            this.DataContext = viewModel;
        }

        #endregion
    }
}