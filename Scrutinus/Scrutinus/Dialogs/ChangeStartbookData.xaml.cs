﻿// // TPS.net TPS8 Scrutinus
// // ChangeStartbookData.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows.Controls;
using DataModel.Models;
using Scrutinus.Dialogs.DialogViewModels;

namespace Scrutinus.Dialogs
{
    /// <summary>
    ///     Interaction logic for ChangeStartbookData.xaml
    /// </summary>
    public partial class ChangeStartbookData : Page
    {
        #region Constructors and Destructors

        public ChangeStartbookData(Round round, ScrutinusContext context)
        {
            this.InitializeComponent();

            this.DataContext = new ChangeStartbookDataViewModel(round, context);
        }

        #endregion
    }
}