﻿// // TPS.net TPS8 Scrutinus
// // QuickAddCoupleDialog.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Windows.Controls;
using Scrutinus.ViewModels;

namespace Scrutinus.Pages
{
    /// <summary>
    ///     Interaction logic for QuickAddCoupleDialog.xaml
    /// </summary>
    public partial class QuickAddCoupleDialog : ScrutinusPage
    {
        #region Constructors and Destructors

        public QuickAddCoupleDialog(int competitionId, int roundId, bool createCoupleAllowed)
            : base(competitionId, roundId)
        {
            this.InitializeComponent();
            this.createCoupleAllowed = createCoupleAllowed;

            this.viewModel = new QuickAddCoupleDialogViewModel(this.competitionId, this.createCoupleAllowed);

            this.viewModel.PropertyChanged += (sender, args) =>
            {
                if (args.PropertyName == "SelectedCouple" && this.viewModel.SelectedCouple != null)
                {
                    this.DataGrid.ScrollIntoView(this.viewModel.SelectedCouple);
                    this.FirstNameMan.Focus();
                }
            };

            this.DataContext = this.viewModel;
        }

        #endregion

        #region Fields

        private readonly bool createCoupleAllowed;

        private readonly QuickAddCoupleDialogViewModel viewModel;

        #endregion

        #region Public Methods and Operators

        public override bool AllowToLeave()
        {
            throw new NotImplementedException();
        }

        public override bool CanMoveNext()
        {
            throw new NotImplementedException();
        }

        protected override void OnNextInternal()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Methods

        protected override void SetViewModel()
        {
            
        }

        private void SearchBox_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            var data = (QuickAddCoupleDialogViewModel)this.DataContext;
            if (this.SearchBox.Text.Length > 2)
            {
                data.UpdateSearchFilter(this.SearchBox.Text);
            }
            else
            {
                data.ClearSearchFilter();
            }
        }

        #endregion
    }
}