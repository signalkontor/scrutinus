﻿// // TPS.net TPS8 Scrutinus
// // CompetitionStatesDialog.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows.Controls;
using MahApps.Metro.Controls;
using MahApps.Metro.SimpleChildWindow;
using Scrutinus.Dialogs.DialogViewModels;
using Xceed.Wpf.Toolkit.Core.Utilities;

namespace Scrutinus.Dialogs
{
    /// <summary>
    ///     Interaction logic for CompetitionStatesDialog.xaml
    /// </summary>
    public partial class CompetitionStatesControl : UserControl
    {
        #region Constructors and Destructors

        public CompetitionStatesControl()
        {
            this.InitializeComponent();

            this.CompetitionStatesViewModel = this.DataContext as CompetitionStatesViewModel;

            this.CompetitionStatesViewModel.ClosingAction = this.Close;
        }

        public CompetitionStatesViewModel CompetitionStatesViewModel { get; set; }

        public void Close()
        {
            var childWindow = VisualTreeHelperEx.FindAncestorByType<ChildWindow>(this);
            if (childWindow != null)
            {
                childWindow.Close();
                return;
            }

            var metroWindow = VisualTreeHelperEx.FindAncestorByType<MetroWindow>(this);
            if (metroWindow != null)
            {
                metroWindow.Close();
            }
        }

        #endregion
    }
}