﻿// // TPS.net TPS8 Scrutinus
// // SqlServerWizardDialog.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows.Controls;

namespace Scrutinus.Dialogs
{
    /// <summary>
    ///     Interaction logic for SqlServerWizardDialog.xaml
    /// </summary>
    public partial class SqlServerWizardDialog : Page
    {
        #region Constructors and Destructors

        public SqlServerWizardDialog()
        {
            this.InitializeComponent();
        }

        #endregion
    }
}