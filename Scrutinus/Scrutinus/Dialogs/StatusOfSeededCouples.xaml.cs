﻿// // TPS.net TPS8 Scrutinus
// // StatusOfSeededCouples.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Linq;
using System.Windows;
using System.Windows.Controls;
using DataModel;
using DataModel.Models;
using Scrutinus.Pages;

namespace Scrutinus.Dialogs
{
    /// <summary>
    ///     Interaction logic for StatusOfSeededCouples.xaml
    /// </summary>
    public partial class StatusOfSeededCouples : ScrutinusPage
    {
        #region Fields

        private readonly int _competitionId;

        #endregion

        #region Constructors and Destructors

        public StatusOfSeededCouples(int competitionId, int roundId)
            : base(competitionId, roundId)
        {
            this.InitializeComponent();

            this._competitionId = competitionId;

            var DataSource =
                this.context.Participants.Where(p => p.Competition.Id == competitionId && p.Star > 0)
                    .OrderBy(p => p.Number)
                    .ToList();

            this.StartListGrid.ItemsSource = DataSource;
        }

        #endregion

        #region Public Methods and Operators

        public override bool AllowToLeave()
        {
            return true;
        }

        public override bool CanMoveNext()
        {
            return true;
        }

        protected override void OnNextInternal()
        {
        }

        #endregion

        #region Methods

        protected override void SetViewModel()
        {
        }

        private void Close_OnClick(object sender, RoutedEventArgs e)
        {
            MainWindow.MainWindowHandle.CloseDialog();
        }

        private void MenuItem_OnClick(object sender, RoutedEventArgs e)
        {
            if (!(sender is MenuItem))
            {
                return;
            }
            // Get the item seletced
            var participant = this.StartListGrid.SelectedItem as Participant;
            // no participant -> exit
            if (participant == null)
            {
                return;
            }

            var item = sender as MenuItem;
            var name = item.Tag as string;

            switch (name)
            {
                case "Dancing":
                    participant.State = CoupleState.Dancing;
                    break;
                case "Excused":
                    participant.State = CoupleState.Excused;
                    break;
                case "Missing":
                    participant.State = CoupleState.Missing;
                    break;
            }

            this.context.SaveChanges();
            // DataContext = _context.Participants.Where(p => p.CompetitionId == _competitionId).ToList();
            this.StartListGrid.ItemsSource =
                this.context.Participants.Where(p => p.Competition.Id == this._competitionId && p.Star > 0).ToList();
            ;
        }

        private void StartListGrid_OnBeginningEdit(object sender, DataGridBeginningEditEventArgs e)
        {
            e.Cancel = true;
        }

        #endregion
    }
}