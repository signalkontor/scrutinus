﻿// // TPS.net TPS8 Scrutinus
// // DefineResultService.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using Scrutinus.Pages;

namespace Scrutinus.Dialogs
{
    /// <summary>
    ///     Interaction logic for DefineResultService.xaml
    /// </summary>
    public partial class DefineResultService : ScrutinusPage
    {
        #region Constructors and Destructors

        public DefineResultService()
            : base(0, 0)
        {
            this.InitializeComponent();
        }

        #endregion

        #region Methods

        protected override void SetViewModel()
        {
        }

        #endregion

        #region Public Methods and Operators

        public override bool AllowToLeave()
        {
            return true;
        }

        public override bool CanMoveNext()
        {
            return true;
        }

        protected override void OnNextInternal()
        {
        }

        #endregion
    }
}