﻿// // TPS.net TPS8 Scrutinus
// // ReadLaufzettelBarcode.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows.Controls;

namespace Scrutinus.Dialogs
{
    /// <summary>
    ///     Interaction logic for ReadLaufzettelBarcode.xaml
    /// </summary>
    public partial class ReadLaufzettelBarcode : Page
    {
        #region Constructors and Destructors

        public ReadLaufzettelBarcode()
        {
            this.InitializeComponent();
        }

        #endregion
    }
}