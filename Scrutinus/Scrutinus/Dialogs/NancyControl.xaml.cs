﻿// // TPS.net TPS8 Scrutinus
// // NancyControl.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows.Controls;
using Scrutinus.Dialogs.DialogViewModels;

namespace Scrutinus.Dialogs
{
    /// <summary>
    ///     Interaction logic for NancyControl.xaml
    /// </summary>
    public partial class NancyControl : Page
    {
        #region Constructors and Destructors

        public NancyControl()
        {
            this.InitializeComponent();

            var viewModel = this.DataContext as NancyControlViewModel;

            viewModel.SelectedInterfaces = this.ListView.SelectedItems;

            var selectedHostnames = Settings.Default.NancyHostNames.Split(',');
            foreach (var hostName in selectedHostnames)
            {
                viewModel.SelectedInterfaces.Add(hostName);
            }
        }

        #endregion
    }
}