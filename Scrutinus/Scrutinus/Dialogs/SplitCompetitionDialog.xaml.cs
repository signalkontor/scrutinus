﻿// // TPS.net TPS8 Scrutinus
// // SplitCompetitionDialog.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows.Controls;
using DataModel.Models;
using Scrutinus.Dialogs.DialogViewModels;

namespace Scrutinus.Dialogs
{
    /// <summary>
    /// Interaction logic for SplitCompetitionDialog.xaml
    /// </summary>
    public partial class SplitCompetitionDialog : Page
    {
        private readonly SplitCompetitionDialogViewModel viewModel;

        public SplitCompetitionDialog(ScrutinusContext context, Competition competition)
        {
            this.InitializeComponent();
            this.viewModel = new SplitCompetitionDialogViewModel(context, competition);
            this.DataContext = this.viewModel;
        }

        public SplitCompetitionDialogViewModel ViewModel
        {
            get { return this.viewModel; }
        }
    }
}
