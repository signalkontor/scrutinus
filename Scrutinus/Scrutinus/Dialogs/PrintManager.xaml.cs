﻿// // TPS.net TPS8 Scrutinus
// // PrintManager.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows;
using Scrutinus.Dialogs.DialogViewModels;

namespace Scrutinus.Dialogs
{
    /// <summary>
    /// Interaction logic for PrintManager.xaml
    /// </summary>
    public partial class PrintManager
    {
        public PrintManager()
        {
            this.InitializeComponent();

            var viewModel = this.DataContext as PrintManagerViewModel;

            if (viewModel != null)
            {
                viewModel.ClosingAction = () => this.Close();
            }
        }

        private void CloseHelpLayer(object sender, RoutedEventArgs e)
        {
            this.HelpLayer.Visibility = Visibility.Collapsed;
        }
    }
}
