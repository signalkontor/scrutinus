﻿// // TPS.net TPS8 Scrutinus
// // DeleteRoundDialog.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows.Controls;

namespace Scrutinus.Dialogs
{
    /// <summary>
    ///     Interaction logic for DeleteRoundDialog.xaml
    /// </summary>
    public partial class DeleteRoundDialog : Page
    {
        #region Constructors and Destructors

        public DeleteRoundDialog()
        {
            this.InitializeComponent();
        }

        #endregion
    }
}