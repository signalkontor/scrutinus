﻿// // TPS.net TPS8 Scrutinus
// // ImportCompetitionFromWDSF.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using DataModel.Models;
using Wdsf.Api.Client;

namespace Scrutinus.Dialogs
{
    /// <summary>
    ///     Interaction logic for ImportFromWDSF.xaml
    /// </summary>
    public partial class ImportCompetitionFromWDSF : Page
    {
        #region Fields

        private readonly ScrutinusContext _context;

        #endregion

        #region Constructors and Destructors

        public ImportCompetitionFromWDSF(ScrutinusContext context)
        {
            this.InitializeComponent();
            this._context = context;
            this.Competitions = new ObservableCollection<Competition>();
        }

        #endregion

        #region Methods

        public ObservableCollection<Competition> Competitions { get; set; }

        private void CancelOnClick(object sender, RoutedEventArgs e)
        {
            MainWindow.MainWindowHandle.CloseDialog();
        }

        private void DoSearch(string dateFrom, string dateTo, string location)
        {
#if DEBUG
            // var apiClient = new Client("guest", "guest", WdsfEndpoint.Sandbox);

            var apiClient = new Client(
                Settings.Default.WDSFApiUser,
                Settings.Default.WDSFApiPassword,
                WdsfEndpoint.Services);
#else
            var apiClient = new Client(
                Settings.Default.WDSFApiUser,
                Settings.Default.WDSFApiPassword,
                WdsfEndpoint.Services);
#endif
            var filter = new Dictionary<string, string> { { "from", dateFrom }, { "to", dateTo } };
            var wdsfEvent = apiClient.GetCompetitions(filter);
            var count = 0;
            foreach (var competition in wdsfEvent)
            {
                var competitionDetails = apiClient.GetCompetition(competition.Id);
                count++;

                this.Dispatcher.Invoke(new Action(() => this.UpdateProgressbar(count, wdsfEvent.Count)));

                if (string.IsNullOrEmpty(location) || competitionDetails.Location.ToLower().Contains(location.ToLower()))
                {
                    var newCompetition = new Competition
                                             {
                                                 Event = this._context.Events.First(),
                                                 Title = competition.Name,
                                                 StartTime = competitionDetails.Date,
                                                 AgeGroup =
                                                     this._context.AgeGroups.Single(
                                                         a => a.Id == 7),
                                                 Class =
                                                     this._context.Classes.Single(
                                                         c => c.Id == 8),
                                                 CompetitionType =
                                                     this._context.CompetitionTypes.Single(
                                                         c => c.Id == 1),
                                                 Section =
                                                     this._context.Sections.Single(
                                                         s => s.Id == 1),
                                                 Stars = 0,
                                                 State = 0,
                                                 ExternalId = competitionDetails.Id.ToString()
                                             };

                    this.Dispatcher.Invoke(new Action(() => { this.Competitions.Add(newCompetition); }));
                }
            }

            this.Dispatcher.Invoke(new Action(() => { this.CompetitionList.ItemsSource = this.Competitions; }));
            
        }

        private void OnClickSearch(object sender, RoutedEventArgs e)
        {
            var dateFromText = this.dateFrom.Text;
            var dateToText = this.dateTo.Text;
            var city = this.cityEvent.Text;

            var task = new Task(() => this.DoSearch(dateFromText, dateToText, city));
            task.Start();
        }

        private void OnClickImport(object sender, RoutedEventArgs e)
        {
            var selected = this.CompetitionList.SelectedItems.Cast<Competition>();

            foreach (var competition in selected)
            {

                this._context.Competitions.Add(competition);
                this._context.SaveChanges();
            }

            MainWindow.MainWindowHandle.ViewModel.UpdateNavigationList();

            this.Dispatcher.Invoke(new Action(() => MainWindow.MainWindowHandle.CloseDialog()));
        }

        private void UpdateProgressbar(int value, int max)
        {
            this.ImportProgress.Maximum = max;
            this.ImportProgress.Value = value;
        }

        #endregion
    }
}