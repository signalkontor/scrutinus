﻿// // TPS.net TPS8 Scrutinus
// // EditStartbuchDialog.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows.Controls;
using DataModel.Models;
using Scrutinus.Dialogs.DialogViewModels;

// ReSharper disable once CheckNamespace

namespace CheckIn.View
{
    /// <summary>
    ///     Interaction logic for EditStartbuchDialog.xaml
    /// </summary>
    public partial class EditStartbuchDialog : Page
    {
        #region Constructors and Destructors

        public EditStartbuchDialog(ScrutinusContext context, Couple couple)
        {
            var viewModel = new EditStartbuchDialogViewModel(context, couple);
            this.DataContext = viewModel;

            this.InitializeComponent();
        }

        #endregion
    }
}