﻿// // TPS.net TPS8 Scrutinus
// // CertificateDialog.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Windows.Controls;
using DataModel.Models;
using Scrutinus.Dialogs.DialogViewModels;

namespace Scrutinus.Dialogs
{
    /// <summary>
    ///     Interaction logic for certificateDialog.xaml
    /// </summary>
    public partial class CertificateDialog : Page
    {
        #region Constructors and Destructors

        public CertificateDialog(IEnumerable<Participant> participants, Round round, string printer)
        {
            this.InitializeComponent();

            var viewModel = this.DataContext as PrintCertificateDialogViewModel;

            foreach (var participant in participants)
            {
                viewModel.Participants.Add(participant);
            }

            viewModel.Printer = printer;
            viewModel.Round = round;
        }

        #endregion
    }
}