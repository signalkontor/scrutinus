﻿// // TPS.net TPS8 Scrutinus
// // SetStartNumberDialog.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using Scrutinus.Localization;

namespace Scrutinus.Dialogs
{
    public enum SortOrders
    {
        None = 0,

        FirstNameMan,

        LastNameMan,

        FirstNameWoman,

        LastNameWoman,

        Club
    }

    public class SortOrderDescriptor
    {
        #region Public Properties

        public SortOrders SortOrder { get; set; }

        public string Title { get; set; }

        #endregion
    }

    /// <summary>
    ///     Interaction logic for SetStartNumberDialog.xaml
    /// </summary>
    public partial class SetStartNumberDialog : Page
    {
        #region Fields

        private int firstStartNumber;

        #endregion

        #region Constructors and Destructors

        public SetStartNumberDialog()
        {
            this.OkCommand = new RelayCommand(this.Ok);
            this.CancelCommand = new RelayCommand(this.Cancel);
            this.firstStartNumber = 1;
            this.CreateSortOrderList();

            this.SortOrder = this.SortOrderList.FirstOrDefault(p => p.SortOrder == SortOrders.LastNameMan);

            this.InitializeComponent();
        }

        #endregion

        #region Public Properties

        public ICommand CancelCommand { get; set; }

        public int FirstStartNumber
        {
            get
            {
                return this.firstStartNumber;
            }

            set
            {
                if (value < 1)
                {
                    throw new InvalidDataException("Value must be greater 0");
                }

                this.firstStartNumber = value;
            }
        }

        public ICommand OkCommand { get; set; }

        public bool Result { get; set; }

        public SortOrderDescriptor SortOrder { get; set; }

        public List<SortOrderDescriptor> SortOrderList { get; set; }

        #endregion

        #region Methods

        private void Cancel()
        {
            this.Result = false;
            MainWindow.MainWindowHandle.CloseDialog();
        }

        private void CreateSortOrderList()
        {
            this.SortOrderList = new List<SortOrderDescriptor>();

            var names = Enum.GetNames(typeof(SortOrders));

            foreach (var name in names)
            {
                var text = LocalizationService.Resolve("Scrutinus:Text:SortOrders_" + name);
                var descriptor = new SortOrderDescriptor
                                     {
                                         Title = text,
                                         SortOrder = (SortOrders)Enum.Parse(typeof(SortOrders), name)
                                     };
                this.SortOrderList.Add(descriptor);
            }
        }

        private void Ok()
        {
            this.Result = true;
            MainWindow.MainWindowHandle.CloseDialog();
        }

        #endregion
    }
}