﻿// // TPS.net TPS8 Scrutinus
// // HelpDialog.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms.Integration;
using MahApps.Metro.SimpleChildWindow;
using WebBrowser = System.Windows.Forms.WebBrowser;

namespace Scrutinus.Dialogs
{
    /// <summary>
    /// Interaktionslogik für HelpDialog.xaml
    /// </summary>
    public partial class HelpDialog : ChildWindow
    {
        public HelpDialog(string navigateTo)
        {
            this.InitializeComponent();

            var host = new WindowsFormsHost();
            var webBrowser = new WebBrowser();
            host.Child = webBrowser;
            Grid.SetRow(host, 0);

            this.DialogGrid.Children.Add(host);

            webBrowser.Navigate(navigateTo);
        }

        private void CloseDialog(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
