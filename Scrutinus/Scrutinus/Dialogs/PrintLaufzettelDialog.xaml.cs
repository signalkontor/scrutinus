﻿// // TPS.net TPS8 Scrutinus
// // PrintLaufzettelDialog.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Input;
using DataModel;
using DataModel.Models;
using GalaSoft.MvvmLight.Command;
using Scrutinus.Reports;

namespace Scrutinus.Dialogs
{
    /// <summary>
    ///     Interaction logic for PrintLaufzettelDialog.xaml
    /// </summary>
    public partial class PrintLaufzettelDialog : Page
    {
        #region Constructors and Destructors

        public PrintLaufzettelDialog(Competition competition, string printerName, int copies)
        {
            this.competition = competition;
            this.printerName = printerName;
            this.copies = copies;

            if (competition != null)
            {
                this.AvailableParticipants =
                    new List<Participant>(
                        competition.Participants.Where(
                            p => p.State == CoupleState.DroppedOut && p.Startbuch != null && p.Startbuch.PrintLaufzettel)
                            .OrderBy(p => p.Number));
            }
            else
            {
                this.AvailableParticipants = new List<Participant>();
            }

            this.SelectedParticipant = this.AvailableParticipants.FirstOrDefault();
            this.PrintCommand = new RelayCommand(this.Print);
            this.CloseCommand = new RelayCommand(this.Close);

            this.InitializeComponent();
        }

        #endregion

        #region Fields

        private readonly Competition competition;

        private readonly int copies;

        private readonly string printerName;

        #endregion

        #region Public Properties

        public List<Participant> AvailableParticipants { get; set; }

        public ICommand CloseCommand { get; set; }

        public ICommand PrintCommand { get; set; }

        public Participant SelectedParticipant { get; set; }

        #endregion

        #region Methods

        private void Close()
        {
            MainWindow.MainWindowHandle.CloseDialog();
        }

        private void Print()
        {
            if (this.SelectedParticipant != null)
            {
                PrintHelper.PrintLaufzettel(
                    this.competition,
                    this.SelectedParticipant,
                    this.printerName,
                    this.copies,
                    false);
            }
        }

        #endregion
    }
}