﻿// // TPS.net TPS8 Scrutinus
// // ChangeDanceOrder.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using DataModel.Models;

namespace Scrutinus.Dialogs
{
    public class ChangeDanceOrderViewData
    {
        #region Public Properties

        public Dance Dance { get; set; }

        public bool IsSolo { get; set; }

        public bool Selected { get; set; }

        public int SortOrder { get; set; }

        #endregion
    }

    /// <summary>
    ///     Interaction logic for ChangeDanceOrder.xaml
    /// </summary>
    public partial class ChangeDanceOrder : Page
    {
        #region Constructors and Destructors

        public ChangeDanceOrder(Round round, ScrutinusContext context)
        {
            this.InitializeComponent();

            this.context = context;
            this.round = round;
            this.viewData = new List<ChangeDanceOrderViewData>();

            var index = 1;

            foreach (var dance in round.Competition.Dances)
            {
                var danceInRound = round.DancesInRound.SingleOrDefault(d => d.Dance.Id == dance.Dance.Id);

                this.viewData.Add(
                    new ChangeDanceOrderViewData
                        {
                            Dance = dance.Dance,
                            SortOrder = index,
                            Selected = danceInRound != null,
                            IsSolo = danceInRound != null && danceInRound.IsSolo
                        });

                index++;
            }

            this.DanceList.ItemsSource = this.viewData.OrderBy(o => o.SortOrder);
        }

        #endregion

        #region Fields

        private readonly ScrutinusContext context;

        private readonly Round round;

        private readonly List<ChangeDanceOrderViewData> viewData;

        #endregion

        #region Methods

        private void BtnCancelClick(object sender, RoutedEventArgs e)
        {
            MainWindow.MainWindowHandle.CloseDialog();
        }

        private void BtnDown_Click(object sender, RoutedEventArgs e)
        {
            var obj = this.DanceList.SelectedItem as ChangeDanceOrderViewData;
            if (obj == null)
            {
                return;
            }

            if (obj.SortOrder == this.viewData.Count)
            {
                return;
            }

            var oldPos = this.viewData.Single(o => o.SortOrder == obj.SortOrder + 1);

            obj.SortOrder++;
            oldPos.SortOrder--;

            this.DanceList.ItemsSource = this.viewData.OrderBy(o => o.SortOrder);
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            var drl = this.round.DancesInRound.ToList();
            foreach (var dr in drl)
            {
                dr.Drawings.Clear();

                this.context.DancesInRounds.Remove(dr);
            }

            this.round.DancesInRound.Clear();
            this.context.SaveChanges();

            // Let's save the new order and selected dances
            var dances =
                this.viewData.Where(v => v.Selected).OrderBy(v => v.SortOrder).ToList();

            for (var i = 0; i < dances.Count; i++)
            {
                this.round.DancesInRound.Add(
                    new DanceInRound
                        {
                            Dance = dances[i].Dance,
                            IsSolo = dances[i].IsSolo,
                            Round = this.round,
                            SortOrder = i + 1
                        });
            }

            this.context.SaveChanges();

            MainWindow.MainWindowHandle.CloseDialog();
        }

        private void BtnUp_Click(object sender, RoutedEventArgs e)
        {
            var obj = this.DanceList.SelectedItem as ChangeDanceOrderViewData;
            if (obj == null)
            {
                return;
            }

            if (obj.SortOrder == 1)
            {
                return;
            }

            var oldPos = this.viewData.Single(o => o.SortOrder == obj.SortOrder - 1);

            obj.SortOrder--;
            oldPos.SortOrder++;

            this.DanceList.ItemsSource = this.viewData.OrderBy(o => o.SortOrder);
        }

        #endregion
    }
}