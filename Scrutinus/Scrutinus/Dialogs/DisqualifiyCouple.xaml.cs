﻿// // TPS.net TPS8 Scrutinus
// // DisqualifiyCouple.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows.Controls;
using Scrutinus.Dialogs.DialogViewModels;

namespace Scrutinus.Dialogs
{
    /// <summary>
    ///     Interaction logic for DisqualifiyCouple.xaml
    /// </summary>
    public partial class DisqualifiyCouple : Page
    {
        #region Constructors and Destructors

        public DisqualifiyCouple(DisqualifyCoupleViewModel viewModel)
        {
            this.InitializeComponent();

            this.DataContext = viewModel;
        }

        #endregion
    }
}