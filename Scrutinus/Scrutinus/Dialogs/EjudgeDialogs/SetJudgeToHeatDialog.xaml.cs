﻿// // TPS.net TPS8 Scrutinus
// // SetJudgeToHeatDialog.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Input;
using DataModel.ModelHelper;
using DataModel.Models;
using GalaSoft.MvvmLight.Command;
using MahApps.Metro.SimpleChildWindow;
using Scrutinus.Localization;

namespace Scrutinus.Dialogs.EjudgeDialogs
{
    /// <summary>
    /// Interaction logic for SetJudgeToHeatDialog.xaml
    /// </summary>
    public partial class SetJudgeToHeatDialog : ChildWindow
    {
        public SetJudgeToHeatDialog(Round round)
        {
            this.Dances = round.DancesInRound.OrderBy(d => d.SortOrder).ToList();
            this.SeletedDance = this.Dances.First();

            this.Heats = new List<Tuple<int, string>>();
            this.FillHeats(round);
            this.SelectedHeat = this.Heats.First();

            this.OkCommand = new RelayCommand(this.Ok);
            this.CancelCommand = new RelayCommand(this.Cancel);

            this.InitializeComponent();
        }

        public List<DanceInRound> Dances { get; set; }

        public DanceInRound SeletedDance { get; set; }

        public List<Tuple<int, string>> Heats { get; set; }

        public Tuple<int, string> SelectedHeat { get; set; }

        public ICommand OkCommand { get; set; }

        public ICommand CancelCommand { get; set; }

        public DialogResult Result { get; set; }


        private void Ok()
        {
            this.Result = DialogResult.OK;
            this.Close();
        }

        private void Cancel()
        {
            this.Result = DialogResult.Cancel;
            this.Close();
        }

        private void FillHeats(Round round)
        {
            var heats = HeatsHelper.GetHeats(new ScrutinusContext(), round.Id);

            for (var i = 1; i <= heats.First().Value.Count; i++)
            {
                this.Heats.Add(new Tuple<int, string>(i, string.Format(LocalizationService.Resolve(() => Text.HeatNumberString), i)));
            }
        }
    }
}
