﻿// // TPS.net TPS8 Scrutinus
// // EJudgeDetailsDialog.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Input;
using DataModel.Models;
using GalaSoft.MvvmLight.Command;
using mobileControl.Helper;
using MahApps.Metro.SimpleChildWindow;

namespace Scrutinus.Dialogs.EjudgeDialogs
{
    /// <summary>
    /// Interaction logic for EJudgeDetailsDialog.xaml
    /// </summary>
    public partial class EJudgeDetailsDialog : ChildWindow
    {
        private readonly ScrutinusContext conetxt;
        private readonly EjudgeData ejudgeData;

        public EJudgeDetailsDialog(ScrutinusContext context, EjudgeData data)
        {
            this.Devices = MainWindow.MainWindowHandle.ViewModel.Devices;
            this.CloseCommand = new RelayCommand(this.CloseDialog);
            this.ejudgeData = data;
            this.conetxt = context;

            this.AssignNewDeviceCommand = new RelayCommand(this.AssignNewDevice);
            this.ClearDeviceCommand = new RelayCommand(this.ClearDevice);
            this.SendDataCommand = new RelayCommand(this.SendData);
            this.RemoveDeviceCommand = new RelayCommand(this.RemoveAssignment);
            this.SetDeviceToDanceCommand = new RelayCommand(this.SetDeviceToDance);

            this.InitializeComponent();
        }

        public ObservableCollection<Device> Devices { get; set; }

        public ICommand CloseCommand { get; set; }

        public ICommand ClearDeviceCommand { get; set; }

        public ICommand RemoveDeviceCommand { get; set; }

        public ICommand SendDataCommand { get; set; }

        public ICommand SetDeviceToDanceCommand { get; set; }

        public ICommand AssignNewDeviceCommand { get; set; }

        public EjudgeData EjudgeData { get
        {
            return this.ejudgeData;
        } }

        public Device SelectedDevice { get; set; }

        private void CloseDialog()
        {
            this.Close();
        }

        private void RemoveAssignment()
        {
            this.ejudgeData.DeviceId = "";
            this.ejudgeData.Device = null;
            this.conetxt.SaveChanges();
        }

        private void ClearDevice()
        {
            ScrutinusCommunicationHandler.ClearDevice(this.ejudgeData.DeviceId, this.ejudgeData.Round.EjudgeName);
        }

        private void SetDeviceToDance()
        {
            var dlg = new SetJudgeToHeatDialog(this.ejudgeData.Round);
            dlg.CloseButtonCommand = new RelayCommand<SetJudgeToHeatDialog>(this.SetHeatExecute);
            dlg.CloseButtonCommandParameter = dlg;

            MainWindow.MainWindowHandle.ShowChildWindow(dlg);
        }

        private void SetHeatExecute(SetJudgeToHeatDialog dlg)
        {
            if (dlg.Result != DialogResult.OK)
            {
                return;
            }

            // Set to heat
            ScrutinusCommunicationHandler.MoveJudge(this.ejudgeData.DeviceId, this.ejudgeData.Round.EjudgeName, dlg.SeletedDance.SortOrder, dlg.SelectedHeat.Item1);
        }

        private void AssignNewDevice()
        {
            if(this.SelectedDevice == null)
            {
                return;
            }

            this.ejudgeData.DeviceId = this.SelectedDevice.DeviceId;
            this.ejudgeData.Device = MainWindow.MainWindowHandle.ViewModel.Devices.FirstOrDefault(d => d.DeviceId == this.SelectedDevice.DeviceId) ;

            this.conetxt.SaveChanges();
        }

        private void SendData()
        {
            ScrutinusCommunicationHandler.SendData(this.ejudgeData.Round, this.ejudgeData);
            ScrutinusCommunicationHandler.SendStart(this.ejudgeData.DeviceId, this.ejudgeData.Round.EjudgeName);
        }
    }
}
