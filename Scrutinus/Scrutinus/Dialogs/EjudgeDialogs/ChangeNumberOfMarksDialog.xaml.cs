﻿// // TPS.net TPS8 Scrutinus
// // ChangeNumberOfMarksDialog.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows.Forms;
using System.Windows.Input;
using DataModel.Models;
using GalaSoft.MvvmLight.Command;
using MahApps.Metro.SimpleChildWindow;

namespace Scrutinus.Dialogs.EjudgeDialogs
{
    /// <summary>
    /// Interaction logic for ChangeNumberOfMarksDialog.xaml
    /// </summary>
    public partial class ChangeNumberOfMarksDialog : ChildWindow
    {
        public ChangeNumberOfMarksDialog(Round round)
        {
            this.Round = round;
            this.OkCommand = new RelayCommand(this.Ok);
            this.CancelCommand = new RelayCommand(this.Cancel);

            this.InitializeComponent();
        }

        public DialogResult Result { get; set; }

        public Round Round { get; set; }

        public ICommand OkCommand { get; set; }

        public ICommand CancelCommand { get; set; }

        private void Ok()
        {
            this.Result = DialogResult.OK;
            this.Close();
        }

        private void Cancel()
        {
            this.Result = DialogResult.Cancel;
            this.Close();
        }
    }
}
