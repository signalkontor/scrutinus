﻿// // TPS.net TPS8 Scrutinus
// // OpenEventDialog.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Threading;
using System.Windows.Controls;
using System.Windows.Markup;

namespace Scrutinus.Dialogs
{
    /// <summary>
    ///     Interaction logic for OpenEventDialog.xaml
    /// </summary>
    public partial class OpenEventDialog : Page
    {
        #region Constructors and Destructors

        public OpenEventDialog()
        {
            this.Language = XmlLanguage.GetLanguage(Thread.CurrentThread.CurrentCulture.Name);
            this.InitializeComponent();
        }

        #endregion
    }
}