﻿// // TPS.net TPS8 Scrutinus
// // AboutDialog.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows;
using System.Windows.Controls;

namespace Scrutinus.Dialogs
{
    /// <summary>
    ///     Interaction logic for AboutDialog.xaml
    /// </summary>
    public partial class AboutDialog : Page
    {
        #region Constructors and Destructors

        public AboutDialog()
        {
            this.InitializeComponent();
        }

        #endregion

        #region Methods

        private void CloseDialogClicked(object sender, RoutedEventArgs e)
        {
            MainWindow.MainWindowHandle.CloseDialog();
        }

        #endregion
    }
}