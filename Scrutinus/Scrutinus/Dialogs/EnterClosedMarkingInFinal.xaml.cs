﻿// // TPS.net TPS8 Scrutinus
// // EnterClosedMarkingInFinal.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using Helpers.Skating;
using Scrutinus.Pages;

namespace Scrutinus.Dialogs
{
    /// <summary>
    ///     Interaction logic for EnterClosedMarkingInFinal.xaml
    /// </summary>
    public partial class EnterClosedMarkingInFinal : Page
    {
        #region Fields

        private readonly SkatingViewModel viewModel;

        #endregion

        #region Constructors and Destructors

        public EnterClosedMarkingInFinal(SkatingViewModel viewModel)
        {
            this.InitializeComponent();

            this.viewModel = viewModel;

            this.CreateTabControl();
        }

        #endregion

        #region Methods

        private void CloseDialog(object sender, RoutedEventArgs e)
        {
            MainWindow.MainWindowHandle.CloseDialog();
        }

        private void CreateJudgingGrid(TabItem tabItem, int judgeId)
        {
            var grid = new Grid();
            tabItem.Content = grid;

            var numberColumns = this.viewModel.Participants.Count + 1;
            var numberRows = this.viewModel.DanceSkating.Count + 1;

            for (var i = 0; i < numberRows; i++)
            {
                grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(30) });
            }

            for (var i = 0; i < numberColumns; i++)
            {
                grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Auto) });
            }

            var col = 1;
            var row = 0;

            foreach (var coupleResult in
                    this.viewModel.DanceSkating.First().CoupleResults.OrderBy(p => p.Participant.Number))
            {
                var label = new Label { Content = coupleResult.Participant.Number };
                grid.Children.Add(label);
                Grid.SetColumn(label, col);
                Grid.SetRow(label, 0);
                col++;
            }

            var tabIndexCount = 0;

            row = 1;

            foreach (var danceViewModel in this.viewModel.DanceSkating)
            {
                var label = new Label { Content = danceViewModel.DanceInRound.Dance.ShortName };
                grid.Children.Add(label);
                Grid.SetColumn(label, 0);
                Grid.SetRow(label, row);

                col = 1;

                foreach (var coupleResult in
                        danceViewModel.CoupleResults.OrderBy(c => c.Participant.Number))
                {
                    var textbox = this.CreateTextBox();
                    textbox.TabIndex = tabIndexCount;
                    tabIndexCount++;

                    var mark = coupleResult.Marks.Single(m => m.Judge.Id == judgeId);

                    textbox.DataContext = mark;

                    grid.Children.Add(textbox);
                    Grid.SetColumn(textbox, col);
                    Grid.SetRow(textbox, row);

                    col++;
                }

                row++;
            }
        }

        private void CreateTabControl()
        {
            foreach (var judge in this.viewModel.Judges.OrderBy(j => j.Sign))
            {
                var tabItem = new TabItem { Header = judge.Sign };

                this.TabControl.Items.Add(tabItem);

                this.CreateJudgingGrid(tabItem, judge.Id);
            }
        }

        private TextBox CreateTextBox()
        {
            var box = new TextBox();

            box.Foreground = Brushes.Black;
            box.MaxLength = this.viewModel.Participants.Count > 9 ? 2 : 1;
            // if our input order is per couple, we set the tab index here:
            if (!Settings.Default.FinalEnterPerJudge)
            {
                // box.TabIndex = (danceCount * judges.Count * qualifieds.Count()) + row * judges.Count + col;
            }

            var binding = new Binding("Mark")
                              {
                                  Converter = new IntegerConverter(),
                                  ConverterParameter = this.viewModel.Participants.Count
                              };
            var bindingError = new Binding("HasError")
                                   {
                                       Converter = new ErrorToBackgroundConverter(),
                                       Mode = BindingMode.OneWay
                                   };

            box.SetBinding(TextBox.TextProperty, binding);
            box.SetBinding(Control.BackgroundProperty, bindingError);
            box.SetBinding(
                IsEnabledProperty,
                new Binding("MarkingInputEnabled") { Source = MainWindow.MainWindowHandle.ViewModel });

            box.TextChanged += (sender, args) =>
                {
                    var textBox = sender as TextBox;
                    if (textBox == null)
                    {
                        return;
                    }

                    if (textBox.Text.Length > 0)
                    {
                        textBox.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
                    }
                };

            box.PreviewKeyDown += (sender, args) =>
                {
                    var textBox = sender as TextBox;
                    if (textBox == null)
                    {
                        return;
                    }

                    if (args.Key == Key.Left || args.Key == Key.Down || args.Key == Key.Right || args.Key == Key.Up)
                    {
                        args.Handled = true;
                        this.HandleCursorKeys(textBox, args.Key);
                    }
                };

            box.GotFocus += this.MarkingTextbox_GotFocus;

            return box;
        }

        private void HandleCursorKeys(TextBox textBox, Key key)
        {
            var tabIndex = textBox.TabIndex;

            switch (key)
            {
                case Key.Up:
                    tabIndex = tabIndex - this.viewModel.Participants.Count;
                    break;
                case Key.Down:
                    tabIndex = tabIndex + this.viewModel.Participants.Count;
                    break;
                case Key.Left:
                    tabIndex--;
                    break;
                case Key.Right:
                    tabIndex++;
                    break;
            }
            var grid = textBox.Parent as Grid;
            if (grid == null)
            {
                return;
            }

            var element = grid.Children.OfType<TextBox>().FirstOrDefault(t => t.TabIndex == tabIndex);

            if (element != null)
            {
                element.Focus();
                element.SelectAll();
            }
        }

        private void MarkingTextbox_GotFocus(object sender, RoutedEventArgs e)
        {
            var box = sender as TextBox;
            box.SelectAll();
        }

        #endregion
    }
}