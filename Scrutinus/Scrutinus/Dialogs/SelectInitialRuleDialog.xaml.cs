﻿// // TPS.net TPS8 Scrutinus
// // SelectInitialRuleDialog.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using Scrutinus.Localization;

namespace Scrutinus.Dialogs
{
    /// <summary>
    ///     Interaction logic for SelectInitialRuleDialog.xaml
    /// </summary>
    public partial class SelectInitialRuleDialog
    {
        #region Constructors and Destructors

        public SelectInitialRuleDialog(List<KeyValuePair<string, string>> rules)
        {
            this.IsOpen = true;
            this.AvailablesRules = new ObservableCollection<KeyValuePair<string, string>>(rules);
            this.SelectedRule = this.AvailablesRules.First();
#if DTV
            this.SelectedRule = this.AvailablesRules.First(r => r.Key.Contains("DTV"));
#endif
            this.CloseCommand = new RelayCommand(this.Close);
            this.CancelCommand = new RelayCommand(this.Cancel);

            this.InitializeComponent();
        }

        #endregion

        #region Public Properties

        public ObservableCollection<KeyValuePair<string, string>> AvailablesRules { get; set; }

        public ICommand CancelCommand { get; set; }

        public ICommand CloseCommand { get; set; }

        public DateTime? DateFrom { get; set; }

        public DateTime? DateTo { get; set; }

        public string EventTitle { get; set; }

        public string FileName { get; set; }

        public bool IsOpen { get; set; }

        public KeyValuePair<string, string>? SelectedRule { get; set; }

        #endregion

        #region Methods

        private void Cancel()
        {
            this.IsOpen = false;
            this.SelectedRule = null;
            MainWindow.MainWindowHandle.CloseDialog();
        }

        private void Close()
        {
            if (string.IsNullOrEmpty(this.EventTitle))
            {
                MainWindow.MainWindowHandle.ShowMessageAsync(
                    LocalizationService.Resolve(() => Text.EventTitleMustNotBeEmpty));

                return;
            }

            if (this.DateFrom == null)
            {
                MainWindow.MainWindowHandle.ShowMessage(LocalizationService.Resolve(() => Text.StartDateOfEventInvalid));

                return;
            }
            MainWindow.MainWindowHandle.CloseDialog();
            this.IsOpen = false;
        }

        #endregion
    }
}