﻿// // TPS.net TPS8 Scrutinus
// // EditCompetitionData.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows.Controls;
using Scrutinus.Dialogs.DialogViewModels;

namespace Scrutinus.Dialogs
{
    /// <summary>
    ///     Interaction logic for EditCompetitionData.xaml
    /// </summary>
    public partial class EditCompetitionData : Page
    {
        #region Constructors and Destructors

        public EditCompetitionData(int competitionId)
        {
            var viewModel = new EditCompetitionViewData(competitionId);
            this.DataContext = viewModel;

            var currentClass = viewModel.Competition.Class;
            var currentType = viewModel.Competition.CompetitionType;
            var currentAgeGroup = viewModel.Competition.AgeGroup;

            this.InitializeComponent();

            // Work Arround: Under XP during data binding, the following fields are set
            // to null, so we set them again:
            viewModel.Competition.CompetitionType = currentType;
            viewModel.Competition.AgeGroup = currentAgeGroup;
            viewModel.Competition.Class = currentClass;
        }

        #endregion
    }
}