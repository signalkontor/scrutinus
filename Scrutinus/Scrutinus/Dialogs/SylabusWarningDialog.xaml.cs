﻿// // TPS.net TPS8 Scrutinus
// // SylabusWarningDialog.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Windows.Controls;
using DataModel.Models;
using Scrutinus.Dialogs.DialogViewModels;

namespace Scrutinus.Dialogs
{
    /// <summary>
    ///     Interaction logic for SylabusWarningDialog.xaml
    /// </summary>
    public partial class SylabusWarningDialog : Page
    {
        #region Constructors and Destructors

        public SylabusWarningDialog(
            ScrutinusContext context,
            IEnumerable<Participant> participants,
            Participant selectedParticipant,
            Round round)
        {
            this.InitializeComponent();

            this.DataContext = new SylabusWarningDialogViewModel(context, participants, selectedParticipant, round);
        }

        #endregion
    }
}