﻿// // TPS.net TPS8 Scrutinus
// // DialogManagerViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows.Controls;

namespace EKA.LisaLite.UI.CommonResources.DialogManager
{
    /// <summary>
    ///     The view model of the dialog manager
    /// </summary>
    public class DialogManagerViewModel
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets the height.
        /// </summary>
        /// <value>
        ///     The height.
        /// </value>
        public double Height { get; set; }

        /// <summary>
        ///     Gets or sets the page.
        /// </summary>
        /// <value>
        ///     The page.
        /// </value>
        public Page Page { get; set; }

        /// <summary>
        ///     Gets or sets the width.
        /// </summary>
        /// <value>
        ///     The width.
        /// </value>
        public double Width { get; set; }

        #endregion
    }
}