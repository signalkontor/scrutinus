﻿// // TPS.net TPS8 Scrutinus
// // ImageExtensions.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace EKA.LisaLite.UI.CommonResources.DialogManager
{
    /// <summary>
    ///     Extension Methods for Images
    /// </summary>
    public static class ImageExtensions
    {
        #region Methods

        private static BitmapSource CaptureScreen(Visual target, double dpiX, double dpiY)
        {
            if (target == null)
            {
                return null;
            }

            var bounds = VisualTreeHelper.GetDescendantBounds(target);
            var rtb = new RenderTargetBitmap(
                (int)(bounds.Width * dpiX / 96.0),
                (int)(bounds.Height * dpiY / 96.0),
                dpiX,
                dpiY,
                PixelFormats.Pbgra32);
            var dv = new DrawingVisual();
            using (var ctx = dv.RenderOpen())
            {
                var vb = new VisualBrush(target);
                vb.Opacity = 0.5;
                ctx.DrawRectangle(vb, null, new Rect(new Point(), bounds.Size));
            }

            rtb.Render(dv);
            return rtb;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Captures the image.
        /// </summary>
        /// <param name="me">The instance of the framework element</param>
        /// <param name="ensureSize">if set to <c>true</c> [ensure size].</param>
        /// <returns>The created image</returns>
        public static Image CaptureImage(this FrameworkElement me, bool ensureSize = false)
        {
            var width = Convert.ToInt32(me.ActualWidth);
            width = width == 0 ? 1 : width;
            var height = Convert.ToInt32(me.ActualHeight);
            height = height == 0 ? 1 : height;

            var image = CaptureScreen(me, 96, 96);

            var img = new Image
                          {
                              Source = image,
                              Stretch = Stretch.None,
                              Width = width - (ensureSize ? 1 : 0),
                              Height = height - (ensureSize ? 1 : 0)
                          };

            return img;
        }

        /// <summary>
        ///     Creates the bitmap from resource.
        /// </summary>
        /// <param name="image">The image.</param>
        /// <param name="callingType">Type of the calling.</param>
        /// <param name="relativePath">The relative path.</param>
        public static void FromBitmapResource(this Image image, Type callingType, string relativePath)
        {
            var assemblyName = callingType.Assembly.FullName;
            var bi =
                new BitmapImage(
                    new Uri(
                        string.Format("pack://application:,,,/{0};component/{1}", assemblyName, relativePath),
                        UriKind.RelativeOrAbsolute));
            image.Source = bi;
        }

        #endregion
    }
}