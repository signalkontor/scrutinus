﻿// // TPS.net TPS8 Scrutinus
// // DialogLayeringHelper.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace EKA.LisaLite.UI.CommonResources.DialogManager
{
    /// <summary>
    ///     A helper class for layer management used in the dialog manager implementation
    /// </summary>
    internal class DialogLayeringHelper
    {
        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="DialogLayeringHelper" /> class.
        /// </summary>
        /// <param name="parent">The content to be replaces by the dialog</param>
        public DialogLayeringHelper(ContentControl parent)
        {
            this.parent = parent;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets a value indicating whether this instance has dialog layers.
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance has dialog layers; otherwise, <c>false</c>.
        /// </value>
        public bool HasDialogLayers
        {
            get
            {
                return this.layerStack.Any();
            }
        }

        #endregion

        #region Fields

        /// <summary>
        ///     The layer stack
        /// </summary>
        private readonly List<object> layerStack = new List<object>();

        /// <summary>
        ///     The parent control
        /// </summary>
        private readonly ContentControl parent;

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Gets the content of the current.
        /// </summary>
        /// <returns>The current content</returns>
        public FrameworkElement GetCurrentContent()
        {
            return this.parent;
        }

        /// <summary>
        ///     Hides the dialog.
        /// </summary>
        /// <param name="dialog">The dialog.</param>
        public void HideDialog()
        {
            if (this.layerStack.Count > 0)
            {
                var oldContent = this.layerStack.Last();
                this.layerStack.Remove(oldContent);
                this.parent.Content = oldContent;
            }
        }

        /// <summary>
        ///     Shows the dialog.
        /// </summary>
        /// <param name="dialog">The dialog.</param>
        public void ShowDialog(FrameworkElement dialog)
        {
            this.layerStack.Add(this.parent.Content);
            this.parent.Content = dialog;
        }

        #endregion
    }
}