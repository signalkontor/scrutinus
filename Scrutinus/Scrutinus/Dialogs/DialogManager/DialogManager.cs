﻿// // TPS.net TPS8 Scrutinus
// // DialogManager.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Windows.Controls;
using System.Windows.Threading;
using EKA.LisaLite.UI.CommonResources.DialogManager;

namespace Scrutinus.Dialogs.DialogManager
{
    /// <summary>
    ///     Implementation of the dialog manager
    /// </summary>
    public class DialogManager : IDialogManager
    {
        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="DialogManager" /> class.
        /// </summary>
        /// <param name="parentControl">The parent control.</param>
        /// <param name="dispatcher">The dispatcher.</param>
        /// <param name="navigationFrame">The navigation frame.</param>
        public DialogManager(ContentControl parentControl, Dispatcher dispatcher)
        {
            this.dispatcher = dispatcher;
            this.layeringHelper = new DialogLayeringHelper(parentControl);
            this.DialogIsOpen = false;
        }

        #endregion

        #region Public Properties

        public bool DialogIsOpen { get; private set; }

        #endregion

        #region Methods

        /// <summary>
        ///     Invokes the UI call.
        /// </summary>
        /// <param name="del">The delete.</param>
        private void InvokeUiCall(Action del)
        {
            this.dispatcher.Invoke(del, DispatcherPriority.DataBind);
        }

        #endregion

        #region Fields

        /// <summary>
        ///     The dispatcher
        /// </summary>
        private readonly Dispatcher dispatcher;

        /// <summary>
        ///     The layering helper
        /// </summary>
        private readonly DialogLayeringHelper layeringHelper;

        /// <summary>
        ///     The closing action
        /// </summary>
        private Action closingAction;

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Closes the specified dialog.
        /// </summary>
        public void Close()
        {
            this.InvokeUiCall(() => this.layeringHelper.HideDialog());
            if (this.closingAction != null)
            {
                this.InvokeUiCall(this.closingAction);
            }

            this.DialogIsOpen = false;
        }

        /// <summary>
        ///     Shows the specified page.
        /// </summary>
        /// <param name="page">The page.</param>
        /// <param name="actionClosing">The action closing.</param>
        /// <param name="width">The width of the dialog</param>
        /// <param name="height">The height of the dialog</param>
        public void Show(Page page, Action actionClosing, double width, double height)
        {
            if (this.DialogIsOpen)
            {
                return;
            }

            this.DialogIsOpen = true;
            this.closingAction = actionClosing;

            this.InvokeUiCall(
                () =>
                    {
                        var dialog = new DialogBaseControl(
                            this.layeringHelper.GetCurrentContent(),
                            new DialogManagerViewModel { Height = height, Width = width });
                        dialog.CustomContent.Content = page;
                        this.layeringHelper.ShowDialog(dialog);
                    });
        }

        #endregion
    }
}