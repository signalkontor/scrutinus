﻿// // TPS.net TPS8 Scrutinus
// // IDialogManager.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Windows.Controls;

namespace Scrutinus.Dialogs.DialogManager
{
    /// <summary>
    ///     An interface for a dialog manager
    /// </summary>
    public interface IDialogManager
    {
        #region Public Methods and Operators

        /// <summary>
        ///     Closes the specified dialog.
        /// </summary>
        /// <param name="dialog">The dialog.</param>
        void Close();

        /// <summary>
        ///     Shows the specified page.
        /// </summary>
        /// <param name="page">The page.</param>
        /// <param name="actionClosing">The action closing.</param>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        void Show(Page page, Action actionClosing, double width, double height);

        #endregion
    }
}