﻿// // TPS.net TPS8 Scrutinus
// // DialogBaseControl.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using EKA.LisaLite.UI.CommonResources.DialogManager;

namespace Scrutinus.Dialogs.DialogManager
{
    /// <summary>
    ///     Interaction logic for DialogBaseControl
    /// </summary>
    public partial class DialogBaseControl : UserControl
    {
        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="DialogBaseControl" /> class.
        /// </summary>
        /// <param name="originalContent">Content of the original.</param>
        /// <param name="viewModel">The view model for the dialog manager</param>
        public DialogBaseControl(FrameworkElement originalContent, DialogManagerViewModel viewModel)
        {
            this.InitializeComponent();

            var backgroundImage = originalContent.CaptureImage();
            backgroundImage.Stretch = Stretch.None;

            this.BackgroundImageHolder.Content = backgroundImage;

            this.DataContext = viewModel;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Connects the specified connection identifier.
        /// </summary>
        /// <param name="connectionId">The connection identifier.</param>
        /// <param name="target">The target.</param>
        public void Connect(int connectionId, object target)
        {
        }

        #endregion
    }
}