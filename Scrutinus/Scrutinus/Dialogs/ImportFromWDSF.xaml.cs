﻿// // TPS.net TPS8 Scrutinus
// // ImportFromWDSF.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using DataModel;
using DataModel.Models;
using MahApps.Metro.Controls.Dialogs;
using Scrutinus.Localization;
using Wdsf.Api.Client;
using Wdsf.Api.Client.Models;
using Competition = DataModel.Models.Competition;
using Couple = DataModel.Models.Couple;
using Official = DataModel.Models.Official;

namespace Scrutinus.Dialogs
{
    /// <summary>
    ///     Interaction logic for ImportFromWDSF.xaml
    /// </summary>
    public partial class ImportFromWDSF : Page
    {
        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="ImportFromWDSF" /> class.
        /// </summary>
        /// <param name="context">
        ///     The context.
        /// </param>
        /// <param name="competition">
        ///     The competition.
        /// </param>
        public ImportFromWDSF(ScrutinusContext context, Competition competition)
        {
            this.InitializeComponent();
            this.context = context;
            this.competition = competition;
        }

        #endregion

        #region Fields

        /// <summary>
        ///     The competition.
        /// </summary>
        private readonly Competition competition;

        /// <summary>
        ///     The context.
        /// </summary>
        private readonly ScrutinusContext context;

        #endregion

        #region Methods

        private void CancelOnClick(object sender, RoutedEventArgs e)
        {
            MainWindow.MainWindowHandle.CloseDialog();
        }

        /// <summary>
        ///     The do import.
        /// </summary>
        /// <param name="importAll">
        ///     The import all.
        /// </param>
        /// <param name="importTeams">
        ///     The import teams.
        /// </param>
        /// <param name="importSingle">
        ///     The import single.
        /// </param>
        private void DoImport(bool importAll, bool importTeams, bool importSingle, bool importJudges)
        {
#if DEBUG
            // var apiClient = new Client("guest", "guest", WdsfEndpoint.Sandbox);
            var apiClient = new Client(
                Settings.Default.WDSFApiUser,
                Settings.Default.WDSFApiPassword,
                WdsfEndpoint.Services);
#else
            var apiClient = new Client(
                Settings.Default.WDSFApiUser,
                Settings.Default.WDSFApiPassword,
                WdsfEndpoint.Services);
#endif

            var wdsfId = 0;
            if (!int.TryParse(this.competition.ExternalId, out wdsfId))
            {
                MainWindow.MainWindowHandle.ShowMessage(
                    LocalizationService.Resolve(() => Text.NoValidWDSFCompetitionId));
                return;
            }

            var wdsfCompetition = apiClient.GetCompetition(wdsfId);

            if (importTeams)
            {
                this.DoImportTeams(wdsfCompetition, apiClient);
                MainWindow.MainWindowHandle.CloseDialog();
                return;
            }

            if (importSingle)
            {
                this.DoImportSingle(wdsfCompetition, apiClient);
                MainWindow.MainWindowHandle.CloseDialog();
                return;
            }

            if (importJudges)
            {
                this.DoImportJudges();
            }

            var participants = apiClient.GetCoupleParticipants(wdsfCompetition.Id);
            var count = 0;

            this.Dispatcher.Invoke(new Action(() => this.ImportProgress.Maximum = participants.Count));

            foreach (var participant in participants)
            {
                try
                {
                    count++;

                    this.Dispatcher.Invoke(new Action(() => { this.ImportProgress.Value = count; }));

                    var details = apiClient.GetCoupleParticipant(participant.Id);
                    if (details.Rank == "0" || importAll)
                    {
                        var firstNameMan = string.Empty;
                        var firstNameWoman = string.Empty;
                        var LastNameMan = string.Empty;
                        var LastNameWoman = string.Empty;

                        var wdsfCouple = apiClient.GetCouple(details.CoupleId);

                        if (wdsfCouple.ManMinSpecified)
                        {
                            var man = apiClient.GetPerson(wdsfCouple.ManMin);
                            firstNameMan = man.Name;
                            LastNameMan = man.Surname;
                        }
                        else
                        {
                            var data = wdsfCouple.UnknownManName.Split(',');
                            firstNameMan = data[1];
                            LastNameMan = data[0];
                        }

                        if (wdsfCouple.WomanMinSpecified)
                        {
                            var woman = apiClient.GetPerson(wdsfCouple.WomanMin);
                            firstNameWoman = woman.Name;
                            LastNameWoman = woman.Surname;
                        }
                        else
                        {
                            var data = wdsfCouple.UnknownWomanName.Split(',');
                            firstNameWoman = data[1];
                            LastNameWoman = data[0];
                        }

                        // Do we have it already?
                        var minman = wdsfCouple.ManMin.ToString();
                        var minwoman = wdsfCouple.WomanMin.ToString();
                        var couple = this.context.Couples.SingleOrDefault(c => c.ExternalId == wdsfCouple.Id);

                        // Import this Couple ...
                        if (couple == null)
                        {
                            couple = new Couple
                                         {
                                             Athlete_Id_Man = minman,
                                             Athlete_Id_Woman = minwoman,
                                             MINMan = minman,
                                             MINWoman = minwoman,
                                             FirstMan = firstNameMan,
                                             LastMan = LastNameMan,
                                             FirstWoman = firstNameWoman,
                                             LastWoman = LastNameWoman,
                                             Country = wdsfCouple.Country,
                                             ExternalId = wdsfCouple.Id
                                         };
                            this.context.Couples.Add(couple);
                            this.context.SaveChanges();
                        }

                        // Add Participant
                        var newParticipant =
                            this.context.Participants.SingleOrDefault(
                                p => p.Couple.Id == couple.Id && p.Competition.Id == this.competition.Id);

                        if (newParticipant == null)
                        {
                            newParticipant = new Participant
                                                 {
                                                     Number = int.Parse(participant.StartNumber),
                                                     Couple = couple,
                                                     Competition = this.competition,
                                                     Star = 0,
                                                     State = CoupleState.Dancing
                                                 };
                            this.context.Participants.Add(newParticipant);
                        }

                        // Maybe the number has been changed
                        var number = 0;
                        if (int.TryParse(participant.StartNumber, out number))
                        {
                            newParticipant.Number = number;
                        }
                        else
                        {
                            newParticipant.Number = 0;
                        }
                    }

                    this.context.SaveChanges();
                }
                catch (Exception ex)
                {
                    var res = MainWindow.MainWindowHandle.ShowMessage(
                        ex.Message,
                        "Continue?",
                        MessageDialogStyle.AffirmativeAndNegative);

                    if (res == MessageDialogResult.Negative)
                    {
                        return;
                    }
                }
            }

            this.context.SaveChanges();

            MainWindow.MainWindowHandle.CloseDialog();
        }

        /// <summary>
        ///     The do import judges.
        /// </summary>
        private void DoImportJudges()
        {
#if DEBUG
            // var apiClient = new Client("guest", "guest", WdsfEndpoint.Sandbox);
            var apiClient = new Client(
                Settings.Default.WDSFApiUser,
                Settings.Default.WDSFApiPassword,
                WdsfEndpoint.Services);
#else
            var apiClient = new Client(
                Settings.Default.WDSFApiUser,
                Settings.Default.WDSFApiPassword,
                WdsfEndpoint.Services);
#endif
            var comId = 0;
            if (int.TryParse(this.competition.ExternalId, out comId))
            {
                var officials = apiClient.GetOfficials(comId);
                foreach (var official in officials)
                {
                    var details = apiClient.GetOfficial(official.Id);
                    var o = new Official
                                {
                                    Firstname = official.Name,
                                    Nationality = context.CountryCodes.FirstOrDefault(c => c.LongName == official.Nationality),
                                    Sign = details.AdjudicatorChar,
                                    MIN = details.Min
                                };

                    if (details.Task == "Adjudicator")
                    {
                        o.Roles.Add(this.context.Roles.Single(r => r.Id == Roles.Judge));
                    }

                    this.context.Officials.Add(o);
                    this.context.SaveChanges();
                }
            }
            else
            {
                MainWindow.MainWindowHandle.ShowMessage(
                    LocalizationService.Resolve(() => Text.NoValidWDSFCompetitionId));
            }
        }

        /// <summary>
        ///     The do import single.
        /// </summary>
        /// <param name="wdsfCompetition">
        ///     The wdsf competition.
        /// </param>
        /// <param name="apiClient">
        ///     The api client.
        /// </param>
        private void DoImportSingle(CompetitionDetail wdsfCompetition, Client apiClient)
        {
            var singles = apiClient.GetSingleParticipants(wdsfCompetition.Id);

            this.Dispatcher.Invoke(new Action(() => this.ImportProgress.Maximum = singles.Count));
            var count = 0;

            foreach (var single in singles)
            {
                count++;
                this.Dispatcher.Invoke(new Action(() => { this.ImportProgress.Value = count; }));

                try
                {
                    var sId = single.Id.ToString();

                    var participant = this.context.Participants.SingleOrDefault(c => c.Couple.ExternalId == sId);
                    if (participant == null)
                    {
                        var c = new Couple
                                    {
                                        FirstMan = single.Name,
                                        Country = single.Country,
                                        IsTeam = false,
                                        IsSingle = true,
                                        ExternalId = sId
                                    };

                        this.context.Couples.Add(c);
                        this.context.SaveChanges();

                        var number = int.Parse(single.StartNumber);

                        var p = new Participant
                                    {
                                        Number = number,
                                        Couple = c,
                                        Competition = this.competition,
                                        Star = 0,
                                        State = CoupleState.Dancing
                                    };
                        this.context.Participants.Add(p);
                        this.context.SaveChanges();
                    }
                    else
                    {
                        participant.Couple.Country = single.Country;
                        this.context.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    var res = MainWindow.MainWindowHandle.ShowMessage(
                        ex.Message,
                        "Continue?",
                        MessageDialogStyle.AffirmativeAndNegative);

                    if (res == MessageDialogResult.Negative)
                    {
                        return;
                    }
                }
            }
        }

        /// <summary>
        ///     The do import teams.
        /// </summary>
        /// <param name="competition">
        ///     The competition.
        /// </param>
        /// <param name="apiClient">
        ///     The api client.
        /// </param>
        private void DoImportTeams(CompetitionDetail competition, Client apiClient)
        {
            var teams = apiClient.GetTeamParticipants(competition.Id);

            this.Dispatcher.Invoke(new Action(() => this.ImportProgress.Maximum = teams.Count));
            var count = 0;

            foreach (var WdsfTeam in teams)
            {
                count++;
                this.Dispatcher.Invoke(new Action(() => { this.ImportProgress.Value = count; }));

                try
                {
                    var sTeamId = WdsfTeam.Id.ToString();
                    var WdsfTeamDetail = apiClient.GetTeamParticipant(WdsfTeam.Id);
                    var team = this.context.Couples.SingleOrDefault(c => c.ExternalId == sTeamId);
                    if (team == null)
                    {
                        team = new Couple { FirstMan = WdsfTeam.Name, Country = WdsfTeam.Country, ExternalId = sTeamId };
                        this.context.Couples.Add(team);
                        this.context.SaveChanges();
                    }

                    var participant =
                        this.context.Participants.SingleOrDefault(
                            p => p.Couple.Id == team.Id && p.Competition.Id == competition.Id);
                    if (participant == null)
                    {
                        // Todo: Check if this realy works
                        var scrutinusCompetition =
                            this.context.Competitions.SingleOrDefault(c => c.ExternalId == competition.Id.ToString());
                        participant = new Participant { Competition = scrutinusCompetition, Couple = team, };
                        this.context.Participants.Add(participant);
                    }

                    team.IsTeam = true;
                    participant.Number = WdsfTeamDetail.StartNumber;
                    this.context.SaveChanges();
                }
                catch (Exception ex)
                {
                    var res = MainWindow.MainWindowHandle.ShowMessage(
                        ex.Message,
                        "Continue?",
                        MessageDialogStyle.AffirmativeAndNegative);

                    if (res == MessageDialogResult.Negative)
                    {
                        return;
                    }
                }
            }

            this.context.SaveChanges();
            MainWindow.MainWindowHandle.CloseDialog();
        }

        /// <summary>
        ///     The import_ on click.
        /// </summary>
        /// <param name="sender">
        ///     The sender.
        /// </param>
        /// <param name="e">
        ///     The e.
        /// </param>
        private void Import_OnClick(object sender, RoutedEventArgs e)
        {
            this.Import.IsEnabled = false;
            var all = this.ImportAll.IsChecked.HasValue && this.ImportAll.IsChecked.Value;
            var teams = this.ImportTeam.IsChecked.HasValue && this.ImportTeam.IsChecked.Value;
            var single = this.ImportSingle.IsChecked.HasValue && this.ImportSingle.IsChecked.Value;
            var judges = this.ImportJudges.IsChecked.HasValue && this.ImportJudges.IsChecked.Value;

            var task = new Task(() => this.DoImport(all, teams, single, judges));
            task.ContinueWith(t => { MainWindow.MainWindowHandle.ShowMessage($"Import done"); });
            task.Start();
        }

        #endregion
    }
}