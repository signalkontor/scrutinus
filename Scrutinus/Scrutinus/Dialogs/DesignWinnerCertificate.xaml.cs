﻿// // TPS.net TPS8 Scrutinus
// // DesignWinnerCertificate.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Scrutinus.Dialogs.DialogViewModels;
using Scrutinus.Localization;

namespace Scrutinus.Dialogs
{
    /// <summary>
    ///     Interaction logic for DesignWinnerCertificate.xaml
    /// </summary>
    public partial class DesignWinnerCertificate : Page
    {
        #region Fields

        private readonly DesignWinnerCertificateViewModel viewModel;

        #endregion

        #region Constructors and Destructors

        public DesignWinnerCertificate()
        {
            this.InitializeComponent();

            this.viewModel = this.DataContext as DesignWinnerCertificateViewModel;

            foreach (var winnerCertificateDefinition in this.viewModel.WinnerCertificateDefinitions)
            {
                winnerCertificateDefinition.Elements.CollectionChanged += this.ElementsOnCollectionChanged;
                winnerCertificateDefinition.PropertyChanged += (sender, args) =>
                    {
                        this.DrawCanvas();
                    };

                foreach (var winnerCertificateElement in winnerCertificateDefinition.Elements)
                {
                    winnerCertificateElement.PropertyChanged += this.WinnerCertificateElementOnPropertyChanged;
                }
            }

            this.viewModel.WinnerCertificateDefinitions.CollectionChanged += this.WinnerCertificateDefinitionsOnCollectionChanged;

            this.viewModel.PropertyChanged += this.ViewModelOnPropertyChanged;

            if (this.viewModel.SelectedWinnerCertificateDefinition != null)
            {
                this.DrawCanvas();
            }
        }

        #endregion

        private void ColorPicker_OnSelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color?> e)
        {
            
        }

        #region Methods

        private void CreateUiElement(WinnerCertificateElement item)
        {
            var border = new Border
                             {
                                 BorderBrush = Brushes.Black,
                                 BorderThickness = new Thickness(1),
                                 Background = Brushes.Transparent,
                                 IsHitTestVisible = false
                             };

            if (item.Rotation > 0)
            {
                border.LayoutTransform = new RotateTransform(item.Rotation);
            }

            var text = LocalizationService.Resolve("Scrutinus:Text:WinnerCertificateElementTypesDisplay_" + item.ElementType);

            if (item.ElementType == WinnerCertificateElementTypes.StaticText)
            {
                text = item.Text;
            }

            if (item.ElementType == WinnerCertificateElementTypes.Picture)
            {
                var src = new BitmapImage();
                if (File.Exists(item.Text))
                {
                    src.BeginInit();
                    src.UriSource = new Uri(item.Text, UriKind.Relative);
                    src.CacheOption = BitmapCacheOption.OnLoad;
                    src.EndInit();
                }

                border.Width = item.WidthInPixel;
                border.Height = item.HeightInPixel;
                border.Child = new Image() { Source =src, Width = item.WidthInPixel, Height = item.HeightInPixel, Stretch = Stretch.Fill };
            }
            else
            {
                border.Child = new TextBlock
                {
                    Text = text,
                    Background = Brushes.Transparent,
                    Foreground = new SolidColorBrush(item.Color),
                    FontFamily = item.FontFamily,
                    FontSize = item.FontSize,
                    FontWeight = FontWeight.FromOpenTypeWeight(item.FontWeight),
                    HorizontalAlignment = item.HorizontalAlignment,
                    VerticalAlignment = item.VerticalAlignment,
                    IsHitTestVisible = false
                };

            }

            if (item.HorizontalAlignment == HorizontalAlignment.Center)
            {
                var width = this.DesignerCanvas.Width
                            - this.viewModel.SelectedWinnerCertificateDefinition.RightMargin / 2.54 * 96
                            - this.viewModel.SelectedWinnerCertificateDefinition.LeftMargin / 2.54 * 96;

                var left = this.viewModel.SelectedWinnerCertificateDefinition.LeftMargin / 2.54 * 96;

                item.LeftInPixel = left;
                item.Left = this.viewModel.SelectedWinnerCertificateDefinition.LeftMargin;
                border.SetValue(WidthProperty, width);
            }
            else
            {
                border.SetValue(WidthProperty, DependencyProperty.UnsetValue);
            }

            var style = this.FindResource("DesignerItemStyle") as Style;
            var contentControl = new ContentControl { Content = border, Style = style, Tag = item };

            var topBinding = new Binding("TopInPixel") { Source = item, Mode = BindingMode.TwoWay };
            contentControl.SetBinding(Canvas.TopProperty, topBinding);

            var leftBinding = new Binding("LeftInPixel") { Source = item, Mode = BindingMode.TwoWay };
            contentControl.SetBinding(Canvas.LeftProperty, leftBinding);

            contentControl.MouseDoubleClick += (sender, args) =>
                {
                    var control = sender as ContentControl;
                    if (control != null)
                    {
                        Selector.SetIsSelected(control, true);
                    }
                };

            item.UiElementOnCanvas = contentControl;

            // Left / Top recaluclate from cm to pixel
            Canvas.SetLeft(item.UiElementOnCanvas, item.LeftInPixel);
            Canvas.SetTop(item.UiElementOnCanvas, item.TopInPixel);

            this.DesignerCanvas.Children.Add(item.UiElementOnCanvas);
        }

        private void DrawCanvas()
        {
            this.DesignerCanvas.Children.Clear();

            this.DrawHelpLines();

            foreach (var element in this.viewModel.SelectedWinnerCertificateDefinition.Elements)
            {
                this.CreateUiElement(element);
            }
        }

        private void DrawHelpLines()
        {
            var lineLeft = new Line
                               {
                                   X1 = this.viewModel.SelectedWinnerCertificateDefinition.LeftMargin / 2.54 * 96,
                                   X2 = this.viewModel.SelectedWinnerCertificateDefinition.LeftMargin / 2.54 * 96,
                                   Y1 = 0,
                                   Y2 = this.DesignerCanvas.Height,
                                   Stroke = Brushes.Gray,
                                   StrokeThickness = 2
                               };
            this.DesignerCanvas.Children.Add(lineLeft);

            var lineRight = new Line
                                {
                                    X1 = this.DesignerCanvas.Width - this.viewModel.SelectedWinnerCertificateDefinition.RightMargin / 2.54 * 96,
                                    X2 = this.DesignerCanvas.Width - this.viewModel.SelectedWinnerCertificateDefinition.RightMargin / 2.54 * 96,
                                    Y1 = 0,
                                    Y2 = this.DesignerCanvas.Height,
                                    Stroke = Brushes.Gray,
                                    StrokeThickness = 2
                                };
            this.DesignerCanvas.Children.Add(lineRight);

            var lineTop = new Line
                              {
                                  X1 = 0,
                                  X2 = this.DesignerCanvas.Width,
                                  Y1 = 1 / 2.54 * 96,
                                  Y2 = 1 / 2.54 * 96,
                                  Stroke = Brushes.Gray,
                                  StrokeThickness = 2
                              };
            this.DesignerCanvas.Children.Add(lineTop);

            var lineBottom = new Line
                                 {
                                     X1 = 0,
                                     X2 = this.DesignerCanvas.Width,
                                     Y1 = this.DesignerCanvas.Height - 1 / 2.54 * 96,
                                     Y2 = this.DesignerCanvas.Height - 1 / 2.54 * 96,
                                     Stroke = Brushes.Gray,
                                     StrokeThickness = 2
                                 };
            this.DesignerCanvas.Children.Add(lineBottom);

            var lineCenter = new Line
                                 {
                                     X1 = this.DesignerCanvas.Width / 2,
                                     X2 = this.DesignerCanvas.Width / 2,
                                     Y1 = 0,
                                     Y2 = this.DesignerCanvas.Height,
                                     Stroke = Brushes.Black,
                                     StrokeThickness = 1,
                                     StrokeDashArray = { 2, 4 }
                                 };

            this.DesignerCanvas.Children.Add(lineCenter);

            lineCenter = new Line
                             {
                                 X1 = 0,
                                 X2 = this.DesignerCanvas.Width,
                                 Y1 = this.DesignerCanvas.Height / 2,
                                 Y2 = this.DesignerCanvas.Height / 2,
                                 Stroke = Brushes.Black,
                                 StrokeThickness = 1,
                                 StrokeDashArray = { 2, 4 }
                             };

            this.DesignerCanvas.Children.Add(lineCenter);
        }

        private void ElementsOnCollectionChanged(
            object sender,
            NotifyCollectionChangedEventArgs notifyCollectionChangedEventArgs)
        {
            if (notifyCollectionChangedEventArgs.NewItems != null)
            {
                foreach (var newItem in notifyCollectionChangedEventArgs.NewItems)
                {
                    var item = newItem as WinnerCertificateElement;

                    if (item != null)
                    {
                        item.PropertyChanged += this.WinnerCertificateElementOnPropertyChanged;

                        this.CreateUiElement(item);
                    }
                }
            }
            if (notifyCollectionChangedEventArgs.OldItems != null)
            {
                foreach (var oldItem in notifyCollectionChangedEventArgs.OldItems)
                {
                    var item = oldItem as WinnerCertificateElement;
                    if (item != null)
                    {
                        this.DesignerCanvas.Children.Remove(item.UiElementOnCanvas);
                    }
                }
            }
        }

        private void PositionChangedHandler(object sender, EventArgs eventArgs)
        {
            var contentControl = sender as ContentControl;
            if (contentControl == null)
            {
                return;
            }
            var element = contentControl.Tag as WinnerCertificateElement;
            if (element == null)
            {
                return;
            }

            element.LeftInPixel = (double)contentControl.GetValue(Canvas.LeftProperty);
            Debug.WriteLine(contentControl.GetValue(Canvas.TopProperty));
            element.TopInPixel = (double)contentControl.GetValue(Canvas.TopProperty);
        }

        private void ViewModelOnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            if (propertyChangedEventArgs.PropertyName == "SelectedWinnerCertificateDefinition"
                && this.viewModel.SelectedWinnerCertificateDefinition != null)
            {
                this.DrawCanvas();
            }

            if (propertyChangedEventArgs.PropertyName == "SelectedWinnerCertificateElement"
                && this.viewModel.SelectedWinnerCertificateDefinition != null)
            {
                this.DrawCanvas();
            }
        }

        private void WinnerCertificateDefinitionsOnCollectionChanged(
            object sender,
            NotifyCollectionChangedEventArgs notifyCollectionChangedEventArgs)
        {
            if (notifyCollectionChangedEventArgs.NewItems != null)
            {
                foreach (var newItem in notifyCollectionChangedEventArgs.NewItems)
                {
                    var item = newItem as WinnerCertificateDefinition;
                    item.Elements.CollectionChanged += this.ElementsOnCollectionChanged;
                }
            }

            if (notifyCollectionChangedEventArgs.OldItems != null)
            {
                foreach (var oldItem in notifyCollectionChangedEventArgs.OldItems)
                {
                    var item = oldItem as WinnerCertificateDefinition;
                    item.Elements.CollectionChanged -= this.ElementsOnCollectionChanged;
                }
            }
        }

        private void WinnerCertificateElementOnPropertyChanged(
            object sender,
            PropertyChangedEventArgs propertyChangedEventArgs)
        {
            if (propertyChangedEventArgs.PropertyName == "LeftInPixel" || propertyChangedEventArgs.PropertyName == "Left")
            {
                return;
            }

            var element = sender as WinnerCertificateElement;

            if (element == null || element.UiElementOnCanvas == null)
            {
                return;
            }

            var contentControl = element.UiElementOnCanvas as ContentControl;
            var border = contentControl.Content as Border;
            var textblock = border.Child as TextBlock;

            if (element.ElementType == WinnerCertificateElementTypes.Picture && propertyChangedEventArgs.PropertyName == "Text")
            {
                var image = new Image();

                var src = new BitmapImage();
                src.BeginInit();
                src.UriSource = new Uri(element.Text, UriKind.Relative);
                src.CacheOption = BitmapCacheOption.OnLoad;
                src.EndInit();

                image.Stretch = Stretch.Fill;
                image.Source = src;

                border.Child = image;
            }

            var text = LocalizationService.Resolve("Scrutinus:Text:WinnerCertificateElementTypesDisplay_" + element.ElementType);

            if (element.ElementType == WinnerCertificateElementTypes.StaticText)
            {
                text = element.Text;
            }

            if (element.ElementType == WinnerCertificateElementTypes.Picture)
            {
                border.Width = element.WidthInPixel;
                border.Height = element.HeightInPixel;
                var image = border.Child as Image;
                if (image != null)
                {
                    image.Width = element.WidthInPixel;
                    image.Height = element.HeightInPixel;
                }
            }

            if (textblock != null)
            {
                textblock.Text = text;
                textblock.Background = Brushes.Transparent;
                textblock.Foreground = new SolidColorBrush(element.Color);
                textblock.FontFamily = element.FontFamily;
                textblock.FontSize = element.FontSize;
                textblock.FontWeight = FontWeight.FromOpenTypeWeight(element.FontWeight);
                textblock.HorizontalAlignment = element.HorizontalAlignment;
                textblock.VerticalAlignment = element.VerticalAlignment;
            }

            if (element.Rotation > 0)
            {
                border.LayoutTransform = new RotateTransform(element.Rotation);
            }

            if (element.HorizontalAlignment == HorizontalAlignment.Center)
            {
                var width = this.DesignerCanvas.Width
                            - this.viewModel.SelectedWinnerCertificateDefinition.RightMargin / 2.54 * 96
                            - this.viewModel.SelectedWinnerCertificateDefinition.LeftMargin / 2.54 * 96;

                var left = this.viewModel.SelectedWinnerCertificateDefinition.LeftMargin / 2.54 * 96;

                element.LeftInPixel = left;
                element.Left = this.viewModel.SelectedWinnerCertificateDefinition.LeftMargin;

                border.SetValue(WidthProperty, width);
                border.SetValue(Canvas.LeftProperty, left);
            }

        }

        #endregion
    }
}