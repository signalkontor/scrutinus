﻿// // TPS.net TPS8 Scrutinus
// // ImportLegacyTpsViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using DataModel;
using DataModel.Models;
using GalaSoft.MvvmLight.Command;
using TPSModel;
using Competition = DataModel.Models.Competition;
using Couple = DataModel.Models.Couple;
using Event = DataModel.Models.Event;

namespace Scrutinus.Dialogs.DialogViewModels
{
    public class ImportLegacyTpsViewModel
    {
        #region Constructors and Destructors

        public ImportLegacyTpsViewModel()
        {
            this.context = new ScrutinusContext();
            this.dispatcher = Dispatcher.CurrentDispatcher;
            this.ImportLegacyTpsCommand = new RelayCommand(this.ImportLegacyTps);
            this.CloseCommand = new RelayCommand(this.Close);

            this.TpsInfPath = "c:\\TPS7\\DatFiles";
        }

        #endregion

        #region Fields

        private readonly ScrutinusContext context;

        private readonly Dispatcher dispatcher;

        private Model tpsModel;

        #endregion

        #region Public Properties

        public ICommand CloseCommand { get; set; }

        public ICommand ImportLegacyTpsCommand { get; set; }

        public string TpsCompetitionPath { get; set; }

        public string TpsInfPath { get; set; }

        #endregion

        #region Methods

        private void Close()
        {
            MainWindow.MainWindowHandle?.CloseDialog();
        }

        private AgeGroup GetAgeGroup(string shortName)
        {
            var group = this.context.AgeGroups.FirstOrDefault(a => a.ShortName.ToLower() == shortName.ToLower());
            return group ?? this.context.AgeGroups.First(g => g.Id == 7);
        }

        private Class GetClass(string shortClass)
        {
            return this.context.Classes.FirstOrDefault(c => c.ClassShortName == shortClass)
                   ?? this.context.Classes.First(c => c.Id == 8);
        }

        private Role GetRoleFromTpsString(string tpsRole)
        {
            switch (tpsRole)
            {
                case "Judge":
                    return this.context.Roles.Single(r => r.Id == Roles.Judge);
                case "Scrutineer":
                    return this.context.Roles.Single(r => r.Id == Roles.Scrutineer);
                case "Associate":
                    return this.context.Roles.Single(r => r.Id == Roles.Associate);
                case "Speaker":
                    return this.context.Roles.Single(r => r.Id == Roles.Supervisor);
            }

            return this.context.Roles.Single(r => r.Id == Roles.Scrutineer);
        }

        private Section GetSection(string section)
        {
            return this.context.Sections.FirstOrDefault(s => s.SectionName == section) ?? this.context.Sections.First();
        }

        private void ImportCompetitions()
        {
            Event theEvent;
            if (this.context.Events.Any())
            {
                theEvent = this.context.Events.First();
            }
            else
            {
                theEvent = new Event { Title = "Michelpokal 2015", Executor = "Glinde / Saltatio" };
                this.context.Events.Add(theEvent);
            }

            this.context.SaveChanges();

            MainWindow.MainWindowHandle?.ReportStatus("Couples", 0, this.tpsModel.Competitions.Count);
            var count = 0;

            foreach (var tpsCompetition in this.tpsModel.Competitions)
            {
                MainWindow.MainWindowHandle?.ReportStatus("Competitions... ", count, this.tpsModel.Competitions.Count);
                count++;

                var competition = new Competition
                                      {
                                          Event = theEvent,
                                          Title = tpsCompetition.Title,
                                          AgeGroup = this.GetAgeGroup(tpsCompetition.AgeGroup),
                                          Section = this.GetSection(tpsCompetition.Section),
                                          Class = this.GetClass(tpsCompetition.StartClassGerman),
                                          CompetitionType = this.context.CompetitionTypes.Single(c => c.Id == 1),
                                          ExternalId = tpsCompetition.Id.ToString("0000"),
                                          DefaultCouplesPerHeat = 12,
                                          StartTime = tpsCompetition.StartTime,
                                          FirstRoundType = RoundTypes.QualificationRound,
                                          MarkingType = MarkingTypes.Marking,
                                          FinalType = MarkingTypes.Skating
                                      };

                Debug.WriteLine(competition.Title + " " + competition.ExternalId);

                this.context.Competitions.Add(competition);
                this.context.SaveChanges();

                this.dispatcher.Invoke(new Action(() => MainWindow.MainWindowHandle?.AddCompetitionToNavigationList(competition)));

                // Set dances of competition:
                var dances = this.GetDances(tpsCompetition.Dances);

                foreach (var dance in dances)
                {
                    var danceInCompetition = new DancesInCompetition
                                                 {
                                                     Competition = competition,
                                                     Dance = dance,
                                                     DanceOrder = dance.DefaultOrder,
                                                     IsSolo = false
                                                 };

                    this.context.DancesCompetition.Add(danceInCompetition);
                }

                this.context.SaveChanges();

                foreach (var tpsParticipant in tpsCompetition.Couples)
                {
                    var participant = new Participant
                                          {
                                              Couple =
                                                  this.context.Couples.First(
                                                      c =>
                                                      c.ExternalId == tpsParticipant.Couple.Id.ToString()),
                                              Number = tpsParticipant.Number,
                                              Competition = competition,
                                              State = CoupleState.Dancing,
                                              AgeGroup = competition.AgeGroup,
                                              Class = competition.Class
                                          };
                    this.context.Participants.Add(participant);
                }

                this.context.SaveChanges();

                foreach (var officalWithRole in tpsCompetition.Officals)
                {
                    var official = new OfficialInCompetition
                                       {
                                           Competition = competition,
                                           Official =
                                               this.context.Officials.First(
                                                   o => o.Sign == officalWithRole.Offical.Sign),
                                           Role = this.GetRoleFromTpsString(officalWithRole.Role)
                                       };

                    this.context.OfficialCompetition.Add(official);

                    if (!official.Official.Roles.Any(r => r.Id == official.Role.Id))
                    {
                        official.Official.Roles.Add(official.Role);
                    }
                }

                this.context.SaveChanges();

                foreach (var officalWithRole in tpsCompetition.Judges)
                {
                    var official = new OfficialInCompetition
                                       {
                                           Competition = competition,
                                           Official =
                                               this.context.Officials.First(
                                                   o => o.Sign == officalWithRole.Offical.Sign),
                                           Role =
                                               this.context.Roles.Single(r => r.Id == Roles.Judge),
                                       };

                    // ReSharper disable once SimplifyLinqExpression
                    if (!official.Official.Roles.Any(r => r.Id == official.Role.Id))
                    {
                        official.Official.Roles.Add(official.Role);
                    }

                    this.context.OfficialCompetition.Add(official);
                }
            }
            this.context.SaveChanges();
        }

        private IEnumerable<Dance> GetDances(List<string> dances)
        {
            var result = new List<Dance>();

            foreach (var d in dances)
            {
                switch (d)
                {
                    case "LW":
                        result.Add(this.context.Dances.Single(dance => dance.Id == 1));
                        break;
                    case "T":
                        result.Add(this.context.Dances.Single(dance => dance.Id == 2));
                        break;
                    case "WW":
                        result.Add(this.context.Dances.Single(dance => dance.Id == 3));
                        break;
                    case "SL":
                        result.Add(this.context.Dances.Single(dance => dance.Id == 4));
                        break;
                    case "Q":
                        result.Add(this.context.Dances.Single(dance => dance.Id == 5));
                        break;
                    case "S":
                        result.Add(this.context.Dances.Single(dance => dance.Id == 6));
                        break;
                    case "CC":
                        result.Add(this.context.Dances.Single(dance => dance.Id == 7));
                        break;
                    case "R":
                        result.Add(this.context.Dances.Single(dance => dance.Id == 8));
                        break;
                    case "PD":
                        result.Add(this.context.Dances.Single(dance => dance.Id == 9));
                        break;
                    case "J":
                        result.Add(this.context.Dances.Single(dance => dance.Id == 10));
                        break;
                    default:
                        throw new Exception("Unknown dance " + d);
                }
            }

            return result;
        }

        private void ImportCouples()
        {
            MainWindow.MainWindowHandle?.ReportStatus("Couples", 0, this.tpsModel.Couples.Count);

            foreach (var tpsCouple in this.tpsModel.Couples)
            {
                var couple = new Couple
                                 {
                                     FirstMan = tpsCouple.FirstNameHe,
                                     LastMan = tpsCouple.LastNameHe,
                                     FirstWoman = tpsCouple.FirstNameShe,
                                     LastWoman = tpsCouple.LastNameShe,
                                     Country = tpsCouple.Country,
                                     ExternalId = tpsCouple.Id.ToString()
                                 };
                this.context.Couples.Add(couple);
            }

            this.context.SaveChanges();
        }

        private void ImportLegacyTps()
        {
            this.ImportLegacyTpsAsync();
        }

        public Task ImportLegacyTpsAsync()
        {
            var task = Task.Factory.StartNew(
                () =>
                    {
                        this.tpsModel = new Model(this.TpsCompetitionPath, null, this.TpsInfPath, 0);

                        this.ImportCouples();
                        this.ImportOfficials();
                        this.ImportCompetitions();

                        MainWindow.MainWindowHandle?.RemoveStatusReport();
                    });

            this.Close();

            return task;
        }

        private void ImportOfficials()
        {
            MainWindow.MainWindowHandle?.ReportStatus("Officials", 0, this.tpsModel.Couples.Count);

            foreach (var tpsOffical in this.tpsModel.Officals)
            {
                var official = new Official
                                   {
                                       Firstname = tpsOffical.FirstName,
                                       Lastname = tpsOffical.LastName,
                                       Sign = tpsOffical.Sign,
                                       Club = tpsOffical.Club,
                                   };
                this.context.Officials.Add(official);
            }

            this.context.SaveChanges();
        }

        #endregion
    }
}