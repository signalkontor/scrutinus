﻿// // TPS.net TPS8 Scrutinus
// // AttachJudgingSheetsViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using DataModel.Models;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Microsoft.Win32;

namespace Scrutinus.Dialogs.DialogViewModels
{
    public class AttachJudgingSheetsViewModel : ViewModelBase
    {
        #region Fields

        private readonly Competition competition;

        private readonly ScrutinusContext context = new ScrutinusContext();

        #endregion

        #region Constructors and Destructors

        public AttachJudgingSheetsViewModel()
        {
            this.JudgingSheets = new ObservableCollection<JudgingSheetData>();

            this.AddCommand = new RelayCommand(this.Add);
            this.DeleteCommand = new RelayCommand(this.Delete);
            this.CancelCommand = new RelayCommand(this.Cancel);
            this.SaveCommand = new RelayCommand(this.Save);
        }

        public AttachJudgingSheetsViewModel(int competitionId)
            : this()
        {
            this.competition = this.context.Competitions.Single(c => c.Id == competitionId);
            this.JudgingSheets = new ObservableCollection<JudgingSheetData>(this.competition.JudgingSheets);
        }

        #endregion

        #region Public Properties

        public ICommand AddCommand { get; set; }

        public ICommand CancelCommand { get; set; }

        public ICommand DeleteCommand { get; set; }

        public ObservableCollection<JudgingSheetData> JudgingSheets { get; set; }

        public ICommand SaveCommand { get; set; }

        public JudgingSheetData SelectedItem { get; set; }

        #endregion

        #region Methods

        private void Add()
        {
            var openFileDialog = new OpenFileDialog();
            openFileDialog.CheckFileExists = true;
            openFileDialog.Multiselect = true;
            openFileDialog.Title = "Bitte wählen Sie die zu sendenden Dateien aus";
            openFileDialog.Filter = "PDF-Dateien (*.pdf)|*.pdf";

            var value = openFileDialog.ShowDialog();
            if (!value.HasValue || !value.Value)
            {
                return;
            }

            foreach (var fileName in openFileDialog.FileNames)
            {
                var sheet = new JudgingSheetData { Competition = this.competition, FileName = fileName };
                this.JudgingSheets.Add(sheet);
                this.context.JudgingSheets.Add(sheet);
            }
        }

        private void Cancel()
        {
            MainWindow.MainWindowHandle.CloseDialog();
        }

        private void Delete()
        {
            if (this.SelectedItem == null)
            {
                return;
            }

            this.context.JudgingSheets.Remove(this.SelectedItem);
            this.competition.JudgingSheets.Remove(this.SelectedItem);
            this.JudgingSheets.Remove(this.SelectedItem);
        }

        private void Save()
        {
            this.context.SaveChanges();
            this.Cancel();
        }

        #endregion
    }
}