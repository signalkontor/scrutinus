﻿// // TPS.net TPS8 Scrutinus
// // SplitCompetitionDialogViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using DataModel;
using DataModel.Models;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Scrutinus.Localization;

namespace Scrutinus.Dialogs.DialogViewModels
{
    public class SplittedCompetition : ViewModelBase
    {
        private string competitionTitle;

        private int fromPlace;

        private int toPlace;

        public string CompetitionTitle
        {
            get { return this.competitionTitle; }
            set {
                this.competitionTitle = value;
                this.RaisePropertyChanged(() => this.CompetitionTitle); }
        }

        public int FromPlace
        {
            get { return this.fromPlace; }
            set
            {
                this.fromPlace = value;
                this.RaisePropertyChanged(() => this.FromPlace);
            }
        }

        public int ToPlace
        {
            get { return this.toPlace; }
            set
            {
                this.toPlace = value;
                this.RaisePropertyChanged(() => this.ToPlace);
            }
        }
    }
    public class SplitCompetitionDialogViewModel : ViewModelBase
    {
        private SplittedCompetition splittedtCompetition;

        public SplitCompetitionDialogViewModel()
        {
            
        }

        public SplitCompetitionDialogViewModel(ScrutinusContext context, Competition competition)
        {
            this.Competition = competition;
            this.DatabaseContext = context;

            this.SplittedCompetitions = new ObservableCollection<SplittedCompetition>();

            this.NewSplittedtCompetition = new SplittedCompetition() { CompetitionTitle = this.Competition.Title + " - A Finale", FromPlace = 1, ToPlace = 6};

            this.AddSplittedtCompetitionCommand = new RelayCommand(this.AddSplittedCompetition);
            this.SplitCompetitionCommand = new RelayCommand(this.ExecuteSplit);
            this.DeleteSelectedSplitCommand = new RelayCommand(this.DeleteSplit);
            this.CloseDialogCommand = new RelayCommand(this.CloseDialog);
        }

        public ObservableCollection<SplittedCompetition> SplittedCompetitions { get; set; }

        public SplittedCompetition SelectedSplit { get; set; }

        public ICommand AddSplittedtCompetitionCommand { get; set; }

        public ICommand SplitCompetitionCommand { get; set; }

        public ICommand CloseDialogCommand { get; set; }

        public ICommand DeleteSelectedSplitCommand { get; set; }

        public ScrutinusContext DatabaseContext { get; set; }

        public Competition Competition { get; set; }

        public bool SplitPerformed { get; set; }

        public SplittedCompetition NewSplittedtCompetition
        {
            get { return this.splittedtCompetition; }
            set
            {
                this.splittedtCompetition = value;
                this.RaisePropertyChanged(() => this.NewSplittedtCompetition);
            }
        }

        private void AddSplittedCompetition()
        {
            var newEntry = new SplittedCompetition() {CompetitionTitle = this.NewSplittedtCompetition.CompetitionTitle + " New", FromPlace = this.NewSplittedtCompetition.ToPlace + 1, ToPlace = this.NewSplittedtCompetition.ToPlace + 7 };
            this.SplittedCompetitions.Add(this.NewSplittedtCompetition);
            this.NewSplittedtCompetition = newEntry;
        }

        private void ExecuteSplit()
        {
            if (!this.ValidateData())
            {
                return;
            }

            // Execute the splitting:
            foreach (var splittedCompetition in this.SplittedCompetitions)
            {
                // here we go, create a new Compeition and add the relevant participants:
                var newCompetition = new Competition()
                {
                    Event = this.Competition.Event,
                    Title = splittedCompetition.CompetitionTitle,
                    AgeGroup = this.Competition.AgeGroup,
                    Section = this.Competition.Section,
                    Class = this.Competition.Class,
                    CompetitionType = this.Competition.CompetitionType,
                    ParentCompetition = this.Competition,
                    PlaceOffset = splittedCompetition.FromPlace - 1,
                    StartTime = this.Competition.StartTime,
                    DefaultCouplesPerHeat = Settings.Default.DefaultCouplesPerHeat
                };

                foreach (var dancesInCompetition in this.Competition.Dances)
                {
                    var dance = new DancesInCompetition()
                                    {
                                        Competition = newCompetition,
                                        Dance = dancesInCompetition.Dance,
                                        DanceOrder = dancesInCompetition.DanceOrder
                                    };

                    this.DatabaseContext.DancesCompetition.Add(dance);
                }

                foreach (var officialInCompetition in this.Competition.Officials)
                {
                    var official = new OfficialInCompetition()
                                       {
                                           Competition = newCompetition,
                                           Official = officialInCompetition.Official,
                                           Role = officialInCompetition.Role
                                       };

                    this.DatabaseContext.OfficialCompetition.Add(official);
                }

                this.DatabaseContext.Competitions.Add(newCompetition);
                this.AddParticipantsToCompetition(newCompetition, splittedCompetition.FromPlace, splittedCompetition.ToPlace);
                MainWindow.MainWindowHandle.AddCompetitionToNavigationList(newCompetition);
            }

            // This competition is finished now.
            // we will continue with our new created competitions
            foreach (var participant in this.Competition.Participants)
            {
                participant.State = CoupleState.DroppedOut;
            }

            this.Competition.State = CompetitionState.Finished;

            this.DatabaseContext.SaveChanges();

            this.SplitPerformed = true;
            MainWindow.MainWindowHandle.CloseDialog();

            MainWindow.MainWindowHandle.ShowMessage(
                "This competition has been splitted in new compeitions and this competition will now be finished. Please continue with the newly created competitions",
                "Competition splitted");
        }

        private bool ValidateData()
        {
            var ordered = this.SplittedCompetitions.OrderBy(s => s.FromPlace).ToList();
            for (var i = 0; i < ordered.Count - 1; i++)
            {
                if (ordered[i].ToPlace + 1 > ordered[i + 1].FromPlace)
                {
                    MainWindow.MainWindowHandle.ShowMessage(
                        string.Format(LocalizationService.Resolve(() => Text.OverlappingCompetition),
                            ordered[i].CompetitionTitle, ordered[i + 1].CompetitionTitle));

                    return false;
                }

                if (ordered[i].ToPlace + 1 != ordered[i + 1].FromPlace)
                {
                    MainWindow.MainWindowHandle.ShowMessage(
                        string.Format(LocalizationService.Resolve(() => Text.GapBetweenSplittedCompetitions),
                            ordered[i].CompetitionTitle, ordered[i + 1].CompetitionTitle));

                    return false;
                }
            }

            return true;
        }

        private void AddParticipantsToCompetition(Competition newCompetition, int fromPlace, int toPlace)
        {
            // Select the relevant participants:
            var participants =
                this.DatabaseContext.Qualifieds.Where(
                    q => q.Round.Competition.Id == this.Competition.Id && q.PlaceFrom >= fromPlace && q.PlaceTo <= toPlace)
                    .Select(q => q.Participant);

            foreach (var participant in participants)
            {
                var newParticipant = new Participant()
                {
                    AgeGroup = participant.AgeGroup,
                    Class = participant.Class,
                    Competition = newCompetition,
                    Number = participant.Number,
                    Couple = participant.Couple,
                };

                this.DatabaseContext.Participants.Add(newParticipant);
                newCompetition.Participants.Add(newParticipant);
            }
        }

        private void DeleteSplit()
        {
            if (this.SelectedSplit != null)
            {
                this.SplittedCompetitions.Remove(this.SelectedSplit);
            }
        }

        private void CloseDialog()
        {
            this.SplitPerformed = false;
            MainWindow.MainWindowHandle.CloseDialog();
        }
    }
}
