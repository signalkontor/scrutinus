﻿// // TPS.net TPS8 Scrutinus
// // DefineResultServiceViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace Scrutinus.Dialogs.DialogViewModels
{
    internal class DefineResultServiceViewModel : ViewModelBase
    {
        #region Constructors and Destructors

        public DefineResultServiceViewModel()
        {
            this.Load();
            this.SaveCommand = new RelayCommand(this.Save);
            this.CancelCommand = new RelayCommand(this.Cancel);
        }

        #endregion

        #region Public Properties

        public ICommand CancelCommand { get; set; }

        public NumberOfCopiesCollectedReports CollectedReportDefinition { get; set; }

        public ICommand SaveCommand { get; set; }

        #endregion

        #region Methods

        private void Cancel()
        {
            MainWindow.MainWindowHandle.CloseDialog();
        }

        private void Load()
        {
            var bytes = Convert.FromBase64String(Settings.Default.CollectedReportDefinition);

            if (bytes.Length == 0)
            {
                this.CollectedReportDefinition = new NumberOfCopiesCollectedReports();
                return;
            }

            var stream = new MemoryStream(bytes);

            var bf = new BinaryFormatter();
            this.CollectedReportDefinition = (NumberOfCopiesCollectedReports)bf.Deserialize(stream);
        }

        private void Save()
        {
            var ms = new MemoryStream();
            var formatter = new BinaryFormatter();
            try
            {
                formatter.Serialize(ms, this.CollectedReportDefinition);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Excption serializing object: " + ex.Message);
                return;
            }

            var bytes = ms.GetBuffer();

            Settings.Default.CollectedReportDefinition = Convert.ToBase64String(bytes);
            Settings.Default.Save();

            MainWindow.MainWindowHandle.CloseDialog();
        }

        #endregion
    }
}