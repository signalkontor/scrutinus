﻿// // TPS.net TPS8 Scrutinus
// // DesignWinnerCertificateViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Printing;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using DataModel.Models;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using General;
using MahApps.Metro.Controls.Dialogs;
using Microsoft.Win32;
using Scrutinus.Localization;
using Scrutinus.Reports.BasicPrinting.CompetitionPrinting;

namespace Scrutinus.Dialogs.DialogViewModels
{
    public class DesignWinnerCertificateViewModel : ViewModelBase
    {
        #region Constructors and Destructors

        public DesignWinnerCertificateViewModel()
        {
            this.Load();

            // Bind commands
            this.SaveCommand = new RelayCommand(this.Save);
            this.CloseCommand = new RelayCommand(this.Cancel);
            this.AddWinnerCertificateDefinitionCommand = new RelayCommand(this.AddWinnerCertificateDefinition);
            this.RemoveWinnerCertificateDefinitionCommand = new RelayCommand(this.RemoveWinnerCertificateDefinition);
            this.AddElementCommand = new RelayCommand(this.AddWinnerCertificateElement);
            this.DeleteElementCommand = new RelayCommand(this.RemoveWinnerCertificateElement);
            this.CopyWinnerCertificateDefinitionCommand = new RelayCommand(this.CopyWinnerCertificate);
            this.DeleteCertificateCommand = new RelayCommand(this.DeleteCertificate);
            this.PrintCommand = new RelayCommand(this.Print);
            this.ExportCommand = new RelayCommand(this.Export);
            this.ImportCommand = new RelayCommand(this.Import);

            this.FillElementTypes();
            this.FillAlignments();

            if (this.WinnerCertificateDefinitions.Count > 0)
            {
                this.SelectedWinnerCertificateDefinition = this.WinnerCertificateDefinitions.First();
            }
        }

        #endregion

        #region Fields

        private WinnerCertificateDefinition selectedWinnerCertificateDefinition;

        private WinnerCertificateElement selectedWinnerCertificateElement;

        private string textDescription;

        private bool textIsVisible;

        #endregion

        #region Public Properties

        public ICommand AddElementCommand { get; set; }

        public ICommand AddWinnerCertificateDefinitionCommand { get; set; }

        public ICommand CloseCommand { get; set; }

        public ICommand CopyWinnerCertificateDefinitionCommand { get; set; }

        public ICommand DeleteCertificateCommand { get; set; }

        public ICommand DeleteElementCommand { get; set; }

        public ICommand ImportCommand { get; set; }

        public ICommand ExportCommand { get; set; }

        public ObservableCollection<KeyValuePair<WinnerCertificateElementTypes, string>> ElementTypes { get; set; }

        public ObservableCollection<KeyValuePair<HorizontalAlignment, string>> HorizontalAlignments { get; set; }

        public string NewCertificateName { get; set; }

        public ICommand PrintCommand { get; set; }

        public ICommand RemoveWinnerCertificateDefinitionCommand { get; set; }

        public ICommand SaveCommand { get; set; }

        public WinnerCertificateDefinition SelectedWinnerCertificateDefinition
        {
            get
            {
                return this.selectedWinnerCertificateDefinition;
            }
            set
            {
                this.selectedWinnerCertificateDefinition = value;
                this.RaisePropertyChanged(() => this.SelectedWinnerCertificateDefinition);
            }
        }

        public WinnerCertificateElement SelectedWinnerCertificateElement
        {
            get
            {
                return this.selectedWinnerCertificateElement;
            }
            set
            {
                this.selectedWinnerCertificateElement = value;
                if (value != null)
                {
                    this.selectedWinnerCertificateElement.PropertyChanged +=
                        this.SelectedWinnerCertificateElementOnPropertyChanged;
                }

                this.RaisePropertyChanged(() => this.SelectedWinnerCertificateElement);
            }
        }

        public string TextDescription
        {
            get
            {
                return this.textDescription;
            }
            set
            {
                this.textDescription = value;
                this.RaisePropertyChanged(() => this.TextDescription);
            }
        }

        public bool TextIsVisible
        {
            get
            {
                return this.textIsVisible;
            }
            set
            {
                this.textIsVisible = value;
                this.RaisePropertyChanged(() => this.TextIsVisible);
            }
        }

        public ObservableCollection<KeyValuePair<VerticalAlignment, string>> VerticalAlignments { get; set; }

        public ObservableCollection<WinnerCertificateDefinition> WinnerCertificateDefinitions { get; set; }

        #endregion

        #region Methods

        private void AddWinnerCertificateDefinition()
        {
            if (string.IsNullOrEmpty(this.NewCertificateName))
            {
                MainWindow.MainWindowHandle.ShowMessageAsync(LocalizationService.Resolve(() => Text.PleaseEnterNameOfCertificate));
                return;
            }

            var newDefinition = new WinnerCertificateDefinition
                                    {
                                        CertificateName = this.NewCertificateName,
                                        LeftMargin = 1,
                                        RightMargin = 1,
                                        Elements = new ObservableCollection<WinnerCertificateElement>()
                                    };

            this.WinnerCertificateDefinitions.Add(newDefinition);

            this.Save();

            this.SelectedWinnerCertificateDefinition = newDefinition;
        }

        private void AddWinnerCertificateElement()
        {
            if (this.SelectedWinnerCertificateDefinition == null)
            {
                return;
            }

            var element = new WinnerCertificateElement
                              {
                                  FontSize = 15,
                                  Left = 1,
                                  Top = 1,
                                  ElementType = WinnerCertificateElementTypes.Class,
                                  FontFamily = new FontFamily("Arial"),
                                  FontWeight = 500,
                                  Width = 10,
                                  Height = 1,
                              };
            this.SelectedWinnerCertificateDefinition.Elements.Add(element);
            this.SelectedWinnerCertificateElement = element;
        }

        private void Cancel()
        {
            this.CloseWindow();
        }

        private void CloseWindow()
        {
            MainWindow.MainWindowHandle.CloseDialog();
        }

        private void CopyWinnerCertificate()
        {
            if (string.IsNullOrEmpty(this.NewCertificateName))
            {
                MainWindow.MainWindowHandle.ShowMessage(LocalizationService.Resolve(() => Text.PleaseEnterNewName));
                return;
            }

            var ms = new MemoryStream();
            var formatter = new BinaryFormatter();
            try
            {
                formatter.Serialize(ms, this.SelectedWinnerCertificateDefinition);
                // Create a clone from this:
                var ms2 = new MemoryStream(ms.GetBuffer());
                var newCertificate = formatter.Deserialize(ms2) as WinnerCertificateDefinition;

                if (newCertificate == null)
                {
                    return;
                }

                newCertificate.CertificateName = this.NewCertificateName;

                this.WinnerCertificateDefinitions.Add(newCertificate);

                this.Save();

                this.SelectedWinnerCertificateDefinition = newCertificate;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Excption serializing object: " + ex.Message);
            }
        }

        private void DeleteCertificate()
        {
            if (this.SelectedWinnerCertificateDefinition == null)
            {
                return;
            }

            var res = MainWindow.MainWindowHandle.ShowMessage(
                            LocalizationService.Resolve(() => Text.DeleteWinnerCertificateMessage),
                            LocalizationService.Resolve(() => Text.DeleteWinnerCertificateCaption),
                            MessageDialogStyle.AffirmativeAndNegative);

            if (res == MessageDialogResult.Negative)
            {
                return;
            }



            this.WinnerCertificateDefinitions.Remove(this.SelectedWinnerCertificateDefinition);
            this.SelectedWinnerCertificateDefinition = null;
        }

        private void FillAlignments()
        {
            this.HorizontalAlignments = new ObservableCollection<KeyValuePair<HorizontalAlignment, string>>();

            var elements = Enum.GetNames(typeof(HorizontalAlignment));

            foreach (var element in elements)
            {
                Debug.WriteLine(element);
                var keyValue =
                    new KeyValuePair<HorizontalAlignment, string>(
                        (HorizontalAlignment)Enum.Parse(typeof(HorizontalAlignment), element),
                        LocalizationService.Resolve("Scrutinus:Text:HorizontalAlignment_" + element));

                this.HorizontalAlignments.Add(keyValue);
            }

            this.VerticalAlignments = new ObservableCollection<KeyValuePair<VerticalAlignment, string>>();
            elements = Enum.GetNames(typeof(VerticalAlignment));

            foreach (var element in elements)
            {
                Debug.WriteLine(element);
                var keyValue =
                    new KeyValuePair<VerticalAlignment, string>(
                        (VerticalAlignment)Enum.Parse(typeof(VerticalAlignment), element),
                        LocalizationService.Resolve("Scrutinus:Text:VerticalAlignment_" + element));

                this.VerticalAlignments.Add(keyValue);
            }
        }

        private void FillElementTypes()
        {
            this.ElementTypes = new ObservableCollection<KeyValuePair<WinnerCertificateElementTypes, string>>();

            var elements = Enum.GetNames(typeof(WinnerCertificateElementTypes));

            foreach (var element in elements)
            {
                Debug.WriteLine(element);
                var keyValue =
                    new KeyValuePair<WinnerCertificateElementTypes, string>(
                        (WinnerCertificateElementTypes)Enum.Parse(typeof(WinnerCertificateElementTypes), element),
                        LocalizationService.Resolve("Scrutinus:Text:WinnerCertificateElementTypes_" + element));

                this.ElementTypes.Add(keyValue);
            }
        }

        private void Load()
        {
            byte[] bytes;

            if (
                File.Exists(
                    Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\TPS.net\\Certificates.bin"))
            {
                bytes =
                    File.ReadAllBytes(
                        Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\TPS.net\\Certificates.bin");
            }
            else
            {
                bytes = Convert.FromBase64String(Settings.Default.WinnerCertificateDefinitions);
            }

            if (bytes.Length == 0)
            {
                this.WinnerCertificateDefinitions = new ObservableCollection<WinnerCertificateDefinition>();
                return;
            }

            var stream = new MemoryStream(bytes);

            var bf = new BinaryFormatter();
            this.WinnerCertificateDefinitions =
                (ObservableCollection<WinnerCertificateDefinition>)bf.Deserialize(stream);

            foreach (var winnerCertificateDefinition in this.WinnerCertificateDefinitions)
            {
                foreach (var element in winnerCertificateDefinition.Elements)
                {
                    element.FontFamily = new FontFamily(element.FontFamilyString);
                }
            }
        }

        private void Print()
        {
            var context = new ScrutinusContext();

            var round = context.Rounds.FirstOrDefault();

            if (round == null)
            {
                var message = LocalizationService.Resolve(() => Text.CertificatePrinterNoRoundMessage);
                var caption = LocalizationService.Resolve(() => Text.CertificatePrinterNoRoundCaption);

                MainWindow.MainWindowHandle.ShowMessage(message, caption, MessageDialogStyle.Affirmative);

                return;
            }

            var participant = round.Qualifieds.First().Participant;

            participant.PlaceFrom = 1;
            participant.PlaceTo = 1;
            participant.PlaceFromOwnCompetition = 1;
            participant.PlaceToOwnCompetition = 1;

            var certificatePrinter = new WinnerCertificatePrinter(
                round,
                this.SelectedWinnerCertificateDefinition,
                new List<Participant> { participant });

            certificatePrinter.PrintDocument(true, null, PageOrientation.Portrait, 1, "Winner Certificate");
        }

        private void RemoveWinnerCertificateDefinition()
        {
            if (this.SelectedWinnerCertificateDefinition != null)
            {
                this.WinnerCertificateDefinitions.Remove(this.SelectedWinnerCertificateDefinition);
            }
        }

        private void RemoveWinnerCertificateElement()
        {
            if (this.SelectedWinnerCertificateDefinition == null || this.SelectedWinnerCertificateElement == null)
            {
                return;
            }

            this.SelectedWinnerCertificateDefinition.Elements.Remove(this.SelectedWinnerCertificateElement);
        }

        private void Save()
        {
            var ms = new MemoryStream();
            var formatter = new BinaryFormatter();
            try
            {
                formatter.Serialize(ms, this.WinnerCertificateDefinitions);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Excption serializing object: " + ex.Message);
                return;
            }

            var bytes = ms.GetBuffer();

            if (!Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)))
            {
                Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments));
            }

            File.WriteAllBytes(
                Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\TPS.net\\Certificates.bin",
                bytes);
        }

        private void SelectedWinnerCertificateElementOnPropertyChanged(
            object sender,
            PropertyChangedEventArgs propertyChangedEventArgs)
        {
            if (this.selectedWinnerCertificateElement == null)
            {
                return;
            }

            if (this.selectedWinnerCertificateElement.ElementType == WinnerCertificateElementTypes.StaticText)
            {
                this.TextDescription = LocalizationService.Resolve(() => Text.StaticText);
                this.TextIsVisible = true;
                return;
            }

            if (this.selectedWinnerCertificateElement.ElementType == WinnerCertificateElementTypes.Picture)
            {
                this.TextDescription = LocalizationService.Resolve(() => Text.PathToImageFile);
                this.TextIsVisible = true;
                return;
            }

            this.TextIsVisible = false;
        }

        private void Import()
        {
            var dlg = new OpenFileDialog { DefaultExt = ".json" };

            if (dlg.ShowDialog() != true)
            {
                return;
            }

            var json = File.ReadAllText(dlg.FileName);

            var data = JsonHelper.DeserializeData<ObservableCollection<WinnerCertificateDefinition>>(json);

            foreach (var def in data)
            {
                foreach (var element in def.Elements)
                {
                    element.FontFamily = new FontFamily(element.FontFamilyString);
                }

                this.WinnerCertificateDefinitions.Add(def);
            }

            if (this.WinnerCertificateDefinitions.Any())
            {
                this.SelectedWinnerCertificateDefinition = this.WinnerCertificateDefinitions.First();
            }

            this.Save();
        }

        private void Export()
        {
            var dlg = new SaveFileDialog { DefaultExt = ".json" };

            if (dlg.ShowDialog() != true)
            {
                return;
            }

            var json = JsonHelper.SerializeObject(this.WinnerCertificateDefinitions);

            File.WriteAllText(dlg.FileName, json);
        }

        #endregion
    }
}