﻿// // TPS.net TPS8 Scrutinus
// // ChangeStartbookDataViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using DataModel.Models;
using DtvEsvModule;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using General.Rules.PointCalculation;

namespace Scrutinus.Dialogs.DialogViewModels
{
    internal class ChangeStartbookDataViewModel : ViewModelBase
    {
        #region Fields

        private readonly ScrutinusContext context;

        private Participant selectedParticipant;

        private Class selectedParticipantsClass;

        private int selectedParticipantsPlacings;

        private int seletcedParticipantsPoints;

        #endregion

        #region Constructors and Destructors

        public ChangeStartbookDataViewModel()
        {
        }

        public ChangeStartbookDataViewModel(Round round, ScrutinusContext context)
        {
            this.context = context;
            this.Participants =
                new ObservableCollection<Participant>(
                    round.Qualifieds.OrderBy(p => p.Participant.Number).Select(q => q.Participant));
            var map =
                round.Competition.Section.AgeGroupClassesMappings.Single(
                    m => m.AgeGroup.Id == round.Competition.AgeGroup.Id);
            this.Classes = new ObservableCollection<Class>(map.Classes);

            this.SaveCommand = new RelayCommand(this.Save);
            this.CloseCommand = new RelayCommand(this.Close);
        }

        #endregion

        #region Public Properties

        public ObservableCollection<Class> Classes { get; set; }

        public ICommand CloseCommand { get; set; }

        public ObservableCollection<Participant> Participants { get; set; }

        public ICommand SaveCommand { get; set; }

        public Participant SelectedParticipant
        {
            get
            {
                return this.selectedParticipant;
            }
            set
            {
                this.selectedParticipant = value;
                this.selectedParticipantsClass = this.selectedParticipant.Class;
                this.selectedParticipantsPlacings = this.selectedParticipant.OriginalPlacings ?? 0;
                this.seletcedParticipantsPoints = this.selectedParticipant.OriginalPoints ?? 0;

                this.RaisePropertyChanged(() => this.SelectedParticipant);
                this.RaisePropertyChanged(() => this.SelectedParticipantsClass);
                this.RaisePropertyChanged(() => this.SelectedParticipantsPlacings);
                this.RaisePropertyChanged(() => this.SeletcedParticipantsPoints);
            }
        }

        public Class SelectedParticipantsClass
        {
            get
            {
                return this.selectedParticipantsClass;
            }
            set
            {
                // What is our target class?
                var climbUpEntry =
                    this.context.ClimbUpTables.FirstOrDefault(
                        c =>
                        c.AgeGroup.Id == this.SelectedParticipant.AgeGroup.Id && c.Class.Id == value.Id
                        && c.LTV == this.selectedParticipant.Couple.Region
                        && c.Section.Id == this.SelectedParticipant.Competition.Section.Id);

                if (climbUpEntry == null)
                {
                    this.selectedParticipant.Class = this.selectedParticipantsClass;
                    climbUpEntry = DtvApiImporter.GetAufstiegstabelleifNotFound(this.context, this.selectedParticipant);
                }

                if (climbUpEntry != null)
                {
                    this.selectedParticipant.TargetClass = climbUpEntry.TargetClass;
                }
                else
                {
                    this.selectedParticipant.TargetClass = null;
                }

                this.selectedParticipantsClass = value;
                this.selectedParticipant.Class = value;

                this.RaisePropertyChanged(() => this.SelectedParticipantsClass);
#if DTV
                DTVPointCalculator.UpdateAllParticipants(this.context, this.selectedParticipant.Id, this.seletcedParticipantsPoints, this.selectedParticipantsPlacings, this.selectedParticipantsClass, false);
#endif
                this.context.SaveChanges();
            }
        }

        public int SelectedParticipantsPlacings
        {
            get
            {
                return this.selectedParticipantsPlacings;
            }
            set
            {
                this.selectedParticipantsPlacings = value;
                this.selectedParticipant.OriginalPlacings = value;
                this.RaisePropertyChanged(() => this.SelectedParticipantsPlacings);
#if DTV
                DTVPointCalculator.UpdateAllParticipants(this.context, this.selectedParticipant.Id, this.seletcedParticipantsPoints, this.selectedParticipantsPlacings, this.selectedParticipantsClass, true);
#endif
            }
        }

        public int SeletcedParticipantsPoints
        {
            get
            {
                return this.seletcedParticipantsPoints;
            }
            set
            {
                this.seletcedParticipantsPoints = value;
                this.selectedParticipant.OriginalPoints = value;
                this.RaisePropertyChanged(() => this.SeletcedParticipantsPoints);
#if DTV
                DTVPointCalculator.UpdateAllParticipants(this.context, this.selectedParticipant.Id, this.seletcedParticipantsPoints, this.selectedParticipantsPlacings, this.selectedParticipantsClass, true);
#endif
            }
        }

        #endregion

        #region Methods

        private void Close()
        {
            MainWindow.MainWindowHandle.CloseDialog();
        }

        private void Save()
        {
            this.context.SaveChanges();
            MainWindow.MainWindowHandle.CloseDialog();
        }

        #endregion
    }
}