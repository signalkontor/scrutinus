﻿// // TPS.net TPS8 Scrutinus
// // DisqualifyCoupleViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Linq;
using System.Windows.Forms;
using System.Windows.Input;
using DataModel;
using DataModel.Models;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using General.Rules.PointCalculation;
using Scrutinus.Pages;

namespace Scrutinus.Dialogs.DialogViewModels
{
    public class DisqualifyCoupleViewModel : ViewModelBase
    {
        #region Public Methods and Operators

        public void Disqualify(bool recalculatePlace, DtvDisqualificationFlag reason)
        {
            if (recalculatePlace)
            {
                this.DisqualifyRecalculatePlaces();
            }
            else
            {
                this.DisqualifyLastPlaceInRound();
            }

            this.participant.DisqualificationFlag = reason;
            if (reason == DtvDisqualificationFlag.WarningSylabus && this.participant.Startbuch != null)
            {
                this.participant.Startbuch.Sylabus = true;
            }

            // Recalculate the places:
            var qualified = this.context.Qualifieds.Where(q => q.Round.Id == this.Round.Id).ToList();

            foreach (var qualified1 in qualified)
            {
                // Reset places for recalculation
                qualified1.PlaceFrom = 0;
            }

            EnterMarking.CalculatePlaces(qualified, this.Round.PlaceOffset);

            // Just in case: Recalculate Points
            var calculator = PointCalculatorFactory.GetCalculator(this.Round.Competition);
            calculator.CalculateRankingPoints(this.context.Participants, this.context);

            this.context.SaveChanges();

            this.Result = DialogResult.OK;

            MainWindow.MainWindowHandle.CloseDialog();
        }

        #endregion

        #region Fields

        private readonly ScrutinusContext context;

        private readonly Participant participant;

        private int originalPlace;

        #endregion

        #region Constructors and Destructors

        public DisqualifyCoupleViewModel()
        {
        }

        public DisqualifyCoupleViewModel(ScrutinusContext context, int roundId, int participantId)
        {
            this.context = context;
            this.Round = context.Rounds.FirstOrDefault(r => r.Id == roundId);
            this.participant = context.Participants.SingleOrDefault(p => p.Id == participantId);

            if (this.participant != null)
            {
                this.Couple = this.participant.Couple;
            }

            this.RuleRecalculatePlaces = true;

            this.SaveCommand = new RelayCommand(this.Save);
            this.CancelCommand = new RelayCommand(this.Cancel);
        }

        #endregion

        #region Public Properties

        public ICommand CancelCommand { get; set; }

        public Couple Couple { get; set; }

        public DialogResult Result { get; set; }

        public Round Round { get; set; }

        public bool RuleLeavePlaceEmpty { get; set; }

        public bool RuleRecalculatePlaces { get; set; }

        public ICommand SaveCommand { get; set; }

        #endregion

        #region Methods

        private void Cancel()
        {
            this.Result = DialogResult.Cancel;
            MainWindow.MainWindowHandle.CloseDialog();
        }

        private void DisqualifyLastPlaceInRound()
        {
            if (!this.participant.PlaceFrom.HasValue)
            {
                return;
            }

            this.originalPlace = this.participant.PlaceFrom.Value;
            var lastPlace =
                this.Round.Qualifieds.Max(
                    q => q.Participant.PlaceTo.HasValue ? q.Participant.PlaceTo.Value : q.Participant.PlaceFrom);
            this.participant.PlaceFrom = lastPlace;
            this.participant.PlaceTo = lastPlace;

            this.context.SaveChanges();

            var participants =
                this.context.Participants.Where(
                    p =>
                    p.Competition.Id == this.Round.Competition.Id && p.PlaceFrom >= this.originalPlace
                    && p.Id != this.participant.Id);

            foreach (var toRecalculate in participants)
            {
                if (toRecalculate.PlaceFrom > this.originalPlace)
                {
                    toRecalculate.PlaceFrom--;
                    if (toRecalculate.PlaceTo.HasValue)
                    {
                        toRecalculate.PlaceTo--;
                    }
                }
                else
                {
                    toRecalculate.PlaceFrom--;
                    // We do not change PlaceTo here 
                }
            }

            this.context.SaveChanges();
        }

        private void DisqualifyRecalculatePlaces()
        {
            this.originalPlace = this.participant.PlaceFrom.Value;

            this.RemoveParticipantAsDisqualifed();

            var participants =
                this.context.Participants.Where(
                    p => p.Competition.Id == this.Round.Competition.Id && p.PlaceFrom >= this.originalPlace);

            foreach (var toRecalculate in participants)
            {
                if (toRecalculate.PlaceFrom > this.originalPlace)
                {
                    toRecalculate.PlaceFrom--;
                    if (toRecalculate.PlaceTo.HasValue)
                    {
                        toRecalculate.PlaceTo--;
                    }
                }
                else
                {
                    toRecalculate.PlaceFrom--;
                    // We do not change PlaceTo here 
                }
            }

            this.context.SaveChanges();
        }

        private void RemoveParticipantAsDisqualifed()
        {
            this.originalPlace = 0;

            var qualified =
                this.context.Qualifieds.SingleOrDefault(
                    q => q.Participant.Id == this.participant.Id && q.Round.Id == this.Round.Id);

            if (qualified != null)
            {
                this.originalPlace = qualified.PlaceFrom.HasValue ? qualified.PlaceFrom.Value : 0;
            }

            this.participant.State = CoupleState.Disqualified;
            this.participant.QualifiedRound = null;
            this.participant.QualifiedRound = null;

            var qualifications =
                this.context.Qualifieds.Where(q => q.Participant.Id == this.participant.Id).ToList();

            this.context.Qualifieds.RemoveRange(qualifications);

            this.context.SaveChanges();
        }

        private void Save()
        {
            if (this.RuleRecalculatePlaces)
            {
                this.Disqualify(true, DtvDisqualificationFlag.Disqualification);
            }
            else
            {
                this.Disqualify(false, DtvDisqualificationFlag.Disqualification);
            }
        }

        #endregion
    }
}