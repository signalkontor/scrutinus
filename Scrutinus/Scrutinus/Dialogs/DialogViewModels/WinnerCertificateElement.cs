// // TPS.net TPS8 Scrutinus
// // WinnerCertificateElement.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System;
using System.Windows;
using System.Windows.Media;
using General;
using Newtonsoft.Json;

namespace Scrutinus.Dialogs.DialogViewModels
{
    [Serializable]
    public class WinnerCertificateElement : NotificationObject
    {
        #region Constructors and Destructors

        public WinnerCertificateElement()
        {
            this.color.PropertyChanged += (sender, args) => { this.OnPropertyChanged("Color"); };
        }

        #endregion

        #region Fields

        private SerializableColor color = new SerializableColor(Colors.Black);

        private WinnerCertificateElementTypes elementType;

        [NonSerialized]
        private FontFamily fontFamily;

        private string fontFamilyString;

        private double fontSize;

        private int fontWeight;

        private double height;

        private HorizontalAlignment horizontalAlignment;

        private double left;

        private string text;

        private double top;

        private double rotation;

        [NonSerialized]
        private UIElement uiElementOnCanvas;

        private VerticalAlignment verticalAlignment;

        private double width;

        #endregion

        #region Public Properties

        [JsonIgnore]
        public Color Color
        {
            get
            {
                if (this.color == null)
                {
                    this.color = new SerializableColor(Colors.Black);
                }

                return this.color.Color;
            }

            set
            {
                this.color = new SerializableColor(value);
                this.OnPropertyChanged("Color");
            }
        }

        public SerializableColor ColorForSerialization
        {
            get { return this.color; }
            set
            {
                this.color = value;
                this.OnPropertyChanged(nameof(this.ColorForSerialization));
                this.OnPropertyChanged(nameof(this.Color));
            }
        }

        public WinnerCertificateElementTypes ElementType
        {
            get
            {
                return this.elementType;
            }
            set
            {
                this.elementType = value;
                this.OnPropertyChanged("ElementType");
            }
        }

        public FontFamily FontFamily
        {
            get
            {
                return this.fontFamily;
            }
            set
            {
                this.fontFamily = value;
                this.OnPropertyChanged("FontFamily");
                this.fontFamilyString = this.fontFamily.Source;
            }
        }

        public string FontFamilyString
        {
            get
            {
                return this.fontFamilyString;
            }
            set
            {
                this.fontFamilyString = value;
                this.FontFamily = new FontFamily(value);
            }
        }

        public double FontSize
        {
            get
            {
                return this.fontSize;
            }
            set
            {
                this.fontSize = value;
                this.OnPropertyChanged("FontSize");
            }
        }

        public int FontWeight
        {
            get
            {
                return this.fontWeight;
            }
            set
            {
                if (value < 0)
                {
                    value = 100;
                }

                this.fontWeight = value < 999 ? value : 999;
                this.OnPropertyChanged("FontWeight");
            }
        }

        public double Height
        {
            get
            {
                return this.height;
            }
            set
            {
                this.height = value;
                this.OnPropertyChanged("Height");
            }
        }

        public double HeightInPixel
        {
            get
            {
                return this.height * 96 / 2.54;
            }
            set
            {
                this.Height = value / 96 * 2.54;
                this.OnPropertyChanged("HeightInPixel");
            }
        }

        public HorizontalAlignment HorizontalAlignment
        {
            get
            {
                return this.horizontalAlignment;
            }
            set
            {
                this.horizontalAlignment = value;
                this.OnPropertyChanged("HorizontalAlignment");
            }
        }

        public double Left
        {
            get
            {
                return this.left;
            }
            set
            {
                this.left = value;
                this.OnPropertyChanged("Left");
                this.OnPropertyChanged("LeftInPixel");
            }
        }

        public double LeftInPixel
        {
            get
            {
                return this.left * 96 / 2.54;
            }
            set
            {
                this.left = value / 96 * 2.54;
                this.OnPropertyChanged("LeftInPixel");
                this.OnPropertyChanged("Left");
            }
        }

        public string Text
        {
            get
            {
                return this.text;
            }
            set
            {
                this.text = value;
                this.OnPropertyChanged(nameof(this.Text));
            }
        }

        public double Top
        {
            get
            {
                return this.top;
            }
            set
            {
                this.top = value;
                this.OnPropertyChanged("Top");
                this.OnPropertyChanged("TopInPixel");
            }
        }

        public double TopInPixel
        {
            get
            {
                return this.top * 96 / 2.54;
            }
            set
            {
                this.top = value / 96 * 2.54;
                this.OnPropertyChanged("Top");
                this.OnPropertyChanged("TopInPixel");
            }
        }

        [JsonIgnore]
        public UIElement UiElementOnCanvas
        {
            get
            {
                return this.uiElementOnCanvas;
            }
            set
            {
                this.uiElementOnCanvas = value;
            }
        }

        public VerticalAlignment VerticalAlignment
        {
            get
            {
                return this.verticalAlignment;
            }
            set
            {
                this.verticalAlignment = value;
                this.OnPropertyChanged("VerticalAlignment");
            }
        }

        public double Width
        {
            get
            {
                return this.width;
            }
            set
            {
                this.width = value;
                this.OnPropertyChanged("Width");
            }
        }

        public double WidthInPixel
        {
            get
            {
                return this.width * 96 / 2.54;
            }
            set
            {
                this.Width = value / 96 * 2.54;
                this.OnPropertyChanged("WidthInPixel");
            }
        }

        public double Rotation
        {
            get
            {
                return this.rotation;
            }
            set
            {
                this.rotation = value;
                this.OnPropertyChanged("Rotation");
            }
        }

        #endregion
    }
}