﻿// // TPS.net TPS8 Scrutinus
// // CombineCompetitionsViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Input;
using DataModel;
using DataModel.Models;
using DataModel.Rules;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MahApps.Metro.Controls.Dialogs;
using Scrutinus.Localization;

namespace Scrutinus.Dialogs.DialogViewModels
{
    public class CombineCompetitionsViewModel : ViewModelBase, IDataErrorInfo
    {
        #region Public Indexers

        public string this[string columnName]
        {
            get
            {
                return this.Error;
            }
        }

        #endregion

        #region Fields

        private readonly ScrutinusContext context;

        private string error;

        #endregion

        #region Constructors and Destructors

        public CombineCompetitionsViewModel()
            : this(new ScrutinusContext())
        {
        }

        public CombineCompetitionsViewModel(ScrutinusContext context)
        {
            this.context = context;

            this.Competitions = new ObservableCollection<Competition>(context.Competitions
                                                    .OrderBy(c => c.StartTime)
                                                    .ToList()
                                                    .Where(c => c.IsCompetitionNotStarted == true));

            this.CancelCommand = new RelayCommand(this.Cancel);
            this.SaveCommand = new RelayCommand(this.Save);
        }

        #endregion

        #region Public Properties

        public ICommand CancelCommand { get; set; }

        public ObservableCollection<Competition> Competitions { get; set; }

        public string Error
        {
            get
            {
                return this.error;
            }
            set
            {
                this.error = value;
                this.RaisePropertyChanged(() => this.Error);
            }
        }

        public Competition FirstCompetition { get; set; }

        public DialogResult Result { get; set; }

        public ICommand SaveCommand { get; set; }

        public Competition SecondCompetition { get; set; }

        #endregion

        #region Methods

        private void Cancel()
        {
            this.Result = DialogResult.Cancel;
            MainWindow.MainWindowHandle.CloseDialog();
        }

        private void Save()
        {
            var result = MainWindow.MainWindowHandle.ShowMessage(
                "Achtung: Eine Kombination kann nicht mehr Rückgängig gemacht werden! Die beiden Ausgangsturniere werden gelöscht! Kombinieren Sie erst nach dem finalen Download vom DTV Portal! Wenn Sie unsicher sind, machen Sie jetzt ein Backup",
                "Kombination", MessageDialogStyle.AffirmativeAndNegative);

            if (result == MessageDialogResult.Canceled || result == MessageDialogResult.Negative)
            {
                return;
            }

            // do some checks:
            this.Error = "";
            if (this.FirstCompetition == null || this.SecondCompetition == null)
            {
                this.Error = LocalizationService.Resolve(() => Text.CombineCompetitionSelectTwo);
            }

            if (this.FirstCompetition.Section != this.SecondCompetition.Section)
            {
                this.Error = LocalizationService.Resolve(() => Text.CombineCompetitionDifferentSection);
            }

            if (this.FirstCompetition.Dances.Count == 0)
            {
                var danceHelper = new DanceRules(this.context);
                var dances = danceHelper.GetDances(
                    this.FirstCompetition.Section.Id,
                    this.FirstCompetition.AgeGroup.Id,
                    this.FirstCompetition.Class.Id);
                foreach (var dance in dances)
                {
                    this.FirstCompetition.Dances.Add(
                        new DancesInCompetition
                            {
                                Dance = dance,
                                DanceOrder = dance.DefaultOrder,
                                Competition = this.FirstCompetition,
                            });
                }
            }

            if (this.SecondCompetition.Dances.Count == 0)
            {
                var danceHelper = new DanceRules(this.context);
                var dances = danceHelper.GetDances(
                    this.SecondCompetition.Section.Id,
                    this.SecondCompetition.AgeGroup.Id,
                    this.SecondCompetition.Class.Id);
                foreach (var dance in dances)
                {
                    this.SecondCompetition.Dances.Add(
                        new DancesInCompetition
                            {
                                Dance = dance,
                                DanceOrder = dance.DefaultOrder,
                                Competition = this.SecondCompetition,
                            });
                }
            }

            if (this.FirstCompetition.Dances.Count != this.SecondCompetition.Dances.Count)
            {
                this.Error = LocalizationService.Resolve(() => Text.CombineCompetetionDifferentDances);
            }

            if (!string.IsNullOrEmpty(this.Error))
            {
                MainWindow.MainWindowHandle.ShowMessageAsync(this.Error);
                return;
            }

            // Now we are ready to combine the competition
            var competition = new Competition
                                  {
                                      Event = this.FirstCompetition.Event,
                                      Section = this.FirstCompetition.Section,
                                      AgeGroup = this.FirstCompetition.AgeGroup,
                                      Class = this.FirstCompetition.Class,
                                      CombinedAgeGroup = this.SecondCompetition.AgeGroup,
                                      CombinedClass = this.SecondCompetition.Class,
                                      CompetitionType = this.FirstCompetition.CompetitionType,
                                      ExternalId = $"{this.FirstCompetition.ExternalId}/{this.SecondCompetition.ExternalId}",
                                      CombinedExternalId = this.SecondCompetition.ExternalId,
                                      HasRankingPoints = this.SecondCompetition.HasRankingPoints && this.FirstCompetition.HasRankingPoints,
                                      StartTime = this.FirstCompetition.StartTime,
                                      Title = $"{this.FirstCompetition.CompetitionType.TypeString} {this.FirstCompetition.AgeGroup.ShortName} {this.FirstCompetition.Class.ClassShortName} / {this.SecondCompetition.AgeGroup.ShortName} {this.SecondCompetition.Class.ClassShortName}",
                                      DefaultCouplesPerHeat = Settings.Default.DefaultCouplesPerHeat,
                                      LateEntriesAllowed = FirstCompetition.LateEntriesAllowed && SecondCompetition.LateEntriesAllowed
                                  };

            this.context.Competitions.Add(competition);

            foreach (var participant in this.FirstCompetition.Participants)
            {
                var newParticipant = new Participant
                        {
                            ExternalId = participant.ExternalId,
                            AgeGroup = this.FirstCompetition.AgeGroup,
                            Class = this.FirstCompetition.Class,
                            Competition = competition,
                            Couple = participant.Couple,
                            Number = participant.Number,
                            OriginalPoints = participant.OriginalPoints,
                            OriginalPlacings = participant.OriginalPlacings,
                            TargetClass = participant.TargetClass,
                            TargetPlacings = participant.TargetPlacings,
                            TargetPoints = participant.TargetPoints,
                            MinimumPoints = participant.MinimumPoints,
                            PlacingsUpto = participant.PlacingsUpto,
                            State = participant.State
                        };
                this.context.Participants.Add(newParticipant);
                competition.Participants.Add(newParticipant);
            }
            // Now the same with the second competition
            foreach (var participant in this.SecondCompetition.Participants)
            {
                var newParticipant = new Participant
                        {
                            ExternalId = participant.ExternalId,
                            AgeGroup = this.SecondCompetition.AgeGroup,
                            Class = this.SecondCompetition.Class,
                            Competition = competition,
                            Couple = participant.Couple,
                            Number = participant.Number,
                            OriginalPoints = participant.OriginalPoints,
                            OriginalPlacings = participant.OriginalPlacings,
                            TargetClass = participant.TargetClass,
                            TargetPlacings = participant.TargetPlacings,
                            TargetPoints = participant.TargetPoints,
                            MinimumPoints = participant.MinimumPoints,
                            PlacingsUpto = participant.PlacingsUpto,
                            State = participant.State
                        };
                this.context.Participants.Add(newParticipant);
                competition.Participants.Add(newParticipant);
            }

            this.context.Participants.RemoveRange(FirstCompetition.Participants);
            this.context.Participants.RemoveRange(SecondCompetition.Participants);
            this.context.OfficialCompetition.RemoveRange(FirstCompetition.Officials);
            this.context.OfficialCompetition.RemoveRange(SecondCompetition.Officials);
            this.context.DancesCompetition.RemoveRange(FirstCompetition.Dances);
            this.context.DancesCompetition.RemoveRange(SecondCompetition.Dances);

            this.context.Competitions.Remove(FirstCompetition);
            this.context.Competitions.Remove(SecondCompetition);

            this.context.SaveChanges();

            MainWindow.MainWindowHandle.ShowMessage(Text.CompetitionCombined, Text.CompetitionCombinedTitle);

            this.Result = DialogResult.OK;
            MainWindow.MainWindowHandle.CloseDialog();
        }

        #endregion
    }
}