﻿// // TPS.net TPS8 Scrutinus
// // OpenEventViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlServerCe;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Input;
using System.Xml;
using DataModel.Models;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MahApps.Metro.Controls.Dialogs;
using Scrutinus.Localization;
using Scrutinus.Pages;
using Scrutinus.ViewModels;

namespace Scrutinus.Dialogs.DialogViewModels
{
    internal class EventListElement
    {
        #region Public Properties

        /// <summary>
        ///     Complete file (directory + path)
        /// </summary>
        public string DatabaseFile { get; set; }

        public Event Event { get; set; }

        /// <summary>
        ///     Only file name without directory
        /// </summary>
        public string FileName { get; set; }

        public DateTime LastAccess { get; set; }

        #endregion
    }

    internal class OpenEventViewModel : ViewModelBase
    {
        #region Constructors and Destructors

        public OpenEventViewModel()
        {
            this.Events = new ObservableCollection<EventListElement>();

            var path = Settings.Default.DataFolder;

            if (path.StartsWith("%Documents%"))
            {
                path = path.Replace("%Documents%", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments));
            }

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            this.SelectedPath = path;

            this.CreateEventCommand = new RelayCommand(this.CreateEvent);
            this.CloseDialogCommand = new RelayCommand(this.Close);
            this.OpenCommand = new RelayCommand(this.Open);
            this.DeleteEventCommand = new RelayCommand(this.DeleteEvent);
        }

        #endregion

        #region Fields

        private SelectInitialRuleDialog selectInitialRuleDialog;

        private string selectedPath;

        #endregion

        #region Public Properties

        public ICommand CloseDialogCommand { get; set; }

        public ICommand CreateEventCommand { get; set; }

        public ICommand DeleteEventCommand { get; set; }

        public ObservableCollection<EventListElement> Events { get; set; }

        public ICommand OpenCommand { get; set; }

        public EventListElement SelectedEvent { get; set; }

        public string SelectedPath
        {
            get
            {
                return this.selectedPath;
            }
            set
            {
                this.selectedPath = value;
                this.LoadEventList();
                this.RaisePropertyChanged(() => this.SelectedPath);
                Settings.Default.DataFolder = value;
                Settings.Default.Save();
            }
        }

        #endregion

        #region Methods

        private void AddEventFromDatabase(string file)
        {
            try
            {

                SqlCeConnection conn = null;
                conn = new SqlCeConnection("Data Source=" + file);
                conn.Open();
                List<KeyValuePair<string, string>> dbinfo = conn.GetDatabaseInfo();

                Console.WriteLine($"\nGetDatabaseInfo() results {file}:");

                foreach (KeyValuePair<string, string> kvp in dbinfo)
                {
                    Console.WriteLine(kvp);
                }

                var connection = new SqlCeConnection("Data Source=" + file);
                connection.Open();
                var command = connection.CreateCommand();
                command.CommandText = "Select Title, DateFrom, DateTo From Events";
                var reader = command.ExecuteReader();

                if (reader.Read())
                {
                    var fileInfo = new FileInfo(file);

                    this.Events.Add(
                        new EventListElement
                            {
                                DatabaseFile = file,
                                FileName = fileInfo.Name,
                                Event =
                                    new Event
                                        {
                                            Title = reader.GetString(0),
                                            DateFrom =
                                                reader.IsDBNull(1)
                                                    ? (DateTime?)null
                                                    : reader.GetDateTime(1),
                                            DateTo =
                                                reader.IsDBNull(2)
                                                    ? (DateTime?)null
                                                    : reader.GetDateTime(2)
                                        },
                                LastAccess = File.GetLastAccessTime(file)
                            });
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void Close()
        {
            MainWindow.MainWindowHandle.CloseDialog();
        }

        private void CreateEvent()
        {
            this.selectInitialRuleDialog = new SelectInitialRuleDialog(this.GetRulesList());

            MainWindow.MainWindowHandle.ShowDialog(
                this.selectInitialRuleDialog,
                this.OnSelectRuleDialogClosed,
                300,
                300);
        }

        private void DeleteEvent()
        {
            if (this.SelectedEvent == null)
            {
                return;
            }

            if (Settings.Default.DataSource == this.SelectedEvent.DatabaseFile)
            {
                MainWindow.MainWindowHandle.ShowMessage(LocalizationService.Resolve(() => Text.CannotDeleteOpenEvent));
                return;
            }

            var res =
                MainWindow.MainWindowHandle.ShowMessage(
                    LocalizationService.Resolve(() => Text.DeleteEventCaption),
                    LocalizationService.Resolve(() => Text.DeleteEventMessage),
                    MessageDialogStyle.AffirmativeAndNegative);

            if (res == MessageDialogResult.Affirmative)
            {
                File.Delete(this.SelectedEvent.DatabaseFile);
                this.Events.Remove(this.SelectedEvent);
            }
        }

        private string GetFileName(string eventTitle)
        {
            if (eventTitle == null)
            {

                return "";
            }

            if (!this.SelectedPath.EndsWith("\\"))
            {
                this.SelectedPath = this.SelectedPath + "\\";
            }

            foreach (var invalidChar in Path.GetInvalidFileNameChars())
            {
                eventTitle = eventTitle.Replace(invalidChar, '_');
            }

            return this.SelectedPath + eventTitle + ".sdf";
        }

        private List<KeyValuePair<string, string>> GetRulesList()
        {
            var assembly = Assembly.Load("DataModel");
            var resources = assembly.GetManifestResourceNames();
            var list = new List<KeyValuePair<string, string>>();

            var initialDataNames = resources.Where(r => r.EndsWith(".xml"));

            if (initialDataNames.Count() > 1)
            {
                foreach (var initialDataName in initialDataNames)
                {
                    var doc = new XmlDocument();

                    using (var stream = typeof(XmlLoader).Assembly.GetManifestResourceStream(initialDataName))
                    {
                        doc.Load(stream);
                        var name = doc.DocumentElement.Attributes["RuleName"].Value;
                        list.Add(new KeyValuePair<string, string>(initialDataName, name));
                    }
                }
            }

            var fileInfo = new FileInfo(assembly.Location);

            var path = fileInfo.Directory + "\\XMLRules";

            try
            {
                if (Directory.Exists(path))
                {
                    var files = Directory.GetFiles(path, "*.xml");

                    foreach (var file in files)
                    {
                        var doc = new XmlDocument();

                        using (Stream stream = new FileStream(file, FileMode.Open))
                        {
                            try
                            {
                                doc.Load(stream);
                                var name = doc.DocumentElement.Attributes["RuleName"].Value;
                                list.Add(new KeyValuePair<string, string>(file, name));
                            }
                            catch (Exception exception)
                            {
                                ScrutinusContext.WriteLogEntry(exception);
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                
            }
            

            return list;
        }

        /// <summary>
        ///     Loads the event list.
        /// </summary>
        private void LoadEventList()
        {
            this.Events.Clear();
            var files = Directory.GetFiles(this.SelectedPath, "*.sdf");

            foreach (var file in files)
            {
                this.AddEventFromDatabase(file);
            }
        }

        private void OnSelectRuleDialogClosed(object result)
        {
            if (this.selectInitialRuleDialog.SelectedRule == null
                || string.IsNullOrWhiteSpace(this.selectInitialRuleDialog.Title)
                || !this.selectInitialRuleDialog.DateFrom.HasValue)
            {
                return;
            }

            if (this.selectInitialRuleDialog.SelectedRule.Value.Value == "DTV National")
            {
                // Check if we have a competiton one day before or after
                var dayBefore = this.selectInitialRuleDialog.DateFrom.Value.Date.AddDays(-1);
                var dayAfter = this.selectInitialRuleDialog.DateTo.HasValue
                                        ? this.selectInitialRuleDialog.DateTo.Value.Date.AddDays(1)
                                        : this.selectInitialRuleDialog.DateFrom.Value.Date.AddDays(1);

                var hasAny =
                    this.Events.Any(
                        e =>
                        e.Event.DateTo.HasValue && dayAfter.Subtract(e.Event.DateTo.Value).TotalDays < 2
                        && dayAfter.Subtract(e.Event.DateTo.Value).TotalDays > -2);
                var events =
                    this.Events.Where(
                        e => e.Event.DateTo.HasValue && dayAfter.Subtract(e.Event.DateTo.Value).TotalDays < 2).ToList();
                if (!hasAny)
                {
                    hasAny =
                        this.Events.Any(
                            e =>
                            !e.Event.DateTo.HasValue && e.Event.DateFrom.HasValue
                            && dayAfter.Subtract(e.Event.DateFrom.Value).TotalDays < 2
                            && dayAfter.Subtract(e.Event.DateFrom.Value).TotalDays > -2);
                }
                if (!hasAny)
                {
                    hasAny =
                        this.Events.Any(
                            e =>
                            e.Event.DateFrom.HasValue && e.Event.DateFrom.Value.Subtract(dayBefore).TotalDays < 2 && e.Event.DateFrom.Value.Subtract(dayBefore).TotalDays > -2);

                    if (hasAny)
                    {
                        var list =
                            this.Events.Where(
                                e =>
                                e.Event.DateFrom.HasValue && e.Event.DateFrom.Value.Subtract(dayBefore).TotalDays < 2)
                                .ToList();
                        Debug.WriteLine(list.First().Event.DateFrom.Value.Subtract(dayBefore).TotalDays);
                    }
                }

                if (!hasAny)
                {
                    hasAny =
                        this.Events.Any(
                            e =>
                            e.Event.DateTo.HasValue && e.Event.DateTo.Value.Subtract(dayBefore).TotalDays < 2 && e.Event.DateTo.Value.Subtract(dayBefore).TotalDays > -2);
                }

                if (hasAny)
                {
                    MainWindow.MainWindowHandle.ShowMessage(
                        "Es gibt bereits einen Event einen Tag vorher oder nachher. Zusammenhängende Veranstaltungen müssen innerhalb einer Veranstaltungsdatei geführt werden");
                    return;
                }
            }

            Settings.Default.DataSource = this.GetFileName(this.selectInitialRuleDialog.EventTitle);

            MainWindow.MainWindowHandle.OpenEvent(
                Settings.Default.DataSource,
                true,
                this.selectInitialRuleDialog.SelectedRule.Value.Key);

            var datacontext = new ScrutinusContext();

            var eventObj = new Event
                               {
                                   Title = this.selectInitialRuleDialog.EventTitle,
                                   DateFrom = this.selectInitialRuleDialog.DateFrom,
                                   DateTo = this.selectInitialRuleDialog.DateTo,
                                   RuleName = this.selectInitialRuleDialog.SelectedRule.Value.Value
                               };

            var existingEvents = datacontext.Events.ToList();

            foreach (var existingEvent in existingEvents)
            {
                datacontext.Events.Remove(existingEvent);
            }

            datacontext.SaveChanges();

            datacontext.Events.Add(eventObj);
            datacontext.SaveChanges();

            MainWindow.MainWindowHandle.ViewModel.Event = eventObj;

            Settings.Default.Save();

            var competitionsPage = new Competitions();

            MainWindow.MainWindowHandle.ViewModel.ActivePage = competitionsPage;
            var viewModel = (CompetitionsViewModel)competitionsPage.DataContext;

            viewModel.EditEventCommand.Execute(null);
        }

        private void Open()
        {
            if (this.SelectedEvent == null)
            {
                return;
            }

            Settings.Default.DataSource = this.SelectedEvent.DatabaseFile;
            Settings.Default.UseSQLCompact = true;
            Settings.Default.Save();
            MainWindow.MainWindowHandle.OpenEvent(this.SelectedEvent.DatabaseFile, true);
            MainWindow.MainWindowHandle.CloseDialog();
        }

        #endregion
    }
}