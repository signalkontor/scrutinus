﻿// // TPS.net TPS8 Scrutinus
// // AboutDialogViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Linq;
using System.Reflection;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace Scrutinus.Dialogs.DialogViewModels
{
    internal class AboutDialogViewModel : ViewModelBase
    {
        #region Constructors and Destructors

        public AboutDialogViewModel()
        {
            this.CloseCommand = new RelayCommand(this.Close);
        }

        #endregion

        #region Public Properties

        public ICommand CloseCommand { get; set; }

        public string Database
        {
            get
            {
                return MainWindow.License.SQLServer ? "SQL Server" : "SQL Compact";
            }
        }

        public string License
        {
            get
            {
                return MainWindow.License.LicenseHolder;
            }
        }

        public string LicensedVersion
        {
            get
            {
                return "TPS.net 2018 - " + (MainWindow.License.SQLServer ? "Expert" : "Standard");
            }
        }

        public string Version
        {
            get
            {
                return this.GetProgramVersion();
            }
        }

        #endregion

        #region Methods

        private void Close()
        {
            MainWindow.MainWindowHandle.CloseDialog();
        }

        private string GetProgramVersion()
        {
            var result = "";

            var attributes =
                Assembly.GetExecutingAssembly()
                    .GetCustomAttributes(false)
                    .Where(t => t is AssemblyFileVersionAttribute)
                    .Cast<AssemblyFileVersionAttribute>();
            if (attributes.Any())
            {
                result = attributes.First().Version + "\r\n";
            }

            var infos =
                Assembly.GetExecutingAssembly()
                    .GetCustomAttributes(false)
                    .Where(t => t is AssemblyInformationalVersionAttribute)
                    .Cast<AssemblyInformationalVersionAttribute>();
            
            if (infos.Any())
            {
                result += infos.First().InformationalVersion;
            }

            return result;
        }

        #endregion
    }
}