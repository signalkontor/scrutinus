// // TPS.net TPS8 Scrutinus
// // WinnerCertificateDefinition.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System;
using System.Collections.ObjectModel;
using General;

namespace Scrutinus.Dialogs.DialogViewModels
{
    [Serializable]
    public class WinnerCertificateDefinition : NotificationObject
    {
        private double leftMargin;

        private double rightMargin;

        #region Constructors and Destructors

        public WinnerCertificateDefinition()
        {
            this.Elements = new ObservableCollection<WinnerCertificateElement>();
        }

        #endregion

        #region Public Properties

        public string CertificateName { get; set; }

        public double LeftMargin { get
        {
            return this.leftMargin;
        }
            set
            {
                this.leftMargin = value;
                this.OnPropertyChanged("LeftMargin");
            } 
        }

        public double RightMargin
        {
            get
            {
                return this.rightMargin;
            }
            set
            {
                this.rightMargin = value;
                this.OnPropertyChanged("RightMargin");
            }
        }

        public ObservableCollection<WinnerCertificateElement> Elements { get; set; }

        #endregion
    }
}