﻿// // TPS.net TPS8 Scrutinus
// // ImportExcelDialogViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Input;
using DataModel.Import;
using DataModel.Models;
using GalaSoft.MvvmLight;
using OfficeOpenXml;

namespace Scrutinus.Dialogs.DialogViewModels
{
    public class ExcelImportDefinition
    {
        #region Public Properties

        public int FieldId { get; set; }

        public int HeaderIndex { get; set; }

        #endregion
    }

    internal class ImportExcelDialogViewModel : ViewModelBase
    {
        #region Constructors and Destructors

        public ImportExcelDialogViewModel(string fileName, string workbookName, bool firstLineHasHeaders)
        {
            this.FieldList = new List<Tuple<string, int>>();
            this.FieldList.Add(new Tuple<string, int>("First Name Man", 1));
            this.FieldList.Add(new Tuple<string, int>("Last Name Man", 2));
            this.FieldList.Add(new Tuple<string, int>("First Name Woman", 3));
            this.FieldList.Add(new Tuple<string, int>("Last Name Woman", 4));
            this.FieldList.Add(new Tuple<string, int>("MIN Man", 5));
            this.FieldList.Add(new Tuple<string, int>("MIN Woman", 6));
            this.FieldList.Add(new Tuple<string, int>("Number in Competition", 7));
            this.FieldList.Add(new Tuple<string, int>("Country", 8));
            this.FieldList.Add(new Tuple<string, int>("Competition Id", 9));
        }

        #endregion

        #region Public Properties

        public ICommand CancelCommand { get; set; }

        public List<Tuple<string, int>> FieldList { get; set; }

        public List<Tuple<string, int>> HeaderList { get; set; }

        public ICommand ImportCommand { get; set; }

        public List<ExcelImportDefinition> ImportDefinitions { get; set; }

        public List<ImportViewModel> ImportedData { get; set; }

        #endregion

        #region Methods

        private void Import(string fileName, string workbookName, bool hasHeader, ScrutinusContext context)
        {
            using (var package = new ExcelPackage(new FileInfo(fileName)))
            {
                var sheet = package.Workbook.Worksheets.FirstOrDefault(n => n.Name == workbookName);
                if (sheet == null)
                {
                    return;
                }

                var row = hasHeader ? 2 : 1;

                while (row <= sheet.Cells.Rows)
                {
                    var import = new ImportViewModel();
                    this.ImportedData.Add(import);

                    var couple = new Couple();
                    var competitionId = "";
                    Competition competition = null;

                    foreach (var excelImportDefinition in this.ImportDefinitions)
                    {
                        var value = sheet.Cells[row, excelImportDefinition.HeaderIndex].Text;

                        switch (excelImportDefinition.FieldId)
                        {
                            case 1:
                                couple.FirstMan = value;
                                break;
                            case 2:
                                couple.LastMan = value;
                                break;
                            case 3:
                                couple.FirstWoman = value;
                                break;
                            case 4:
                                couple.LastWoman = value;
                                break;
                            case 5:
                                couple.MINMan = value;
                                break;
                            case 6:
                                couple.MINWoman = value;
                                break;
                            case 7:
                                import.Number = int.Parse(value);
                                break;
                            case 8:
                                couple.Country = value;
                                break;
                            case 9:
                                competitionId = value;
                                break;
                        }
                    }

                    if (!string.IsNullOrEmpty(competitionId))
                    {
                        competition = context.Competitions.FirstOrDefault(c => c.ExternalId == competitionId);
                    }

                    var state = 0;
                    if (competition == null)
                    {
                        state = 1;
                    }
                    else
                    {
                        import.Competition = competition;
                    }
                    var existingCouple =
                        context.Couples.FirstOrDefault(
                            c =>
                            c.FirstMan == couple.FirstMan && c.LastMan == couple.LastMan
                            && c.FirstWoman == couple.FirstWoman && c.LastWoman == couple.LastWoman
                            && c.Country == couple.Country);

                    if (existingCouple == null)
                    {
                        import.Couple = couple;
                        state += 2;
                    }
                    else
                    {
                        import.Couple = existingCouple;
                    }

                    import.State = state;
                    row++;
                }
            }
        }

        private void ReadHeaders(string fileName, string workbookName, bool hasHeaders)
        {
            this.HeaderList = new List<Tuple<string, int>>();

            using (var package = new ExcelPackage(new FileInfo(fileName)))
            {
                var sheet = package.Workbook.Worksheets.FirstOrDefault(n => n.Name == workbookName);
                if (sheet == null)
                {
                    return;
                }
                for (var i = 1; i <= sheet.Cells.Columns; i++)
                {
                    if (hasHeaders)
                    {
                        this.HeaderList.Add(new Tuple<string, int>(sheet.Cells[1, i].Text, i));
                    }
                    else
                    {
                        this.HeaderList.Add(new Tuple<string, int>(i.ToString(), i));
                    }
                }
            }
        }

        #endregion
    }
}