﻿// // TPS.net TPS8 Scrutinus
// // PrintCertificateDialogViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Input;
using DataModel.Models;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Scrutinus.Helper;
using Scrutinus.Localization;

namespace Scrutinus.Dialogs.DialogViewModels
{
    public enum CertificatePrintMode
    {
        Undefined,

        AllParticipants,

        FirstThreePlaces,

        SelectedParticipants
    }

    public class PrintCertificateDialogViewModel : ViewModelBase
    {
        #region Constructors and Destructors

        public PrintCertificateDialogViewModel()
        {
            this.context = new ScrutinusContext();

            this.Participants = new ObservableCollection<Participant>();
            this.CancelCommand = new RelayCommand(this.Cancel);
            this.PrintCommand = new RelayCommand(this.Print);
            this.PrintMode = CertificatePrintMode.AllParticipants;
            this.Copies = this.context.GetParameter("PrintCertificateDialogViewModel.Copies", 1);

            this.Load();
        }

        #endregion

        #region Fields

        private readonly ScrutinusContext context;

        private CertificatePrintMode printMode;

        private Round round;

        private WinnerCertificateDefinition selectedDefinition;

        private Participant selectedParticipant;

        #endregion

        #region Public Properties

        public ICommand CancelCommand { get; set; }

        public int Copies { get; set; }

        public ObservableCollection<Participant> Participants { get; set; }

        public ICommand PrintCommand { get; set; }

        public CertificatePrintMode PrintMode
        {
            get
            {
                return this.printMode;
            }
            set
            {
                this.printMode = value;
                this.RaisePropertyChanged(() => this.PrintMode);
            }
        }

        public string Printer { get; set; }

        public DialogResult Result { get; set; }

        public Round Round { 
            get
            {
                return this.round;

            }
            set
            {
                this.round = value;
                this.RaisePropertyChanged(() => this.Round);
                if (this.round.Competition.WinnerCertificateTemplate != null)
                {
                    this.SelectedDefinition = this.WinnerCertificateDefinitions.FirstOrDefault(c => c.CertificateName == this.round.Competition.WinnerCertificateTemplate);
                }
            } 
        }

        public WinnerCertificateDefinition SelectedDefinition
        {
            get
            {
                return this.selectedDefinition;
            }
            set
            {
                this.selectedDefinition = value;
                this.RaisePropertyChanged(() => this.SelectedDefinition);
            }
        }

        public Participant SelectedParticipant
        {
            get
            {
                return this.selectedParticipant;
            }
            set
            {
                this.selectedParticipant = value;
                this.RaisePropertyChanged(() => this.SelectedParticipant);
                this.PrintMode = CertificatePrintMode.SelectedParticipants;
            }
        }

        public ObservableCollection<WinnerCertificateDefinition> WinnerCertificateDefinitions { get; set; }

        #endregion

        #region Methods

        private void Cancel()
        {
            this.Result = DialogResult.Cancel;
            MainWindow.MainWindowHandle.CloseDialog();
        }

        private void Load()
        {
            this.WinnerCertificateDefinitions = new ObservableCollection<WinnerCertificateDefinition>(WinnerCertificateHelper.GetWinnerCertificates());

            var context = new ScrutinusContext();

            var lastCertificate = context.GetParameter<string>("PrintCertificateDialogViewModel.LastCertificate", null);

            if (lastCertificate != null)
            {
                this.SelectedDefinition =
                    this.WinnerCertificateDefinitions.FirstOrDefault(c => c.CertificateName == lastCertificate);
            }
            else
            {
                this.SelectedDefinition = this.WinnerCertificateDefinitions.FirstOrDefault();
            }
        }

        private void Print()
        {
            if (this.SelectedDefinition == null)
            {
                MainWindow.MainWindowHandle.ShowMessage(
                    LocalizationService.Resolve(() => Text.CertificateTemplateEmpty));
                return;
            }

            this.Result = DialogResult.OK;

            if (this.PrintMode == CertificatePrintMode.SelectedParticipants)
            {
                var participantToPrint = this.SelectedParticipant;
                this.Participants.Clear();
                this.Participants.Add(participantToPrint);
            }

            if (this.PrintMode == CertificatePrintMode.FirstThreePlaces)
            {
                var toRemove = this.Participants.Where(p => p.PlaceFrom > 3).ToList();
                foreach (var participant in toRemove)
                {
                    this.Participants.Remove(participant);
                }
            }

            if (this.PrintMode == CertificatePrintMode.AllParticipants)
            {
                var toRemove = this.Participants.Where(p => !p.QualifiedRound.RoundType.IsFinal).ToList();
                foreach (var participant in toRemove)
                {
                    this.Participants.Remove(participant);
                }
            }

            this.context.SetParameter(
                "PrintCertificateDialogViewModel.LastCertificate",
                this.SelectedDefinition.CertificateName);
            this.context.SetParameter("PrintCertificateDialogViewModel.Copies", this.Copies);

            MainWindow.MainWindowHandle.CloseDialog();
        }

        #endregion
    }
}