﻿// // TPS.net TPS8 Scrutinus
// // EditStartbuchDialogViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using DataModel;
using DataModel.Models;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using General.Rules.PointCalculation;

namespace Scrutinus.Dialogs.DialogViewModels
{
    public class StartbuchViewModel
    {
        #region Public Properties

        public int Id { get; set; }

        public AgeGroup AgeGroup { get; set; }

        public ICollection<Class> AvailableClasses { get; set; }

        public Class Class { get; set; }

        public Couple Couple { get; set; }

        public Class NextClass { get; set; }

        public int OriginalPlacings { get; set; }

        public int OriginalPoints { get; set; }

        public Section Section { get; set; }

        #endregion
    }

    public class EditStartbuchDialogViewModel : ViewModelBase
    {
        #region Fields

        private readonly ScrutinusContext context;

        #endregion

        #region Constructors and Destructors

        public EditStartbuchDialogViewModel()
        {
            this.CancelCommand = new RelayCommand(this.Cancel);
            this.SaveCommand = new RelayCommand(this.Save);
        }

        public EditStartbuchDialogViewModel(ScrutinusContext context, Couple couple)
            : this()
        {
            this.context = context;
            this.Classes = context.Classes.ToList();

            this.Couple = couple;
            this.Startbucher = this.CloneStartbucher();
        }

        #endregion

        #region Public Properties

        public ICommand CancelCommand { get; set; }

        public IEnumerable<Class> Classes { get; set; }

        public Couple Couple { get; set; }

        public ICommand SaveCommand { get; set; }

        public IEnumerable<StartbuchViewModel> Startbucher { get; set; }

        #endregion

        #region Methods

        private void Cancel()
        {
            MainWindow.MainWindowHandle.CloseDialog();
        }

        private IEnumerable<StartbuchViewModel> CloneStartbucher()
        {
            var result = new List<StartbuchViewModel>();
            foreach (var startbook in this.Couple.Startbooks)
            {
                var clonedBook = new StartbuchViewModel
                                     {
                                         Id = startbook.Id,
                                         AgeGroup = startbook.AgeGroup,
                                         Class = startbook.Class,
                                         Couple = startbook.Couple,
                                         Section = startbook.Section,
                                         OriginalPoints = startbook.OriginalPoints,
                                         OriginalPlacings = startbook.OriginalPlacings,
                                     };

                clonedBook.AvailableClasses = this.Classes.ToList();

                result.Add(clonedBook);
            }

            return result;
        }

        private void Save()
        {
            foreach (var startbuch in this.Startbucher)
            {
                // find the one:
                var book = this.Couple.Startbooks.Single(b => b.Id == startbuch.Id);
                if (book.Class.Id != startbuch.Class.Id || book.OriginalPlacings != startbuch.OriginalPlacings
                    || book.OriginalPoints != startbuch.OriginalPoints)
                {
                    book.PrintLaufzettel = true;
                    book.OriginalPoints = startbuch.OriginalPoints;
                    book.OriginalPlacings = startbuch.OriginalPlacings;
                    book.Class = startbuch.Class;
                    book.NextClass = this.context.Classes.FirstOrDefault(c => c.Id == startbuch.Class.Id + 1);

                    var participant = this.context.Participants.FirstOrDefault(p => p.Couple.Id == startbuch.Couple.Id && p.State == CoupleState.Dancing);

                    if (participant != null)
                    {
                        DTVPointCalculator.UpdateAllParticipants(this.context, participant.Id, book.OriginalPoints, book.OriginalPlacings, book.Class, true, true);
                    }
                }
            }

            this.context.SaveChanges();

            MainWindow.MainWindowHandle.CloseDialog();
        }

        #endregion
    }
}