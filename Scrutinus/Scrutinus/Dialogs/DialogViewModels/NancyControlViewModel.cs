﻿// // TPS.net TPS8 Scrutinus
// // NancyControlViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Windows.Input;
using DataModel.Models;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MahApps.Metro.Controls.Dialogs;
using NancyServer;
using Scrutinus.Localization;

namespace Scrutinus.Dialogs.DialogViewModels
{
    public class NancyControlViewModel : ViewModelBase
    {
        #region Constructors and Destructors

        public NancyControlViewModel()
        {
            this.NetworkAddresses = new ObservableCollection<string>();
            this.FillNetworkAddresses();

            this.ServerPort = Settings.Default.NancyServerPort;
            this.SelectedInterfaces = new List<string>();
            this.AutostartNancy = Settings.Default.AutoStartNancy;

            this.StartServerCommand = new RelayCommand(this.SaveSettingsAndStartNancyServer);
            this.StopServerCommand = new RelayCommand(this.StopNancyServer);
            this.CloseCommand = new RelayCommand(this.CloseWindow);

            this.ChairmanPassword = Settings.Default.ChairmanPassword;
            if (string.IsNullOrWhiteSpace(this.ChairmanPassword))
            {
                this.ChairmanPassword = "1234";
            }
        }

        #endregion

        #region Methods

        private void FillNetworkAddresses()
        {
            var hostName = Dns.GetHostName();

            this.NetworkAddresses.Add(hostName);
            var hostEntry = Dns.GetHostEntry(hostName);
            foreach (var ipAddress in hostEntry.AddressList)
            {
                var addrBytes = ipAddress.GetAddressBytes();
                this.NetworkAddresses.Add(
                    string.Format("{0}.{1}.{2}.{3}", addrBytes[0], addrBytes[1], addrBytes[2], addrBytes[3]));
            }

            this.NetworkAddresses.Add("localhost");
        }

        #endregion

        #region Public Properties

        public bool AutostartNancy { get; set; }

        public string ChairmanPassword { get; set; }

        public ICommand CloseCommand { get; set; }

        public ObservableCollection<string> NetworkAddresses { get; set; }

        public IList SelectedInterfaces { get; set; }

        public int ServerPort { get; set; }

        public ICommand StartServerCommand { get; set; }

        public ICommand StopServerCommand { get; set; }

        #endregion

        #region Public Methods and Operators

        public static void StartNancyServer(bool windowInitialized)
        {
            try
            {
                if (MainWindow.MainWindowHandle.ViewModel.NancyServer != null
                    && MainWindow.MainWindowHandle.ViewModel.NancyServer.IsRunning)
                {
                    MainWindow.MainWindowHandle.ViewModel.NancyServer.StopHost();
                }

                var userManager = new SimpleNancyUserManager();
                userManager.Users.Add(
                    new UserIdentity
                        {
                            Claims = new List<string> { "chairman", "marking" },
                            UserName = "chairman",
                            Password = Settings.Default.ChairmanPassword
                        });

                var urls =
                    Settings.Default.NancyHostNames.Split(',')
                        .Select(s => new Uri(string.Format("http://{0}:{1}", s, Settings.Default.NancyServerPort)))
                        .ToArray();

                MainWindow.MainWindowHandle.ViewModel.NancyServer = new HttpServer(
                    urls,
                    Settings.Default.DataSource,
                    userManager);

                HttpServer.ApplicationLocale = Settings.Default.LanguageLocale;
                HttpServer.ShowCarouselDroppedOut = Settings.Default.ShowCarouselDroppedOut;
                HttpServer.MaxCouplesPerPageInCarousel = Settings.Default.CarouselMaxCouples;

                MainWindow.MainWindowHandle.ViewModel.NancyServer.StartHost();
            }
            catch (Exception ex)
            {
                var message = "\r\n" + LocalizationService.Resolve(() => Text.NancyServerCannotStart);

                Console.WriteLine(message);

                if (windowInitialized)
                {
                    MainWindow.MainWindowHandle.ShowMessage(
                        ex.Message + message,
                        "Error starting Web Server",
                        MessageDialogStyle.Affirmative);
                }
                else
                {
                    throw;
                }
            }
        }

        public void CloseWindow()
        {
            Settings.Default.AutoStartNancy = this.AutostartNancy;
            Settings.Default.Save();

            MainWindow.MainWindowHandle.CloseDialog();
        }

        public void SaveSettingsAndStartNancyServer()
        {
            Settings.Default.NancyServerPort = this.ServerPort;
            Settings.Default.NancyHostNames = string.Join(",", this.SelectedInterfaces.Cast<string>().ToArray());
            Settings.Default.AutoStartNancy = this.AutostartNancy;
            Settings.Default.ChairmanPassword = this.ChairmanPassword;
            Settings.Default.Save();

            new ScrutinusContext().SetParameter("NancyServerControl.ChairmanPassword", this.ChairmanPassword);

            var urls =
                this.SelectedInterfaces.Cast<string>()
                    .Select(s => new Uri(string.Format("http://{0}:{1}", s, this.ServerPort)))
                    .ToArray();

            StartNancyServer(true);

            var url = urls.First().AbsoluteUri;
            Process.Start(url);
        }

        public void StopNancyServer()
        {
            if (MainWindow.MainWindowHandle.ViewModel.NancyServer != null)
            {
                MainWindow.MainWindowHandle.ViewModel.NancyServer.StopHost();
            }
            this.CloseWindow();
        }

        #endregion
    }
}