﻿// // TPS.net TPS8 Scrutinus
// // SelectTeamMembersViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using DataModel.Models;
using GalaSoft.MvvmLight.Command;
using General.Extensions;

namespace Scrutinus.Dialogs.DialogViewModels
{
    public class SelectTeamMembersViewModel
    {
        private readonly ScrutinusContext context;
        private Team selectedTeam;

        public SelectTeamMembersViewModel(ScrutinusContext context, IEnumerable<Team> teams)
        {
            this.context = context;
            this.Teams = new ObservableCollection<Team>(teams);
            this.Persons = new ObservableCollection<Person>();

            this.CancelCommand = new RelayCommand(this.Cancel);
            this.OkCommand = new RelayCommand(this.Save);
        }

        public Team SelectedTeam
        {
            get { return this.selectedTeam; }
            set
            {
                this.selectedTeam = value;
                this.FillPersonList();
            }
        }

        public ObservableCollection<Team> Teams { get; set; }

        public ICommand OkCommand { get; set; }

        public ICommand CancelCommand { get; set; }

        public ObservableCollection<Person> Persons { get; set; }

        private void FillPersonList()
        {
            this.Persons.Clear();
            this.Persons.AddRange(this.selectedTeam.TeamMembers);
        }

        private void Save()
        {
            this.context.SaveChanges();
            MainWindow.MainWindowHandle.CloseDialog();
        }

        private void Cancel()
        {
            MainWindow.MainWindowHandle.CloseDialog();
        }
    }
}
