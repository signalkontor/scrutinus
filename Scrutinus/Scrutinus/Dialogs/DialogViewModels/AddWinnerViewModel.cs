﻿// // TPS.net TPS8 Scrutinus
// // AddWinnerViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Input;
using DataModel;
using DataModel.Models;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using General.Extensions;
using Scrutinus.Localization;

namespace Scrutinus.Dialogs.DialogViewModels
{
    public class ParticipateWithPlaceViewModel
    {
        #region Public Properties

        public int CoupleId { get; set; }

        public string NameWithPlace { get; set; }

        public Participant Participant { get; set; }

        #endregion
    }

    public class AddWinnerViewModel : ViewModelBase
    {
        #region Constructors and Destructors

        public AddWinnerViewModel()
        {
           this.context = new ScrutinusContext();

            this.AddAsClimbUp = false;
            this.AddAsWinner = true;

            // initially only show competitions of today
            this.CompetitionComboLabel = Text.TodaysCompetition;
            this.Competitions = new ObservableCollection<Competition>(this.context.Competitions.ToList().Where(c => c.State == CompetitionState.Finished 
                                                                                                                && c.StartTime.HasValue 
                                                                                                                && c.StartTime.Value.Date == DateTime.Today));
            this.DoShowAllCompetitions = false;
            this.DoShowTodayCompetitions = true;

            this.Participants = new ObservableCollection<ParticipateWithPlaceViewModel>();

            this.SaveCommand = new RelayCommand(this.Save);
            this.CancelCommand = new RelayCommand(this.Cancel);
            this.ShowAllCompetitionsCommand = new RelayCommand(this.ShowAllCompetitions);
            this.ShowTodaysCompetitionsCommand = new RelayCommand(this.ShowTodaysCompetitions);
        }

        #endregion

        #region Fields

        private Competition selectedCompetition;

        private ParticipateWithPlaceViewModel selectedParticipant;

        private readonly ScrutinusContext context;

        private string competitionComboLabel;

        #endregion

        #region Public Properties

        private bool addAsClimbUp;
        public bool AddAsClimbUp
        {
            get { return addAsClimbUp; }
            set { this.addAsClimbUp = value; this.RaisePropertyChanged(() => this.AddAsClimbUp); }
        }

        private bool doShowTodayCompetitions;

        public bool DoShowTodayCompetitions
        {
            get { return doShowTodayCompetitions; }
            set { doShowTodayCompetitions = value; this.RaisePropertyChanged(() => this.DoShowTodayCompetitions);}
        }

        private bool showAllCompetitions;

        public bool DoShowAllCompetitions
        {
            get { return showAllCompetitions; }
            set { showAllCompetitions = value; this.RaisePropertyChanged(() => this.DoShowAllCompetitions); }
        }

        private bool addAsWinner;
        public bool AddAsWinner
        {
            get { return addAsWinner; }
            set { this.addAsWinner = value; this.RaisePropertyChanged(() => this.AddAsWinner); }
        }

        public Competition Competition { get; set; }

        public ICommand CancelCommand { get; set; }

        public ObservableCollection<Competition> Competitions { get; set; }

        public ObservableCollection<ParticipateWithPlaceViewModel> Participants { get; set; }

        public DialogResult Result { get; set; }

        public ICommand SaveCommand { get; set; }

        public ICommand ShowAllCompetitionsCommand { get; set; }

        public ICommand ShowTodaysCompetitionsCommand { get; set; }

        public string CompetitionComboLabel
        {
            get { return this.competitionComboLabel; }
            set
            {
                this.competitionComboLabel = value;
                RaisePropertyChanged(() => this.CompetitionComboLabel);
            }
        }

        public Competition SelectedCompetition
        {
            get
            {
                return this.selectedCompetition;
            }
            set
            {
                this.selectedCompetition = value;
                if (this.selectedCompetition != null)
                {
                    this.FillParticipants();
                    this.SelectedParticipant = this.Participants.FirstOrDefault(p => p.Participant.PlaceFrom == 1);
                }
                this.RaisePropertyChanged(() => this.SelectedCompetition);
            }
        }

        public ParticipateWithPlaceViewModel SelectedParticipant {
            get
            {
                return this.selectedParticipant;
            }
            set
            {
                this.selectedParticipant = value;
                this.RaisePropertyChanged(() => this.SelectedParticipant);

                if (this.selectedParticipant == null)
                {
                    return;
                }

                if (this.selectedCompetition.ClimbUps.Any(c => c.Couple.Id == selectedParticipant.CoupleId))
                {
                    this.AddAsClimbUp = true;
                    this.AddAsWinner = false;
                }
                else
                {
                    this.AddAsClimbUp = false;
                    this.AddAsWinner = true;
                }
            }
        }


        public void SetCompetition(int competitionId)
        {
            var competition = this.context.Competitions.First(c => c.Id == competitionId);

            // try to find a similar competition but lower class:
            var lower =
                this.Competitions.FirstOrDefault(
                    c =>
                    c.AgeGroup.Id == competition.AgeGroup.Id && c.Section.Id == competition.Section.Id
                    && c.Class.Id == competition.Class.Id - 1);

            if (lower != null)
            {
                this.SelectedCompetition = lower;
            }
        }

        #endregion

        #region Methods

        private void Cancel()
        {
            this.Result = DialogResult.Cancel;
            MainWindow.MainWindowHandle.CloseDialog();
        }

        private void FillParticipants()
        {
            this.Participants.Clear();
            // We do not add BSW couples:
            foreach (
                var participant in
                    this.SelectedCompetition.Participants.Where(
                        p => p.PlaceFrom.HasValue && p.PlaceFrom.Value < 13 && p.Class.Id != 7)
                        .OrderBy(p => p.PlaceFrom))
            {
                this.Participants.Add(
                    new ParticipateWithPlaceViewModel
                        {
                            Participant = participant,
                            CoupleId = participant.Couple.Id,
                            NameWithPlace =
                                string.Format(
                                    "({0}) - {1} - {2}",
                                    participant.Place,
                                    participant.Number,
                                    participant.Couple.NiceName)
                        });
            }
        }

        private void ShowAllCompetitions()
        {
            this.CompetitionComboLabel = Text.AllCompetitions;
            this.Competitions.Clear();
            var compettions = this.context.Competitions.Where(c => c.State == CompetitionState.Finished);

            this.Competitions.AddRange(compettions);

            this.DoShowAllCompetitions = true;
            this.DoShowTodayCompetitions = false;
        }

        private void ShowTodaysCompetitions()
        {
            this.CompetitionComboLabel = Text.TodaysCompetition;
            this.Competitions.Clear();
            var compettions = this.context.Competitions.ToList().Where(c => c.State == CompetitionState.Finished 
                                                                   && c.StartTime.HasValue
                                                                   && c.StartTime.Value.Date == DateTime.Today);

            this.Competitions.AddRange(compettions);

            this.DoShowAllCompetitions = false;
            this.DoShowTodayCompetitions = true;
        }

        private void Save()
        {
            if (this.SelectedParticipant == null)
            {
                return;
            }
            this.Result = DialogResult.OK;
            MainWindow.MainWindowHandle.CloseDialog();
        }

        #endregion
    }
}