﻿// // TPS.net TPS8 Scrutinus
// // AddOfficialsFromDtvView.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows.Controls;

namespace Scrutinus.Dialogs.DialogViewModels
{
    /// <summary>
    /// Interaction logic for AddOfficialsFromDtvView.xaml
    /// </summary>
    public partial class AddOfficialsFromDtvView : Page
    {
        public AddOfficialsFromDtvView(AddOfficialsFromDtvViewModel viewModel)
        {
            this.DataContext = viewModel;

            this.InitializeComponent();

            viewModel.SelectedOfficials = this.OfficialsDataGrid.SelectedItems;
        }
    }
}
