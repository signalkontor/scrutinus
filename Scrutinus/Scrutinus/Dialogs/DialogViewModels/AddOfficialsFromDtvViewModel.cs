﻿// // TPS.net TPS8 Scrutinus
// // AddOfficialsFromDtvViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using System.Windows.Threading;
using CsQuery.ExtensionMethods.Internal;
using DataModel;
using DataModel.Models;
using GalaSoft.MvvmLight.Command;
using Scrutinus.ViewModels;

namespace Scrutinus.Dialogs.DialogViewModels
{
    public class AddOfficialsFromDtvViewModel : BaseViewModel
    {

        private new readonly ScrutinusContext context;

        private readonly List<DtvLicenseHolder> allOfficials;

        private string searchText;

        public AddOfficialsFromDtvViewModel()
        {
           
        }

        public AddOfficialsFromDtvViewModel(ScrutinusContext context)
        {
            this.context = context;
            this.allOfficials = this.context.DtvLicenseHolders.ToList();
            this.DtvOfficials = new ObservableCollection<DtvLicenseHolder>(this.allOfficials);

            this.AddCommand = new RelayCommand(this.Add);
            this.CloseCommand = new RelayCommand(this.Close);
            this.ClearSearchCommand = new RelayCommand(new Action(() => this.SearchText = ""));
        }

        public ObservableCollection<DtvLicenseHolder> DtvOfficials { get; set; }

        public IList SelectedOfficials { get; set; }

        public string SearchText
        {
            get => this.searchText;
            set
            {
                this.searchText = value;
                this.Search();
            }
        }

        public ICommand ClearSearchCommand { get; set; }

        public ICommand AddCommand { get; set; }

        public ICommand CloseCommand { get; set; }

        public override bool Validate()
        {
            return true;
        }

        private void Search()
        {
            if (string.IsNullOrEmpty(this.searchText))
            {
                this.DtvOfficials.Clear();
                this.DtvOfficials.AddRange(this.allOfficials);
                return;
            }

            if (this.searchText.Length < 3)
            {
                return;
            }

            var result =
                this.allOfficials.Where(
                    a =>
                        a.DtvId.ToLower().Contains(this.searchText.ToLower()) || a.FirstName.ToLower().Contains(this.searchText.ToLower()) ||
                        a.LastName.ToLower().Contains(this.searchText.ToLower()) || a.Club.ToLower().Contains(this.searchText.ToLower()));

            this.DtvOfficials.Clear();
            this.DtvOfficials.AddRange(result);
        }

        private void Close()
        {
            MainWindow.MainWindowHandle.CloseDialog();
        }

        private void Add()
        {
            if (this.SelectedOfficials == null || this.SelectedOfficials.Count == 0)
            {
                return;
            }

            var sign = context.Officials.Max(o => o.Sign);

            foreach(var dtvOfficial in this.SelectedOfficials.Cast<DtvLicenseHolder>())
            {
                sign = Increment(sign);

                var official = new Official()
                {
                    Firstname = dtvOfficial.FirstName,
                    Lastname = dtvOfficial.LastName,
                    Club = dtvOfficial.Club,
                    Nationality = dtvOfficial.Nationality,
                    ExternalId = dtvOfficial.DtvId,
                    Licenses = dtvOfficial.LicensesAsString,
                    Sign = sign
                };

                if (dtvOfficial.LicensesAsString.Contains("TL"))
                {
                    var role = this.context.Roles.FirstOrDefault(r => r.Id == Roles.Associate);
                    official.Roles.Add(role);
                    role = this.context.Roles.FirstOrDefault(r => r.Id == Roles.Supervisor);
                    official.Roles.Add(role);
                }

                if (dtvOfficial.LicensesAsString.Contains("WR"))
                {
                    var role = this.context.Roles.FirstOrDefault(r => r.Id == Roles.Judge);
                    official.Roles.Add(role);
                }

                this.context.Officials.Add(official);
            }
            this.context.SaveChanges();

            MainWindow.MainWindowHandle.CloseDialog();
        }

        static string Increment(string s)

        {

            // first case - string is empty: return "a"

            if ((s == null) || (s.Length == 0))
            {
                return "A";
            }
            // next case - last char is less than 'z': simply increment last char

            char lastChar = s[s.Length - 1];

            string fragment = s.Substring(0, s.Length - 1);

            if (lastChar < 'Z')

            {

                ++lastChar;

                return fragment + lastChar;

            }

            // next case - last char is 'z': roll over and increment preceding string

            return Increment(fragment) + 'A';

        }
    }
}
