﻿// // TPS.net TPS8 Scrutinus
// // PrintManagerViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Printing;
using System.Reflection;
using System.Windows;
using System.Windows.Input;
using System.Xml;
using System.Xml.Linq;
using CsQuery.ExtensionMethods.Internal;
using DataModel;
using DataModel.Models;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using General;
using GongSolutions.Wpf.DragDrop;
using Microsoft.Win32;
using Scrutinus.Localization;
using Scrutinus.Reports.Attributes;
using Scrutinus.Reports.BasicPrinting;

namespace Scrutinus.Dialogs.DialogViewModels
{
    public class PrinterViewModel
    {
        public string Name { get; set; }

        public PrintQueue PrintQueue { get; set; }
    }

    public class ReportViewModel
    {
        public string ReportName { get; set; }

        public string ReportClassName { get; set; }

        public PrintCollectionType PrintCollectionType { get; set; }
    }

    public class ExportModel
    {
        public List<Printer> Printers { get; set; }

        public List<PrintReportDefinition> PrintReportDefinitions { get; set; }
    }

    public class PrintManagerDeleteViewModel : IDropTarget
    {
        private readonly PrintManagerViewModel printManagerViewModel;

        public PrintManagerDeleteViewModel(PrintManagerViewModel printManagerViewModel)
        {
            this.printManagerViewModel = printManagerViewModel;
        }

        public void DragOver(IDropInfo dropInfo)
        {
            if (dropInfo.Data is PrintReportDefinition || dropInfo.Data is Printer)
            {
                dropInfo.DropTargetAdorner = DropTargetAdorners.Highlight;
                dropInfo.Effects = DragDropEffects.Move;
            }
        }

        public void Drop(IDropInfo dropInfo)
        {
            if (dropInfo.Data is PrintReportDefinition)
            {
                this.printManagerViewModel.RemovePrintReportDefintion((PrintReportDefinition) dropInfo.Data);
            }

            if (dropInfo.Data is Printer)
            {
                this.printManagerViewModel.RemovePrinter((Printer) dropInfo.Data);
            }
        }
    }

    public class PrintManagerViewModel : ViewModelBase, IDropTarget
    {
        private List<ReportViewModel> availableReports;

        private readonly ScrutinusContext context;

        private Tuple<string, PrintCollectionType> selectedPrintCollection;

        private Printer selectedPrinter;

        private bool selectedPrinterCanPrintColor;
        private PrinterViewModel selectedPrintQueue;

        private PrintReportDefinition selectedPrintReportDefinition;

        public PrintManagerViewModel()
        {
            this.context = new ScrutinusContext();
            this.PrintQueues = new ObservableCollection<PrinterViewModel>();
            this.InputBins = new ObservableCollection<string>();
            this.Printers = new ObservableCollection<Printer>(this.context.Printers);

            this.PrintDefinitions = new ObservableCollection<PrintReportDefinition>();
            this.PrintCollections = new List<Tuple<string, PrintCollectionType>>();
            this.PrintReports = new ObservableCollection<ReportViewModel>();

            this.SetupPrintCollections();
            this.SetupPrintReports();
            
            this.NewPrinterCommand = new RelayCommand(this.NewPrinter);
            this.CloseCommand = new RelayCommand(this.Close);
            this.ExportCommand = new RelayCommand(this.Export);
            this.ImportCommand = new RelayCommand(this.Import);

            this.DeleteViewModel = new PrintManagerDeleteViewModel(this);

            this.SetupQueues();
            this.FillAvailableReportList();

            this.SelectedPrintCollection = this.PrintCollections.First();
        }

        public PrintManagerDeleteViewModel DeleteViewModel { get; set; }

        public ObservableCollection<PrinterViewModel> PrintQueues { get; set; }

        public ObservableCollection<string> InputBins { get; set; }

        public ObservableCollection<Printer> Printers { get; set; }

        public ObservableCollection<PrintReportDefinition> PrintDefinitions { get; set; }


        public List<Tuple<string, PrintCollectionType>> PrintCollections { get; set; }

        public ObservableCollection<ReportViewModel> PrintReports { get; set; }

        public ICommand NewPrinterCommand { get; private set; }

        public ICommand CloseCommand { get; set; }

        public ICommand ExportCommand { get; set; }

        public ICommand ImportCommand { get; set; }

        public PrinterViewModel SelectedPrintQueue
        {
            get
            {
                return this.selectedPrintQueue;
            }
            set
            {
                this.selectedPrintQueue = value;
                if (this.selectedPrinter != null && value != null)
                {
                    this.selectedPrinter.PrintQueueName = value.PrintQueue?.Name ?? "Standard";
                }
                this.SetupCapabilities();
                this.RaisePropertyChanged(() => this.SelectedPrintQueue);
                this.context.SaveChanges();
            }
        }

        public Action ClosingAction { get; set; }

        public bool SelectedPrinterCanPrintColor
        {
            get
            {
                return this.selectedPrinterCanPrintColor;
            }
            set
            {
                this.selectedPrinterCanPrintColor = value;
                this.RaisePropertyChanged(() => this.SelectedPrinterCanPrintColor);
            }
        }

        public Tuple<string, PrintCollectionType> SelectedPrintCollection
        {
            get { return this.selectedPrintCollection; }
            set
            {
                this.selectedPrintCollection = value;
                this.SetupPrintDefinitions();
                this.SetupPrintReports();
                this.RaisePropertyChanged(() => this.SelectedPrintCollection);
            }
        }

        public Printer SelectedPrinter
        {
            get { return this.selectedPrinter; }
            set
            {
                this.selectedPrinter = value;

                if (this.selectedPrinter != null)
                {
                    var queue = this.PrintQueues.FirstOrDefault(q => q.Name == this.selectedPrinter.PrintQueueName);
                    this.SelectedPrintQueue = queue;
                }

                this.RaisePropertyChanged(() => this.SelectedPrinter);
            }
        }

        public PrintReportDefinition SelectedPrintReportDefinition
        {
            get { return this.selectedPrintReportDefinition; }
            set
            {
                this.selectedPrintReportDefinition = value;
                this.RaisePropertyChanged(() => this.SelectedPrintReportDefinition);
            }
        }

        public void DragOver(IDropInfo dropInfo)
        {
            if (dropInfo.Data is ReportViewModel || dropInfo.Data is Printer)
            {
                dropInfo.DropTargetAdorner = DropTargetAdorners.Highlight;
                dropInfo.Effects = DragDropEffects.Copy;
            }            
        }

        public void Drop(IDropInfo dropInfo)
        {
            if (dropInfo.Data is ReportViewModel)
            {
                var model = (ReportViewModel) dropInfo.Data;

                var printReportDefinition = new PrintReportDefinition()
                {
                    ReportName = model.ReportName,
                    Copies = 1,
                    PrintCollectionType = model.PrintCollectionType,
                    PrintReportType = model.ReportClassName,
                    Printer = this.Printers.FirstOrDefault()
                };

                this.PrintDefinitions.Add(printReportDefinition);
                this.context.PrintReportDefinitions.Add(printReportDefinition);
                this.context.SaveChanges();
            }
        }

        internal void RemovePrintReportDefintion(PrintReportDefinition toRemove)
        {
            this.context.PrintReportDefinitions.Remove(toRemove);
            this.context.SaveChanges();
            this.PrintDefinitions.Remove(toRemove);
        }

        internal void RemovePrinter(Printer toRemove)
        {
            if (this.context.PrintReportDefinitions.Any(d => d.Printer.Id == toRemove.Id))
            {
                MainWindow.MainWindowHandle.ShowMessageAsync("The printer cannot be deleted because it is used");
                return;
            }

            this.context.Printers.Remove(toRemove);
            this.context.SaveChanges();
            this.Printers.Remove(toRemove);
        }

        private void SetupPrintDefinitions()
        {
            var definitions =
                this.context.PrintReportDefinitions.Where(
                    p => p.PrintCollectionType == this.selectedPrintCollection.Item2);

            this.PrintDefinitions.Clear();
            this.PrintDefinitions.AddRange(definitions); 
        }

        private void SetupPrintCollections()
        {
            this.PrintCollections.Add(new Tuple<string, PrintCollectionType>(LocalizationService.Resolve(() => Text.BeforFirstRound), PrintCollectionType.BeforeFirstRound));
            this.PrintCollections.Add(new Tuple<string, PrintCollectionType>(LocalizationService.Resolve(() => Text.BeforeQualificationRound), PrintCollectionType.BeforeQualificationRound));
            this.PrintCollections.Add(new Tuple<string, PrintCollectionType>(LocalizationService.Resolve(() => Text.BeforeFinal), PrintCollectionType.BeforeFinal));
            this.PrintCollections.Add(new Tuple<string, PrintCollectionType>(LocalizationService.Resolve(() => Text.AfterQualificationRound), PrintCollectionType.AfterQualificationRound));
            this.PrintCollections.Add(new Tuple<string, PrintCollectionType>(LocalizationService.Resolve(() => Text.AfterFinal), PrintCollectionType.AfterFinal));
        }

        private void FillAvailableReportList()
        {
            this.availableReports = new List<ReportViewModel>();

            // get all types that implement abstract printer:
            var type = typeof(AbstractPrinter);

            var types =
                AppDomain.CurrentDomain.GetAssemblies()
                    .SelectMany(GetTypesSafely)
                    .Where(p => type.IsAssignableFrom(type) && !p.IsAbstract);

            foreach (var t in types)
            {
                var attributes = t.GetCustomAttributes(typeof(CanPrintBeforeQualificationRoundAttribute), false);
                if (attributes.Any())
                {
                    this.availableReports.Add(new ReportViewModel()
                    {
                        PrintCollectionType = PrintCollectionType.BeforeFirstRound,
                        ReportClassName = t.FullName,
                        ReportName = LocalizationService.Resolve("Scrutinus:Text:" + t.Name.Replace("Printer", ""))
                    });
                }

                attributes = t.GetCustomAttributes(typeof(CanPrintBeforeQualificationRoundAttribute), false);
                if (attributes.Any())
                {
                    this.availableReports.Add(new ReportViewModel()
                                                  {
                                                      PrintCollectionType = PrintCollectionType.BeforeQualificationRound,
                                                      ReportClassName = t.FullName,
                                                      ReportName = LocalizationService.Resolve("Scrutinus:Text:" + t.Name.Replace("Printer", ""))
                                                  });
                }

                attributes = t.GetCustomAttributes(typeof(CanPrintAfterQualificationRoundAttribute), false);
                if (attributes.Any())
                {
                    this.availableReports.Add(new ReportViewModel()
                    {
                        PrintCollectionType = PrintCollectionType.AfterQualificationRound,
                        ReportClassName = t.FullName,
                        ReportName = LocalizationService.Resolve("Scrutinus:Text:" + t.Name.Replace("Printer", ""))
                    });
                }

                attributes = t.GetCustomAttributes(typeof(CanPrintBeforeFinalRoundAttribute), false);
                if (attributes.Any())
                {
                    this.availableReports.Add(new ReportViewModel()
                    {
                        PrintCollectionType = PrintCollectionType.BeforeFinal,
                        ReportClassName = t.FullName,
                        ReportName = LocalizationService.Resolve("Scrutinus:Text:" + t.Name.Replace("Printer", ""))
                    });
                }

                attributes = t.GetCustomAttributes(typeof(CanPrintAfterFinalRoundAttribute), false);
                if (attributes.Any())
                {
                    this.availableReports.Add(new ReportViewModel()
                    {
                        PrintCollectionType = PrintCollectionType.AfterFinal,
                        ReportClassName = t.FullName,
                        ReportName = LocalizationService.Resolve("Scrutinus:Text:" + t.Name.Replace("Printer", ""))
                    });
                }
            }
        }

        private static IEnumerable<Type> GetTypesSafely(Assembly assembly)
        {
            try
            {
                return assembly.GetTypes();
            }
            catch (ReflectionTypeLoadException ex)
            {
                return ex.Types.Where(x => x != null);
            }
        }


        private void SetupCapabilities()
        {
            if (this.selectedPrintQueue == null || this.selectedPrintQueue.PrintQueue == null)
            {
                return;
            }

            var xmlDoc = new XmlDocument();
            xmlDoc.Load(this.selectedPrintQueue.PrintQueue.GetPrintCapabilitiesAsXml());
            if (Directory.Exists(@"d:\temp\"))
            {
                File.WriteAllText(@"d:\temp\printer.xml", xmlDoc.OuterXml);
            }

            this.SetupBinsFromXml(this.selectedPrintQueue.PrintQueue.GetPrintCapabilitiesAsXml());

            var capabilities = this.selectedPrintQueue.PrintQueue.GetPrintCapabilities();
            
            if (capabilities.OutputColorCapability.Any(p => p == OutputColor.Color))
            {
                this.SelectedPrinterCanPrintColor = true;
            }
            else
            {
                this.SelectedPrinterCanPrintColor = false;
            }
        }

        private void SetupBinsFromXml(Stream xmlStream)
        {
            var xElement = XElement.Load(xmlStream);

            foreach (var element in xElement.Elements())
            {
                Debug.WriteLine(element.Name);
            }

            var binFeature = xElement.Elements().Where(n => n.Name.LocalName == "Feature" && n.HasAttributes && n.Attribute("name").Value == "psk:JobInputBin");
            var options = binFeature.Elements().Where(e => e.Name.LocalName == "Option"); //.Select(e => e.Descendants(XName.Get("Value", "psf")).First().Value);

            this.InputBins.Clear();
            this.InputBins.Add("Standard");

            foreach (var option in options)
            {
                this.InputBins.Add(option.Value);

                var bins = option.Descendants(XName.Get("Value", "psf"));

                foreach (var bin in bins)
                {
                    this.InputBins.Add(option.Value);
                }
            }
        }


        private void SetupQueues()
        {
            EnumeratedPrintQueueTypes[] enumerationFlags = {EnumeratedPrintQueueTypes.Local,
                                                EnumeratedPrintQueueTypes.Connections};

            var printServer = new LocalPrintServer();
            var queues = printServer.GetPrintQueues(enumerationFlags);

            this.PrintQueues.Add(new PrinterViewModel() { Name = "Standard", PrintQueue = null });
            foreach (var queue in queues)
            {
                this.PrintQueues.Add(new PrinterViewModel() { Name = queue.Name, PrintQueue = queue });
            }
        }

        private void NewPrinter()
        {
            var printer = new Printer()
            {
                Name = LocalizationService.Resolve(() => Text.NewPrinter),
                PrintQueueName = this.PrintQueues.FirstOrDefault()?.Name,
            };

            this.context.Printers.Add(printer);
            this.context.SaveChanges();
            this.Printers.Add(printer);
            this.SelectedPrinter = printer;
        }

        private void SetupPrintReports()
        {
            if (this.selectedPrintCollection == null)
            {
                return;
            }

            this.PrintReports.Clear();
            this.PrintReports.AddRange(this.availableReports.Where(r => r.PrintCollectionType == this.selectedPrintCollection.Item2));
        }

        private void Close()
        {
            this.context.SaveChanges();
            this.ClosingAction();
        }

        private void Export()
        {
            var dlg = new SaveFileDialog {DefaultExt = ".json"};

            if (dlg.ShowDialog() != true)
            {
                return;
            }

            var data = new ExportModel
            {
                Printers = this.context.Printers.ToList(),
                PrintReportDefinitions = this.context.PrintReportDefinitions.ToList()
            };

            var json = JsonHelper.SerializeObject(data);

            File.WriteAllText(dlg.FileName, json);
        }

        private void Import()
        {
            var dlg = new OpenFileDialog {DefaultExt = ".json"};

            if (dlg.ShowDialog() != true)
            {
                return;
            }

            var json = File.ReadAllText(dlg.FileName);

            var data = JsonHelper.DeserializeData<ExportModel>(json);

            // Import this data

            foreach (var printer in data.Printers)
            {
                printer.Id = 0;
                this.context.Printers.Add(printer);
                this.Printers.Add(printer);
            }

            this.context.SaveChanges();

            foreach (var report in data.PrintReportDefinitions)
            {
                report.Printer = data.Printers.FirstOrDefault(p => p.Name == report.Printer.Name);
                this.context.PrintReportDefinitions.Add(report);
                this.PrintDefinitions.Add(report);
            }

            this.context.SaveChanges();
        }
    }
}
