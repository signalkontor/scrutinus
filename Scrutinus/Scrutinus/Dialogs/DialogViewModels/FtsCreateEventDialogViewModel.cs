﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using DataModel.Models;
using FtsRestAPI;
using FtsRestAPI.DTO;
using GalaSoft.MvvmLight.Command;
using MahApps.Metro.Controls.Dialogs;
using Competition = FtsRestAPI.DTO.Competition;

namespace Scrutinus.Dialogs.DialogViewModels
{
    public class FtsCreateEventDialogViewModel
    {
        public ObservableCollection<FtsRestAPI.DTO.CompetitionSimple> Events { get; set; }

        public ICommand OkCommand { get; set; }

        public ICommand CancelCommand { get; set; }

        public CompetitionSimple SelectedCompetition { get; set; }

        public Action ClosingAction { get; set; }

        private Dispatcher dispatcher;

        public FtsCreateEventDialogViewModel()
        {
            this.dispatcher = Dispatcher.CurrentDispatcher;
            this.Events = new ObservableCollection<CompetitionSimple>();
            this.OkCommand = new RelayCommand(this.OnOk);
            this.CancelCommand = new RelayCommand(this.OnCancel);
            // Trigger download of current events:

            var apiClient = new FtsRestClient(Settings.Default.WDSFApiUser, Settings.Default.WDSFApiPassword);

            apiClient.GetCurrentCompetitions().ContinueWith((task) =>
            {
                foreach (var competition in task.Result.Competitions)
                {
                    this.dispatcher.Invoke(() => this.Events.Add(competition));
                }
            });
        }

        private void OnOk()
        {
            this.CreateTpsEvent();
            this.ClosingAction();
        }

        private void OnCancel()
        {
            this.ClosingAction();
        }

        private Task CreateTpsEvent()
        {
            return Task.Factory.StartNew(() =>
            {
                Task<ProgressDialogController> controller = null;

                this.dispatcher.Invoke(() =>
                {
                    controller =
                        MainWindow.MainWindowHandle.ShowProgressAsync("Creating event", "Event will now be created",
                            false);
                });
   
                this.CreateData();

                this.dispatcher.Invoke(() =>
                {
                    controller.Result.CloseAsync();
                });
                
            });
        }

        private async void CreateData()
        {
            //var context = new ScrutinusContext();
            //var apiClient = new FtsRestClient(Settings.Default.WDSFApiUser, Settings.Default.WDSFApiPassword);

            //var details = await apiClient.GetCompetitionDetails(this.SelectedCompetition.id);

            //foreach (var category in details.Categories)
            //{
            //    context
            //}

        }
    }
}
