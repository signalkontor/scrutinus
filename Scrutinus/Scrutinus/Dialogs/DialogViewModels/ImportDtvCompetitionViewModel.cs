﻿// // TPS.net TPS8 Scrutinus
// // ImportDtvCompetitionViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using DataModel;
using DtvEsvModule;
using DtvEsvModule.Model;
using GalaSoft.MvvmLight.Command;
using Scrutinus.Localization;
using Scrutinus.ViewModels;

namespace Scrutinus.Dialogs.DialogViewModels
{
    public class ImportDtvCompetitionViewModel : BaseViewModel
    {
        #region Constructors and Destructors

        public ImportDtvCompetitionViewModel()
        {
            try
            {
                this.apiClient = new ApiClient(
                    Settings.Default.DtvApiUrl,
                    Settings.Default.WDSFApiUser,
                    Settings.Default.WDSFApiPassword);

                var events = this.apiClient.GetEvents();

                if (events != null)
                {
                    this.Events = new ObservableCollection<DtvEsvModule.Model.EventListElement>(events.ToList());
                }

                this.dispatcher = Dispatcher.CurrentDispatcher;
            }
            catch (Exception ex)
            {
                MainWindow.MainWindowHandle.ShowMessage(LocalizationService.Resolve(() => Text.ErrorLoadingEvents) +  " : " + ex.Message);
                this.Events = new ObservableCollection<DtvEsvModule.Model.EventListElement>();
            }

            this.ImportCommand = new RelayCommand(this.Import);

            this.CloseCommand = new RelayCommand(this.Close);
        }

        #endregion

        #region Public Methods and Operators

        public override bool Validate()
        {
            return true;
        }

        #endregion

        #region Fields

        private readonly ApiClient apiClient;

        private readonly Dispatcher dispatcher;

        #endregion

        #region Public Properties

        public ICommand CloseCommand { get; set; }

        public ObservableCollection<DtvEsvModule.Model.EventListElement> Events { get; set; }

        public ICommand ImportCommand { get; set; }

        public DtvEsvModule.Model.EventListElement SelectedElement { get; set; }

        #endregion

        #region Methods

        private void Close()
        {
            MainWindow.MainWindowHandle.CloseDialog();
        }

        private void Import()
        {
            if (this.SelectedElement == null)
            {
                return;
            }

            Veranstaltung ev = null;

            try
            {
                ev = this.apiClient.GetVeranstaltung(this.SelectedElement.id);
            }
            catch (Exception ex)
            {
                MainWindow.MainWindowHandle.ShowMessageAsync(ex.Message);
                return;
            }

            Task.Factory.StartNew(
                () =>
                {
                    var importer = new DtvApiImporter(
                        Settings.Default.DtvApiUrl,
                        Settings.Default.WDSFApiUser,
                        Settings.Default.WDSFApiPassword,
                        this.context);

                    importer.ImportAsync(
                        MainWindow.MainWindowHandle,
                        ev,
                        MainWindow.License != null && MainWindow.License.Ejudge
                            ? MarkingTypes.Marking
                            : MarkingTypes.MarksSumOnly).ContinueWith(new Action<Task>(
                                (t) =>
                                {
                                    MainWindow.MainWindowHandle.RemoveStatusReport();

                                    MainWindow.MainWindowHandle.ShowMessage(
                                        "Die Daten wurden vom DTV Portal importiert.");
                                    // Sync Navigation:
                                    this.dispatcher.Invoke(
                                        new Action(
                                            () =>
                                            {
                                                MainWindow.MainWindowHandle.ViewModel.Competitions.Clear();
                                                MainWindow.MainWindowHandle.ViewModel.ClearCompetitions();

                                                foreach (var competition in this.context.Competitions)
                                                {
                                                    MainWindow.MainWindowHandle.AddCompetitionToNavigationList(
                                                        competition);
                                                }
                                            }));
                                }));


                });

            this.Close();
        }

        #endregion
    }
}