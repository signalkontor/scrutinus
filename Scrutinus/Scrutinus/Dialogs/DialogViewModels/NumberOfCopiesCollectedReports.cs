// // TPS.net TPS8 Scrutinus
// // NumberOfCopiesCollectedReports.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System;

namespace Scrutinus.Dialogs.DialogViewModels
{
    [Serializable]
    public class NumberOfCopiesCollectedReports
    {
        #region Public Properties

        public int NumberDtvAppendix { get; set; }

        public int NumberDtvPart1 { get; set; }

        public int NumberFirstPage { get; set; }

        public int NumberResultTotal { get; set; }

        public int NumberSkatingTable { get; set; }

        public int NumberSortedByResult { get; set; }

        public int NumberStartListWithResult { get; set; }

        #endregion
    }
}