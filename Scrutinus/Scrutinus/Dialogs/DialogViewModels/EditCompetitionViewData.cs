﻿// // TPS.net TPS8 Scrutinus
// // EditCompetitionViewData.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Linq;
using System.Windows.Input;
using DataModel.Models;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace Scrutinus.Dialogs.DialogViewModels
{
    internal class EditCompetitionViewData : ViewModelBase
    {
        #region Constructors and Destructors

        public EditCompetitionViewData(int competitionId)
        {
            this.Context = new ScrutinusContext();
            this.Competition = this.Context.Competitions.Single(c => c.Id == competitionId);

            this.SaveCommand = new RelayCommand(this.SaveAndClose);
            this.CloseCommand = new RelayCommand(this.Close);
        }

        #endregion

        #region Fields

        private Competition competition;

        private ScrutinusContext context;

        #endregion

        #region Public Properties

        public ICommand CloseCommand { get; set; }

        public Competition Competition
        {
            get
            {
                return this.competition;
            }
            set
            {
                this.competition = value;
                this.RaisePropertyChanged(() => this.Competition);
            }
        }

        public ScrutinusContext Context
        {
            get
            {
                return this.context;
            }
            set
            {
                this.context = value;
                this.RaisePropertyChanged(() => this.Context);
            }
        }

        public ICommand ControlSaveCommand { get; set; }

        public ICommand SaveCommand { get; set; }

        #endregion

        #region Methods

        private void Close()
        {
            MainWindow.MainWindowHandle.CloseDialog();
        }

        private void SaveAndClose()
        {
            if (this.ControlSaveCommand != null)
            {
                this.ControlSaveCommand.Execute(null);
            }
            MainWindow.MainWindowHandle.CloseDialog();
        }

        #endregion
    }
}