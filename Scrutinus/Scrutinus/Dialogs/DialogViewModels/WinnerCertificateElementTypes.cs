// // TPS.net TPS8 Scrutinus
// // WinnerCertificateElementTypes.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH
namespace Scrutinus.Dialogs.DialogViewModels
{
    public enum WinnerCertificateElementTypes
    {
        StaticText,

        Picture,

        EventName,

        CompetitionTitle,

        EventPlace,

        Place,

        PlaceOwnCompetition,

        GroupClassKind,

        GroupClassKindLong,

        AgeGroup,

        AgeGroupLong,

        CompetitionKind,

        CompetitionKindShort,

        Class,

        CoupleName,

        CoupleNameWomanMan,

        ManName,

        ManLastName,

        ManFirstName,

        WomanName,

        WomanLastName,

        WomanFirstName,

        Date,

        NameOfSupervisor,

        NameOfAssociate,

        NameOfChairperson,

        CountryClub
    }
}