﻿// // TPS.net TPS8 Scrutinus
// // FindWdsfStarCouplesViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Linq;
using System.Windows.Input;
using DataModel.Models;
using GalaSoft.MvvmLight.Command;
using Scrutinus.ViewModels;
using Wdsf.Api.Client;

namespace Scrutinus.Dialogs.DialogViewModels
{
    public class FindWdsfStarCouplesViewModel : BaseViewModel
    {
        public FindWdsfStarCouplesViewModel()
        {
            this.CheckCommand = new RelayCommand(this.CheckWdsfApi);
            this.CancelCommand = new RelayCommand(this.Cancel);
        }

        public ICommand CheckCommand { get; set; }

        public ICommand CancelCommand { get; set; }

        public Competition Competition { get; set; }

        public int NumberOfStars { get; set; }

        public override bool Validate()
        {
            return true;
        }

        private void CheckWdsfApi()
        {
            var client = new Client(Settings.Default.WDSFApiUser, Settings.Default.WDSFApiPassword, WdsfEndpoint.Services);

            var section = this.Competition.Section.Id == 1 ? "Standard" : "Latin";
            var ageGroup = "Adult";
            var division = "General";

            switch (this.Competition.AgeGroup.Id)
            {
                case 6:
                    ageGroup = "Youth";
                    break;
                case 7:
                    ageGroup = "Adult";
                    break;
                case 10:
                    ageGroup = "Senior I";
                    break;
                case 11:
                    ageGroup = "Senior II";
                    break;
                case 12:
                    ageGroup = "Senior III";
                    break;
            }

            var date = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);

            var rankingList = client.GetWorldRanking(section, ageGroup, division, date).Where(r => r.Rank < 51).OrderBy(r => r.Rank);
            var stars = 0;

            foreach (var ranking in rankingList)
            {
                // do we have this couple?
                var couple =
                    this.Competition.Participants.FirstOrDefault(
                        p => p.Couple.MINMan == ranking.ManMin && p.Couple.MINWoman == ranking.WomanMin);

                if (couple != null)
                {
                    couple.Star = 1;
                    couple.OriginalPlacings = ranking.Rank;
                    stars++;

                    if (stars == this.NumberOfStars)
                    {
                        MainWindow.MainWindowHandle.CloseDialog();
                        MainWindow.MainWindowHandle.ShowMessage($"Found {stars} Star-Couples");
                        return;
                    }
                }
            }

            MainWindow.MainWindowHandle.CloseDialog();
            MainWindow.MainWindowHandle.ShowMessage($"Found {stars} Star-Couples");
        }

        private void Cancel()
        {
            MainWindow.MainWindowHandle.CloseDialog();
        }
    }
}
