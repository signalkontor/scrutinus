﻿// // TPS.net TPS8 Scrutinus
// // SylabusWarningDialogViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using DataModel.Models;
using GalaSoft.MvvmLight.Command;
using MahApps.Metro.Controls.Dialogs;

namespace Scrutinus.Dialogs.DialogViewModels
{
    internal class SylabusWarningDialogViewModel
    {
        #region Fields

        private readonly ScrutinusContext context;

        private readonly Round round;

        #endregion

        #region Constructors and Destructors

        public SylabusWarningDialogViewModel()
        {
            this.WarnCommand = new RelayCommand(this.Warn);
            this.CloseCommand = new RelayCommand(this.Close);
            this.Participants = new ObservableCollection<Participant>();
        }

        public SylabusWarningDialogViewModel(
            ScrutinusContext context,
            IEnumerable<Participant> participants,
            Participant selectedParticipant,
            Round round)
            : this()
        {
            this.Participants = new ObservableCollection<Participant>(participants);
            this.SelectedParticipant = selectedParticipant;
            this.round = round;
            this.context = context;
        }

        #endregion

        #region Public Properties

        public ICommand CloseCommand { get; set; }

        public ObservableCollection<Participant> Participants { get; set; }

        public Participant SelectedParticipant { get; set; }

        public ICommand WarnCommand { get; set; }

        #endregion

        #region Methods

        private void Close()
        {
            MainWindow.MainWindowHandle.CloseDialog();
        }

        private void DisqualifyCouple()
        {
            var disqualifyViewModel = new DisqualifyCoupleViewModel(
                new ScrutinusContext(),
                this.round.Id,
                this.SelectedParticipant.Id);

            disqualifyViewModel.Disqualify(false, DtvDisqualificationFlag.DisqualificationSylabus);
        }

        private void Warn()
        {
            if (this.SelectedParticipant == null)
            {
                return;
            }

            if (this.SelectedParticipant.Startbuch == null)
            {
                MainWindow.MainWindowHandle.ShowMessage(
                    "Für dieses Paar ist kein Startbuch hinterlegt. Bitte führen Sie eine normale Disqualifikation durch");
                return;
            }

            if (this.SelectedParticipant.Startbuch.Sylabus || this.SelectedParticipant.SylabusWarning)
            {
                var res =
                    MainWindow.MainWindowHandle.ShowMessage(
                        "Das Paar ist bereits verwarnt und wird nun disqualifiziert!",
                        "Warnung",
                        MessageDialogStyle.AffirmativeAndNegative);

                if (res == MessageDialogResult.Negative)
                {
                    return;
                }

                this.DisqualifyCouple();
                return;
            }

            this.SelectedParticipant.Startbuch.Sylabus = true;
            this.SelectedParticipant.SylabusWarning = true;
            this.SelectedParticipant.DisqualificationFlag = DtvDisqualificationFlag.WarningSylabus;

            this.context.SaveChanges();

            MainWindow.MainWindowHandle.CloseDialog();
        }

        #endregion
    }
}