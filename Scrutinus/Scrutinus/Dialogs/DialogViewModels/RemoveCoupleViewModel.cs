﻿// // TPS.net TPS8 Scrutinus
// // RemoveCoupleViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using DataModel;
using DataModel.Models;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using mobileControl.Helper;

namespace Scrutinus.Dialogs.DialogViewModels
{
    public class RemoveCoupleViewModel : ViewModelBase
    {
        #region Fields

        private readonly ScrutinusContext context;

        private readonly Round round;

        #endregion

        #region Constructors and Destructors

        public RemoveCoupleViewModel()
        {
        }

        public RemoveCoupleViewModel(Round round, ScrutinusContext context)
        {
            this.context = context;
            this.round = round;
            this.Qualifieds = new ObservableCollection<Qualified>(round.Qualifieds.ToList());

            this.RemoveCoupleCommand = new RelayCommand(this.RemoveCouple);
            this.CloseCommand = new RelayCommand(this.Close);
        }

        #endregion

        #region Public Properties

        public ICommand CloseCommand { get; set; }

        public ObservableCollection<Qualified> Qualifieds { get; set; }

        public ICommand RemoveCoupleCommand { get; set; }

        public Qualified SelctedCouple { get; set; }

        #endregion

        #region Public Methods and Operators

        public void Close()
        {
            MainWindow.MainWindowHandle.CloseDialog();
        }

        public void RemoveCouple()
        {
            if (this.SelctedCouple == null)
            {
                return;
            }

            // Remove the qualified and set participant to 
            this.SelctedCouple.Participant.State = CoupleState.Missing;

            var heats =
                this.context.Drawings.Where(
                    d => d.Round.Id == this.round.Id && d.Participant.Id == this.SelctedCouple.Participant.Id)
                    .ToList();

            foreach (var drawing in heats)
            {
                this.context.Drawings.Remove(drawing);
            }

            this.round.Qualifieds.Remove(this.SelctedCouple);
            this.context.Qualifieds.Remove(this.SelctedCouple);
            this.context.SaveChanges();

            if (this.round.EjudgeEnabled)
            {
                ScrutinusCommunicationHandler.RemoveCouple(this.round, this.SelctedCouple.Participant.Number.ToString(), 1);
            }

            this.Close();
        }

        #endregion
    }
}