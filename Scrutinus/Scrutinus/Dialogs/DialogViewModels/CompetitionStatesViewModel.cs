﻿// // TPS.net TPS8 Scrutinus
// // CompetitionStatesViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Windows.Input;
using DataModel;
using DataModel.Models;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using General.Extensions;
using MahApps.Metro.Controls;
using MobileControlLib.Helper;

namespace Scrutinus.Dialogs.DialogViewModels
{
    public class CompetitionStatesViewModel : ViewModelBase
    {
        #region Constructors and Destructors

        public CompetitionStatesViewModel()
        {
            this.LockCompetitionCommand = new RelayCommand(() => this.SetState(CompetitionState.Closed));
            this.EnableCheckinCommand = new RelayCommand(() => this.SetState(CompetitionState.CheckIn));
            this.EnableTpsCommand = new RelayCommand(() => this.SetState(CompetitionState.Startlist));
            this.CloseDialogCommand = new RelayCommand(this.CloseDialog);
            this.OpenInExtraWindowCommand = new RelayCommand(this.OpenInExtraWindow);
            this.ShowCompetitionCommand = new RelayCommand<int>(this.ShowCompetition);
            this.ToggleEjudgeEnableCommand = new RelayCommand<int>(this.ToggleEjudgeEnabled);

            this.context = new ScrutinusContext();
            this.FillModel();

            this.refreshThread = new Thread(this.RefreshThread) { IsBackground = true };
            this.refreshThread.Start();
        }

        #endregion

        #region Fields

        private readonly ScrutinusContext context;

        private Competition selectedCompetition;

        private Thread refreshThread;

        private bool refreshing = false;

        #endregion

        #region Public Properties

        public ICommand CloseDialogCommand { get; set; }

        public ObservableCollection<Competition> Competitions { get; set; }

        public ICommand EnableCheckinCommand { get; set; }

        public ICommand EnableTpsCommand { get; set; }

        public ICommand LockCompetitionCommand { get; set; }

        public ICommand EnableEjudgeCommand { get; set; }

        public ICommand OpenInExtraWindowCommand { get; set; }

        public ICommand ShowCompetitionCommand { get; set; }

        public ICommand ToggleEjudgeEnableCommand { get; set; }

        public Action ClosingAction { get; set; }

        public Competition SelectedCompetition
        {
            get
            {
                return this.selectedCompetition;
            }
            set
            {
                this.selectedCompetition = value;
                this.RaisePropertyChanged(() => this.SelectedCompetition);
            }
        }

        #endregion

        #region Methods

        private void CloseDialog()
        {
            if (this.ClosingAction != null)
            {
                this.refreshThread?.Abort();
                this.refreshThread = null;
                this.ClosingAction();
            }
        }

        private void FillModel()
        {
            

            if (this.Competitions == null)
            {
                this.Competitions = new ObservableCollection<Competition>();
            }

            this.Competitions.AddRange(this.context.Competitions.ToList());
        }

        private void SetState(CompetitionState state)
        {
            if (this.SelectedCompetition == null)
            {
                return;
            }

            while (this.refreshing)
            {
                // prevent parallel access to db context
                Thread.Sleep(250);
            }

            this.SelectedCompetition.State = state;
            this.context.SaveChanges();
        }

        private void OpenInExtraWindow()
        {
            var metroWindow = new MetroWindow() {ShowCloseButton = true};

            var competitionStateDialog = new CompetitionStatesControl();
            metroWindow.Content = competitionStateDialog;
            competitionStateDialog.CompetitionStatesViewModel.ClosingAction = metroWindow.Close;
            metroWindow.Closing += (sender, args) =>
            {
                var childWindow = sender as MetroWindow;
                if (childWindow != null)
                {
                    var stateDlg = childWindow.Content as CompetitionStatesControl;
                    if (stateDlg != null)
                    {
                        stateDlg.CompetitionStatesViewModel.refreshThread?.Abort();
                        stateDlg.CompetitionStatesViewModel.refreshThread = null;
                    }
                }
            };

            metroWindow.Show();

            this.refreshThread?.Abort();

            this.refreshThread = null;

            this.CloseDialog();
        }

        private void ToggleEjudgeEnabled(int competitionId)
        {
            var competition = this.context.Competitions.Single(c => c.Id == competitionId);

            if (competition.CurrentRound == null)
            {
                return;
            }

            if (!competition.CurrentRound.EjudgeEnabled)
            {
                DeviceHelper.CreateEjudgeData(this.context, competition.CurrentRound,
                    MainWindow.MainWindowHandle.ViewModel.Devices);
            }

            competition.CurrentRound.EjudgeEnabled = !competition.CurrentRound.EjudgeEnabled;
        }

        private void ShowCompetition(int Id)
        {
            Debug.WriteLine(Id);
            var element = MainWindow.MainWindowHandle.ViewModel.Competitions.Single(e => e.Competition.Id == Id);
            MainWindow.MainWindowHandle.ViewModel.SelectedCompetition = null;
            MainWindow.MainWindowHandle.ViewModel.SelectedCompetition = element;
        }

        private void RefreshThread()
        {
            while (true)
            {
                try
                {
                    Thread.Sleep(3000);
                    this.refreshing = true;
                    this.context.RefreshAll();
                    this.refreshing = false;
                }
                catch (ThreadAbortException)
                {
                    this.refreshing = false;
                    return; // Exit thread
                }
                catch (Exception)
                {
                    this.refreshing = false;
                    continue;
                }
            }
        }

        #endregion
    }
}