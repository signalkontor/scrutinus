﻿// // TPS.net TPS8 Scrutinus
// // DeleteRoundViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using DataModel;
using DataModel.Messages;
using DataModel.Models;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;

namespace Scrutinus.Dialogs.DialogViewModels
{
    public class DeleteRoundViewModel : ViewModelBase
    {
        #region Constructors and Destructors

        public DeleteRoundViewModel()
        {
            this.dataContext = new ScrutinusContext();
            this.Competitions = new ObservableCollection<Competition>(this.dataContext.Competitions);

            this.DeleteCommand = new RelayCommand(this.Delete);
            this.CloseCommand = new RelayCommand(this.Close);
        }

        #endregion

        #region Fields

        private readonly ScrutinusContext dataContext;

        private Competition selectedCompetition;

        #endregion

        #region Public Properties

        public ICommand CloseCommand { get; set; }

        public ObservableCollection<Competition> Competitions { get; set; }

        public ICommand DeleteCommand { get; set; }

        public string RoundTitleToDelete
        {
            get
            {
                if (this.selectedCompetition == null || this.selectedCompetition.Rounds.Count == 0)
                {
                    return null;
                }
                var number = this.selectedCompetition.Rounds.Max(n => n.Number);
                var round = this.selectedCompetition.Rounds.Single(r => r.Number == number);
                return round.Name;
            }
        }

        public Competition SelectedCompetition
        {
            get
            {
                return this.selectedCompetition;
            }
            set
            {
                this.selectedCompetition = value;
                this.RaisePropertyChanged(() => this.SelectedCompetition);
                this.RaisePropertyChanged(() => this.RoundTitleToDelete);
            }
        }

        #endregion

        #region Methods

        private void Close()
        {
            MainWindow.MainWindowHandle.CloseDialog();
        }

        private void Delete()
        {
            if (this.selectedCompetition == null || this.selectedCompetition.Rounds.Count == 0)
            {
                return;
            }

            var number = this.selectedCompetition.Rounds.Max(n => n.Number);
            var round = this.selectedCompetition.Rounds.Single(r => r.Number == number);
            var competition = this.dataContext.Competitions.Single(c => c.Id == round.Competition.Id);
            Round previousRound = null;
            if (round.Number > 1)
            {
                previousRound = this.selectedCompetition.Rounds.Single(r => r.Number == number - 1);
            }

            // Delete all markings:
            foreach (var marking in round.Markings.ToList())
            {
                this.dataContext.Markings.Remove(marking);
            }

            var eJudgeDatas = this.dataContext.EjudgeDatas.Where(e => e.Round.Id == round.Id);
            this.dataContext.EjudgeDatas.RemoveRange(eJudgeDatas);

            // delete drawings:
            foreach (var drawing in round.Drawings.ToList())
            {
                this.dataContext.Drawings.Remove(drawing);
            }
            var participants =
                this.dataContext.Participants.Where(p => p.QualifiedRound.Id == round.Id).ToList();
            foreach (var participant in participants)
            {
                participant.QualifiedRound = previousRound;
            }

            // delete qualifieds:
            foreach (var qualified in round.Qualifieds.ToList())
            {
                qualified.Participant.QualifiedRound = previousRound;
                this.dataContext.Qualifieds.Remove(qualified);
            }

            competition.CurrentRound = previousRound;

            this.dataContext.SaveChanges();

            if (previousRound != null)
            {
                competition.State = CompetitionState.RoundDrawing;
                previousRound.State = CompetitionState.RoundDrawing;
            }
            else
            {
                competition.State = CompetitionState.Startlist;
            }

            // delete round itself:
            this.dataContext.Rounds.Remove(round);

            this.dataContext.SaveChanges();

            Messenger.Default.Send(new InvalidateNavigationMessage { Competition = competition });

            this.Close();
        }

        #endregion
    }
}