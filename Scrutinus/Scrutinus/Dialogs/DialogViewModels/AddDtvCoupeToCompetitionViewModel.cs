﻿// // TPS.net TPS8 Scrutinus
// // AddDtvCoupeToCompetitionViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using DataModel.Models;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace Scrutinus.Dialogs.DialogViewModels
{
    public class AddDtvCoupleToCompetitionViewModel : ViewModelBase
    {
        #region Fields

        private readonly Competition competition;

        private readonly ScrutinusContext context;

        private string club;

        private bool coupleIsValid;

        private string dtvIdMan;

        private string dtvIdWoman;

        private string errorMessage;

        private string firstNameMan;

        private string firstNameWoman;

        private bool idManValid;

        private bool idWomanValid;

        private string lastNameMan;

        private string lastNameWoman;

        private string ltv;

        #endregion

        #region Constructors and Destructors

        public AddDtvCoupleToCompetitionViewModel()
        {
        }

        public AddDtvCoupleToCompetitionViewModel(ScrutinusContext context, Competition competition)
        {
            this.context = context;
            this.competition = competition;
            this.LTVs = context.ClimbUpTables.Select(c => c.LTV).Distinct().ToList();
            this.SaveCommand = new RelayCommand(this.Save);
            this.CancelCommand = new RelayCommand(this.Cancel);
        }

        #endregion

        #region Public Properties

        public ICommand CancelCommand { get; set; }

        public string Club
        {
            get
            {
                return this.club;
            }
            set
            {
                this.club = value;
                this.RaisePropertyChanged(() => this.Club);
            }
        }

        public bool CoupleIsValid
        {
            get
            {
                return this.coupleIsValid;
            }
            set
            {
                this.coupleIsValid = value;
                this.RaisePropertyChanged(() => this.CoupleIsValid);
            }
        }

        public string DtvIdMan
        {
            get
            {
                return this.dtvIdMan;
            }
            set
            {
                this.dtvIdMan = value;

                var entry = this.context.SimpleStartbucher.FirstOrDefault(s => s.IdMan == this.dtvIdMan);
                if (entry != null)
                {
                    this.dtvIdWoman = entry.IdWoman;
                    this.Club = entry.Club;
                    this.LTV = entry.LTV;

                    this.RaisePropertyChanged(() => this.DtvIdWoman);
                    this.IdManValid = true;
                    this.IdWomanValid = true;
                }

                this.RaisePropertyChanged(() => this.DtvIdMan);
                this.CheckCoupleIsValid();
            }
        }

        public string DtvIdWoman
        {
            get
            {
                return this.dtvIdWoman;
            }
            set
            {
                this.dtvIdWoman = value;

                var entry = this.context.SimpleStartbucher.FirstOrDefault(s => s.IdWoman == this.dtvIdWoman);
                if (entry != null)
                {
                    this.dtvIdMan = entry.IdMan;
                    this.Club = entry.Club;
                    this.LTV = entry.LTV;
                    this.IdManValid = true;
                    this.IdWomanValid = true;
                    this.RaisePropertyChanged(() => this.DtvIdMan);
                }

                this.RaisePropertyChanged(() => this.DtvIdWoman);

                this.CheckCoupleIsValid();
            }
        }

        public string ErrorMessage
        {
            get
            {
                return this.errorMessage;
            }
            set
            {
                this.errorMessage = value;
                this.RaisePropertyChanged(() => this.ErrorMessage);
            }
        }

        public string FirstNameMan
        {
            get
            {
                return this.firstNameMan;
            }
            set
            {
                this.firstNameMan = value;
                this.RaisePropertyChanged(() => this.FirstNameMan);
            }
        }

        public string FirstNameWoman
        {
            get
            {
                return this.firstNameWoman;
            }
            set
            {
                this.firstNameWoman = value;
                this.RaisePropertyChanged(() => this.FirstNameWoman);
            }
        }

        public bool IdManValid
        {
            get
            {
                return this.idManValid;
            }
            set
            {
                this.idManValid = value;
                this.RaisePropertyChanged(() => this.IdManValid);
            }
        }

        public bool IdWomanValid
        {
            get
            {
                return this.idWomanValid;
            }
            set
            {
                this.idWomanValid = value;
                this.RaisePropertyChanged(() => this.IdWomanValid);
            }
        }

        public string LTV
        {
            get
            {
                return this.ltv;
            }
            set
            {
                this.ltv = value;
                this.RaisePropertyChanged(() => this.LTV);
            }
        }

        public List<string> LTVs { get; set; }

        public string LastNameMan
        {
            get
            {
                return this.lastNameMan;
            }
            set
            {
                this.lastNameMan = value;
                this.RaisePropertyChanged(() => this.LastNameMan);
            }
        }

        public string LastNameWoman
        {
            get
            {
                return this.lastNameWoman;
            }
            set
            {
                this.lastNameWoman = value;
                this.RaisePropertyChanged(() => this.LastNameWoman);
            }
        }

        public ICommand SaveCommand { get; set; }

        #endregion

        #region Methods

        private void Cancel()
        {
            MainWindow.MainWindowHandle.CloseDialog();
        }

        private bool CheckCoupleIsValid()
        {
            // Check the couple has a correct startbook for this competition:
            var startbuches =
                this.context.SimpleStartbucher.FirstOrDefault(
                    s =>
                    s.IdMan == this.DtvIdMan && s.IdWoman == this.DtvIdWoman
                    && s.Section.Id == this.competition.Section.Id);

            if (startbuches == null)
            {
                this.CoupleIsValid = false;
                this.ErrorMessage = "Das Paar hat kein Startbuch für Turnierart " + this.competition.Section.SectionName;
                return false;
            }

            // We check, if we can find the couple already:
            var couple =
                this.context.Couples.FirstOrDefault(c => c.MINMan == this.DtvIdMan && c.MINWoman == this.DtvIdWoman);
            if (couple != null)
            {
                this.FirstNameMan = couple.FirstMan;
                this.LastNameMan = couple.LastMan;
                this.FirstNameWoman = couple.FirstWoman;
                this.LastNameWoman = couple.LastWoman;
                this.Club = couple.Country;
                this.LTV = couple.Region;

                var participant =
                    this.context.Participants.FirstOrDefault(
                        p => p.Couple.Id == couple.Id && p.Competition.Id == this.competition.Id);

                if (participant != null)
                {
                    this.CoupleIsValid = false;
                    this.ErrorMessage = "Das Paar ist bereits mit Nummer " + participant.Number
                                        + " in diesem Turnier gemeldet";
                    return false;
                }
            }

            if (string.IsNullOrWhiteSpace(this.FirstNameMan) || string.IsNullOrWhiteSpace(this.LastNameMan)
                || string.IsNullOrWhiteSpace(this.FirstNameWoman) || string.IsNullOrWhiteSpace(this.LastNameWoman))
            {
                this.CoupleIsValid = false;
                this.ErrorMessage = "Bitte geben Sie jeweils Vor- und Zuname von Parter und Partnerin an.";
                return false;
            }

            if (string.IsNullOrWhiteSpace(this.Club))
            {
                this.CoupleIsValid = false;
                this.ErrorMessage = "Bitte geben Sie den Club an";
                return false;
            }

            if (string.IsNullOrWhiteSpace(this.LTV))
            {
                this.CoupleIsValid = false;
                this.ErrorMessage = "Bitte geben Sie den LTV an";
                return false;
            }

            return true;
        }

        private void Save()
        {
            if (!this.CheckCoupleIsValid())
            {
                return;
            }

            var couple =
                this.context.Couples.FirstOrDefault(c => c.MINMan == this.DtvIdMan && c.MINWoman == this.DtvIdWoman);

            if (couple == null)
            {
                couple = new Couple
                             {
                                 MINMan = this.DtvIdMan,
                                 MINWoman = this.DtvIdWoman,
                                 FirstMan = this.FirstNameMan,
                                 LastMan = this.LastNameMan,
                                 FirstWoman = this.FirstNameWoman,
                                 LastWoman = this.LastNameWoman,
                                 Region = this.LTV,
                                 Country = this.Club,
                             };
            }

            var startbuch = couple.Startbooks.FirstOrDefault(s => s.Section.Id == this.competition.Section.Id);

            if (startbuch == null)
            {
                startbuch = new Startbuch { Couple = couple };
                startbuch.Section = this.competition.Section;
                couple.Startbooks.Add(startbuch);
                this.context.Startbuecher.Add(startbuch);
            }

            var template =
                this.context.SimpleStartbucher.First(s => s.IdMan == this.DtvIdMan && s.IdWoman == this.DtvIdWoman);

            startbuch.AgeGroup = template.AgeGroup;
            startbuch.Class = template.Class;
            startbuch.Blocked = template.Blocked;
            startbuch.BlockedDate = template.BlockedDate;
            startbuch.IsSelfGenerated = false;
            startbuch.MinimumPoints = template.MinimumPoints;
            startbuch.TargetPlacings = template.TargetPlacings;
            startbuch.TargetPoints = template.TargetPoints;
            startbuch.PlacingsUpto = template.PlacingsUpto;
            startbuch.OriginalPlacings = template.OriginalPlacings;
            startbuch.OriginalPoints = template.OriginalPoints;
            startbuch.NextClass = template.NextClass;
            startbuch.Sylabus = template.Sylabus;
            startbuch.PrintLaufzettel = template.PrintLaufzettel;

            var participant = new Participant
                                  {
                                      Number = 0,
                                      AgeGroup = startbuch.AgeGroup,
                                      Class = startbuch.Class,
                                      Competition = this.competition,
                                      Couple = couple,
                                      OriginalPlacings = startbuch.OriginalPlacings,
                                      OriginalPoints = startbuch.OriginalPoints,
                                      TargetClass = startbuch.NextClass,
                                      TargetPlacings = startbuch.TargetPlacings,
                                      TargetPoints = startbuch.TargetPoints,
                                      MinimumPoints = startbuch.MinimumPoints,
                                      PlacingsUpto = startbuch.PlacingsUpto,
                                      ExternalId = this.competition.ExternalId + "_" + template.StarterId,
                                      RegistrationFlag = RegistrationFlag.Late,
                                  };

            couple.Participants.Add(participant);
            this.context.Participants.Add(participant);

            this.context.SaveChanges();

            MainWindow.MainWindowHandle.CloseDialog();
        }

        #endregion
    }
}