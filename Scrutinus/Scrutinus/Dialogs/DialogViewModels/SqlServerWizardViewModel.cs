﻿// // TPS.net TPS8 Scrutinus
// // SqlServerWizardViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data.SqlServerCe;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using DataModel;
using DataModel.Models;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using General.Interfaces;
using MahApps.Metro.Controls.Dialogs;
using Microsoft.Win32;
using Scrutinus.Commands;
using Scrutinus.Localization;

namespace Scrutinus.Dialogs.DialogViewModels
{
    public class SqlServerWizardViewModel : ViewModelBase, ISaveSettingsProvider
    {
        #region Constructors and Destructors

        public SqlServerWizardViewModel()
        {
            this.dispatcher = Dispatcher.CurrentDispatcher;

            this.UserName = Settings.Default.SqlServerUser;
            this.PassWord = Settings.Default.SqlServerPassword;
            this.SelectedServer = Settings.Default.SqlServerName;

            // Start discovering the available SQL Servers
            this.AvailableServers = new ObservableCollection<string>();
            this.AvailableDatabases = new ObservableCollection<string>();
            this.GetListOfSqlServers();

            this.ConnectCommand = new SaveSettingsCommand(this.Connect, this);
            this.CloseCommand = new RelayCommand(this.Close);
            this.CopyDatabaseCommand = new SaveSettingsCommand<string>(this.CopyDataBase, this);
            this.CreateNewDatabaseCommand = new SaveSettingsCommand(this.CreateNewDatabase, this);
            this.OpenDatabaseCommand = new SaveSettingsCommand(this.OpenDatabase, this);
            this.SetCouplesMissingCommand = new SaveSettingsCommand(this.SetCouplesMissing, this);
        }

        #endregion

        #region Public Methods and Operators

        public static IList<string> GetLocalSqlServerInstanceNames()
        {
            var registryValueDataReader = new RegistryValueDataReader();

            var instances64Bit = registryValueDataReader.ReadRegistryValueData(
                RegistryHive.Wow64,
                Registry.LocalMachine,
                @"SOFTWARE\Microsoft\Microsoft SQL Server",
                "InstalledInstances");

            var instances32Bit = registryValueDataReader.ReadRegistryValueData(
                RegistryHive.Wow6432,
                Registry.LocalMachine,
                @"SOFTWARE\Microsoft\Microsoft SQL Server",
                "InstalledInstances");

            IList<string> localInstanceNames = instances32Bit.Select(s => ".\\" + s).ToList();

            foreach (var s in instances64Bit)
            {
                localInstanceNames.Add(".\\" + s);
            }

            return localInstanceNames;
        }

        #endregion

        #region Fields

        private string passWord;

        private string selectedServer;

        private string selectedDatabase;

        private readonly string sqlServerConnectionString =
            @"Data Source={0};Initial Catalog={3};Persist Security Info=True;User ID={1};Password={2};MultipleActiveResultSets=True";

        private string userName;

        private string newDatabaseNameSdf;

        private string newDatabaseNameServer;

        private readonly Dispatcher dispatcher;

        #endregion

        #region Public Properties

        public ObservableCollection<string> AvailableDatabases { get; set; }

        public ObservableCollection<string> AvailableServers { get; set; }

        public ICommand CloseCommand { get; set; }

        public ICommand ConnectCommand { get; set; }

        public ICommand CopyDatabaseCommand { get; set; }

        public ICommand CreateNewDatabaseCommand { get; set; }

        public ICommand SetCouplesMissingCommand { get; set; }

        public ObservableCollection<string> Databases { get; set; }

        public string NewDatabaseNameSdf
        {
            get { return this.newDatabaseNameSdf; }
            set
            {
                this.newDatabaseNameSdf = value;
                this.RaisePropertyChanged(() => this.NewDatabaseNameSdf);
            }
        }

        public string NewDatabaseNameServer
        {
            get { return this.newDatabaseNameServer; }
            set
            {
                this.newDatabaseNameServer = value;
                this.RaisePropertyChanged(() => this.NewDatabaseNameServer);
            }
        }

        public ICommand OpenDatabaseCommand { get; set; }

        public string PassWord
        {
            get
            {
                return this.passWord;
            }
            set
            {
                this.passWord = value;
                this.RaisePropertyChanged(() => this.PassWord);
            }
        }


        public KeyValuePair<string, string>? SelectedRule { get; set; }

        public string SelectedServer
        {
            get
            {
                return this.selectedServer;
            }
            set
            {
                this.selectedServer = value;
                this.RaisePropertyChanged(() => this.SelectedServer);
            }
        }

        public string UserName
        {
            get
            {
                return this.userName;
            }
            set
            {
                this.userName = value;
                this.RaisePropertyChanged(() => this.UserName);
            }
        }

        public string SelectedDatabase
        {
            get { return this.selectedDatabase; }
            set
            {
                this.selectedDatabase = value;
                this.RaisePropertyChanged(() => this.SelectedDatabase);
            }
        }

        #endregion

        #region Methods

        public static void CopyTable(
            string tableName,
            DbConnection sourceConnection,
            DbConnection destinationConnection,
            bool tableHasIdentity)
        {
            Debug.WriteLine("Copy Table " + tableName);

            using (var transaction = destinationConnection.BeginTransaction())
            {
                string sql;
                DbCommand command;

                if (tableHasIdentity)
                {
                    sql = string.Format("SET IDENTITY_INSERT [{0}] ON", tableName);
                    ExecuteNoQuery(destinationConnection, sql, transaction);
                }

                var sourceCommand = sourceConnection.CreateCommand();
                sourceCommand.CommandText = "select * from " + tableName;
                var reader = sourceCommand.ExecuteReader();

                var sqlInsert = CreateSqlInsert(tableName, reader);

                while (sqlInsert != null)
                {
                    ExecuteNoQuery(destinationConnection, sqlInsert, transaction);

                    sqlInsert = CreateSqlInsert(tableName, reader);
                }

                reader.Close();

                if (tableHasIdentity)
                {
                    sql = string.Format("SET IDENTITY_INSERT [{0}] OFF", tableName);
                    ExecuteNoQuery(destinationConnection, sql, transaction);

                    transaction.Commit();

                    if (destinationConnection is SqlCeConnection)
                    {
                        // for CE we have to setup the seed again:
                        sql = string.Format("Select MAX(ID) FROM {0}", tableName);
                        command = destinationConnection.CreateCommand();
                        command.CommandText = sql;
                        command.Transaction = null;
                        var result = command.ExecuteReader();
                        var max = 1;
                        if (result.Read() && !result.IsDBNull(0))
                        {
                            max = result.GetInt32(0) + 1;
                        }
                        
                        result.Close();
                        // now reset the seed...
                        sql = string.Format("ALTER TABLE {0} Alter Column Id IDENTITY({1}, 1)", tableName, max + 1);
                        ExecuteNoQuery(destinationConnection, sql, null);
                    }
                }
                else
                {
                    transaction.Commit();
                }
                
            }
        }

        public void SaveSettings()
        {
            Settings.Default.SqlServerName = this.SelectedServer;
            Settings.Default.SqlServerUser = this.UserName;
            Settings.Default.SqlServerPassword = this.PassWord;

            Settings.Default.Save();
        }

        private static void ExecuteNoQuery(DbConnection destinationConnection, string sql, DbTransaction transaction)
        {
            DbCommand command;
            command = destinationConnection.CreateCommand();
            command.CommandText = sql;
            command.Transaction = transaction;
            command.ExecuteNonQuery();
        }

        private static string CreateSqlInsert(string tableName, DbDataReader reader)
        {
            if (!reader.Read())
            {
                return null;
            }

            var names = new List<string>();
            var values = new List<string>();

            for (var i = 0; i < reader.FieldCount; i++)
            {
                if (reader.GetName(i) == "CurrentRound_Id")
                {
                    // We cannot set CurrentRound_Id, because we habe not yet importet rounds
                    // and we cannot import rounds before we importet competitions
                    continue;
                }

                names.Add("[" + reader.GetName(i) + "]");

                if (reader.IsDBNull(i))
                {
                    values.Add("NULL");
                }
                else
                {
                    var value = reader.GetValue(i);
                    if (value is bool)
                    {
                        var b = (bool)value;
                        values.Add(b ? "1" : "0");
                        continue;
                    }
                    if (value is DateTime)
                    {
                        var d = (DateTime)value;
                        values.Add(string.Format("convert(datetime, '{0}', 20)", d.ToString("yyyy-MM-dd HH:mm:ss")));
                        continue;
                    }
                    if (value is byte[])
                    {
                        values.Add("0x" + BitConverter.ToString((byte[])value).Replace("-", ""));
                        continue;
                    }
                    if (value is decimal || value is double)
                    {
                        var str = value.ToString();
                        values.Add(str.Replace(",", "."));
                        continue;
                    }
                    // Default. String or number
                    values.Add(
                        value is string
                            ? "N'" + reader.GetValue(i).ToString().Replace("'", "''") + "'"
                            : reader.GetValue(i).ToString());
                }
            }

            var sql = string.Format(
                "Insert into [{2}] ({0}) Values ({1})",
                string.Join(",", names),
                string.Join(",", values),
                tableName);

            return sql;
        }

        private static void UpdateCurrentRoundInCompetition(
            DbConnection sourceConnection,
            DbConnection destinationConnection)
        {
            var sql = "Select Id, CurrentRound_Id from Competitions";
            var command = sourceConnection.CreateCommand();
            command.CommandText = sql;
            var sourceReader = command.ExecuteReader();

            while (sourceReader.Read())
            {
                if (sourceReader.IsDBNull(1))
                {
                    continue;
                }

                var updateSql = string.Format(
                    "Update Competitions Set CurrentRound_Id={1} WHERE Id={0}",
                    sourceReader.GetInt32(0),
                    sourceReader.GetInt32(1));

                var updateCommand = destinationConnection.CreateCommand();
                updateCommand.CommandText = updateSql;
                updateCommand.ExecuteNonQuery();
            }

            sourceReader.Close();
        }

        private void Close()
        {
            MainWindow.MainWindowHandle.CloseDialog();
        }

        private void Connect()
        {
            this.FillDatabases();

            if (this.AvailableDatabases.Any())
            {
                this.SelectedDatabase = this.AvailableDatabases.First();
            }
        }

        private void CopyDataBase(string destination)
        {
            var newDatabaseName = destination == "SDF" ? this.NewDatabaseNameSdf : this.NewDatabaseNameServer;

            if (string.IsNullOrEmpty(newDatabaseName))
            {
                MainWindow.MainWindowHandle.ShowMessage("You must set the name of the new database");
                return;
            }

            if (string.IsNullOrEmpty(this.UserName) || string.IsNullOrEmpty(this.PassWord))
            {
                MainWindow.MainWindowHandle.ShowMessage("You must set User name and Password!");
                return;
            }

            var progress = MainWindow.MainWindowHandle.ShowProgressAsync(
                "Database Copy",
                "Copying tables to the new database");

            if (destination == "SDF")
            {
                if (!newDatabaseName.ToLower().EndsWith(".sdf"))
                {
                    this.NewDatabaseNameSdf += ".sdf";
                    newDatabaseName = this.NewDatabaseNameSdf;
                }
            }
            else
            {
                if (newDatabaseName.ToLower().EndsWith(".sdf"))
                {
                    this.NewDatabaseNameServer = this.NewDatabaseNameServer.Substring(0, this.NewDatabaseNameServer.Length - 4);
                    newDatabaseName = this.NewDatabaseNameServer;
                }
            }

            Task.Factory.StartNew(
                new Action(
                    () =>
                        {
                            MainWindow.MainWindowHandle.ViewModel.DoNotUpdateNavigationList = true;
                            Thread.Sleep(2000); // just to be sure the update thread is not disturbing us

                            DbConnection destinationConnection;
                            ScrutinusContext context;
                            ScrutinusContext.DoNotInitializeDatabase = true;

                            if (newDatabaseName.ToLower().EndsWith(".sdf"))
                            {
                                destinationConnection = new SqlCeConnection("Data Source=" + newDatabaseName);
                                context = new ScrutinusContext(newDatabaseName, null);
                            }
                            else
                            {
                                destinationConnection = new SqlConnection(this.GetSqlServerConnectionString(newDatabaseName));
                                context = new ScrutinusContext(destinationConnection.ConnectionString, null);
                            }

                            progress.Result.SetMessage("Creating the new database");

                            var classes = context.Classes.ToList();
                            if (classes.Count > 0)
                            {
                                progress.Result.CloseAsync().ContinueWith(new Action<Task>((t) =>
                                    {
                                        MainWindow.MainWindowHandle.ShowMessage("Database not empty!");
                                    }));
                                
                                MainWindow.MainWindowHandle.ViewModel.DoNotUpdateNavigationList = false;
                                return;
                            }

                            ScrutinusContext.DoNotInitializeDatabase = false;

                            progress.Result.SetMessage("Preparing the connection");

                            destinationConnection.Open();

                            DbConnection sourceConnection;
                            if (!Settings.Default.DataSource.StartsWith("Data Source="))
                            {
                                sourceConnection = new SqlCeConnection("Data Source=" + Settings.Default.DataSource);
                            }
                            else
                            {
                                sourceConnection = new SqlConnection(Settings.Default.DataSource);
                            }

                            sourceConnection.Open();
                            CopyTable("AgeGroups", sourceConnection, destinationConnection, false);
                            progress.Result.SetMessage("Classes");
                            progress.Result.SetProgress(0.03);
                            CopyTable("Classes", sourceConnection, destinationConnection, false);

                            progress.Result.SetMessage("Sections");
                            progress.Result.SetProgress(0.06);
                            CopyTable("Sections", sourceConnection, destinationConnection, false);

                            progress.Result.SetMessage("ClimbUps");
                            progress.Result.SetProgress(0.09);
                            CopyTable("ClimbUpTables", sourceConnection, destinationConnection, true);

                            progress.Result.SetMessage("Round Types");
                            progress.Result.SetProgress(0.12);
                            CopyTable("RoundType", sourceConnection, destinationConnection, false);

                            progress.Result.SetMessage("Rules");
                            progress.Result.SetProgress(0.15);
                            CopyTable("CompetitionRule", sourceConnection, destinationConnection, false);

                            progress.Result.SetMessage("Competition Types");
                            progress.Result.SetProgress(0.18);
                            CopyTable("CompetitionType", sourceConnection, destinationConnection, false);

                            progress.Result.SetMessage("Events");
                            progress.Result.SetProgress(0.21);
                            CopyTable("Events", sourceConnection, destinationConnection, true);

                            progress.Result.SetMessage("Dances");
                            progress.Result.SetProgress(0.24);
                            CopyTable("Dance", sourceConnection, destinationConnection, false);


                            progress.Result.SetMessage("Templates");
                            progress.Result.SetProgress(0.27);
                            CopyTable("DanceTemplate", sourceConnection, destinationConnection, true);

                            progress.Result.SetMessage("Draw Types");
                            progress.Result.SetProgress(0.30);
                            CopyTable("DrawingType", sourceConnection, destinationConnection, false);

                            progress.Result.SetMessage("Judging Components");
                            progress.Result.SetProgress(0.33);
                            CopyTable("JudgingComponents", sourceConnection, destinationConnection, false);

                            progress.Result.SetMessage("Judging Components per Competition");
                            progress.Result.SetProgress(0.36);
                            CopyTable("JudgingComponentCompetitionTypes", sourceConnection, destinationConnection, false);

                            progress.Result.SetMessage("Clubs");
                            progress.Result.SetProgress(0.39);
                            CopyTable("Clubs", sourceConnection, destinationConnection, true);

                            progress.Result.SetMessage("Countries");
                            progress.Result.SetProgress(0.48);
                            CopyTable("CountryCodes", sourceConnection, destinationConnection, true);

                            progress.Result.SetMessage("Officials");
                            progress.Result.SetProgress(0.42);
                            CopyTable("Official", sourceConnection, destinationConnection, true);

                            progress.Result.SetMessage("Parameters");
                            progress.Result.SetProgress(0.45);
                            CopyTable("Parameters", sourceConnection, destinationConnection, true);

                            progress.Result.SetMessage("Roles");
                            progress.Result.SetProgress(0.51);
                            CopyTable("Role", sourceConnection, destinationConnection, false);

                            progress.Result.SetMessage("Roles of Officials");
                            progress.Result.SetProgress(0.57);
                            CopyTable("RoleOfficials", sourceConnection, destinationConnection, false);

                            progress.Result.SetMessage("Couples");
                            progress.Result.SetProgress(0.65);
                            CopyTable("Couple", sourceConnection, destinationConnection, true);

                            progress.Result.SetMessage("Competitions");
                            progress.Result.SetProgress(0.70);
                            CopyTable("Competitions", sourceConnection, destinationConnection, true);

                            progress.Result.SetMessage("Dances per Competition");
                            progress.Result.SetProgress(0.75);
                            CopyTable("DancesInCompetitions", sourceConnection, destinationConnection, true);

                            progress.Result.SetMessage("Judging Sheet data");
                            progress.Result.SetProgress(0.78);
                            CopyTable("JudgingSheetDatas", sourceConnection, destinationConnection, true);

                            progress.Result.SetMessage("Rounds");
                            progress.Result.SetProgress(0.81);
                            CopyTable("Rounds", sourceConnection, destinationConnection, true);

                            progress.Result.SetMessage("Dances in Rounds");
                            progress.Result.SetProgress(0.84);
                            CopyTable("DanceInRounds", sourceConnection, destinationConnection, true);

                            progress.Result.SetMessage("Participants");
                            progress.Result.SetProgress(0.87);
                            CopyTable("Participants", sourceConnection, destinationConnection, true);

                            progress.Result.SetMessage("Drawings");
                            progress.Result.SetProgress(0.88);
                            CopyTable("Drawings", sourceConnection, destinationConnection, true);

                            progress.Result.SetMessage("Climb ups");
                            progress.Result.SetProgress(0.90);
                            CopyTable("ClimbUps", sourceConnection, destinationConnection, true);

                            progress.Result.SetMessage("Officials in Competitions");
                            progress.Result.SetProgress(0.92);
                            CopyTable("OfficialInCompetitions", sourceConnection, destinationConnection, true);

                            progress.Result.SetMessage("Qualifieds");
                            progress.Result.SetProgress(0.94);
                            CopyTable("Qualifieds", sourceConnection, destinationConnection, true);

                            progress.Result.SetMessage("DTV Licenses");
                            progress.Result.SetProgress(0.95);
                            CopyTable("DtvLicenseHolders", sourceConnection, destinationConnection, true);

                            progress.Result.SetMessage("Startbücher");
                            progress.Result.SetProgress(0.96);
                            CopyTable("Startbuches", sourceConnection, destinationConnection, true);

                            progress.Result.SetMessage("Section Mappings");
                            progress.Result.SetProgress(0.97);
                            CopyTable("SectionAgeGroupClassesMappings", sourceConnection, destinationConnection, true);

                            progress.Result.SetMessage("Section per Age Groups Mappings");
                            progress.Result.SetProgress(0.98);
                            CopyTable("SectionAgeGroupClassesMappingClasses", sourceConnection, destinationConnection, false);

                            progress.Result.SetMessage("Markings");
                            progress.Result.SetProgress(0.99);
                            CopyTable("Markings", sourceConnection, destinationConnection, true);

                            progress.Result.SetMessage("Printers");
                            progress.Result.SetProgress(0.99);
                            CopyTable("Printers", sourceConnection, destinationConnection, true);

                            progress.Result.SetMessage("PrintReportDefinitions");
                            progress.Result.SetProgress(0.99);
                            CopyTable("PrintReportDefinitions", sourceConnection, destinationConnection, true);

                            progress.Result.CloseAsync();
                            // ToDo: CurrentRound_ID in Competitions muss anschließend neu gesetzt werden
                            UpdateCurrentRoundInCompetition(sourceConnection, destinationConnection);

                            MainWindow.MainWindowHandle.ViewModel.DoNotUpdateNavigationList = false;

                        })).ContinueWith((t) =>
                        {
                            if (t.IsFaulted)
                            {
                                progress.Result.CloseAsync().Wait();
                                MainWindow.MainWindowHandle.ShowMessage("Error copy database: " +
                                                                        string.Join(", ",
                                                                            t.Exception.InnerExceptions.Select(
                                                                                e => e.Message)));
                                MainWindow.MainWindowHandle.ShowMessage(t.Exception.StackTrace);
                            }
                            else
                            {
                                if (destination != "SDF")
                                {
                                    this.dispatcher.Invoke(() =>
                                    {
                                        this.AvailableDatabases.Add(newDatabaseName);
                                        this.SelectedDatabase = newDatabaseName;
                                        this.OpenDatabase();
                                    });
                                }
                            }
                        });
        }

        private void CreateNewDatabase()
        {
            if (string.IsNullOrEmpty(this.NewDatabaseNameServer) || this.SelectedRule == null)
            {
                MainWindow.MainWindowHandle.ShowMessage("You must set the name of the new database and select a rule");
                return;
            }

            if (string.IsNullOrEmpty(this.UserName) || string.IsNullOrEmpty(this.PassWord))
            {
                MainWindow.MainWindowHandle.ShowMessage("You must set User name and Password!");
                return;
            }

            Settings.Default.DataSource = this.GetSqlServerConnectionString(this.NewDatabaseNameServer);
            MainWindow.MainWindowHandle.OpenEvent(Settings.Default.DataSource, true, this.SelectedRule.Value.Key);

            // Update Competition Rule in Event Data:
            var context = new ScrutinusContext(Settings.Default.DataSource, null);
            var eventObject = context.Events.FirstOrDefault();
            if (eventObject != null)
            {
                eventObject.RuleName = this.SelectedRule.Value.Value;
                context.SaveChanges();
                MainWindow.MainWindowHandle.ViewModel.Event.RuleName = this.SelectedRule.Value.Value;
            }
            else
            {
                eventObject = new Event();
                eventObject.RuleName = this.SelectedRule.Value.Value;
                context.Events.Add(eventObject);
                context.SaveChanges();
            }

            MainWindow.MainWindowHandle.CloseDialog();
        }

        private void FillDatabases()
        {
            if (this.selectedServer != null && !string.IsNullOrEmpty(this.userName)
                && !string.IsNullOrEmpty(this.passWord))
            {
                var connectionString = this.GetSqlServerConnectionString("master");

                try
                {
                    this.AvailableDatabases.Clear();

                    var connection = new SqlConnection(connectionString);
                    connection.Open();
                    var command = connection.CreateCommand();
                    command.CommandText = "Select [name] FROM master.dbo.sysdatabases where dbid > 4";
                    var reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        this.AvailableDatabases.Add(reader.GetString(0));
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    MainWindow.MainWindowHandle.ShowMessage(LocalizationService.Resolve(() => Text.ErrorSqlServer) + " : " + ex.Message);
                }
            }
        }

        private void GetListOfSqlServers()
        {
            var list = GetLocalSqlServerInstanceNames();

            foreach (var server in list)
            {
                this.AvailableServers.Add(server);
            }
        }


        private string GetSqlServerConnectionString(string databaseName)
        {
            var connectionString = string.Format(
                this.sqlServerConnectionString,
                this.SelectedServer,
                this.UserName,
                this.PassWord,
                databaseName);

            return connectionString;
        }

        private void OpenDatabase()
        {
            if (string.IsNullOrEmpty(this.UserName) || string.IsNullOrEmpty(this.PassWord))
            {
                MainWindow.MainWindowHandle.ShowMessage("You must set User name and Password!");
                return;
            }

            if (this.SelectedDatabase == null)
            {
                return;
            }

            Settings.Default.DataSource = this.GetSqlServerConnectionString(this.SelectedDatabase);
            Settings.Default.UseSQLCompact = false;
            Settings.Default.Save();

            MainWindow.MainWindowHandle.OpenEvent(Settings.Default.DataSource, true, null);
            MainWindow.MainWindowHandle.CloseDialog();
        }

        private void SetCouplesMissing()
        {
            var result = MainWindow.MainWindowHandle.ShowMessage(
                    LocalizationService.Resolve(() => Text.SetMissingMessage), 
                    LocalizationService.Resolve(() => Text.SetMissingCaption), MessageDialogStyle.AffirmativeAndNegative);

            if (result == MessageDialogResult.Negative)
            {
                return;
            }

            using (var context = new ScrutinusContext())
            {
                var participants = context.Participants.Where(p => p.Competition.CurrentRound == null && p.State == CoupleState.Dancing);

                foreach (var participant in participants)
                {
                    participant.State = CoupleState.Missing;
                }

                context.SaveChanges();
            }

        }

        #endregion
    }

    public enum RegistryHive
    {
        Wow64,

        Wow6432
    }

    public class RegistryValueDataReader
    {
        #region Public Methods and Operators

        public string[] ReadRegistryValueData(
            RegistryHive registryHive,
            RegistryKey registryKey,
            string subKey,
            string valueName)
        {
            var instanceNames = new string[0];

            var key = GetRegistryHiveKey(registryHive);
            var registryKeyUIntPtr = GetRegistryKeyUIntPtr(registryKey);

            IntPtr hResult;

            var res = RegOpenKeyEx(registryKeyUIntPtr, subKey, 0, KEY_QUERY_VALUE | key, out hResult);

            if (res == 0)
            {
                uint type;
                uint dataLen = 0;

                RegQueryValueEx(hResult, valueName, 0, out type, IntPtr.Zero, ref dataLen);

                var databuff = new byte[dataLen];
                var temp = new byte[dataLen];

                var values = new List<string>();

                var handle = GCHandle.Alloc(databuff, GCHandleType.Pinned);
                try
                {
                    RegQueryValueEx(hResult, valueName, 0, out type, handle.AddrOfPinnedObject(), ref dataLen);
                }
                finally
                {
                    handle.Free();
                }

                var i = 0;
                var j = 0;

                while (i < databuff.Length)
                {
                    if (databuff[i] == '\0')
                    {
                        j = 0;
                        var str = Encoding.Default.GetString(temp).Trim('\0');

                        if (!string.IsNullOrEmpty(str))
                        {
                            values.Add(str);
                        }

                        temp = new byte[dataLen];
                    }
                    else
                    {
                        temp[j++] = databuff[i];
                    }

                    ++i;
                }

                instanceNames = new string[values.Count];
                values.CopyTo(instanceNames);
            }

            return instanceNames;
        }

        #endregion

        #region Static Fields

        private static readonly UIntPtr HKEY_LOCAL_MACHINE = (UIntPtr)0x80000002;

        private static readonly int KEY_QUERY_VALUE = 0x1;

        private static readonly int KEY_WOW64_32KEY = 0x200;

        private static readonly int KEY_WOW64_64KEY = 0x100;

        #endregion

        #region Methods

        private static int GetRegistryHiveKey(RegistryHive registryHive)
        {
            return registryHive == RegistryHive.Wow64 ? KEY_WOW64_64KEY : KEY_WOW64_32KEY;
        }

        private static UIntPtr GetRegistryKeyUIntPtr(RegistryKey registry)
        {
            if (registry == Registry.LocalMachine)
            {
                return HKEY_LOCAL_MACHINE;
            }

            return UIntPtr.Zero;
        }

        [DllImport("advapi32.dll", CharSet = CharSet.Unicode, EntryPoint = "RegOpenKeyEx")]
        private static extern int RegOpenKeyEx(UIntPtr hKey, string subKey, uint options, int sam, out IntPtr phkResult);

        [DllImport("advapi32.dll", SetLastError = true)]
        private static extern int RegQueryValueEx(
            IntPtr hKey,
            string lpValueName,
            int lpReserved,
            out uint lpType,
            IntPtr lpData,
            ref uint lpcbData);

        #endregion
    }
}