﻿// // TPS.net TPS8 Scrutinus
// // ImportDtvCompetition.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows.Controls;

namespace Scrutinus.Dialogs
{
    /// <summary>
    ///     Interaction logic for ImportDtvCompetition.xaml
    /// </summary>
    public partial class ImportDtvCompetition : Page
    {
        #region Constructors and Destructors

        public ImportDtvCompetition()
        {
            this.InitializeComponent();
        }

        #endregion
    }
}