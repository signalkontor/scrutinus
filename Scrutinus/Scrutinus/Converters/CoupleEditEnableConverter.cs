﻿// // TPS.net TPS8 Scrutinus
// // CoupleEditEnableConverter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Globalization;
using System.Windows.Data;
using DataModel.Models;

namespace Scrutinus.Converters
{
    public class CoupleEditEnableConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var couple = value as Couple;

            if (couple == null)
            {
                return false;
            }

            if (!string.IsNullOrEmpty(couple.ExternalId))
            {
                return false;
            }

            return true;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
