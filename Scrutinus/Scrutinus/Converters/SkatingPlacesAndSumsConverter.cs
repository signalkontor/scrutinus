﻿// // TPS.net TPS8 Scrutinus
// // SkatingPlacesAndSumsConverter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Globalization;
using System.Windows.Data;
using Helpers.Skating;

namespace Scrutinus.Converters
{
    public class SkatingPlacesAndSumsConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(parameter is int))
            {
                return "";
            }

            if (!(value is CoupleResultFinal))
            {
                return "";
            }

            var place = (int) parameter;
            var coupleResult = value as CoupleResultFinal;

            if (coupleResult.NumberPlaces[place - 1] == 0)
            {
                return "";
            }

            if (coupleResult.CalculationUsed[place -1])
            {
                return coupleResult.SumsUsed[place - 1]
                           ? string.Format(" {0} ({1}) ", coupleResult.NumberPlaces[place - 1], coupleResult.Sums[place - 1])
                           : string.Format(" {0} ", coupleResult.NumberPlaces[place - 1]);
            }

            return "";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
