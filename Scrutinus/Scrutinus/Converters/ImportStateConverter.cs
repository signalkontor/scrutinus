﻿// // TPS.net TPS8 Scrutinus
// // ImportStateConverter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Globalization;
using System.Windows.Data;

namespace Scrutinus.Converters
{
    internal class ImportStateConverter : IValueConverter
    {
        #region Methods

        private string Add(string val, string add)
        {
            if (!String.IsNullOrEmpty(val))
            {
                val += ", ";
            }
            val += add;

            return val;
        }

        #endregion

        #region Public Methods and Operators

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is int))
            {
                return null;
            }

            var val = (int)value;

            var ret = "";
            if ((val & 1) == 1)
            {
                ret = this.Add(ret, "Competition not found");
            }
            if ((val & 2) == 2)
            {
                ret = this.Add(ret, "New Couple");
            }
            if ((val & 4) == 4)
            {
                ret = this.Add(ret, "No MIN");
            }
            if ((val & 8) == 8)
            {
                ret = this.Add(ret, "Not an valid WDSF Couple");
            }
            if ((val & 16) == 16)
            {
                ret = this.Add(ret, "Wrong Number Format");
            }
            if ((val & 32) == 32)
            {
                ret = this.Add(ret, "Wrong line format");
            }

            return ret;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return 0;
        }

        #endregion
    }
}