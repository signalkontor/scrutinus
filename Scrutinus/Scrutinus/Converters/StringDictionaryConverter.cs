﻿// // TPS.net TPS8 Scrutinus
// // StringDictionaryConverter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Data;

namespace Scrutinus.Converters
{
    public class StringDictionaryConverter : IValueConverter
    {
        #region Public Methods and Operators

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var dict = value as Dictionary<string, string>;
            var sparam = parameter as string;

            if (!dict.ContainsKey(sparam))
            {
                return "unknown " + sparam;
            }
            return dict[sparam];
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}