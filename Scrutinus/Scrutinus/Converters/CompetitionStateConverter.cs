﻿// // TPS.net TPS8 Scrutinus
// // CompetitionStateConverter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Globalization;
using System.Windows.Data;
using DataModel;
using Scrutinus.Localization;

namespace Scrutinus.Converters
{
    public class CompetitionStateConverter : IValueConverter
    {
        #region Public Methods and Operators

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is CompetitionState))
            {
                return null;
            }

            var state = (CompetitionState)value;

            switch (state)
            {
                case CompetitionState.Closed:
                    return LocalizationService.Resolve(() => Text.CompetitionState_Closed);
                case CompetitionState.CheckIn:
                    return LocalizationService.Resolve(() => Text.CompetitionState_Checkin);
                case CompetitionState.Startlist:
                    return LocalizationService.Resolve(() => Text.CompetitionState_Tps);
                case CompetitionState.Finished:
                    return LocalizationService.Resolve(() => Text.CompetitionState_Finished);
                case CompetitionState.EnterMarking:
                    return LocalizationService.Resolve(() => Text.EnterMarking);
                case CompetitionState.SelectQualified:
                    return LocalizationService.Resolve(() => Text.SelectQualifiedCouples);
                case CompetitionState.Printing:
                    return LocalizationService.Resolve(() => Text.Print);
                case CompetitionState.QualifiedList:
                    return LocalizationService.Resolve(() => Text.Qualified);
                case CompetitionState.RoundDrawing:
                    return LocalizationService.Resolve(() => Text.RoundDrawing);
                case CompetitionState.RoundDrawingResult:
                    return LocalizationService.Resolve(() => Text.RoundDrawingResult);
                case CompetitionState.Splitted:
                    return LocalizationService.Resolve(() => Text.Splitted);

            }

            return LocalizationService.Resolve(() => Text.CompetitionState_Others);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}