﻿// // TPS.net TPS8 Scrutinus
// // WinnerCertificateElementTranslatorConverter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Globalization;
using System.Windows.Data;
using Scrutinus.Dialogs.DialogViewModels;
using Scrutinus.Localization;

namespace Scrutinus.Converters
{
    internal class WinnerCertificateElementTranslatorConverter : IValueConverter
    {
        #region Public Methods and Operators

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is WinnerCertificateElementTypes))
            {
                return value;
            }

            var elementType = (WinnerCertificateElementTypes)value;

            return LocalizationService.Resolve("Scrutinus:Text:WinnerCertificateElementTypes_" + elementType);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}