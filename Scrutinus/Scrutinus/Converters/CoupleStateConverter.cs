﻿// // TPS.net TPS8 Scrutinus
// // CoupleStateConverter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;
using DataModel;

namespace Scrutinus.Converters
{
    public class CoupleStateConverter : IValueConverter
    {
        #region Public Methods and Operators

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is CoupleState))
            {
                return new SolidColorBrush(Color.FromRgb(40, 40, 40));
            }

            var val = (CoupleState)value;

            switch (val)
            {
                case CoupleState.Dancing:
                    return new SolidColorBrush(Colors.Transparent);
                case CoupleState.Missing:
                    var redBrush = new SolidColorBrush(Colors.Tomato);
                    redBrush.Opacity = 0.5;
                    return redBrush;
                case CoupleState.Excused:
                    var yellowBrush = new SolidColorBrush(Colors.Gold);
                    yellowBrush.Opacity = 0.5;
                    return yellowBrush;
            }

            return new SolidColorBrush(Colors.LightGray);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return 0;
        }

        #endregion
    }
}