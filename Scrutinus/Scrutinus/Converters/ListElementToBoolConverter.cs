﻿// // TPS.net TPS8 Scrutinus
// // ListElementToBoolConverter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Windows.Data;
using Scrutinus.ViewModels;

namespace Scrutinus.Converters
{
    internal class ListElementToBoolConverter : IValueConverter
    {
        #region Public Methods and Operators

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var index = (int)parameter;
            var comps = (List<AssignJudgeToCompetionCompetitionData>)value;

            return comps.Single(c => c.Competition.Id == index).IsSelected;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Debug.WriteLine(value.ToString());

            return null;
        }

        #endregion
    }
}