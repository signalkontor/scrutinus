﻿// // TPS.net TPS8 Scrutinus
// // CompetitionStateToBoolConverter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Globalization;
using System.Windows.Data;
using DataModel;

namespace Scrutinus.Converters
{
    internal class CompetitionStateToBoolConverter : IValueConverter
    {
        public CompetitionState NeededState { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is CompetitionState))
            {
                return false;
            }

            var state = (CompetitionState) value;

            return state == this.NeededState;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
