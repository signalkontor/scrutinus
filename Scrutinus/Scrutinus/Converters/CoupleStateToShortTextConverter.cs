﻿// // TPS.net TPS8 Scrutinus
// // CoupleStateToShortTextConverter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Globalization;
using System.Windows.Data;
using DataModel;
using Scrutinus.Localization;

namespace Scrutinus.Converters
{
    public class CoupleStateToShortTextConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is CoupleState))
            {
                return "";
            }

            var state = (CoupleState) value;

            switch (state)
            {
                case CoupleState.Dancing:
                    return Text.CoupleStateDancingShort;
                case CoupleState.Disqualified:
                    return Text.CoupleStateDisqualifiedShort;
                case CoupleState.DroppedOut:
                    return Text.CoupleStateDroppedOutShort;
                case CoupleState.Excused:
                    return Text.CoupleStateExcusedShort;
                case CoupleState.Missing:
                    return Text.CoupleStateMissingShort;
            }

            return "";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
