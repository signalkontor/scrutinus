﻿// // TPS.net TPS8 Scrutinus
// // CompetitionStateToBrushConverter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using DataModel;
using DataModel.Models;

namespace Scrutinus.Converters
{
    public class CompetitionStateToBrushConverter : IValueConverter
    {
        #region Public Methods and Operators

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is Competition))
            {
                return Brushes.White;
            }

            var competition = (Competition)value;

            if (competition.State == CompetitionState.CheckIn || competition.State == CompetitionState.Closed)
            {
                return Brushes.Brown;
            }

            if (competition.State == CompetitionState.Finished)
            {
                return Brushes.PaleVioletRed;
            }

            if (competition.CurrentRound == null)
            {
                return Application.Current.Resources["LabelTextBrush"];
            }

            return Brushes.Green;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}