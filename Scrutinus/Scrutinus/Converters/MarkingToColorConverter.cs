﻿// // TPS.net TPS8 Scrutinus
// // MarkingToColorConverter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace Scrutinus.Converters
{
    internal class MarkingToColorConverter : IValueConverter
    {
        #region Public Methods and Operators

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is int)
            {
                var mark = (int)value;
                switch (mark)
                {
                    case 0:
                        var brush = new SolidColorBrush(Colors.Black);
                        brush.Freeze();
                        return brush;
                    case 1:
                        var greenBrush = new SolidColorBrush(Colors.Green);
                        greenBrush.Freeze();
                        return greenBrush;
                    default:
                        return new SolidColorBrush(Colors.White);
                }
            }
            if (value is bool)
            {
                var mark = (bool)value;
                if (mark)
                {
                    return new SolidColorBrush(Colors.Green);
                }
                return new SolidColorBrush(Colors.DarkGray);
            }

            return new SolidColorBrush(Colors.Red);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var val = (SolidColorBrush)value;
            if (val.Color == Colors.Green)
            {
                return 1;
            }
            return 0;
        }

        #endregion
    }
}