﻿// // TPS.net TPS8 Scrutinus
// // BooleanVisibilityConverter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Scrutinus.Converters
{
    internal class BooleanVisibilityConverter : IValueConverter
    {
        #region Public Properties

        public Visibility FalseVisibility { get; set; }

        public Visibility TrueVisibility { get; set; }

        #endregion

        #region Public Methods and Operators

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is bool))
            {
                return this.FalseVisibility;
            }

            var b = (bool)value;

            return b ? this.TrueVisibility : this.FalseVisibility;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}