﻿// // TPS.net TPS8 Scrutinus
// // TeamOrJmdToVisibilityConverter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using DataModel.Models;

namespace Scrutinus.Converters
{
    public class TeamOrJmdToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is Competition))
            {
                return Visibility.Collapsed;
            }

            var competition = (Competition) value;

            if (competition.CompetitionType.IsFormation || competition.CompetitionType.IsJMD)
            {
                return Visibility.Visible;
            }

            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
