﻿// // TPS.net TPS8 Scrutinus
// // Converters.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH
namespace Scrutinus.Converters
{
    public static class Converters
    {
        #region Static Fields

        public static CoupleQualifiedConverter CoupleIsQualifiedToBrushConverter = new CoupleQualifiedConverter();

        public static CoupleStateConverter CoupleStateConverter = new CoupleStateConverter();

        public static StateToStringConverter StateToStringConverter = new StateToStringConverter();

        #endregion
    }
}