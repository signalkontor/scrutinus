﻿// // TPS.net TPS8 Scrutinus
// // MarksSumPerHeatConverter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Data;
using DataModel.Models;
using Scrutinus.Pages;

namespace Scrutinus.Converters
{
    public class MarksSumPerHeatConverter : IValueConverter
    {
        #region Constructors and Destructors

        public MarksSumPerHeatConverter(List<JudgementViewModel> judgements, Official judge, Round currentRound)
        {
            this.judgements = judgements;
            this.judge = judge;
            this.currentRound = currentRound;
        }

        #endregion

        #region Fields

        private readonly Official judge;

        private readonly List<JudgementViewModel> judgements;

        private Round currentRound;

        #endregion

        #region Public Methods and Operators

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (parameter is int)
            {
                var danceId = (int)parameter;
                var count = this.judgements.Count(m => m.Dance.Dance.Id == danceId && m.Official.Id == this.judge.Id && m.Mark);

                return count;

            }

            return 0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}