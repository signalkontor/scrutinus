﻿// // TPS.net TPS8 Scrutinus
// // CoupleStateDancingToBoolConverter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Globalization;
using System.Windows.Data;
using DataModel;

namespace Scrutinus.Converters
{
    internal class CoupleStateDancingToBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(!(value is CoupleState))
            {
                return false;
            }

            var state = (CoupleState) value;

            return state == CoupleState.Dancing || state == CoupleState.Missing;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
