﻿// // TPS.net TPS8 Scrutinus
// // BoolToTextDecorationConverter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Scrutinus.Converters
{
    internal class BoolToTextDecorationConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is bool))
            {
                return null;
            }

            var b = (bool)value;

            if (b)
            {
                return new TextDecorationCollection(TextDecorations.Strikethrough);
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
