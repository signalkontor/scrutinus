﻿// // TPS.net TPS8 Scrutinus
// // StateToStringConverter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Globalization;
using System.Windows.Data;
using DataModel;
using Scrutinus.Localization;

namespace Scrutinus.Converters
{
    /// <summary>
    ///     Converts a state of a participant to a localized string
    /// </summary>
    public class StateToStringConverter : IValueConverter
    {
        #region Public Methods and Operators

        /// <summary>
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is CoupleState))
            {
                return string.Empty;
            }

            var state = (CoupleState)value;

            switch (state)
            {
                case CoupleState.Dancing:
                    return LocalizationService.Resolve(() => Text.StateDancing);
                case CoupleState.Missing:
                    return LocalizationService.Resolve(() => Text.StateMissing);
                case CoupleState.Excused:
                    return LocalizationService.Resolve(() => Text.StateExcused);
                case CoupleState.DroppedOut:
                    return LocalizationService.Resolve(() => Text.StateDroppedOut);
                case CoupleState.Disqualified:
                    return LocalizationService.Resolve(() => Text.StateDisqualified);
            }

            return "unknown";
        }

        /// <summary>
        ///     Converts back, not used
        /// </summary>
        /// <param name="value">The parameter is not used.</param>
        /// <param name="targetType">not usedThe parameter is not used.</param>
        /// <param name="parameter">not usedThe parameter is not used.</param>
        /// <param name="culture">The parameter is not used.</param>
        /// <returns>not used</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}