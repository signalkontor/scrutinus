﻿// // TPS.net TPS8 Scrutinus
// // OnlineOfflineToColorConverter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace Scrutinus.Converters
{
    public class OnlineOfflineToColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is bool))
            {
                return Brushes.Transparent;
            }

            var b = (bool)value;

            if (b)
            {
                return Brushes.LightGreen;
            }
            else
            {
                return Brushes.Red;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
