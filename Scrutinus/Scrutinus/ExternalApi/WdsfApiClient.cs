﻿// // TPS.net TPS8 Scrutinus
// // WdsfApiClient.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using DataModel;
using DataModel.Models;
using NLog;
using Scrutinus.Localization;
using Wdsf.Api.Client;
using Wdsf.Api.Client.Exceptions;
using Wdsf.Api.Client.Models;
using Competition = DataModel.Models.Competition;
using Couple = DataModel.Models.Couple;
using Dance = Wdsf.Api.Client.Models.Dance;
using Official = DataModel.Models.Official;
using Round = DataModel.Models.Round;
using Team = DataModel.Models.Team;

namespace Scrutinus.ExternalApi
{
    public class WdsfApiClient
    {
        /*
         * <xs:restriction base="xs:string">
                <xs:enumeration value="JUVENILE"/>
                <xs:enumeration value="JUVENILE I"/>
                <xs:enumeration value="JUVENILE II"/>
                <xs:enumeration value="JUNIOR"/>
                <xs:enumeration value="JUNIOR I"/>
                <xs:enumeration value="JUNIOR II"/>
                <xs:enumeration value="YOUTH"/>
                <xs:enumeration value="RISING STAR"/>
                <xs:enumeration value="UNDER 21"/>
                <xs:enumeration value="ADULT"/>
                <xs:enumeration value="SENIOR"/>
                <xs:enumeration value="SENIOR I"/>
                <xs:enumeration value="SENIOR II"/>
                <xs:enumeration value="SENIOR III"/>
                <xs:enumeration value="SENIOR IV"/>
                <xs:enumeration value="MASTER CLASS I"/>
                <xs:enumeration value="MASTER CLASS II"/>
                </xs:restriction>
         */

        #region Constructors and Destructors

        public WdsfApiClient(ScrutinusContext context, Competition competition)
        {
            this.competition = competition;

            this.context = context;

            this.translateDanceName = new Dictionary<string, string>
                                          {
                                              { "SW", "WALTZ" },
                                              { "LW", "WALTZ" },
                                              { "TG", "TANGO" },
                                              { "VW", "VIENNESE WALTZ" },
                                              { "WW", "VIENNESE WALTZ" },
                                              { "SF", "SLOW FOXTROT" },
                                              { "QS", "QUICKSTEP" },
                                              { "QU", "QUICKSTEP" },
                                              { "SB", "SAMBA" },
                                              { "CC", "CHA CHA CHA" },
                                              { "RB", "RUMBA" },
                                              { "PD", "PASO DOBLE" },
                                              { "JV", "JIVE" },
                                              { "SA", "SALSA" },
                                              // { "YN", "SALSA" },
                                              { "SS", "SHOWDANCE" },
                                              { "SL", "SHOWDANCE" },
                                              { "FL", "FORMATION LATIN" },
                                              { "FS", "FORMATION STANDARD" }
                                          };
#if DEBUG
            var endpoint = WdsfEndpoint.Services;
            // this.apiClient = new Client("guest", "guest", endpoint);
            this.apiClient = new Client(Settings.Default.WDSFApiUser, Settings.Default.WDSFApiPassword, endpoint);
#else
            var endpoint = WdsfEndpoint.Services;
    this.apiClient = new Client(Settings.Default.WDSFApiUser, Settings.Default.WDSFApiPassword, endpoint);
#endif


            this.officials = new List<OfficialDetail>();

            if (competition == null)
            {
                return;
            }

            int.TryParse(competition.ExternalId, out this.wdsfCompetitionId);
        }

        #endregion

        #region Fields

        private readonly Client apiClient;

        private readonly Competition competition;

        private readonly ScrutinusContext context;

        private readonly Logger logger = LogManager.GetLogger("wdsf");

        private readonly List<OfficialDetail> officials;

        private readonly Dictionary<string, string> translateDanceName;

        private readonly int wdsfCompetitionId;

        private Dictionary<int, string> translateAgeGroups;

        #endregion

        #region Public Methods and Operators

        public void CheckCouples(IEnumerable<Couple> couples)
        {
            var count = 0;
            foreach (var couple in couples)
            {
                count++;
                MainWindow.MainWindowHandle.ReportStatus("Registering Couples", count, couples.Count());

                if (!string.IsNullOrEmpty(couple.WdsfIdMan) && !string.IsNullOrEmpty(couple.WdsfIdWoman))
                {
                    this.CheckWithMins(couple);
                    continue;
                }

                this.SearchByName(couple);
            }
        }

        public void CheckOfficials(IEnumerable<Official> officials)
        {
            var count = 0;
            var max = officials.Count();

            foreach (var official in officials)
            {
                count++;
                MainWindow.MainWindowHandle.ReportStatus("Check Official", count, max);

                if (official.MIN.HasValue)
                {
                    official.WdsfApiStatus = ApiStatus.OK;
                    this.SetLicenses(official.MIN.Value, official);
                    continue;
                }

                // find the official my name
                var personFilter = new Dictionary<string, string>
                                       {
                                           { "name", official.Firstname },
                                           { "surname", official.Lastname },
                                       };

                official.WdsfApiStatus = ApiStatus.ManNotFound;

                var list = this.apiClient.GetPersons(personFilter);

                if (list.Count == 1)
                {
                    this.SetLicenses(list[0].Min, official);

                    official.MIN = list[0].Min;
                    official.WdsfApiStatus = ApiStatus.OK;
                    continue;
                }

                foreach (var person in list)
                {
                    if (person.Name == string.Format("{0} {1}", official.Firstname, official.Lastname))
                    {
                        this.SetLicenses(list[0].Min, official);

                        official.MIN = person.Min;
                        official.WdsfApiStatus = ApiStatus.OK;
                        break;
                    }
                }
            }

            MainWindow.MainWindowHandle.RemoveStatusReport();
        }

        private void SetLicenses(int min, Official official)
        {
            var details = this.apiClient.GetPerson(min);

            if (details.Licenses != null)
            {
                official.Licenses = string.Join(", ", details.Licenses.Where(l => l.Status == "Active").Select(l => l.Type + "(" + l.Division + ")"));
            }
        }

        public void CheckParticipants(IEnumerable<Participant> participants, bool createIfNotExisting)
        {
            var numParticipants = participants.Count();
            MainWindow.MainWindowHandle.ReportStatus("Checking Couples", 0, numParticipants);

            this.logger.Info("Checking Participants");

            var count = 0;

            foreach (var participant in participants)
            {

                if (participant.Couple is Team)
                {
                    continue;
                }
                if (string.IsNullOrEmpty(participant.Couple.LastMan))
                {
                    var index = participant.Couple.FirstMan.LastIndexOf(" ");
                    if (index > -1)
                    {
                        participant.Couple.LastMan = participant.Couple.FirstMan.Substring(index + 1);
                        participant.Couple.FirstMan = participant.Couple.FirstMan.Substring(0, index);
                    }
                }

                if (string.IsNullOrEmpty(participant.Couple.LastWoman))
                {
                    var index = participant.Couple.FirstWoman.LastIndexOf(" ");
                    if (index > -1)
                    {
                        participant.Couple.LastWoman = participant.Couple.FirstWoman.Substring(index + 1);
                        participant.Couple.FirstWoman = participant.Couple.FirstWoman.Substring(0, index);
                    }
                }

                count++;
                MainWindow.MainWindowHandle.ReportStatus("Checking Couples", count, numParticipants);

                if (!string.IsNullOrEmpty(participant.Couple.WdsfIdMan)
                    && !string.IsNullOrEmpty(participant.Couple.WdsfIdWoman))
                {
                    this.CheckWithMins(participant.Couple);
                    continue;
                }

                this.SearchByName(participant.Couple);

                if (createIfNotExisting && participant.WdsfApiStatus == ApiStatus.NotACouple)
                {
                    var newCouple = this.CreateCouple(participant);
                }
            }

            this.context.SaveChanges();

            MainWindow.MainWindowHandle.RemoveStatusReport();
        }

        public void ClearAllParticipants()
        {
            try
            {
                MainWindow.MainWindowHandle.ReportStatus("Clearing all Couples", 0, 100);

                var participantsList = this.apiClient.GetCoupleParticipants(this.wdsfCompetitionId);
                var max = participantsList.Count;
                var current = 0;
                foreach (var wdsfParticipant in participantsList)
                {
                    MainWindow.MainWindowHandle.ReportStatus("Clearing all Couples", current, max);
                    this.apiClient.DeleteCoupleParticipant(wdsfParticipant.Id);
                    current++;
                }
            }
            catch (Exception excepetion)
            {
                MainWindow.MainWindowHandle.ShowMessage("Error: " + excepetion.Message);
            }
        }

        public void ClearAllTeams()
        {
            try
            {
                MainWindow.MainWindowHandle.ReportStatus("Clearing all Teams", 0, 100);

                var participantsList = this.apiClient.GetTeamParticipants(this.wdsfCompetitionId);
                var max = participantsList.Count;
                var current = 0;
                foreach (var wdsfParticipant in participantsList)
                {
                    MainWindow.MainWindowHandle.ReportStatus("Clearing all Teams", current, max);
                    this.apiClient.DeleteCoupleParticipant(wdsfParticipant.Id);
                    current++;
                }
            }
            catch (Exception excepetion)
            {
                MainWindow.MainWindowHandle.ShowMessage("Error: " + excepetion.Message);
            }
        }

        public void ClearCompetition()
        {
            var participants = this.apiClient.GetCoupleParticipants(this.wdsfCompetitionId);

            foreach (var participant in participants)
            {
                this.apiClient.DeleteCoupleParticipant(participant.Id);
            }

            // now delete all judges:
            var officials = this.apiClient.GetOfficials(this.wdsfCompetitionId);
            foreach (var official in officials)
            {
                this.apiClient.DeleteOfficial(official.Id);
            }
        }

        public void ClearOfficials()
        {
            MainWindow.MainWindowHandle.ReportStatus("Deleting Official", 0, 100);

            var competitionOfficials = this.apiClient.GetOfficials(this.wdsfCompetitionId);

            foreach (var officialDetail in competitionOfficials)
            {
                this.apiClient.DeleteOfficial(officialDetail.Id);
            }
        }

        public void RegisterOfficials()
        {
            this.logger.Info("Registering Officials");
            var judges = this.competition.GetJudges();

            MainWindow.MainWindowHandle.ReportStatus("Register Official", 0, judges.Count);

            if (judges.Any(j => !j.MIN.HasValue))
            {
                MainWindow.MainWindowHandle.ShowMessage(LocalizationService.Resolve(() => Text.NotAllMinsSet));
            }

            this.CheckOfficials(this.competition.Officials.Select(o => o.Official));

            var officials = this.apiClient.GetOfficials(this.wdsfCompetitionId);

            foreach (var official in officials)
            {
                var details = this.apiClient.GetOfficial(official.Id);
                this.officials.Add(details);
            }

            var counter = 0;

            foreach (var judge in judges)
            {
                var existing = this.officials.SingleOrDefault(o => o.AdjudicatorChar == judge.Sign);
                if (existing != null)
                {
                    judge.WdsfId = existing.Id.ToString();
                    continue;
                }

                // Register this new judge
                var wdsfOfficial = new OfficialDetail
                                       {
                                           CompetitionId = this.wdsfCompetitionId,
                                           AdjudicatorChar = judge.Sign,
                                           Min = judge.MIN.HasValue ? judge.MIN.Value : 0,
                                           Nationality = judge.Nationality?.LongName ?? "",
                                           Person = judge.NiceName,
                                           Task = "Adjudicator"
                                       };
                try
                {
                    counter ++;
                    var uri = this.apiClient.SaveOfficial(wdsfOfficial);
                    var official = this.apiClient.Get<OfficialDetail>(uri);
                    judge.WdsfId = official.Id.ToString();
                    this.logger.Info("Official {0} registered for the competition", judge.NiceName);
                    MainWindow.MainWindowHandle.ReportStatus("Register Official", counter, judges.Count);
                }
                catch (Exception ex)
                {
                    this.logger.Error(
                        "Can not register " + judge.NiceName + " : " + (this.apiClient.LastApiMessage ?? ex.Message));
                    if (ex.InnerException != null)
                    {
                        this.logger.Error(ex.InnerException.Message);
                    }
                }
            }

            var chairman = this.competition.Officials.SingleOrDefault(o => o.Role.Id == Roles.Chairman);

            if (chairman != null && this.officials.All(o => o.AdjudicatorChar != chairman.Official.Sign))
            {
                // Ok, we have a chairperson but this person is not yet registered. So let's do that now:
                var wdsfOfficial = new OfficialDetail
                                       {
                                           CompetitionId = this.wdsfCompetitionId,
                                           AdjudicatorChar = chairman.Official.Sign,
                                           Min =
                                               chairman.Official.MIN.HasValue
                                                   ? chairman.Official.MIN.Value
                                                   : 0,
                                           Nationality = chairman.Official.Club,
                                           Person = chairman.Official.NiceName,
                                           Task = "Chairman"
                                       };
                try
                {
                    var uri = this.apiClient.SaveOfficial(wdsfOfficial);
                    var wdsfChair = this.apiClient.Get<OfficialDetail>(uri);
                    chairman.WdsfId = wdsfChair.Id.ToString();
                }
                catch (Exception ex)
                {
                    this.logger.Error(
                        "Can not register " + chairman.Official.NiceName + " : "
                        + (this.apiClient.LastApiMessage ?? ex.Message));
                }

                MainWindow.MainWindowHandle.RemoveStatusReport();
            }
        }

        public void RegisterParticipants(IEnumerable<Participant> participants)
        {
            this.CheckParticipants(participants.Where(p => string.IsNullOrEmpty(p.Couple.WdsfCoupleId)), true);

            MainWindow.MainWindowHandle.ReportStatus("Registering Couples", 0, participants.Count());

            var number = 0;

            foreach (var participant in participants)
            {
                participant.WdsfId = null;
                participant.WdsfApiStatus = ApiStatus.Unknown;
            }

            var test = participants.FirstOrDefault();
            if (test == null)
            {
                return;
            }

            if (test.Couple is Team)
            {
                var teamList = this.apiClient.GetTeamParticipants(this.wdsfCompetitionId);
                foreach (var wdsfParticipant in teamList)
                {
                    var localParticipant =
                        participants.SingleOrDefault(p => p.Number.ToString() == wdsfParticipant.StartNumber);

                    if (localParticipant != null)
                    {
                        localParticipant.WdsfId = wdsfParticipant.Id.ToString();
                        localParticipant.WdsfApiStatus = ApiStatus.Registered;
                    }
                }
            }
            else
            {
                var participantsList = this.apiClient.GetCoupleParticipants(this.wdsfCompetitionId);
                foreach (var wdsfParticipant in participantsList)
                {
                    var localParticipant =
                        participants.SingleOrDefault(p => p.Number.ToString() == wdsfParticipant.StartNumber);

                    if (localParticipant != null)
                    {
                        localParticipant.WdsfId = wdsfParticipant.Id.ToString();
                        localParticipant.WdsfApiStatus = ApiStatus.Registered;
                    }
                }

            }

            foreach (var participant in participants.Where(p => p.WdsfId == null))
            {
                var uri = this.RegisterParticipant(participant);

                if (uri != null)
                {
                    if (participant.Couple is Team team)
                    {
                        var wdsfTeamParticipant = this.apiClient.Get<ParticipantTeamDetail>(uri);
                        participant.WdsfApiStatus = ApiStatus.Registered;
                        participant.WdsfId = wdsfTeamParticipant.Id.ToString();
                    }
                    else
                    {
                        var wdsfParticipant = this.apiClient.Get<ParticipantCoupleDetail>(uri);
                        participant.WdsfApiStatus = ApiStatus.Registered;
                        participant.WdsfId = wdsfParticipant.Id.ToString();
                    }

                }
                MainWindow.MainWindowHandle.ReportStatus("Registering Couples", number, participants.Count());

                number++;
            }

            this.context.SaveChanges();

            MainWindow.MainWindowHandle.RemoveStatusReport();
        }

        public void SendResults(Round round)
        {
            if (round.Qualifieds.First().Participant.Couple is Team)
            {
                this.SendResultTeams(round);
                return;
            }

            this.RegisterOfficials();
            this.RegisterParticipants(round.Competition.Participants);

            MainWindow.MainWindowHandle.ReportStatus("Sending Results", 0, this.competition.Participants.Count);

            var number = 0;

            var registered = this.apiClient.GetCoupleParticipants(this.wdsfCompetitionId);

            foreach (var participant in this.competition.Participants)
            {
                ParticipantCoupleDetail wdsfParticipant = null;

                var wdsfParticipantSimple =
                    registered.SingleOrDefault(r => r.StartNumber == participant.Number.ToString());
                if (wdsfParticipantSimple == null)
                {
                    // We try to register the couple
                    var uri = this.RegisterParticipant(participant);
                    if (uri != null)
                    {
                        wdsfParticipant = this.apiClient.Get<ParticipantCoupleDetail>(uri);
                    }
                    else
                    {
                        this.logger.Error("Couple {0} not registerd", participant.Couple.NiceName);
                    }
                }
                else
                {
                    wdsfParticipant = this.apiClient.GetCoupleParticipant(wdsfParticipantSimple.Id);
                }

                if (wdsfParticipant != null)
                {
                    switch (participant.State)
                    {
                        case CoupleState.DroppedOut:
                            wdsfParticipant.Rank = participant.PlaceFrom.Value.ToString();
                            break;
                        case CoupleState.Excused:
                            wdsfParticipant.Status = "Excused";
                            break;
                        case CoupleState.Missing:
                            wdsfParticipant.Status = "Noshow";
                            break;
                    }

                    this.SetRoundMarking(wdsfParticipant, participant.Star);

                    try
                    {
                        this.apiClient.UpdateCoupleParticipant(wdsfParticipant);
                    }
                    catch (Exception ex)
                    {
                        this.logger.Error(ex);
                    }
                }
                else
                {
                    this.logger.Error("Coul not load Participant {0}", participant.NiceNameWithNumber);
                }

                MainWindow.MainWindowHandle.ReportStatus("Sending Results", number, this.competition.Participants.Count);
                number++;
            }

            this.SetStatus("InProgress");

            this.context.SaveChanges();

            MainWindow.MainWindowHandle.RemoveStatusReport();
            MainWindow.MainWindowHandle.ShowMessage("Results have been uploaded, please check the logfile.");
            // Open the log file:
            var file = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
                          + @"/TPS.net/WdsfLog.txt";
            // Show this file in notepad:
            Process.Start("file:///" + file);
        }

        public void SendResultTeams(Round round)
        {
            this.RegisterOfficials();
            this.RegisterParticipants(round.Competition.Participants);

            MainWindow.MainWindowHandle.ReportStatus("Sending Results", 0, this.competition.Participants.Count);

            var number = 0;

            var registered = this.apiClient.GetTeamParticipants(this.wdsfCompetitionId);

            foreach (var participant in this.competition.Participants)
            {
                ParticipantTeamDetail wdsfParticipant = null;

                var wdsfParticipantSimple =
                    registered.SingleOrDefault(r => r.StartNumber == participant.Number.ToString());
                if (wdsfParticipantSimple == null)
                {
                    // We try to register the couple
                    var uri = this.RegisterParticipant(participant);
                    if (uri != null)
                    {
                        wdsfParticipant = this.apiClient.Get<ParticipantTeamDetail>(uri);
                    }
                    else
                    {
                        this.logger.Error("Couple {0} not registerd", participant.Couple.NiceName);
                    }
                }
                else
                {
                    wdsfParticipant = this.apiClient.GetTeamParticipant(wdsfParticipantSimple.Id);
                }

                if (wdsfParticipant != null)
                {
                    switch (participant.State)
                    {
                        case CoupleState.DroppedOut:
                            wdsfParticipant.Rank = participant.PlaceFrom.Value.ToString();
                            break;
                        case CoupleState.Excused:
                            wdsfParticipant.Status = "Excused";
                            break;
                        case CoupleState.Missing:
                            wdsfParticipant.Status = "Noshow";
                            break;
                    }

                    this.SetRoundMarkingTeam(wdsfParticipant, participant.Star);

                    try
                    {
                        var result = this.apiClient.UpdateTeamParticipant(wdsfParticipant);
                        if (!result)
                        {
                            logger.Error("Update Error: " + apiClient.LastApiMessage);
                        }
                    }
                    catch (Exception ex)
                    {
                        this.logger.Error(ex);
                    }
                }
                else
                {
                    this.logger.Error("Coul not load Participant {0}", participant.NiceNameWithNumber);
                }

                MainWindow.MainWindowHandle.ReportStatus("Sending Results", number, this.competition.Participants.Count);
                number++;
            }

            this.SetStatus("InProgress");

            this.context.SaveChanges();

            MainWindow.MainWindowHandle.RemoveStatusReport();
            MainWindow.MainWindowHandle.ShowMessage("Results have been uploaded, please check the logfile.");
            // Open the log file:
            var file = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
                          + @"/TPS.net/WdsfLog.txt";
            // Show this file in notepad:
            Process.Start("file:///" + file);
        }

        public void SetStatus(string status)
        {
            var wdsfCompetition = this.apiClient.GetCompetition(this.wdsfCompetitionId);

            if (wdsfCompetition.Status == "Closed")
            {
                throw new Exception("The competition is already closed!");
            }

            if (status == "Closed" && wdsfCompetition.Status != "Processing")
            {
                throw new Exception(
                    "The competition is not in state 'Processing' and cannot be closed. Please Upload the results first!");
            }

            wdsfCompetition.Status = status;

            var res = this.apiClient.UpdateCompetition(wdsfCompetition);

            if (!res)
            {
                this.logger.Error(this.apiClient.LastApiMessage);
                throw new Exception(this.apiClient.LastApiMessage);
            }
        }

        #endregion

        #region Methods

        private void CheckWithCoupleId(Participant participant)
        {
            var couple = this.apiClient.GetCouple(participant.Couple.WdsfCoupleId);
            if (couple != null)
            {
                var personMan = this.apiClient.GetPerson(couple.ManMin);
                participant.Couple.FirstMan = personMan.Name;
                participant.Couple.LastMan = personMan.Surname;
                var personWoman = this.apiClient.GetPerson(couple.WomanMin);
                participant.Couple.FirstWoman = personWoman.Name;
                participant.Couple.LastWoman = personWoman.Surname;
                participant.WdsfApiStatus = ApiStatus.OK;
            }
            else
            {
                participant.WdsfApiStatus = ApiStatus.CoupleNotFound;
            }
        }

        private void CheckWithMins(Couple couple)
        {
            var mans = this.apiClient.GetPersons(new Dictionary<string, string> { { "Min", couple.WdsfIdMan } });
            if (mans.Count != 1)
            {
                couple.WdsfApiStatus = ApiStatus.ManNotFound;
                return;
            }

            var womans =
                this.apiClient.GetPersons(new Dictionary<string, string> { { "Min", couple.WdsfIdWoman } });
            if (womans.Count != 1)
            {
                couple.WdsfApiStatus = ApiStatus.WomanNotFound;
                return;
            }

            if (mans[0].ActiveCoupleId == null || womans[0].ActiveCoupleId != mans[0].ActiveCoupleId)
            {
                couple.WdsfApiStatus = ApiStatus.NotACouple;
                return;
            }

            couple.WdsfCoupleId = mans[0].ActiveCoupleId;
            couple.WdsfApiStatus = ApiStatus.OK;

            var personMan = this.apiClient.GetPerson(mans[0].Min);
            couple.FirstMan = personMan.Name;
            couple.LastMan = personMan.Surname;
            var personWoman = this.apiClient.GetPerson(womans[0].Min);
            couple.FirstWoman = personWoman.Name;
            couple.LastWoman = personWoman.Surname;
        }

        private CoupleDetail CreateCouple(Participant participant)
        {
            if (string.IsNullOrWhiteSpace(participant.Couple.WdsfIdMan)
                || string.IsNullOrWhiteSpace(participant.Couple.WdsfIdWoman))
            {
                return null;
            }

            
            try
            {
                var newCouple = new CoupleDetail
                {
                    Country = participant.Couple.Country,
                    Name = participant.Couple.NiceName,
                    Division = "General",
                    ManMin = int.Parse(participant.Couple.WdsfIdMan),
                    WomanMin = int.Parse(participant.Couple.WdsfIdWoman),
                    ManMinSpecified = true,
                    WomanMinSpecified = true,
                    Status = "Active",
                    AgeGroup = "ADULT"
                };

                // Save the couple but catch exceptions (because of some violations in Country Names
                var ret = this.apiClient.SaveCouple(newCouple);
                newCouple = this.apiClient.Get<CoupleDetail>(ret);
                participant.Couple.WdsfCoupleId = newCouple.Id;
                participant.WdsfApiStatus = ApiStatus.Registered;

                return newCouple;
            }
            catch (Exception ex)
            {
                participant.WdsfApiStatus = ApiStatus.Unknown;
                this.logger.Error("Could not Create Couple " + participant.NiceNameWithNumber);
                this.logger.Error(ex);
            }

            return null;
        }

        private Uri RegisterParticipant(Participant participant)
        {
            if (participant.Couple is Team team)
            {
                var teamId = 0;

                if (int.TryParse(participant.Couple.MINMan, out teamId))
                {
                    var teamDetails = new ParticipantTeamDetail()
                    {
                        CompetitionId = this.wdsfCompetitionId,
                        TeamId = teamId,
                        StartNumber = participant.Number
                    };

                    return this.apiClient.SaveTeamParticipant(teamDetails);
                }
            }

            if (participant.Couple.WdsfCoupleId == null ||  string.IsNullOrWhiteSpace(participant.Couple.WdsfCoupleId))
            {
                participant.Couple.WdsfCoupleId = null;
                this.CreateCouple(participant);
            }

            if (participant.Couple.WdsfCoupleId == null)
            {
                return null;
            }


        
            var participantDetails = new ParticipantCoupleDetail
                                         {
                                             CompetitionId = this.wdsfCompetitionId,
                                             CoupleId =
                                                 participant.Couple.WdsfCoupleId.StartsWith("rls-")
                                                 || participant.Couple.WdsfCoupleId.StartsWith(
                                                     "wdsf-")
                                                     ? participant.Couple.WdsfCoupleId
                                                     : "wdsf-" + participant.Couple.WdsfCoupleId,
                                             StartNumber = participant.Number
                                         };
            try
            {
                return this.apiClient.SaveCoupleParticipant(participantDetails);
            }
            catch (ApiException ex)
            {
                this.logger.Error("Error with Couple #" + participant.NiceNameWithNumber);
                this.logger.Error(ex);
            }

            return null;
        }

        private void SearchByName(Couple couple)
        {
            // Search the couple
            var personFilter = new Dictionary<string, string>
                                   {
                                       { "name", couple.FirstMan },
                                       { "surname", couple.LastMan }
                                   };

            var pMan = this.apiClient.GetPersons(personFilter);

            var man = pMan.FirstOrDefault(p => p.Name.Contains(couple.FirstMan) && p.Name.Contains(couple.LastMan));

            if (man == null)
            {
                couple.WdsfApiStatus = ApiStatus.ManNotFound;
                return;
            }

            couple.WdsfIdMan = man.Min.ToString();
            couple.Country = man.Country;

            personFilter = new Dictionary<string, string>
                               {
                                   { "name", couple.FirstWoman },
                                   { "surname", couple.LastWoman }
                               };

            var pWoman = this.apiClient.GetPersons(personFilter);

            var woman =
                pWoman.FirstOrDefault(p => p.Name.Contains(couple.FirstWoman) && p.Name.Contains(couple.LastWoman));

            if (woman == null)
            {
                couple.WdsfApiStatus = ApiStatus.WomanNotFound;
                return;
            }

            couple.WdsfIdWoman = woman.Min.ToString();

            if (man.ActiveCoupleId == woman.ActiveCoupleId)
            {
                couple.WdsfCoupleId = man.ActiveCoupleId;
                couple.WdsfApiStatus = ApiStatus.OK;
                return;
            }

            couple.WdsfApiStatus = ApiStatus.NotACouple;
        }

        private void SetRoundForStarCouple(
            ParticipantCoupleDetail participant,
            Round round,
            Wdsf.Api.Client.Models.Round wdsfRound)
        {
            foreach (var dancesInCompetition in this.competition.Dances)
            {
                var dance = new Dance { Name = this.translateDanceName[dancesInCompetition.Dance.WdsfCode ?? dancesInCompetition.Dance.ShortName], };

                wdsfRound.Dances.Add(dance);

                foreach (var judge in this.competition.GetJudges())
                {
                    int officialId;

                    if (!Int32.TryParse(judge.WdsfId, out officialId))
                    {
                        this.logger.Error("Invalid WdsfId of judge " + judge.NiceName);
                    }

                    if (round.MarksInputType == MarkingTypes.NewJudgingSystemV2)
                    {
                        var score = new OnScale3Score() { IsSet = true, OfficialId = officialId};
                        dance.Scores.Add(score);
                    }
                    else
                    {
                        var score = new MarkScore(officialId, true) { IsSet = true, OfficialId = officialId };
                        dance.Scores.Add(score);
                    }

                }
            }
        }

        private void SetRoundMarking(ParticipantCoupleDetail participant, int stars)
        {
            participant.Rounds.Clear();

            foreach (var round in this.competition.Rounds.OrderBy(r => r.Number))
            {
                var wdsfRound = new Wdsf.Api.Client.Models.Round();             
                wdsfRound.Name = round.WdsfName;

                if (round.MarksInputType == MarkingTypes.NewJudgingSystemV2 && round.Js3CutOffValue > 0)
                {
                    wdsfRound.MaxDeviation = round.Js3CutOffValue.ToString().Replace(",", ".");
                }

                // check, if team was qualified in this round:
                if (round.Qualifieds.All(q => q.Participant.Number != participant.StartNumber))
                {
                    if (round.Number == 1 && stars > 0 || round.IsRedanceRound)
                    {
                        participant.Rounds.Add(wdsfRound);
                        this.SetRoundForStarCouple(participant, round, wdsfRound);
                    }
                    continue;
                }

                participant.Rounds.Add(wdsfRound);
                // Check for stars:
                if (round.Number == 1 && stars > 0)
                {
                    this.SetRoundForStarCouple(participant, round, wdsfRound);
                    continue;
                }

                foreach (var dancesInCompetition in this.competition.Dances)
                {
                    var danceInRound = context.DancesInRounds.First(d => d.Dance.Id == dancesInCompetition.Dance.Id && d.Round.Id == round.Id);

                    var dance = new Dance { Name = this.translateDanceName[dancesInCompetition.Dance.WdsfCode ?? dancesInCompetition.Dance.ShortName], IsGroupDance = !danceInRound.IsSolo };
                    // now we add a score element for each judge
                    foreach (var official in this.competition.GetJudges())
                    {
                        int officialId;

                        if (!Int32.TryParse(official.WdsfId, out officialId))
                        {
                            this.logger.Error("Invalid WdsfId of judge " + official.NiceName);
                        }

                        var ourParticipant =
                            this.competition.Participants.Single(p => p.Number == participant.StartNumber);
                        var markings = round.Markings.Where(
                                m =>
                                m.Dance.Id == dancesInCompetition.Dance.Id && m.Judge.Id == official.Id
                                && m.Participant.Id == ourParticipant.Id).ToList();

                        if(markings.Count > 1)
                        {
                            Debug.WriteLine("Doppelte");
                        }

                        var marking =
                            round.Markings.FirstOrDefault(
                                m =>
                                m.Dance.Id == dancesInCompetition.Dance.Id && m.Judge.Id == official.Id
                                && m.Participant.Id == ourParticipant.Id);

                        if (marking != null)
                        {
                            Score score = null;
                            switch (round.MarksInputType)
                            {
                                case MarkingTypes.Skating:
                                    score = new FinalScore(officialId, marking.Mark);
                                    break;
                                case MarkingTypes.Marking:
                                    if (marking.Mark > 0)
                                    {
                                        var markscore = new MarkScore(officialId, false);
                                        score = markscore;
                                    }
                                    break;
                                case MarkingTypes.NewJudgingSystemV1:
                                    score = new OnScale2Score
                                                {
                                                    OfficialId = officialId,
                                                    TQ = (decimal)marking.MarkA,
                                                    MM = (decimal)marking.MarkB,
                                                    CP = (decimal)marking.MarkC,
                                                    PS = (decimal)marking.MarkD
                                                };
                                    break;
                                case MarkingTypes.NewJudgingSystemV2:
                                    score = new OnScale3Score()
                                                {
                                                    OfficialId = officialId,
                                                    TQ = (decimal) marking.MarkA,
                                                    MM = (decimal) marking.MarkB,
                                                    PS = (decimal) marking.MarkC,
                                                    CP = (decimal) marking.MarkD,
                                                    
                                                };
                                    break;
                            }
                            if (score != null)
                            {
                                dance.Scores.Add(score);
                            }
                        }
                    }
                    wdsfRound.Dances.Add(dance);
                }
            }
        }

        private void SetRoundMarkingTeam(ParticipantTeamDetail team, int stars)
        {
            team.Rounds.Clear();

            foreach (var round in this.competition.Rounds.OrderBy(r => r.Number))
            {
                var wdsfRound = new Wdsf.Api.Client.Models.Round();
                wdsfRound.Name = round.WdsfName;

                if (round.MarksInputType == MarkingTypes.NewJudgingSystemV2 && round.Js3CutOffValue > 0)
                {
                    wdsfRound.MaxDeviation = round.Js3CutOffValue.ToString().Replace(",", ".");
                }

                // check, if team was qualified in this round:
                if (round.Qualifieds.All(q => q.Participant.Number != team.StartNumber))
                {
                    if (round.Number == 1 && stars > 0 || round.IsRedanceRound)
                    {
                        team.Rounds.Add(wdsfRound);
                        // this.SetRoundForStarCouple(team, round, wdsfRound);
                    }
                    continue;
                }

                team.Rounds.Add(wdsfRound);
                // Check for stars:
                if (round.Number == 1 && stars > 0)
                {
                    // this.SetRoundForStarCouple(team, round, wdsfRound);
                    continue;
                }

                foreach (var dancesInCompetition in this.competition.Dances)
                {
                    var danceInRound = context.DancesInRounds.First(d => d.Dance.Id == dancesInCompetition.Dance.Id && d.Round.Id == round.Id);

                    var dance = new Dance { Name = this.translateDanceName[dancesInCompetition.Dance.WdsfCode ?? dancesInCompetition.Dance.ShortName], IsGroupDance = false };
                    // now we add a score element for each judge
                    foreach (var official in this.competition.GetJudges())
                    {
                        if (!Int32.TryParse(official.WdsfId, out var officialId))
                        {
                            this.logger.Error("Invalid WdsfId of judge " + official.NiceName);
                        }

                        var ourTeam =
                            this.competition.Participants.Single(p => p.Number == team.StartNumber);
                        var markings = round.Markings.Where(
                                m =>
                                m.Dance.Id == dancesInCompetition.Dance.Id && m.Judge.Id == official.Id
                                && m.Participant.Id == ourTeam.Id).ToList();

                        if (markings.Count > 1)
                        {
                            Debug.WriteLine("Doppelte");
                        }

                        var marking =
                            round.Markings.FirstOrDefault(
                                m =>
                                m.Dance.Id == dancesInCompetition.Dance.Id && m.Judge.Id == official.Id
                                && m.Participant.Id == ourTeam.Id);

                        if (marking != null)
                        {
                            Score score = null;
                            switch (round.MarksInputType)
                            {
                                case MarkingTypes.Skating:
                                    score = new FinalScore(officialId, marking.Mark);
                                    break;
                                case MarkingTypes.Marking:
                                    if (marking.Mark > 0)
                                    {
                                        var markscore = new MarkScore(officialId, false);
                                        score = markscore;
                                    }
                                    break;
                                case MarkingTypes.NewJudgingSystemV1:
                                    score = new OnScale2Score
                                    {
                                        OfficialId = officialId,
                                        TQ = (decimal)marking.MarkA,
                                        MM = (decimal)marking.MarkB,
                                        CP = (decimal)marking.MarkC,
                                        PS = (decimal)marking.MarkD
                                    };
                                    break;
                                case MarkingTypes.NewJudgingSystemV2:
                                    score = new OnScale3Score()
                                    {
                                        OfficialId = officialId,
                                        TQ = (decimal)marking.MarkA,
                                        MM = (decimal)marking.MarkB,
                                        PS = (decimal)marking.MarkC,
                                        CP = (decimal)marking.MarkD,
                                        IsSet = false
                                    };
                                    break;
                            }
                            if (score != null)
                            {
                                dance.Scores.Add(score);
                            }
                        }
                    }
                    wdsfRound.Dances.Add(dance);
                }
            }
        }

        #endregion
    }
}