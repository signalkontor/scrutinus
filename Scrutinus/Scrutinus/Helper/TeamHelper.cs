﻿// // TPS.net TPS8 Scrutinus
// // TeamHelper.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using DataModel.Models;
using System.Linq;

namespace Scrutinus.Helper
{
    public static class TeamHelper
    {
        public static bool CheckTeamsAreValid(Competition competition)
        {
            if (!competition.CompetitionType.IsFormation && !competition.CompetitionType.IsJMD)
            {
                return true;
            }

            if (competition.MinStarters == null || competition.MaxStarters == null)
            {
                return true;
            }

            foreach (var participant in competition.Participants)
            {
                if (!(participant.Couple is Team))
                {
                    MainWindow.MainWindowHandle.ShowMessageAsync($"{participant.NiceNameWithNumber} ist KEIN Team!");
                    return false;
                }

                var team = (Team)participant.Couple;

                if (competition.CompetitionType.IsFormation
                    && (team.TeamMembers.Count(p => p.Gender == Gender.Male) < competition.MinStarters.Value
                    || team.TeamMembers.Count(p => p.Gender == Gender.Female) < competition.MinStarters.Value))
                {
                    MainWindow.MainWindowHandle.ShowMessageAsync($"{team.NiceName} hat nicht genug Starter!");
                    return false;
                }
               
                if (competition.CompetitionType.IsFormation
                    && (team.TeamMembers.Count(p => p.Gender == Gender.Male) > competition.MaxStarters.Value
                    || team.TeamMembers.Count(p => p.Gender == Gender.Female) > competition.MaxStarters.Value))
                {
                    MainWindow.MainWindowHandle.ShowMessageAsync($"{team.NiceName} hat zuviele Starter!");
                    return false;
                }

                if (competition.CompetitionType.IsJMD
                    && team.TeamMembers.Count() < competition.MinStarters.Value)
                {
                    MainWindow.MainWindowHandle.ShowMessageAsync($"{team.NiceName} hat nicht genug Starter!");
                    return false;
                }

                if (competition.CompetitionType.IsJMD
                    && team.TeamMembers.Count() > competition.MaxStarters.Value)
                {
                    MainWindow.MainWindowHandle.ShowMessageAsync($"{team.NiceName} hat zuviele Starter!");
                    return false;
                }

                if (competition.MaxForeingnStarters.HasValue)
                {
                    if (team.TeamMembers.Count(t => t.Nationality != "GER") > competition.MaxForeingnStarters.Value)
                    {
                        MainWindow.MainWindowHandle.ShowMessageAsync($"{team.NiceName} hat zuviele ausländiche Starter!");
                        return false;
                    }
                }
            }

            return true;
            
        }
    }
}
