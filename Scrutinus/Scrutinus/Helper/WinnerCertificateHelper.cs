﻿// // TPS.net TPS8 Scrutinus
// // WinnerCertificateHelper.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Media;
using Scrutinus.Dialogs.DialogViewModels;

namespace Scrutinus.Helper
{
    public static class WinnerCertificateHelper
    {
        public static IEnumerable<WinnerCertificateDefinition> GetWinnerCertificates()
        {
            byte[] bytes;

            IEnumerable<WinnerCertificateDefinition> result;

            if (
                File.Exists(
                    Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\TPS.net\\Certificates.bin"))
            {
                bytes =
                    File.ReadAllBytes(
                        Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\TPS.net\\Certificates.bin");
            }
            else
            {
                bytes = Convert.FromBase64String(Settings.Default.WinnerCertificateDefinitions);
            }

            if (bytes.Length == 0)
            {
                result = new List<WinnerCertificateDefinition>();
                return result;
            }

            var stream = new MemoryStream(bytes);

            var bf = new BinaryFormatter();
            result  = (ObservableCollection<WinnerCertificateDefinition>)bf.Deserialize(stream);

            foreach (var winnerCertificateDefinition in result)
            {
                foreach (var element in winnerCertificateDefinition.Elements)
                {
                    element.FontFamily = new FontFamily(element.FontFamilyString);
                }
            }

            return result;
        }
    }
}
