﻿// // TPS.net TPS8 Scrutinus
// // PrintManagerHelper.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DataModel;
using DataModel.Models;
using DataModel.ReportModels;
using NLog;
using Scrutinus.Dialogs;
using Scrutinus.Dialogs.DialogViewModels;
using Scrutinus.Pages;
using Scrutinus.Reports;
using Scrutinus.Reports.BasicPrinting;
using Scrutinus.Reports.BasicPrinting.CompetitionPrinting;
using Scrutinus.Reports.BasicPrinting.RoundPrinting;
using LogLevel = DataModel.Models.LogLevel;

namespace Scrutinus.Helper
{
    public static class PrintManagerHelper
    {
        public static void Print(ScrutinusContext context, Competition competition, Round round, IEnumerable<PrintReportViewModel> printJobs)
        {
            var previousRound = context.Rounds.FirstOrDefault(r => r.Competition.Id == round.Competition.Id && r.Number == round.Number - 1);
            var printPage = new PrintReportPage();
            
            foreach (var printReportDefinition in printJobs.Where(p => p.Print))
            {
                AbstractPrinter printer;

                var roundToPrint = round;
                if (printReportDefinition.PrintDefinition.PrintCollectionType == PrintCollectionType.AfterQualificationRound)
                {
                    if (previousRound != null)
                    {
                        roundToPrint = previousRound;

                    }

                    if (round.RoundType.Id == RoundTypes.Final && round.Markings.Any())
                    {
                        roundToPrint = round;
                    }
                }

                if (printReportDefinition.PrintDefinition.PrintReportType.Contains(nameof(FinalSkatingTablePrinter)))
                {
                    if (round.RoundType.Id == RoundTypes.Final)
                    {
                        var grouped = roundToPrint.Qualifieds.GroupBy(q => q.Participant.Points);
                        if (grouped.All(g => g.Count() == 1))
                        {
                            continue;
                        }
                    }
                }

                if (printReportDefinition.PrintDefinition.PrintReportType.Contains(nameof(WinnerCertificatePrinter)))
                {
                    var printdlg = new CertificateDialog(round.Qualifieds.Select(q => q.Participant), roundToPrint, printReportDefinition.PrintDefinition.Printer.PrintQueueName);
                    MainWindow.MainWindowHandle.ShowDialog(printdlg, CertificateDialogClosed, 300, 300);
                    continue;
                }

                try
                {
                    printer = ConstructPrinter(printReportDefinition.PrintDefinition.PrintReportType, context, competition, roundToPrint);
                }
                catch (Exception ex)
                {
                    ScrutinusContext.WriteLogEntry(ex);
                    continue;
                }

                printPage.Print(printer, false, false, printReportDefinition.PrintDefinition.Printer.PrintQueueName, printReportDefinition.Copies, printReportDefinition.PrintDefinition.ReportName, printReportDefinition.PrintDefinition.Printer.InputBin);
            }
        }

        public static IEnumerable<PrintReportViewModel> GetPrintJobs(ScrutinusContext context,Competition competition, Round round, Round previousRound)
        {
            var result = new List<PrintReportViewModel>();

            if (round != null)
            {
                result.AddRange(GetRoundPrintJobs(context, round, previousRound));
            }

            return result;
        }

        private static void CertificateDialogClosed(object obj)
        {
            var sender = obj as CertificateDialog;

            var viewModel = sender?.DataContext as PrintCertificateDialogViewModel;

            if (viewModel?.Result == DialogResult.OK)
            {
                PrintCertificate(viewModel);
            }
        }

        private static void PrintCertificate(PrintCertificateDialogViewModel viewModel)
        {
            var certificatePrinter = new WinnerCertificatePrinter(
                viewModel.Round,
                viewModel.SelectedDefinition,
                viewModel.Participants);

            var printPage = new PrintReportPage();
            printPage.Print(certificatePrinter, false, false, viewModel.Printer, viewModel.Copies, "Certificates");
        }

        private static IEnumerable<PrintReportViewModel> GetRoundPrintJobs(ScrutinusContext context, Round round, Round previousRound)
        {
            var result = new List<PrintReportViewModel>();

            if (round.RoundType.Id == RoundTypes.Final)
            {
                if (round.Markings.Any(m => m.Mark > 0))
                {
                    result.AddRange(context.PrintReportDefinitions.Where(r => r.PrintCollectionType == PrintCollectionType.AfterFinal).Select(p => new PrintReportViewModel() {Print = true, Copies = p.Copies, PrintDefinition = p}));
                    return result; // After final we only print the after final stuff
                }

                result.AddRange(context.PrintReportDefinitions.Where(r => r.PrintCollectionType == PrintCollectionType.BeforeFinal).Select(p => new PrintReportViewModel() { Print = true, Copies = p.Copies, PrintDefinition = p }));
            }

            if (round.Number == 1 && !round.Markings.Any())
            {
                result.AddRange(context.PrintReportDefinitions.Where(r => r.PrintCollectionType == PrintCollectionType.BeforeFirstRound).Select(p => new PrintReportViewModel() { Print = true, Copies = p.Copies, PrintDefinition = p }));
            }

            if (previousRound != null)
            {
                result.AddRange(context.PrintReportDefinitions.Where(r => r.PrintCollectionType == PrintCollectionType.AfterQualificationRound).Select(p => new PrintReportViewModel() { Print = true, Copies = p.Copies, PrintDefinition = p }));
            }

            if (round.RoundType.Id == RoundTypes.QualificationRound)
            {
                result.AddRange(context.PrintReportDefinitions.Where(r => r.PrintCollectionType == PrintCollectionType.BeforeQualificationRound).Select(p => new PrintReportViewModel() { Print = true, Copies = p.Copies, PrintDefinition = p }));
            }
            return result.Distinct(new PrintReportDestinctComparer());
        }

        private static AbstractPrinter ConstructPrinter(string reportType, ScrutinusContext context, Competition competition, Round round)
        {

            var type = Type.GetType(reportType);

            if (type == null)
            {
                return null;
            }

            var construtor = type.GetConstructors().FirstOrDefault();

            if (construtor == null)
            {
                return (AbstractPrinter) Activator.CreateInstance(type);
            }

            var parameterInfos = construtor.GetParameters();
            var parameters = new List<object>();

            foreach (var parameterInfo in parameterInfos)
            {
                if (parameterInfo.ParameterType == typeof(ScrutinusContext))
                {
                    parameters.Add(context);
                }
                else if (parameterInfo.ParameterType == typeof(Competition))
                {
                    parameters.Add(competition);
                }
                else if (parameterInfo.ParameterType == typeof(Round))
                {
                    parameters.Add(round);
                }
                else if (parameterInfo.ParameterType == typeof(IEnumerable<Competition>))
                {
                    parameters.Add(new List<Competition>() { competition });
                }
                else if (parameterInfo.ParameterType == typeof(MarkingV2Model))
                {
                    parameters.Add(new MarkingV2Model());
                } else if (parameterInfo.ParameterType == typeof(System.Boolean))
                {
                    parameters.Add(true);
                }
                else
                {
                    throw new Exception("Unknown Type " + parameterInfo.ParameterType + " in PrintManagerHelper");
                }
            }

            return (AbstractPrinter)construtor.Invoke(parameters.ToArray());
        }
    }
}
