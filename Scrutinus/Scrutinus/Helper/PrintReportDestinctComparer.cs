// // TPS.net TPS8 Scrutinus
// // PrintReportDestinctComparer.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System.Collections.Generic;
using Scrutinus.Pages;

namespace Scrutinus.Helper
{
    internal class PrintReportDestinctComparer : IEqualityComparer<PrintReportViewModel>
    {
        public bool Equals(PrintReportViewModel x, PrintReportViewModel y)
        {
            if (x.PrintDefinition == null || y.PrintDefinition == null)
            {
                return false;
            }

            if (x.PrintDefinition.Printer == null || y.PrintDefinition.Printer == null)
            {
                return false;
            }

            return x.PrintDefinition.PrintReportType.Equals(y.PrintDefinition.PrintReportType) && x.PrintDefinition.Printer.Name.Equals(y.PrintDefinition.Printer.Name);
        }

        public int GetHashCode(PrintReportViewModel obj)
        {
            return (obj.PrintDefinition.PrintReportType + obj.PrintDefinition.Printer?.Name).GetHashCode();
        }
    }
}