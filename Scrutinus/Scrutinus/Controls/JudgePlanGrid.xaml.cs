﻿// // TPS.net TPS8 Scrutinus
// // JudgePlanGrid.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using DataModel;
using DataModel.Models;

namespace Scrutinus.Controls
{
    /// <summary>
    /// Interaction logic for JudgePlanGrid.xaml
    /// </summary>
    public partial class JudgePlanGrid : UserControl
    {
        public JudgePlanGrid()
        {
            this.InitializeComponent();

            this.CreateGrid();
        }

        private void CreateGrid()
        {
            var context = new ScrutinusContext();

            this.PlanGrid.RowDefinitions.Add(new RowDefinition());
            this.PlanGrid.ColumnDefinitions.Add(new ColumnDefinition());

            var column = 1;
            var competitions = context.Competitions.OrderBy(c => c.StartTime);
            foreach (var competition in competitions)
            {
                this.PlanGrid.ColumnDefinitions.Add(new ColumnDefinition());
                this.AddText(competition.Title, 0, column, 90);
                column++;
            }

            var judges = context.Officials.Where(o => o.Roles.Any(r => r.Id == Roles.Judge)).OrderBy(o => o.Sign);
            var row = 1;
            foreach (var judge in judges)
            {
                this.PlanGrid.RowDefinitions.Add(new RowDefinition());
                this.AddText(judge.NiceName, row, 0, 0);
                row++;
            }

            this.FillJudges(competitions, judges);
        }

        private void FillJudges(IEnumerable<Competition> competitons, IEnumerable<Official> judges)
        {
            var row = 1;

            foreach (var official in judges)
            {
                var column = 1;
                foreach (var competiton in competitons)
                {
                    if (competiton.GetJudges().Any(j => j.Sign == official.Sign))
                    {
                        this.AddText("Y", row, column, 0);
                    }
                    column++;
                }
                row++;
            }
        }


        private void AddText(string text, int row, int column, double rotate)
        {
            var textblock = new TextBlock() {Text = text, Margin = new Thickness(4)};
            if (rotate != 0)
            {
                textblock.LayoutTransform = new RotateTransform() {Angle = rotate};
            }
            Grid.SetColumn(textblock, column);
            Grid.SetRow(textblock, row);
            this.PlanGrid.Children.Add(textblock);
        }

        private void PlanGrid_OnMouseMove(object sender, MouseEventArgs e)
        {
            var uiElement = (UIElement)e.Source;

            int c = Grid.GetColumn(uiElement);
            int r = Grid.GetRow(uiElement);

            var elements = PlanGrid.Children.OfType<UIElement>().Where(ui => Grid.GetRow(ui) == r);

            foreach (var element in elements)
            {
                Debug.WriteLine(element);
            }
        }
    }
}
