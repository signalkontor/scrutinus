﻿// // TPS.net TPS8 Scrutinus
// // HeatMarking.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows.Controls;

namespace Scrutinus.Controls
{
    /// <summary>
    ///     Interaction logic for HeatMarking.xaml
    /// </summary>
    public partial class HeatMarking : UserControl
    {
        #region Constructors and Destructors

        public HeatMarking()
        {
            this.InitializeComponent();
        }

        #endregion
    }
}