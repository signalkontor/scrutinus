﻿// // TPS.net TPS8 Scrutinus
// // AssignJudgesGridControl.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using DataModel;
using DataModel.Models;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Scrutinus.Localization;

namespace Scrutinus.Controls
{
    public class AssignJudgesSumsViewModel : ViewModelBase
    {
        #region Fields

        private int totalCompetitions;

        #endregion

        #region Public Properties

        public Official Judge { get; set; }

        public Role Role { get; set; }

        public int TotalCompetitions
        {
            get
            {
                return this.totalCompetitions;
            }
            set
            {
                this.totalCompetitions = value;
                this.RaisePropertyChanged(() => this.TotalCompetitions);
            }
        }

        #endregion
    }

    public class AssignCompetitionsSumsViewModel : ViewModelBase
    {
        #region Fields

        private int totalJudges;

        #endregion

        #region Public Properties

        public Competition Competition { get; set; }

        public int TotalJudges
        {
            get
            {
                return this.totalJudges;
            }
            set
            {
                this.totalJudges = value;
                this.RaisePropertyChanged(() => this.TotalJudges);
            }
        }

        #endregion
    }

    public class AssignJudgesViewModel : ViewModelBase
    {
        #region Fields

        private bool isSelected;

        #endregion

        #region Public Properties

        public Competition Competition { get; set; }

        public bool IsSelected
        {
            get
            {
                return this.isSelected;
            }
            set
            {
                this.isSelected = value;
                this.RaisePropertyChanged(() => this.IsSelected);
            }
        }

        public Official Judge { get; set; }

        public bool OriginalValue { get; set; }

        public Role Role { get; set; }

        #endregion
    }

    public class AssignJudgesGridControl : UserControl
    {
        #region Constructors and Destructors

        public AssignJudgesGridControl()
        {
            this.context = new ScrutinusContext();
            this.viewModel = new List<AssignJudgesViewModel>();
            this.sumsPerCompetition = new List<AssignCompetitionsSumsViewModel>();
            this.sumsPerJudge = new List<AssignJudgesSumsViewModel>();

            this.SaveCommand = new RelayCommand(this.SaveData);

            this.SetupGrid();
        }

        #endregion

        #region Public Properties

        public ICommand SaveCommand { get; set; }

        #endregion

        #region Fields

        private readonly ScrutinusContext context;

        private readonly List<AssignCompetitionsSumsViewModel> sumsPerCompetition;

        private readonly List<AssignJudgesSumsViewModel> sumsPerJudge;

        private readonly List<AssignJudgesViewModel> viewModel;

        private int column;

        #endregion

        #region Methods

        private void AddControlToGrid(Grid grid, int row, int column, UIElement uiElement)
        {
            grid.Children.Add(uiElement);
            Grid.SetColumn(uiElement, column);
            Grid.SetRow(uiElement, row);
        }

        private void HighlightRowAndColumn(Tuple<int, int> cell, Brush brush)
        {
            var grid = (Grid)this.Content;

            var border =
                (Label)
                grid.Children.Cast<UIElement>().First(ui => Grid.GetRow(ui) == cell.Item1 && Grid.GetColumn(ui) == 0);
            border.Background = brush;

            border =
                (Label)
                grid.Children.Cast<UIElement>().First(ui => Grid.GetRow(ui) == 0 && Grid.GetColumn(ui) == cell.Item2);
            border.Background = brush;

            border =
                (Label)
                grid.Children.Cast<UIElement>().First(ui => Grid.GetRow(ui) == 1 && Grid.GetColumn(ui) == cell.Item2);
            border.Background = brush;
        }


        private void SaveData()
        {
            foreach (var source in this.viewModel.Where(s => s.Competition.IsCompetitionNotStarted))
            {
                if (source.IsSelected)
                {
                    // Add the judge to the competition
                    var existing =
                        source.Competition.Officials.FirstOrDefault(
                            o =>
                            o.Official.Id == source.Judge.Id && o.Competition.Id == source.Competition.Id
                            && o.Role == source.Role);

                    if (existing == null)
                    {
                        source.Competition.Officials.Add(
                            new OfficialInCompetition
                                {
                                    Official = source.Judge,
                                    Competition = source.Competition,
                                    Role = source.Role
                                });
                    }
                }
                else
                {
                    var official =
                        source.Competition.Officials.SingleOrDefault(o => o.Official.Id == source.Judge.Id);
                    if (official != null)
                    {
                        source.Competition.Officials.Remove(official);
                        this.context.OfficialCompetition.Remove(official);
                    }
                }

                this.context.SaveChanges();
            }
        }

        private void SetupCheckboxes(
            Grid grid,
            IEnumerable<AssignJudgesViewModel> judges,
            IEnumerable<Competition> competitions)
        {
            var row = 2;
            var column = 1;

            foreach (var competition in competitions)
            {
                column = 1;
                foreach (var official in judges)
                {
                    var dataSource = new AssignJudgesViewModel
                                         {
                                             Competition = competition,
                                             Judge = official.Judge,
                                             IsSelected =
                                                 competition.Officials.Any(
                                                     j =>
                                                     j.Official.Id == official.Judge.Id
                                                     && j.Role.Id == official.Role.Id),
                                             Role = official.Role
                                         };

                    // var isSelected = competition.Officials.Any(j => j.Official.Id == official.Judge.Id && j.Role.Id == official.Role.Id);

                    dataSource.PropertyChanged += (sender, args) =>
                        {
                            if (args.PropertyName != "IsSelected")
                            {
                                return;
                            }

                            var viewModel = sender as AssignJudgesViewModel;
                            var sumJudge =
                                this.sumsPerJudge.Single(j => j.Judge.Id == viewModel.Judge.Id && j.Role.Id == viewModel.Role.Id);

                            var sumCompetition =
                                this.sumsPerCompetition.Single(c => c.Competition.Id == viewModel.Competition.Id);

                            if (viewModel.IsSelected)
                            {
                                sumJudge.TotalCompetitions++;
                                sumCompetition.TotalJudges++;
                            }
                            else
                            {
                                sumJudge.TotalCompetitions--;
                                sumCompetition.TotalJudges--;
                            }
                        };

                    this.viewModel.Add(dataSource);

                    var checkbox = new CheckBox() {VerticalAlignment = VerticalAlignment.Center, HorizontalAlignment = HorizontalAlignment.Center , IsEnabled = competition.IsCompetitionNotStarted};
                    checkbox.Tag = new Tuple<int, int>(row, column);

                    var border = new Border();
                    border.Child = checkbox;

                    checkbox.MouseEnter += (sender, args) =>
                        {
                            if (checkbox.IsMouseDirectlyOver)
                            {
                                var ch = (CheckBox)sender;
                                this.HighlightRowAndColumn((Tuple<int, int>)ch.Tag, Brushes.Red);
                            }
                            else
                            {
                                var ch = (CheckBox)sender;
                                this.HighlightRowAndColumn((Tuple<int, int>)ch.Tag, Brushes.Transparent);
                            }
                        };

                    checkbox.MouseLeave += (sender, args) =>
                        {
                            var ch = (CheckBox)sender;
                            this.HighlightRowAndColumn((Tuple<int, int>)ch.Tag, Brushes.Transparent);
                        };

                    var binding = new Binding("IsSelected") { Source = dataSource, Mode = BindingMode.TwoWay };

                    checkbox.SetBinding(ToggleButton.IsCheckedProperty, binding);

                    this.AddControlToGrid(grid, row, column, checkbox);

                    var sumPerJudge =
                        this.sumsPerJudge.SingleOrDefault(s => s.Judge.Id == official.Judge.Id && s.Role.Id == official.Role.Id);

                    if (sumPerJudge == null)
                    {
                        sumPerJudge = new AssignJudgesSumsViewModel { Judge = official.Judge, TotalCompetitions = 0, Role = official.Role};
                        this.sumsPerJudge.Add(sumPerJudge);
                        var label = new Label();
                        var sumBinding = new Binding("TotalCompetitions")
                                             {
                                                 Source = sumPerJudge,
                                                 Mode = BindingMode.TwoWay
                                             };
                        label.SetBinding(ContentProperty, sumBinding);
                        this.AddControlToGrid(grid, competitions.Count() + 2, column, label);
                    }

                    var sumPerCompetition =
                        this.sumsPerCompetition.SingleOrDefault(s => s.Competition.Id == competition.Id);
                    if (sumPerCompetition == null)
                    {
                        sumPerCompetition = new AssignCompetitionsSumsViewModel
                                                {
                                                    Competition = competition,
                                                    TotalJudges = 0
                                                };
                        this.sumsPerCompetition.Add(sumPerCompetition);
                        var label = new Label();
                        var sumBinding = new Binding("TotalJudges")
                                             {
                                                 Source = sumPerCompetition,
                                                 Mode = BindingMode.TwoWay
                                             };
                        label.SetBinding(ContentProperty, sumBinding);
                        this.AddControlToGrid(grid, row, judges.Count() + 1, label);
                    }

                    if (dataSource.IsSelected)
                    {
                        sumPerJudge.TotalCompetitions++;
                        sumPerCompetition.TotalJudges++;
                    }

                    column++;
                }

                row++;
            }
        }

        private void SetupCompetitions(Grid grid, List<Competition> competitions)
        {
            var row = 2;

            foreach (var competition in competitions)
            {
                var label = new Label { Content = competition.Title, FontSize = 15 };
                this.AddControlToGrid(grid, row, 0, label);
                row++;
            }
        }

        private void SetupGrid()
        {
            var judges = this.context.Officials.Where(o => o.Roles.Any(r => r.Id == Roles.Judge)).ToList();
            var chairmans =
                this.context.Officials.Where(o => o.Roles.Any(r => r.Id == Roles.Chairman)).ToList();
            var associates =
                this.context.Officials.Where(o => o.Roles.Any(r => r.Id == Roles.Associate)).ToList();
            var supervisors =
                this.context.Officials.Where(o => o.Roles.Any(r => r.Id == Roles.Supervisor)).ToList();
            var scrutineers =
                this.context.Officials.Where(o => o.Roles.Any(r => r.Id == Roles.Scrutineer)).ToList();
            var competitions =
                this.context.Competitions.ToList().OrderBy(c => c.StartTime).ToList();

            var columnCount = this.context.Officials.ToList().Sum(o => o.Roles.Count) + 2;

            var grid = new Grid();
            grid.ShowGridLines = true;
            
            // We rows for competitions and columns for judges
            for (var row = 0; row < competitions.Count + 4; row++)
            {
                grid.RowDefinitions.Add(new RowDefinition());
            }

            for (var column = 0; column < columnCount; column++)
            {
                grid.ColumnDefinitions.Add(new ColumnDefinition());
            }

            var button = new Button
                             {
                                 Content = LocalizationService.Resolve(() => Text.Save),
                                 Command = this.SaveCommand,
                                 Height = 40,
                                 MinWidth = 100,
                                 Margin = new Thickness(0, 20, 0, 0)
                             };
            this.AddControlToGrid(grid, competitions.Count + 3, 0, button);

            this.column = 1;
            this.SetupJudges(grid, judges, LocalizationService.Resolve(() => Text.RoleShortAdjudicator));
            this.SetupJudges(grid, chairmans, LocalizationService.Resolve(() => Text.RoleShortChairman));
            this.SetupJudges(grid, associates, LocalizationService.Resolve(() => Text.RoleShortAssociate));
            this.SetupJudges(grid, supervisors, LocalizationService.Resolve(() => Text.RoleShortSupervisor));
            this.SetupJudges(grid, scrutineers, LocalizationService.Resolve(() => Text.RoleShortScrutineer));

            this.SetupCompetitions(grid, competitions);

            var roleJudge = this.context.Roles.Single(r => r.Id == Roles.Judge);
            var roleChairman = this.context.Roles.Single(r => r.Id == Roles.Chairman);
            var roleAssociate = this.context.Roles.Single(r => r.Id == Roles.Associate);
            var roleSupervisor = this.context.Roles.Single(r => r.Id == Roles.Supervisor);
            var roleScrutineer = this.context.Roles.Single(r => r.Id == Roles.Scrutineer);

            var allOfficials = new List<AssignJudgesViewModel>();
            allOfficials.AddRange(judges.Select(s => new AssignJudgesViewModel { Judge = s, Role = roleJudge }));
            allOfficials.AddRange(chairmans.Select(s => new AssignJudgesViewModel { Judge = s, Role = roleChairman }));
            allOfficials.AddRange(associates.Select(s => new AssignJudgesViewModel { Judge = s, Role = roleAssociate }));
            allOfficials.AddRange(
                supervisors.Select(s => new AssignJudgesViewModel { Judge = s, Role = roleSupervisor }));
            allOfficials.AddRange(
                scrutineers.Select(s => new AssignJudgesViewModel { Judge = s, Role = roleScrutineer }));

            this.SetupCheckboxes(grid, allOfficials, competitions);

            this.Content = grid;
        }

        private void SetupJudges(Grid grid, IEnumerable<Official> judges, string role)
        {
            foreach (var official in judges.OrderBy(j => j.Sign))
            {
                var textblock = new Label
                                    {
                                        Content = official.Sign,
                                        FontSize = 15,
                                        VerticalAlignment = VerticalAlignment.Center
                                    };
                this.AddControlToGrid(grid, 1, this.column, textblock);

                textblock = new Label
                                {
                                    Content = string.Format("{0} [{1}]", official.NiceName, role),
                                    FontSize = 12,
                                    LayoutTransform = new RotateTransform(270),
                                    VerticalAlignment = VerticalAlignment.Center
                                };
                this.AddControlToGrid(grid, 0, this.column, textblock);

                this.column++;
            }
        }

        #endregion
    }
}