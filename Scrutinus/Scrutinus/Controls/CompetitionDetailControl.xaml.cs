﻿// // TPS.net TPS8 Scrutinus
// // CompetitionDetailControl.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using DataModel;
using DataModel.Models;
using DataModel.Rules;
using GalaSoft.MvvmLight.Command;
using General.Extensions;
using Scrutinus.Dialogs.DialogViewModels;
using Scrutinus.Helper;
using Scrutinus.ViewModels;

namespace Scrutinus.Controls
{
    /// <summary>
    ///     Interaction logic for CompetitionDetailControl.xaml
    /// </summary>
    public partial class CompetitionDetailControl : UserControl, INotifyPropertyChanged
    {
        #region Constructors and Destructors

        public CompetitionDetailControl()
        {
            this.AllDances = new ObservableCollection<Dance>();
            this.AllJudges = new ObservableCollection<Official>();
            this.AllOfficials = new ObservableCollection<OfficialInCompetition>();
            this.SelectedDances = new ObservableCollection<Dance>();
            this.Sections = new ObservableCollection<Section>();
            this.CompetitionTypes = new ObservableCollection<CompetitionType>();
            this.AgeGroups = new ObservableCollection<AgeGroup>();

            this.Stars = ComboBoxItemSources.GetStarTypeList();
            this.FirstRoundTypes = ComboBoxItemSources.GetFirstRoundType();
            this.FinalTypes = ComboBoxItemSources.GetMarkingTypeList();
            this.MarkingTypes = ComboBoxItemSources.GetMarkingTypeList();
            this.WinnerCertificates = WinnerCertificateHelper.GetWinnerCertificates();

            this.InitializeComponent();

            this.SelectedJudges = this.JudgesList.SelectedItems;
            this.SelectedOfficials = this.OfficialsList.SelectedItems;

            this.AddDanceCommand = new RelayCommand(this.AddDance);
            this.DeleteDanceCommand = new RelayCommand(this.DeleteDance);

            this.SaveCommand = new RelayCommand(this.SaveCompetition);

            this.DataChanged = false;
        }

        #endregion

        #region Public Events

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Static Fields

        public static readonly DependencyProperty CompetitionProperty = DependencyProperty.Register(
            "Competition",
            typeof(Competition),
            typeof(CompetitionDetailControl),
            new PropertyMetadata(OnCompetitionChanged));

        public static readonly DependencyProperty DatabaseContextProperty =
            DependencyProperty.Register(
                "DatabaseContext",
                typeof(ScrutinusContext),
                typeof(CompetitionDetailControl),
                new PropertyMetadata(OnDatabaseContextChanged));

        public static readonly DependencyProperty SaveCommandProperty = DependencyProperty.Register(
            "SaveCommand",
            typeof(ICommand),
            typeof(CompetitionDetailControl),
            new PropertyMetadata(OnSaveCommandChanged));

        #endregion

        #region Fields

        protected Competition currentCompetition = null;

        private bool dataChanged;

        private bool suspendNotifyChangedEvent = false;

        #endregion

        #region Public Properties

        public ICommand AddDanceCommand { get; set; }

        public ObservableCollection<AgeGroup> AgeGroups { get; set; }

        public ObservableCollection<Dance> AllDances { get; set; }

        public ObservableCollection<Official> AllJudges { get; set; }

        public ObservableCollection<OfficialInCompetition> AllOfficials { get; set; }

        public Competition Competition
        {
            get
            {
                return (Competition)this.GetValue(CompetitionProperty);
            }

            set
            {
                this.SetValue(CompetitionProperty, value);
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged(this, new PropertyChangedEventArgs("Competition"));
                }
            }
        }

        public ObservableCollection<CompetitionType> CompetitionTypes { get; set; }

        public bool DataChanged
        {
            get
            {
                return this.dataChanged;
            }
            set
            {
                this.dataChanged = value;
            }
        }

        public ScrutinusContext DatabaseContext
        {
            get
            {
                return (ScrutinusContext)this.GetValue(DatabaseContextProperty);
            }
            set
            {
                this.SetValue(DatabaseContextProperty, value);
            }
        }

        public ICommand DeleteDanceCommand { get; set; }

        public ICommand DownDanceCommand { get; set; }

        public IEnumerable<MarkingTypeItemValue> FinalTypes { get; set; }

        public IEnumerable<ItemValue> FirstRoundTypes { get; set; }

        public IEnumerable<MarkingTypeItemValue> MarkingTypes { get; set; }

        public Action OnDataSaved { get; set; }

        public ICommand SaveCommand
        {
            get
            {
                return (ICommand)this.GetValue(SaveCommandProperty);
            }

            set
            {
                this.SetValue(SaveCommandProperty, value);
            }
        }

        public ObservableCollection<Section> Sections { get; set; }

        public Dance SelectedDanceInList { get; set; }

        public Dance SelectedDanceToAdd { get; set; }

        public ObservableCollection<Dance> SelectedDances { get; set; }

        public IList SelectedJudges { get; set; }

        public IList SelectedOfficials { get; set; }

        public IEnumerable<ItemValue> Stars { get; set; }

        public ICommand UpDanceCommand { get; set; }

        public IEnumerable<WinnerCertificateDefinition> WinnerCertificates { get; set; }

        #endregion

        #region Methods

        private static void OnCompetitionChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = d as CompetitionDetailControl;
            if (control == null)
            {
                return;
            }

            control.OnCompetitionChanged();
        }

        private static void OnDatabaseContextChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = d as CompetitionDetailControl;
            if (control == null)
            {
                return;
            }

            control.SetupLists();
            control.OnCompetitionChanged();
        }

        private static void OnSaveCommandChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = d as CompetitionDetailControl;
            if (control == null)
            {
                return;
            }
            if (e.NewValue == null)
            {
                control.SaveCommand = new RelayCommand(control.SaveCompetition);
            }
        }

        private void AddDance()
        {
            if (this.SelectedDanceToAdd != null)
            {
                this.SelectedDances.Add(this.SelectedDanceToAdd);
            }
        }

        private void DeleteDance()
        {
            if (this.SelectedDanceInList != null)
            {
                this.SelectedDances.Remove(this.SelectedDanceInList);
            }
        }

        private void DownDance()
        {
            if (this.SelectedDanceInList == null)
            {
                return;
            }

            var index = this.SelectedDances.IndexOf(this.SelectedDanceInList);

            if (index == this.SelectedDances.Count - 1)
            {
                return;
            }

            var element = this.SelectedDanceInList;
            this.SelectedDances.RemoveAt(index);
            this.SelectedDances.Insert(index + 1, element);

            this.SelectedDanceInList = element;
        }

        private void OnClickButtonAdd(object sender, RoutedEventArgs e)
        {
            this.AddDance();
        }

        private void OnClickButtonDelete(object sender, RoutedEventArgs e)
        {
            this.DeleteDance();
        }

        private void OnClickButtonDown(object sender, RoutedEventArgs e)
        {
            this.DownDance();
        }

        private void OnClickButtonUp(object sender, RoutedEventArgs e)
        {
            this.UpDance();
        }

        private void OnCompetitionChanged()
        {
            if (this.DatabaseContext == null)
            {
                return;
            }

            if (this.currentCompetition != null)
            {
                this.currentCompetition.PropertyChanged -= this.OnCompetitionPropertyChanged;
                this.SaveCompetition();
            }

            this.currentCompetition = this.Competition;

            if (this.Competition == null)
            {
                return;
            }

            this.currentCompetition.PropertyChanged += this.OnCompetitionPropertyChanged;

            // No dances yet? try tho set them...
            if (!this.currentCompetition.Dances.Any())
            {
                this.ResetDances();
            }

            // Setup the list of selected judges, etc:
            this.SelectedJudges.Clear();
            foreach (var official in this.Competition.GetJudges())
            {
                var existing = this.AllJudges.FirstOrDefault(o => o.Id == official.Id);
                if (existing == null)
                {
                    // Someone removed his judges sign but not from this
                    // competition
                    this.AllJudges.Add(official);
                }
                this.SelectedJudges.Add(official);
            }

            this.PropertyChanged(this, new PropertyChangedEventArgs("SelectedJudges"));

            // Setup the list of selected officials:
            this.SelectedOfficials.Clear();
            foreach (var official in this.Competition.Officials.Where(o => o.Role.Id != Roles.Judge))
            {
                var entry =
                    this.AllOfficials.FirstOrDefault(
                        o => o.Official.Id == official.Official.Id && o.Role.Id == official.Role.Id);

                this.SelectedOfficials.Add(entry);
            }
            this.PropertyChanged(this, new PropertyChangedEventArgs("SelectedOfficials"));

            this.SelectedDances.Clear();
            this.SelectedDances.AddRange(
                this.Competition.Dances.OrderBy(d => d.DanceOrder).Select(d => d.Dance).ToList());

            this.PropertyChanged(this, new PropertyChangedEventArgs("SelectedDances"));
        }

        private void OnCompetitionPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Class" || e.PropertyName == "Section" || e.PropertyName == "AgeGroup")
            {
                this.ResetDances();
                // If we have participants, we have to update the age group and class
                foreach (var participant in this.Competition.Participants)
                {
                    participant.AgeGroup = this.Competition.AgeGroup;
                    participant.Class = this.Competition.Class;
                }
            }
            if (e.PropertyName == this.PropertyName(() => this.Competition.CurrentRound) || e.PropertyName == this.PropertyName(() => this.Competition.IsCompetitionNotStarted))
            {
                return;
            }

            this.DataChanged = true;
        }

        private void RaisePropertyChanged(string propertyName)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void ResetDances()
        {
            if (this.Competition == null || this.Competition.Section == null || this.Competition.AgeGroup == null
                || this.Competition.Class == null)
            {
                return;
            }

            var rules = new DanceRules(this.DatabaseContext);

            foreach (var dance in this.Competition.Dances.ToList())
            {
                this.DatabaseContext.DancesCompetition.Remove(dance);
            }
            this.Competition.Dances.Clear();
            this.DatabaseContext.SaveChanges();

            // Now add the one we just calculated
            var dances = rules.GetDances(
                this.Competition.Section.Id,
                this.Competition.AgeGroup.Id,
                this.Competition.Class.Id);

            var i = 1;
            foreach (var dance in dances)
            {
                var danceInCompetition = new DancesInCompetition
                                             {
                                                 Competition = this.Competition,
                                                 Dance =
                                                     this.DatabaseContext.Dances.Single(
                                                         d => d.Id == dance.Id),
                                                 IsSolo = false,
                                                 DanceOrder = i
                                             };

                this.Competition.Dances.Add(danceInCompetition);
                this.DatabaseContext.DancesCompetition.Add(danceInCompetition);
                i++;
            }

            this.SelectedDances.Clear();
            this.SelectedDances.AddRange(dances);

            this.DatabaseContext.SaveChanges();
            this.RaisePropertyChanged("Competition");
        }

        private void SaveCompetition()
        {
            if (this.currentCompetition == null)
            {
                return;
            }

            this.DatabaseContext.SaveChanges();
            // todo: Save the values of the old competition
            foreach (var officialInCompetition in this.currentCompetition.Officials.ToList())
            {
                this.DatabaseContext.OfficialCompetition.Remove(officialInCompetition);
            }

            if (this.DatabaseContext.Competitions.SingleOrDefault(c => c.Id == this.currentCompetition.Id) == null)
            {
                // the current competition was deleted, so we set it to null and exit
                this.currentCompetition = null;
                return;
            }

            this.currentCompetition.Officials.Clear();
            this.DatabaseContext.SaveChanges();

            foreach (var officialInCompetition in this.SelectedOfficials.Cast<OfficialInCompetition>()
                )
            {
                this.currentCompetition.Officials.Add(
                    new OfficialInCompetition
                        {
                            Competition =
                                this.DatabaseContext.Competitions.Single(
                                    c => c.Id == this.currentCompetition.Id),
                            Official =
                                this.DatabaseContext.Officials.Single(
                                    o => o.Id == officialInCompetition.Official.Id),
                            Role =
                                this.DatabaseContext.Roles.Single(
                                    r => r.Id == officialInCompetition.Role.Id),
                        });
            }

            this.DatabaseContext.SaveChanges();

            foreach (var official in this.SelectedJudges.Cast<Official>())
            {
                this.currentCompetition.Officials.Add(
                    new OfficialInCompetition
                        {
                            Competition =
                                this.DatabaseContext.Competitions.Single(
                                    c => c.Id == this.currentCompetition.Id),
                            Official =
                                this.DatabaseContext.Officials.Single(o => o.Id == official.Id),
                            Role = this.DatabaseContext.Roles.Single(r => r.Id == Roles.Judge),
                        });
            }
            // Save the current values
            this.DatabaseContext.SaveChanges();

            foreach (var dance in this.currentCompetition.Dances.ToList())
            {
                this.DatabaseContext.DancesCompetition.Remove(dance);
            }

            this.currentCompetition.Dances.Clear();
            this.DatabaseContext.SaveChanges();

            var order = 1;
            foreach (var selectedDance in this.SelectedDances)
            {
                this.currentCompetition.Dances.Add(
                    new DancesInCompetition
                        {
                            Dance = selectedDance,
                            Competition = this.currentCompetition,
                            DanceOrder = order
                        });
                order++;
            }
            this.DatabaseContext.SaveChanges();

            this.dataChanged = false;
        }

        private void SetupLists()
        {
            var context = this.DatabaseContext;

            // Setup all Judges of the event
            this.AllJudges.AddRange(context.Officials.Where(o => o.Roles.Any(r => r.Id == Roles.Judge)).OrderBy(o =>o.Sign));

            // Setup all other officials of the event
            var otherOfficials = context.Officials.Where(o => o.Roles.Any(r => r.Id != Roles.Judge));

            foreach (var otherOfficial in otherOfficials)
            {
                foreach (var role in otherOfficial.Roles.Where(r => r.Id != Roles.Judge))
                {
                    this.AllOfficials.Add(
                        new OfficialInCompetition
                            {
                                Competition = this.Competition,
                                Official = otherOfficial,
                                Role = role
                            });
                }
            }

            // setup the dances:
            this.AllDances.AddRange(context.Dances);

            this.Sections.AddRange(context.Sections);

            this.AgeGroups.AddRange(context.AgeGroups);

            this.CompetitionTypes.AddRange(context.CompetitionTypes);

            this.OnCompetitionChanged();
        }

        private void UpDance()
        {
            if (this.SelectedDanceInList == null)
            {
                return;
            }

            var index = this.SelectedDances.IndexOf(this.SelectedDanceInList);

            if (index == 0)
            {
                return;
            }

            var element = this.SelectedDanceInList;
            this.SelectedDances.RemoveAt(index);
            this.SelectedDances.Insert(index - 1, element);

            this.SelectedDanceInList = element;
        }

        #endregion
    }
}