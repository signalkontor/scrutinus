﻿// // TPS.net TPS8 Scrutinus
// // PathSelector.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using UserControl = System.Windows.Controls.UserControl;

namespace Scrutinus.Controls
{
    /// <summary>
    ///     Interaction logic for FileSelector.xaml
    /// </summary>
    public partial class PathSelector : UserControl
    {
        #region Static Fields

        public static DependencyProperty SelectedPathProperty = DependencyProperty.Register(
            "SelectedPath",
            typeof(string),
            typeof(PathSelector),
            new PropertyMetadata(""));

        #endregion

        #region Constructors and Destructors

        public PathSelector()
        {
            this.DirectoryOpenCommand = new RelayCommand(this.FileOpen);

            this.InitializeComponent();
        }

        #endregion

        #region Methods

        private void FileOpen()
        {
            var openFileDialog = new FolderBrowserDialog { ShowNewFolderButton = true };

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                this.SelectedPath = openFileDialog.SelectedPath;
            }
        }

        #endregion

        #region Public Properties

        public ICommand DirectoryOpenCommand { get; set; }

        public string SelectedPath
        {
            get
            {
                return (string)this.GetValue(SelectedPathProperty);
            }
            set
            {
                this.SetValue(SelectedPathProperty, value);
            }
        }

        #endregion
    }
}