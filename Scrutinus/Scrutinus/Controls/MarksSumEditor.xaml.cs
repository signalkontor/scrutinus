﻿// // TPS.net TPS8 Scrutinus
// // MarksSumEditor.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;
using DataModel.Models;
using Scrutinus.ViewModels;

namespace Scrutinus.Controls
{
    /// <summary>
    ///     Interaction logic for MarksSumEditor.xaml
    /// </summary>
    public partial class MarksSumEditor : UserControl
    {
        #region Constructors and Destructors

        public MarksSumEditor(IEnumerable<Marking> marking, int numberOfDances, CalculationMode calculationMode, bool isLastBlockForInput)
        {
            this.InitializeComponent();

            this.calculationMode = calculationMode;

            this.isLastBlockForInput = isLastBlockForInput;

            this.markings = marking;

            this.numberOfDances = numberOfDances;

            this.BuildControl();
        }

        #endregion

        #region Fields

        private readonly IEnumerable<Marking> markings;

        private readonly int numberOfDances;

        private readonly CalculationMode calculationMode;

        private TextBox firstTextBox = null;

        private bool navigatingOnEnter = false;

        private readonly bool isLastBlockForInput;

        #endregion

        #region Methods

        public Action<MarksSumEditor> OnLastBoxLostFocus { get; set; }

        private void BuildControl()
        {
            var column = 1;

            foreach (var marking in this.markings.OrderBy(m => m.Participant.Number))
            {
                this.MarkingGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(30) });

                var textblock = new TextBlock { Text = marking.Participant.Number.ToString() };
                textblock.HorizontalAlignment = HorizontalAlignment.Center;
                this.MarkingGrid.Children.Add(textblock);
                Grid.SetRow(textblock, 0);
                Grid.SetColumn(textblock, column);

                var textbox = new TextBox();
                textbox.TextChanged += this.OnTextBoxTextChanged;
                textbox.GotKeyboardFocus += this.TextboxOnGotKeyboardFocus;
                var binding = new Binding(this.calculationMode == CalculationMode.Marks ? "Mark" : "MarkA") { Source = marking };
                textbox.SetBinding(TextBox.TextProperty, binding);
                this.MarkingGrid.Children.Add(textbox);
                Grid.SetColumn(textbox, column);
                Grid.SetRow(textbox, 1);

                if (column == this.markings.Count())
                {
                    textbox.LostFocus += this.TextboxOnLostFocus;
                    textbox.PreviewKeyDown += this.TextboxOnPreviewKeyDown;
                }

                if (this.firstTextBox == null)
                {
                    this.firstTextBox = textbox;
                }

                column++;
            }
        }

        private void TextboxOnPreviewKeyDown(object sender, KeyEventArgs keyEventArgs)
        {
            if (keyEventArgs.Key == Key.Enter && this.isLastBlockForInput)
            {
                this.navigatingOnEnter = true;
                this.OnLastBoxLostFocus?.Invoke(this);
            }
        }


        public void SetFocus(TextBox textbox)
        {
            if (textbox == null)
            {
                textbox = this.firstTextBox;
            }

            this.firstTextBox.Dispatcher.BeginInvoke(new Action<UIElement>(x =>
            {
                x.Focus();
            }), DispatcherPriority.ApplicationIdle, textbox);
        }

        private void TextboxOnLostFocus(object sender, RoutedEventArgs routedEventArgs)
        {
            var textbox = sender as TextBox;

            if (!string.IsNullOrEmpty(textbox?.Text) && !this.navigatingOnEnter && this.isLastBlockForInput)
            {
                // we cancel the lost focus
                this.SetFocus(textbox);
            }
        }

        private void OnTextBoxTextChanged(object sender, TextChangedEventArgs textChangedEventArgs)
        {
            var textbox = sender as TextBox;

            if (this.calculationMode == CalculationMode.Marks)
            {
                this.CheckValueMarks(textbox);
            }
            else
            {
                this.CheckValuePoints(textbox);
            }
        }

        private void CheckValueMarks(TextBox textbox)
        {
            int value;

            if (Int32.TryParse(textbox.Text, out value))
            {
                if (value > this.numberOfDances || value < 0)
                {
                    textbox.Text = "";
                }

                var maxCharacters = this.numberOfDances.ToString().Length;

                if (textbox.Text.Length == maxCharacters)
                {
                    // goto next tabstop ...
                    var request = new TraversalRequest(FocusNavigationDirection.Next);

                    // Gets the element with keyboard focus.
                    var elementWithFocus = Keyboard.FocusedElement as UIElement;

                    // Change keyboard focus.
                    if (elementWithFocus != null)
                    {
                        elementWithFocus.MoveFocus(request);
                    }
                }
            }
        }

        private void CheckValuePoints(TextBox textbox)
        {
            double value;

            if (double.TryParse(textbox.Text, out value))
            {
                if (value > 99.99 || value < 0)
                {
                    textbox.Text = "";
                }
            }
        }

        private void TextboxOnGotKeyboardFocus(
            object sender,
            KeyboardFocusChangedEventArgs keyboardFocusChangedEventArgs)
        {
            var textbox = sender as TextBox;

            textbox.SelectAll();
        }

        #endregion
    }
}