﻿// // TPS.net TPS8 Scrutinus
// // OfficialMatrixControl.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using DataModel;
using DataModel.Models;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using NancyServer.ViewModels;
using Scrutinus.Converters;
using Scrutinus.Localization;

namespace Scrutinus.Controls
{
    public class OfficalViewModel : ViewModelBase
    {
        private bool isAssigned;

        private int sum;

        public Official Offical { get; set; }

        public Role Role { get; set; }

        public bool IsAssigned
        {
            get
            {
                return this.isAssigned;
            }
            set
            {
                this.isAssigned = value;
                this.RaisePropertyChanged(() => this.IsAssigned);
            }
        }

        public int Sum
        {
            get
            {
                return this.sum;
            }
            set
            {
                this.sum = value;
                this.RaisePropertyChanged(() => this.Sum);
            }
        }
    }

    public class RowViewModel : ViewModelBase
    {
        private int sum;

        public RowViewModel()
        {
            this.OfficialViewModels = new ObservableCollection<OfficalViewModel>();
            this.OfficialViewModels.CollectionChanged += this.OfficialViewModels_CollectionChanged;
        }

        public Competition Competition { get; set; }

        public ObservableCollection<OfficalViewModel> OfficialViewModels { get; set; }

        public bool AssignmentOk { get; set; }

        public int Sum
        {
            get
            {
                return this.sum;
            }
            set
            {
                this.sum = value;
                this.RaisePropertyChanged(() => this.Sum);
            }
        }

        private bool isSelected;

        public bool IsSelected
        {
            get { return this.isSelected; }
            set
            {
                this.isSelected = value;
                this.RaisePropertyChanged(() => IsSelected);
            }
        }

        private void OfficialViewModels_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            foreach (var item in e.NewItems)
            {
                ((OfficalViewModel)item).PropertyChanged += (obj, args) =>
                    {
                        if (args.PropertyName == "IsAssigned")
                        {
                            this.Sum = this.OfficialViewModels.Count(o => o.IsAssigned);
                        }
                    };
            }
        }
    }

    /// <summary>
    /// Interaction logic for OfficialMatrixControl.xaml
    /// </summary>
    public partial class OfficialMatrixControl : UserControl
    {
        private List<OfficalViewModel> allOfficalViewModels;
        private readonly ScrutinusContext context;

        public OfficialMatrixControl()
        {
            this.context = new ScrutinusContext();

            this.SetupViewModel();

            this.SaveCommand = new RelayCommand(this.Save);

            this.InitializeComponent();

            // this.OfficialsMatrix.RowStyleSelector = new OfficialMatrixRowStyleSelector(this.FindResource);

            this.CreateControl();        
        }

        public ObservableCollection<RowViewModel> DataRows { get; set; }

        public ICommand SaveCommand { get; set; }

        public List<OfficalViewModel> TotalSumsRow { get; set; }

        private void SetupViewModel()
        {
            this.DataRows = new ObservableCollection<RowViewModel>();
            this.TotalSumsRow = new List<OfficalViewModel>();
            this.allOfficalViewModels = new List<OfficalViewModel>()
                ;
            foreach (var competition in this.context.Competitions.ToList().Where(c => c.IsCompetitionNotStarted).OrderBy(c => c.StartTime))
            {
                var viewModel = new RowViewModel()
                {
                    Competition = competition,
                    Sum = 0
                };

                this.DataRows.Add(viewModel);

                this.FillOfficials(viewModel, competition);
            }
        }

        private void FillOfficials(RowViewModel rowViewModel, Competition competition)
        {
            // get all judges from event:
            var judges = this.context.Officials.Where(o => o.Roles.Any(r => r.Id == Roles.Judge)).OrderBy(j => j.Sign);
            this.CreateRow(rowViewModel, competition, judges, Roles.Judge);

            var chairperson = this.context.Officials.Where(o => o.Roles.Any(r => r.Id == Roles.Chairman)).OrderBy(j => j.Sign);
            this.CreateRow(rowViewModel, competition, chairperson, Roles.Chairman);

            var supervisors = this.context.Officials.Where(o => o.Roles.Any(r => r.Id == Roles.Supervisor)).OrderBy(j => j.Sign);
            this.CreateRow(rowViewModel, competition, supervisors, Roles.Supervisor);

            var associate = this.context.Officials.Where(o => o.Roles.Any(r => r.Id == Roles.Associate)).OrderBy(j => j.Sign);
            this.CreateRow(rowViewModel, competition, associate, Roles.Associate);

            var scrutnieer = this.context.Officials.Where(o => o.Roles.Any(r => r.Id == Roles.Scrutineer)).OrderBy(j => j.Sign);
            this.CreateRow(rowViewModel, competition, scrutnieer, Roles.Scrutineer);

            // Setup initial row-sums:
            foreach (var row in this.DataRows)
            {
                row.Sum = row.OfficialViewModels.Count(o => o.IsAssigned);
            }

            foreach (var row in this.TotalSumsRow)
            {
                row.Sum =
                    this.allOfficalViewModels.Count(
                        a => a.Offical.Id == row.Offical.Id && a.Role.Id == row.Role.Id && a.IsAssigned);
            }

        }

        private void CreateRow(RowViewModel rowViewModel, Competition competition, IEnumerable<Official> officials, int roleId)
        {
            foreach (var official in officials)
            {
                var viewModel = new OfficalViewModel()
                                    {
                                        Role = this.context.Roles.Single(r => r.Id == roleId),
                                        Offical = official,
                                        IsAssigned =
                                            competition.Officials.Any(
                                                o =>
                                                o.Official.Id == official.Id && o.Role.Id == roleId)
                                    };

                viewModel.PropertyChanged += (sender, args) =>
                    {
                        var view = sender as OfficalViewModel;
                        var sumElement = this.TotalSumsRow.FirstOrDefault(o => o.Offical.Id == view.Offical.Id && o.Role.Id == view.Role.Id);
                        sumElement.Sum = this.allOfficalViewModels.Count(o => o.Offical.Id == view.Offical.Id && o.Role.Id == view.Role.Id && o.IsAssigned);
                    }; 

                rowViewModel.OfficialViewModels.Add(viewModel);
                this.allOfficalViewModels.Add(viewModel);

                this.TotalSumsRow.Add(new OfficalViewModel() {Offical = official, Role = this.context.Roles.Single(r => r.Id == roleId) });
            }
        }

        private void CreateControl()
        {
            // We add the control to the 

            var firstColumn = new DataGridTextColumn()
                                  {
                                      Header = LocalizationService.Resolve(() => Text.Competition),
                                      Binding = new Binding("Competition.Title")
                                  };

            this.OfficialsMatrix.Columns.Add(firstColumn);
            
            this.AddGridColumn(firstColumn, 0, LocalizationService.Resolve(() => Text.Sum) ,null);
            var label = new Label() {Content = LocalizationService.Resolve(() => Text.All)};
            Grid.SetRow(label, 1);
            Grid.SetColumn(label, 0);
            this.SumGrid.Children.Add(label);

            if (!this.DataRows.Any())
            {
                MainWindow.MainWindowHandle.ShowMessage(
                    LocalizationService.Resolve(() => Text.NoCompetitionassigningIsAllowed));
                return;
            }

            var index = 0;

            foreach (var official in this.DataRows.First().OfficialViewModels)
            {
                var column = new DataGridCheckBoxColumn()
                                 {
                                     Header = $"[{official.Offical.Sign}] {official.Offical.NiceName} [{official.Role.RoleShort}] ",
                                     Binding = new Binding($"OfficialViewModels[{index}].IsAssigned") { UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged },
                                     
                                 };

                var style = new Style(typeof(DataGridColumnHeader));
                style.BasedOn = (Style) this.FindResource("MetroDataGridColumnHeader");
                style.Setters.Add(new Setter(ToolTipService.ToolTipProperty, official.Offical.Licenses));
                style.Setters.Add(new Setter(LayoutTransformProperty, new RotateTransform(270)));

                column.HeaderStyle = style;

                var rowItem = this.TotalSumsRow.FirstOrDefault(r => r.Offical.Id == official.Offical.Id && r.Role.Id == official.Role.Id);

                var sumBinding = new Binding()
                                    {
                                        Source = rowItem,
                                        Path = new PropertyPath("Sum")
                                    };

                this.OfficialsMatrix.Columns.Add(column);
                this.AddGridColumn(column, index + 1, null, sumBinding);

                var checkbox = new CheckBox() { Tag = official, VerticalAlignment = VerticalAlignment.Center, HorizontalAlignment = HorizontalAlignment.Center, Content = new Label(){ Content = "Hell"}};
                checkbox.Checked += this.CheckboxOnChecked;
                checkbox.Unchecked += this.CheckboxOnUnchecked;

                Grid.SetRow(checkbox, 1);
                Grid.SetColumn(checkbox, index + 1);
                this.SumGrid.Children.Add(checkbox);

                index++;
            }

            var lastColumn = new DataGridTextColumn()
            {
                Header = LocalizationService.Resolve(() => Text.Competition),
                Binding = new Binding("Sum")
            };

            this.OfficialsMatrix.Columns.Add(lastColumn);
        }

        private void CheckboxOnUnchecked(object sender, RoutedEventArgs routedEventArgs)
        {
            this.SetAssignmentValues(sender, false);
        }


        private void CheckboxOnChecked(object sender, RoutedEventArgs routedEventArgs)
        {
            this.SetAssignmentValues(sender, true);
        }

        private void SetAssignmentValues(object sender, bool newValue)
        {
            var checkbox = sender as CheckBox;
            if (checkbox == null)
            {
                return;
            }
            var official = checkbox.Tag as OfficalViewModel;
            if (official == null)
            {
                return;
            }

            foreach (var row in this.DataRows)
            {
                var officialViewModel = row.OfficialViewModels.FirstOrDefault(o => o.Offical.Id == official.Offical.Id && o.Role.Id == official.Role.Id);
                if (officialViewModel == null)
                {
                    continue;
                }

                officialViewModel.IsAssigned = newValue;
            }
        }

        private void AddGridColumn(DataGridColumn column, int columnIndex, string text, Binding binding)
        {
            var columnDef = new ColumnDefinition();

            columnDef.SetBinding(
                ColumnDefinition.WidthProperty,
                new Binding("ActualWidth") { Source = column, Converter = new DoubleToThicknesConverter()});

            this.SumGrid.ColumnDefinitions.Add(columnDef);

            var label = new Label();
            if (binding != null)
            {
                label.SetBinding(ContentProperty, binding);
            }
            else
            {
                label.Content = text;
            }

            Grid.SetColumn(label, columnIndex);
            Grid.SetRow(label, 0);

            this.SumGrid.Children.Add(label);
        }

        private void Save()
        {
            // Testcode
            var changes =
                this.context.ChangeTracker.Entries()
                    .Where(e => e.State == EntityState.Added || e.State == EntityState.Modified)
                    .ToList();

            this.context.SaveChanges();
            // For each competition we setup the officials again
            foreach (var rowview in this.DataRows)
            {
                foreach (var assignment in rowview.Competition.Officials.ToList())
                {
                    this.context.OfficialCompetition.Remove(assignment);
                }

                rowview.Competition.Officials.Clear();

                this.context.SaveChanges();

                foreach (var offialAndRole in rowview.OfficialViewModels.Where(o => o.IsAssigned))
                {
                    rowview.Competition.Officials.Add(new OfficialInCompetition()
                                                          {
                                                              Competition = rowview.Competition,
                                                              Official = offialAndRole.Offical,
                                                              Role = offialAndRole.Role
                                                          });

                    this.context.SaveChanges();
                }

                this.context.SaveChanges();
            }
        }

        private void OfficialsMatrix_OnSelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            var columnStyle = new Style(typeof(DataGridColumnHeader));
            columnStyle.BasedOn = (Style)this.FindResource("MetroDataGridColumnHeader");
            columnStyle.Setters.Add(new Setter(LayoutTransformProperty, new RotateTransform(270)));

            foreach (var row in this.DataRows)
            {
                row.IsSelected = false;
            }

            foreach (var item in OfficialsMatrix.Columns)
            {
                item.HeaderStyle = columnStyle;
            }

            if (OfficialsMatrix.SelectedCells != null)
            {
                columnStyle = new Style(typeof(DataGridColumnHeader));
                columnStyle.BasedOn = (Style)this.FindResource("MetroDataGridColumnHeader");
                columnStyle.Setters.Add(new Setter(LayoutTransformProperty, new RotateTransform(270)));
                columnStyle.Setters.Add(new Setter(Border.BackgroundProperty, new SolidColorBrush(Colors.Orange)));

                foreach (var item in OfficialsMatrix.SelectedCells)
                {
                    item.Column.HeaderStyle = columnStyle;
                    var row = item.Item as RowViewModel;
                    if (row != null)
                    {
                        row.IsSelected = true;
                    }
                }
            }
        }
    }

    public class OfficialMatrixRowStyleSelector : StyleSelector
    {
        private Func<string, object> FindResource { get; set; }

        public OfficialMatrixRowStyleSelector(Func<string, object> findResource)
        {
            this.FindResource = findResource;
        }

        public override Style SelectStyle(object item, DependencyObject container)
        {
            if (item is RowViewModel row)
            {
                if (row.IsSelected)
                {
                    return (Style) this.FindResource("SelectedRowStyle");
                }
                else
                {
                    return (Style)this.FindResource("UnSelectedRowStyle");
                }
            }

            return null;
        }
    }
}
