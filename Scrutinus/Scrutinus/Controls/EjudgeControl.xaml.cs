﻿// // TPS.net TPS8 Scrutinus
// // EjudgeControl.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using CsQuery.ExtensionMethods.Internal;
using DataModel.Models;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using mobileControl.Helper;
using MobileControlLib.EjudgeMessages;
using MobileControlLib.Helper;
using Scrutinus.Dialogs.EjudgeDialogs;
using Scrutinus.Localization;
using UserControl = System.Windows.Controls.UserControl;

namespace Scrutinus.Controls
{
    /// <summary>
    /// Interaction logic for EjudgeControl.xaml
    /// </summary>
    public partial class EjudgeControl : UserControl, INotifyPropertyChanged
    {
        public static readonly DependencyProperty RoundToJudgeProperty = DependencyProperty.Register("RoundToJudge", typeof(Round), typeof(EjudgeControl), new FrameworkPropertyMetadata(RoundChanged));

        public static readonly DependencyProperty ScrutinusContextProperty = DependencyProperty.Register("ScrutinusContext", typeof(ScrutinusContext), typeof(EjudgeControl), new FrameworkPropertyMetadata(ContextChanged));

        public EjudgeControl()
        {
            this.Devices = new ObservableCollection<EjudgeData>();

            this.InitializeComponent();

            this.OpenJudgeDetailsCommand = new RelayCommand(this.ShowJudgeDetails);
            this.RequestDataFromDevicesCommand = new RelayCommand(this.RequestResults);
            this.ClearDevicesCommand = new RelayCommand(this.SendClear);
            this.ShutdownCommand = new RelayCommand(this.Shutdown);
            this.SetHeatCommand = new RelayCommand(this.SetHeat);
            this.ChangeNumberOfMarksCommand = new RelayCommand(this.SetMarks);

            Messenger.Default.Register<NewDeviceMessage>(this, this.OnNewDevice);
        }

        public Round RoundToJudge
        {
            get
            {
                return (Round)this.GetValue(RoundToJudgeProperty);
            }
            set
            {
                this.SetValue(RoundToJudgeProperty, value);
            }
        }


        public ScrutinusContext ScrutinusContext
        {
            get
            {
                return (ScrutinusContext)this.GetValue(ScrutinusContextProperty);
            }
            set
            {
                this.SetValue(ScrutinusContextProperty, value);                
            }
        }

        public bool EjudgeEnabled
        {
            get
            {
                return this.RoundToJudge != null ? this.RoundToJudge.EjudgeEnabled : false;
            }
            set
            {
                this.RoundToJudge.EjudgeEnabled = value;
                this.EjudgeEnabledChanged();
                this.ScrutinusContext.SaveChanges();
            }
        }

        public ObservableCollection<EjudgeData> Devices { get; set; }

        public EjudgeData SelectedDevice { get; set; }

        public ICommand SendDataCommand { get; set; }

        public ICommand SetHeatCommand { get; set; }

        public ICommand RequestDataFromDevicesCommand { get; set; }

        public ICommand OpenJudgeDetailsCommand { get; set; }

        public ICommand ChangeNumberOfMarksCommand { get; set; }

        public ICommand ClearDevicesCommand { get; set; }

        public ICommand ShutdownCommand { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;


        private static void RoundChanged(
            DependencyObject dependencyObject,
            DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var control = dependencyObject as EjudgeControl;
            if (control == null)
            {
                return;
            }

            if (control.IsVisible && control.ScrutinusContext != null && control.RoundToJudge != null && control.RoundToJudge.EjudgeEnabled)
            {
                control.FillEjudgeList();
            }


            control.RaisePropertyChanged("EjudgeEnabled");
        }

        private static void ContextChanged(
            DependencyObject dependencyObject,
            DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var control = dependencyObject as EjudgeControl;
            if (control.ScrutinusContext != null && control.RoundToJudge != null && control.RoundToJudge.EjudgeEnabled)
            {
                control.FillEjudgeList();
            }

            control.RaisePropertyChanged("EjudgeEnabled");
        }

        private void ShowJudgeDetails()
        {
            if (this.SelectedDevice == null)
            {
                MainWindow.MainWindowHandle.ShowMessageAsync(
                    LocalizationService.Resolve(() => Text.SelectJudgeMessage),
                    LocalizationService.Resolve(() => Text.SelectJudgeCaption));

                return;
            }

            var dlg = new EJudgeDetailsDialog(this.ScrutinusContext, this.SelectedDevice)
            {
                CloseButtonCommand = new RelayCommand<EJudgeDetailsDialog>(this.JudgeDetailsClosed)
            };

            dlg.CloseButtonCommandParameter = dlg;

            MainWindow.MainWindowHandle.ShowChildWindow(dlg);
        }

        private void JudgeDetailsClosed(EJudgeDetailsDialog dialog)
        {
            this.ScrutinusContext.SaveChanges();
        }

        private void EjudgeEnabledChanged()
        {
            if (!this.EjudgeEnabled || this.RoundToJudge == null)
            {
                return;
            }

            // Check we have devices ...
            this.RoundToJudge.EjudgeEnabled = true;
            this.Devices.Clear();

            var devices = DeviceHelper.CreateEjudgeData(this.ScrutinusContext, this.RoundToJudge, MainWindow.MainWindowHandle.ViewModel.Devices);
            this.Devices.AddRange(devices);

            var flags = this.GetFlags();

            foreach (var ejudgeData in this.Devices)
            {
                ejudgeData.Flags = flags;
            }

            this.ScrutinusContext.SaveChanges();
        }

        private void RequestResults()
        {
            ScrutinusCommunicationHandler.SendSaveCmd(this.RoundToJudge);
        }

        private void SendData()
        {
            ScrutinusCommunicationHandler.SendData(this.RoundToJudge);
            ScrutinusCommunicationHandler.SendStart(this.RoundToJudge);
        }

        private void FillEjudgeList()
        {
            this.Devices.AddRange(DeviceHelper.CreateEjudgeData(this.ScrutinusContext, this.RoundToJudge, MainWindow.MainWindowHandle.ViewModel.Devices));

            var flags = this.GetFlags();

            foreach (var ejudgeData in this.Devices)
            {
                ejudgeData.Flags = flags;
            }

            this.ScrutinusContext.SaveChanges();
        }

        private void SetHeat()
        {
            var dlg = new SetJudgeToHeatDialog(this.RoundToJudge);
            dlg.CloseButtonCommand = new RelayCommand<SetJudgeToHeatDialog>(this.SetHeatExecute);
            dlg.CloseButtonCommandParameter = dlg;

            MainWindow.MainWindowHandle.ShowChildWindow(dlg);
        }

        private void OnNewDevice(NewDeviceMessage message)
        {
            var device = this.Devices.FirstOrDefault(d => d.DeviceId == message.Device.DeviceId);
            if (device != null)
            {
                device.Device = message.Device;
            }
        }


        private void SetHeatExecute(SetJudgeToHeatDialog dlg)
        {
            if (dlg.Result != DialogResult.OK)
            {
                return;
            }

            // Set to heat
            ScrutinusCommunicationHandler.MoveJudges(this.RoundToJudge, dlg.SeletedDance.SortOrder, dlg.SelectedHeat.Item1);
        }

        private void SetMarks()
        {
            var dlg = new ChangeNumberOfMarksDialog(this.RoundToJudge);
            dlg.CloseButtonCommand = new RelayCommand<ChangeNumberOfMarksDialog>(this.SetMarksExecute);
            dlg.CloseButtonCommandParameter = dlg;

            MainWindow.MainWindowHandle.ShowChildWindow(dlg);
        }

        private void SetMarksExecute(ChangeNumberOfMarksDialog dlg)
        {
            if (dlg.Result != DialogResult.OK)
            {
                return;
            }   

            ScrutinusCommunicationHandler.SendConfig(this.RoundToJudge);

            this.ScrutinusContext.SaveChanges();
        }

        private void SendClear()
        {
            ScrutinusCommunicationHandler.ClearDevices(this.RoundToJudge);
        }

        private void Shutdown()
        {
            ScrutinusCommunicationHandler.ClearDevices(this.RoundToJudge);

            foreach (var device in this.RoundToJudge.EjudgeData)
            {
                device.DeviceId = null;
                device.Device = null;

                this.RoundToJudge.EjudgeEnabled = false;
            }
        }

        private void RaisePropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private int GetFlags()
        {
            var flags = 0;

            flags += Settings.Default.eJudgeHelpmarks1 ? 1 : 0;
            flags += Settings.Default.eJudgeHelpmarks2 ? 32 : 0;
            flags += Settings.Default.eJudgetshowHelpmarkOverview ? 2 : 0;
            flags += Settings.Default.eJudgeallowMultiLastPlaces ? 4 : 0;
            flags += Settings.Default.eJudgeshowFinalResults ? 8 : 0;
            flags += Settings.Default.eJudgeshowLift ? 16 : 0;
            flags += Settings.Default.eJudgeshowLanguageSelection ? 64 : 0;
            flags += Settings.Default.eJudgeshowLastPlaceButtons ? 128 : 0;

            return flags;
        }
    }
}
