﻿// // TPS.net TPS8 Scrutinus
// // EditCoupleDetails.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using DataModel.Models;

namespace Scrutinus.Controls
{
    /// <summary>
    /// Interaction logic for EditCoupleDetails.xaml
    /// </summary>
    public partial class EditCoupleDetails : UserControl, INotifyPropertyChanged
    {
        public static readonly DependencyProperty CoupleProperty = DependencyProperty.Register(
            "Couple",
            typeof(Couple),
            typeof(EditCoupleDetails), new FrameworkPropertyMetadata(CallbackHandler));

        public EditCoupleDetails()
        {
            this.InitializeComponent();

            var context = new ScrutinusContext();

            this.Countries = new ObservableCollection<CountryCode>(context.CountryCodes);
        }

        public Couple Couple
        {
            get
            {
                return (Couple)this.GetValue(CoupleProperty);
            }
            set
            {
                this.SetValue(CoupleProperty, value);
                this.RaisePropertyChanged();

                if (value != null)
                {
                    Keyboard.Focus(this.FirstNameManTextBox);
                }
            }
        }

        public ObservableCollection<CountryCode> Countries { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        private static void CallbackHandler(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = d as EditCoupleDetails;

            if (control != null)
            {
                control.RaisePropertyChanged();
            }
        }

        private void RaisePropertyChanged()
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs("Couple"));
            }
        }
    }
}
