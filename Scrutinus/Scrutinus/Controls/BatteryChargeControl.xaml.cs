﻿// // TPS.net TPS8 Scrutinus
// // BatteryChargeControl.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Scrutinus.Controls
{
    /// <summary>
    /// Interaction logic for BatteryChargeControl.xaml
    /// </summary>
    public partial class BatteryChargeControl : UserControl, INotifyPropertyChanged
    {
        public static readonly DependencyProperty BatteryLevelProperty = DependencyProperty.Register("BatteryLevel", typeof(int), typeof(BatteryChargeControl), new PropertyMetadata(0, OnBatteryLevelChanged));

        public BatteryChargeControl()
        {
            this.RectangleColor = Brushes.Green;

            this.InitializeComponent();
        }

        public int BatteryLevel
        {
            get
            {
                return (int)this.GetValue(BatteryLevelProperty);
            }
            set
            {
                this.SetValue(BatteryLevelProperty, value);
            }
        }

        public string Percentage { get; set; }

        public double RectangleWidth { get; set; }

        public Brush RectangleColor { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        private static void OnBatteryLevelChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var batteryChargeControl = d as BatteryChargeControl;

            if (batteryChargeControl == null)
            {
                return;
            }

            var batteryDouble = Convert.ToDouble(batteryChargeControl.BatteryLevel);

            batteryChargeControl.Percentage = batteryChargeControl.BatteryLevel + " %";

            // we have a maring of 5 so 2 * 5 = 10:
            batteryChargeControl.RectangleWidth = (batteryChargeControl.ActualWidth - 10) * (batteryDouble / 100);

            // Min Width of Rectangle is 3 so that we at least see there is a rectangle
            if (batteryChargeControl.RectangleWidth < 3)
            {
                batteryChargeControl.RectangleWidth = 3;
            }

            batteryChargeControl.RectangleColor = batteryDouble < 15 ? Brushes.Red : Brushes.Green;

            batteryChargeControl.RaisePropertyChanged("Percentage");
            batteryChargeControl.RaisePropertyChanged("RectangleWidth");
            batteryChargeControl.RaisePropertyChanged("RectangleColor");
        }

        private void RaisePropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
