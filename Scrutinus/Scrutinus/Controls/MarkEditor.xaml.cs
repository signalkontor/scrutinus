﻿// // TPS.net TPS8 Scrutinus
// // MarkEditor.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Scrutinus.Controls
{
    /// <summary>
    ///     Interaction logic for MarkEditor.xaml
    /// </summary>
    public partial class MarkEditor : UserControl, INotifyPropertyChanged
    {
        #region Constructors and Destructors

        public MarkEditor()
        {
            this.InitializeComponent();
            this.DataContext = this;
        }

        #endregion

        #region Public Events

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Static Fields

        public static readonly DependencyProperty MarkProperty = DependencyProperty.Register(
            "Mark",
            typeof(bool),
            typeof(MarkEditor),
            new FrameworkPropertyMetadata(false, ValuePropertyChanges) { BindsTwoWayByDefault = true });

        public static readonly DependencyProperty NumberProperty = DependencyProperty.Register(
            "Number",
            typeof(int),
            typeof(MarkEditor),
            new FrameworkPropertyMetadata(0, ValuePropertyChanges) { BindsTwoWayByDefault = true });

        public static readonly DependencyProperty TooltipProperty = DependencyProperty.Register(
            "Tooltip",
            typeof(string),
            typeof(MarkEditor),
            new FrameworkPropertyMetadata("", ValuePropertyChanges) { BindsTwoWayByDefault = false });

        #endregion

        #region Public Properties

        public bool Mark
        {
            get
            {
                return (bool)this.GetValue(MarkProperty);
            }
            set
            {
                this.SetValue(MarkProperty, value);
                this.HandlePropertyChanged("Mark");
            }
        }

        public int Number
        {
            get
            {
                return (int)this.GetValue(NumberProperty);
            }
            set
            {
                this.SetValue(NumberProperty, value);
                this.HandlePropertyChanged("Number");
            }
        }

        #endregion

        #region Methods

        private static void ValuePropertyChanges(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            if (o is MarkEditor)
            {
                var markeditor = o as MarkEditor;
                markeditor.RenderControl();
            }
        }

        private void HandlePropertyChanged(string name)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        private void MarkEditor_OnKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                this.Mark = !this.Mark;
            }
            if (e.Key == Key.NumPad0 || e.Key == Key.D0)
            {
                this.Mark = false;
                this.MoveToNextUIElement(e);
            }
            if (e.Key == Key.NumPad1 || e.Key == Key.D1)
            {
                this.Mark = true;
                this.MoveToNextUIElement(e);
            }
        }

        private void MarkEditor_OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            this.Mark = !this.Mark;
        }

        private void MoveToNextUIElement(KeyEventArgs e)
        {
            // Creating a FocusNavigationDirection object and setting it to a
            // local field that contains the direction selected.
            var focusDirection = FocusNavigationDirection.Next;

            // MoveFocus takes a TraveralReqest as its argument.
            var request = new TraversalRequest(focusDirection);

            // Gets the element with keyboard focus.
            var elementWithFocus = Keyboard.FocusedElement as UIElement;

            // Change keyboard focus.
            if (elementWithFocus != null)
            {
                if (elementWithFocus.MoveFocus(request))
                {
                    e.Handled = true;
                }
            }
        }

        private void RenderControl()
        {
            // this.Border.Background = this.Mark ? Brushes.Green : Brushes.Black;
        }

        #endregion
    }
}