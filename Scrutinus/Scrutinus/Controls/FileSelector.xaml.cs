﻿// // TPS.net TPS8 Scrutinus
// // FileSelector.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using Microsoft.Win32;

namespace Scrutinus.Controls
{
    /// <summary>
    ///     Interaction logic for FileSelector.xaml
    /// </summary>
    public partial class FileSelector : UserControl
    {
        #region Constructors and Destructors

        public FileSelector()
        {
            this.FileOpenCommand = new RelayCommand(this.FileOpen);

            this.InitializeComponent();
        }

        #endregion

        #region Methods

        private void FileOpen()
        {
            var openFileDialog = new OpenFileDialog { CheckFileExists = false, Filter = this.FileFilter };

            if (openFileDialog.ShowDialog() == true)
            {
                if (!string.IsNullOrEmpty(this.Extension))
                {
                    if (!openFileDialog.FileName.EndsWith(this.Extension))
                    {
                        openFileDialog.FileName += this.Extension;
                    }
                }

                this.SelectedFile = openFileDialog.FileName;
            }
        }

        #endregion

        #region Static Fields

        public static DependencyProperty FileFilterProperty = DependencyProperty.Register(
            "FileFilter",
            typeof(string),
            typeof(FileSelector),
            new PropertyMetadata(""));

        public static DependencyProperty SelectedPathProperty = DependencyProperty.Register(
            "SelectedFile",
            typeof(string),
            typeof(FileSelector),
            new PropertyMetadata(""));

        public static DependencyProperty ExtensionProperty = DependencyProperty.Register(
            "Extension",
            typeof(string),
            typeof(FileSelector),
            new PropertyMetadata(""));

        #endregion

        #region Public Properties

        public string FileFilter
        {
            get
            {
                return (string)this.GetValue(FileFilterProperty);
            }
            set
            {
                this.SetValue(FileFilterProperty, value);
            }
        }

        public ICommand FileOpenCommand { get; set; }

        public string SelectedFile
        {
            get
            {
                return (string)this.GetValue(SelectedPathProperty);
            }
            set
            {
                this.SetValue(SelectedPathProperty, value);
            }
        }

        public string Extension
        {
            get { return (string) this.GetValue(ExtensionProperty); }
            set { this.SetValue(ExtensionProperty, value); }
        }

        #endregion
    }
}