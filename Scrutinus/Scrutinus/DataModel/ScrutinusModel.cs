﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;
using System.Data.Objects.DataClasses;
using System.Data.EntityClient;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Runtime.Serialization;

namespace DataModel
{
    public partial class Couple : EntityObject
    {
        public string NiceName
        {
            get { return String.Format("{0} {1} & {2} {3}", FirstMan, LastMan, FirstWoman, LastWoman); }
        }
    }

    public partial class Official : EntityObject
    {
        private IObserver<Official> _observer;

        public string NiceName
        {
            get { return String.Format("{0} {1}", Firstname, Lastname); }
        }

        public string RolesAsString
        {
            get { return Role_Official.Aggregate("", (current, roleOfficial) => current + (", " + roleOfficial.Role.RoleShort)); }
        }

    }

    public partial class Participant : EntityObject
    {
        public string Place
        {
            get
            {
                if (PlaceFrom.HasValue && PlaceFrom.Value > 0)
                {
                    return PlaceFrom == PlaceTo ? PlaceFrom + "." : String.Format("{0}.-{1}.", PlaceFrom, PlaceTo);
                }
                else
                {
                    switch (State)
                    {
                        case CoupleState.Dancing:
                            return "In Competition";
                        case CoupleState.Missing:
                            return "Missing";
                        case CoupleState.Excused:
                            return "Excused";
                    }

                    return "unknown";
                }
            }
        }

        public string StateString
        {
            get
            {
                switch (State)
                {
                    case CoupleState.Dancing:
                        return "In Competition";
                    case CoupleState.Missing:
                        return "Missing";
                    case CoupleState.Excused:
                        return "Excused";
                }

                return "unknown";
            }
        }
    }

}
