﻿// // TPS.net TPS8 Scrutinus
// // NavigationHelper.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using DataModel;
using DataModel.Models;
using Scrutinus.Localization;
using Scrutinus.Pages;
using Scrutinus.ViewModels;
using Printing = Scrutinus.Pages.Printing;

namespace Scrutinus.Navigation
{
    public static class NavigationHelper
    {
        #region Methods

        private static Round HandleSecondFirstRound(ScrutinusContext context, CompetitionElement competition)
        {
            // we have a second first round, so we jump directly to the new round...
            var nextRound =
                context.Rounds.SingleOrDefault(r => r.Competition.Id == competition.Competition.Id && r.Number == 2);
            if (nextRound == null)
            {
                nextRound = CreateRound(
                    context,
                    RoundTypes.QualificationRound,
                    2,
                    0,
                    competition.Competition.Id,
                    new List<Participant>());

                context.SaveChanges();
            }

            foreach (var qualified in nextRound.Qualifieds)
            {
                qualified.PlaceFrom = -1;
            }

            foreach (
                var participant in
                    competition.Competition.Participants.Where(p => p.State == CoupleState.Dancing))
            {
                participant.QualifiedRound = nextRound;
                var existing = nextRound.Qualifieds.SingleOrDefault(q => q.Participant.Id == participant.Id);
                if (existing == null)
                {
                    nextRound.Qualifieds.Add(new Qualified { Participant = participant, Round = nextRound });
                }
                else
                {
                    existing.PlaceFrom = 0;
                }
            }

            // Remove all with qualified.PlaceFrom -> Including any markings etc ...
            foreach (var qualified in nextRound.Qualifieds.Where(q => q.PlaceFrom == -1).ToList())
            {
                context.DeleteQualified(qualified);
                nextRound.Qualifieds.Remove(qualified);
            }

            nextRound.State = CompetitionState.RoundDrawing;
            competition.CurrentRound = nextRound;
            competition.Competition.CurrentRound = nextRound;
            context.SaveChanges();

            competition.CurrentPage = new DefineHeat(competition.Competition.Id, nextRound.Id);

            // Feedback to the user ... 
            MainWindow.MainWindowHandle.ShowMessage(LocalizationService.Resolve(() => Text.WarningTwoFirstRounds));
            return nextRound;
        }

        #endregion

        #region Public Methods and Operators

        public static Round CreateRound(
            ScrutinusContext context,
            int roundTypeId,
            int number,
            int placeOffset,
            int competitionId,
            IEnumerable<Participant> participants)
        {
            var roundType = context.RoundTypes.Single(r => r.Id == roundTypeId);
            var competition = context.Competitions.Single(c => c.Id == competitionId);
            var newRound = new Round
                               {
                                   Competition = competition,
                                   Number = number,
                                   RoundType = roundType,
                                   PlaceOffset = placeOffset,
                                   MarksInputType =
                                       roundType.IsFinal ? competition.FinalType : competition.MarkingType
                               };

            context.Rounds.Add(newRound);
            context.SaveChanges();

            if (participants != null)
            {
                foreach (var participant in participants)
                {
                    participant.QualifiedRound = newRound;
                    var newParticipant = context.Participants.Single(p => p.Id == participant.Id);
                    newRound.Qualifieds.Add(new Qualified { Participant = newParticipant, Round = newRound });
                }
            }
            var order = 0;

            foreach (var dancesInCompetition in competition.Dances.OrderBy(o => o.DanceOrder))
            {
                order ++;

                var danceInRound = new DanceInRound
                                       {
                                           Dance = dancesInCompetition.Dance,
                                           IsSolo = false,
                                           Round = newRound,
                                           SortOrder = order
                                       };

                newRound.DancesInRound.Add(danceInRound);
            }

            return newRound;
        }

        public static void GetNextAfterEnterMarking(ScrutinusContext context, CompetitionElement competition)
        {
            if (competition.CurrentRound.Number == 1
                && competition.Competition.FirstRoundType == RoundTypes.SecondFirstRound)
            {
                var nextRound = HandleSecondFirstRound(context, competition);

                return;
            }

            if (competition.CurrentRound.RoundType.Id == RoundTypes.ABFinal)
            {
                var nextRound =
                    context.Rounds.Single(
                        r =>
                        r.Competition.Id == competition.Competition.Id
                        && r.Number == competition.CurrentRound.Number + 1);

                competition.CurrentRound = nextRound;
                var comp = context.Competitions.Single(c => c.Id == competition.Competition.Id);
                comp.CurrentRound = nextRound;
                context.SaveChanges();
                competition.CurrentRound.State = CompetitionState.RoundDrawing;
                competition.CurrentPage = new DefineHeat(competition.Competition.Id, competition.CurrentRound.Id);
            }
            else
            {
                if (competition.CurrentRound.RoundType.Id == RoundTypes.Final)
                {
                    competition.CurrentRound.Competition.State = CompetitionState.Finished;
                    competition.CurrentRound.State = CompetitionState.Finished;
                    context.SaveChanges();

                    if (context.PrintReportDefinitions.Any())
                    {
                        competition.CurrentPage = new PrintingPrintManager(
                                                        competition.Competition.Id,
                                                        competition.Competition.CurrentRound.Id);
                    }
                    else
                    {
                        competition.CurrentPage = new Printing(
                                                        competition.Competition.Id,
                                                        competition.Competition.CurrentRound.Id);
                    }
                }
                else
                {
                    competition.CurrentRound.State = CompetitionState.SelectQualified;
                    competition.CurrentPage = new SelectQualified(
                        competition.Competition.Id,
                        competition.CurrentRound.Id);
                }
            }
        }

        #endregion
    }
}