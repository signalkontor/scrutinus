﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModel.Models;

namespace Scrutinus.Export
{
    public class DanceSportEuropeExporter : IDataExporter
    {
        public string Name => "DanceSport Europe Results";

        public void ExportCompetitions(IEnumerable<Competition> competitions, string outputPath, Action<int, string> progressCallback)
        {
            foreach (var competition in competitions)
            {
                // get the final round
                try
                {
                    var final = competition.Rounds.FirstOrDefault(r => r.RoundType.IsFinal);

                    if (final == null)
                    {
                        continue;
                    }

                    progressCallback.Invoke(0, $"Exporting {competition.Title}");

                    var file = new StreamWriter(new FileStream($"{outputPath}\\{competition.Title}.csv", FileMode.Create), Encoding.UTF8);

                    foreach (var qualified in final.Qualifieds.OrderBy(q => q.Participant.PlaceFrom))
                    {
                        var couple = qualified.Participant.Couple;

                        file.WriteLine($"{couple.MINMan};{couple.FirstMan} {couple.LastMan};{couple.MINWoman};{couple.FirstWoman} {couple.LastWoman};{qualified.Participant.PlaceFrom};{competition.Title};;;{couple.Country}");
                    }

                    file.Close();
                }
                catch (Exception e)
                {
                    ScrutinusContext.WriteLogEntry(e);
                }
                
            }

            progressCallback.Invoke(0, "");
        }

        public void ExportRound(Competition competition, Round round, string outputPath)
        {
            throw new NotImplementedException();
        }
    }
}
