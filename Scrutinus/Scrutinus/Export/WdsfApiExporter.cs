﻿// // TPS.net TPS8 Scrutinus
// // WdsfApiExporter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using DataModel.Models;

namespace Scrutinus.Export
{
    internal class WdsfApiExporter : IDataExporter
    {
        #region Public Properties

        public string Name
        {
            get
            {
                return "WDSF API";
            }
        }

        #endregion

        #region Public Methods and Operators

        public void ExportCompetitions(
            IEnumerable<Competition> competitions,
            string outputPath,
            Action<int, string> progressCallback)
        {
            throw new NotImplementedException();
        }

        public void ExportRound(Competition competition, Round round, string outputPath)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}