﻿// // TPS.net TPS8 Scrutinus
// // CsvRegisteredCouplesExporter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using DataModel.Models;
using Scrutinus.Localization;

namespace Scrutinus.Export
{
    public class CsvRegisteredCouplesExporter : IDataExporter
    {
        public string Name
        {
            get
            {
                return LocalizationService.Resolve(() => Text.ExportRegisteredAsCsv);
            }
        }

        public void ExportCompetitions(IEnumerable<Competition> competitions, string outputPath, Action<int, string> progressCallback)
        {
            try
            {
                using (var context = new ScrutinusContext())
                {
                    var stream = new StreamWriter(new FileStream(outputPath + "\\ExportRegisteredCouples.csv", FileMode.Create), Encoding.UTF8);

                    foreach (var competition in context.Competitions)
                    {
                        stream.WriteLine("{0};{1}", competition.StartTime, competition.Title);

                        foreach (var participant in competition.Participants.OrderBy(p => p.Number))
                        {
                            stream.WriteLine("{0};{1};{2};{3};{4};{5};{6}", participant.Number, 
                                participant.Couple.FirstMan, 
                                participant.Couple.LastMan, 
                                participant.Couple.FirstWoman, 
                                participant.Couple.LastWoman, 
                                participant.Couple.Country, 
                                participant.Couple.Region);

                        }
                    }

                    stream.Close();
                }
            }
            catch (Exception ex)
            {
                MainWindow.MainWindowHandle.ShowMessageAsync(ex.Message, "Es ist ein Fehler aufgetreten");
            }
        }

        public void ExportRound(Competition competition, Round round, string outputPath)
        {
            
        }
    }
}
