﻿// // TPS.net TPS8 Scrutinus
// // XmlResultExporter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DataModel;
using DataModel.Models;

namespace Scrutinus.Export
{
    internal class XmlResultExporter : IDataExporter
    {
        #region Public Properties

        public string Name
        {
            get
            {
                return "XML Export";
            }
        }

        #endregion

        #region Fields

        private readonly Dictionary<string, string> sectionLookup = new Dictionary<string, string>
                                                                        {
                                                                            { "Standard", "ST" },
                                                                            { "Latino", "LA" },
                                                                            { "10 Bailes", "10" }
                                                                        };

        private readonly Dictionary<int, int> compCodeLookup = new Dictionary<int, int>()
                                                                        {
                                                                            { 1, 11 },  // JUV I
                                                                            { 2, 10 },  // JUV II
                                                                            // { 3, JUV } // JUV not used
                                                                            { 4, 20 },  // JUN I
                                                                            { 5, 30 },  // JUN II
                                                                            { 6, 40 },  // Youth
                                                                            { 7, 50 },  // Adult I
                                                                            { 8, 60 },  // Adult II
                                                                            { 9, 70 },  // SEN I
                                                                            { 10, 80 }, // SEN II
                                                                            { 11, 90 }, // SEN III
                                                                            { 12, 95 }, // SEN IV
                                                                            { 13, 95 }  // U21
                                                                        };

        private readonly string resultTemplate = @"<Results>
                                            <EventCode>{0}</EventCode>
                                            <CoupleCode>{1}</CoupleCode>
                                            <CoupleNumber>{2}</CoupleNumber>
                                            <Names>{3}</Names>
                                            <CompCode>{4}</CompCode>
                                            <CompLevel>{5}</CompLevel>
                                            <CompType>{6}</CompType>
                                            <Position1>{7}</Position1>
                                            <Position2>{8}</Position2>
                                            <Missing>{9}</Missing>
                                            <TotalCouples>{10}</TotalCouples>
                                            <RoundsDanced>{11}</RoundsDanced>
                                          </Results>";

        private readonly string schema =
            @"<xs:schema id=""Event"" xmlns="""" xmlns:xs=""http://www.w3.org/2001/XMLSchema"" xmlns:msdata=""urn:schemas-microsoft-com:xml-msdata"">
    <xs:element name=""Event"" msdata:IsDataSet=""true"" msdata:UseCurrentLocale=""true"">
      <xs:complexType>
        <xs:choice minOccurs=""0"" maxOccurs=""unbounded"">
          <xs:element name=""Results"">
            <xs:complexType>
              <xs:sequence>
                <xs:element name=""EventCode"" type=""xs:string"" minOccurs=""0""/>
                <xs:element name=""CoupleCode"" type=""xs:string"" minOccurs=""0""/>
                <xs:element name=""CoupleNumber"" type=""xs:string"" minOccurs=""0""/>
                <xs:element name=""Names"" type=""xs:string"" minOccurs=""0""/>
                <xs:element name=""CompCode"" type=""xs:string"" minOccurs=""0""/>
                <xs:element name=""CompLevel"" type=""xs:string"" minOccurs=""0""/>
                <xs:element name=""CompType"" type=""xs:string"" minOccurs=""0""/>
                <xs:element name=""Position1"" type=""xs:string"" minOccurs=""0""/>
                <xs:element name=""Position2"" type=""xs:string"" minOccurs=""0""/>
                <xs:element name=""Missing"" type=""xs:string"" minOccurs=""0""/>
                <xs:element name=""TotalCouples"" type=""xs:string"" minOccurs=""0""/>
                <xs:element name=""RoundsDanced"" type=""xs:string"" minOccurs=""0""/>
              </xs:sequence>
            </xs:complexType>
          </xs:element>
        </xs:choice>
      </xs:complexType>
    </xs:element>
  </xs:schema>";

        #endregion

        #region Public Methods and Operators

        public void ExportCompetitions(
            IEnumerable<Competition> competitions,
            string outputPath,
            Action<int, string> progressCallback)
        {
            var stream = this.CreateFile(outputPath);

            foreach (var competition in competitions)
            {
                this.WriteCompetition(competition, stream);
            }

            stream.WriteLine("</Event>");

            stream.Close();
        }

        public void ExportRound(Competition competition, Round round, string outputPath)
        {
            throw new NotImplementedException();
        }

        private void WriteCompetition(Competition competition, StreamWriter stream)
        {
            if (competition != null)
            {
                var participants = competition.Participants.OrderBy(p => p.PlaceFrom);

                foreach (var participant in participants)
                {
                    var state = 0;
                    if (participant.State == CoupleState.Excused)
                    {
                        state = 1;
                    }
                    if (participant.State == CoupleState.Missing)
                    {
                        state = 2;
                    }

                    var result = string.Format(
                        this.resultTemplate,
                        competition.Event.ExternalId,
                        participant.Couple.MINMan,
                        participant.Number,
                        participant.Couple.NiceName,
                        this.compCodeLookup[competition.AgeGroup.Id],
                        competition.Class.ClassShortName,
                        this.sectionLookup.ContainsKey(competition.Section.SectionName)
                            ? this.sectionLookup[competition.Section.SectionName]
                            : competition.Section.SectionName,
                        participant.PlaceFrom,
                        participant.PlaceTo,
                        state,
                        participants.Count(),
                        participant.Qualifieds.Count);

                    stream.WriteLine(result);
                }
            }

        }

        private StreamWriter CreateFile(string outputPath)
        {
            if (!Directory.Exists(outputPath))
            {
                Directory.CreateDirectory(outputPath);
            }

            var outputFile = $"{outputPath}\\results.xml";

            var stream = new StreamWriter(outputFile, false);

            stream.WriteLine("<?xml version=\"1.0\" encoding=\"ISO-8859-1\" standalone=\"yes\"?>");

            stream.WriteLine("<Event>");

            stream.WriteLine(this.schema);

            return stream;
        }

        #endregion
    }
}