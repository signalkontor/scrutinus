﻿// // TPS.net TPS8 Scrutinus
// // HtmlExporter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DataModel.Models;
using Nancy;
using NancyServer;

namespace Scrutinus.Export
{
    internal class HtmlExporter : IDataExporter
    {
        #region Fields

        private HtmlGenerator htmlGenerator;

        #endregion

        #region Public Properties

        public string Name
        {
            get
            {
                return "Html Export";
            }
        }

        #endregion

        #region Public Methods and Operators

        public void ExportCompetitions(
            IEnumerable<Competition> competitions,
            string outputPath,
            Action<int, string> progressCallback)
        {
            HttpServer.ApplicationLocale = Settings.Default.LanguageLocale;

            this.htmlGenerator = new HtmlGenerator();

            this.GenerateIndexPage(competitions, outputPath);
            var count = 0;

            StaticConfiguration.DisableErrorTraces = false;

            foreach(var competition in competitions)
            {
                progressCallback?.Invoke(count * 100 / competitions.Count(), "Export Html " + competition.Title);

                this.ExportRound(competition, null, $"{outputPath}\\{competition.Id} {competition.Title.Replace("/", "_").Replace(" ", "_")}");

                count++;
            }

            // Copy the files from content and scripts to the new folder
            this.CreateDirectoryIfNotExists(outputPath + "\\content");
            this.CreateDirectoryIfNotExists(outputPath + "\\scripts");

            var appPath = AppDomain.CurrentDomain.BaseDirectory;

            this.CopyFolder(appPath + "\\Content", outputPath + "\\content");
            this.CopyFolder(appPath + "\\Scripts", outputPath + "\\scripts");

            progressCallback?.Invoke(0, string.Empty);
        }

        public void ExportRound(Competition competition, Round round, string outputPath)
        {
            if (!Directory.Exists(outputPath))
            {
                Directory.CreateDirectory(outputPath);
            }

            // Create an Overview Page for this competition
            this.ExportFile($"{outputPath}\\index.html", "/Export/Overview/" + competition.Id);
            // Registered Couples
            this.ExportFile(
                $"{outputPath}\\registeredcouples.html",
                "/Export/RegisteredCouples/" + competition.Id);
            // Export Final Table
            this.ExportFile($"{outputPath}\\finaltable.html", "/Export/FinalTable/" + competition.Id);
            // Export Marking
            this.ExportFile($"{outputPath}\\result.html", "/Export/Result/" + competition.Id);
            // Export Result
            this.ExportFile($"{outputPath}\\officials.html", "/Export/Officials/" + competition.Id);
            // Marking Table
            this.ExportFile(
                $"{outputPath}\\markingtable.html",
                "/Export/MarkingTable/" + competition.Id);
        }

        #endregion

        #region Methods

        private void CopyFolder(string sourcePath, string destinationPath)
        {
            //Now Create all of the directories
            foreach (var dirPath in Directory.GetDirectories(sourcePath, "*", SearchOption.AllDirectories))
            {
                Directory.CreateDirectory(dirPath.Replace(sourcePath, destinationPath));
            }

            //Copy all the files & Replaces any files with the same name
            foreach (var newPath in Directory.GetFiles(sourcePath, "*.*", SearchOption.AllDirectories))
            {
                File.Copy(newPath, newPath.Replace(sourcePath, destinationPath), true);
            }
        }

        private void CreateDirectoryIfNotExists(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }

        private void ExportFile(string filename, string url, string key = "", string value = "")
        {
            var page = this.htmlGenerator.GeneratePage(url, key, value);

            File.WriteAllBytes(filename, page);
        }

        private void GenerateIndexPage(IEnumerable<Competition> competitions, string path)
        {
            var ids = string.Join(",", competitions.Select(c => c.Id.ToString()));
            this.ExportFile(path + "\\index.html", "/Export", "ids", ids);
        }

        #endregion
    }
}