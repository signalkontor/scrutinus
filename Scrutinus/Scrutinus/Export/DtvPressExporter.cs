﻿// // TPS.net TPS8 Scrutinus
// // DtvPressExporter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DataModel;
using DataModel.Models;
using Scrutinus.Localization;

namespace Scrutinus.Export
{
    public class DtvPressExporter : IDataExporter
    {
        #region Fields

        private StreamWriter outStream;

        #endregion

        #region Public Properties

        public string Name
        {
            get
            {
                return LocalizationService.Resolve(() => Text.DTVPressExport);
            }
        }

        #endregion

        #region Public Methods and Operators

        public void ExportCompetitions(
            IEnumerable<Competition> competitions,
            string outputPath,
            Action<int, string> progressCallback)
        {
            this.outStream = new StreamWriter(outputPath + "\\presseinfo.txt");

            foreach (var competition in competitions)
            {
                this.ExportRound(competition, null, outputPath);
            }

            this.outStream.Close();
        }

        public void ExportRound(Competition competition, Round round, string outputPath)
        {
            var closeStream = false;
            if (this.outStream == null)
            {
                closeStream = true;
                this.outStream = new StreamWriter(outputPath + "\\presseinfo.txt");
            }

            this.outStream.WriteLine("{0:d}", competition.StartTime);
            this.outStream.WriteLine(
                "{0} {1} {2} ({3})",
                competition.AgeGroup.AgeGroupName,
                competition.Class.ClassShortName,
                competition.Section.SectionName,
                competition.Title);
            this.outStream.WriteLine(
                "{0} Paare am Start",
                competition.Participants.Count(p => p.State == CoupleState.DroppedOut || p.State == CoupleState.Dancing));
            this.outStream.WriteLine();
            // Output Final Result:
            var final = competition.Rounds.FirstOrDefault(r => r.RoundType.IsFinal);
            if (final != null)
            {
                foreach (var qualified in final.Qualifieds.OrderBy(q => q.Participant.PlaceFrom))
                {
                    this.outStream.WriteLine(
                        "{0}\t{1}, {2} ({3})",
                        qualified.Participant.Place,
                        qualified.Participant.Couple.NiceName.Replace("/", "-"),
                        qualified.Participant.Couple.Country,
                        qualified.Participant.Points);
                }
            }
            this.outStream.WriteLine();
            this.outStream.WriteLine("Wertungsrichter:");
            foreach (var judge in competition.GetJudges().OrderBy(j => j.Sign))
            {
                this.outStream.WriteLine("{0} {1}, {2}", judge.Firstname, judge.Lastname, judge.Club);
            }

            this.outStream.WriteLine("--------------------------------------------------------------\r\n");

            if (closeStream)
            {
                this.outStream.Close();
            }
        }

        #endregion
    }
}