﻿// // TPS.net TPS8 Scrutinus
// // IDataExporter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using DataModel.Models;

namespace Scrutinus.Export
{
    public interface IDataExporter
    {
        #region Public Properties

        string Name { get; }

        #endregion

        #region Public Methods and Operators

        void ExportCompetitions(
            IEnumerable<Competition> competitions,
            string outputPath,
            Action<int, string> progressCallback);

        void ExportRound(Competition competition, Round round, string outputPath);

        #endregion
    }
}