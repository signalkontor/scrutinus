// // TPS.net TPS8 Scrutinus
// // SaveSettingsCommand.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System;
using GalaSoft.MvvmLight.Command;
using General.Interfaces;

namespace Scrutinus.Commands
{
    public class SaveSettingsCommand<T> : RelayCommand<T>
    {
        private readonly ISaveSettingsProvider viewModel;

        public SaveSettingsCommand(Action<T> action, ISaveSettingsProvider viewModel) : base(action)
        {
            this.viewModel = viewModel;
        }

        public override void Execute(object parameter)
        {
            base.Execute(parameter);
            this.viewModel.SaveSettings();
        }
    }

    public class SaveSettingsCommand : RelayCommand
    {
        private readonly ISaveSettingsProvider viewModel;

        public SaveSettingsCommand(Action action, ISaveSettingsProvider viewModel) : base(action)
        {
            this.viewModel = viewModel;
        }

        public override void Execute(object parameter)
        {
            base.Execute(parameter);
            this.viewModel.SaveSettings();
        }
    }
}