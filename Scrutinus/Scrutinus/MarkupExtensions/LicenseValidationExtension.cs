﻿// // TPS.net TPS8 Scrutinus
// // LicenseValidationExtension.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Windows;
using System.Windows.Markup;
using DataModel.Models;

namespace Scrutinus.MarkupExtensions
{
    [MarkupExtensionReturnType(typeof(Visibility))]
    internal class LicenseValidationExtension : MarkupExtension
    {
        #region Public Properties

        public FeatueLicened WhantedFeatue { get; set; }

        #endregion

        #region Public Methods and Operators

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            if (MainWindow.License == null)
            {
                return Visibility.Collapsed;
            }

            switch (this.WhantedFeatue)
            {
                case FeatueLicened.TabletServer:
                    return MainWindow.License.TabletServer ? Visibility.Visible : Visibility.Collapsed;

                case FeatueLicened.eJudge:
                    return MainWindow.License.Ejudge ? Visibility.Visible : Visibility.Collapsed;

                case FeatueLicened.eCheckin:
                    return MainWindow.License.CheckIn ? Visibility.Visible : Visibility.Collapsed;
            }

            return Visibility.Visible;
        }

        #endregion
    }
}