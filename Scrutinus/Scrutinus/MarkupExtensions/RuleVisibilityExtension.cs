﻿// // TPS.net TPS8 Scrutinus
// // RuleVisibilityExtension.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Windows;
using System.Windows.Markup;

namespace Scrutinus.MarkupExtensions
{
    [MarkupExtensionReturnType(typeof(Visibility))]
    public class RuleVisibilityExtension : MarkupExtension
    {
        #region Public Properties

        public string RequiredRule { get; set; }

        #endregion

        #region Public Methods and Operators

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            if (MainWindow.MainWindowHandle == null)
            {
                return Visibility.Visible;
            }

            if (MainWindow.MainWindowHandle.ViewModel == null || MainWindow.MainWindowHandle.ViewModel.Event == null)
            {
                return Visibility.Collapsed;
            }

            var currentRule = MainWindow.MainWindowHandle.ViewModel.Event.RuleName;

            if (currentRule == null)
            {
                return Visibility.Collapsed;
            }

            if (this.RequiredRule.Contains(currentRule))
            {
                return Visibility.Visible;
            }

            return Visibility.Collapsed;
        }

        #endregion
    }
}