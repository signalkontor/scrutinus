﻿// // TPS.net TPS8 Scrutinus
// // ProjectorControlViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using AsyncIO;
using CsQuery.ExtensionMethods.Internal;
using DataModel.ModelHelper;
using DataModel.Models;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using NetMQ;
using NetMQ.Monitoring;
using NetMQ.Sockets;
using Newtonsoft.Json;
using Scrutinus.Annotations;
using Scrutinus.Localization;

namespace Scrutinus.ViewModels
{
    public class HeatViewModel
    {
        public string HeatName { get; set; }

        public int Number { get; set; }

        public List<Participant> Participants { get; set; }

        public DanceInRound DanceInRound { get; set; }
    }

    public class ProjectorControlViewModel : ViewModelBase, IDisposable
    {
        private PublisherSocket publisher = new PublisherSocket();

        private ResponseSocket responseSocket;

        private Thread responseSocketThread;

        private NetMQBeacon beacon = new NetMQBeacon();

        private Dictionary<AsyncSocket, string> socketDictionary = new Dictionary<AsyncSocket, string>();

        private NetMQMonitor monitor;

        private ScrutinusContext context = new ScrutinusContext();

        private int currentPricePresentationPlace;

        private Competition selectedCompetition;

        private HeatViewModel selectedHeat;

        private Round selectedRound;

        private Dispatcher dispatcher;

        private string leftUrl;

        private string rightUrl;

        private bool backgroundThreadRunning = true;

        public ProjectorControlViewModel()
        {
            this.dispatcher = Dispatcher.CurrentDispatcher;

            this.Competitions.AddRange(this.context.Competitions);

            this.ReloadCommand = new RelayCommand(this.Reload);
            this.ShowDrawOfHeatCommand = new RelayCommand(this.WriteDraw);
            this.ShowNextHeatCommand = new RelayCommand(this.NextDraw);
            this.ShowQualifiedCouplesCommand = new RelayCommand(this.QualifiedCouples);
            this.ShowPricePresentationCommand = new RelayCommand(this.PricePresentationStart);
            this.PricePresentaionNextPlaceCommand = new RelayCommand(this.ShowNextPlace);
            this.ShowEventCommand = new RelayCommand(this.ShowEvent);
            this.ShowRoundTitleCommand = new RelayCommand(this.ShowRoundTitle);
            this.ShowGeneralTextCommand = new RelayCommand(this.ShowGeneralText);

            this.ActiveClients = new ObservableCollection<HostEntry>();


            publisher.Bind("tcp://*:5556");

            monitor = new NetMQMonitor(publisher, "inproc://publisher.inproc",
                SocketEvents.All);

            monitor.Disconnected += MonitorOnDisconnected;
            monitor.Closed += (sender, args) => Debug.WriteLine($"The sockezt t {args.Address} has been closed");
            monitor.Accepted += MonitorOnAccepted;

            var monitorTask = Task.Factory.StartNew(monitor.Start);

            StartResponseSocketThread();

            // initialize the beacon
            this.beacon.Configure(6000);
            this.beacon.Publish("5556", TimeSpan.FromSeconds(5));

            Application.Current.Exit += (sender, args) =>
            {
                this.Dispose();
            };
        }

        private double currentSliderValue;

        public double CurrentSliderValue
        {
            get { return currentSliderValue; }
            set
            {
                this.currentSliderValue = value;
                SendCurrentState();
                RaisePropertyChanged(nameof(CurrentSliderValue));
            }
        }


        public ObservableCollection<Competition> Competitions { get; set; } = new ObservableCollection<Competition>();

        public ObservableCollection<Round> Rounds { get; set; } = new ObservableCollection<Round>();

        public ObservableCollection<HeatViewModel> Heats { get; set; } = new ObservableCollection<HeatViewModel>();

        public WebBrowser RightWebBrowser { get; set; }

        public WebBrowser LeftWebBrowser { get; set; }

        public bool FadeOverAutomatically { get; set; }

        public Competition SelectedCompetition
        {
            get { return this.selectedCompetition; }
            set
            {
                this.selectedCompetition = value;
                this.FillRounds();
                this.currentPricePresentationPlace = 8;
                this.RaisePropertyChanged(() => this.SelectedCompetition);
            }
        }

        public Round SelectedRound
        {
            get { return this.selectedRound; }
            set
            {
                this.selectedRound = value;
                this.FillHeats();
                this.RaisePropertyChanged(() => this.SelectedRound);
            }
        }

        public ICommand ShowDrawOfHeatCommand { get; set; }

        public ICommand ShowNextHeatCommand { get; set; }

        public ICommand ShowRoundTitleCommand { get; set; }

        public ICommand ShowPricePresentationCommand { get; set; }

        public ICommand ReloadCommand { get; set; }

        public ICommand ShowQualifiedCouplesCommand { get; set; }

        public ICommand PricePresentaionNextPlaceCommand { get; set; }

        public ICommand ShowEventCommand { get; set; }

        public ICommand ShowGeneralTextCommand {get; set; }

        public string GeneralTextHeader { get; set; }

        public string GeneralTextBody { get; set; }

        public ObservableCollection<HostEntry> ActiveClients { get; set; }

        public HeatViewModel SelectedHeat
        {
            get { return this.selectedHeat; }
            set
            {
                this.selectedHeat = value;
                this.RaisePropertyChanged(() => this.SelectedHeat);
            }
        }

        private void FillRounds()
        {
            this.SelectedRound = null;
            this.Rounds.Clear();

            if (this.selectedCompetition == null)
            {
                return;
            }

            this.Rounds.AddRange(this.selectedCompetition.Rounds);
            this.SelectedRound = this.selectedCompetition.CurrentRound;
        }

        private void FillHeats()
        {
            this.Heats.Clear();
            if (this.selectedRound == null)
            {
                return;
            }

            var heats = HeatsHelper.GetHeats(this.context, this.selectedRound.Id);

            foreach (var dance in heats.Keys.OrderBy(d => d.SortOrder))
            {
                for (var i = 1; i <= heats[dance].Count; i++)
                {
                    this.Heats.Add(new HeatViewModel()
                    {
                        HeatName = $"{dance.Dance.DanceName} - {i}. Heat",
                        Number = i,
                        Participants = heats[dance][i - 1],
                        DanceInRound = dance
                    });
                }
            }
        }

        private void MonitorOnAccepted(object sender, NetMQMonitorSocketEventArgs e)
        {
            this.socketDictionary.Add(e.Socket, e.Socket.RemoteEndPoint.Address.ToString());
        }

        private void MonitorOnDisconnected(object sender, NetMQMonitorSocketEventArgs e)
        {
            try
            {
                Debug.WriteLine($"Disconnected {e.Socket}");

                if (this.socketDictionary.ContainsKey(e.Socket))
                {
                    this.RemoveFromList(this.socketDictionary[e.Socket]);
                    this.socketDictionary.Remove(e.Socket);
                }
            }
            catch (Exception) { }
        }

        protected void StartResponseSocketThread()
        {
            responseSocketThread = new Thread(() =>
                {
                    responseSocket = new ResponseSocket("@tcp://*:5557");

                    while (backgroundThreadRunning)
                    {
                        try
                        {
                            var message = responseSocket.ReceiveFrameString();
                            responseSocket.SendFrame("OK");

                            Status status = JsonConvert.DeserializeObject<Status>(message);

                            AddToList(status.HostName, status.UrlLoaded1, status.UrlLoaded2, status.Endpoint);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e);
                        }
                    }
                })
                { IsBackground = true, Name = "ProjectorControlClientStatus"};

            responseSocketThread.Start();
        }

        private void AddToList(string hostName, string url1, string url2, string endpoint)
        {
            if (Application.Current == null)
            {
                return;
            }

            Application.Current.Dispatcher.Invoke(() =>
            {
                var entry = this.ActiveClients.AsQueryable().FirstOrDefault(h => h.HostName == hostName);

                if (entry != null)
                {
                    entry.CurrentUrl1 = url1;
                    entry.CurrentUrl2 = url2;
                    return;
                }

                this.ActiveClients.Add(new HostEntry()
                {
                    HostName = hostName,
                    CurrentUrl1 = url1,
                    CurrentUrl2 = url2,
                    Endpoint = endpoint
                });
            });
        }

        private void RemoveFromList(string endpoint)
        {
            if (Application.Current == null)
            {
                return;
            }

            Application.Current.Dispatcher.Invoke(() =>
            {
                var entry = ActiveClients.AsQueryable().FirstOrDefault(h => h.Endpoint == endpoint);

                if (entry != null)
                {
                    this.ActiveClients.Remove(entry);
                }
            });
        }

        protected void OnClosing(CancelEventArgs e)
        {
            monitor.Stop();
            beacon.Dispose();
        }

        private void Reload()
        {
            this.context = new ScrutinusContext();
            this.Competitions.Clear();
            this.Competitions.AddRange(this.context.Competitions);
        }

        private void WriteDraw()
        {
            Navigate($"http://localhost:{Settings.Default.NancyServerPort}/projector/drawing/{SelectedRound.Id}?danceId={SelectedHeat.DanceInRound.Id}&heatIndex=0");
            SendCurrentState();
        }

        private void NextDraw()
        {
            if (this.selectedHeat == null)
            {
                return;
            }

            var index = this.Heats.IndexOf(this.selectedHeat);
            index++;

            if (index < this.Heats.Count)
            {
                this.SelectedHeat = this.Heats[index];
                // get index within dance
                Navigate($"http://localhost:{Settings.Default.NancyServerPort}/projector/drawing/{SelectedRound.Id}?danceId={SelectedHeat.DanceInRound.Id}&heatIndex={SelectedHeat.Number - 1}");
                SendCurrentState();
            }
        }

        private void QualifiedCouples()
        {
            if (this.SelectedRound == null)
            {
                return;
            }

            Navigate($"http://localhost:{Settings.Default.NancyServerPort}/projector/qualified/{SelectedRound.Id}?skip=0");
            SendCurrentState();
        }

        private void PricePresentationStart()
        {
            // Start price presentation with empty screen
            this.currentPricePresentationPlace = this.SelectedRound.Qualifieds.Count + 1;
            this.ShowNextPlace();
        }

        private void ShowNextPlace()
        {
            if (this.currentPricePresentationPlace > 0)
            {
                this.currentPricePresentationPlace--;
            }

            Navigate($"http://localhost:{Settings.Default.NancyServerPort}/projector/pricepresentation/{SelectedRound.Competition.Id}?place={currentPricePresentationPlace}");
            SendCurrentState();

        }

        private void ShowRoundTitle()
        {
            if (this.SelectedRound == null)
            {
                return;
            }

            Navigate($"http://localhost:{Settings.Default.NancyServerPort}/projector/round/{SelectedRound.Id}");
            SendCurrentState();
        }

        private void ShowEvent()
        {
            if (!this.Competitions.Any())
            {
                return;
            }

            Navigate($"http://localhost:{Settings.Default.NancyServerPort}/projector/event");
            SendCurrentState();
        }

        private void ShowGeneralText()
        {
            var stream = new StreamWriter(Settings.Default.ProjectorClientPath + @"\GeneralText.txt");

            stream.WriteLine(this.GeneralTextHeader ?? "");
            stream.WriteLine(this.GeneralTextBody ?? "");

            stream.Close();
        }

        private void Navigate(string url)
        {
            Debug.WriteLine($"Navigating to {url}");

            if (this.FadeOverAutomatically)
            {
                if (CurrentSliderValue < 0.5)
                {
                    CurrentSliderValue = 1;
                }
                else
                {
                    CurrentSliderValue = 0;
                }
            }

            if (CurrentSliderValue < 0.5)
            {
                RightWebBrowser.Source = new Uri(url);
                rightUrl = url;
            }
            else
            {
                LeftWebBrowser.Source = new Uri(url);
                leftUrl = url;
            }
        }

        private void SendCurrentState()
        {
            Command command = new Command()
            {
                Opacity = currentSliderValue,
                AddressBrowser1 = leftUrl,
                AddressBrowser2 = rightUrl
            };

            publisher.SendMoreFrame("opacity").SendFrame(JsonConvert.SerializeObject(command));
        }

        public void Dispose()
        {
            publisher?.Dispose();
            responseSocket?.Dispose();
            beacon?.Dispose();
            monitor?.Dispose();
            context?.Dispose();
            RightWebBrowser?.Dispose();
            LeftWebBrowser?.Dispose();

            backgroundThreadRunning = false;
            responseSocketThread.Abort();
        }
    }

    public class Status
    {
        public string HostName { get; set; }

        public string UrlLoaded1 { get; set; }

        public string UrlLoaded2 { get; set; }

        public string Endpoint { get; set; }
    }

    public class HostEntry : INotifyPropertyChanged
    {
        private string hostName;

        private string currentUrl1;

        private string currentUrl2;

        public string HostName
        {
            get => hostName;
            set
            {
                hostName = value;
                OnPropertyChanged(nameof(HostName));
            }
        }

        public string CurrentUrl1
        {
            get => currentUrl1;
            set
            {
                currentUrl1 = value;
                OnPropertyChanged(nameof(CurrentUrl1));
            }
        }

        public string CurrentUrl2
        {
            get => currentUrl2;
            set
            {
                currentUrl2 = value;
                OnPropertyChanged(nameof(CurrentUrl2));
            }
        }

        public string Endpoint { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }


    }

    public class Command
    {
        public string AddressBrowser1 { get; set; }

        public string AddressBrowser2 { get; set; }

        public double Opacity { get; set; }

    }

}
