// // TPS.net TPS8 Scrutinus
// // CompetitionElement.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System.ComponentModel;
using System.Windows.Controls;
using DataModel.Models;

namespace Scrutinus.ViewModels
{
    public class CompetitionElement : INotifyPropertyChanged
    {
        #region Public Events

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Public Methods and Operators

        public override string ToString()
        {
            return this.Competition.Title;
        }

        #endregion

        #region Methods

        protected void RaiseEvent(string Property)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(Property));
            }
        }

        #endregion

        #region Fields

        private Competition competition;

        private Round currentRound;

        #endregion

        #region Public Properties

        public Competition Competition
        {
            get
            {
                return this.competition;
            }
            set
            {
                this.competition = value;
                this.RaiseEvent("Competition");
            }
        }

        public Page CurrentPage { get; set; }

        public Round CurrentRound
        {
            get
            {
                return this.currentRound;
            }
            set
            {
                this.currentRound = value;
                this.RaiseEvent("CurrentRound");
                this.RaiseEvent("RoundTitle");
            }
        }

        #endregion
    }
}