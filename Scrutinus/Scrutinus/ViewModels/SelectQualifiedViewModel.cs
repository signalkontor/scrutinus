﻿// // TPS.net TPS8 Scrutinus
// // SelectQualifiedViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using DataModel;
using DataModel.Models;
using GalaSoft.MvvmLight.Command;
using General.Rules.PointCalculation;
using mobileControl.Helper;
using Scrutinus.Dialogs;
using Scrutinus.Dialogs.DialogViewModels;
using Scrutinus.Localization;
using Scrutinus.Navigation;
using Scrutinus.Reports;

namespace Scrutinus.ViewModels
{
    internal class SelectQualifiedViewModel : BaseViewModel
    {
        #region Fields

        private readonly int currentRoundId;

        private bool overrideCouplesNotSelected = false;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Dummy Constructor to use with Blend
        /// </summary>
        public SelectQualifiedViewModel()
        {
        }

        public SelectQualifiedViewModel(int CurrentRoundId)
        {
            this.currentRoundId = CurrentRoundId;

            this.Round = this.context.Rounds.SingleOrDefault(r => r.Id == CurrentRoundId);

            this.NextRoundTypeSelectionEnabled = true;

            var source =
                this.context.Qualifieds.Where(q => q.Round.Id == this.currentRoundId)
                    .OrderBy(q => q.PlaceFrom)
                    .ThenBy(q => q.Participant.Number)
                    .ToList();

            this.Couples = new ObservableCollection<Qualified>(source);

            if (!this.Couples.Any(q => q.QualifiedNextRound) 
                && this.Round?.MarksFrom != null 
                && this.Round.MarksTo.HasValue 
                && this.Round.MarksFrom.Value == this.Round.MarksTo.Value)
            {
                // We try to autoselect the qualified couples based on required marks:
                this.SelectedCouple = this.Couples.Take(this.Round.MarksFrom.Value).LastOrDefault();

                if (this.SelectedCouple != null)
                {
                    this.SelectCouplesNextRound();
                }
            }


            this.NextRoundTypes = ComboBoxItemSources.GetNextRoundTypes();

            this.NextRoundType = this.NextRoundTypes[0];

            if (this.Round.Competition.FirstRoundType == RoundTypes.Redance && this.Round.Number == 1)
            {
                this.NextRoundType = this.NextRoundTypes[1];
            }
            else
            {
                this.NextRoundTypes.RemoveAt(1);
            }

            this.DisqualifyCommand = new RelayCommand(this.Disqualify);

            this.PrintQualifiedResultCommand = new RelayCommand(this.PrintQualifiedResults);

            this.SylabusWarningCommand = new RelayCommand(this.SylabusWarning);

            this.SplitCompetitionCommand = new RelayCommand(this.SplitCompetition);

            this.SelectCouplesForNextRoundCommand = new RelayCommand(this.SelectCouplesForNextRound);

            this.context.SaveChanges();
        }

        #endregion

        #region Public Properties

        public ObservableCollection<Qualified> Couples { get; set; }

        public Round Round { get; set; }

        public string MarkingString
        {
            get
            {
                if (this.Round == null)
                {
                    return "";
                }

                if (this.Round.MarksFrom == this.Round.MarksTo)
                {
                    return string.Format(LocalizationService.Resolve(() => Text.MarkingEqualMarks), this.Round.MarksFrom);
                }
                else
                {
                    return string.Format(LocalizationService.Resolve(() => Text.MarkingNotEqualMarks), this.Round.MarksFrom, this.Round.MarksTo);
                }
            }
        }

        public ScrutinusContext Context {
            get
            {
                return this.context; 
            }
        }

        public ICommand DisqualifyCommand { get; set; }

        public ICommand PrintQualifiedResultCommand { get; set; }

        public ItemValue NextRoundType { get; set; }

        public bool NextRoundTypeSelectionEnabled { get; set; }

        public List<ItemValue> NextRoundTypes { get; set; }

        public ICommand SelectCouplesForNextRoundCommand { get; set; }

        public Qualified SelectedCouple { get; set; }

        public ICommand SylabusWarningCommand { get; set; }

        public ICommand SplitCompetitionCommand { get; set; }

        #endregion

        #region Public Methods and Operators

        public override void SaveData()
        {
            if (!this.Validate())
            {
                return;
            }

            this.context.SaveChanges();
            // Create a new context to be sure we access the right data from the database...
            this.context = new ScrutinusContext();

            var currentRound = this.context.Rounds.Single(r => r.Id == this.currentRoundId);

            if (currentRound.EjudgeEnabled)
            {
                // at this point, we should clear the devices:
                currentRound.EjudgeEnabled = false;
                ScrutinusCommunicationHandler.ClearDevices(currentRound);
            }
            

            var competition = currentRound.Competition;

            // We create a new round according to our settings.
            // When redance -> create a redance round
            if (this.NextRoundType.Value == RoundTypes.Redance || this.NextRoundType.Value == RoundTypes.SeparationRound)
            {
                var redance = this.CreateRedanceRound(currentRound.Number + 1, currentRound.Competition);

                if (this.NextRoundType.Value == RoundTypes.SeparationRound)
                {
                    // We only add the couples that are on the same place behind the qualified couples.
                    var samePlace =
                        this.Couples.Where(c => !c.QualifiedNextRound).OrderBy(p => p.PlaceFrom).First().PlaceFrom;
                    var redanceCouples =
                        this.Couples.Where(c => !c.QualifiedNextRound && c.PlaceFrom == samePlace).ToList();
                    this.AddCouplesToRound(redance, redanceCouples);
                }

                competition.CurrentRound = redance;

                var secondRound = this.CreateNextRound(redance.Number + 1, redance.Competition, RoundTypes.QualificationRound, false);
                this.AddStarCouples(secondRound, 1);
            }

            if (this.NextRoundType.Value == RoundTypes.QualificationRound)
            {
                var qualified = this.Couples.Count(c => c.QualifiedNextRound);
                if (!currentRound.IsRedanceRound)
                {
                    var round = this.CreateNextRound(
                        currentRound.Number + 1,
                        currentRound.Competition,
                        qualified < 8 ? RoundTypes.Final : RoundTypes.QualificationRound,
                        true);

                    round.IsRedanceRound = false;
                    round.Name = "2nd Round";

                    var competitionHasRedance = competition.Rounds.Any(r => r.RoundType.Id == RoundTypes.Redance);

                    if (round.Number == 4 && competitionHasRedance)
                    {
                        // Just in case, we have two stars, we add them to the forth round (2 = redance, 3 = 2. Round, 4 = 3. round)
                        this.AddStarCouples(round, 2);
                    }

                    if (round.Number == 2 && !competitionHasRedance)
                    {
                        this.AddStarCouples(round, 1);
                    }

                    if (round.Number == 3 && !competitionHasRedance)
                    {
                        this.AddStarCouples(round, 2);
                    }

                    competition.CurrentRound = round;
                }
                else
                {
                    // Current Round is a Redance Round ...
                    // We have the new round already. We just add the qualified couples and stars etc.
                    var secondRound =
                        this.context.Rounds.SingleOrDefault(
                            r => r.Competition.Id == currentRound.Competition.Id && r.Number == currentRound.Number + 1);

                    if (secondRound == null)
                    {
                        throw new Exception("Second Round is null, did you delete the second round???");
                    }

                    this.AddCouplesAfterRedance(secondRound);
                    competition.CurrentRound = secondRound;
                    // set places of dropped out
                    this.SetPlacesofDroppedOutCouples(secondRound.Competition.Id);
                }
            }

            if (this.NextRoundType.Value == RoundTypes.ABFinal)
            {
                var bFinal = this.CreateNextRound(currentRound.Number + 1, competition, RoundTypes.ABFinal, false);
                bFinal.Name = "B-Final";
                bFinal.PlaceOffset = this.Couples.Count(c => c.QualifiedNextRound);
                this.AddCouplesToRound(bFinal, this.Couples.Where(c => !c.QualifiedNextRound));
                competition.CurrentRound = bFinal;
                this.context.SaveChanges();

                var aFinal = this.CreateNextRound(currentRound.Number + 2, competition, RoundTypes.Final, false);
                aFinal.PlaceOffset = 0;
                aFinal.Name = "A-Final";
                this.AddCouplesToRound(aFinal, this.Couples.Where(c => c.QualifiedNextRound));
                this.context.SaveChanges();
            }

            this.context.SaveChanges();
        }

        public void SelectCouplesNextRound()
        {
            if (this.SelectedCouple == null)
            {
                return;
            }

            // Reset any not qualified couples
            foreach (var q in this.context.Qualifieds.Where(q => q.Round.Id == this.currentRoundId))
            {
                q.QualifiedNextRound = false;
            }

            var qualified =
                this.context.Qualifieds.Where(
                    q => q.Round.Id == this.currentRoundId && q.PlaceTo <= this.SelectedCouple.PlaceTo);

            foreach (var qualified1 in qualified)
            {
                qualified1.QualifiedNextRound = true;
            }

            this.context.SaveChanges();

            var source =
                this.context.Qualifieds.Where(q => q.Round.Id == this.currentRoundId).OrderBy(q => q.PlaceFrom).ToList();
            this.Couples.Clear();

            foreach (var q in source)
            {
                this.Couples.Add(q);
            }
        }

        public override bool Validate()
        {
            if (this.context.Qualifieds.Any(q => q.Round.Id == this.currentRoundId && q.QualifiedNextRound))
            {
                return true;
            }

            // We splitted this competition, so let's continue:
            if (this.overrideCouplesNotSelected)
            {
                return true;
            }

            MainWindow.MainWindowHandle.ShowMessage(
                LocalizationService.Resolve(() => Text.NoCoupleSelectedForQualification));
            return false;
        }

        #endregion

        #region Methods

        private void AddCouplesAfterRedance(Round secondRound)
        {
            // We add all our selected couples from the redance + star couples (if we have any ...)
            foreach (var qualified in this.Couples.Where(c => c.QualifiedNextRound))
            {
                if (secondRound.Qualifieds.All(q => q.Participant.Number != qualified.Participant.Number))
                {
                    // we only add if the couple is not yet in the list
                    var participant = this.context.Participants.Single(p => p.Id == qualified.Participant.Id);
                    participant.QualifiedRound = secondRound;
                    secondRound.Qualifieds.Add(
                        new Qualified
                            {
                                Participant = participant,
                                PlaceFrom = 0,
                                PlaceTo = 0,
                                Points = 0,
                                QualifiedNextRound = false,
                                Round = secondRound
                            });
                }
            }

            this.AddStarCouples(secondRound, 1);
                
            this.context.SaveChanges();
        }

        private void AddCouplesToRound(Round round, IEnumerable<Qualified> qualifiedRedance)
        {
            foreach (var q in round.Qualifieds.ToList())
            {
                this.context.Qualifieds.Remove(q);
            }

            round.Qualifieds.Clear();
            this.context.SaveChanges();

            foreach (var qualified in qualifiedRedance)
            {
                var participant = this.context.Participants.Single(p => p.Id == qualified.Participant.Id);
                round.Qualifieds.Add(
                    new Qualified
                        {
                            Participant = participant,
                            Round = round,
                            PlaceFrom = 0,
                            PlaceTo = 0,
                            Points = 0,
                            QualifiedNextRound = false
                        });
            }

            this.context.SaveChanges();

            foreach (var qualified in round.Qualifieds)
            {
                var participant = qualified.Participant
                                          ?? this.context.Participants.Single(p => p.Id == qualified.Participant.Id);
                participant.QualifiedRound = round;
            }
            this.context.SaveChanges();
        }

        private void AddStarCouples(Round round, int numberStars)
        {
            var stars = round.Competition.Participants.Where(p => p.Star == numberStars && p.State == CoupleState.Dancing);

            // just in case, remove any star couple we have right now:
            var oldQualifiedStars = round.Qualifieds.Where(q => q.Participant.Star == numberStars).ToList();
            foreach (var oldQualifiedStar in oldQualifiedStars)
            {
                round.Qualifieds.Remove(oldQualifiedStar);
                this.context.Qualifieds.Remove(oldQualifiedStar);
            }

            foreach (var starCouple in stars)
            {
                round.Qualifieds.Add(
                    new Qualified
                        {
                            Participant = starCouple,
                            PlaceFrom = 0,
                            PlaceTo = 0,
                            Points = 0,
                            QualifiedNextRound = false,
                            Round = round
                        });
            }

            this.context.SaveChanges();
        }

        private Round CreateNextRound(int number, Competition competition, int roundTypeId, bool dropOutNotQualified)
        {
            var round = this.context.Rounds.SingleOrDefault(r => r.Competition.Id == competition.Id && r.Number == number);

            if (round == null)
            {
                round = NavigationHelper.CreateRound(this.context, roundTypeId, number, 0, competition.Id, null);
            }
            else
            {
                round.PlaceOffset = round.Number == 1 ? competition.Participants.Count(p => p.Star > 0 && p.State == CoupleState.Dancing) : 
                    round.Number == 2 ? competition.Participants.Count(p => p.Star > 1 && p.State == CoupleState.Dancing) : 0;

                // We have a round allready, so lets remove any qualified couples for this round and continue
                foreach (var qualified in round.Qualifieds.ToList())
                {
                    this.context.Qualifieds.Remove(qualified);
                    round.Qualifieds.Remove(qualified);
                }
            }

            this.context.SaveChanges();

            // Add all qualified couples
            foreach (var q in round.Qualifieds.ToList())
            {
                this.context.Qualifieds.Remove(q);
            }
            round.Qualifieds.Clear();
            this.context.SaveChanges();

            foreach (var qualified in this.Couples.Where(c => c.QualifiedNextRound))
            {
                var participant = this.context.Participants.Single(p => p.Id == qualified.Participant.Id);
                participant.PlaceFrom = null;
                participant.PlaceTo = null;
                participant.State = CoupleState.Dancing;

                round.Qualifieds.Add(
                    new Qualified
                        {
                            Participant = participant,
                            Round = round,
                            PlaceFrom = 0,
                            PlaceTo = 0,
                            Points = 0,
                            QualifiedNextRound = false
                        });
            }

            this.context.SaveChanges();

            foreach (var qualified in round.Qualifieds)
            {
                qualified.Participant.State = CoupleState.Dancing;
                qualified.Participant.PlaceFrom = null;
                qualified.Participant.PlaceTo = null;
                qualified.Participant.QualifiedRound = round;
            }
            // Set places of the couples dropped out
            if (dropOutNotQualified)
            {
                var notqualifieds =
                    this.context.Qualifieds.Where(q => q.Round.Id == this.currentRoundId && !q.QualifiedNextRound);

                foreach (var notQualified in notqualifieds)
                {
                    notQualified.Participant.PlaceFrom = notQualified.PlaceFrom;
                    notQualified.Participant.PlaceTo = notQualified.PlaceTo;
                    notQualified.Participant.State = CoupleState.DroppedOut;
                    notQualified.Participant.Points = notQualified.Points;
                }
            }

            this.context.SaveChanges();

            // Recalculate ranking points of all couples:
            var participants =
                this.context.Participants.Where(p => p.Competition.Id == round.Competition.Id);
            var calculator = PointCalculatorFactory.GetCalculator(round.Competition);
            calculator.CalculateRankingPoints(participants, this.context);

            if (participants.Any(p => p.ClimbUp))
            {
                foreach (var participant in participants.Where(p => p.ClimbUp))
                {
                    var participantId = participant.Id;
                    var otherStarts =
                        participant.Couple.Participants.Where(
                            p =>
                            p.Competition.Section.Id == round.Competition.Section.Id && p.Id != participantId
                            && p.State == CoupleState.Dancing);

                    // Check, if this participant has other competitions
                    if (otherStarts.Any())
                    {
                        foreach (var otherStart in otherStarts)
                        {
                            otherStart.State = CoupleState.Excused;
                        }
                    }
                    var namelist = string.Join(
                        ", ",
                        round.Qualifieds.Select(q => q.Participant)
                            .Where(p => p.ClimbUp)
                            .Select(p => p.NiceNameWithNumber));
                    MainWindow.MainWindowHandle.ShowMessage(
                        "Es gibt Aufsteiger in diesem Turnier:" + namelist
                        + ". Doppelstarter wurden auf Entschuldigt gesetzt.");
                }
            }

            this.context.SaveChanges();

            return round;
        }

        private Round CreateRedanceRound(int number, Competition competition)
        {
            var redance = this.context.Rounds.SingleOrDefault(r => r.Competition.Id == competition.Id && r.Number == number);

            var offset = this.Couples.Count(c => c.QualifiedNextRound);
            offset += competition.Participants.Count(p => p.Star > 0 && p.State == CoupleState.Dancing);

            if (redance == null)
            {
                redance = NavigationHelper.CreateRound(
                    this.context,
                    RoundTypes.Redance,
                    number,
                    offset,
                    competition.Id,
                    null);
                redance.IsRedanceRound = true;

                this.context.SaveChanges();
            }

            redance.PlaceOffset = offset;
            redance.IsRedanceRound = true;

            // Add all not qualified couples to 
            var qualifiedRedance =
                this.context.Qualifieds.Where(
                    q => q.QualifiedNextRound == false && q.Round.Id == competition.CurrentRound.Id).ToList();
                
            // Reset places of redance couples:
            foreach (var qualified in qualifiedRedance)
            {
                qualified.Participant.PlaceFrom = null;
                qualified.Participant.PlaceTo = null;
                qualified.Participant.State = CoupleState.Dancing;
            }

            this.context.SaveChanges();

            this.AddCouplesToRound(redance, qualifiedRedance);

            return redance;
        }

        private void Disqualify()
        {
            if (this.SelectedCouple == null)
            {
                MainWindow.MainWindowHandle.ShowMessage(
                    LocalizationService.Resolve(() => Text.SelectCoupleToDisqualify));
                return;
            }

            var diqualifyPage =
                new DisqualifiyCouple(
                    new DisqualifyCoupleViewModel(this.context, this.currentRoundId, this.SelectedCouple.Participant.Id));

            MainWindow.MainWindowHandle.ShowDialog(diqualifyPage, this.UpdateAfterDisqualification, 400, 200);
        }

        private void SelectCouplesForNextRound()
        {
            if (this.SelectedCouple == null)
            {
                MainWindow.MainWindowHandle.ShowMessage(
                    LocalizationService.Resolve(() => Text.NoCoupleSelectedForQualification));
                return;
            }

            this.SelectCouplesNextRound();
        }

        private void SetPlacesofDroppedOutCouples(int competitionId)
        {
            // how many star-couples do we have?

            foreach (var qualified in this.Couples.Where(c => !c.QualifiedNextRound))
            {
                var participant =
                    this.context.Participants.SingleOrDefault(p => p.Id == qualified.Participant.Id);
                participant.State = CoupleState.DroppedOut;
                participant.PlaceFrom = qualified.PlaceFrom;
                participant.PlaceTo = qualified.PlaceTo;
            }

            this.context.SaveChanges();
        }

        private void SylabusWarning()
        {
            var round = this.context.Rounds.Single(r => r.Id == this.currentRoundId);
            var dialog = new SylabusWarningDialog(
                this.context,
                this.Couples.Select(c => c.Participant).OrderBy(o => o.Number).ToList(),
                this.SelectedCouple != null ? this.SelectedCouple.Participant : null,
                round);

            MainWindow.MainWindowHandle.ShowDialog(dialog, this.UpdateAfterDisqualification, 400, 200);
        }

        private void SplitCompetition()
        {
            var dialog = new SplitCompetitionDialog(this.context, this.context.Rounds.Single(r => r.Id == this.currentRoundId).Competition);
            MainWindow.MainWindowHandle.ShowDialog(dialog, this.OnDialogClosing, 300, 300);
        }

        private void OnDialogClosing(object dialog)
        {
            var splitCompetitionDialog = dialog as SplitCompetitionDialog;
            if (splitCompetitionDialog != null)
            {
                if (splitCompetitionDialog.ViewModel.SplitPerformed)
                {
                    // Move to next step in competition
                    this.overrideCouplesNotSelected = true;
                    MainWindow.MainWindowHandle.NextState(this.Round.Competition.Id);
                    this.overrideCouplesNotSelected = false;
                }
            }
        }

        private void UpdateAfterDisqualification(object sender)
        {
            // Save Sylabus / Disqualification data.
            this.context.SaveChanges();

            var source =
                this.context.Qualifieds.Where(q => q.Round.Id == this.currentRoundId).OrderBy(q => q.PlaceFrom).ToList();
            this.Couples.Clear();

            foreach (var qualified in source)
            {
                this.Couples.Add(qualified);
            }
        }

        private void PrintQualifiedResults()
        {
            PrintHelper.PrintPlacingsOfQualifiedCouples(this.Round, "Standard", 1, true);
        }

        #endregion
    }
}