// // TPS.net TPS8 Scrutinus
// // DanceResult.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System.ComponentModel;

namespace Scrutinus.ViewModels
{
    public class DanceResult : INotifyPropertyChanged
    {
        #region Fields

        private double _place;

        #endregion

        #region Public Events

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Methods

        protected void RaiseEvent(string name)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        #endregion

        #region Public Properties

        public int DanceId { get; set; }

        public int ParticipantId { get; set; }

        public double Place
        {
            get
            {
                return this._place;
            }
            set
            {
                this._place = value;
                this.RaiseEvent("Place");
            }
        }

        public double Sum { get; set; }

        #endregion
    }
}