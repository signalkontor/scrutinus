﻿// // TPS.net TPS8 Scrutinus
// // EditParticipantsViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using CheckIn.View;
using CsQuery.ExtensionMethods.Internal;
using DataModel;
using DataModel.Messages;
using DataModel.Models;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Scrutinus.Dialogs;
using Scrutinus.ExternalApi;
using Scrutinus.Localization;
using Scrutinus.MyHeats;
using Scrutinus.Pages.Teams;

namespace Scrutinus.ViewModels
{
    public class EditParticipantsViewModel : ViewModelBase
    {
        #region Constructors and Destructors

        public EditParticipantsViewModel()
        {
            this.context = new ScrutinusContext();
            this.dispatcher = Dispatcher.CurrentDispatcher;

            this.Couples = new ObservableCollection<Couple>(this.context.Couples);
            this.Countries = this.context.CountryCodes.ToList();

            this.AddCommand = new RelayCommand(this.Add);
            this.DeleteCommand = new RelayCommand(this.Delete);
            this.SaveCommand = new RelayCommand(this.Save);
            this.ShowTeamCommand = new RelayCommand(this.ShowTeam);
            this.EditStartbuchCommand = new RelayCommand(this.EditStartbuch);
            this.ClearSearchCommand = new RelayCommand(this.ClearSearch);
            this.CheckWdsfApiCommand = new RelayCommand(this.CheckWdsfApi);
            this.SetNumberInEventCommand = new RelayCommand(this.SetNumbersInEvent);
            this.SwitchPartnersCommand = new RelayCommand(this.SwitchPartners);
            this.SetExcusedCommand = new RelayCommand(this.SetToExcused);
            this.SetSingleCompetitioneExcusedCommand = new RelayCommand<int>(this.SetSingleCompetitioneExcused);
            this.SetSingleCompetitionMissingCommand = new RelayCommand<int>(this.SetSingleCompetitionMissing);
            this.SetSingleCompetitionDancingCommand = new RelayCommand<int>(this.SetSingleCompetitionDancing);
            this.UploadCouplesCommand = new RelayCommand(this.UploadCouples);

            this.AvailableCompetitions =
                new ObservableCollection<SelectCompetitionViewModel>(
                    this.context.Competitions.ToList().Select(
                        s => new SelectCompetitionViewModel(this) { Competition = s, IsSelected = false }));
        }

        #endregion

        #region Fields

        private readonly ScrutinusContext context;

        private readonly Dispatcher dispatcher;

        private string searchText = "";

        private Couple selectedCouple;

        private bool uploadingMyCouples;

        #endregion

        #region Public Properties

        public ICommand AddCommand { get; set; }

        public ICommand SetExcusedCommand { get; set; }

        public ICommand SetSingleCompetitionMissingCommand { get; set; }

        public ICommand SetSingleCompetitionDancingCommand { get; set; }

        public ICommand SetSingleCompetitioneExcusedCommand { get; set; }

        public ObservableCollection<SelectCompetitionViewModel> AvailableCompetitions { get; set; }

        public ICommand CheckWdsfApiCommand { get; set; }

        public ICommand ClearSearchCommand { get; set; }

        public List<CountryCode> Countries { get; set; }

        public ObservableCollection<Couple> Couples { get; set; }

        public ICommand DeleteCommand { get; set; }

        public ICommand EditStartbuchCommand { get; set; }

        public ICommand SaveCommand { get; set; }

        public ICommand UploadCouplesCommand { get; set; }

        public string SearchText
        {
            get
            {
                return this.searchText;
            }
            set
            {
                this.searchText = value;
                IEnumerable<Couple> filteredCouples;

                if (string.IsNullOrWhiteSpace(this.searchText))
                {
                    filteredCouples = this.context.Couples;
                }
                else
                {
                    filteredCouples = this.context.Couples.ToList().Where(c =>
                        (c.FirstMan?.ToLower() + c.LastMan?.ToLower() + c.FirstWoman?.ToLower() + c.LastWoman?.ToLower() + c.Country?.ToLower()).Contains(value.ToLower()));
                }


                this.Couples.Clear();
                this.Couples.AddRange(filteredCouples);

                this.RaisePropertyChanged(() => this.SearchText);
            }
        }

        public Couple SelectedCouple
        {
            get
            {
                return this.selectedCouple;
            }
            set
            {
                this.SaveCouple();

                this.selectedCouple = value;

                foreach (var selectCompetitionViewModel in this.AvailableCompetitions)
                {
                    selectCompetitionViewModel.IsSelected = false;
                    selectCompetitionViewModel.CoupleState = CoupleState.Unknown;
                }

                if (this.SelectedCouple != null)
                {
                    foreach (var participant in this.selectedCouple.Participants)
                    {
                        var viewModel =
                            this.AvailableCompetitions.FirstOrDefault(c => c.Competition.Id == participant.Competition.Id);

                        if (viewModel == null)
                        {
                            continue;
                        }

                        viewModel.IsSelected = true;
                        viewModel.CoupleState = participant.State;
                        viewModel.ParticipantId = participant.Id;
                    }
                }

                this.RaisePropertyChanged(() => this.SelectedCouple);
            }
        }

        public ICommand SetNumberInEventCommand { get; set; }

        public ICommand ShowTeamCommand { get; set; }

        public ICommand SwitchPartnersCommand { get; set; }

        public bool UploadingMyCouples
        {
            get { return uploadingMyCouples; }
            set
            {
                uploadingMyCouples = value;
                RaisePropertyChanged(nameof(UploadingMyCouples));
            }
        }

        #endregion

        #region Methods

        private void Add()
        {
            var couple = new Couple();
            this.Couples.Add(couple);
            this.context.Couples.Add(couple);
            this.SelectedCouple = couple;
        }

        private void CheckWdsfApi()
        {
            var task = Task.Factory.StartNew(
                () =>
                    {
                        var apiClient = new WdsfApiClient(this.context, null);

                        apiClient.CheckCouples(this.Couples);
                    });
        }

        private void ClearSearch()
        {
            this.SearchText = "";
        }

        private void UploadCouples()
        {
            this.UploadingMyCouples = true;

            var task = Task.Factory.StartNew(() =>
            {
                var service = new MyHeatsService();

                service.UploadCouples();

                this.dispatcher.Invoke(() => { this.UploadingMyCouples = false; });
            });
        }

        private void Delete()
        {
            if (this.SelectedCouple != null)
            {
                if (this.context.Participants.Any(p => p.Couple.Id == this.SelectedCouple.Id))
                {
                    MainWindow.MainWindowHandle.ShowMessage(LocalizationService.Resolve(() => Text.CoupleAssigned));
                    return;
                }

                this.context.Couples.Remove(this.SelectedCouple);
                this.Couples.Remove(this.SelectedCouple);
                this.SelectedCouple = null;
            }
        }

        // called, when SelectedCouple changes

        private void EditStartbuch()
        {
            if (this.SelectedCouple == null)
            {
                return;
            }

            var dialog = new EditStartbuchDialog(this.context, this.SelectedCouple);
            MainWindow.MainWindowHandle.ShowDialog(dialog, null, 300, 300);
        }

        private void Save()
        {
            this.SaveCouple();
            this.context.SaveChanges();
        }

        private void SaveCouple()
        {
            if (this.SelectedCouple == null)
            {
                return;
            }

            // Check, if we need to change the start-numbers of participants:
            if (this.selectedCouple.NumberInEvent.HasValue && this.selectedCouple.NumberInEvent.Value > 0)
            {
               var participants = this.context.Participants.Where(p => p.Couple.Id == this.selectedCouple.Id);

                foreach (var participant in participants)
                {
                    participant.Number = this.selectedCouple.NumberInEvent.Value;
                    Messenger.Default.Send(new StartlistChangesMessage { CompetitionId = participant.Competition.Id });
                }

                this.context.SaveChanges();
            }

            // Check the selected competitions:
            var selectedCompetitions =
                this.AvailableCompetitions.Where(a => a.IsSelected).Select(s => s.Competition);

            foreach (var competition in selectedCompetitions)
            {
                if (this.SelectedCouple.Participants.All(p => p.Competition.Id != competition.Id))
                {
                    var startbook = this.SelectedCouple.Startbooks.FirstOrDefault(s => s.Section.Id == competition.Section.Id);

                    // we have to add this competition
                    var participant = new Participant
                                          {
                                              AgeGroup = competition.AgeGroup,
                                              Class = startbook?.Class ?? competition.Class,
                                              Couple = this.SelectedCouple,
                                              Competition = competition,
                                              Number = this.SelectedCouple.NumberInEvent ?? 0,
                                              TargetClass = startbook?.NextClass,
                                              TargetPlacings = startbook?.TargetPlacings,
                                              TargetPoints = startbook?.TargetPoints,
                                              NewClass = null,
                                              NewPlacings = 0,
                                              NewPoints = 0,
                                              OriginalPlacings = startbook?.OriginalPlacings,
                                              OriginalPoints = startbook?.OriginalPoints,
                                              PlacingsUpto = startbook?.PlacingsUpto,
                                              State = CoupleState.Dancing
                                          };

                    // this.context.Participants.Add(participant);
                    this.SelectedCouple.Participants.Add(participant);
                    this.context.SaveChanges();

                    Messenger.Default.Send(new StartlistChangesMessage { CompetitionId = competition.Id });
                }
            }
            // now we check what to remove:
            var remove = new List<Participant>();
            foreach (var participant in this.SelectedCouple.Participants)
            {
                if (!selectedCompetitions.Any(c => c.Id == participant.Competition.Id))
                {
                    remove.Add(participant);
                }
            }

            foreach (var participant in remove)
            {
                var competitionId = participant.Competition.Id;
                this.SelectedCouple.Participants.Remove(participant);
                this.context.Participants.Remove(participant);
                this.context.SaveChanges();
                Messenger.Default.Send(new StartlistChangesMessage { CompetitionId = competitionId });
            }
        }

        private void SetNumbersInEvent()
        {
            var dialog = new SetStartNumberDialog();
            MainWindow.MainWindowHandle.ShowDialog(dialog, this.SetStartNumber, 300, 300);
        }

        private void SetToExcused()
        {
            if (this.SelectedCouple == null)
            {
                return;
            }

            var message = "";

            foreach (
                var participant in
                    this.SelectedCouple.Participants.Where(
                        p => (p.State == CoupleState.Dancing || p.State == CoupleState.Missing) && p.Competition.CurrentRound == null))
            {
                participant.State = CoupleState.Excused;
            }

            message = string.Join(", ",
                this.SelectedCouple.Participants.Where(p => p.State == CoupleState.Excused)
                    .Select(m => m.Competition.Title + $" ( {m.Competition.StartTime?.ToString("dd.MM")} )"));

            this.context.SaveChanges();

            MainWindow.MainWindowHandle.ShowMessage(message, Text.CoupleIsExcused);
        }

        private void SetStartNumber(object result)
        {
            var dialog = result as SetStartNumberDialog;
            if (dialog == null)
            {
                return;
            }
            if (dialog.FirstStartNumber == 0 || !dialog.Result)
            {
                return;
            }

            var number = dialog.FirstStartNumber;

            var couples = this.Couples.ToList();
            var coupleCount = this.Couples.Count;

            switch (dialog.SortOrder.SortOrder)
            {
                case SortOrders.None:
                    break;
                case SortOrders.FirstNameMan:
                    couples = couples.OrderBy(p => p.FirstMan).ToList();
                    break;
                case SortOrders.LastNameMan:
                    couples = couples.OrderBy(p => p.LastMan).ToList();
                    break;
                case SortOrders.FirstNameWoman:
                    couples = couples.OrderBy(p => p.FirstWoman).ToList();
                    break;
                case SortOrders.LastNameWoman:
                    couples = couples.OrderBy(p => p.LastWoman).ToList();
                    break;
                case SortOrders.Club:
                    couples = couples.OrderBy(p => p.Country).ToList();
                    break;
            }

            foreach (var couple in couples)
            {
                MainWindow.MainWindowHandle.ReportStatus("Setting Numbers in Event", number, coupleCount);

                couple.NumberInEvent = number;

                var participants =
                    this.context.Participants.Where(p => p.Couple.Id == couple.Id && p.Competition.CurrentRound == null);

                foreach (var participant in participants)
                {
                    participant.Number = number;
                }

                number++;
            }

            this.context.SaveChanges();

            MainWindow.MainWindowHandle.RemoveStatusReport();
        }

        private void ShowTeam()
        {
            MainWindow.MainWindowHandle.SetActivePage(new Teams());
        }

        private void SwitchPartners()
        {
            var help = this.SelectedCouple.FirstMan;
            this.SelectedCouple.FirstMan = this.SelectedCouple.FirstWoman;
            this.SelectedCouple.FirstWoman = help;

            help = this.SelectedCouple.LastMan;
            this.SelectedCouple.LastMan = this.SelectedCouple.LastWoman;
            this.SelectedCouple.LastWoman = help;

            help = this.SelectedCouple.MINMan;
            this.SelectedCouple.MINMan = this.SelectedCouple.MINWoman;
            this.SelectedCouple.MINWoman = help;
        }


        private void SetParticipantState(int participantId, CoupleState state)
        {
            var participant = this.context.Participants.FirstOrDefault(p => p.Id == participantId);

            if (participant == null || !participant.Competition.IsCompetitionNotStarted)
            {
                return;
            }

            participant.State = state;
            this.context.SaveChanges();

            var competition = this.AvailableCompetitions.FirstOrDefault(a => a.ParticipantId == participantId);
            if (competition == null)
            {
                return;
            }

            competition.CoupleState = state;
        }

        private void SetSingleCompetitioneExcused(int participantId)
        {
            this.SetParticipantState(participantId, CoupleState.Excused);
        }

        private void SetSingleCompetitionMissing(int participantId)
        {
            this.SetParticipantState(participantId, CoupleState.Missing);
        }

        private void SetSingleCompetitionDancing(int participantId)
        {
            this.SetParticipantState(participantId, CoupleState.Dancing);
        }

        #endregion
    }
}