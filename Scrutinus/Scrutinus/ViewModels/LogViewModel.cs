﻿// // TPS.net TPS8 Scrutinus
// // LogViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.ObjectModel;
using System.Linq;
using DataModel.Models;
using GalaSoft.MvvmLight;

namespace Scrutinus.ViewModels
{
    public class LogViewModel : ViewModelBase
    {
        private LogEntry selectedLogEntry;

        public LogViewModel()
        {
            var context = new ScrutinusContext();
            this.LogEntries = new ObservableCollection<LogEntry>(context.LogEntries.ToList());
        }

        public ObservableCollection<LogEntry> LogEntries { get; set; }

        public LogEntry SelectedLogEntry
        {
            get
            {
                return this.selectedLogEntry;
            }
            set
            {
                this.selectedLogEntry = value;
                this.RaisePropertyChanged(() => this.SelectedLogEntry);
            }
        }
    }
}
