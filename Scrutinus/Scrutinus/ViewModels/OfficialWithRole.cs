// // TPS.net TPS8 Scrutinus
// // OfficialWithRole.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using DataModel.Models;

namespace Scrutinus.ViewModels
{
    public class OfficialWithRole
    {
        #region Public Properties

        public string Name { get; set; }

        public Official Official { get; set; }

        public string Role { get; set; }

        public int RoleId { get; set; }

        #endregion

        #region Public Methods and Operators

        public override bool Equals(object obj)
        {
            if (!(obj is OfficialWithRole))
            {
                return base.Equals(obj);
            }

            var ob2 = obj as OfficialWithRole;

            return ob2.RoleId == this.RoleId && ob2.Official.Id == this.Official.Id;
        }

        public override int GetHashCode()
        {
            return this.RoleId.GetHashCode();
        }

        #endregion
    }
}