// // TPS.net TPS8 Scrutinus
// // EnterMarkingSumsViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using DataModel.Models;
using GalaSoft.MvvmLight.Command;
using Helpers.Calculation;
using Scrutinus.Dialogs;
using Scrutinus.Localization;
using Scrutinus.Pages;

namespace Scrutinus.ViewModels
{
    public enum CalculationMode
    {
        Marks,
        Points
    }
    public class EnterMarkingSumsViewModel : BaseViewModel
    {
        #region Fields

        private readonly Round currentRound;

        private readonly CalculationMode calculationMode;

        private readonly List<Marking> allMarkings;

        private bool isSaving;

        #endregion

        #region Constructors and Destructors

        public EnterMarkingSumsViewModel(ScrutinusContext context, int currentRoundId, CalculationMode calculationMode)
        {
            this.context = context;
            this.currentRound = context.Rounds.SingleOrDefault(r => r.Id == currentRoundId);
            this.ShowEditStartbuchCommand = new RelayCommand(this.ShowEditStartbuch);
            this.calculationMode = calculationMode;
            this.allMarkings = new List<Marking>();

            this.Markings = new List<MarkingViewModel>();

            foreach (var judge in this.currentRound.Competition.GetJudges().OrderBy(j => j.Sign))
            {
                var markingViewModel = new MarkingViewModel(this.calculationMode)
                                           {
                                               Judge = judge,
                                               CouplesMarks = new ObservableCollection<Marking>()
                                           };
                this.Markings.Add(markingViewModel);

                var dbJudge = context.Officials.Single(j => j.Id == judge.Id);
                foreach (var qualified in this.currentRound.Qualifieds)
                {
                    var mark =
                        context.Markings.FirstOrDefault(
                            m =>
                            m.Dance == null && m.Participant.Id == qualified.Participant.Id && m.Judge.Id == judge.Id
                            && m.Round.Id == currentRoundId);

                    if (mark == null)
                    {
                        mark = new Marking(qualified.Participant, this.currentRound, null, dbJudge)
                                   {
                                        Mark = 0
                                   };
                        this.context.Markings.Add(mark);
                    }
                    this.allMarkings.Add(mark);
                    mark.PropertyChanged += this.MarkOnPropertyChanged;
                    markingViewModel.CouplesMarks.Add(mark);
                }

                context.SaveChanges();
            }
        }

        private void MarkOnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            if (propertyChangedEventArgs.PropertyName != "Mark")
            {
                return;
            }

            if (this.isSaving)
            {
                return;
            }

            this.isSaving = true;
            this.context.SaveChanges();
            this.isSaving = false;
        }

        #endregion

        #region Public Properties

        public List<MarkingViewModel> Markings { get; set; }

        public ICommand ShowEditStartbuchCommand { get; set; }

        public Round CurrentRound
        {
            get
            {
                return this.currentRound; 
            }
        }

        #endregion

        #region Public Methods and Operators

        public override void SaveData()
        {
            try
            {
                this.context.SaveChanges();
            }
            catch (Exception ex)
            {
                ScrutinusContext.WriteLogEntry(ex);
                MainWindow.MainWindowHandle.ShowMessageAsync(Text.ErrorSavingData);
            }


            foreach (var allMarking in this.allMarkings)
            {
                allMarking.PropertyChanged -= this.MarkOnPropertyChanged;
            }

            this.allMarkings.Clear();
        }

        public override bool Validate()
        {
            this.UpdateQualified();
            return true;
        }

        #endregion

        #region Methods

        private void ShowEditStartbuch()
        {
            var dialog = new ChangeStartbookData(this.currentRound, this.context);
            MainWindow.MainWindowHandle.ShowDialog(dialog, null, 300, 300);
        }

        private void UpdateQualified()
        {
            try
            {
                this.context.SaveChanges(); // Save Markings to database
            }
            catch (Exception ex)
            {
                ScrutinusContext.WriteLogEntry(ex);
                MainWindow.MainWindowHandle.ShowMessageAsync(Text.ErrorSavingData); 
            }
           

            var qualifieds = this.currentRound.Qualifieds;
            foreach (var qualified in qualifieds)
            {
                if (this.calculationMode == CalculationMode.Marks)
                {
                    qualified.Sum =
                        this.context.Markings.Where(
                            m =>
                                m.Round.Id == this.currentRound.Id && m.Participant.Id == qualified.Participant.Id
                                && m.Dance == null).Sum(s => s.Mark);
                    qualified.Points = qualified.Sum;
                }
                else
                {
                    qualified.Points =
                        this.context.Markings.Where(
                            m =>
                                m.Round.Id == this.currentRound.Id && m.Participant.Id == qualified.Participant.Id
                                && m.Dance == null).Sum(s => s.MarkA);
                }

                qualified.PlaceFrom = 0;
                qualified.PlaceTo = 0;
            }
            this.context.SaveChanges();

            PlaceCalculation.CalculatePlaces(this.currentRound.Qualifieds, this.currentRound.PlaceOffset);

            try
            {
                this.context.SaveChanges(); // Save Markings to database
            }
            catch (Exception ex)
            {
                ScrutinusContext.WriteLogEntry(ex);
                MainWindow.MainWindowHandle.ShowMessageAsync(Text.ErrorSavingData);
            }
        }

        #endregion
    }
}