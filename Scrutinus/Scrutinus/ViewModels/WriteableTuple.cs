﻿// // TPS.net TPS8 Scrutinus
// // WriteableTuple.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using GalaSoft.MvvmLight;

namespace Scrutinus.ViewModels
{
    public class WriteableTuple<T1, T2, T3> : ViewModelBase
    {
        private T1 item1;

        private T2 item2;

        private T3 item3 ;

        public WriteableTuple(T1 item1, T2 item2, T3 item3)
        {
            this.Item1 = item1;
            this.Item2 = item2;
            this.Item3 = item3;
        }

        public T1 Item1 
        { 
            get
            {
                return this.item1;
            }
            set
            {
                this.item1 = value;
                this.RaisePropertyChanged(() => this.Item1);
            }
        }

        public T2 Item2
        {
            get
            {
                return this.item2;
            }
            set
            {
                this.item2 = value;
                this.RaisePropertyChanged(() => this.Item2);
            }
        }

        public T3 Item3
        {
            get
            {
                return this.item3;
            }
            set
            {
                this.item3 = value;
                this.RaisePropertyChanged(() => this.Item3);
            }
        }
    }
}
