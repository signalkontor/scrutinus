﻿// // TPS.net TPS8 Scrutinus
// // StartListViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Threading;
using DataModel;
using DataModel.Messages;
using DataModel.Models;
using DtvEsvModule;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using General.Rules.PointCalculation;
using MahApps.Metro.Controls.Dialogs;
using Scrutinus.Dialogs;
using Scrutinus.Dialogs.DialogViewModels;
using Scrutinus.ExternalApi;
using Scrutinus.Helper;
using Scrutinus.Localization;
using Scrutinus.Pages;

namespace Scrutinus.ViewModels
{
    /// <summary>
    ///     The start list view model.
    /// </summary>
    internal class StartListViewModel : BaseViewModel
    {
        #region Fields

        /// <summary>
        ///     The _round id.
        /// </summary>
        private readonly int _roundId;

        /// <summary>
        ///     The _competition id.
        /// </summary>
        private readonly int competitionId;

        private readonly Dispatcher dispatcher;

        private bool UpdateStartbookOnChanges = true;

        private AddWinnerDialog addWinnerDialog;

        private bool isVisible;

        /// <summary>
        ///     The _competition.
        /// </summary>
        private Competition competition;

        /// <summary>
        ///     The _enter number visible.
        /// </summary>
        private Visibility enterNumberVisible;

        /// <summary>
        ///     The _selected participant.
        /// </summary>
        private Participant selectedParticipant;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="StartListViewModel" /> class.
        /// </summary>
        public StartListViewModel()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="StartListViewModel" /> class.
        /// </summary>
        /// <param name="competitionId">
        ///     The competition id.
        /// </param>
        /// <param name="roundId">
        ///     The round id.
        /// </param>
        public StartListViewModel(int competitionId, int roundId)
        {
            this.dispatcher = Dispatcher.CurrentDispatcher;

            this.competitionId = competitionId;
            this._roundId = roundId;
            this.competition = this.context.Competitions.Include(p => p.Officials)
                    .Include(p => p.Officials.Select(s => s.Official.Nationality))
                    .Single(c => c.Id == this.competitionId);

            if (this.Competition.State != CompetitionState.CheckIn && this.Competition.State != CompetitionState.Closed)
            {
                this.competition.State = CompetitionState.Startlist;
            }
            
            this.EnterNumberVisible = Visibility.Collapsed;
            // Set the available classes for this section / age-group
            if (this.Competition.AgeGroup != null && this.Competition.Section != null)
            {
                var ageGroups =
                    this.Competition.Section.AgeGroupClassesMappings.SingleOrDefault(
                        a => a.AgeGroup.Id == this.Competition.AgeGroup.Id);
                if (ageGroups != null)
                {
                    this.AvailableClasses = new ObservableCollection<Class>(ageGroups.Classes);

                    // if age group is younger then adult, we also add the next higher call
                    if (this.Competition.AgeGroup.Id < 7 && this.AvailableClasses.Any(c => c.Id < 6))
                    {
                        var max = this.AvailableClasses.Where(c => c.Id < 6).Max(c => c.Id);
                        this.AvailableClasses.Add(this.context.Classes.SingleOrDefault(c => c.Id == max + 1));
                    }
                    // We alway add BSW:
                    this.AvailableClasses.Add(this.context.Classes.Single(c => c.Id == 7));
                }
                else
                {
                    this.AvailableClasses = new ObservableCollection<Class>(this.context.Classes);
                }
            }
            else
            {
                this.AvailableClasses = new ObservableCollection<Class>();
            }
            // Command Bindings
            this.SyncCommand = new RelayCommand(this.SyncParticipants);
            this.ChangeStateCommand = new RelayCommand<object>(this.ChangeCoupleState);
            this.CancelEditCommand = new RelayCommand(this.CancelEdit);
            this.SaveEditCommand = new RelayCommand(this.SaveEdit);
            this.ShowEnterNumberCommand = new RelayCommand(this.ShowEnterNumber);
            this.SetNumberCommand = new RelayCommand(this.SetFirstNumber);
            this.QuickAddCoupleCommand = new RelayCommand(this.QuickAddCouple);
            this.AddWinnerCoupleCommand = new RelayCommand(this.AddWinnerCouple);
            this.CheckWithWdsfApiCommand = new RelayCommand(this.CheckWithWdsfApi);
            this.RegisterWithWdsfCommand = new RelayCommand(this.RegisterCouplesAndOfficials);
            this.EditCompetitionDataCommand = new RelayCommand(this.EditCompetitionData);
            this.AddLateEntryCommand = new RelayCommand(this.AddLateEntry);
            this.DeleteParticipantCommand = new RelayCommand(this.DeleteParticipant);
            this.UploadDtvCommand = new RelayCommand(this.UploadDtv);
            this.EditCoupleCommand = new RelayCommand(this.EditCouple);
            this.StarCouplesCommand = new RelayCommand(this.ShowStarCouplesDialog);
            this.EditTeamMemberCommand = new RelayCommand(this.EditTeamMembers);

            this.Participants = new ObservableCollection<Participant>();
            this.StarList = ComboBoxItemSources.GetStarList(this.competition.Stars);

            this.FillParticipants();

            foreach (var participant in this.Participants.Where(p => p.State == CoupleState.DroppedOut))
            {
                participant.State = CoupleState.Dancing;
            }

            this.competition.CurrentRound = null;

            this.context.SaveChanges();

            Messenger.Default.Register<StartlistChangesMessage>(this, this.HandleUpdateMessage);
        }

        #endregion

        #region Public Properties

        public override bool IsVisible
        {
            get
            {
                return this.isVisible;
            }
            set
            {
                this.isVisible = value;
                this.context.Refresh(this.Competition);
                this.RaisePropertyChanged(() => this.IsVisible);
            }
        }

        public ICommand AddLateEntryCommand { get; set; }

        public ICommand UploadDtvCommand { get; set; }

        public ICommand StarCouplesCommand { get; set; }

        /// <summary>
        ///     Gets or sets the add winner couple command.
        /// </summary>
        /// <value>
        ///     The add winner couple command.
        /// </value>
        public ICommand AddWinnerCoupleCommand { get; set; }

        /// <summary>
        ///     Gets or sets the available classes.
        /// </summary>
        /// <value>
        ///     The available classes.
        /// </value>
        public ObservableCollection<Class> AvailableClasses { get; set; }

        /// <summary>
        ///     Gets or sets the cancel edit command.
        /// </summary>
        public ICommand CancelEditCommand { get; set; }

        /// <summary>
        ///     Gets or sets the change state command.
        /// </summary>
        public RelayCommand<object> ChangeStateCommand { get; set; }

        /// <summary>
        ///     Gets or sets the check with WDSF API command.
        /// </summary>
        /// <value>
        ///     The check with WDSF API command.
        /// </value>
        public ICommand CheckWithWdsfApiCommand { get; set; }

        public ICommand EditCoupleCommand { get; set; }

        public Competition Competition
        {
            get
            {
                return this.competition;
            }
        }

        public string CoupleStateString
        {
            get
            {
                if (this.Participants == null)
                {
                    return "";
                }

                return string.Format(
                    LocalizationService.Resolve(() => Text.NumberCouples),
                    this.Participants.Count,
                    this.Participants.Count(p => p.State == CoupleState.Dancing));
            }
        }

        public ICommand DeleteParticipantCommand { get; set; }

        public ICommand EditCompetitionDataCommand { get; set; }

        public bool EditPointsEnabled { get; set; }

        /// <summary>
        ///     Gets or sets the enter number visible.
        /// </summary>
        public Visibility EnterNumberVisible
        {
            get
            {
                return this.enterNumberVisible;
            }

            set
            {
                this.enterNumberVisible = value;
                if (this.DoRaiseEvents)
                {
                    this.RaisePropertyChanged(() => this.EnterNumberVisible);
                }
            }
        }

        /// <summary>
        ///     Gets or sets the first start number.
        /// </summary>
        public int FirstStartNumber { get; set; }

        public Class OriginalClass
        {
            get
            {
                return this.selectedParticipant?.Class;
            }
            set
            {
                if (this.selectedParticipant == null || value == null || this.selectedParticipant.Class == value)
                {
                    return;
                }

                // Ask the user if he really wants to change the  age group:
                var result = MainWindow.MainWindowHandle.ShowMessage("Soll die Startklasse wirklich geändert werden? Die Startklasse darf nur bei einem Aufstieg geändert werden",
                                                                "Änderung der Startklasse",
                                                                MessageDialogStyle.AffirmativeAndNegative);

                if (result == MessageDialogResult.Negative)
                {
                    var temp = this.OriginalClass;
                    this.selectedParticipant.Class = null;
                    this.RaisePropertyChanged(() => this.OriginalClass);
                    this.selectedParticipant.Class = temp;
                    this.RaisePropertyChanged(() => this.OriginalClass);
                    return;
                }

                this.selectedParticipant.Class = this.context.Classes.Single(c => c.Id == value.Id);

                var climbUpEntry =
                    this.context.ClimbUpTables.FirstOrDefault(
                        c =>
                        c.AgeGroup.Id == this.SelectedParticipant.AgeGroup.Id && c.Class.Id == this.OriginalClass.Id
                        && c.LTV == this.selectedParticipant.Couple.Region
                        && c.Section.Id == this.Competition.Section.Id);

                if (climbUpEntry == null)
                {
                    this.selectedParticipant.Class = this.OriginalClass;
                    climbUpEntry = DtvApiImporter.GetAufstiegstabelleifNotFound(this.context, this.selectedParticipant);
                }

                if (climbUpEntry != null)
                {
                    this.selectedParticipant.TargetClass = climbUpEntry.TargetClass;
                }
                else
                {
                    this.selectedParticipant.TargetClass = null;
                }

                if (this.UpdateStartbookOnChanges)
                {
                    DTVPointCalculator.UpdateAllParticipants(
                        this.context,
                        this.selectedParticipant.Id,
                        this.selectedParticipant.OriginalPoints.HasValue
                            ? this.selectedParticipant.OriginalPoints.Value
                            : 0,
                        this.selectedParticipant.OriginalPlacings.HasValue
                            ? this.selectedParticipant.OriginalPlacings.Value
                            : 0,
                        this.SelectedParticipant.Class,
                        false);
                }

                this.RaisePropertyChanged(() => this.OriginalClass);
            }
        }

        public int OriginalPlacings
        {
            get
            {
                if (this.selectedParticipant != null)
                {
                    return this.selectedParticipant.OriginalPlacings.HasValue
                               ? this.selectedParticipant.OriginalPlacings.Value
                               : 0;
                }
                return 0;
            }
            set
            {
                if (this.selectedParticipant == null || this.selectedParticipant.OriginalPlacings == null)
                {
                    return;
                }

                this.selectedParticipant.OriginalPlacings = value;
                if (this.UpdateStartbookOnChanges)
                {
                    DTVPointCalculator.UpdateAllParticipants(
                        this.context,
                        this.selectedParticipant.Id,
                        this.selectedParticipant.OriginalPoints.HasValue
                            ? this.selectedParticipant.OriginalPoints.Value
                            : 0,
                        this.selectedParticipant.OriginalPlacings.Value,
                        this.SelectedParticipant.Class,
                        true);
                }

                this.RaisePropertyChanged(() => this.OriginalPlacings);
            }
        }

        public int OriginalPoints
        {
            get
            {
                if (this.selectedParticipant != null)
                {
                    return this.selectedParticipant.OriginalPoints.HasValue
                               ? this.selectedParticipant.OriginalPoints.Value
                               : 0;
                }
                return 0;
            }
            set
            {
                if (this.selectedParticipant == null || this.selectedParticipant.OriginalPoints == value)
                {
                    return;
                }

                this.selectedParticipant.OriginalPoints = value;
                if (this.UpdateStartbookOnChanges)
                {
                    DTVPointCalculator.UpdateAllParticipants(
                        this.context,
                        this.selectedParticipant.Id,
                        this.selectedParticipant.OriginalPoints.Value,
                        this.selectedParticipant.OriginalPlacings.HasValue
                            ? this.selectedParticipant.OriginalPlacings.Value
                            : 0,
                        this.SelectedParticipant.Class,
                        true);
                }

                this.RaisePropertyChanged(() => this.OriginalPoints);
            }
        }

        /// <summary>
        ///     Gets or sets the participants.
        /// </summary>
        public ObservableCollection<Participant> Participants { get; set; }

        public ICommand QuickAddCoupleCommand { get; set; }

        /// <summary>
        ///     Gets or sets the register with WDSF command.
        /// </summary>
        /// <value>
        ///     The register with WDSF command.
        /// </value>
        public ICommand RegisterWithWdsfCommand { get; set; }

        /// <summary>
        ///     Gets or sets the save edit command.
        /// </summary>
        public ICommand SaveEditCommand { get; set; }

        public ICommand EditTeamMemberCommand { get; set; }

        /// <summary>
        ///     Gets or sets the selected participant.
        /// </summary>
        public Participant SelectedParticipant
        {
            get
            {
                return this.selectedParticipant;
            }

            set
            {
                this.selectedParticipant = value;
#if DTV
                if (this.selectedParticipant != null)
                {
                    this.UpdateStartbookOnChanges = false;
                    this.OriginalPlacings = this.selectedParticipant.OriginalPlacings.HasValue ? this.selectedParticipant.OriginalPlacings.Value : 0;
                    this.OriginalPoints = this.selectedParticipant.OriginalPoints.HasValue ? this.selectedParticipant.OriginalPoints.Value : 0;
                    this.OriginalClass = this.selectedParticipant.Class;
                    this.UpdateStartbookOnChanges = true;

                    this.EditPointsEnabled = true;

                    //if (this.SelectedParticipant.Startbuch == null || this.SelectedParticipant.Startbuch.IsSelfGenerated || (this.selectedParticipant.TargetClass == null || this.selectedParticipant.TargetClass.Id == 6) && this.selectedParticipant.Class.Id != 7)
                    //{
                    //    this.EditPointsEnabled = false;
                    //}
                    //else
                    //{
                    //    this.EditPointsEnabled = true;
                    //}
                    this.RaisePropertyChanged(() => this.EditPointsEnabled);
                }
                else
                {
                    this.UpdateStartbookOnChanges = false;
                    this.OriginalPlacings = 0;
                    this.OriginalPoints = 0;
                    this.UpdateStartbookOnChanges = true;
                    this.EditPointsEnabled = false;
                    this.RaisePropertyChanged(() => this.EditPointsEnabled);
                }
#endif
                if (this.DoRaiseEvents)
                {
                    this.RaisePropertyChanged(() => this.SelectedParticipant);
                    this.RaisePropertyChanged(() => this.OriginalPlacings);
                    this.RaisePropertyChanged(() => this.OriginalPoints);
                    this.RaisePropertyChanged(() => this.OriginalClass);
                    this.RaisePropertyChanged(() => this.EditPointsEnabled);
                    this.RaisePropertyChanged(() => this.SelectedParticipantOtherCompetitions);
                }
            }
        }

        public IEnumerable<Participant> SelectedParticipantOtherCompetitions
        {
            get
            {
                if (this.SelectedParticipant == null)
                {
                    return null;
                }
                else
                {
                    return this.SelectedParticipant.Couple.Participants.Where(p => p.Competition.Id != this.Competition.Id).OrderBy(c => c.Competition.StartTime);
                }
            }
        }

        /// <summary>
        ///     Gets or sets the set number command.
        /// </summary>
        public ICommand SetNumberCommand { get; set; }

        /// <summary>
        ///     Gets or sets the show enter number command.
        /// </summary>
        public ICommand ShowEnterNumberCommand { get; set; }

        /// <summary>
        ///     Gets or sets the star list.
        /// </summary>
        public IEnumerable<ItemValue> StarList { get; set; }

        /// <summary>
        ///     Gets or sets the sync command.
        /// </summary>
        public ICommand SyncCommand { get; set; }

        #endregion

        // UI Control Properties:

        #region Public Methods and Operators

        /// <summary>
        ///     The handle update message.
        /// </summary>
        /// <param name="message">
        ///     The message.
        /// </param>
        public void HandleUpdateMessage(StartlistChangesMessage message)
        {
            this.SyncParticipants();
        }

        /// <summary>
        ///     The save data.
        /// </summary>
        public override void SaveData()
        {
            this.context.SaveChanges();
            Messenger.Default.Send(new StartlistChangesMessage());
        }

        /// <summary>
        ///     The validate.
        /// </summary>
        /// <returns>
        ///     The <see cref="bool" />.
        /// </returns>
        public override bool Validate()
        {
            // We test this before we sync participant because SyncParticipants would fail otherwise:
            var newContext = new ScrutinusContext();
            var competitionToTest = newContext.Competitions.Single(c => c.Id == this.competition.Id);

            if (competitionToTest.Section == null)
            {
                MainWindow.MainWindowHandle.ShowMessage(LocalizationService.Resolve(() => Text.SectionNotSet));
                return false;
            }

            if (competitionToTest.CompetitionType == null)
            {
                MainWindow.MainWindowHandle.ShowMessage(LocalizationService.Resolve(() => Text.CompetitionTypeNotSet));
                return false;
            }

            if (competitionToTest.AgeGroup == null)
            {
                MainWindow.MainWindowHandle.ShowMessage(LocalizationService.Resolve(() => Text.AgeGroupNotSet));
                return false;
            }

            if (competitionToTest.Class == null)
            {
                MainWindow.MainWindowHandle.ShowMessage(LocalizationService.Resolve(() => Text.ClassNotSet));
                return false;
            }

#if DTV
            var dancingCouples = this.Participants.Count(p => p.State == CoupleState.Dancing);
            if (dancingCouples < 3)
            {
                var result = MainWindow.MainWindowHandle.ShowMessage(
                        $"Es gibt nur {dancingCouples} anwesende Paare. Soll das Turnier ausfallen?",
                        "Rückfrage",
                        MessageDialogStyle.AffirmativeAndNegative);

                if (result == MessageDialogResult.Affirmative)
                {
                    competitionToTest.Canceled = true;
                    newContext.SaveChanges();
                    this.competition.Canceled = true;
                    this.context.SaveChanges();

                    MainWindow.MainWindowHandle.ContentFrame.Content = null;

                    return false;
                }
            }

            var judgesGroupes = competitionToTest.GetJudges().GroupBy(j => j.Club);
            if (judgesGroupes.Any(g => g.Count() > 1))
            {
                var club = judgesGroupes.First(g => g.Count() > 1).Key;
                var hasDancers = competitionToTest.Participants.Any(p => p.Couple.Country == club && p.State == CoupleState.Dancing);

                if (!hasDancers)
                {
                    var result = MainWindow.MainWindowHandle.ShowMessage(
                        $"Mehrere Wertungsrichter sind aus dem Verein {club}, trotzdem weiter machen?",
                        "Rückfrage",
                        MessageDialogStyle.AffirmativeAndNegative);

                    if (result == MessageDialogResult.Negative)
                    {
                        return false;
                    }
                }
                else
                {
                    var result = MainWindow.MainWindowHandle.ShowMessage(
                        $"Mehr als ein Wertungsrichter sind aus dem Verein {club}. Es tanzen Paare aus diesem Verein!!! Trotzdem weiter machen?",
                        "Rückfrage",
                        MessageDialogStyle.AffirmativeAndNegative);

                    if (result == MessageDialogResult.Negative)
                    {
                        return false;
                    }
                }
            }

            // Check all licenses:
            foreach (var officialInCompetition in this.Competition.Officials)
            {
                var dtvOfficial = this.context.DtvLicenseHolders.FirstOrDefault(d => d.DtvId == officialInCompetition.Official.ExternalId);
                if (dtvOfficial != null)
                {
                    officialInCompetition.Official.Licenses = dtvOfficial.LicensesAsString;
                }
            }
            
            // Check that BS and TL have a valid TL license:
            if (this.Competition.CompetitionType.CompetitionRule.AssosiateRequired)
            {
                var tls = this.Competition.Officials.Where(o => o.Role.Id == Roles.Supervisor && (o.Official.Licenses == null || !o.Official.Licenses.Contains("TL"))).ToList();
                if (tls.Any())
                {
                    var names = string.Join(", ", tls.Select(o => o.Official.NiceName));
                    MainWindow.MainWindowHandle.ShowMessage($"Folgende(r) Turnierleiter hat keine gültige TL Lizenz: {names}");
                    // return false;
                }
            }

            if(this.Competition.CompetitionType.CompetitionRule.AssosiateRequired)
            { 
                var bss = this.Competition.Officials.Where(o => o.Role.Id == Roles.Associate && (o.Official.Licenses == null || !o.Official.Licenses.Contains("TL"))).ToList();
                if (bss.Any())
                {
                    var names = string.Join(", ", bss.Select(o => o.Official.NiceName));
                    MainWindow.MainWindowHandle.ShowMessage($"Folgende(r) Beisitzer hat keine gültige TL Lizenz: {names}");
                    //return false;
                } 
            }

            // Check, the judges have a valid license:
            var validLicenses = this.GetValidLicensesForCompetition();
            var wrongJudges = new List<Official>(); //Competition.Officials.Where(o => o.Role.Id == Roles.Judge && o.Official.Licenses != null && !o.Official.Licenses.Contains(minimumLicense)).ToList();

            foreach (var judge in this.Competition.GetJudges().Where(j => j.Licenses != null))

            {
                var licenses = judge.Licenses.Split(',').Select(s => s.Trim());
                var hasLicense = licenses.Any(s => validLicenses.Contains(s));
                if (!hasLicense)
                {
                    wrongJudges.Add(judge);
                }
            }


            if (wrongJudges.Any())
            {
                var names = string.Join(", ", wrongJudges.Select(o => o.NiceName));
                var dlgResult = MainWindow.MainWindowHandle.ShowMessage($"Folgende(r) Wertungsrichter haben möglicherweise eine falsche Lizenz: {names}. Mit Ok dennoch weiter machen", "Warnung", MessageDialogStyle.AffirmativeAndNegative);
                if (dlgResult == MessageDialogResult.Negative)
                {
                    return false;
                }
            }

            var judgesWithoutLicense = this.Competition.Officials.Where(o => o.Role.Id == Roles.Judge && o.Official.Licenses == null);
            if (judgesWithoutLicense.Any())
            {
                var names = string.Join(", ", judgesWithoutLicense.Select(o => o.Official.NiceName));
                var dlgResult = MainWindow.MainWindowHandle.ShowMessage($"Folgende(r) Wertungsrichter haben keine Lizenz hinterlegt. Bitte prüfen: {names}. Mit Ok dennoch weiter machen", "Warnung", MessageDialogStyle.AffirmativeAndNegative);
                if (dlgResult == MessageDialogResult.Negative)
                {
                    return false;
                }
            }
#endif
            // Check we have unique dances:
            var groups = competitionToTest.Dances.GroupBy(d => d.Dance.Id);
            if (groups.Any(g => g.Count() > 1))
            {
                MainWindow.MainWindowHandle.ShowMessage(LocalizationService.Resolve(() => Text.DanceIsDouble));
                return false;
            }

            // Check, if everything is setup correctly
            this.SyncParticipants();

            if (this.competition.CompetitionType.Federation == "DTV")
            {
                if (!this.competition.Event.DtvDownloadDataValid)
                {
                    var result =
                        MainWindow.MainWindowHandle.ShowMessage(
                            "Die Aufstiegsdaten sind nicht gültig, bitte führen Sie einen Download der aktuellen Startliste vom DTV Portal durch.",
                            "Warnung!",
                            MessageDialogStyle.AffirmativeAndNegative);

                    if (result == MessageDialogResult.Negative)
                    {
                        return false;
                    }
                }
            }

            if (this.competition.Canceled)
            {
                MainWindow.MainWindowHandle.ShowMessage(
                    "Das Turnier ist als ausgefallen markiert und kann nicht gestartet werden.");
                return false;
            }

            if (this.competition.State == CompetitionState.CheckIn)
            {
                var result = MainWindow.MainWindowHandle.ShowMessage(LocalizationService.Resolve(() => Text.CheckinStillOpen), "Checkin", MessageDialogStyle.AffirmativeAndNegative);

                if (result == MessageDialogResult.Negative)
                {
                    return false;
                }

                this.competition.State = CompetitionState.Startlist;
                this.context.SaveChanges();
            }

#if DTV
            if (!this.CheckAllCouplesAreValid())
            {
                return false;
            }

            if (!TeamHelper.CheckTeamsAreValid(this.Competition))
            {
                return false;
            }
#endif

            if (this.competition.State == CompetitionState.Closed)
            {
                MainWindow.MainWindowHandle.ShowMessage(LocalizationService.Resolve(() => Text.CompetitionClosed));
                return false;
            }

            if (this.competition.Participants.All(p => p.State != CoupleState.Dancing))
            {
                MainWindow.MainWindowHandle.ShowMessage(
                    LocalizationService.Resolve(() => Text.CompetitionHasNoDancingCouples));
                return false;
            }

            // Do we have Judges?
            var countJudges = this.context.OfficialCompetition.Count(o => o.Competition.Id == this.competitionId);
            if (countJudges < 3)
            {
                MainWindow.MainWindowHandle.ShowMessage(LocalizationService.Resolve(() => Text.NotEnoughJudges));
                return false;
            }

            var countDances = this.context.DancesCompetition.Count(d => d.Competition.Id == this.competitionId);
            if (countDances == 0)
            {
                MainWindow.MainWindowHandle.ShowMessage(LocalizationService.Resolve(() => Text.NoDancesDefined));
                return false;
            }

            // Check that no couple has a zero:
            if (this.competition.Participants.Any(p => p.Number == 0 && p.State == CoupleState.Dancing))
            {
                MainWindow.MainWindowHandle.ShowMessage(
                    LocalizationService.Resolve(() => Text.ZeroIsNotAValidNumber),
                    LocalizationService.Resolve(() => Text.ZeroIsNotAValidNumber),
                    MessageDialogStyle.Affirmative);
                return false;
            }

            // Check, start numbers are unique:
            if (this.competition.Participants.GroupBy(x => x.Number).Any(g => g.Count() > 1))
            {
                MainWindow.MainWindowHandle.ShowMessage(
                    LocalizationService.Resolve(() => Text.NumbersNotUnique),
                    LocalizationService.Resolve(() => Text.NumbersNotUniqueShort),
                    MessageDialogStyle.Affirmative);
                return false;
            }

            var rule = this.competition.CompetitionType.CompetitionRule;

            // Check, if we meet all requirements of our rule
            if (rule != null)
            {
                if (rule.ChairmanRequired)
                {
                    if (
                        !this.CheckOfficialAvailable(
                            Roles.Chairman,
                            () => Text.NoChairman,
                            true,
                            () => Text.MoreThenOneChairman))
                    {
                        return false;
                    }
                }

                if (rule.AssosiateRequired)
                {
                    if (
                        !this.CheckOfficialAvailable(
                            Roles.Associate,
                            () => Text.NoAssociate,
                            true,
                            () => Text.MoreThenOneAssociate))
                    {
                        return false;
                    }
                }

                if (rule.SupervisorRequired)
                {
                    if (
                        !this.CheckOfficialAvailable(
                            Roles.Supervisor,
                            () => Text.NoSupervisor,
                            false,
                            () => Text.MoreThenOneSupervisor))
                    {
                        return false;
                    }
                }
            }

            var couples =
                this.context.Participants.Count(
                    c => c.Competition.Id == this.competitionId && c.State == CoupleState.Dancing && c.Star == 0);
            var stars =
                this.context.Participants.Count(
                    c => c.Competition.Id == this.competitionId && c.State == CoupleState.Dancing && c.Star > 0);

            // Translate and Format text for messagebox
            // dependend on if we have stars or not
            // German clubs would run mad if we show the star couples even they are zero
            var message = stars == 0
                                 ? string.Format(
                                     LocalizationService.Resolve(() => Text.MsgStartListNumberCouples),
                                     couples)
                                 : string.Format(
                                     LocalizationService.Resolve(() => Text.MsgStartListNumberWithStars),
                                     couples,
                                     stars);

            var res = MainWindow.MainWindowHandle.ShowMessage(
                message,
                LocalizationService.Resolve(() => Text.CouplesDancing),
                MessageDialogStyle.AffirmativeAndNegative);

            this.Participants.Clear();
            this.FillParticipants();

            return res == MessageDialogResult.Affirmative;
        }


        private IEnumerable<string> GetValidLicensesForCompetition()
        {
            var result = new List<string>();

            var section = this.Competition.Section.Id == 1 ? "Std" : "Lat";

            if (this.Competition.Class.Id == 1 || this.Competition.Class.Id == 2)
            {
                result.Add("WR-C");
                result.Add("WR-A-Std");
                result.Add("WR-S-Std");
                result.Add("WR-A-Lat");
                result.Add("WR-S-Lat");

                return result;
            }

            if (this.Competition.Class.Id == 3 || this.Competition.Class.Id == 4)
            {
                result.Add("WR-A-" + section);
            }

            result.Add("WR-S-" + section);

            return result;
        }

        #endregion

        #region Methods

        private void AddLateEntry()
        {
            var dialog = new AddDtvCoupleToCompetition(this.context, this.Competition);
            MainWindow.MainWindowHandle.ShowDialog(dialog, d => this.SyncParticipants(), 300, 300);
        }

        private void AddWinnerClosing(object sender)
        {
            var viewModel = this.addWinnerDialog.DataContext as AddWinnerViewModel;
            if (viewModel.Result == DialogResult.OK)
            {
                var couple = this.context.Couples.Single(c => c.Id == viewModel.SelectedParticipant.CoupleId);
                var competition = this.context.Competitions.Single(c => c.Id == this.Competition.Id);

                Startbuch startbuch = null;
#if DTV
                startbuch = couple.Startbooks.FirstOrDefault(b => b.Section.Id == this.Competition.Section.Id);
                if (startbuch == null)
                {
                    MainWindow.MainWindowHandle.ShowMessage("Das Paar hat kein Startbuch für diese Turnierart!");
                    return;
                }
#endif
                var selectedParticipant =
                    this.context.Participants.Single(p => p.Id == viewModel.SelectedParticipant.Participant.Id);

                var existingParticipant = this.Competition.Participants.FirstOrDefault(p => p.Couple.Id == selectedParticipant.Couple.Id);
                if (existingParticipant != null)
                {
#if DTV
                    if (viewModel.AddAsWinner)
                    {
                        MainWindow.MainWindowHandle.ShowMessage("Das Paar war schon auf der Startliste, die Siegerkennzeichnung wurde gesetzt");

                        existingParticipant.IsWinnerCouple = true;
                        existingParticipant.RegistrationFlag = RegistrationFlag.Winner;
                        existingParticipant.State = CoupleState.Dancing;
                        this.context.SaveChanges();

                        this.SyncCommand.Execute(null);

                        return;
                    }
#endif
                    MainWindow.MainWindowHandle.ShowMessage(
                        string.Format(
                            LocalizationService.Resolve(() => Text.CoupleAlreadyOnList),
                            existingParticipant.Number));
                    return;
                }

                var participant = new Participant
                                      {
                                          Competition = competition,
                                          AgeGroup = startbuch != null ? startbuch.AgeGroup : competition.AgeGroup,
                                          Class = startbuch != null ? startbuch.Class : competition.Class,
                                          Number = couple.NumberInEvent ?? selectedParticipant.Number,
                                          RegistrationFlag = viewModel.AddAsWinner
                                                  ? RegistrationFlag.Winner
                                                  : RegistrationFlag.ClimbUp,
                                          OriginalPlacings = selectedParticipant.NewPlacings,
                                          OriginalPoints = selectedParticipant.NewPoints,
                                          TargetClass = selectedParticipant.TargetClass,
                                          TargetPlacings = selectedParticipant.TargetPlacings,
                                          TargetPoints = selectedParticipant.TargetPoints,
                                          PlacingsUpto = selectedParticipant.PlacingsUpto,
                                          MinimumPoints = selectedParticipant.MinimumPoints,
                                          Couple = couple,
                                          ExternalId = selectedParticipant.ExternalId,
                                          IsWinnerCouple = true
                                      };

                if (!viewModel.AddAsWinner)
                {
                    participant.OriginalPoints = 0;
                    participant.OriginalPlacings = 0;
                    participant.Class = participant.TargetClass;
                    // Find climb-Up-Table Entry:
                    var climbUpEntry =
                        this.context.ClimbUpTables.FirstOrDefault(
                            c =>
                            c.AgeGroup.Id == participant.AgeGroup.Id
                            && c.Section.Id == participant.Competition.Section.Id && c.LTV == participant.Couple.Region
                            && c.Class.Id == participant.Class.Id);

                    if (climbUpEntry != null)
                    {
                        participant.TargetClass = climbUpEntry.TargetClass;
                        participant.TargetPlacings = climbUpEntry.Placings;
                        participant.TargetPoints = climbUpEntry.Points;
                        participant.MinimumPoints = climbUpEntry.MinPoints;
                        participant.PlacingsUpto = climbUpEntry.MinimumPlace;
                    }
                }

                this.context.Participants.Add(participant);
                this.context.SaveChanges();

                this.SyncCommand.Execute(null);

                MainWindow.MainWindowHandle.ViewModel.UpdateCoupleStatistic(this.competitionId, this.Participants.Count, this.Participants.Count(p => p.State == CoupleState.Dancing));
            }
        }

        private void AddWinnerCouple()
        {
            this.addWinnerDialog = new AddWinnerDialog(this.Competition);
            MainWindow.MainWindowHandle.ShowDialog(this.addWinnerDialog, this.AddWinnerClosing, 250, 300);

            
        }

        /// <summary>
        ///     The cancel edit.
        /// </summary>
        private void CancelEdit()
        {
            this.SyncParticipants();
        }

        /// <summary>
        ///     The change couple state.
        /// </summary>
        /// <param name="parameter">
        ///     The parameter.
        /// </param>
        private void ChangeCoupleState(object parameter)
        {
            // no participant -> exit
            if (this.SelectedParticipant == null)
            {
                return;
            }

            var state = parameter as string;

            switch (state)
            {
                case "Dancing":
                    this.SelectedParticipant.State = CoupleState.Dancing;
                    break;
                case "Excused":
                    this.SelectedParticipant.State = CoupleState.Excused;
                    break;
                case "Missing":
                    this.SelectedParticipant.State = CoupleState.Missing;
                    break;
            }

            this.context.SaveChanges();

            this.RaisePropertyChanged(() => this.CoupleStateString);

            MainWindow.MainWindowHandle.ViewModel.UpdateCoupleStatistic(this.competitionId, this.Participants.Count, this.Participants.Count(p => p.State == CoupleState.Dancing));
        }

        private bool CheckAllCouplesAreValid()
        {
            if (this.Competition.CompetitionType.Id == 13)
            {
                // In Rising-Stars dürfen auch A-Paare tanzen, wir lassen nun mal
                // schnell alle zu:
                return true;
            }

            var wrongClassParticipants =
                this.competition.Participants.Where(
                    p =>
                    p.State == CoupleState.Dancing && p.Class != null && p.Class.Id != this.competition.Class.Id
                    && !p.IsWinnerCouple).ToList();

            // Remove BSW couples in D class
            if (this.competition.Class.Id == 1)
            {
                wrongClassParticipants.RemoveAll(p => p.Class.Id == 7);
            }
            // Remove combined class participants
            if (this.competition.CombinedClass != null)
            {
                wrongClassParticipants =
                    wrongClassParticipants.Where(p => p.Class.Id != this.competition.CombinedClass.Id).ToList();
            }
            // Nun müssen wir noch Jugend-A Turnier mit S-Klasse Teilnehmern filtern
            if (this.competition.AgeGroup.Id < 7)
            {
                if (this.Competition.Class.Id
                    == this.GetHighestClassOfAgeGroup(this.Competition.AgeGroup, this.Competition.Section).Id
                    || this.Competition.CombinedClass != null
                    && this.Competition.CombinedClass.Id
                    == this.GetHighestClassOfAgeGroup(this.Competition.AgeGroup, this.Competition.Section).Id)
                {
                    // Ok, dieses Turnier ist die höchste Startklasse, daher dürfen hier Paare der nächst höheren noch mittanzen:
                    var highestClass = this.GetHighestClassOfAgeGroup(
                        this.Competition.AgeGroup,
                        this.Competition.Section);
                    wrongClassParticipants =
                        wrongClassParticipants.ToList().Where(p => p.Class.Id != highestClass.Id + 1).ToList();
                    if (this.competition.CombinedClass != null)
                    {
                        highestClass = this.GetHighestClassOfAgeGroup(
                            this.Competition.AgeGroup,
                            this.Competition.Section);
                        wrongClassParticipants =
                            wrongClassParticipants.ToList().Where(p => p.Class.Id != highestClass.Id + 1).ToList();
                    }
                }
            }

            if (wrongClassParticipants.Any())
            {
                var wrongCoupleMessage = string.Join(
                    "\r\n",
                    wrongClassParticipants.Select(p => p.NiceNameWithNumber));

                MainWindow.MainWindowHandle.ShowMessage(
                    "Folgende Paare haben eine falsche Start-Klasse: \r\n" + wrongCoupleMessage);

                return false;
            }

            return true;
        }

        private bool CheckOfficialAvailable<T>(
            int requiredRoleId,
            Expression<Func<T>> messageKeyNotAvailable,
            bool onlyOne,
            Expression<Func<T>> messageKeyMoreThenOne)
        {
            var cm =
                this.context.OfficialCompetition.Where(
                    c => c.Competition.Id == this.competitionId && c.Role.Id == requiredRoleId);

            if (cm.Count() > 1 && onlyOne)
            {
                MainWindow.MainWindowHandle.ShowMessage(LocalizationService.Resolve(messageKeyMoreThenOne));
                return false;
            }

            if (!cm.Any())
            {
                MainWindow.MainWindowHandle.ShowMessage(LocalizationService.Resolve(messageKeyNotAvailable));
                return false;
            }

            return true;
        }

        private void CheckWithWdsfApi()
        {
            var client = new WdsfApiClient(this.context, this.competition);
            var task = new Task(
                () =>
                    {
                        try
                        {
                            client.CheckParticipants(this.competition.Participants, false);
                            this.context.SaveChanges();
                        }
                        catch (Exception ex)
                        {
                            var message = ex.Message
                                             + (ex.InnerException != null ? " : " + ex.InnerException.Message : "");
                            MainWindow.MainWindowHandle.ShowMessage("WDSF API failed: " + message);
                        }
                    });
            task.Start();
        }

        private void DeleteParticipant()
        {
            if (this.SelectedParticipant == null)
            {
                return;
            }

            var res = MainWindow.MainWindowHandle.ShowMessage(
                LocalizationService.Resolve(() => Text.DeleteCoupleMessage),
                LocalizationService.Resolve(() => Text.DeleteCoupleCaption),
                MessageDialogStyle.AffirmativeAndNegative);

            if (res == MessageDialogResult.Negative)
            {
                return;
            }

            var toDelete = this.SelectedParticipant;
            this.Participants.Remove(toDelete);
            this.context.Participants.Remove(toDelete);
            this.context.SaveChanges();
        }

        private void EditCompetitionData()
        {
            var dialog = new EditCompetitionData(this.competitionId);
            MainWindow.MainWindowHandle.ShowDialog(dialog, null, 300, 300);
        }

        /// <summary>
        ///     The fill participants.
        /// </summary>
        private void FillParticipants()
        {
            var participants =
                this.context.Participants.Where(p => p.Competition.Id == this.competitionId).OrderBy(p => p.Number);

            foreach (var p in participants)
            {
                this.Participants.Add(p);
            }
        }

        private Class GetHighestClassOfAgeGroup(AgeGroup group, Section section)
        {
            var map = section.AgeGroupClassesMappings.Single(m => m.AgeGroup == group);

            return map.Classes.OrderByDescending(o => o.OrderWhenCombined).First();
        }

        private void QuickAddCouple()
        {
            var page = new QuickAddCoupleDialog(this.competitionId, this._roundId, this.Competition.LateEntriesAllowed);

            MainWindow.MainWindowHandle.ShowDialog(page, null, 600, 600);

            this.SyncParticipants();
        }

        private void RegisterCouplesAndOfficials()
        {
            var client = new WdsfApiClient(this.context, this.competition);
            var task = new Task(
                () =>
                    {
                        try
                        {
                            client.ClearAllParticipants();
                            foreach (var participant in this.competition.Participants)
                            {
                                participant.WdsfId = null;
                                participant.WdsfApiStatus = ApiStatus.Unknown;
                            }
                            client.RegisterParticipants(this.competition.Participants);
                            client.ClearOfficials();
                            foreach (var official in this.competition.Officials)
                            {
                                official.WdsfId = null;
                            }
                            client.RegisterOfficials();
                            this.context.SaveChanges();
                        }
                        catch (Exception ex)
                        {
                            var message = ex.Message
                                             + (ex.InnerException != null ? " : " + ex.InnerException.Message : "");
                            MainWindow.MainWindowHandle.ShowMessage("WDSF API failed: " + message);
                        }
                    });

            task.Start();
        }

        /// <summary>
        ///     The save edit.
        /// </summary>
        private void SaveEdit()
        {
            this.context.SaveChanges();
            this.SyncParticipants();
            Messenger.Default.Send(new StartlistChangesMessage());
        }

        /// <summary>
        ///     The set first number.
        /// </summary>
        private void SetFirstNumber()
        {
            var number = this.FirstStartNumber;

            foreach (var participant in this.Participants)
            {
                participant.Number = number;
                number++;
            }

            this.context.SaveChanges();

            this.EnterNumberVisible = Visibility.Collapsed;
        }

        private void SetStartNumber(object result)
        {
            var dialog = result as SetStartNumberDialog;
            if (dialog == null)
            {
                return;
            }
            if (dialog.FirstStartNumber == 0 || !dialog.Result)
            {
                return;
            }

            var number = dialog.FirstStartNumber;

            var participants = this.Participants.ToList();

            switch (dialog.SortOrder.SortOrder)
            {
                case SortOrders.None:
                    break;
                case SortOrders.FirstNameMan:
                    participants = participants.OrderBy(p => p.Couple.FirstMan).ToList();
                    break;
                case SortOrders.LastNameMan:
                    participants = participants.OrderBy(p => p.Couple.LastMan).ToList();
                    break;
                case SortOrders.FirstNameWoman:
                    participants = participants.OrderBy(p => p.Couple.FirstWoman).ToList();
                    break;
                case SortOrders.LastNameWoman:
                    participants = participants.OrderBy(p => p.Couple.LastWoman).ToList();
                    break;
                case SortOrders.Club:
                    participants = participants.OrderBy(p => p.Couple.Country).ToList();
                    break;
            }

            foreach (var participant in participants)
            {
                participant.Number = number;
                number++;
            }

            this.context.SaveChanges();
        }

        private void ShowEnterNumber()
        {
            var dialog = new SetStartNumberDialog();
            MainWindow.MainWindowHandle.ShowDialog(dialog, this.SetStartNumber, 300, 300);
        }

        /// <summary>
        ///     The sync participants.
        /// </summary>
        private void SyncParticipants()
        {
            this.context = new ScrutinusContext();
            // Just to be sure we invoke this:
            this.dispatcher.Invoke(
                new Action(() =>
                {
                    this.competition = this.context.Competitions.Single(c => c.Id == this.Competition.Id);

                    this.AvailableClasses.Clear();
                    var map = this.Competition.Section.AgeGroupClassesMappings.Single(m => m.AgeGroup.Id == this.Competition.AgeGroup.Id);

                    foreach (var Class in map.Classes)
                    {
                        this.AvailableClasses.Add(Class);
                    }

                    this.RaisePropertyChanged(() => this.Competition);
                    
                    this.Participants.Clear();
                    this.FillParticipants();

                    this.RaisePropertyChanged(() => this.CoupleStateString);
                }));
        }

        private void UploadDtv()
        {
            var importer = new DtvApiImporter(
                    Settings.Default.DtvApiUrl,
                    Settings.Default.WDSFApiUser,
                    Settings.Default.WDSFApiPassword,
                    this.context);

            if (competition.Canceled)
            {
                if (!context.Competitions.Any(c =>
                    c.Id != competition.Id && (c.ExternalId == competition.ExternalId || c.CombinedExternalId == competition.ExternalId)))
                {
                    MainWindow.MainWindowHandle.ShowMessage("Dieses Turnier wurde kominiert. Laden Sie bitte das ergebnis des kombinierten Turniers hoch");
                }
            }

            var result = importer.SaveResult(this.competition);
            this.context.SaveChanges();

            var message = string.Join("\r\n", result);

            MainWindow.MainWindowHandle.ShowMessage(message);

            this.Competition.LastDtvUpload = DateTime.Now;
            this.Competition.DtvResultUploaded = true;
            this.context.SaveChanges();
        }

        private void EditCouple()
        {
            if (this.SelectedParticipant == null || this.SelectedParticipant.Couple.IsReadOnly)
            {
                return;
            }

            var dialog = new EditCoupleDialog(this.SelectedParticipant.Couple, this.context);
            MainWindow.MainWindowHandle.ShowChildWindow(dialog);
        }

        private void ShowStarCouplesDialog()
        {
            var dlg = new FindWdsfStarCouples(this.Competition);
            MainWindow.MainWindowHandle.ShowDialog(dlg, (p) => this.context.SaveChanges(), 300, 200);
        }

        private void EditTeamMembers()
        {
            var dlg = new SelectTeamMembers(new SelectTeamMembersViewModel(this.context, this.Competition.Participants.ToList().Where(p => p.Couple is Team).Select(p => p.Couple).Cast<Team>()));
            MainWindow.MainWindowHandle.ShowDialog(dlg, null, 300, 300);
        }

        #endregion
    }
}