﻿// // TPS.net TPS8 Scrutinus
// // CompetitionsViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using DataModel;
using DataModel.ModelHelper;
using DataModel.Models;
using DtvEsvModule;
using DtvEsvModule.Model;
using GalaSoft.MvvmLight.Command;
using General.Extensions;
using MahApps.Metro.Controls.Dialogs;
using Scrutinus.Dialogs;
using Scrutinus.Dialogs.DialogViewModels;
using Scrutinus.Localization;
using Scrutinus.MyHeats;
using Scrutinus.Pages;

namespace Scrutinus.ViewModels
{
    public class CompetitionsViewModel : BaseViewModel
    {
        #region Fields

        private readonly Dispatcher dispatcher;

        private ObservableCollection<Official> _competitionJudges;

        private ObservableCollection<DancesInCompetition> danceList;

        private string searchBoxContent;

        private Competition selectedCompetition;

        private bool uploadingMyHeats;

        #endregion

        #region Constructors and Destructors

        public CompetitionsViewModel()
        {
        }

        public CompetitionsViewModel(ScrutinusContext context)
        {
            this.context = context;
            this.InitializeViewModel();
            this.dispatcher = Dispatcher.CurrentDispatcher;
        }

        #endregion

        #region Public Properties

        public ICommand AddCompetitionCommand { get; set; }

        public ICommand AddDanceCommand { get; set; }

        public List<AgeGroup> AgeGroupList { get; set; }

        public IEnumerable<Dance> AvailableDances { get; set; }

        public Dance AvailableDancesSelectedItem { get; set; }

        public IEnumerable<Class> ClassesList { get; set; }

        public ICommand ClearSearchCommand { get; set; }

        public ICommand CombineCompetitionCommand { get; set; }

        public ICommand UploadMyHeatsCommand { get; set; }

        public bool UploadingMyHeats
        {
            get { return uploadingMyHeats;  }
            set
            {
                uploadingMyHeats = value;
                RaisePropertyChanged(nameof(UploadingMyHeats));
            }
        }

        public ObservableCollection<Official> CompetitionJudges
        {
            get
            {
                return this._competitionJudges;
            }
            set
            {
                this._competitionJudges = value;
                this.RaisePropertyChanged(() => this.CompetitionJudges);
            }
        }

        public ObservableCollection<Competition> CompetitionList { get; set; }

        public IEnumerable<CompetitionType> CompetitionTypeList { get; set; }

        public ObservableCollection<DancesInCompetition> DanceList
        {
            get
            {
                return this.danceList;
            }
            set
            {
                this.danceList = value;
                this.RaisePropertyChanged(() => this.DanceList);
            }
        }

        public ScrutinusContext DatabaseContext
        {
            get
            {
                return this.context;
            }
        }

        public ICommand DeleteCompetitionCommand { get; set; }

        public ICommand CopyCompetitionCommand { get; set; }

        public ICommand DownloadDtvCommand { get; set; }

        public ICommand EditEventCommand { get; set; }

        public IEnumerable<MarkingTypeItemValue> FinalTypes { get; set; }

        public IEnumerable<ItemValue> FirstRoundTypes { get; set; }

        public ICommand ImportWdsfCommand { get; set; }

        public ObservableCollection<Official> JudgesList { get; set; }

        public IEnumerable<MarkingTypeItemValue> MarkingTypes { get; set; }

        public List<OfficialWithRole> OfficialsDataSource { get; set; }

        public ICommand ResetDancesCommand { get; set; }

        public ICommand SaveDataCommand { get; set; }

        public string SearchBoxContent
        {
            get
            {
                return this.searchBoxContent;
            }
            set
            {
                this.searchBoxContent = value;
                this.RaisePropertyChanged(() => this.SearchBoxContent);
            }
        }

        public IEnumerable<Section> SectionsList { get; set; }

        public Competition SelectedCompetition
        {
            get
            {
                return this.selectedCompetition;
            }
            set
            {
                this.selectedCompetition = value;
                this.RaisePropertyChanged(() => this.SelectedCompetition);
            }
        }

        public ObservableCollection<Official> SelectedJudgesList { get; set; }

        public ObservableCollection<OfficialWithRole> SelectedOfficalsList { get; set; }

        public IEnumerable<ItemValue> StarsList { get; set; }

        public ICommand UploadDtvCommand { get; set; }

        #endregion

        #region Public Methods and Operators

        public void SearchTextChanges(string search)
        {
            if (!String.IsNullOrWhiteSpace(search))
            {
                this.JudgesList.Clear();
                var newList =
                    this.context.Officials.Where(
                        o =>
                        o.Roles.Any(r => r.Id == Roles.Judge)
                        && (o.Firstname.Contains(search) || o.Lastname.Contains(search))).ToList();
                this.JudgesList.AddRange(newList);
            }
            else
            {
                this.JudgesList.Clear();
                var judges =
                    this.context.Officials.Where(o => o.Roles.Any(r => r.Id == Roles.Judge)).ToList();
                this.JudgesList.AddRange(judges);

                var comp = this.SelectedCompetition;

                this.SelectedJudgesList.Clear();

                foreach (var judge in comp.Officials.Where(j => j.Role.Id == Roles.Judge))
                {
                    this.SelectedJudgesList.Add(judge.Official);
                }
            }
        }

        public override bool Validate()
        {
            return true;
        }

        #endregion

        #region Methods

        private void AddCompetition()
        {
            if (!this.context.AgeGroups.Any() || !this.context.Classes.Any())
            {
                MainWindow.MainWindowHandle.ShowMessageAsync(LocalizationService.Resolve(() => Text.PleaseCreateNewEvent), LocalizationService.Resolve(() => Text.EventDataNotValid));
                return;
            }

            var ev = this.context.Events.FirstOrDefault();

            if (ev == null)
            {
                ev = new Event();
                ev.Title = "Event";
                this.context.Events.Add(ev);

                try
                {
                    this.context.SaveChanges();
                }
                catch (DbEntityValidationException validation)
                {
                    foreach (var dbEntityValidationResult in validation.EntityValidationErrors)
                    {
                        ScrutinusContext.WriteLogEntry(LogLevel.Error, dbEntityValidationResult.ToString());
                        foreach (var dbValidationError in dbEntityValidationResult.ValidationErrors)
                        {
                            ScrutinusContext.WriteLogEntry(LogLevel.Error, $"{dbValidationError.ErrorMessage}, {dbValidationError.PropertyName}");
                        }
                    }
                }
            }

            var competitionType = this.context.CompetitionTypes.SingleOrDefault(t => t.Id == 4);

            // Create Competitions with Defaults:
            var comp = new Competition
                           {
                               AgeGroup = this.context.AgeGroups.First(),
                               Class = this.context.Classes.First(),
                               CompetitionType = competitionType ?? this.context.CompetitionTypes.First(),
                               FirstRoundType = RoundTypes.QualificationRound,
                               FinalType = DataModel.MarkingTypes.Skating,
                               Title = null,
                               MarkingType = DataModel.MarkingTypes.Marking,
                               Event = ev,
                               Section = this.context.Sections.First(),
                               StartTime = ev.DateFrom ?? new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day),
                               LastDtvDownload = DateTime.Now,
                               EndTime = null,
                               LateEntriesAllowed = true,
                               DefaultCouplesPerHeat = Settings.Default.DefaultCouplesPerHeat
                           };

            this.context.Competitions.Add(comp);

            try
            {
                this.context.SaveChanges();
            }
            catch (Exception ex)
            {
                MainWindow.MainWindowHandle.ShowMessage(ex.Message);
            }
            this.CompetitionList.Add(comp);
            // Does this work???
            this.SelectedCompetition = comp;

            MainWindow.MainWindowHandle.AddCompetitionToNavigationList(comp);
        }

        private void AddDance()
        {
            if (this.AvailableDancesSelectedItem == null)
            {
                return;
            }

            if (this.SelectedCompetition == null)
            {
                return;
            }

            var dance = this.AvailableDancesSelectedItem;

            var newObj = new DancesInCompetition
                             {
                                 Competition = this.SelectedCompetition,
                                 Dance = dance,
                                 DanceOrder = this.DanceList.Count,
                                 IsSolo = false
                             };

            this.DanceList.Add(newObj);
            this.SelectedCompetition.Dances.Add(newObj);
            this.context.SaveChanges();

            for (var i = 0; i < this.DanceList.Count; i++)
            {
                this.DanceList[i].DanceOrder = i + 1;
            }
        }

        private void ClearSearch()
        {
            this.SearchBoxContent = "";
            this.SearchTextChanges(this.SearchBoxContent);
        }

        private void CombineCompetitions()
        {
            var combineViewModel = new CombineCompetitionsViewModel(this.context);
            var combineCompetitionsView = new CombineCompetitions();
            combineCompetitionsView.DataContext = combineViewModel;
            MainWindow.MainWindowHandle.ShowDialog(combineCompetitionsView, this.UpdateCompetitionList, 420, 400);
        }

        private void CompetitionList_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
        }

        private List<OfficialWithRole> CreateOfficialWithRole()
        {
            var ret = new List<OfficialWithRole>();

            foreach (var role in this.context.Roles.ToList())
            {
                if (role.Id == Roles.Judge)
                {
                    continue;
                }
                var officials = this.context.Officials.Where(o => o.Roles.Any(r => r.Id == role.Id)).ToList();
                ret.AddRange(
                    officials.Select(
                        s =>
                        new OfficialWithRole
                            {
                                RoleId = role.Id,
                                Official = s,
                                Name = s.NiceName,
                                Role = "( " + role.RoleLong + ")"
                            }));
            }

            return ret;
        }

        private void DeleteCompetition(object selectedItems)
        {
            if (this.SelectedCompetition == null)
            {
                return;
            }

            if (this.SelectedCompetition.CurrentRound != null)
            {
                MainWindow.MainWindowHandle.ShowMessage(
                    LocalizationService.Resolve(() => Text.CompetitionStartedCanNotDelete));
                return;
            }

            var res =
                MainWindow.MainWindowHandle.ShowMessage(
                    LocalizationService.Resolve(() => Text.DeleteSelectedCompetition),
                    LocalizationService.Resolve(() => Text.DeleteCompetition),
                    MessageDialogStyle.AffirmativeAndNegative);

            if (res == MessageDialogResult.Affirmative)
            {

                try
                {
                    CompetitionHelper.DeleteCompetition(this.context, this.SelectedCompetition);
                    this.CompetitionList.Remove(this.SelectedCompetition);
                }
                catch (Exception)
                {
                    MainWindow.MainWindowHandle.ShowMessage(
                        LocalizationService.Resolve(() => Text.ErrorDeleteCompetition),
                        LocalizationService.Resolve(() => Text.ErrorDeleteCompetitionCaption),
                        MessageDialogStyle.Affirmative);
                }

                MainWindow.MainWindowHandle.UpdateNavigationList();
            }
        }

        private void DownloadDtvData()
        {
            var ev = this.context.Events.First();
            if (string.IsNullOrEmpty(ev.ExternalId))
            {
                MainWindow.MainWindowHandle.ShowMessage(
                    "Die DTV Veranstaltungsnummer ist nicht gesetzt. Bitte setzen Sie diese in den Veranstaltungsdaten");
                this.EditEvent();
                return;
            }

            var dtvId = 0;
            if (!int.TryParse(ev.ExternalId, out dtvId))
            {
                MainWindow.MainWindowHandle.ShowMessage(
                    "Die DTV Veranstaltungsnummer ist nicht gültig. Bitte setzen Sie diese in den Veranstaltungsdaten");
                this.EditEvent();
                return;
            }

            var apiClient = new ApiClient(
                Settings.Default.DtvApiUrl,
                Settings.Default.WDSFApiUser,
                Settings.Default.WDSFApiPassword);

            Veranstaltung dtvEvent;

            try
            {
                dtvEvent = apiClient.GetVeranstaltung(dtvId);
                if (dtvEvent == null)
                {
                    MainWindow.MainWindowHandle.ShowMessage(
                        "Die DTV Veranstaltung konnte nicht gefunden werden. Bitte setzen Sie diese in den Veranstaltungsdaten");
                    this.EditEvent();
                    return;
                }
            }
            catch (Exception ex)
            {
                MainWindow.MainWindowHandle.ShowMessage("Fehler beim Laden der Veranstaltung: \r\n" + ex.Message);
                return;
            }
            Task.Factory.StartNew(
                () =>
                    {
                        MainWindow.MainWindowHandle.ReportStatus("Lade DTV Daten herunter", 0, 0);

                        try
                        {
                            var importer = new DtvApiImporter(
                                Settings.Default.DtvApiUrl,
                                Settings.Default.WDSFApiUser,
                                Settings.Default.WDSFApiPassword,
                                this.context);
                            importer.ImportAsync(
                                MainWindow.MainWindowHandle,
                                dtvEvent,
                                MainWindow.License != null && MainWindow.License.Ejudge
                                    ? DataModel.MarkingTypes.Marking
                                    : DataModel.MarkingTypes.MarksSumOnly).ContinueWith(new Action<Task>(
                                        (t) =>
                                            {
                                                this.dispatcher.Invoke(
                                new Action(
                                    () =>
                                    {
                                        MainWindow.MainWindowHandle.ShowMessage(
                                            "Der Download wurde erfolgreich abgeschlossen");
                                        this.RefreshAfterSync();
                                    }));
                                            }));

                            // Now update everything 
                            
                        }
                        catch (Exception ex)
                        {
                            MainWindow.MainWindowHandle.ShowMessage(
                                "Fehler beim Laden der Startlisten: \r\n" + ex.Message);
                        }
                    });
        }

        private void EditEvent()
        {
            var editEventView = new EditEvent();
            MainWindow.MainWindowHandle.ShowDialog(editEventView, null, 600, 320);
        }

        private void ImportWDSF()
        {
            var dlg = new ImportCompetitionFromWDSF(this.context);
            MainWindow.MainWindowHandle.ShowDialog(dlg, this.OnImportWDSFClosed, 400, 400);
        }

        private void InitializeViewModel()
        {
            this.AgeGroupList = this.context.AgeGroups.ToList();
            this.JudgesList =
                new ObservableCollection<Official>(
                    this.context.Officials.Where(o => o.Roles.Any(r => r.Id == Roles.Judge)));

            this.CompetitionTypeList = this.context.CompetitionTypes.ToList();
            this.StarsList = ComboBoxItemSources.GetStarTypeList();
            this.FirstRoundTypes = ComboBoxItemSources.GetFirstRoundType();
            this.FinalTypes = ComboBoxItemSources.GetMarkingTypeList();
            this.SectionsList = this.context.Sections.ToList();
            this.MarkingTypes = ComboBoxItemSources.GetMarkingTypeList();
            this.ClassesList = this.context.Classes.ToList();
            this.AvailableDances = this.context.Dances.ToList();

            this.OfficialsDataSource = this.CreateOfficialWithRole();

            this.SelectedJudgesList = new ObservableCollection<Official>();
            this.SelectedOfficalsList = new ObservableCollection<OfficialWithRole>();
            this.DanceList = new ObservableCollection<DancesInCompetition>();

            this.CompetitionList = new ObservableCollection<Competition>();
            this.CompetitionList.CollectionChanged += this.CompetitionList_CollectionChanged;
            this.CompetitionList.AddRange(this.context.Competitions);

            // Setup the Commands
            this.ImportWdsfCommand = new RelayCommand(this.ImportWDSF);
            this.AddDanceCommand = new RelayCommand(this.AddDance);
            this.SaveDataCommand = new RelayCommand(this.SaveData);
            this.AddCompetitionCommand = new RelayCommand(this.AddCompetition);
            this.DeleteCompetitionCommand = new RelayCommand<object>(this.DeleteCompetition);
            this.ClearSearchCommand = new RelayCommand(this.ClearSearch);
            this.EditEventCommand = new RelayCommand(this.EditEvent);
            this.CombineCompetitionCommand = new RelayCommand(this.CombineCompetitions);
            this.DownloadDtvCommand = new RelayCommand(this.DownloadDtvData);
            this.UploadDtvCommand = new RelayCommand(this.UploadDtvData);
            this.CopyCompetitionCommand = new RelayCommand(this.CopyCompetition);
            this.UploadMyHeatsCommand = new RelayCommand(this.SyncMyHeats);
        }

        private void SyncMyHeats()
        {
            this.UploadingMyHeats = true;

            var task = Task.Factory.StartNew(() =>
            {
                MyHeatsService.UploadData();

                this.dispatcher.Invoke(() => { this.UploadingMyHeats = false; });
            });
        }

        private void OnImportWDSFClosed(object sender)
        {
            var complist = this.context.Competitions.ToList();
            this.CompetitionList.Clear();
            this.CompetitionList.AddRange(complist);
        }

        private void RefreshAfterSync()
        {
            this.CompetitionList.Clear();
            MainWindow.MainWindowHandle.ViewModel.Competitions.Clear();
            MainWindow.MainWindowHandle.ViewModel.ClearCompetitions();

            foreach (var competition in this.context.Competitions)
            {
                this.CompetitionList.Add(competition);
                MainWindow.MainWindowHandle.AddCompetitionToNavigationList(competition);
            }
        }

        private void SaveJudges()
        {
            // Sync Judges
            var competition = this.SelectedCompetition;

            if (competition == null)
            {
                MainWindow.MainWindowHandle.ShowMessage(LocalizationService.Resolve(() => Text.SelectCompetition));
                return;
            }

            foreach (var official in competition.Officials.ToList())
            {
                this.context.OfficialCompetition.Remove(official);
            }
            competition.Officials.Clear();
            foreach (var judge in this.SelectedJudgesList)
            {
                this.context.OfficialCompetition.Add(
                    new OfficialInCompetition
                        {
                            Competition = competition,
                            Official = judge,
                            Role = this.context.Roles.Single(r => r.Id == Roles.Judge),
                        });
            }
            foreach (var official in this.SelectedOfficalsList)
            {
                this.context.OfficialCompetition.Add(
                    new OfficialInCompetition
                        {
                            Competition = competition,
                            Official = official.Official,
                            Role = this.context.Roles.Single(r => r.Id == official.RoleId),
                        });
            }
        }

        private void UpdateCompetitionList(object sender)
        {
            this.CompetitionList.Clear();

            foreach (var competition in this.context.Competitions)
            {
                this.CompetitionList.Add(competition);
            }

            MainWindow.MainWindowHandle.UpdateNavigationList();
        }

        private void UploadDtvData()
        {
            if (this.context.Competitions.Any(c => c.State == CompetitionState.Startlist && !c.Canceled))
            {
                MainWindow.MainWindowHandle.ShowMessage(
                    "Hinweis: Nicht alle Turniere sind abgeschlossen. Bitte markieren ausgefallene Turniere bei Bedarf. Dies können Sie in den Turnierdaten machen.");
            }

            if (
                this.context.Officials.ToList()
                    .Any(
                        o =>
                        o.Roles.Any(
                            r =>
                            r.Id == Roles.Judge || r.Id == Roles.Associate || r.Id == Roles.Chairman
                            || r.Id == Roles.Supervisor) && string.IsNullOrEmpty(o.ExternalId)))
            {
                var result =
                    MainWindow.MainWindowHandle.ShowMessage(
                        "Hinweis: Nicht alle Funktionäre haben die DTV-Id gesetzt. Dennoch senden?",
                        "DTV-Id nicht gesetzt",
                        MessageDialogStyle.AffirmativeAndNegative);

                if (result == MessageDialogResult.Negative)
                {
                    return;
                }
            }

            MainWindow.MainWindowHandle.ReportStatus("Sende DTV Daten", 0, 0);

            Task.Factory.StartNew(
                () =>
                    {
                        MainWindow.MainWindowHandle.ReportStatus("Sende DTV Daten:", 0, 0);

                        var importer = new DtvApiImporter(
                            Settings.Default.DtvApiUrl,
                            Settings.Default.WDSFApiUser,
                            Settings.Default.WDSFApiPassword, this.context);

                        var competitions = this.context.Competitions.Where(
                                c => (c.State == CompetitionState.Finished || c.Canceled) && !c.DtvResultUploaded).ToList();

                        var allCompetitions = this.context.Competitions.ToList();

                        if (!competitions.Any())
                        {
                            MainWindow.MainWindowHandle.ShowMessage(
                                "Derzeit gibt es keine abgeschlossenen Turniere, deren Daten noch nicht gesendet wurden.");
                        }

                        var competitionIndex = 0;
                        foreach (var competition in competitions)
                        {
                            MainWindow.MainWindowHandle.ReportStatus(
                                "Sende Ergebnis für Turnier " + competition.Title,
                                competitionIndex,
                                competitions.Count());

                            competitionIndex++;

                            if (competition.Canceled && competition.ExternalId != null)
                            { 
                                // Do not send if another combined
                                // competition exists
                                if (allCompetitions.Any(c =>
                                    c.Id != competition.Id && c.ExternalId != null && c.ExternalId.Contains(competition.ExternalId)))
                                {
                                    competition.ResultSend = true;
                                    continue;
                                }
                            } else if (competition.Canceled)
                            {
                                continue;
                            }

                            var result = importer.SaveResult(competition);

                            var message = string.Join("\n\r", result);

                            if (message.Contains("Ergebnis wurde gespeichert") || message == "\"OK\"")
                            {
                                // Alle Startbuch-Einträge schreiben
                                competition.DtvResultUploaded = true;
                                this.CompetitionList.First(c => c.Id == competition.Id).DtvResultUploaded = true;
                                competition.LastDtvUpload = DateTime.Now;
                            }
                            else
                            {
                                MainWindow.MainWindowHandle.ShowMessage(
                                message,
                                "Ergebnis für Turnier " + competition.Title,
                                MessageDialogStyle.Affirmative);
                            }

                            try
                            {
                                var digitalSheetsSend = false;

                                if (competition.JudgingSheets.Any())
                                {
                                    importer.SendJudgingSheets(competition);
                                }

                                if (competition.Rounds.Any() && competition.Rounds.First().EjudgeData.Any())
                                {
                                    digitalSheetsSend = importer.SendDigitalJudgingSheets(competition).Any();
                                }
                                else if (Directory.Exists(Settings.Default.MobileControlExchange))
                                {
                                    digitalSheetsSend =
                                        importer.SendDigitalJudgingSheetsFromMobileControl(
                                            Settings.Default.MobileControlExchange,
                                            competition).Any();
                                }

                                if (!competition.JudgingSheets.Any() && !digitalSheetsSend && !competition.Canceled)
                                {
                                    MainWindow.MainWindowHandle.ShowMessage(
                                        string.Format(
                                            "Für Turnier {0} waren keine Wertungsrichterzettel vorhanden!",
                                            competition.Title));
                                }
                            }
                            catch (Exception ex)
                            {
                                MainWindow.MainWindowHandle.ShowMessage(
                                    "Fehler beim Hochladen der Wertungsrichterzettel!\r\n" + ex.Message);
                            }
                        }

                        MainWindow.MainWindowHandle.RemoveStatusReport();
                    }).ContinueWith(
                        t =>
                        {
                            this.context.SaveChanges();
                            if (t.IsFaulted && t.Exception != null)
                            {
                                MainWindow.MainWindowHandle.ShowMessage(
                                    "Fehler beim Hochladen der Daten: " + t.Exception.Message);
                            }
                        });
        }

        private void CopyCompetition()
        {
            if (this.SelectedCompetition == null)
            {
                return;
            }

            var newCompetition = new Competition()
                                     {
                                         Event = this.SelectedCompetition.Event,
                                         AgeGroup = this.SelectedCompetition.AgeGroup,
                                         Class = this.SelectedCompetition.Class,
                                         CompetitionType = this.SelectedCompetition.CompetitionType,
                                         Title = this.SelectedCompetition.Title + " Copy",
                                         DefaultCouplesPerHeat = Settings.Default.DefaultCouplesPerHeat
                                     };

            this.context.Competitions.Add(newCompetition);

            foreach (var participant in this.SelectedCompetition.Participants)
            {
                var newParticipant = new Participant()
                                         {
                                             Competition = newCompetition,
                                             AgeGroup = newCompetition.AgeGroup,
                                             Class = newCompetition.Class,
                                             Number = participant.Number,
                                             Couple = participant.Couple
                                         };

                newCompetition.Participants.Add(newParticipant);
            }

            foreach (var officialInCompetition in this.SelectedCompetition.Officials)
            {
                var official = new OfficialInCompetition()
                                   {
                                       Competition = newCompetition,
                                       Official = officialInCompetition.Official,
                                       Role = officialInCompetition.Role
                                   };

                newCompetition.Officials.Add(official);
            }

            this.CompetitionList.Add(newCompetition);
            this.context.SaveChanges();
        }

        #endregion
    }
}