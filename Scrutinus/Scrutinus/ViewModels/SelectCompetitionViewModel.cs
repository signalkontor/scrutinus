// // TPS.net TPS8 Scrutinus
// // SelectCompetitionViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System.Windows.Input;
using DataModel;
using DataModel.Models;
using GalaSoft.MvvmLight;

namespace Scrutinus.ViewModels
{
    public class SelectCompetitionViewModel : ViewModelBase
    {
        #region Fields

        private Competition competition;

        private bool isSelected;

        private CoupleState coupleState;

        private int participantId;

        #endregion

        #region Public Properties

        public SelectCompetitionViewModel(EditParticipantsViewModel editParticipantsViewModel)
        {
            this.SetStateDancingCommand = editParticipantsViewModel.SetSingleCompetitionDancingCommand;
            this.SetStateExcusedCommand = editParticipantsViewModel.SetSingleCompetitioneExcusedCommand;
            this.SetStateMissingCommand = editParticipantsViewModel.SetSingleCompetitionMissingCommand;
        }

        public Competition Competition
        {
            get
            {
                return this.competition;
            }
            set
            {
                this.competition = value;
                this.RaisePropertyChanged(() => this.Competition);
            }
        }

        public bool IsSelected
        {
            get
            {
                return this.isSelected;
            }
            set
            {
                this.isSelected = value;
                this.RaisePropertyChanged(() => this.IsSelected);
            }
        }

        public CoupleState CoupleState
        {
            get { return this.coupleState; }
            set
            {
                this.coupleState = value; 
                this.RaisePropertyChanged(() => this.CoupleState);
            }
        }

        public int ParticipantId
        {
            get { return this.participantId; }
            set
            {
                this.participantId = value;
                this.RaisePropertyChanged(() => this.ParticipantId);
            }
        }

        public ICommand SetStateMissingCommand { get; set; }

        public ICommand SetStateDancingCommand { get; set; }

        public ICommand SetStateExcusedCommand { get; set; }

        #endregion
    }
}