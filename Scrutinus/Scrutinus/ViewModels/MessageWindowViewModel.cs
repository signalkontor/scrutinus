﻿// // TPS.net TPS8 Scrutinus
// // MessageWindowViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;

namespace Scrutinus.ViewModels
{
    public class MessageWindowViewModel
    {
        #region Constructors and Destructors

        public MessageWindowViewModel()
        {
            this.ButtonOkClickCommand = new RelayCommand(this.ButtonOkClick);
            this.ButtonCancelClickCommand = new RelayCommand(this.ButtonCancelClick);
            this.ButtonYesClickCommand = new RelayCommand(this.ButtonYesClick);
            this.ButtonNoClickCommand = new RelayCommand(this.ButtonNoClick);

            this.Result = MessageBoxResult.None;
        }

        #endregion

        #region Public Properties

        public ICommand ButtonCancelClickCommand { get; set; }

        public ICommand ButtonNoClickCommand { get; set; }

        public ICommand ButtonOkClickCommand { get; set; }

        public ICommand ButtonYesClickCommand { get; set; }

        public string Message { get; set; }

        public MessageBoxResult Result { get; set; }

        public bool ShowCancelButton { get; set; }

        public bool ShowNoButton { get; set; }

        public bool ShowOkButton { get; set; }

        public bool ShowYesButton { get; set; }

        public string Title { get; set; }

        #endregion

        #region Methods

        private void ButtonCancelClick()
        {
            this.Result = MessageBoxResult.Cancel;
        }

        private void ButtonNoClick()
        {
            this.Result = MessageBoxResult.No;
        }

        private void ButtonOkClick()
        {
            this.Result = MessageBoxResult.OK;
        }

        private void ButtonYesClick()
        {
            this.Result = MessageBoxResult.Yes;
        }

        #endregion
    }
}