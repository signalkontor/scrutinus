﻿// // TPS.net TPS8 Scrutinus
// // EventStateViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Windows.Threading;
using DataModel;
using DataModel.Models;
using GalaSoft.MvvmLight;

namespace Scrutinus.ViewModels
{
    internal class EventStateItem : ViewModelBase
    {
        #region Fields

        private int dancing;

        private int excused;

        private int missing;

        private int registered;

        #endregion

        #region Public Properties

        public Competition Competition { get; set; }

        public int Dancing
        {
            get
            {
                return this.dancing;
            }
            set
            {
                this.dancing = value;
                this.RaisePropertyChanged(() => this.Dancing);
            }
        }

        public int Excused
        {
            get
            {
                return this.excused;
            }
            set
            {
                this.excused = value;
                this.RaisePropertyChanged(() => this.Excused);
            }
        }

        public int Missing
        {
            get
            {
                return this.missing;
            }
            set
            {
                this.missing = value;
                this.RaisePropertyChanged(() => this.Missing);
            }
        }

        public int Registered
        {
            get
            {
                return this.registered;
            }
            set
            {
                this.registered = value;
                this.RaisePropertyChanged(() => this.Registered);
            }
        }

        #endregion
    }

    internal class EventStateViewModel : ViewModelBase
    {
        #region Constructors and Destructors

        public EventStateViewModel()
        {
            this.dispatcher = Dispatcher.CurrentDispatcher;
            this.EventItems = new ObservableCollection<EventStateItem>();
            this.threadRunning = true;
            this.updateDataThread = new Thread(this.UpdateThreadMethod) { IsBackground = true };
            this.updateDataThread.Start();
        }

        #endregion

        #region Public Properties

        public ObservableCollection<EventStateItem> EventItems { get; set; }

        #endregion

        #region Public Methods and Operators

        public void StopUpdateThread()
        {
            this.threadRunning = false;
        }

        #endregion

        #region Methods

        private void UpdateThreadMethod()
        {
            while (this.threadRunning)
            {
                using (var context = new ScrutinusContext())
                {
                    foreach (
                        var competition in context.Competitions.ToList().Where(c => c.IsCompetitionNotStarted))
                    {
                        var dancing = competition.Participants.Count(p => p.State == CoupleState.Dancing);
                        var excused = competition.Participants.Count(p => p.State == CoupleState.Excused);
                        var missing = competition.Participants.Count(p => p.State == CoupleState.Missing);
                        var total = competition.Participants.Count;

                        this.dispatcher.Invoke(
                            new Action(
                                () =>
                                    {
                                        var eventItem =
                                            this.EventItems.SingleOrDefault(c => c.Competition.Id == competition.Id);

                                        if (eventItem == null)
                                        {
                                            this.EventItems.Add(
                                                new EventStateItem
                                                    {
                                                        Competition = competition,
                                                        Dancing = dancing,
                                                        Excused = excused,
                                                        Missing = missing,
                                                        Registered = total
                                                    });
                                        }
                                        else
                                        {
                                            eventItem.Registered = total;
                                            eventItem.Dancing = dancing;
                                            eventItem.Excused = excused;
                                            eventItem.Missing = missing;
                                        }
                                    }));
                    }
                }

                Thread.Sleep(5000);
            }
        }

        #endregion

        #region Fields

        private readonly Dispatcher dispatcher;

        private bool threadRunning;

        private readonly Thread updateDataThread;

        #endregion
    }
}