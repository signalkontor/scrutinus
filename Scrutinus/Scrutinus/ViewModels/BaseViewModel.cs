﻿// // TPS.net TPS8 Scrutinus
// // BaseViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using DataModel.Models;
using GalaSoft.MvvmLight;

namespace Scrutinus.ViewModels
{
    public abstract class BaseViewModel : ViewModelBase
    {
        #region Fields

        protected ScrutinusContext context = new ScrutinusContext();

        #endregion

        #region Constructors and Destructors

        public BaseViewModel()
        {
            this.DoRaiseEvents = true;
        }

        #endregion

        #region Public Properties

        public bool DoRaiseEvents { get; set; }

        public virtual bool IsVisible { get; set; }

        #endregion

        #region Public Methods and Operators

        public virtual void SaveData()
        {
            this.context.SaveChanges();
        }

        public abstract bool Validate();

        #endregion
    }
}