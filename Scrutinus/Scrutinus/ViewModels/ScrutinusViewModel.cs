﻿// // TPS.net TPS8 Scrutinus
// // ScrutinusViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.ComponentModel;

namespace Scrutinus.ViewModels
{
    public abstract class ScrutinusViewModelOld : INotifyPropertyChanged
    {
        #region Public Events

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Public Methods and Operators

        public abstract bool CanSave();

        public void RaiseEvent(string property)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        public abstract void SaveData();

        #endregion
    }
}