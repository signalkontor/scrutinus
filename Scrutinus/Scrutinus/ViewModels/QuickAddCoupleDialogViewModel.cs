﻿// // TPS.net TPS8 Scrutinus
// // QuickAddCoupleDialogViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Input;
using DataModel;
using DataModel.Messages;
using DataModel.Models;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using General.Extensions;
using mobileControl.Helper;
using Scrutinus.Localization;

namespace Scrutinus.ViewModels
{
    public class QuickAddCoupleDialogViewModel : BaseViewModel, IDataErrorInfo
    {
        #region Public Indexers

        public string this[string columnName]
        {
            get
            {
                if (columnName == "StartNumber")
                {
                    if (this.StartNumber == 0)
                    {
                        return LocalizationService.Resolve(() => Text.ZeroIsNotAValidNumber);
                    }

                    if (this.competition.Participants.Any(p => p.Number == this.StartNumber))
                    {
                        return LocalizationService.Resolve(() => Text.StartnumberAlreadyInUs);
                    }
                }

                return string.Empty;
            }
        }

        #endregion

        #region Fields

        private readonly Competition competition;

        private int? heatNumber;

        private bool isFiltered;

        private Couple selectedCouple;

        private int startNumber;

        #endregion

        #region Constructors and Destructors

        public QuickAddCoupleDialogViewModel()
        {
        }

        public QuickAddCoupleDialogViewModel(int competitionId, bool createCoupleAllowed)
        {
            this.competition = this.context.Competitions.Single(c => c.Id == competitionId);

            if (this.competition.Participants.Count > 0)
            {
                this.StartNumber = this.competition.Participants.Max(p => p.Number) + 1;
            }
            else
            {
                this.StartNumber = 1;
            }

            this.CreateCoupleAllowed = createCoupleAllowed;

            this.AddNewCoupleCommand = new RelayCommand(this.AddNewCouple);
            this.SaveAndCloseCommand = new RelayCommand(this.SaveAndClose);
            this.AddCoupleToCompetitionCommand = new RelayCommand(this.AddCoupleToCompetition);
            this.CancelCommand = new RelayCommand(this.Cancel);
            // Fill list with all couples from our database
            this.Couples = new ObservableCollection<Couple>(this.context.Couples);

            if (this.competition.CurrentRound == null)
            {
                return;
            }

            var currentRound = this.context.Rounds.SingleOrDefault(r => r.Id == this.competition.CurrentRound.Id);
            // Fill available Heats based on current round:
            if (currentRound == null || this.competition.CurrentRound == null || !currentRound.Drawings.Any())
            {
                return;
            }

            var max = currentRound.Drawings.Max(d => d.Heat);
            this.HeatNumbers = new List<ItemValue>();
            for (var i = 0; i <= max; i++)
            {
                this.HeatNumbers.Add(
                    new ItemValue(i, string.Format(LocalizationService.Resolve(() => Text.HeatNumberString), i + 1)));
            }
            this.HeatNumber = 0;
        }

        #endregion

        #region Public Properties

        public ICommand AddCoupleToCompetitionCommand { get; set; }

        public ICommand AddNewCoupleCommand { get; set; }

        public ICommand CancelCommand { get; set; }

        public ObservableCollection<Couple> Couples { get; set; }

        public bool CreateCoupleAllowed { get; set; }

        public string Error
        {
            get
            {
                return !this.NumberIsValid() ? "Number not valid!" : string.Empty;
            }
        }

        public int? HeatNumber
        {
            get
            {
                return this.heatNumber;
            }
            set
            {
                this.heatNumber = value;
                this.RaisePropertyChanged(() => this.HeatNumber);
            }
        }

        public List<ItemValue> HeatNumbers { get; set; }

        public ICommand SaveAndCloseCommand { get; set; }

        public Couple SelectedCouple
        {
            get
            {
                return this.selectedCouple;
            }
            set
            {
                this.selectedCouple = value;
                if (this.selectedCouple != null && this.selectedCouple.NumberInEvent.HasValue && this.selectedCouple.NumberInEvent.Value > 0)
                {
                    this.StartNumber = this.selectedCouple.NumberInEvent.Value;
                }

                this.RaisePropertyChanged(() => this.SelectedCouple);
            }
        }

        public int StartNumber
        {
            get
            {
                return this.startNumber;
            }
            set
            {
                this.startNumber = value;
                this.RaisePropertyChanged(() => this.StartNumber);
            }
        }

        #endregion

        #region Public Methods and Operators

        public void ClearSearchFilter()
        {
            if (!this.isFiltered)
            {
                return;
            }
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            this.Couples.Clear();
            Debug.WriteLine("Timer after Clear: " + stopwatch.ElapsedMilliseconds);
            this.Couples.Clear();
            this.Couples.AddRange(this.context.Couples.ToList());
            Debug.WriteLine("Timer after AddRange in ClearSearchFilter: " + stopwatch.ElapsedMilliseconds);
            this.isFiltered = false;
        }

        public void UpdateSearchFilter(string filter)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            var couples =
                this.context.Couples.Where(
                    c =>
                    c.FirstMan.Contains(filter) || c.LastMan.Contains(filter) || c.FirstWoman.Contains(filter)
                    || c.LastWoman.Contains(filter));

            Debug.WriteLine("Timer after where: " + stopwatch.ElapsedMilliseconds);

            this.Couples.Clear();
            Debug.WriteLine("Timer after Clear: " + stopwatch.ElapsedMilliseconds);
            this.Couples.AddRange(couples.ToList()); //.AddRange(couples);
            this.isFiltered = true;
            Debug.WriteLine("Timer after AddRange: " + stopwatch.ElapsedMilliseconds);
            this.RaisePropertyChanged(() => this.Couples);
        }

        public override bool Validate()
        {
            return true;
        }

        #endregion

        #region Methods

        private void AddCoupleToCompetition()
        {
            if (!this.NumberIsValid())
            {
                return;
            }

            if (this.SelectedCouple == null)
            {
                return;
            }

            var participant = new Participant
                                  {
                                      Couple = this.SelectedCouple,
                                      Competition = this.competition,
                                      State = CoupleState.Dancing,
                                      Star = 0,
                                      Number = this.StartNumber,
                                      QualifiedRound = this.competition.CurrentRound,
                                      AgeGroup = this.competition.AgeGroup,
                                      Class = this.competition.Class,
                                      RegistrationFlag = RegistrationFlag.Late,
                                  };

            // Do we have a startbook of this participant?
            if (this.SelectedCouple.Startbooks.Any(s => s.Section == this.competition.Section))
            {
                var startbook =
                    this.SelectedCouple.Startbooks.First(s => s.Section == this.competition.Section);

                participant.Class = startbook.Class;
                participant.OriginalPlacings = startbook.OriginalPlacings;
                participant.OriginalPoints = startbook.OriginalPoints;
                participant.TargetClass = startbook.NextClass;
                participant.TargetPlacings = startbook.TargetPlacings;
                participant.TargetPoints = startbook.TargetPoints;
                participant.MinimumPoints = startbook.MinimumPoints;
                participant.PlacingsUpto = startbook.PlacingsUpto;
            }

            this.context.Participants.Add(participant);
            this.context.SaveChanges();

            if (participant.QualifiedRound != null)
            {
                // Add a qualified with this pasrticipant:
                var qualified = new Qualified
                                    {
                                        Participant = participant,
                                        QualifiedNextRound = false,
                                        Round = participant.QualifiedRound
                                    };

                this.context.Qualifieds.Add(qualified);
                this.context.SaveChanges();
            }

            this.StartNumber++;

            if (this.HeatNumber.HasValue && this.competition.CurrentRound != null)
            {
                var round = this.context.Rounds.Single(r => r.Id == this.competition.CurrentRound.Id);
                // Add heats
                foreach (var danceRound in round.DancesInRound)
                {
                    this.context.Drawings.Add(
                        new Drawing
                            {
                                DanceRound = danceRound,
                                Heat = this.HeatNumber.Value,
                                Participant = participant,
                                Round = this.competition.CurrentRound
                            });
                }

                this.context.SaveChanges();

                // If we are using ejudge -> send a message:
                if (this.competition.CurrentRound.EjudgeEnabled)
                {
                    ScrutinusCommunicationHandler.AddCouple(this.competition.CurrentRound, participant.Number.ToString(), 1, this.HeatNumber.Value);
                }
            }

            Messenger.Default.Send(new StartlistChangesMessage { CompetitionId = this.competition.Id });
        }

        private void AddNewCouple()
        {
            var couple = new Couple();

            this.Couples.Add(couple);
            this.SelectedCouple = couple;
            this.context.Couples.Add(couple);
            this.context.SaveChanges();

            this.RaisePropertyChanged(() => this.SelectedCouple);

            // this.AddCoupleToCompetition();
        }

        private void Cancel()
        {
            MainWindow.MainWindowHandle.CloseDialog();
        }

        private bool NumberIsValid()
        {
            if (this.StartNumber == 0)
            {
                return false;
            }

            if (this.competition.Participants.Any(p => p.Number == this.StartNumber))
            {
                return false;
            }

            return true;
        }

        private void SaveAndClose()
        {
            this.context.SaveChanges();
            MainWindow.MainWindowHandle.CloseDialog();
            // Broadcast startlist might have changed ...
            Messenger.Default.Send(new StartlistChangesMessage { CompetitionId = this.competition.Id });
        }

        #endregion
    }
}