﻿// // TPS.net TPS8 Scrutinus
// // FinalMarkingViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Linq;
using DataModel;
using DataModel.Models;
using DtvEsvModule;

namespace Scrutinus.ViewModels
{
    internal class FinalMarkingViewModel : BaseViewModel
    {
        #region Constructors and Destructors

        public FinalMarkingViewModel(ScrutinusContext context, int competitionId, int roundId)
        {
            this.context = context;
            this.competitionId = competitionId;
            this.roundId = roundId;
            // Calculate places for dances (if we have all markings)
            this.Initialize();
        }

        #endregion

        private class SkatingResult
        {
            #region Public Properties

            public Participant Participant { get; set; }

            public double Places { get; set; }

            public double Sums { get; set; }

            #endregion
        }

        // Markings per Dance and judge

        #region Fields

        private readonly int competitionId;

        private readonly int roundId;

        private Competition competition;

        private Round round;

        #endregion

        #region Public Properties

        public List<DanceInRound> Dances { get; set; }

        public List<Official> Judges { get; set; }

        public List<Marking> Markings { get; set; }

        public int NumCouples { get; set; }

        public int NumJudges { get; set; }

        public List<Participant> Participants { get; set; }

        public List<ResultCouple> ResultCouple { get; set; }

        public List<DanceResult> Results { get; set; }

        public double Sum { get; set; }

        #endregion

        #region Public Methods and Operators

        public void CalculatePlaces(int danceId)
        {
            var qualifieds = this.round.Qualifieds.ToList();
            // First: Calculate number of places and the sums for majority calculation
            for (var i = 0; i < qualifieds.Count; i++)
            {
                for (var place = 1; place <= qualifieds.Count; place++)
                {
                    var result =
                        this.ResultCouple.SingleOrDefault(
                            r =>
                            r.ParticipantId == qualifieds[i].Participant.Id && r.DanceId == danceId && r.Place == place);

                    result.CountPlaces =
                        this.Markings.Count(
                            m => m.Dance.Id == danceId && m.Participant.Id == result.ParticipantId && m.Mark <= place);
                    result.SumPlaces =
                        this.Markings.Where(
                            m => m.Dance.Id == danceId && m.Participant.Id == result.ParticipantId && m.Mark <= place)
                            .Sum(m => m.Mark);

                    result.IsSet = false;
                    result.IsUsed = false;
                }
            }
            // What is our last place???
            var lastPlace = qualifieds.Count;
            var majority = this.round.Competition.GetJudges().Count / 2 + 1;
            var currentPlace = 1;

            while (currentPlace <= lastPlace)
            {
                currentPlace = this.SetPlaces(null, danceId, majority, lastPlace, currentPlace, currentPlace);
            }
        }

        public override bool Validate()
        {
            if (this.Markings.All(m => m.Mark != 0))
            {
#if DTV
                var apiImporter = new DtvApiImporter("", "", "", this.context);
                apiImporter.CalculateChecksum(this.competition);
                
#endif
                this.context.SaveChanges();

                return true;
            }

            return false;
        }

        #endregion

        #region Methods

        protected void MarkAsSet(int dance, int participant)
        {
            foreach (
                var re in this.ResultCouple.Where(c => c.ParticipantId == participant && c.DanceId == dance))
            {
                re.IsSet = true;
            }
        }

        private void CalculatePlaces()
        {
            var sorted = this.round.Qualifieds.OrderBy(o => o.Points).ToList();

            foreach (var qualified in sorted)
            {
                qualified.PlaceFrom = 0;
                qualified.PlaceTo = 0;
            }

            var placefrom = 1;
            for (var i = 0; i < sorted.Count - 1; i++)
            {
                sorted[i].PlaceFrom = placefrom;
                sorted[i].PlaceTo = placefrom;
                if (sorted[i + 1].Points == sorted[i].Points)
                {
                    var index = i + 1;
                    var placecount = 1;
                    while (index < sorted.Count && sorted[i].Points == sorted[index].Points)
                    {
                        index++;
                        placecount++;
                    }
                    for (var j = i; j < index; j++)
                    {
                        sorted[j].PlaceFrom = placefrom;
                        sorted[j].PlaceTo = placefrom + placecount - 1;
                    }
                    i = index - 1;
                    placefrom += placecount - 1;
                }
                placefrom++;
            }
            if (sorted[sorted.Count - 1].PlaceFrom == 0)
            {
                sorted[sorted.Count - 1].PlaceFrom = placefrom;
                sorted[sorted.Count - 1].PlaceTo = placefrom;
            }

            foreach (var qualified in this.round.Qualifieds)
            {
                qualified.Participant.Points = qualified.Points;
                qualified.Participant.PlaceFrom = qualified.PlaceFrom;
                qualified.Participant.PlaceTo = qualified.PlaceTo;
            }

            if (this.round.Qualifieds.Any(q => q.PlaceFrom != q.PlaceTo))
            {
                this.DoSkating();
            }
        }

        private void DoSkating()
        {
            var list = this.round.Qualifieds.OrderBy(o => o.PlaceFrom).ToList();

            var participantIds = new List<int>();
            // Now check for any same Places and do skating for them
            for (var i = 1; i < list.Count; i++)
            {
                if (list[i - 1].PlaceFrom == list[i].PlaceFrom)
                {
                    if (!participantIds.Contains(list[i - 1].Participant.Id))
                    {
                        participantIds.Add(list[i - 1].Participant.Id);
                    }
                    participantIds.Add(list[i].Participant.Id);
                }
                else
                {
                    this.DoSkating(participantIds);
                    participantIds.Clear();
                }
            }
            if (participantIds.Count > 0)
            {
                this.DoSkating(participantIds);
            }
        }

        private void DoSkating(IEnumerable<int> particpipantIds)
        {
            // OK, same place eg same sum ... lets try rule 10 and 11
            // Rule 10 is a sort of majority system based on the result of dances
            // What place are we actually checking?
            var results = (from id in particpipantIds
                                           let partipant = this.context.Participants.Single(p => p.Id == id)
                                           let place =
                                               this.Results.Count(
                                                   r => r.ParticipantId == id && r.Place <= partipant.PlaceFrom)
                                           let sum =
                                               this.Results.Where(
                                                   r => r.ParticipantId == id && r.Place <= partipant.PlaceFrom)
                                               .Sum(s => s.Place)
                                           select
                                               new SkatingResult { Participant = partipant, Places = place, Sums = sum })
                .OrderByDescending(o => o.Places).ThenBy(o => o.Sums).ToList();
            if (results[0].Places > results[1].Places || results[0].Sums < results[1].Sums)
            {
                // OK, found place, now we set the place for the participant
                results[0].Participant.PlaceTo = results[0].Participant.PlaceFrom;
                for (var i = 1; i < results.Count; i++)
                {
                    results[i].Participant.PlaceFrom = results[0].Participant.PlaceTo + 1;
                }
                this.context.SaveChanges();
                // If it was more then two couples, we redo or skating:
                if (results.Count > 2)
                {
                    results.RemoveAt(0);
                    var ids = results.Select(p => p.Participant.Id).ToList();
                    this.DoSkating(ids);
                }
            }
            // When here, Rule 10 was not enough, now use rule 11:
            // Basicaly now we do majority over all dances and judges
        }

        private void Initialize()
        {
            this.competition = this.context.Competitions.Single(c => c.Id == this.competitionId);
            this.round = this.context.Rounds.Single(r => r.Id == this.roundId);

            this.NumJudges = this.competition.Officials.Count(o => o.Role.Id == Roles.Judge);
            this.Judges =
                this.competition.Officials.Where(o => o.Role.Id == Roles.Judge).Select(o => o.Official).ToList();
            this.NumCouples = this.round.Qualifieds.Count;
            this.Dances = this.round.DancesInRound.ToList();
            this.Participants = this.round.Qualifieds.Select(p => p.Participant).OrderBy(p => p.Number).ToList();
            this.Markings = new List<Marking>();

            // create an entry for every couple, dance and judge ...

            foreach (var dance in this.competition.Dances)
            {
                foreach (var judge in this.competition.GetJudges())
                {
                    foreach (var qualified in this.round.Qualifieds)
                    {
                        var marking =
                            this.context.Markings.SingleOrDefault(
                                m =>
                                m.Dance.Id == dance.Dance.Id && m.Round.Id == this.round.Id && m.Judge.Id == judge.Id
                                && m.Participant.Id == qualified.Participant.Id);
                        if (marking == null)
                        {
                            marking = new Marking(qualified.Participant, this.round, dance.Dance, judge)
                                          {
                                              Created = DateTime.Now,
                                              Mark = 0,
                                          };
                            this.context.Markings.Add(marking);
                        }
                        this.Markings.Add(marking);
                    }
                }
            }
            this.context.SaveChanges();

            var qualifiedList = this.round.Qualifieds.ToList();
            this.Results = new List<DanceResult>();
            this.ResultCouple = new List<ResultCouple>();
            foreach (var dance in this.round.DancesInRound)
            {
                for (var i = 0; i < qualifiedList.Count; i++)
                {
                    var danceResult = new DanceResult
                                          {
                                              DanceId = dance.Dance.Id,
                                              ParticipantId = qualifiedList[i].Participant.Id,
                                              Place = 0
                                          };

                    this.Results.Add(danceResult);

                    for (var place = 1; place <= qualifiedList.Count; place++)
                    {
                        var res = new ResultCouple
                                      {
                                          DanceId = dance.Dance.Id,
                                          ParticipantId = qualifiedList[i].Participant.Id,
                                          Place = place,
                                          CountPlaces = 0,
                                          SumPlaces = 0,
                                      };
                        this.ResultCouple.Add(res);
                    }
                }

                if (!this.Markings.Any(m => m.Mark == 0 && m.Dance.Id == dance.Dance.Id))
                {
                    this.CalculatePlaces(dance.Dance.Id);
                }
            }
        }

        private int SetPlaces(
            int[] participantsIds,
            int danceId,
            int majority,
            int maxPlace,
            int currentPlace,
            int searchFrom)
        {
            if (searchFrom > maxPlace)
            {
                //we can not search anymore. So we give all couples i the list a taied place
                if (participantsIds == null)
                {
                    // Ids was null, so obviously all couples need to get a place:
                    participantsIds = this.ResultCouple.Select(c => c.ParticipantId).ToArray();
                }
                var placetoSet = (double)participantsIds.Count() / 2 + currentPlace - .5;
                foreach (var participantsId in participantsIds)
                {
                    var result =
                        this.Results.Single(r => r.DanceId == danceId && r.ParticipantId == participantsId);
                    result.Place = placetoSet;
                    this.MarkAsSet(danceId, participantsId);
                }
                return currentPlace + participantsIds.Count();
            }

            var searchPlace = searchFrom;
            var elements = participantsIds != null
                                                     ? this.ResultCouple.Where(
                                                         c => participantsIds.Contains(c.ParticipantId) && !c.IsSet)
                                                     : this.ResultCouple.Where(c => !c.IsSet);

            var results =
                elements.Where(r => r.DanceId == danceId && r.Place == searchPlace && r.CountPlaces >= majority)
                    .ToList();

            if (results.Count == 1)
            {
                var result =
                    this.Results.Single(r => r.DanceId == danceId && r.ParticipantId == results[0].ParticipantId);
                result.Place = currentPlace;
                this.MarkAsSet(danceId, results[0].ParticipantId);
                results[0].IsSet = true;
                results[0].IsUsed = true;
                return currentPlace + 1;
            }

            if (results.Count == 0)
            {
                // Search again, with next place
                return this.SetPlaces(participantsIds, danceId, majority, maxPlace, currentPlace, searchFrom + 1);
            }

            if (results.Count > 1)
            {
                // Case 1: one couple with lower sum -> get's the place
                var sorted = results.OrderBy(s => s.SumPlaces).ToList();
                var listSamePlace = new List<int>();
                if (sorted[0].SumPlaces == sorted[1].SumPlaces)
                {
                    listSamePlace.Add(sorted[0].ParticipantId);
                }
                else
                {
                    var result =
                        this.Results.Single(r => r.DanceId == danceId && r.ParticipantId == sorted[0].ParticipantId);
                    result.Place = currentPlace;
                    this.MarkAsSet(danceId, sorted[0].ParticipantId);
                    sorted[0].IsSet = true;
                    sorted[0].IsUsed = true;
                    currentPlace++;
                }
                // Loop through all couples in this list 
                for (var i = 1; i < results.Count; i++)
                {
                    if (sorted[i].SumPlaces == sorted[i - 1].SumPlaces)
                    {
                        // Sum equal, so add them to the list for further processing
                        listSamePlace.Add(sorted[i].ParticipantId);
                    }
                    else
                    {
                        // Process the list of same places if it contains elements
                        if (listSamePlace.Count > 0)
                        {
                            currentPlace = this.SetPlaces(
                                listSamePlace.ToArray(),
                                danceId,
                                majority,
                                maxPlace,
                                currentPlace,
                                searchFrom + 1);
                            listSamePlace.Clear();
                        }
                        // Now set the remaining place
                        var result =
                            this.Results.Single(r => r.DanceId == danceId && r.ParticipantId == sorted[i].ParticipantId);
                        result.Place = currentPlace;
                        this.MarkAsSet(danceId, sorted[i].ParticipantId);
                        sorted[i].IsSet = true;
                        sorted[i].IsUsed = true;
                        currentPlace++;
                    }
                }
                if (listSamePlace.Count > 0)
                {
                    currentPlace = this.SetPlaces(
                        listSamePlace.ToArray(),
                        danceId,
                        majority,
                        maxPlace,
                        currentPlace,
                        searchFrom + 1);
                    listSamePlace.Clear();
                }

                return currentPlace;
            }

            // We still have no place, so let's continue:
            var Ids = results.Select(r => r.ParticipantId).ToArray();

            return this.SetPlaces(Ids, danceId, majority, maxPlace, currentPlace, searchFrom + 1);
        }

        #endregion
    }
}