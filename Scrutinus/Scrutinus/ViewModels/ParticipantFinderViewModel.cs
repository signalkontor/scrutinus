﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using CsQuery.ExtensionMethods;
using DataModel;
using DataModel.Models;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Scrutinus.Localization;

namespace Scrutinus.ViewModels
{
    public class ParticipantFinderViewModel : ViewModelBase
    {
        private readonly ScrutinusContext context;

        private List<Participant> allParticipants;

        private string filterString;

        public ParticipantFinderViewModel()
        {
            this.context = new ScrutinusContext();

            this.Participants = new ObservableCollection<Participant>(this.context.Participants);

            this.allParticipants = this.Participants.ToList();

            this.ChangeStateCommand = new RelayCommand<string>(this.ChangeCoupleState);
        }

        public ICommand ChangeStateCommand { get; set; }

        public Participant SelectedParticipant { get; set; }

        private DispatcherTimer timer;

        public string FilterString
        {
            get { return filterString; }
            set
            {
                this.filterString = value;

                if (this.timer != null)
                {
                    this.timer.Stop();
                    this.timer.IsEnabled = false;
                    this.timer = null;
                }

                this.timer = new DispatcherTimer();
                timer.Interval = new TimeSpan(0,0,0,0,300);
                timer.Tick += this.DoSearch;
                timer.IsEnabled = true;
                timer.Start();
            }
        }



        public string CoupleStateString
        {
            get
            {
                if (this.Participants == null)
                {
                    return "";
                }

                return string.Format(
                    LocalizationService.Resolve(() => Text.NumberCouples),
                    this.Participants.Count,
                    this.Participants.Count(p => p.State == CoupleState.Dancing));
            }
        }

        public ObservableCollection<Participant> Participants { get; set; }

        private void ChangeCoupleState(object parameter)
        {
            // no participant -> exit
            if (this.SelectedParticipant == null)
            {
                return;
            }

            if (!this.SelectedParticipant.Competition.IsCompetitionNotStarted)
            {
                return;
            }

            var state = parameter as string;

            switch (state)
            {
                case "Dancing":
                    this.SelectedParticipant.State = CoupleState.Dancing;
                    break;
                case "Excused":
                    this.SelectedParticipant.State = CoupleState.Excused;
                    break;
                case "Missing":
                    this.SelectedParticipant.State = CoupleState.Missing;
                    break;
            }

            this.context.SaveChanges();

            this.RaisePropertyChanged(() => this.CoupleStateString);
        }

        private void DoSearch(object sender, EventArgs e)
        {
            this.timer.Stop();
            this.timer.IsEnabled = false;
            this.timer = null;

            this.Participants.Clear();

            this.allParticipants.Where(p =>
                (p.Couple.FirstMan.ToLower() + p.Couple.LastMan.ToLower() + p.Couple.FirstWoman.ToLower() +
                 p.Couple.LastWoman.ToLower() + p.Couple.Country.ToLower()).Contains(this.filterString.ToLower())).ForEach(
                (p) =>
                {
                    this.Participants.Add(p);
                }
            );
        }
    }
}
