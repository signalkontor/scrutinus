﻿// // TPS.net TPS8 Scrutinus
// // ComboBoxItemSources.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using DataModel;
using Scrutinus.Localization;

namespace Scrutinus.ViewModels
{
    public class StringItemValue
    {
        #region Constructors and Destructors

        public StringItemValue(string value, string text)
        {
            this.Value = value;
            this.Display = text;
        }

        #endregion

        #region Public Properties

        public string Display { get; set; }

        public string Value { get; set; }

        #endregion
    }

    public class ItemValue
    {
        #region Constructors and Destructors

        public ItemValue(int value, string text)
        {
            this.Value = value;
            this.Display = text;
        }

        #endregion

        #region Public Properties

        public string Display { get; set; }

        public int Value { get; set; }

        #endregion
    }

    public class MarkingTypeItemValue
    {
        #region Constructors and Destructors

        public MarkingTypeItemValue(MarkingTypes value, string text)
        {
            this.Value = value;
            this.Display = text;
        }

        #endregion

        #region Public Properties

        public string Display { get; set; }

        public MarkingTypes Value { get; set; }

        #endregion
    }

    public class ComboBoxItemSources
    {
        #region Public Methods and Operators

        public static IEnumerable<ItemValue> GetFirstRoundType()
        {
            return new List<ItemValue>
                       {
                           new ItemValue(
                               RoundTypes.QualificationRound,
                               LocalizationService.Resolve(() => Text.OneFirstRound)),
                           new ItemValue(
                               RoundTypes.Redance,
                               LocalizationService.Resolve(() => Text.FirstRoundRedance)),
                           new ItemValue(
                               RoundTypes.SecondFirstRound,
                               LocalizationService.Resolve(() => Text.TwoFirstRounds)),
                       };
        }

        public static IEnumerable<StringItemValue> GetLocales()
        {
            return new List<StringItemValue>
                       {
                           new StringItemValue("en", "English"),
                           new StringItemValue("de", "Deutsch")
                       };
        }

        public static IEnumerable<MarkingTypeItemValue> GetMarkingTypeList()
        {
            return new List<MarkingTypeItemValue>
                       {
                           new MarkingTypeItemValue(
                               MarkingTypes.Marking,
                               LocalizationService.Resolve(() => Text.Marks)),
                           new MarkingTypeItemValue(
                               MarkingTypes.MarksSumOnly,
                               LocalizationService.Resolve(() => Text.MarksSumOnly)),
                           new MarkingTypeItemValue(
                               MarkingTypes.NewJudgingSystemV1,
                               LocalizationService.Resolve(() => Text.PointsJudgingSystem1)),
                           new MarkingTypeItemValue(
                               MarkingTypes.NewJudgingSystemV2,
                               LocalizationService.Resolve(() => Text.PointsJudgingSystem2)),
                           new MarkingTypeItemValue(
                               MarkingTypes.Skating,
                               LocalizationService.Resolve(() => Text.Skating)),
                           new MarkingTypeItemValue(
                               MarkingTypes.TeamMatch,
                               LocalizationService.Resolve(() => Text.TeamMatch)),
                           new MarkingTypeItemValue(
                               MarkingTypes.PointsGeneral,
                               LocalizationService.Resolve(() => Text.PointsGeneral))
                       };
        }

        public static List<ItemValue> GetNextRoundTypes()
        {
            return new List<ItemValue>
                       {
                           new ItemValue(
                               RoundTypes.QualificationRound,
                               LocalizationService.Resolve(() => Text.QualificationOrFinalRound)),
                           new ItemValue(
                               RoundTypes.Redance,
                               LocalizationService.Resolve(() => Text.Redance)),
                           new ItemValue(
                               RoundTypes.ABFinal,
                               LocalizationService.Resolve(() => Text.ABFinal)),
                           new ItemValue(
                               RoundTypes.ABCFinal,
                               LocalizationService.Resolve(() => Text.ABCFinal)),
                           new ItemValue(
                               RoundTypes.SeparationRound,
                               LocalizationService.Resolve(() => Text.Stichrunde))
                       };
        }

        public static IEnumerable<ItemValue> GetStarList(int maxStars)
        {
            var list = new List<ItemValue> { new ItemValue(0, "-"), };

            if (maxStars > 0)
            {
                list.Add(new ItemValue(1, "*"));
            }
            if (maxStars > 1)
            {
                list.Add(new ItemValue(2, "**"));
            }
            return list;
        }

        public static IEnumerable<ItemValue> GetStarTypeList()
        {
            return new List<ItemValue>
                       {
                           new ItemValue(0, LocalizationService.Resolve(() => Text.NoStars)),
                           new ItemValue(1, LocalizationService.Resolve(() => Text.MaxOneStar)),
                           new ItemValue(2, LocalizationService.Resolve(() => Text.MaxTwoStar))
                       };
        }

        #endregion
    }
}