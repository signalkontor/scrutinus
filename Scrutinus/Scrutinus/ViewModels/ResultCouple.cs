// // TPS.net TPS8 Scrutinus
// // ResultCouple.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System;
using System.ComponentModel;

namespace Scrutinus.ViewModels
{
    public class ResultCouple : INotifyPropertyChanged
    {
        #region Public Events

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Public Methods and Operators

        public bool OnSamePlace(ResultCouple other)
        {
            return this.CountPlaces == other.CountPlaces && this.SumPlaces == other.SumPlaces;
        }

        #endregion

        #region Methods

        protected void RaiseEvent(string name)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        #endregion

        #region Fields

        private int _countPlaces;

        private bool _isUsed;

        private int _sumPlaces;

        #endregion

        #region Public Properties

        public int CountPlaces
        {
            get
            {
                return this._countPlaces;
            }
            set
            {
                this._countPlaces = value;
                this.RaiseEvent("CountPlaces");
                this.RaiseEvent("LabelValue");
            }
        }

        public int DanceId { get; set; }

        public bool IsSet { get; set; }

        public bool IsUsed
        {
            get
            {
                return this._isUsed;
            }
            set
            {
                this._isUsed = value;
                this.RaiseEvent("LabelValue");
                this.RaiseEvent("IsUsed");
            }
        }

        public string LabelValue
        {
            get
            {
                return this._countPlaces > 0 && this._isUsed
                           ? String.Format("{0} ({1})", this._countPlaces, this._sumPlaces)
                           : " - ";
            }
        }

        public int ParticipantId { get; set; }

        public int Place { get; set; }

        public int SumPlaces
        {
            get
            {
                return this._sumPlaces;
            }
            set
            {
                this._sumPlaces = value;
                this.RaiseEvent("SumPlaces");
                this.RaiseEvent("LabelValue");
            }
        }

        #endregion
    }
}