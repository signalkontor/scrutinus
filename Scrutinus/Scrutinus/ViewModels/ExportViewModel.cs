﻿// // TPS.net TPS8 Scrutinus
// // ExportViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;
using DataModel;
using DataModel.Models;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Scrutinus.Export;
using Scrutinus.Localization;

namespace Scrutinus.ViewModels
{
    public class ExportItem : ViewModelBase
    {
        #region Fields

        private bool isSelected;

        #endregion

        #region Public Properties

        public IDataExporter Exporter { get; set; }

        public bool IsSelected
        {
            get
            {
                return this.isSelected;
            }
            set
            {
                this.isSelected = value;
                this.RaisePropertyChanged(() => this.IsSelected);
            }
        }

        #endregion
    }

    public class SelectedCompetitionViewModel : ViewModelBase
    {
        #region Fields

        private bool isSelected;

        #endregion

        #region Public Properties

        public Competition Competition { get; set; }

        public bool IsSelected
        {
            get
            {
                return this.isSelected;
            }
            set
            {
                this.isSelected = value;
                this.RaisePropertyChanged(() => this.IsSelected);
            }
        }

        public string TitleWithState
        {
            get
            {
                var state = this.Competition.State == CompetitionState.Finished ? $"({LocalizationService.Resolve(() => Text.CompetitionState_Finished)})" : this.Competition.IsCompetitionNotStarted ? $"({LocalizationService.Resolve(() => Text.CompetitionState_NotStarted)})" : $"({LocalizationService.Resolve(() => Text.CompetitionState_InProgress)})";
                return this.Competition.Title + " " + state;
            }
        }

        #endregion
    }

    public class ExportViewModel : BaseViewModel
    {
        #region Constructors and Destructors

        public ExportViewModel()
        {
            this.ExporterItems = new ObservableCollection<ExportItem>();

            var type = typeof(IDataExporter);

            var types =
                AppDomain.CurrentDomain.GetAssemblies()
                    .Where(a => a.FullName != "Exceptionless, Version=4.2.1982.0, Culture=neutral, PublicKeyToken=null")
                    .SelectMany(s =>
                    {
                        try
                        {
                            return s.GetTypes();
                        }
                        catch (Exception ex)
                        {
                            return new Type[0];
                        }
                    })
                    .Where(p => type.IsAssignableFrom(p) && !p.IsInterface);

            foreach (var exporterType in types)
            {
                try
                {
                    this.ExporterItems.Add(
                        new ExportItem
                        {
                            Exporter = (IDataExporter) Activator.CreateInstance(exporterType),
                            IsSelected = false,
                        });
                }
                catch (Exception ex)
                {
                    ScrutinusContext.WriteLogEntry(LogLevel.Error, "Error loading assembly for type " + exporterType.FullName + ". " + ex.Message);
                }
            }

            foreach (var exporterItem in this.ExporterItems)
            {
                exporterItem.PropertyChanged +=
                    (sender, args) => this.RaisePropertyChanged(() => this.AllReportsSelected);
            }

            // get all competitions:
            this.dataContext = new ScrutinusContext();
            this.CompetitionsView = new ObservableCollection<SelectedCompetitionViewModel>();

            foreach (var competition in this.dataContext.Competitions)
            {
                this.CompetitionsView.Add(
                    new SelectedCompetitionViewModel { Competition = competition, IsSelected = false });
            }

            foreach (var selectedCompetitionViewModel in this.CompetitionsView)
            {
                selectedCompetitionViewModel.PropertyChanged +=
                    (sender, args) => this.RaisePropertyChanged(() => this.AllCompetitionSelected);
            }

            this.exportDirectory = Settings.Default.ExportDirectory;
            this.ExportCommand = new RelayCommand(this.Export);
            this.SelectDirectoryCommand = new RelayCommand(this.SelectDirectory);
        }

        #endregion

        #region Public Methods and Operators

        public override bool Validate()
        {
            return true;
        }

        #endregion

        #region Fields

        private readonly ScrutinusContext dataContext;

        private string exportDirectory;

        #endregion

        #region Public Properties

        public bool AllCompetitionSelected
        {
            get
            {
                return this.CompetitionsView.All(c => c.IsSelected);
            }
            set
            {
                foreach (var selectedCompetitionViewModel in this.CompetitionsView)
                {
                    selectedCompetitionViewModel.IsSelected = value;
                }

                this.RaisePropertyChanged(() => this.AllCompetitionSelected);
            }
        }

        public bool AllReportsSelected
        {
            get
            {
                return this.ExporterItems.All(e => e.IsSelected);
            }
            set
            {
                foreach (var exporter in this.ExporterItems)
                {
                    exporter.IsSelected = value;
                }

                this.RaisePropertyChanged(() => this.AllReportsSelected);
            }
        }

        public ObservableCollection<SelectedCompetitionViewModel> CompetitionsView { get; set; }

        public ICommand ExportCommand { get; set; }

        public string ExportDirectory
        {
            get
            {
                return this.exportDirectory;
            }
            set
            {
                this.exportDirectory = value;
                Settings.Default.ExportDirectory = value;
                Settings.Default.Save();
                this.RaisePropertyChanged(() => this.ExportDirectory);
            }
        }

        public ObservableCollection<ExportItem> ExporterItems { get; private set; }

        public ICommand SelectDirectoryCommand { get; set; }

        #endregion

        #region Methods

        private void Export()
        {
            foreach (var exporterItem in this.ExporterItems.Where(e => e.IsSelected))
            {
                Task.Factory.StartNew(
                    () =>
                        {
                            try
                            {
                                if (!Directory.Exists(this.ExportDirectory))
                                {
                                    Directory.CreateDirectory(this.ExportDirectory);
                                }

                                exporterItem.Exporter.ExportCompetitions(
                                    this.CompetitionsView.Where(c => c.IsSelected).Select(c => c.Competition).ToList(),
                                    this.ExportDirectory,
                                    this.ReportProgress);
                            }
                            catch (Exception exception)
                            {
                                var message = string.Format(
                                    "{0} {1}: {2}",
                                    LocalizationService.Resolve(() => Text.ErrorDuringExport),
                                    exporterItem.Exporter.Name,
                                    exception.Message);

                                MainWindow.MainWindowHandle.ShowMessage(message);
                            }
                        });
            }
        }

        private void ReportProgress(int progress, string message)
        {
            MainWindow.MainWindowHandle.ReportStatus(message, progress, 100);
        }

        private void SelectDirectory()
        {
            var dialog = new FolderBrowserDialog();
            dialog.ShowNewFolderButton = true;
            var result = dialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                this.ExportDirectory = dialog.SelectedPath;
            }
        }

        #endregion
    }
}