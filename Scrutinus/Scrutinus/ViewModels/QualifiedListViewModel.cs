﻿// // TPS.net TPS8 Scrutinus
// // QualifiedListViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Linq;
using DataModel.Models;
using General.Extensions;
using Scrutinus.Localization;

namespace Scrutinus.ViewModels
{
    public class QualifiedListViewModel : BaseViewModel
    {
        #region Fields

        private readonly Round round;

        #endregion

        #region Constructors and Destructors

        public QualifiedListViewModel(ScrutinusContext context, int roundId)
        {
            this.context = context;
            this.round = context.Rounds.SingleOrDefault(r => r.Id == roundId);
            this.QualifiedCouples = this.round.Qualifieds.ToList();
            this.SetRoundTitle();
        }

        #endregion

        #region Public Methods and Operators

        public override bool Validate()
        {
            return true;
        }

        #endregion

        #region Methods

        private void SetRoundTitle()
        {
            var title = String.Format(
                LocalizationService.Resolve(() => Text.LabelQualifiedCouples),
                this.round.PrintTitle(),
                this.round.Qualifieds.Count);

            this.RoundTitle = title;
        }

        #endregion

        #region Public Properties

        public List<Qualified> QualifiedCouples { get; set; }

        public string NumberQualifiedAsString
        {
            get
            {
                return string.Format(LocalizationService.Resolve(() => Text.NumberQualifiedCouples), this.QualifiedCouples.Count);
            }
        }

        public string RoundTitle { get; set; }

        #endregion
    }
}