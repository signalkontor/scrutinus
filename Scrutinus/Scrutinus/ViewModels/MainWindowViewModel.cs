﻿// // TPS.net TPS8 Scrutinus
// // MainWindowViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using DataModel;
using DataModel.Models;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MahApps.Metro.Controls.Dialogs;
using NancyServer;
using Scrutinus.Pages;

namespace Scrutinus.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        #region Constructors and Destructors

        public MainWindowViewModel()
        {
            this.currentFilterPredicate = e => true;

            this.competitionElements = new ObservableCollection<CompetitionElement>();

            this.Competitions = new ObservableCollection<CompetitionElement>();
            
            this.ApplyFilter();

            this.ToggleFlyOutCommand = new RelayCommand(this.ToggleFlyOutState);

            this.RefreshFlyOutCommand = new RelayCommand(this.RefreshFlyOut);

            this.Devices = new ObservableCollection<Device>();

            this.FilterNavigationListAll = true;

            this.dispatcher = Dispatcher.CurrentDispatcher;

            if (MainWindow.IsInNetworkMode)
            {
                this.backgroundUpdateThread = new Thread(this.UpdateNavigationListThread) { IsBackground = true };
                this.backgroundUpdateThread.Start();
            }

            this.ShowHamburgerMenuByState();
        }

        #endregion

        #region Fields

        private readonly ObservableCollection<CompetitionElement> competitionElements;

        private readonly Predicate<CompetitionElement> currentFilterPredicate;

        private Page activePage;

        private CompetitionElement currentCompetition;

        private string currentOpenDatabase;

        private bool filterNavigationListAll;

        private bool filterNavigationListToday;

        private bool filterNavigationListTomorrow;

        private bool flyOutIsOpen = true;

        private Page lastPage;

        private GridLength leftColumnLength;

        private bool markingInputEnabled;

        private string navigationListFilter;

        private bool nextStepEnabled;

        private readonly Dispatcher dispatcher;

        private CompetitionElement selectedCompetition;

        private readonly Thread backgroundUpdateThread;

        private Event currentEvent;

        private int hamburgerState = 2;

        #endregion

        #region Public Properties

        public Page ActivePage
        {
            get
            {
                return this.activePage;
            }
            set
            {
                if (this.activePage == value)
                {
                    return;
                }

                if (this.activePage is ScrutinusPage)
                {
                    var sp = (ScrutinusPage)this.activePage;

                    if (!sp.AllowToLeave())
                    {
                        return;
                    }

                    sp.IsPageVisible = false;
                }

                if (this.activePage is ProjectorControl disposable)
                {
                    disposable.Dispose();
                }

                if (value == null)
                {
                    value = new Page();
                }

                if (value is ScrutinusPage)
                {
                    var sp = (ScrutinusPage) value;
                    sp.IsPageVisible = true;
                }

                this.lastPage = this.activePage;
                this.activePage = value;
                this.RaisePropertyChanged(() => this.LastPage);
                this.RaisePropertyChanged(() => this.ActivePage);
            }
        }

        public CompetitionElement Competition
        {
            get
            {
                return this.currentCompetition;
            }
            set
            {
                this.currentCompetition = value;
                this.RaisePropertyChanged(() => this.Competition);
            }
        }

        public ObservableCollection<CompetitionElement> Competitions { get; set; }

        public ObservableCollection<Device> Devices { get; set; }

        public bool HamburgerMenuPaneOpen { get; set; }

        public CompetitionElement CurrentCompetition
        {
            get
            {
                return this.currentCompetition;
            }
            set
            {
                if (this.currentCompetition == value)
                {
                    return;
                }

                this.currentCompetition = value;
                this.RaisePropertyChanged(() => this.CurrentCompetition);
            }
        }

        public string CurrentOpenDatabase
        {
            get
            {
                return this.currentOpenDatabase;
            }
            set
            {
                this.currentOpenDatabase = value;
                this.RaisePropertyChanged(() => this.CurrentOpenDatabase);
            }
        }

        public Event Event
        {
            get { return this.currentEvent; }
            set
            {
                this.currentEvent = value;
                this.RaisePropertyChanged(() => this.Event);
            }
        }

        public ICommand RefreshFlyOutCommand { get; set; }

        public bool FilterNavigationListAll
        {
            get
            {
                return this.filterNavigationListAll;
            }
            set
            {
                this.filterNavigationListAll = value;
                this.RaisePropertyChanged(() => this.FilterNavigationListAll);
            }
        }

        public bool FilterNavigationListToday
        {
            get
            {
                return this.filterNavigationListToday;
            }
            set
            {
                this.filterNavigationListToday = value;
                this.RaisePropertyChanged(() => this.FilterNavigationListToday);
                this.ApplyFilter();
            }
        }

        public bool FilterNavigationListTomorrow
        {
            get
            {
                return this.filterNavigationListTomorrow;
            }
            set
            {
                this.filterNavigationListTomorrow = value;
                this.RaisePropertyChanged(() => this.FilterNavigationListTomorrow);
                this.ApplyFilter();
            }
        }

        public bool DoNotUpdateNavigationList { get; set; }

        public bool FlyoutIsOpen
        {
            get
            {
                return this.flyOutIsOpen;
            }
            set
            {
                this.flyOutIsOpen = value;
                if (value == true)
                {
                    this.UpdateNavigationList();
                }
                this.RaisePropertyChanged(() => this.FlyoutIsOpen);
            }
        }

        public Page LastPage
        {
            get
            {
                return this.lastPage;
            }
        }

        public Visibility ShortLabelVisible
        {
            get { return this.hamburgerState < 2 ? Visibility.Visible : Visibility.Collapsed; }
        }

        public Visibility LongLabelVisible
        {
            get { return this.hamburgerState == 2 ? Visibility.Visible : Visibility.Collapsed; }
        }

        public bool MarkingInputEnabled
        {
            get
            {
                return this.markingInputEnabled;
            }
            set
            {
                this.markingInputEnabled = value;
                this.RaisePropertyChanged(() => this.MarkingInputEnabled);
            }
        }

        public HttpServer NancyServer { get; set; }

        public bool DoNotRaiseSelectionChangedEvent { get; set; }

        public string NavigationListFilter
        {
            get
            {
                return this.navigationListFilter;
            }
            set
            {
                this.navigationListFilter = value;
                this.RaisePropertyChanged(() => this.NavigationListFilter);

                this.ApplyFilter();
            }
        }

        public bool NextStep_Enabled
        {
            get
            {
                return this.nextStepEnabled;
            }
            set
            {
                this.nextStepEnabled = value;
                this.RaisePropertyChanged(() => this.NextStep_Enabled);
            }
        }

        public CompetitionElement SelectedCompetition
        {
            get
            {
                return this.selectedCompetition;
            }
            set
            {
                this.selectedCompetition = value;
                this.RaisePropertyChanged(() => this.SelectedCompetition);
            }
        }

        /// <summary>
        /// Gets or sets the toggle fly out command.
        /// </summary>
        /// <value>
        /// The toggle fly out command.
        /// </value>
        public ICommand ToggleFlyOutCommand { get; set; }

        #endregion

        #region Public Methods and Operators

        public void AddCompetition(CompetitionElement element)
        {
            this.competitionElements.Add(element);
            this.ApplyFilter();
        }

        public void UpdateCoupleStatistic(int competitionId, int total, int dancing)
        {
            var competitionElement = this.Competitions.FirstOrDefault(c => c.Competition.Id == competitionId);

            if (competitionElement != null)
            {
                competitionElement.Competition.NumberCouplesRegistered = total;
                competitionElement.Competition.NumberParticipantsDancing = dancing;
            }
        }

        public void CheckForUpdates()
        {
            Task.Factory.StartNew(
                () =>
                    {
                        try
                        {
                            Thread.Sleep(3000);

                            var webClient = new WebClient();
                            var version = webClient.DownloadString("http://tps-ejudge.de/version/current.txt");

                            // the string must start with 'Version'
                            // if not it might be some wrong answer
                            if (!version.StartsWith("Version"))
                            {
                                return;
                            }

                            var data = version.Split(';');
                            if (data[0] != this.GetProgrammVersion())
                            {
                                if (Thread.CurrentThread.CurrentUICulture.TwoLetterISOLanguageName == "de")
                                {
                                    this.dispatcher.Invoke(
                                        new Action(
                                            () =>
                                                {
                                                    MainWindow.MainWindowHandle.ShowMessageAsync(
                                                        "Neue Programmversion verfügbar: " + data[0],
                                                        "Änderungen in dieser Version: " + data[1],
                                                        MessageDialogStyle.Affirmative);
                                                }));
                                    
                                }
                                else
                                {
                                    this.dispatcher.Invoke(
                                        new Action(
                                            () =>
                                            {
                                                MainWindow.MainWindowHandle.ShowMessageAsync(
                                                    "New Programversion available: " + data[0],
                                                    "Updates in this version: " + data[2],
                                                    MessageDialogStyle.Affirmative);
                                            }));
                                }
                            }
                        }
                        catch (Exception)
                        {
                            // Don't do anything ...
                        }
                    });
        }

        public void ClearCompetitions()
        {
            this.competitionElements.Clear();
        }

        public void InvalidateLastPage()
        {
            this.lastPage = null;
            this.RaisePropertyChanged(() => this.LastPage);
        }

        public void RaiseNewState()
        {
            this.RaisePropertyChanged(() => this.Competition);
        }

        #endregion

        #region Methods

        private void RefreshFlyOut()
        {
            this.UpdateNavigationList();
        }

        private void ApplyFilter()
        {
            IEnumerable<CompetitionElement> filteredCompetitions;

            if (string.IsNullOrWhiteSpace(this.navigationListFilter))
            {
                filteredCompetitions = this.competitionElements.Where(q => true).ToList();
            }
            else
            {
                filteredCompetitions =
                    this.competitionElements.Where(
                        e =>
                        e.Competition.Title.IndexOf(this.navigationListFilter, StringComparison.CurrentCulture) > -1);
            }

            if (this.filterNavigationListToday)
            {
                filteredCompetitions =
                    filteredCompetitions.Where(
                        c => c.Competition.StartTime.HasValue && c.Competition.StartTime.Value.Date == DateTime.Now.Date);
            }

            if (this.filterNavigationListTomorrow)
            {
                filteredCompetitions =
                    filteredCompetitions.Where(
                        c =>
                        c.Competition.StartTime.HasValue
                        && c.Competition.StartTime.Value.Date == DateTime.Now.AddDays(1).Date);
            }

            var selected = this.SelectedCompetition;

            this.Competitions.Clear();

            // All all running competitions:
            foreach (var competitionElement in filteredCompetitions.Where(c => !c.Competition.IsCompetitionNotStarted && c.Competition.State != CompetitionState.Finished).OrderBy(c => c.Competition.StartTime))
            {
                this.Competitions.Add(competitionElement);
            }

            foreach (var competitionElement in filteredCompetitions.Where(c => c.Competition.IsCompetitionNotStarted && !c.Competition.Canceled).OrderBy(c => c.Competition.StartTime))
            {
                this.Competitions.Add(competitionElement);
            }

            foreach (var competitionElement in filteredCompetitions.Where(c => c.Competition.State == CompetitionState.Finished).OrderBy(c => c.Competition.StartTime))
            {
                this.Competitions.Add(competitionElement);
            }

            if (selected != null)
            {
                this.DoNotRaiseSelectionChangedEvent = true;
                this.SelectedCompetition = selected;
                this.DoNotRaiseSelectionChangedEvent = false;
            }
        }

        private string GetProgrammVersion()
        {
            var attributes =
                Assembly.GetExecutingAssembly()
                    .GetCustomAttributes(false)
                    .Where(t => t is AssemblyFileVersionAttribute)
                    .Cast<AssemblyFileVersionAttribute>();
            if (attributes.Any())
            {
                return "Version " + attributes.First().Version;
            }

            return "";
        }

        private void ToggleFlyOutState()
        {
            this.hamburgerState++;
            if (this.hamburgerState > 2)
            {
                this.hamburgerState = 0;
            }

            this.ShowHamburgerMenuByState();
        }

        private void ShowHamburgerMenuByState()
        {
            switch (this.hamburgerState)
            {
                case 0:
                    MainWindow.MainWindowHandle.NavigationListControl.Width = 0;
                    break;
                case 1:
                    MainWindow.MainWindowHandle.NavigationListControl.Width = 120;
                    break;
                case 2:
                    MainWindow.MainWindowHandle.NavigationListControl.Width = double.NaN;
                    break;
            }

            this.RaisePropertyChanged(() => this.LongLabelVisible);
            this.RaisePropertyChanged(() => this.ShortLabelVisible);
        }


        private void UpdateNavigationListThread()
        {
            while (true)
            {
                try
                {
                    Thread.Sleep(5000);
                    var stopwatch = new Stopwatch();
                    stopwatch.Start();

                    if (this.DoNotUpdateNavigationList)
                    {
                        continue;
                    }

                    this.UpdateNavigationList();

                    this.dispatcher.Invoke(new Action(this.ApplyFilter));
                    Debug.WriteLine("Update Runtime: " + stopwatch.ElapsedMilliseconds);
                    stopwatch.Stop();
                }
                catch (ThreadAbortException)
                {
                    return;
                }
                catch (Exception)
                {
                    Thread.Sleep(5000);
                }
            }
        }

        public void UpdateNavigationList()
        {
            Task.Factory.StartNew(new Action(() =>
            {
                this.dispatcher.Invoke(new Action(this.UpdateNavigationListInternal), DispatcherPriority.ApplicationIdle);
            }));
        }

        private void UpdateNavigationListInternal()
        {
            using (var dbContext = new ScrutinusContext())
            {
                try
                {
                    foreach (var competition in dbContext.Competitions)
                    {
                        var local = this.competitionElements.FirstOrDefault(c => c.Competition.Id == competition.Id);
                        if (local == null)
                        {
                            this.competitionElements.Add(new CompetitionElement()
                            {
                                Competition = competition,
                                CurrentRound = competition.CurrentRound
                            });
                        }
                    }

                    foreach (var competitionElement in this.competitionElements)
                    {
                        var dbCompetition =
                            dbContext.Competitions.FirstOrDefault(c => c.Id == competitionElement.Competition.Id);
                        if (dbCompetition == null)
                        {
                            continue;
                        }

                        competitionElement.Competition.NumberParticipantsDancing =
                            dbCompetition.NumberParticipantsDancing;
                        competitionElement.Competition.State = dbCompetition.State;
                        competitionElement.Competition.Title = dbCompetition.Title;
                        competitionElement.Competition.StartTime = dbCompetition.StartTime;
                        competitionElement.Competition.Canceled = dbCompetition.Canceled;
                    }

                    this.ApplyFilter();
                }
                catch (Exception ex)
                {
                    ScrutinusContext.WriteLogEntry(ex);
                }
            }
        }

        #endregion
    }
}