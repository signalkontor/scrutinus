// // TPS.net TPS8 Scrutinus
// // DefineHeatViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using DataModel;
using DataModel.Models;
using GalaSoft.MvvmLight.Command;
using MobileControlLib.Helper;
using Scrutinus.Dialogs;
using Scrutinus.Localization;

namespace Scrutinus.ViewModels
{
    /// <summary>
    ///     Interaction logic for DefineHeat.xaml
    /// </summary>
    public class DefineHeatViewModel : BaseViewModel, IDataErrorInfo
    {
        #region Public Indexers

        public string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "MarksFrom":
                        return this.ValidateMarksFrom();
                    case "MarksTo":
                        return this.ValidateMarksTo();
                    case "CouplesPerHeat":
                        return this.ValidateCouplesPerHeat();
                    case "MarkingType":
                        return this.Round.MarksNumberType == 0 ? "Marking Type cannot be empty" : string.Empty;
                }

                return String.Empty;
            }
        }

        #endregion

        #region Fields

        private string danceOrderText;

        private int numberOfHeats;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Only used to enable designtime support
        /// </summary>
        public DefineHeatViewModel()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="DefineHeatViewModel" /> class.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="currentRoundId">The current round identifier.</param>
        public DefineHeatViewModel(ScrutinusContext context, int currentRoundId)
        {
            this.context = context;
            this.Round = context.Rounds.Single(r => r.Id == currentRoundId);

            if (this.Round.DrawingType == null)
            {
                this.Round.DrawingType = DataModel.DrawingTypes.Random;
            }

            this.Js2SettingsVisible = Visibility.Collapsed;
            this.MarksVisible = Visibility.Visible;

            this.MarkingTypes = new ObservableCollection<MarkingTypeItemValue>();

            this.RoundTypes = this.context.RoundTypes.ToList();

            // Filter the round types that do not match:
            if (this.Round.Number == 1)
            {
                this.RoundTypes.RemoveAll(
                    t =>
                    t.Id == DataModel.RoundTypes.ABFinal || t.Id == DataModel.RoundTypes.ABCFinal
                    || t.Id == DataModel.RoundTypes.Redance);
            }

            if (this.Round.Competition.CompetitionType.Id == 4
                && this.Round.Competition.Event.RuleName == "DTV National")
            {
                this.RoundTypes.RemoveAll(
                    t =>
                    t.Id == DataModel.RoundTypes.ABCFinal
                    || t.Id == DataModel.RoundTypes.Redance || t.Id == DataModel.RoundTypes.SecondFirstRound);
            }

            if (this.Round.MarksInputType == DataModel.MarkingTypes.Undefinied)
            {
                this.Round.MarksInputType = this.Round.RoundType.IsFinal
                                                ? this.Round.Competition.FinalType
                                                : this.Round.Competition.MarkingType;
            }

            this.DrawingTypes = this.context.DrawingTypes.ToList();

            this.DeleteHeatsCommand = new RelayCommand(this.DeleteRoundDrawing);

            if (this.Round.MarksNumberType == MarkNumberType.Undefined)
            {
                this.Round.MarksNumberType = MarkNumberType.Manual;
                this.Round.MarksFrom = this.GetMarksFrom();
                this.Round.MarksTo = this.Round.MarksFrom;
            }

            this.DrawingExists = this.CheckDrawingExists();

            this.Round.MinimumPointsJs2 = this.Round.MinimumPointsJs2 == 0.0 ? 0.5 : this.Round.MinimumPointsJs2;
            this.Round.MaximumPointsJs2 = this.Round.MaximumPointsJs2 == 0.0 ? 10.0 : this.Round.MaximumPointsJs2;
            this.Round.Js3CutOffValue = this.Round.Js3CutOffValue == 0.0 ? 1.5 : this.Round.Js3CutOffValue;
            this.Round.Intervall = this.Round.Intervall == 0 ? 0.25 : this.Round.Intervall;

            var numberOfHeats = this.Round.CouplesPerHeat ?? 0;

            if (this.CheckIsSoloDances())
            {
                numberOfHeats = this.Round.Qualifieds.Count;
            }
            else
            {
                if (numberOfHeats == 0)
                {
                    if (this.Round.Competition.DefaultCouplesPerHeat == 0)
                    {
                        this.Round.Competition.DefaultCouplesPerHeat = 12;
                    }
                    // Test, if we find a better number:
                    numberOfHeats = this.Round.Qualifieds.Count / this.Round.Competition.DefaultCouplesPerHeat;
                    if (this.Round.Qualifieds.Count % this.Round.Competition.DefaultCouplesPerHeat > 0)
                    {
                        numberOfHeats++;
                    }
                }
            }

            this.NumberOfHeats = numberOfHeats;

            this.ChangeDanceOrderCommand = new RelayCommand(this.ChangeDanceOrder);
            this.SetDanceString();

            // Set the visibilities
            this.SetVisibilitiesFromMarksInputTpye();
            // To show the current marking-type:
            this.SetMarkingTypesFromRoundType();

            this.Round.PropertyChanged += this.Round_PropertyChanged;
        }

        private bool CheckIsSoloDances()
        {
            if (this.Round.Competition.CompetitionType.IsJMD || this.Round.Competition.CompetitionType.IsFormation)
            {
                return true;
            }

            return false;
        }

        #endregion

        #region Public Properties

        public string MarksWithCouples
        {
            get { return string.Format(Text.MarksHeaderWithCouplesNumber, this.Round.Qualifieds.Count); }
        }

        public ICommand ChangeDanceOrderCommand { get; set; }

        public string DanceOrderText
        {
            get
            {
                return this.danceOrderText;
            }
            set
            {
                this.danceOrderText = value;
                this.RaisePropertyChanged(() => this.DanceOrderText);
            }
        }

        public ICommand DeleteHeatsCommand { get; set; }

        public bool DrawingExists { get; set; }

        public Visibility DrawingTypeVisible { get; set; }

        public IEnumerable<DrawingType> DrawingTypes { get; set; }

        public string Error
        {
            get
            {
                if (this.ValidateMarksFrom() != String.Empty)
                {
                    return this.ValidateMarksFrom();
                }
                if (this.ValidateMarksTo() != String.Empty)
                {
                    return this.ValidateMarksTo();
                }
                if (this.ValidateCouplesPerHeat() != String.Empty)
                {
                    return this.ValidateCouplesPerHeat();
                }
                if (this.Round.MarksNumberType == 0 && this.MarksVisible == Visibility.Visible)
                {
                    return LocalizationService.Resolve(() => Text.NumberOfMarksNotSet);
                }

                if (this.Round.MarksInputType == 0)
                {
                    return LocalizationService.Resolve(() => Text.RoundMarkingTypeCannotBeEmpty);
                }

                if (this.Round.MarksInputType == DataModel.MarkingTypes.NewJudgingSystemV2 &&
                    (this.Round.Js3CutOffValue < 0.5 || this.Round.Js3CutOffValue > 5))
                {
                    return $"The Max-Deviation for JS3 is set to {Round.Js3CutOffValue}. This is to low / to high";
                }
                    
                return String.Empty;
            }
        }

        public Visibility Js2SettingsVisible { get; set; }

        public ObservableCollection<MarkingTypeItemValue> MarkingTypes { get; set; }

        public Visibility MarksManualVisibility
        {
            get
            {
                return this.Round.MarksNumberType == MarkNumberType.Manual ? Visibility.Visible : Visibility.Hidden;
            }
        }

        public string MarksString
        {
            get
            {
                if (this.Round == null)
                {
                    return "";
                }

                var message = this.Round.MarksFrom == this.Round.MarksTo
                                     ? string.Format(
                                         LocalizationService.Resolve(() => Text.MarksFrom),
                                         this.Round.MarksTo)
                                     : string.Format(
                                         LocalizationService.Resolve(() => Text.MarksFromTo),
                                         this.Round.MarksFrom,
                                         this.Round.MarksTo);

                return message;
            }
        }

        public Visibility MarksVisible { get; set; }

        public int NumberOfHeats
        {
            get
            {
                return this.numberOfHeats;
            }
            set
            {
                this.numberOfHeats = value;
                this.RaisePropertyChanged(() => this.NumberOfHeats);
                this.RaisePropertyChanged(() => this.Error);
            }
        }

        public Round Round { get; set; }

        public List<RoundType> RoundTypes { get; set; }

        public Visibility ShowRoundDrawingExists
        {
            get
            {
                return this.DrawingExists ? Visibility.Visible : Visibility.Hidden;
            }
        }

        #endregion

        #region Public Methods and Operators

        public override void SaveData()
        {
            this.SetMarks();
            this.Round.CouplesPerHeat = this.NumberOfHeats;

            this.context.SaveChanges();
            if (MainWindow.License.Ejudge && Settings.Default.UseInternalMobileControl)
            {
                this.Round.EjudgeEnabled = false;
                // Create the eJudge Device data:
                // we give in an emtpy list of devices to prevent it to setup Devices
                // from the wrong context.
                DeviceHelper.CreateEjudgeData(this.context, this.Round, new List<Device>());
            }

            this.context.SaveChanges();
        }

        public override bool Validate()
        {
            if (this.MarksVisible == Visibility.Visible)
            {
                
            }

            var valid = this.Error == String.Empty;
            if (!valid)
            {
                this.RaisePropertyChanged(() => this.Error);
            }

            return valid;
        }

        #endregion

        #region Methods

        private void ChangeDanceOrder()
        {
            // Open Dialog
            var dlg = new ChangeDanceOrder(this.Round, this.context);
            MainWindow.MainWindowHandle.ShowDialog(dlg, this.SetDanceString, 310, 430);
        }

        private bool CheckDrawingExists()
        {
            var drawingExists = false;
            if (this.context.Drawings.Any(r => r.Round.Id == this.Round.Id))
            {
                var firstDance = this.Round.DancesInRound.First();
                var numCouples =
                    this.context.Drawings.Count(r => r.Round.Id == this.Round.Id && r.DanceRound.Id == firstDance.Id);
                if (numCouples == this.Round.Qualifieds.Count)
                {
                    // OK, we have the same number of couples, but are they the same couples?
                    var couplesAreEqual = true;
                    foreach (var qualified in this.Round.Qualifieds)
                    {
                        if (
                            !this.context.Drawings.Any(
                                r => r.Round.Id == this.Round.Id && r.Participant.Id == qualified.Participant.Id))
                        {
                            // cant find this couple, so we need a new round drawing
                            couplesAreEqual = false;
                            break;
                        }
                    }
                    drawingExists = couplesAreEqual;
                    // We need a new round drawing, so we have to delete the old one:
                    if (!drawingExists)
                    {
                        foreach (
                            var drawing in this.context.Drawings.Where(r => r.Round.Id == this.Round.Id).ToList())
                        {
                            this.context.Drawings.Remove(drawing);
                        }
                    }
                    this.context.SaveChanges();
                }
            }

            return drawingExists;
        }

        private void DeleteRoundDrawing()
        {
            foreach (var drawing in this.context.Drawings.Where(r => r.Round.Id == this.Round.Id).ToList())
            {
                this.context.Drawings.Remove(drawing);
            }
            this.context.SaveChanges();

            // Delete it and then:
            this.DrawingExists = false;
            if (this.DoRaiseEvents)
            {
                this.RaisePropertyChanged(() => this.DrawingExists);
                this.RaisePropertyChanged(() => this.ShowRoundDrawingExists);
            }
        }

        private void Round_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "RoundType")
            {
                this.SetMarkingTypesFromRoundType();

                if (!this.Round.RoundType.HasDrawing)
                {
                    this.Round.DrawingType = null;
                }
                else
                {
                    if (this.Round.DrawingType == null)
                    {
                        // Default: Random:
                        this.Round.DrawingType = DataModel.DrawingTypes.Random;
                    }
                }
            }

            if (e.PropertyName == "MarksNumberType")
            {
                this.SetMarks();
                this.RaisePropertyChanged(() => this.MarksManualVisibility);
            }

            if (e.PropertyName == "MarksInputType")
            {
                this.SetVisibilitiesFromMarksInputTpye();
            }
            // We raise the changed notification for error
            this.RaisePropertyChanged(() => this.Error);
        }

        private void SetDanceString(object sender = null)
        {
            var str = "";

            foreach (var dance in this.Round.DancesInRound.OrderBy(o => o.SortOrder))
            {
                if (str.Length > 0)
                {
                    str += ", ";
                }
                str += dance.Dance.ShortName;
            }

            this.DanceOrderText = str;
        }

        private void SetMarkingTypesFromRoundType()
        {
            // we select only marking types that are allowed
            var list = new List<MarkingTypes>();
            var strings = this.Round.RoundType.AllowedMarkingTypes.Split(',');

            foreach (var s in strings)
            {
                list.Add((MarkingTypes)Int32.Parse(s));
            }

            // DTV and Open Competition:
            if (this.Round.Competition.Event.RuleName == "DTV National"
                && this.Round.Competition.CompetitionType.Id == 4)
            {
                list.RemoveAll(
                    m =>
                    m == DataModel.MarkingTypes.NewJudgingSystemV1 || m == DataModel.MarkingTypes.NewJudgingSystemV2
                    || m == DataModel.MarkingTypes.TeamMatch);
            }

            this.MarkingTypes.Clear();
            var markings = ComboBoxItemSources.GetMarkingTypeList();
            foreach (var itemValue in markings)
            {
                if (list.Contains(itemValue.Value))
                {
                    this.MarkingTypes.Add(itemValue);
                }
            }

            if (this.MarkingTypes.All(i => i.Value != this.Round.MarksInputType))
            {
                this.Round.MarksInputType = 0;
                // Try to find the value based on the competition marking type
                if (this.Round.Competition != null)
                {
                    if (this.Round.RoundType.IsFinal)
                    {
                        this.Round.MarksInputType = this.Round.Competition.FinalType;
                    }
                    else
                    {
                        this.Round.MarksInputType = (MarkingTypes)this.Round.Competition.FirstRoundType;
                    }
                }
            }
        }

        private void SetMarks()
        {
            // Set the number of Marks in this round:
            switch (this.Round.MarksNumberType)
            {
                case MarkNumberType.Half:
                    this.Round.MarksFrom =
                        this.Round.MarksTo =
                        (int)Math.Round((double)this.Round.Qualifieds.Count / 2, MidpointRounding.AwayFromZero);
                    break;
                case MarkNumberType.HalfToTwoThird:
                    this.Round.MarksFrom =
                        (int)Math.Round((double)this.Round.Qualifieds.Count / 2, MidpointRounding.AwayFromZero);
                    this.Round.MarksTo =
                        (int)Math.Round((double)this.Round.Qualifieds.Count * 2 / 3, MidpointRounding.AwayFromZero);
                    break;
                case MarkNumberType.TwoThird:
                    this.Round.MarksFrom =
                        this.Round.MarksTo =
                        (int)Math.Round((double)this.Round.Qualifieds.Count * 2 / 3, MidpointRounding.AwayFromZero);
                    break;
            }

            this.RaisePropertyChanged(() => this.MarksString);
        }

        private void SetVisibilitiesFromMarksInputTpye()
        {
            if (this.Round.MarksInputType == DataModel.MarkingTypes.Marking
                || this.Round.MarksInputType == DataModel.MarkingTypes.MarksSumOnly)
            {
                this.MarksVisible = Visibility.Visible;
            }
            else
            {
                this.MarksVisible = Visibility.Collapsed;
            }

            if (this.Round.MarksInputType == DataModel.MarkingTypes.NewJudgingSystemV2)
            {
                this.Js2SettingsVisible = Visibility.Visible;
            }
            else
            {
                this.Js2SettingsVisible = Visibility.Collapsed;
            }

            if (this.Round.RoundType.HasDrawing
                || this.Round.MarksInputType == DataModel.MarkingTypes.NewJudgingSystemV2
                || this.Round.MarksInputType == DataModel.MarkingTypes.NewJudgingSystemV1)
            {
                this.DrawingTypeVisible = Visibility.Visible;
            }
            else
            {
                this.DrawingTypeVisible = Visibility.Collapsed;
            }

            this.RaisePropertyChanged(() => this.MarksVisible);
            this.RaisePropertyChanged(() => this.Js2SettingsVisible);
            this.RaisePropertyChanged(() => this.DrawingTypeVisible);
        }

        private string ValidateCouplesPerHeat()
        {
            if (!this.Round.RoundType.HasDrawing)
            {
                return String.Empty;
            }

            if (this.NumberOfHeats < 1)
            {
                return LocalizationService.Resolve(() => Text.NumberOfHeatsGreaterZero);
            }

            return String.Empty;
        }

        private string ValidateMarksFrom()
        {
            if (this.Round.MarksNumberType != MarkNumberType.Manual)
            {
                return String.Empty;
            }

            if (!this.Round.MarksFrom.HasValue && !this.Round.MarksTo.HasValue)
            {
                return LocalizationService.Resolve(() => Text.MarksShouldNotBeEmpty);
            }

            if (this.Round.MarksFrom < 1)
            {
                return LocalizationService.Resolve(() => Text.MarksFromShouldBeGreateZero);
            }

            if (this.Round.MarksFrom > this.Round.MarksTo)
            {
                return LocalizationService.Resolve(() => Text.MarksFromShouldBeLowerEqual);
            }

            return String.Empty;
        }

        private string ValidateMarksTo()
        {
            if (this.Round.MarksNumberType != MarkNumberType.Manual)
            {
                return String.Empty;
            }

            if (this.Round.MarksTo < 1)
            {
                return LocalizationService.Resolve(() => Text.MarksToGreaterZero);
            }

            if (this.Round.MarksFrom > this.Round.MarksTo)
            {
                return LocalizationService.Resolve(() => Text.MarksToShouldBeGreaterMarksFrom);
            }

            return String.Empty;
        }

        private int GetMarksFrom()
        {
            int qualified = this.Round.Qualifieds.Count;

            if (this.Round.Number == 1)
            {
                if (qualified > 59)
                {
                    return 48;
                }
                if (qualified > 48)
                {
                    return 36;
                }
                if (qualified > 29)
                {
                    return 24;
                }
                if (qualified > 24)
                {
                    return 18;
                }
                if (qualified > 12)
                {
                    return 12;
                }

                return 6;
            }
            
            if (qualified > 35)
            {
                return 24;
            }
            if (qualified > 17)
            {
                return 12;
            }

            return 6;
            
        }

        #endregion
    }
}