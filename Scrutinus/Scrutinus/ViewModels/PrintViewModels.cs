﻿// // TPS.net TPS8 Scrutinus
// // PrintViewModels.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Printing;
using DataModel.Models;
using Scrutinus.Localization;
using Scrutinus.ViewModels;

namespace Scrutinus.PrintModels
{
    [Serializable]
    public class PrintDefinition : INotifyPropertyChanged
    {
        #region Public Events

        public event PropertyChangedEventHandler PropertyChanged
        {
            add
            {
                this.propertyChanged += value;
            }

            remove
            {
                if (this.propertyChanged != null)
                {
                    this.propertyChanged -= value;
                }
            }
        }

        #endregion

        #region Methods

        private void RaisePropertyChanged(string property)
        {
            if (this.propertyChanged != null)
            {
                this.propertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        #endregion

        #region Fields

        private bool enabled;

        private bool print;

        [NonSerialized]
        private PropertyChangedEventHandler propertyChanged;

        #endregion

        #region Constructors and Destructors

        public PrintDefinition(bool enabled, bool print, int copies, string printer)
        {
            this.Enabled = enabled;
            this.Print = print;
            this.Copies = copies;
            this.Printer = printer;
        }

        public PrintDefinition()
        {
            this.Enabled = true;
            this.Print = false;
            this.Copies = 1;
            this.Printer = LocalizationService.Resolve(() => Text.DefaultPrinter);
        }

        #endregion

        #region Public Properties

        public int Copies { get; set; }

        public bool Enabled
        {
            get
            {
                return this.enabled;
            }
            set
            {
                this.enabled = value;
                this.RaisePropertyChanged("Enabled");
            }
        }

        public bool Print
        {
            get
            {
                return this.print;
            }
            set
            {
                this.print = value;
                this.RaisePropertyChanged("Print");
            }
        }

        public string Printer { get; set; }

        #endregion
    }

    [Serializable]
    public class PrintingViewModel : BaseViewModel
    {
        #region Fields

        private Round selectedRound;

        #endregion

        #region Constructors and Destructors

        public PrintingViewModel()
        {
            var printServer = new LocalPrintServer();
            this.Printer =
                printServer.GetPrintQueues(
                    new[] { EnumeratedPrintQueueTypes.Local, EnumeratedPrintQueueTypes.Connections })
                    .Select(q => q.Name)
                    .ToList();

            this.Printer.Insert(0, LocalizationService.Resolve(() => Text.DefaultPrinter));
        }

        #endregion

        #region Public Properties

        public PrintDefinition Checksums { get; set; }

        public PrintDefinition JudgesWorkPlan { get; set; } = new PrintDefinition(true, false, 1, "Standard");

        public PrintDefinition WdsfChecksums { get; set; }

        // Printing Competition
        public PrintDefinition CorrectedStartList { get; set; }

        public PrintDefinition CoupleState { get; set; }

        public PrintDefinition CouplesDroppedOut { get; set; }

        public PrintDefinition CouplesResult { get; set; }

        public PrintDefinition CouplesResultSorted { get; set; }

        public PrintDefinition ResultOfRound { get; set; } = new PrintDefinition(true, false, 1, "Standard");

        public PrintDefinition CoverPage { get; set; }

        public PrintDefinition DTVAnnex { get; set; }

        public PrintDefinition DTVPart1 { get; set; }

        public PrintDefinition Drawing { get; set; }

        public PrintDefinition DrawingCouples { get; set; }

        public PrintDefinition EmptyJudgingSheets { get; set; }

        // Printing Event-Data
        public PrintDefinition EventNumberOverview { get; set; }

        public PrintDefinition EventCollectedReports { get; set; }

        public PrintDefinition EventCorrectedStartList { get; set; }

        public PrintDefinition EventParticipants { get; set; }

        public PrintDefinition EventResultOrderedByPlace { get; set; }

        public PrintDefinition EventStatistics { get; set; }

        public PrintDefinition EventProgramBook { get; set; }

        public PrintDefinition EventOfficials { get; set; }

        public PrintDefinition CompetitionOverview { get; set; }

        public PrintDefinition FinalResult { get; set; }

        public PrintDefinition FinalSkatingTable { get; set; }

        public PrintDefinition FinalTable { get; set; }

        public PrintDefinition Laufzettel { get; set; }

        public PrintDefinition MarkingLastRound { get; set; }

        public PrintDefinition MarkingRound { get; set; }

        public PrintDefinition MarkingTableTotal { get; set; }

        public PrintDefinition Officals { get; set; }

        public List<string> Printer { get; set; }

        public PrintDefinition Qualified { get; set; }

        public PrintDefinition RegisteredCouples { get; set; }

        public PrintDefinition ResultService { get; set; }

        public PrintDefinition ResultSortedByRegions { get; set; }

        public List<Round> RoundSelectionList { get; set; }

        public PrintDefinition RoundWithNames { get; set; }

        public PrintDefinition SeededCouples { get; set; }

        public PrintDefinition PinCodeJudges { get; set; }

        public PrintDefinition WdsfChecksumOfEvent { get; set; }

        public Round SelectedRound
        {
            get
            {
                return this.selectedRound;
            }
            set
            {
                this.selectedRound = value;
                if (!this.selectedRound.RoundType.IsFinal)
                {
                    this.FinalResult.Enabled = false;
                    this.FinalTable.Enabled = false;
                    this.FinalSkatingTable.Enabled = false;
                }
                else
                {
                    this.FinalResult.Enabled = true;
                    this.FinalTable.Enabled = true;
                    this.FinalSkatingTable.Enabled = false;

                    var grouped = this.selectedRound.Qualifieds.GroupBy(q => q.Participant.Points);
                    if (grouped.Any(g => g.Count() > 1))
                    {
                        // We have a skating table so enable printing ...
                        this.FinalSkatingTable.Enabled = true;
                    }
                }

                if (this.selectedRound.DrawingType == null)
                {
                    this.Drawing.Enabled = false;
                    this.DrawingCouples.Enabled = false;
                    this.RoundWithNames.Enabled = false;
                }
                else
                {
                    this.Drawing.Enabled = true;
                    this.DrawingCouples.Enabled = true;
                    this.RoundWithNames.Enabled = true;
                }

                this.CouplesDroppedOut.Enabled = this.selectedRound.Qualifieds.Any(q => q.Participant.State == DataModel.CoupleState.DroppedOut);


                this.RaisePropertyChanged(() => this.SelectedRound);
                this.RaisePropertyChanged(() => this.FinalTable);
                this.RaisePropertyChanged(() => this.FinalSkatingTable);
            }
        }

        public PrintDefinition WinnerCertificate { get; set; }

        #endregion

        #region Public Methods and Operators

        public void LoadSettings(string name)
        {
            this.FinalResult = this.context.GetParameter<PrintDefinition>(name + ".PrintDefinition.FinalResult")
                               ?? new PrintDefinition();
            this.RegisteredCouples =
                this.context.GetParameter<PrintDefinition>(name + ".PrintDefinition.RegisteredCouples")
                ?? new PrintDefinition();
            this.Qualified = this.context.GetParameter<PrintDefinition>(name + ".PrintDefinition.Qualified")
                             ?? new PrintDefinition();
            this.Drawing = this.context.GetParameter<PrintDefinition>(name + ".PrintDefinition.Drawing")
                           ?? new PrintDefinition();
            this.DrawingCouples = this.context.GetParameter<PrintDefinition>(name + ".PrintDefinition.DrawingCouples")
                                  ?? new PrintDefinition();
            this.RoundWithNames = this.context.GetParameter<PrintDefinition>(name + ".PrintDefinition.RoundWithNames")
                                  ?? new PrintDefinition();
            this.EmptyJudgingSheets =
                this.context.GetParameter<PrintDefinition>(name + ".PrintDefinition.EmptyJudgingSheets")
                ?? new PrintDefinition();
            this.CouplesDroppedOut =
                this.context.GetParameter<PrintDefinition>(name + ".PrintDefinition.CouplesDroppedOut")
                ?? new PrintDefinition();

            this.ResultOfRound = this.context.GetParameter<PrintDefinition>(name + ".ResultOfRound") ??
                                 new PrintDefinition();

            this.MarkingRound = this.context.GetParameter<PrintDefinition>(name + ".PrintDefinition.MarkingRound")
                                ?? new PrintDefinition();
            this.MarkingLastRound = this.context.GetParameter<PrintDefinition>(name + ".PrintDefinition.MarkingRound")
                                    ?? new PrintDefinition();
            this.WinnerCertificate =
                this.context.GetParameter<PrintDefinition>(name + ".PrintDefinition.MarkingLastRound")
                ?? new PrintDefinition();
            this.CouplesResult = this.context.GetParameter<PrintDefinition>(name + ".PrintDefinition.WinnerCertificate")
                                 ?? new PrintDefinition();
            this.CouplesResultSorted =
                this.context.GetParameter<PrintDefinition>(name + ".PrintDefinition.CouplesResultSorted")
                ?? new PrintDefinition();
            this.SeededCouples = this.context.GetParameter<PrintDefinition>(name + ".PrintDefinition.SeededCouples")
                                 ?? new PrintDefinition();
            this.MarkingTableTotal =
                this.context.GetParameter<PrintDefinition>(name + ".PrintDefinition.MarkingTableTotal")
                ?? new PrintDefinition();
            this.FinalTable = this.context.GetParameter<PrintDefinition>(name + ".PrintDefinition.FinalTable")
                              ?? new PrintDefinition();
            this.FinalSkatingTable =
                this.context.GetParameter<PrintDefinition>(name + ".PrintDefinition.FinalSkatingTable")
                ?? new PrintDefinition();
            this.DTVPart1 = this.context.GetParameter<PrintDefinition>(name + ".PrintDefinition.DTVPart1")
                            ?? new PrintDefinition();
            this.DTVAnnex = this.context.GetParameter<PrintDefinition>(name + ".PrintDefinition.DTVAnnex")
                            ?? new PrintDefinition();
            this.ResultService = this.context.GetParameter<PrintDefinition>(name + ".PrintDefinition.ResultService")
                                 ?? new PrintDefinition();
            this.CoupleState = this.context.GetParameter<PrintDefinition>(name + ".PrintDefinition.CoupleState")
                               ?? new PrintDefinition();
            this.Officals = this.context.GetParameter<PrintDefinition>(name + ".PrintDefinition.Officials")
                            ?? new PrintDefinition();
            this.ResultSortedByRegions =
                this.context.GetParameter<PrintDefinition>(name + ".PrintDefinition.ResultSortedByRegions")
                ?? new PrintDefinition();

            this.CoverPage = this.context.GetParameter<PrintDefinition>(name + ".PrintDefinition.CoverPage")
                             ?? new PrintDefinition();

            this.Laufzettel = this.context.GetParameter<PrintDefinition>(name + ".PrintDefinition.Laufzettel")
                              ?? new PrintDefinition();

            this.Checksums = this.context.GetParameter<PrintDefinition>(name + ".PrintDefinition.Checksums")
                             ?? new PrintDefinition();

            this.EventCollectedReports =
                this.context.GetParameter<PrintDefinition>(name + ".PrintDefinition.EventCollectedReports")
                ?? new PrintDefinition();

            this.EventParticipants =
                this.context.GetParameter<PrintDefinition>(name + ".PrintDefinition.EventParticipants")
                ?? new PrintDefinition();

            this.EventResultOrderedByPlace =
                this.context.GetParameter<PrintDefinition>(name + ".PrintDefinition.EventResultOrderedByPlace")
                ?? new PrintDefinition();

            this.EventStatistics = this.context.GetParameter<PrintDefinition>(name + ".PrintDefinition.EventStatistics") ?? new PrintDefinition();

            this.EventOfficials = this.context.GetParameter<PrintDefinition>(name + ".PrintDefinition.EventOfficials") ?? new PrintDefinition();

            this.CompetitionOverview = this.context.GetParameter<PrintDefinition>(name + ".PrintDefinition.CompetitionOverview") ?? new PrintDefinition();

            this.EventCorrectedStartList =
                this.context.GetParameter<PrintDefinition>(name + ".PrintDefinition.EventCorrectedStartList")
                ?? new PrintDefinition();

            this.CorrectedStartList =
                this.context.GetParameter<PrintDefinition>(name + ".PrintDefinition.CorrectedStartList")
                ?? new PrintDefinition();

            this.PinCodeJudges =
                this.context.GetParameter<PrintDefinition>(name + ".PrintDefinition.PinCodeJudges")
                ?? new PrintDefinition();

            this.EventNumberOverview =
                this.context.GetParameter<PrintDefinition>(name + ".PrintDefinition.EventNumberOverview") ??
                new PrintDefinition();

            this.EventProgramBook = this.context.GetParameter<PrintDefinition>(name + ".PrintDefinition.EventProgramBook") ??
                new PrintDefinition();

            this.WdsfChecksums = this.context.GetParameter<PrintDefinition>(name + ".PrintDefinition.WdsfChecksums") ?? new PrintDefinition();

            this.WdsfChecksumOfEvent = this.context.GetParameter<PrintDefinition>(name + ".PrintDefinition.WdsfChecksumOfEvent") ?? new PrintDefinition();

            this.CheckAllSelectedPrinters();

            /// todo: nach dem 1.9. Checksummen-Druck komplett raus nehmen.
            var checkDate = new DateTime(2017, 8, 31);
            
            if (DateTime.Now.Date > checkDate)
            {
                // no more DTV Checksums after 1.9.2017
                this.Checksums.Enabled = false;
            }
        }

        public void SaveConfiguration(string name)
        {
            this.context.SetParameter(name + ".PrintDefinition.FinalResult", this.FinalResult);
            this.context.SetParameter(name + ".PrintDefinition.RegisteredCouples", this.RegisteredCouples);
            this.context.SetParameter(name + ".PrintDefinition.Qualified", this.Qualified);
            this.context.SetParameter(name + ".PrintDefinition.Drawing", this.Drawing);
            this.context.SetParameter(name + ".PrintDefinition.DrawingCouples", this.DrawingCouples);
            this.context.SetParameter(name + ".PrintDefinition.RoundWithNames", this.RoundWithNames);
            this.context.SetParameter(name + ".PrintDefinition.EmptyJudgingSheets", this.EmptyJudgingSheets);
            this.context.SetParameter(name + ".PrintDefinition.CouplesDroppedOut", this.CouplesDroppedOut);
            this.context.SetParameter(name + ".PrintDefinition.MarkingRound", this.MarkingRound);
            this.context.SetParameter(name + ".PrintDefinition.MarkingLastRound", this.MarkingRound);
            this.context.SetParameter(name + ".PrintDefinition.WinnerCertificate", this.WinnerCertificate);
            this.context.SetParameter(name + ".PrintDefinition.CouplesResult", this.CouplesResult);
            this.context.SetParameter(name + ".PrintDefinition.CouplesResultSorted", this.CouplesResultSorted);
            this.context.SetParameter(name + ".PrintDefinition.SeededCouples", this.SeededCouples);
            this.context.SetParameter(name + ".PrintDefinition.MarkingTableTotal", this.MarkingTableTotal);
            this.context.SetParameter(name + ".PrintDefinition.FinalTable", this.FinalTable);
            this.context.SetParameter(name + ".PrintDefinition.FinalSkatingTable", this.FinalSkatingTable);
            this.context.SetParameter(name + ".PrintDefinition.DTVPart1", this.DTVPart1);
            this.context.SetParameter(name + ".PrintDefinition.DTVAnnex", this.DTVAnnex);
            this.context.SetParameter(name + ".PrintDefinition.ResultService", this.ResultService);
            this.context.SetParameter(name + ".PrintDefinition.CoupleState", this.CoupleState);
            this.context.SetParameter(name + ".PrintDefinition.Officials", this.Officals);
            this.context.SetParameter(name + ".PrintDefinition.ResultSortedByRegions", this.ResultSortedByRegions);
            this.context.SetParameter(name + ".PrintDefinition.CoverPage", this.CoverPage);
            this.context.SetParameter(name + ".PrintDefinition.Laufzettel", this.Laufzettel);
            this.context.SetParameter(name + ".PrintDefinition.EventStatistics", this.EventStatistics);
            this.context.SetParameter(name + ".PrintDefinition.CompetitionOverview", this.CompetitionOverview);
            this.context.SetParameter(name + ".PrintDefinition.EventOfficials", this.EventOfficials);
            this.context.SetParameter(name + ".PrintDefinition.EventCollectedReports", this.EventCollectedReports);
            this.context.SetParameter(name + ".PrintDefinition.EventParticipants", this.EventParticipants);
            this.context.SetParameter(name + ".PrintDefinition.EventResultOrderedByPlace", this.EventResultOrderedByPlace);
            this.context.SetParameter(name + ".PrintDefinition.Checksums", this.Checksums);
            this.context.SetParameter(name + ".PrintDefinition.CorrectedStartList", this.CorrectedStartList);
            this.context.SetParameter(name + ".PrintDefinition.EventCorrectedStartList", this.EventCorrectedStartList);
            this.context.SetParameter(name + ".PrintDefinition.PinCodeJudges", this.PinCodeJudges);
            this.context.SetParameter(name + ".PrintDefinition.EventNumberOverview", this.EventNumberOverview);
            this.context.SetParameter(name + ".PrintDefinition.EventProgramBook", this.EventProgramBook);
            this.context.SetParameter(name + ".PrintDefinition.WdsfChecksums", this.WdsfChecksums);
            this.context.SetParameter(name + ".PrintDefinition.WdsfChecksumOfEvent", this.WdsfChecksumOfEvent);
            this.context.SetParameter(name + "ResultOfRound", this.ResultOfRound);
        }

        public override void SaveData()
        {
            this.SaveConfiguration("Default");
        }

        public override bool Validate()
        {
            this.SaveData();
            return true;
        }

        public void WithAllPrintDefinitions(Action<PrintDefinition> action)
        {
            var properties =
                this.GetType().GetProperties().Where(p => p.PropertyType == typeof(PrintDefinition));

            foreach (var propertyInfo in properties)
            {
                var value = propertyInfo.GetValue(this, null) as PrintDefinition;
                action(value);
            }
        }

        #endregion

        #region Methods

        private void CheckAllSelectedPrinters()
        {
            this.WithAllPrintDefinitions(this.CheckSelectedPrinter);
        }

        private void CheckSelectedPrinter(PrintDefinition printDefinition)
        {
            if (printDefinition == null)
            {
                return;
            }

            if (!this.Printer.Contains(printDefinition.Printer))
            {
                // Unknown printer, let's go pack to default printer
                printDefinition.Printer = LocalizationService.Resolve(() => Text.DefaultPrinter);
            }
        }

        #endregion
    }
}