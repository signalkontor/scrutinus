﻿// // TPS.net TPS8 Scrutinus
// // CompetitionResultViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using DataModel;
using DataModel.Models;
using DtvEsvModule;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using General.Rules.PointCalculation;
using MahApps.Metro.Controls.Dialogs;
using Scrutinus.Dialogs;
using Scrutinus.Dialogs.DialogViewModels;
using Scrutinus.ExternalApi;
using Scrutinus.Localization;
using Scrutinus.MyHeats;
using Scrutinus.PrintModels;
using Scrutinus.Reports;

namespace Scrutinus.ViewModels
{
    public class CompetitionResultViewModel : ViewModelBase
    {
        #region Fields

        private readonly ScrutinusContext context;

        private Task resultTask;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CompetitionResultViewModel"/> class.
        /// For Design Time Only!!!
        /// </summary>
        public CompetitionResultViewModel()
        {
            
        }

        public CompetitionResultViewModel(int roundId)
        {
            this.context = new ScrutinusContext();

            this.Competition = this.context.Rounds.Single(r => r.Id == roundId).Competition;

            if (!this.Competition.EndTime.HasValue)
            {
                // This will be only written once ...
                this.Competition.EndTime = DateTime.Now;
                this.context.SaveChanges();
            }

            this.ClimbUps = new ObservableCollection<ClimbUp>(this.Competition.ClimbUps);

            this.Classes = this.context.Classes.ToList();

            this.Couples = this.Competition.Participants.OrderBy(p => p.PlaceFrom).Select(p => p.Couple).ToList();

            this.AddClimbUpCommand = new RelayCommand(this.AddClimbUp);

            this.SaveCommand = new RelayCommand(this.SaveData);

            this.DeleteClimbUpCommand = new RelayCommand(this.DeleteClimbUp);

            this.SendWdsfCommand = new RelayCommand(this.SendWdsf);

            this.WdsfCloseCommand = new RelayCommand(this.WdsfCloseCompetition);

            this.AddJudgingSheetsCommand = new RelayCommand(this.AddJudgingSheets);

            this.SendResultToDtvCommand = new RelayCommand(this.SendResultToDtv);

            this.DisqualifySylabusCommand = new RelayCommand(this.DisqualifySylabus);

            this.PrintLaufzettelCommand = new RelayCommand(this.PrintLaufzettel);

            this.DisqualifyCommand = new RelayCommand(this.Disqualify);

            this.DancingParticipants =
                new ObservableCollection<Participant>(
                    this.Competition.Participants.Where(
                        p => p.State == CoupleState.DroppedOut || p.State == CoupleState.Dancing)
                        .OrderBy(p => p.PlaceFrom));

            var laufzettel =
                this.Competition.Participants.Where(p => p.Startbuch != null && p.Startbuch.PrintLaufzettel)
                    .Select(s => new WriteableTuple<bool, string, Participant>(false, s.NiceNameWithNumber, s));

            this.LaufzettelToPrint = new ObservableCollection<WriteableTuple<bool, string, Participant>>(laufzettel);

            this.PrintViewModel = new PrintingViewModel();

            this.SelectedPrinter = this.PrintViewModel.Printer.FirstOrDefault();
        }

        #endregion

        #region Public Properties

        public ICommand AddClimbUpCommand { get; set; }

        public ICommand AddJudgingSheetsCommand { get; set; }

        public ICommand PrintLaufzettelCommand { get; set; }

        public IEnumerable<Class> Classes { get; set; }

        public ICommand DisqualifyCommand { get; set; }

        public Participant SelectedParticipantInGrid { get; set; }

        public ObservableCollection<ClimbUp> ClimbUps { get; set; }

        public ObservableCollection<WriteableTuple<bool, string, Participant>> LaufzettelToPrint { get; set; }

        public Competition Competition { get; set; }

        public IEnumerable<Couple> Couples { get; set; }

        public PrintingViewModel PrintViewModel { get; set; }

        public string SelectedPrinter { get; set; }

        public ObservableCollection<Participant> DancingParticipants { get; private set; }

        public ICommand DeleteClimbUpCommand { get; set; }

        public ICommand DisqualifySylabusCommand { get; set; }

        public ICommand SaveCommand { get; set; }

        public Class SelectedClass { get; set; }

        public ClimbUp SelectedClimbUp { get; set; }

        public Couple SelectedCouple { get; set; }

        public Participant SelectedParticipant { get; set; }

        public ICommand SendResultToDtvCommand { get; set; }

        public ICommand SendWdsfCommand { get; set; }

        public ICommand WdsfCloseCommand { get; set; }

        #endregion

        #region Methods

        private void AddClimbUp()
        {
            if (this.SelectedParticipant == null)
            {
                MainWindow.MainWindowHandle.ShowMessage(LocalizationService.Resolve(() => Text.SelectCoupleForClimbup));
                return;
            }

            var climbUp = new ClimbUp
                              {
                                  ClimbupType = ClimbupType.Decision,
                                  Competition = this.Competition,
                                  Couple = this.SelectedParticipant.Couple,
                                  NewClass = this.SelectedParticipant.TargetClass
                              };

            this.SelectedParticipant.ClimbUp = true;

            DTVPointCalculator.UpdateAllParticipants(
                this.context,
                this.SelectedParticipant.Id,
                0,
                0,
                this.SelectedParticipant.TargetClass,
                false,
                true);

            this.ClimbUps.Add(climbUp);
            this.Competition.ClimbUps.Add(climbUp);
            this.LaufzettelToPrint.Add(new WriteableTuple<bool, string, Participant>(true, this.SelectedParticipant.NiceNameWithNumber, this.SelectedParticipant));

            this.context.ClimbUps.Add(climbUp);

#if DTV
            if (this.Competition.Event.RuleName == "DTV National")
            {
                this.SelectedParticipant.Startbuch.PrintLaufzettel = true;

                this.RecalculateCheckSum();
            }
#endif

            this.context.SaveChanges();
        }

        private void AddJudgingSheets()
        {
            var dialog = new AttachJudgingSheets(this.Competition.Id);
            MainWindow.MainWindowHandle.ShowDialog(dialog, null, 500, 700);
        }

        private void DeleteClimbUp()
        {
            if (this.SelectedClimbUp == null)
            {
                MainWindow.MainWindowHandle.ShowMessage(LocalizationService.Resolve(() => Text.SelectClimbUpMessage));
                return;
            }

            if (this.SelectedClimbUp.ClimbupType != ClimbupType.Decision)
            {
                MainWindow.MainWindowHandle.ShowMessage(
                    LocalizationService.Resolve(() => Text.CannotDeleteGeneratedClimbUps));
                return;
            }

            this.context.ClimbUps.Remove(this.SelectedClimbUp);
            this.context.SaveChanges();

            this.ClimbUps.Remove(this.SelectedClimbUp);

#if DTV
            this.RecalculateCheckSum();
#endif
        }

        private void DisqualifySylabus()
        {
            var final = this.Competition.Rounds.First(r => r.RoundType.Id == RoundTypes.Final);

            var dialog = new SylabusWarningDialog(this.context, this.DancingParticipants, null, final);

            MainWindow.MainWindowHandle.ShowDialog(dialog, null, 300, 300);
        }

        private void ErrorHandling(Task task)
        {
            if (task.Exception != null)
            {
                var exception = (Exception)task.Exception;
                {
                    while (exception.InnerException != null)
                    {
                        exception = exception.InnerException;
                    }
                }

                MainWindow.MainWindowHandle.ShowMessage(exception.Message);
            }
            else
            {
                MainWindow.MainWindowHandle.ShowMessage("Task executed successfully!");
            }
        }

        private void RecalculateCheckSum()
        {
            var apiImporter = new DtvApiImporter("", "", "", this.context);
            var oldChecksum = this.Competition.DtvChecksum;
            apiImporter.CalculateChecksum(this.Competition);

            if (oldChecksum != null && oldChecksum != this.Competition.DtvChecksum)
            {
                MainWindow.MainWindowHandle.ShowMessage(
                    "Die DTV Checksumme für dieses Turnier hat sich geändert. Falls bereits Checksummen für die Funktionäre gedruckt wurden, müssen diese nun neu gedruckt werden!");
            }
        }

        private void SaveData()
        {
#if DTV
            this.RecalculateCheckSum();
#endif
            this.context.SaveChanges();
        }

        private void SendResultToDtv()
        {
            using (var dbContext = new ScrutinusContext())
            {
                var competition = dbContext.Competitions.Single(c => c.Id == this.Competition.Id);

                var importer = new DtvApiImporter(
                    Settings.Default.DtvApiUrl,
                    Settings.Default.WDSFApiUser,
                    Settings.Default.WDSFApiPassword,
                    dbContext);

                var result = importer.SaveResult(competition);
                dbContext.SaveChanges();

                var message = string.Join("\r\n", result);

                MainWindow.MainWindowHandle.ShowMessage(message);

                competition.LastDtvUpload = DateTime.Now;
                competition.DtvResultUploaded = true;
                dbContext.SaveChanges();

                // Upload Judging-Sheets ( if any)
                try
                {
                    var judgingSheetsSend = false;
                    var digitalSend = false;

                    if (competition.JudgingSheets.Any())
                    {
                        importer.SendJudgingSheets(competition);
                        judgingSheetsSend = true;
                    }

                    var resultSendJudgingSheets = new string[] {""};

                    if (competition.Rounds.First().EjudgeData.Any())
                    {
                        resultSendJudgingSheets = importer.SendDigitalJudgingSheets(competition);
                    }
                    else if (Directory.Exists(Settings.Default.MobileControlExchange))
                    {
                        resultSendJudgingSheets = importer.SendDigitalJudgingSheetsFromMobileControl(Settings.Default.MobileControlExchange, competition);
                        digitalSend = resultSendJudgingSheets != null && resultSendJudgingSheets.Length > 0;
                    }

                    if (judgingSheetsSend || digitalSend)
                    {
                        MainWindow.MainWindowHandle.ShowMessage(String.Join("\r\n", resultSendJudgingSheets), "Wertungsrichterzettel hochgeladen.", MessageDialogStyle.Affirmative);
                    }
                    else if (!competition.Canceled)
                    {
                        MainWindow.MainWindowHandle.ShowMessage("Achtung! Keine Wertungsrichterzettel hochgeladen!");
                    }
                }
                catch (Exception ex)
                {
                    MainWindow.MainWindowHandle.ShowMessage(
                        "Fehler beim Hochladen der Wertungsrichterzettel!\r\n" + ex.Message);
                }
            }
        }

        private void SendWdsf()
        {
            if (this.resultTask != null && !this.resultTask.IsCanceled && !this.resultTask.IsCompleted && !this.resultTask.IsFaulted)
            {
                // task still running, we do not start a new one
                return;
            }

            var shouldIClear =
                MainWindow.MainWindowHandle.ShowMessage(
                    "Should I clear all previously upploaded data before sending the final results?",
                    "Clear Data?",
                    MessageDialogStyle.AffirmativeAndNegative);

            this.resultTask = Task.Factory.StartNew(
                () =>
                    {
                        var dbContext = new ScrutinusContext();
                        var competition = dbContext.Competitions.Single(c => c.Id == this.Competition.Id);
                        var apiClient = new WdsfApiClient(dbContext, competition);

                        if (shouldIClear == MessageDialogResult.Affirmative)
                        {
                            if (this.Competition.Participants.First().Couple is Team)
                            {
                                apiClient.ClearAllTeams();
                            }
                            else
                            {
                                apiClient.ClearAllParticipants();
                            }
                            
                            foreach (
                                var source in
                                    dbContext.Participants.Where(p => p.Competition.Id == competition.Id))
                            {
                                source.ExternalId = null;
                            }

                            apiClient.ClearOfficials();
                        }

                        apiClient.SendResults(dbContext.Rounds.Single(r => r.Id == this.Competition.CurrentRound.Id));
                        apiClient.SetStatus("Processing");
                        dbContext.SaveChanges();
                    });

            this.resultTask.ContinueWith(this.ErrorHandling);
        }

        private void WdsfCloseCompetition()
        {
            if (this.resultTask != null && !this.resultTask.IsCanceled && !this.resultTask.IsCompleted && !this.resultTask.IsFaulted)
            {
                // task still running, we do not start a new one
                return;
            }

            this.resultTask = Task.Factory.StartNew(
                () =>
                    {
                        var dbContext = new ScrutinusContext();
                        var competition = dbContext.Competitions.Single(c => c.Id == this.Competition.Id);
                        var apiClient = new WdsfApiClient(this.context, competition);

                        apiClient.SetStatus("Closed");
                    });

            this.resultTask.ContinueWith(this.ErrorHandling);
        }

        private void PrintLaufzettel()
        {
            foreach (var writeableTuple in this.LaufzettelToPrint.Where(t => t.Item1))
            {
                PrintHelper.PrintLaufzettel(this.Competition, writeableTuple.Item3, this.SelectedPrinter, 1, false);
            }
           
        }

        private void Disqualify()
        {
            if (this.SelectedParticipantInGrid == null)
            {
                MainWindow.MainWindowHandle.ShowMessage(
                    LocalizationService.Resolve(() => Text.SelectCoupleToDisqualify));
                return;
            }

            var final = this.Competition.Rounds.OrderBy(r => r.Number).Last();

            var diqualifyPage =
                new DisqualifiyCouple(
                    new DisqualifyCoupleViewModel(this.context, final.Id, this.SelectedParticipantInGrid.Id));

            MainWindow.MainWindowHandle.ShowDialog(diqualifyPage, this.UpdateAfterDisqualification, 400, 200);
        }

        private void UpdateAfterDisqualification(object sender)
        {
            // Save Sylabus / Disqualification data.
            this.context.SaveChanges();

            var final = this.Competition.Rounds.OrderBy(r => r.Number).Last();

            this.DancingParticipants.Clear();

            var participants =
                this.Competition.Participants.Where(
                    p => p.State == CoupleState.DroppedOut || p.State == CoupleState.Dancing).OrderBy(p => p.PlaceFrom);

            foreach (var participant in participants)
            {
                this.DancingParticipants.Add(participant);
            }
        }

        #endregion
    }
}