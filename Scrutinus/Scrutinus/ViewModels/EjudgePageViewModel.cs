﻿// // TPS.net TPS8 Scrutinus
// // EjudgePageViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.ObjectModel;
using DataModel.Models;

namespace Scrutinus.ViewModels
{
    public class EjudgePageViewModel
    {
        public EjudgePageViewModel()
        {
            this.Devices = MainWindow.MainWindowHandle.ViewModel.Devices;
        }

        public ObservableCollection<Device> Devices { get; set; }
    }
}
