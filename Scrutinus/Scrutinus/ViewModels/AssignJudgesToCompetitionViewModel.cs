﻿// // TPS.net TPS8 Scrutinus
// // AssignJudgesToCompetitionViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using DataModel.Models;

namespace Scrutinus.ViewModels
{
    public class AssignJudgeToCompetionCompetitionData
    {
        #region Public Properties

        public Competition Competition { get; set; }

        public bool IsSelected { get; set; }

        #endregion
    }

    public class AssignJudgeToCompetitionJudgesData
    {
        #region Public Properties

        public List<AssignJudgeToCompetionCompetitionData> Assignements { get; set; }

        public Official Judge { get; set; }

        public Role Role { get; set; }

        #endregion
    }
}