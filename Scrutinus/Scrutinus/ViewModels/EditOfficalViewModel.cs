﻿// // TPS.net TPS8 Scrutinus
// // EditOfficalViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using DataModel.Models;

namespace Scrutinus.ViewModels
{
    public class EditOfficialViewModel
    {
        #region Public Properties

        public bool RoleEnabled { get; set; }

        public int RoleId { get; set; }

        public string RoleName { get; set; }

        #endregion
    }

    public class EditOfficalList : List<EditOfficialViewModel>
    {
        #region Constructors and Destructors

        public EditOfficalList(Official offical, ScrutinusContext context)
        {
            foreach (var role in context.Roles)
            {
                var roleedit = new EditOfficialViewModel { RoleId = role.Id, RoleName = role.RoleLong };

                if (offical.Id > 0)
                {
                    if (offical.Roles.Count(o => o.Id == role.Id) > 0)
                    {
                        roleedit.RoleEnabled = true;
                    }
                }
                this.Add(roleedit);
            }
        }

        #endregion
    }
}