﻿// // TPS.net TPS8 Scrutinus
// // EditOfficialsViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using DataModel;
using DataModel.Models;
using DtvEsvModule;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using General.Extensions;
using Helpers.Extensions;
using MahApps.Metro.Controls.Dialogs;
using Scrutinus.Dialogs.DialogViewModels;
using Scrutinus.ExternalApi;
using Scrutinus.Localization;

namespace Scrutinus.ViewModels
{
    public class NewOfficialAddedMessage
    {}

    public class EditOfficialsViewModel : ViewModelBase
    {
        #region Constructors and Destructors

        public EditOfficialsViewModel()
        {
            this.context = new ScrutinusContext();
            this.dispatcher = Dispatcher.CurrentDispatcher;

            this.Officials = new ObservableCollection<Official>(this.context.Officials);
            this.RoleList = new ObservableCollection<Role>(this.context.Roles);
            this.CountryCodes = new ObservableCollection<CountryCode>(this.context.CountryCodes);

            this.LanguageCodes = new ObservableCollection<Tuple<int, string>>()
            {
                new Tuple<int, string>(0, "Deutsch"),
                new Tuple<int, string>(1, "English"),
                new Tuple<int, string>(2, "Italiano"),
                new Tuple<int, string>(4, "Español"),
                new Tuple<int, string>(5, "Português"),
                new Tuple<int, string>(6, "Pоссийский"),
                new Tuple<int, string>(7, "Nederlands"),
                new Tuple<int, string>(8, "Català"),
                new Tuple<int, string>(9, "Français"),
                new Tuple<int, string>(10, "Dansk"),
                new Tuple<int, string>(11, "Hungarian"),
                new Tuple<int, string>(12, "Suomi")
            };
            

            this.AvailableJudgingDevices = MainWindow.MainWindowHandle.ViewModel.Devices;

            this.AddOfficialCommand = new RelayCommand(this.AddOfficial);
            this.DeleteCommand = new RelayCommand(this.Delete);
            this.SaveCommand = new RelayCommand(this.Save);
            this.RegisterAtWdsfCommand = new RelayCommand(this.RegisterAtWdsf);
            this.CheckDtvLicensesCommand = new RelayCommand(this.CheckDtvLicenses);
            this.GeneratePinCodesCommand = new RelayCommand(this.GeneratePinCodes);
            this.UpdateDtvOfficalsCommand = new RelayCommand(this.UpdateDtvOfficials);
            this.AddDtvOfficialCommand = new RelayCommand(this.AddDtvOfficials);
        }

        #endregion

        #region Fields

        private readonly ScrutinusContext context;

        private Official selectedOfficial;

        private readonly Dispatcher dispatcher;

        #endregion

        #region Public Properties

        public ICommand AddOfficialCommand { get; set; }

        public ICommand UpdateDtvOfficalsCommand { get; set; }

        public ICommand AddDtvOfficialCommand { get; set; }

        public ObservableCollection<Device> AvailableJudgingDevices { get; set; }

        public ObservableCollection<Tuple<int, string>> LanguageCodes { get; set; }

        public ICommand CheckDtvLicensesCommand { get; set; }

        public ObservableCollection<CountryCode> CountryCodes { get; set; }


        public ICommand DeleteCommand { get; set; }

        public ICommand GeneratePinCodesCommand { get; set; }

        public ObservableCollection<Official> Officials { get; set; }

        public ICommand RegisterAtWdsfCommand { get; set; }

        public ObservableCollection<Role> RoleList { get; set; }

        public ICommand SaveCommand { get; set; }

        public Official SelectedOfficial
        {
            get
            {
                return this.selectedOfficial;
            }
            set
            {
                if (this.selectedOfficial != null)
                {
                    this.SetRolesForOfficial(this.selectedOfficial);
                    this.selectedOfficial.PropertyChanged -= this.OfficialPropertyChanged;
                }

                this.selectedOfficial = value;

                this.SelectedRoles.Clear();

                if (this.selectedOfficial != null && this.selectedOfficial.Roles != null)
                {
                    foreach (var role in this.selectedOfficial.Roles.ToList())
                    {
                        this.SelectedRoles.Add(role);
                    }
                }

                if (this.selectedOfficial != null)
                {
                    this.selectedOfficial.PropertyChanged += this.OfficialPropertyChanged;

                    this.RaisePropertyChanged(() => this.SelectedOfficial);
                }
            }
        }

        private void OfficialPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
#if DTV
            if (propertyChangedEventArgs.PropertyName == "ExternalId")
            {
                // Now check if we can find this Official
                var licenseHolder = this.context.DtvLicenseHolders.FirstOrDefault(d => d.DtvId == this.SelectedOfficial.ExternalId);
                if (licenseHolder != null)
                {
                    this.SelectedOfficial.Licenses = string.Join(", ", licenseHolder.Licenses);
                }
                else
                {
                    this.ValidateDtvId(this.SelectedOfficial.ExternalId);
                }
            }
#endif
        }

        private void ValidateDtvId(string externalId)
        {
            if (!externalId.StartsWith("DE"))
            {
                MainWindow.MainWindowHandle.ShowMessageAsync("Die DTV-ID muss mit 'DE' beginnen");
                return;
            }
            if (externalId.Length != 11)
            {
                MainWindow.MainWindowHandle.ShowMessageAsync("Die Länge der DTV ID scheint falsch zu sein.");
                return;
            }

            if (!this.IsCheckSumOk(externalId))
            {
                MainWindow.MainWindowHandle.ShowMessageAsync("Die Dtv-Id scheint einen Vertipper zu haben.");
                return;
            }
        }

        private bool IsCheckSumOk(string externalId)
        {
            try
            {
                var position = 2;
                var value = 9;
                var sum = 0;

                while (position < 10)
                {
                    sum += int.Parse(externalId.Substring(position, 1))*value;
                    position++;
                    value--;
                }
                var mod = sum%11;
                var checksum = 11 - mod;

                if (checksum == 10)
                {
                    checksum = 0;
                } else if (checksum == 11)
                {
                    checksum = 5;
                }

                var dtvCheck = int.Parse(externalId.Substring(10, 1));

                return dtvCheck == checksum;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public IList SelectedRoles { get; set; }

        #endregion

        #region Methods

        private void AddOfficial()
        {
            var newOfficial = new Official
                                  {
                                      Sign = string.Empty,
                                      Firstname = string.Empty,
                                      Lastname = string.Empty,
                                      Club = string.Empty,
                                  };
            try
            {
                this.context.Officials.Add(newOfficial);
            }
            catch (DbEntityValidationException ex)
            {
                var msg = ex.EntityValidationErrors.Aggregate(
                    string.Empty,
                    (current, dbEntityValidationResult) => current + dbEntityValidationResult.Entry.ToString());
                MainWindow.MainWindowHandle.ShowMessage(msg);
            }

            this.Officials.Add(newOfficial);
            var roleJudge = this.context.Roles.FirstOrDefault(r => r.Id == Roles.Judge);
            newOfficial.Roles.Add(roleJudge);
            this.SelectedOfficial = newOfficial;

            Messenger.Default.Send(new NewOfficialAddedMessage());
        }

        private void CheckDtvLicenses()
        {
            var apiClient = new ApiClient(
                Settings.Default.DtvApiUrl,
                Settings.Default.WDSFApiUser,
                Settings.Default.WDSFApiPassword);

            //this.progressControlerTask = MainWindow.MainWindowHandle.ShowProgressAsync(
            //    "Überprüfe DTV Funktionäre",
            //    "Die DTV Funktionäre werden nun überprüft");

            Task.Factory.StartNew(new Action(
                    () =>
                        {
                            

                            Task.Factory.StartNew(
                                () =>
                                    {
                                        var index = 0;
                                        foreach (var official in this.Officials)
                                        {
                                            index++;
                                            MainWindow.MainWindowHandle.ReportStatus(
                                                "Prüfe " + official.NiceName,
                                                index,
                                                this.Officials.Count);

                                            if (!string.IsNullOrEmpty(official.ExternalId))
                                            {
                                                // Get this person:
                                                try
                                                {
                                                    var funktionär =
                                                        apiClient.GetFunktionaer(official.ExternalId);

                                                    if (funktionär == null)
                                                    {
                                                        MainWindow.MainWindowHandle.ShowMessage(
                                                            string.Format(
                                                                "Funktionär {0} {1} mit DTV ID {2} wurde nicht gefunden!",
                                                                official.Firstname,
                                                                official.Lastname,
                                                                official.ExternalId));
                                                        continue;
                                                    }

                                                    official.Firstname = funktionär.vorname;
                                                    official.Lastname = funktionär.nachname;
                                                    official.Club = funktionär.club?.name;
                                                    official.Region = funktionär.club?.ltv?.name;
                                                    official.Licenses = string.Join(", ", funktionär.lizenzen);
                                                }
                                                catch (Exception ex)
                                                {
                                                    MainWindow.MainWindowHandle.ShowMessage(
                                                        string.Format(
                                                            "Beim Zugriff auf den DTV Server ist ein Fehler aufgetretten: {0}",
                                                            ex.Message));
                                                    return;
                                                }
                                            }
                                        }
                                        this.context.SaveChanges();
     
                                        MainWindow.MainWindowHandle.RemoveStatusReport();
                                    });
                        }));
        }

        private void Delete()
        {
            if (this.SelectedOfficial == null)
            {
                return;
            }

            this.context.Officials.Remove(this.SelectedOfficial);
            this.context.SaveChanges();
            this.Officials.Remove(this.SelectedOfficial);
        }

        private void GeneratePinCodes()
        {
            var random = new Random();

            var minRandom = (int)Math.Pow(10, Settings.Default.PinCodeLength - 1);
            var maxRandom = (int)Math.Pow(10, Settings.Default.PinCodeLength) - 1;

            var judges = this.context.Officials.Where(o => o.Roles.Any(r => r.Id == Roles.Judge));

            foreach (var official in judges.ToList().Where(j => string.IsNullOrWhiteSpace(j.PinCode)))
            {
                var unique = false;
                while (!unique)
                {
                    official.PinCode = random.Next(minRandom, maxRandom).ToString();
                    unique = !judges.Any(o => o.PinCode == official.PinCode && o.Sign != official.Sign);
                }
                
            }

            this.Save();
        }

        private void RegisterAtWdsf()
        {
            var client = new WdsfApiClient(this.context, null);

            var task = new Task(
                () =>
                    {
                        try
                        {
                            client.CheckOfficials(this.Officials);
                        }
                        catch (Exception ex)
                        {
                            var message = ex.Message
                                             + (ex.InnerException != null ? " : " + ex.InnerException.Message : "");
                            MainWindow.MainWindowHandle.ShowMessage("WDSF API failed: " + message);
                        }
                    });
            task.Start();
        }

        private void Save()
        {
            if (this.selectedOfficial != null)
            {
                this.SetRolesForOfficial(this.selectedOfficial);
            }

            var doubles =
                this.Officials.GroupBy(g => g.Sign).Where(g => g.Count() > 1).Select(x => x.Key).ToList();

            if (doubles.Any())
            {
                var msg = string.Format(LocalizationService.Resolve(() => Text.DoublicateJudgeSign), doubles.First());
                MainWindow.MainWindowHandle.ShowMessage(msg);
                return;
            }

            this.context.SaveChanges();

            this.WritePinCodes();
        }

        private void SetRolesForOfficial(Official official)
        {
            if (official.Roles.Any(r => r.Id == Roles.Judge) &&
                this.SelectedRoles.Cast<Role>().All(r => r.Id != Roles.Judge))
            {
                var competitions =
                    this.context.Competitions.Where(
                        c => c.CurrentRound == null && c.Officials.Any(o => o.Role.Id == Roles.Judge && o.Official.Id == official.Id)).ToList();

                if (competitions.Any())
                {
                    var result =
                        MainWindow.MainWindowHandle.ShowMessage(string.Format(Text.RemoveRoleJudge, official.NiceName),
                            Text.RemoveRoleJudgeTitle, MessageDialogStyle.AffirmativeAndNegative);

                    if (result == MessageDialogResult.Affirmative)
                    {
                        foreach (var competiton in competitions)
                        {
                            var item =
                                competiton.Officials.FirstOrDefault(
                                    o => o.Official.Id == official.Id && o.Role.Id == Roles.Judge);

                            if (item != null)
                            {
                                competiton.Officials.Remove(item);
                                this.context.OfficialCompetition.Remove(item);
                            }

                        }
                    }
                }
            }

            official.Roles.Clear();

            foreach (Role selectedRole in this.SelectedRoles)
            {
                official.Roles.Add(selectedRole);
            }

            official.RaisePropertyChanged("RolesAsString");
        }

        private void WritePinCodes()
        {
            var path = Settings.Default.MobileControlExchange + @"\..\Cfg";
            if (!Directory.Exists(path))
            {
                try
                {
                    Directory.CreateDirectory(path);
                }
                catch (Exception ex)
                {
                    MainWindow.MainWindowHandle.ShowMessageAsync(ex.Message);
                    return;
                }
            }

            using (var stream = new StreamWriter(path + @"\PinsJudges.csv"))
            {
                foreach (var judge in this.Officials.Where(o => o.Roles.Any(r => r.Id == Roles.Judge)))
                {
                    stream.WriteLine("{0};{1};{2};", judge.Sign, judge.NiceName, judge.PinCode);
                }
            }
        }

        private void UpdateDtvOfficials()
        {
            var apiImporter = new DtvApiImporter(Settings.Default.DtvApiUrl, Settings.Default.WDSFApiUser, Settings.Default.WDSFApiPassword, this.context);

            var progress = MainWindow.MainWindowHandle.ShowProgressAsync("Download Funktionäre", "Lade Funktionäre",
                false);

            progress.WaitWithoutBlockingUI(this.dispatcher);

            Task.Factory.StartNew(new Action(() =>
            {
                try
                {
                    apiImporter.ImportOfficials(progress.Result);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                }
                finally
                {
                    progress.Result.CloseAsync();
                }
            }));
        }

        private void AddDtvOfficials()
        {
            var dialog = new AddOfficialsFromDtvView(new AddOfficialsFromDtvViewModel(this.context));
            MainWindow.MainWindowHandle.ShowDialog(dialog, new Action<object>((o) =>
            {
                this.Officials.Clear();
                this.Officials.AddRange(this.context.Officials);
            }), 300,300);
        }

        #endregion
    }
}