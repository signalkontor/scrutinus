﻿// // TPS.net TPS8 Scrutinus
// // MultiSelectionBehavior.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections;
using System.Collections.Specialized;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;

namespace Scrutinus.Behaviours
{
    internal class MultiSelectionBehavior : Behavior<ListView>
    {
        #region Static Fields

        public static readonly DependencyProperty SelectedItemsProperty = DependencyProperty.Register(
            "SelectedItems",
            typeof(IList),
            typeof(MultiSelectionBehavior),
            new UIPropertyMetadata(null, SelectedItemsChanged));

        #endregion

        #region Public Properties

        public IList SelectedItems
        {
            get
            {
                return (IList)this.GetValue(SelectedItemsProperty);
            }
            set
            {
                this.SetValue(SelectedItemsProperty, value);
            }
        }

        #endregion

        #region Fields

        private bool _isUpdatingSource;

        private bool _isUpdatingTarget;

        #endregion

        #region Methods

        protected override void OnAttached()
        {
            base.OnAttached();
            if (this.SelectedItems != null)
            {
                this.AssociatedObject.SelectedItems.Clear();
                foreach (var item in this.SelectedItems)
                {
                    this.AssociatedObject.SelectedItems.Add(item);
                }
            }
        }

        private static void SelectedItemsChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var behavior = o as MultiSelectionBehavior;
            if (behavior == null)
            {
                return;
            }

            var oldValue = e.OldValue as INotifyCollectionChanged;
            var newValue = e.NewValue as INotifyCollectionChanged;

            if (oldValue != null)
            {
                oldValue.CollectionChanged -= behavior.SourceCollectionChanged;
                behavior.AssociatedObject.SelectionChanged -= behavior.ListBoxSelectionChanged;
            }
            if (newValue != null)
            {
                behavior.AssociatedObject.SelectedItems.Clear();
                foreach (var item in (IEnumerable)newValue)
                {
                    behavior.AssociatedObject.SelectedItems.Add(item);
                }

                behavior.AssociatedObject.SelectionChanged += behavior.ListBoxSelectionChanged;
                newValue.CollectionChanged += behavior.SourceCollectionChanged;
            }
        }

        private void ListBoxSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this._isUpdatingTarget)
            {
                return;
            }

            var selectedItems = this.SelectedItems;
            if (selectedItems == null)
            {
                return;
            }

            try
            {
                this._isUpdatingSource = true;

                foreach (var item in e.RemovedItems)
                {
                    selectedItems.Remove(item);
                }

                foreach (var item in e.AddedItems)
                {
                    selectedItems.Add(item);
                }
            }
            finally
            {
                this._isUpdatingSource = false;
            }
        }

        private void SourceCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (this._isUpdatingSource)
            {
                return;
            }

            try
            {
                this._isUpdatingTarget = true;

                if (e.OldItems != null)
                {
                    foreach (var item in e.OldItems)
                    {
                        this.AssociatedObject.SelectedItems.Remove(item);
                    }
                }

                if (e.NewItems != null)
                {
                    foreach (var item in e.NewItems)
                    {
                        this.AssociatedObject.SelectedItems.Add(item);
                    }
                }
            }
            finally
            {
                this._isUpdatingTarget = false;
            }
        }

        #endregion
    }
}