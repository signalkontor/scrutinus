﻿// // TPS.net TPS8 Scrutinus
// // HelpFileExtension.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows;

namespace Scrutinus.Behaviours
{
    public static class HelpFileExtension
    {
        public static readonly DependencyProperty HelpFileProperty = DependencyProperty.RegisterAttached(
            "HelpFile",
            typeof(string),
            typeof(HelpFileExtension),
            new FrameworkPropertyMetadata(null,
                    FrameworkPropertyMetadataOptions.Inherits)
);

        public static void SetHelpFile(DependencyObject element, string value)
        {
            element.SetValue(HelpFileProperty, value);
        }

        public static string GetHelpFile(DependencyObject element)
        {
            return (string) element.GetValue(HelpFileProperty);
        }
    }
}
