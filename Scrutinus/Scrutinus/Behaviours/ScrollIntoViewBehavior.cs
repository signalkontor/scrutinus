﻿// // TPS.net TPS8 Scrutinus
// // ScrollIntoViewBehavior.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Windows.Controls;
using System.Windows.Interactivity;

namespace Scrutinus.Behaviours
{
    public class ScrollIntoViewBehavior : Behavior<DataGrid>
    {
        #region Methods

        protected override void OnAttached()
        {
            base.OnAttached();
            this.AssociatedObject.SelectionChanged += this.AssociatedObject_SelectionChanged;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            this.AssociatedObject.SelectionChanged -= this.AssociatedObject_SelectionChanged;
        }

        private void AssociatedObject_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sender is DataGrid)
            {
                var grid = sender as DataGrid;
                if (grid.SelectedItem != null)
                {
                    grid.Dispatcher.BeginInvoke(
                        new Action(
                            () =>
                                {
                                    grid.UpdateLayout();
                                    grid.ScrollIntoView(grid.SelectedItem, null);
                                }));
                }
            }
        }

        #endregion
    }
}