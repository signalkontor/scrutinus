﻿// // TPS.net TPS8 Scrutinus
// // ComboBoxWatermarkBehavior.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Interactivity;
using System.Windows.Media;

namespace Scrutinus.Behaviours
{
    public class ComboBoxWatermarkBehavior : Behavior<ComboBox>
    {
        // Using a DependencyProperty as the backing store for Text.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string), typeof(ComboBoxWatermarkBehavior), new PropertyMetadata("Watermark"));

        // Using a DependencyProperty as the backing store for FontSize.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty FontSizeProperty =
            DependencyProperty.Register("FontSize", typeof(double), typeof(ComboBoxWatermarkBehavior), new PropertyMetadata(12.0));

        // Using a DependencyProperty as the backing store for Foreground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ForegroundProperty =
            DependencyProperty.Register("Foreground", typeof(Brush), typeof(ComboBoxWatermarkBehavior), new PropertyMetadata(Brushes.Black));

        // Using a DependencyProperty as the backing store for FontFamily.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty FontFamilyProperty =
            DependencyProperty.Register("FontFamily", typeof(string), typeof(ComboBoxWatermarkBehavior), new PropertyMetadata("Segoe UI"));

        private WaterMarkAdorner adorner;

        public string Text
        {
            get { return (string) this.GetValue(TextProperty); }
            set { this.SetValue(TextProperty, value); }
        }


        public double FontSize
        {
            get { return (double) this.GetValue(FontSizeProperty); }
            set { this.SetValue(FontSizeProperty, value); }
        }


        public Brush Foreground
        {
            get { return (Brush) this.GetValue(ForegroundProperty); }
            set { this.SetValue(ForegroundProperty, value); }
        }


        public string FontFamily
        {
            get { return (string) this.GetValue(FontFamilyProperty); }
            set { this.SetValue(FontFamilyProperty, value); }
        }


        protected override void OnAttached()
        {
            this.adorner = new WaterMarkAdorner(this.AssociatedObject, this.Text, this.FontSize, this.FontFamily, this.Foreground);

            this.AssociatedObject.Loaded += this.OnLoaded;
            this.AssociatedObject.GotFocus += this.OnFocus;
            this.AssociatedObject.LostFocus += this.OnLostFocus;
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            if (!this.AssociatedObject.IsFocused)
            {
                if (String.IsNullOrEmpty(this.AssociatedObject.Text))
                {
                    var layer = AdornerLayer.GetAdornerLayer(this.AssociatedObject);
                    layer.Add(this.adorner);
                }
            }
        }

        private void OnLostFocus(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrEmpty(this.AssociatedObject.Text))
            {
                try
                {
                    var layer = AdornerLayer.GetAdornerLayer(this.AssociatedObject);
                    layer.Add(this.adorner);
                }
                catch { }
            }
        }

        private void OnFocus(object sender, RoutedEventArgs e)
        {
            var layer = AdornerLayer.GetAdornerLayer(this.AssociatedObject);
            layer.Remove(this.adorner);
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
        }

        public class WaterMarkAdorner : Adorner
        {
            private readonly string fontFamily;
            private readonly double fontSize;
            private readonly Brush foreground;
            private readonly string text;

            public WaterMarkAdorner(UIElement element, string text, double fontsize, string font, Brush foreground)
                : base(element)
            {
                this.IsHitTestVisible = false;
                this.Opacity = 0.6;
                this.text = text;
                this.fontSize = fontsize;
                this.fontFamily = font;
                this.foreground = foreground;
            }

            protected override void OnRender(DrawingContext drawingContext)
            {
                base.OnRender(drawingContext);
                var text = new FormattedText(
                        this.text,
                        CultureInfo.CurrentCulture,
                        FlowDirection.LeftToRight,
                        new Typeface(this.fontFamily), this.fontSize, this.foreground);

                drawingContext.DrawText(text, new Point(3, 3));
            }
        }
    }
}
