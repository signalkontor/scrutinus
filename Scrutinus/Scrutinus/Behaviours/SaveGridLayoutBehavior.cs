﻿// // TPS.net TPS8 Scrutinus
// // SaveGridLayoutBehavior.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;
using System.Windows.Threading;
using DataModel.Models;

namespace Scrutinus.Behaviours
{
    public class ExtendedTextColumn : DataGridTextColumn, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);

            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(e.Property.Name));
        }
    }

    [Serializable]
    public class GridLayoutData
    {
        public double Width { get; set; }

        public int ColumnIndex { get; set; }
    }

    public class SaveGridLayoutBehavior : Behavior<DataGrid>
    {
        private Dispatcher dispatcher;

        private string gridName;

        private bool layoutLoaded;

        private bool layoutLoading = false;
        private Timer timer;

        protected override void OnAttached()
        {
            this.layoutLoaded = false;

            base.OnAttached();

            this.gridName = this.AssociatedObject.Name;
            this.dispatcher = this.AssociatedObject.Dispatcher;

            this.timer = new Timer(500)
            {
                AutoReset = false,
                Enabled = false
            };

            this.timer.Elapsed += this.ManageLayout;


            this.AssociatedObject.Columns.CollectionChanged += this.ColumnsOnCollectionChanged;

            foreach (var column in this.AssociatedObject.Columns)
            {
                if (column is ExtendedTextColumn)
                {
                    var extended = column as ExtendedTextColumn;
                    extended.PropertyChanged += this.OnPropertyChanged;
                    Debug.WriteLine($"OnAttached: Column {column.Header} attached, {column.DisplayIndex}");
                }
            }
        }

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            if (propertyChangedEventArgs.PropertyName == "ActualWidth")
            {
                var extendedColumn = sender as ExtendedTextColumn;
                Debug.WriteLine($"Width changed: {extendedColumn.Header} {extendedColumn.ActualWidth}");
                if (this.timer.Enabled)
                {
                    this.timer.Stop();
                }
                else
                {
                    this.timer.Enabled = true;
                }

                this.timer.Start();
            }
        }

        private void ColumnsOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs notifyCollectionChangedEventArgs)
        {
            if (notifyCollectionChangedEventArgs.NewItems != null && notifyCollectionChangedEventArgs.NewItems.Count > 0)
            {
                foreach (var column in notifyCollectionChangedEventArgs.NewItems.Cast<DataGridColumn>())
                {
                    if (column is ExtendedTextColumn)
                    {
                        var extended = column as ExtendedTextColumn;
                        extended.PropertyChanged += this.OnPropertyChanged;
                        Debug.WriteLine($"ColumnsCollectionChanged: Column {column.Header} attached, {column.DisplayIndex}");
                    }
                }
            }
        }

        private void ManageLayout(Object source, ElapsedEventArgs e)
        {
            Debug.WriteLine("Save Layout!");
            this.timer.Stop();
            this.timer.Enabled = false;

            if (this.layoutLoaded && !this.layoutLoading)
            {
                this.dispatcher.Invoke(this.SaveLayout);
            }
            else
            {
                this.layoutLoading = true;
                this.dispatcher.Invoke(this.LoadLayout);
                this.layoutLoading = false;
            }
        }

        private void SaveLayout()
        {
            Debug.WriteLine("SaveLayout");
            try
            {
                var layoutList = new List<GridLayoutData>();

                foreach (var column in this.AssociatedObject.Columns)
                {
                    layoutList.Add(new GridLayoutData()
                    {
                        ColumnIndex = column.DisplayIndex,
                        Width = column.ActualWidth
                    });
                }

                using (var context = new ScrutinusContext())
                {
                    context.SetParameter(this.gridName, layoutList);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        private void LoadLayout()
        {
            Debug.WriteLine("LoadLayout");
            try
            {
                using (var context = new ScrutinusContext())
                {
                    var layoutData = context.GetParameter<List<GridLayoutData>>(this.gridName);
                    if (layoutData == null)
                    {
                        this.layoutLoaded = true;
                        return;
                    }

                    foreach (var layout in layoutData)
                    {
                        var column = this.AssociatedObject.Columns.FirstOrDefault(c => c.DisplayIndex == layout.ColumnIndex);
                        if (column != null)
                        {
                            column.Width = new DataGridLength(layout.Width);
                        }
                    }
                }
            }
            finally
            {
                this.layoutLoaded = true;
            }
        }
    }
}
