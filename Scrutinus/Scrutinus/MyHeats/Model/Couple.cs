﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TPS.Heats.Model
{
    public class Couple
    {
        public Couple()
        {
            
        }
        public Couple(DataModel.Models.Couple couple)
        {
            Id = couple.Id;
            Name = couple.NiceName;
            ExternalIdWoman = couple.MINWoman ?? couple.WdsfIdWoman;
            ExternalIdMan = couple.MINMan ?? couple.WdsfIdMan;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        [MaxLength(250)]
        public string Name { get; set; }

        [MaxLength(20)]
        public string ExternalIdMan { get; set; }

        [MaxLength(20)]
        public string ExternalIdWoman { get; set; }
    }
}
