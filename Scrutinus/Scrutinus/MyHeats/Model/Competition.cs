﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using DataModel.ModelHelper;
using DataModel.Models;
using Newtonsoft.Json;

namespace TPS.Heats.Model
{
    public class Competition
    {
        public Competition(ScrutinusContext context, DataModel.Models.Competition competition)
        {
            this.Id = competition.Id;
            this.Name = competition.Title;
            this.Start = competition.StartTime;

            this.Rounds = new List<Round>();

            foreach (var round in competition.Rounds.OrderBy(r => r.Number))
            {
                var heats = HeatsHelper.GetHeats(context, round.Id);

                this.Rounds.Add(new TPS.Heats.Model.Round(round, heats));
            }
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime? Start { get; set; }

        public ICollection<Round> Rounds { get; set; }

    }
}
