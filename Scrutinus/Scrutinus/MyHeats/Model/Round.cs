﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using DataModel.Models;

namespace TPS.Heats.Model
{
    public class Round
    {

        public Round(DataModel.Models.Round round, Dictionary<DanceInRound, List<List<Participant>>> heats)
        {
            this.Id = round.Id;
            this.Name = round.Name;
            // do not serialize competition
            this.Competition = null;
            this.Heats = new List<Heat>();

            foreach (var  danceInRound in heats.Keys)
            {
                var heatsOfDance = heats[danceInRound];

                for (var index = 0;index < heatsOfDance.Count; index++)
                {
                    foreach (var participant in heatsOfDance[index])
                    {
                        this.Heats.Add(new Heat()
                        {
                            Couple = new Couple() { Id = participant.Couple.Id },
                            Dance = danceInRound.Dance.DanceName,
                            HeatIndex = index + 1,
                            DanceIndex = danceInRound.SortOrder
                        });
                    }
                }
            }
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        public string Name { get; set; }

        public Competition Competition { get; set; }

        public ICollection<Heat> Heats { get; set; }

    }
}
