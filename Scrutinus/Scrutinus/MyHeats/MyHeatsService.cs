﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using DataModel.Models;
using General.ApiClient;

namespace Scrutinus.MyHeats
{
    public class PlacingDto
    {
        public PlacingDto(Participant participant)
        {
            this.CompetitionId = participant.Competition.Id;
            this.CoupleId = participant.Couple.Id;
            this.PlaceInCompetition = participant.Place;
        }

        public int CoupleId { get; set; }

        public int CompetitionId { get; set; }

        public string PlaceInCompetition { get; set; }

    }

public class MyHeatsService
    {
        private readonly ScrutinusContext context;

        public MyHeatsService()
        {
            context = new ScrutinusContext();
        }

        public static void UploadData()
        {
            var service = new MyHeatsService();

            service.UploadAllData();
        }

        public void UploadAllData()
        {
            var ids = context.Competitions.Where(c => c.CurrentRound != null).Select(c => c.Id);

            this.UploadCompetitions(ids);

            if (Settings.Default.MyHeatsUploadPlaces)
            {
                this.UploadPlacings();
            }
        }

        public void UploadCouples()
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            MainWindow.MainWindowHandle.ReportStatus("Creating Couple Data", 0, 2);

            var couples = context.Couples.ToList().Select(c => new TPS.Heats.Model.Couple(c));

            Debug.WriteLine($"DTO Couple Data created after {stopwatch.Elapsed}");

            MainWindow.MainWindowHandle.ReportStatus("Sending Data", 1, 2);

            try
            {
                ApiClientBase.PostDataAsync<object>($"{Settings.Default.MyHeatsBaseUrl}/couples", couples, WebRequest.DefaultWebProxy);
            }
            catch (WebException webException)
            {
                ScrutinusContext.WriteLogEntry(webException);
                MainWindow.MainWindowHandle.ReportStatus("", 0, 2);
                return;
            }

            Debug.WriteLine($"DTO Couple Data uploaded after {stopwatch.Elapsed}");
            MainWindow.MainWindowHandle.ReportStatus("", 0, 2);
        }

        public bool UploadCompetitions(IEnumerable<int> competitionIds)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            MainWindow.MainWindowHandle.ReportStatus("Creating Competition Data", 0, 2);

            var list = new List<TPS.Heats.Model.Competition>();

            foreach (var id in competitionIds)
            {
                var competition = context.Competitions.First(c => c.Id == id);

                var competitionDto = new TPS.Heats.Model.Competition(context, competition);

                list.Add(competitionDto);
            }

            Debug.WriteLine($"DTO Data created after {stopwatch.Elapsed}");

            try
            {
                MainWindow.MainWindowHandle.ReportStatus("Sending Competition Data", 1, 2);
                ApiClientBase.PostDataAsync<object>($"{Settings.Default.MyHeatsBaseUrl}/competitions", list, WebRequest.DefaultWebProxy);
            }
            catch (WebException webException)
            {
                ScrutinusContext.WriteLogEntry(webException);
                MainWindow.MainWindowHandle.ReportStatus("", 0, 2);
                return false;
            }

            Debug.WriteLine($"DTO Competitions uploaded after {stopwatch.Elapsed}");
            MainWindow.MainWindowHandle.ReportStatus("", 0, 2);
            return true;
        }

        public bool UploadPlacings()
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            MainWindow.MainWindowHandle.ReportStatus("Creating Placing Data", 0, 2);

            var participantsPlaced = context.Participants
                .Where(p => p.PlaceFrom != null)
                .ToList()
                .Select(p => new PlacingDto(p));

            try
            {
                MainWindow.MainWindowHandle.ReportStatus("Sending Placing Data", 1, 2);
                ApiClientBase.PostDataAsync<object>($"{Settings.Default.MyHeatsBaseUrl}/placings", participantsPlaced, WebRequest.DefaultWebProxy);
            }
            catch (WebException webException)
            {
                ScrutinusContext.WriteLogEntry(webException);
                MainWindow.MainWindowHandle.ReportStatus("", 0, 2);
                return false;
            }

            Debug.WriteLine($"DTO Placings uploaded after {stopwatch.Elapsed}");
            MainWindow.MainWindowHandle.ReportStatus("", 0, 2);

            return true;
        }
    }
}
