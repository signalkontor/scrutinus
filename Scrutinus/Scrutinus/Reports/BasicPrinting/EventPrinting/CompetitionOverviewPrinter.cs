﻿// // TPS.net TPS8 Scrutinus
// // EventStatisticsPrinter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Documents;
using DataModel.Models;
using Scrutinus.Localization;
using Scrutinus.Reports.PrintModels;

namespace Scrutinus.Reports.BasicPrinting.EventPrinting
{
    public class CompetitionOverviewPrinter : GenericTablePrinter<CompetitionStatistic>
    {
        #region Constructors and Destructors

        public CompetitionOverviewPrinter(Event eventData, ScrutinusContext context)
        {
            this.ShowComplexHeader = false;
            this.ShowSimpleHeader = true;

            this.Competition = eventData.Competitions.FirstOrDefault();

            this.RoundTitle = "";
            this.CompetitionTitle = eventData.Title;
            this.DocumentTitle = LocalizationService.Resolve(() => Printing.OverviewOfCompetitions);

            this.printModel = new StatisticPrintModel(context);

            this.CreateContent();
        }

        #endregion

        #region Fields

        private readonly StatisticPrintModel printModel;

        private FixedPage page;

        #endregion

        #region Methods

        protected override void CreateCustomContent()
        {
            this.PrintCompetitions();
        }

        private void PrintCompetitions()
        {
            this.page = this.CreatePage();

            this.dataList = this.printModel.CompetitionStatistics;

            this.columns = new List<ColumnDescriptor<CompetitionStatistic>>
                               {
                                   new ColumnDescriptor
                                       <CompetitionStatistic>
                                       {
                                           Header =
                                               LocalizationService
                                               .Resolve
                                               (
                                                   ()
                                                   =>
                                                   Text
                                                       .Competition),
                                           ValueFunc = d => d.Competition?.Title ?? d.Text,
                                           Width =
                                               new GridLength
                                               (
                                               5,
                                               GridUnitType
                                               .Star)
                                       },
                                   new ColumnDescriptor
                                       <CompetitionStatistic>
                                       {
                                           Header =
                                               LocalizationService
                                                   .Resolve(() => Text.EventFloor),
                                           ValueFunc = d => d.Competition?.Title ?? d.Text,
                                           Width =
                                               new GridLength
                                               (
                                                   5,
                                                   GridUnitType
                                                       .Star)
                                       },
                                   new ColumnDescriptor
                                       <CompetitionStatistic>
                                       {
                                           Header =
                                               LocalizationService
                                               .Resolve
                                               (
                                                   ()
                                                   =>
                                                   Printing
                                                       .CompetitionDateStarted),
                                           ValueFunc
                                               =
                                               d => d.Competition != null && Competition.StartTime.HasValue
                                                   ? d.Competition.StartTime.Value.ToString("g")
                                                   : "",
                                           Width =
                                               new GridLength
                                               (
                                               2,
                                               GridUnitType
                                               .Star)
                                       },
                                   
                                   new ColumnDescriptor
                                       <CompetitionStatistic>
                                       {
                                           Header =
                                               LocalizationService
                                               .Resolve
                                               (
                                                   ()
                                                   =>
                                                   Printing
                                                       .CouplesRegistered),
                                           ValueFunc
                                               =
                                               d =>
                                               d
                                                   .Registered,
                                           Width =
                                               new GridLength
                                               (
                                               1,
                                               GridUnitType
                                               .Star)
                                       },
                                   
                               };

            this.DoCreateHeader = true;

            this.HeaderHeigthInDots += DotsFromCm(0.8);

            this.AddText(
                this.page,
                LocalizationService.Resolve(() => Printing.CouplesStatistic),
                CmFromDots(this.HeaderHeigthInDots),
                this.LeftPageMargin,
                14);

            this.HeaderHeigthInDots += DotsFromCm(0.8);

            double height;

            this.page = TablePrintHelper.PrintTable(
                this.page,
                this.printModel.CompetitionStatistics,
                this.columns,
                this.CreatePage,
                this.DoCreateHeader,
                new Size(this.Doc.DocumentPaginator.PageSize.Width, this.Doc.DocumentPaginator.PageSize.Height - DotsFromCm(1)), 
                this.LeftPageMargin,
                this.RightPageMargin,
                this.HeaderHeigthInDots,
                this.pageHeaderHeight,
                out height);

            this.HeaderHeigthInDots += height;
        }

        private void PrintRegionStatistics()
        {
            this.HeaderHeigthInDots += DotsFromCm(1.2);

            this.AddText(
                this.page,
                LocalizationService.Resolve(() => Printing.RegionsStatistics),
                CmFromDots(this.HeaderHeigthInDots),
                this.LeftPageMargin,
                14);

            this.HeaderHeigthInDots += DotsFromCm(0.8);

            var columns = new List<ColumnDescriptor<IGrouping<string, Participant>>>
                              {
                                  new ColumnDescriptor
                                      <
                                      IGrouping
                                      <string, Participant>>
                                      {
                                          Header
                                              =
                                              LocalizationService
                                              .Resolve
                                              (
                                                  ()
                                                  =>
                                                  Text
                                                      .Region),
                                          ValueFunc
                                              =
                                              d
                                              =>
                                              d
                                                  .Key,
                                          Width
                                              =
                                              new GridLength
                                              (
                                              5,
                                              GridUnitType
                                              .Star)
                                      },
                                  new ColumnDescriptor
                                      <
                                      IGrouping
                                      <string, Participant>>
                                      {
                                          Header
                                              =
                                              LocalizationService
                                              .Resolve
                                              (
                                                  ()
                                                  =>
                                                  Printing
                                                      .Count),
                                          ValueFunc
                                              =
                                              d
                                              =>
                                              d
                                                  .Count
                                                  (
                                                      ),
                                          Width
                                              =
                                              new GridLength
                                              (
                                              5,
                                              GridUnitType
                                              .Star)
                                      }
                              };

            double height;

            this.page = TablePrintHelper.PrintTable(
                this.page,
                this.printModel.CouplesByRegions,
                columns,
                this.CreatePage,
                this.DoCreateHeader,
                new Size(this.Doc.DocumentPaginator.PageSize.Width, this.Doc.DocumentPaginator.PageSize.Height - 1),
                this.LeftPageMargin,
                this.RightPageMargin,
                this.HeaderHeigthInDots,
                this.pageHeaderHeight,
                out height);
        }

        #endregion
    }
}