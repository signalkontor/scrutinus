﻿// // TPS.net TPS8 Scrutinus
// // JudgesWorkPlanPrinter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows.Documents;
using DataModel.Models;
using Scrutinus.Controls;

namespace Scrutinus.Reports.BasicPrinting.EventPrinting
{
    internal class JudgesWorkPlanPrinter : AbstractPrinter
    {
        public JudgesWorkPlanPrinter(Competition competition, bool landscapeMode) : base(competition, landscapeMode)
        {
            this.CompetitionTitle = "";
            this.DocumentTitle = "Work Plan";
            this.ShowComplexHeader = false;
            this.ShowFederationHeader = false;
            this.ShowSimpleHeader = false;


            this.CreateDocument();

            this.CreateCustomContent();

            this.FinalizeDocument();
        }

        protected override void CreateCustomContent()
        {
            var page = this.CreatePage();
            this.AddTitleBar(page);
            var planGrid = new JudgePlanGrid();

            planGrid.Height = DotsFromCm(this.PageHeight) - this.HeaderHeigthInDots - 80;
            planGrid.Width = DotsFromCm(this.PageWidth - this.LeftPageMargin - this.RightPageMargin);

            this.AddControlToPage(page, planGrid);
        }

        protected override void AddCommenContent(FixedPage page)
        {
            
        }
    }
}
