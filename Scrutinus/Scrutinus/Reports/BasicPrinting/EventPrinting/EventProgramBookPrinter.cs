﻿// // TPS.net TPS8 Scrutinus
// // EventProgramBookPrinter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Documents;
using DataModel.Models;
using Scrutinus.Localization;

namespace Scrutinus.Reports.BasicPrinting.EventPrinting
{
    internal class EventProgramBookPrinter : GenericTablePrinter<Participant>
    {
        private FixedPage page;

        internal EventProgramBookPrinter()
        {
            this.ShowComplexHeader = false;
            this.ShowSimpleHeader = false;

            this.DocumentTitle = LocalizationService.Resolve(() => Printing.ProgramBook);

            this.CreateDocument();

            this.page = this.CreatePage();

            this.CreateCustomContent();

            this.FinalizeDocument();
        }

        protected override void CreateCustomContent()
        {

            this.columns = new List<ColumnDescriptor<Participant>>
                               {
                                   new ColumnDescriptor<Participant>()
                                       {
                                           Header = "",
                                           ValueFunc = (p) => p.Number,
                                           Width = new GridLength(40)
                                       },
                                   new ColumnDescriptor<Participant>()
                                       {
                                           Header = "",
                                           ValueFunc = (p) => p.Couple.NiceName,
                                           Width = new GridLength(3, GridUnitType.Star)
                                       },
                                   new ColumnDescriptor<Participant>()
                                       {
                                           Header="",
                                           ValueFunc = (p) => p.Couple.Country,
                                           Width = new GridLength(2, GridUnitType.Star)
                                       }
                               };

            var context = new ScrutinusContext();

            this.PrintCoverPage(context.Events.First(), context);

            foreach (var competition in context.Competitions)
            {
                this.PrintCompetition(competition);
            }
        }

        private void PrintCoverPage(Event ev, ScrutinusContext context)
        {
            var coverPagePrinter = new CoverPagePrinter(ev, context);

            coverPagePrinter.Document = this.Document;

            coverPagePrinter.PrintEventTitle(this.page);
            this.page = coverPagePrinter.PrintCompetitions(this.page);
            this.page = coverPagePrinter.PrintJudges(this.page);
            this.page = coverPagePrinter.PrintOfficials(this.page);

            this.page = this.CreatePage();
        }

        private void PrintCompetition(Competition competition)
        {
            if (this.HeaderHeigthInDots > 980)
            {
                this.page = this.CreatePage();
            }

            this.AddText(
                this.page,
                competition.Title + (competition.StartTime.HasValue ? $" ({competition.StartTime.Value.Date.ToShortDateString()})" : ""),
                CmFromDots(this.HeaderHeigthInDots),
                this.LeftPageMargin,
                16,
                FontWeights.Bold);

            this.HeaderHeigthInDots += DotsFromCm(0.8);

            this.dataList = competition.Participants.OrderBy(p => p.Number).ToList();

            this.page = this.PrintTable(this.page);

            this.HeaderHeigthInDots += DotsFromCm(0.8);
        }
    }
}
