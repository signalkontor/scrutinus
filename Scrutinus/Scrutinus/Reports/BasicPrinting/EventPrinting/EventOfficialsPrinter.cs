﻿// // TPS.net TPS8 Scrutinus
// // OfficialsPrinter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Documents;
using DataModel;
using DataModel.Models;
using Scrutinus.Localization;
using Scrutinus.Reports.Attributes;

namespace Scrutinus.Reports.BasicPrinting.CompetitionPrinting
{
    public class EventOfficialsPrinter : GenericTablePrinter<Official>
    {
        #region Fields

        private FixedPage page;

        #endregion

        #region Constructors and Destructors

        public EventOfficialsPrinter(Event eventData)
        {
            this.ShowComplexHeader = false;
            this.ShowSimpleHeader = true;
            this.PrintFloor = false;

            this.CompetitionTitle = "";
            this.RoundTitle = "";
            this.Competition = eventData.Competitions.FirstOrDefault();
            this.CompetitionTitle = eventData.Title;
            this.DocumentTitle = LocalizationService.Resolve(() => Printing.OfficialsOfEvent);

            this.CreateContent();
        }

        #endregion

        #region Methods

        protected override void CreateCustomContent()
        {
            this.PrintJudges();
            this.PrintOtherOfficials();
        }

        private void PrintJudges()
        {
            this.page = this.CreatePage();
            var context = new ScrutinusContext();
            this.dataList = context.Officials.Where(o => o.Roles.Any(r => r.Id == Roles.Judge)).OrderBy(o => o.Sign)
                .ToList();

            this.columns = new List<ColumnDescriptor<Official>>
                               {
                                   new ColumnDescriptor<Official>
                                       {
                                           Header = "",
                                           ValueFunc = (o) => o.Sign ,
                                           Width =
                                               new GridLength(
                                               1,
                                               GridUnitType.Star)
                                       },
                                   new ColumnDescriptor<Official>
                                       {
                                           Header = "",
                                           ValueFunc = (o) => o.NiceName,
                                           Width =
                                               new GridLength(
                                               3,
                                               GridUnitType.Star)
                                       },
                                   new ColumnDescriptor<Official>
                                   {
                                       Header = "",
                                       ValueFunc = (o) => o.Club,
                                       Width =
                                           new GridLength(
                                               3,
                                               GridUnitType.Star)
                                   },
                                   new ColumnDescriptor<Official>
                                       {
                                           Header = "",
                                           ValueFunc = (o) => o.Nationality?.LongName ?? "",
                                           Width =
                                               new GridLength(
                                               3,
                                               GridUnitType.Star)
                                       },
                               };

            this.DoCreateHeader = false;

            this.HeaderHeigthInDots += DotsFromCm(0.5);

            this.AddText(
                this.page,
                LocalizationService.Resolve(() => Printing.Adjudicator),
                CmFromDots(this.HeaderHeigthInDots),
                this.LeftPageMargin,
                14);

            this.HeaderHeigthInDots += DotsFromCm(0.5);
            this.page = this.PrintTable(this.page);
        }

        private void PrintOtherOfficials()
        {
            this.HeaderHeigthInDots += DotsFromCm(0.5);

            this.AddText(
                this.page,
                LocalizationService.Resolve(() => Printing.OtherOfficials),
                CmFromDots(this.HeaderHeigthInDots),
                this.LeftPageMargin,
                14);

            this.HeaderHeigthInDots += DotsFromCm(0.5);

            this.columns = new List<ColumnDescriptor<Official>>
                               {
                                   new ColumnDescriptor<Official>
                                       {
                                           Header = "",
                                           ValueFunc = (o) => o.Sign,
                                           Width =
                                               new GridLength(
                                               1,
                                               GridUnitType.Star)
                                       },
                                   new ColumnDescriptor<Official>
                                       {
                                           Header = "",
                                           ValueFunc = (o) => o.NiceName,
                                           Width =
                                               new GridLength(
                                               3,
                                               GridUnitType.Star)
                                       },
                                   new ColumnDescriptor<Official>
                                   {
                                       Header = "",
                                       ValueFunc = (o) => o.Club,
                                       Width =
                                           new GridLength(
                                               3,
                                               GridUnitType.Star)
                                   },
                                   new ColumnDescriptor<Official>
                                       {
                                           Header = "",
                                           ValueFunc = (o) => o.Nationality?.LongName ?? "",
                                           Width =
                                               new GridLength(
                                               3,
                                               GridUnitType.Star)
                                       },
                                   new ColumnDescriptor<Official>
                                       {
                                           Header = "",
                                           ValueFunc = (o) => o.RolesAsString,
                                           Width =
                                               new GridLength(
                                               2,
                                               GridUnitType.Star)
                                       }
                               };

            var context = new ScrutinusContext();
            this.dataList = context.Officials.Where(o => o.Roles.Any(r => r.Id != Roles.Judge)).ToList().OrderBy(o => o.RolesAsString).ThenBy(o => o.Sign).ToList();

            this.page = this.PrintTable(this.page);
        }

        #endregion
    }
}