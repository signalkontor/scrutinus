﻿// // TPS.net TPS8 Scrutinus
// // WdsfChecksumsEventPrinter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Documents;
using DataModel.Models;
using Helpers.Checksum;
using Scrutinus.Reports.Attributes;
using Scrutinus.Reports.BasicPrinting.RoundPrinting;

namespace Scrutinus.Reports.BasicPrinting.EventPrinting
{

    [CanPrintAfterQualificationRound(true)]
    [CanPrintAfterFinalRound(true)]
    public class WdsfChecksumsEventPrinter : GenericTablePrinter<ChecksumPrintModel>
    {
        private readonly IEnumerable<Competition> competitions;

        private readonly IEnumerable<Official> judges;

        private FixedPage page = null;

        public WdsfChecksumsEventPrinter(IEnumerable<Competition> competitions, IEnumerable<Official> judges)
        {
            this.DocumentTitle = "Wdsf Checksum of Round ";
            this.ShowComplexHeader = false;
            this.ShowSimpleHeader = false;
            this.competitions = competitions;
            this.judges = judges;
            this.CreateContent();
        }

        protected override void CreateCustomContent()
        {
            foreach (var official in this.judges)
            {
                var checksums = this.CreateChecksums(official);

                if (checksums.Any())
                {
                    this.PrintChecksumForJudge(checksums, official.NiceName);
                }
            }
        }

        private  void PrintChecksumForJudge(IList<ChecksumPrintModel> checksums, string judgeName)
        {
            this.dataList = checksums;

            this.page = this.CreatePage();
            this.DocumentTitle = "Event Checksums for " + judgeName;

            this.TableRowHeight = new GridLength(40);

            this.columns = new List<ColumnDescriptor<ChecksumPrintModel>>()
                               {
                                   new ColumnDescriptor<ChecksumPrintModel>()
                                       {
                                           Header = "Judge",
                                           Width = new GridLength(1, GridUnitType.Star),
                                           ValueFunc = p => p.Official.NiceName,
                                           FontSize = 16
                                       },
                                       new ColumnDescriptor<ChecksumPrintModel>()
                                       {
                                           Header = "Round",
                                           Width = new GridLength(1, GridUnitType.Star),
                                           ValueFunc = p => p.CompetitionName + " / " + p.RoundName,
                                           FontSize = 16
                                       },
                                   new ColumnDescriptor<ChecksumPrintModel>()
                                       {
                                           FontSize = 16,
                                           Header = "Checksum",
                                           ValueFunc = p => p.Checksum,
                                           Width = GridLength.Auto
                                       }
                               };
            
            this.TableRowHeight = new GridLength(60);

            this.page = this.PrintTable(this.page);
        }

        private IList<ChecksumPrintModel> CreateChecksums(Official judge)
        {
            var rounds =
                this.competitions.Where(c => c.Officials.Any(o => o.Official.Id == judge.Id))
                    .SelectMany(c => c.Rounds)
                    .OrderBy(r => r.Competition.StartTime)
                    .ThenBy(r => r.Number);

            return rounds.Select(r => new ChecksumPrintModel() { Official = judge, Checksum = ChecksumHelper.CalculateChecksum(r, judge), RoundName = r.Name, CompetitionName = r.Competition.Title}).ToList();
        }
    }
}
