﻿// // TPS.net TPS8 Scrutinus
// // EventStartNumberOverviewPrinter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Documents;
using DataModel.Models;
using Scrutinus.Localization;

namespace Scrutinus.Reports.BasicPrinting.EventPrinting
{
    public class EventStartNumberOverviewPrinter : GenericTablePrinter<Competition>
    {
        private readonly IList<Competition> competitions;
        private FixedPage page;

        public EventStartNumberOverviewPrinter(IList<Competition> competitions)
        {
            this.competitions = competitions;
            this.ShowComplexHeader = false;
            this.ShowSimpleHeader = false;

            this.DocumentTitle = LocalizationService.Resolve(() => Printing.StartnumberOverview);

            this.CreateDocument();

            this.CreateCustomContent();

            this.FinalizeDocument();
        }

        protected override void CreateCustomContent()
        {
            this.dataList = this.competitions;

            this.columns = new List<ColumnDescriptor<Competition>>
            {
                new ColumnDescriptor<Competition>()
                        {
                            Header = LocalizationService.Resolve(() => Printing.Competition),
                            ValueFunc = (c) => c.Title, 
                            Width = new GridLength(3, GridUnitType.Star),
                        },
                new ColumnDescriptor<Competition>()
                        {
                            Header = LocalizationService.Resolve(() => Printing.CompetitionStartTime),
                            ValueFunc = (c) => c.StartTime.HasValue ? c.StartTime.Value.ToString("G") : "",
                            Width = new GridLength(2, GridUnitType.Star),
                        },
                new ColumnDescriptor<Competition>()
                        {
                            Header = LocalizationService.Resolve(() => Printing.CouplesRegistered),
                            ValueFunc = (c) => c.Participants.Count.ToString(),
                            Width = new GridLength(1, GridUnitType.Star),
                        },
                new ColumnDescriptor<Competition>()
                        {
                            Header = LocalizationService.Resolve(() => Printing.FirstNumber),
                            ValueFunc = (c) => c.Participants.Min(p => p.Number).ToString(),
                            Width = new GridLength(1, GridUnitType.Star),
                        },
                new ColumnDescriptor<Competition>()
                        {
                            Header = LocalizationService.Resolve(() => Printing.LastNumber),
                            ValueFunc = (c) => c.Participants.Max(p => p.Number).ToString(),
                            Width = new GridLength(1, GridUnitType.Star),
                        },
            };

            this.DoCreateHeader = true;

            this.page = this.CreatePage();

            this.AddTitleBar(this.page);

            this.HeaderHeigthInDots += DotsFromCm(0.5);

            this.page = this.PrintTable(this.page);
        }
    }
}
