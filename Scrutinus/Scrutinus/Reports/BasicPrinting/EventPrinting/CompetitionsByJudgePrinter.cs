﻿// // TPS.net TPS8 Scrutinus
// // CompetitionsByJudgePrinter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Documents;
using DataModel;
using DataModel.Models;
using Scrutinus.Localization;

namespace Scrutinus.Reports.BasicPrinting.EventPrinting
{
    public class CompetitionsByJudgePrinter : GenericTablePrinter<Competition>
    {
        private readonly ScrutinusContext context;

        private FixedPage page;

        public CompetitionsByJudgePrinter(ScrutinusContext context)
        {
            this.context = context;

            this.ShowComplexHeader = false;
            this.ShowSimpleHeader = false;

            this.CreateDocument();

            this.CreateCustomContent();

            this.FinalizeDocument();
        }

        protected override void CreateCustomContent()
        {
            foreach (var judge in this.context.Officials.Where(o => o.Roles.Any(r => r.Id == Roles.Judge)))
            {
                this.dataList = judge.OfficialCompetition.Select(o => o.Competition).ToList();
                this.DocumentTitle = LocalizationService.Resolve(() => Printing.CompetitionOfJudge) + " " + judge.NiceName;

                this.PrintCompetitions();
            }
        }

        private void PrintCompetitions()
        {
            this.columns = new List<ColumnDescriptor<Competition>>()
            {
                new ColumnDescriptor<Competition>()
                {
                    FontSize = 16,
                    ValueFunc = (c) => c.Title,
                    Header = "",
                    Width = new GridLength(4, GridUnitType.Star)
                },
                new ColumnDescriptor<Competition>()
                {
                    FontSize = 16,
                    ValueFunc = (c) => c.StartTime.HasValue ? c.StartTime.Value.ToShortDateString() + " " + c.StartTime.Value.ToShortTimeString() : "",
                    Header = "",
                    Width = new GridLength(1, GridUnitType.Star)
                }
            };

            this.DoCreateHeader = false;
            this.page = this.CreatePage();
            this.AddTitleBar(this.page);
            this.PrintTable(this.page);
        }
    }
}
