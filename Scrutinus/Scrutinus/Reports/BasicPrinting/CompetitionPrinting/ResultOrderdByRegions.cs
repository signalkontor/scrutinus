﻿// // TPS.net TPS8 Scrutinus
// // ResultOrderdByRegions.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Documents;
using DataModel;
using DataModel.Models;
using Scrutinus.Localization;
using Scrutinus.Reports.Attributes;
using Scrutinus.Reports.BasicPrinting.RoundPrinting;

namespace Scrutinus.Reports.BasicPrinting.CompetitionPrinting
{
    [CanPrintAfterFinalRound(true)]
    public class ResultOrderedByRegions : GenericTablePrinter<CouplesResultPrintModel>
    {
        #region Fields

        private FixedPage page;

        #endregion

        #region Constructors and Destructors

        public ResultOrderedByRegions(Competition competition)
        {
            this.ShowComplexHeader = false;
            this.ShowSimpleHeader = true;
            this.Competition = competition;

            this.CompetitionTitle = competition.Title;
            this.RoundTitle = "";

            this.DocumentTitle = LocalizationService.Resolve(() => Printing.ResultOrderedByRegions);

            this.CreateContent();
        }

        #endregion

        #region Methods

        protected override void CreateCustomContent()
        {
            this.CreateCustomContentCompetition();
        }

        private void CreateCustomContentCompetition()
        {
            // Result ordered by Regions and then by place
            var lists =
                this.Competition.Participants.Where(c => c.PlaceFrom.HasValue)
                    .OrderBy(p => p.Couple.Region)
                    .ThenBy(p => p.PlaceFrom)
                    .Select(
                        p =>
                        new CouplesResultPrintModel
                            {
                                Participant = p,
                                Star = p.StarString,
                                RankingPoints = p.RankingPoints.ToString(),
                                Number = p.Number,
                                Place = p.Place,
                                StateString = p.StateString,
                                LastLastRoundQualified = p.QualifiedRound,
                            })
                    .ToList()
                    .GroupBy(p => p.Participant.Couple.Region)
                    .ToList();

            // Calculate the Places within the regions
            foreach (var group in lists)
            {
                var place = 1 + this.Competition.Participants.Count(p => p.Couple.Region == group.Key && p.State == CoupleState.Dancing);
                CouplesResultPrintModel lastPrintModel = null;
                foreach (var printModel in group)
                {
                    if (lastPrintModel != null && lastPrintModel.Place == printModel.Place)
                    {
                        printModel.PlaceInRegion = lastPrintModel.PlaceInRegion;
                    }
                    else
                    {
                        printModel.PlaceInRegion = place + ".";
                    }
                    place++;
                    lastPrintModel = printModel;
                }
            }

            this.columns = new List<ColumnDescriptor<CouplesResultPrintModel>>
                               {
                                   new ColumnDescriptor
                                       <CouplesResultPrintModel>
                                       {
                                           Header
                                               =
                                               "",
                                           ValueFunc
                                               =
                                               p
                                               =>
                                               p
                                                   .Star,
                                           Width
                                               =
                                               new GridLength
                                               (
                                               40,
                                               GridUnitType
                                               .Pixel)
                                       },
                                   new ColumnDescriptor
                                       <CouplesResultPrintModel>
                                       {
                                           Header
                                               =
                                               LocalizationService
                                               .Resolve
                                               (
                                                   ()
                                                   =>
                                                   Printing
                                                       .Number),
                                           ValueFunc
                                               =
                                               p
                                               =>
                                               p
                                                   .Participant
                                                   .Number,
                                           Width
                                               =
                                               new GridLength
                                               (
                                               1,
                                               GridUnitType
                                               .Star)
                                       },
                                   new ColumnDescriptor
                                       <CouplesResultPrintModel>
                                       {
                                           Header
                                               =
                                               LocalizationService
                                               .Resolve
                                               (
                                                   ()
                                                   =>
                                                   Text
                                                       .CoupleName),
                                           ValueFunc
                                               =
                                               p
                                               =>
                                               p
                                                   .Participant
                                                   .Couple
                                                   .NiceName,
                                           Width
                                               =
                                               new GridLength
                                               (
                                               3,
                                               GridUnitType
                                               .Star)
                                       },
                                   new ColumnDescriptor
                                       <CouplesResultPrintModel>
                                       {
                                           Header
                                               =
                                               LocalizationService
                                               .Resolve
                                               (
                                                   ()
                                                   =>
                                                   Printing
                                                       .ClubCountry),
                                           ValueFunc
                                               =
                                               p
                                               =>
                                               p
                                                   .Participant
                                                   .Couple
                                                   .Country,
                                           Width
                                               =
                                               new GridLength
                                               (
                                               2,
                                               GridUnitType
                                               .Star)
                                       },
                                   new ColumnDescriptor
                                       <CouplesResultPrintModel>
                                       {
                                           Header
                                               =
                                               LocalizationService
                                               .Resolve
                                               (
                                                   ()
                                                   =>
                                                   Printing
                                                       .Region),
                                           ValueFunc
                                               =
                                               p
                                               =>
                                               p
                                                   .Participant
                                                   .Couple
                                                   .Region,
                                           Width
                                               =
                                               new GridLength
                                               (
                                               2,
                                               GridUnitType
                                               .Star)
                                       },
                                   new ColumnDescriptor
                                       <CouplesResultPrintModel>
                                       {
                                           Header
                                               =
                                               LocalizationService
                                               .Resolve
                                               (
                                                   ()
                                                   =>
                                                   Printing
                                                       .PlaceInRegion),
                                           ValueFunc
                                               =
                                               p
                                               =>
                                               p
                                                   .PlaceInRegion,
                                           Width
                                               =
                                               new GridLength
                                               (
                                               1,
                                               GridUnitType
                                               .Star)
                                       },
                                   new ColumnDescriptor
                                       <CouplesResultPrintModel>
                                       {
                                           Header
                                               =
                                               LocalizationService
                                               .Resolve
                                               (
                                                   ()
                                                   =>
                                                   Printing
                                                       .PlaceInCompetition),
                                           ValueFunc
                                               =
                                               p
                                               =>
                                               p
                                                   .Place,
                                           Width
                                               =
                                               new GridLength
                                               (
                                               1,
                                               GridUnitType
                                               .Star)
                                       }
                               };

            this.DoCreateHeader = true;

            this.page = this.CreatePage();

            this.HeaderHeigthInDots += DotsFromCm(0.3);

            foreach (var group in lists.Where(g => g.Key != null))
            {
                if (CmFromDots(this.HeaderHeigthInDots) > 25)
                {
                    this.page = this.CreatePage();
                }
                // we add the name of this Region:
                this.AddText(this.page, group.Key, CmFromDots(this.HeaderHeigthInDots), this.LeftPageMargin, 14);
                this.HeaderHeigthInDots += DotsFromCm(0.5);

                this.dataList = new List<CouplesResultPrintModel>();
                foreach (var couplesResultPrintModel in group)
                {
                    this.dataList.Add(couplesResultPrintModel);
                }



                this.page = this.PrintTable(this.page);

                this.HeaderHeigthInDots += DotsFromCm(0.3);
            }
        }

        #endregion
    }
}