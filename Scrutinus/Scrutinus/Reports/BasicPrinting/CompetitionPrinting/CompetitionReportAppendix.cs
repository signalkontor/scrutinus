﻿// // TPS.net TPS8 Scrutinus
// // CompetitionReportAppendix.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using DataModel;
using DataModel.Models;
using Scrutinus.Localization;
using Scrutinus.Reports.Attributes;
using Scrutinus.Reports.BasicPrinting.Controls;

namespace Scrutinus.Reports.BasicPrinting.CompetitionPrinting
{
    [CanPrintAfterFinalRound(true)]
    public class CompetitionReportAppendix : TablePrinter
    {
        #region Fields

        private FixedPage page;

        #endregion

        #region Constructors and Destructors

        public CompetitionReportAppendix(Competition competition)
        {
            this.ShowComplexHeader = true;
            this.ShowSimpleHeader = false;
            this.Competition = competition;
            this.DocumentTitle = LocalizationService.Resolve(() => Printing.CompetitionReport);

            this.CreateContent();
        }

        #endregion

        #region Methods

        protected override void CreateCustomContent()
        {
            this.CreateReportAppendix();
        }

        private void CreateClimbUps()
        {
            this.AddText(this.page, Printing.ClimbUp, CmFromDots(this.HeaderHeigthInDots), this.LeftPageMargin, 12);

            this.HeaderHeigthInDots += 18;

            this.list = this.Competition.ClimbUps.ToList();

            this.columns = new List<ColumnDescriptor>
                               {
                                   new ColumnDescriptor
                                       {
                                           Header = Printing.Couple,
                                           Path = "Couple.NiceName",
                                           Width =
                                               new GridLength(
                                               3,
                                               GridUnitType.Star)
                                       },
                                   new ColumnDescriptor
                                       {
                                           Header = Printing.Club,
                                           Path = "Couple.Country",
                                           Width =
                                               new GridLength(
                                               3,
                                               GridUnitType.Star)
                                       },
                                   new ColumnDescriptor
                                       {
                                           Header = Printing.Region,
                                           Path = "Couple.Region",
                                           Width =
                                               new GridLength(
                                               1,
                                               GridUnitType.Star)
                                       },
                                   new ColumnDescriptor
                                       {
                                           Header = "nach",
                                           Path = "NewClass.ClassLongName",
                                           Width =
                                               new GridLength(
                                               3,
                                               GridUnitType.Star)
                                       },
                               };

            this.page = this.PrintTable(this.page);

            this.HeaderHeigthInDots += 10;

            this.InsertVerticalLine(this.page);

            this.HeaderHeigthInDots += 10;
        }

        private void CreateCompetitionOverview()
        {
            var typedList = new List<Tuple<string, string>>
                                {
                                    new Tuple<string, string>(Printing.CompetitionType, "Einzel"),
                                    new Tuple<string, string>(
                                        Printing.CompetitionType,
                                        this.Competition.CompetitionType.ShortName),
                                    new Tuple<string, string>(
                                        Printing.AdmissionAllowance,
                                        this.Competition.CompetitionType.Federation),
                                    new Tuple<string, string>(
                                        Printing.GroupClass,
                                        this.Competition.AgeGroup.AgeGroupName),
                                    new Tuple<string, string>(
                                        Printing.Section,
                                        this.Competition.Section.SectionName)
                                };
            this.list = typedList;

            this.columns = new List<ColumnDescriptor>
                               {
                                   new ColumnDescriptor
                                       {
                                           Header = "Sign",
                                           Path = "Item1",
                                           Width =
                                               new GridLength(
                                               2,
                                               GridUnitType.Star)
                                       },
                                   new ColumnDescriptor
                                       {
                                           Header = "Judge",
                                           Path = "Item2",
                                           Width =
                                               new GridLength(
                                               3,
                                               GridUnitType.Star)
                                       },
                               };

            this.DoCreateTableHeaders = false;

            this.page = this.PrintTable(this.page);

            this.HeaderHeigthInDots += 10;

            this.InsertVerticalLine(this.page);

            this.HeaderHeigthInDots += 10;
        }

        private void CreateFinalResult()
        {
            this.AddText(this.page, Printing.ResultFinal, CmFromDots(this.HeaderHeigthInDots), this.LeftPageMargin, 12);

            this.HeaderHeigthInDots += 18;

            var finalRound = this.Competition.Rounds.SingleOrDefault(r => r.RoundType.Id == RoundTypes.Final);
            if (finalRound == null)
            {
                return;
            }

            this.list = finalRound.Qualifieds.OrderBy(p => p.Participant.PlaceFrom).ToList();

            this.columns = new List<ColumnDescriptor>
                               {
                                   new ColumnDescriptor
                                       {
                                           Header = Printing.PlaceInCompetition,
                                           Path = "Participant.Place",
                                           Width =
                                               new GridLength(
                                               2,
                                               GridUnitType.Star)
                                       },
                                   new ColumnDescriptor
                                       {
                                           Header = Printing.Couple,
                                           Path = "Participant.Couple.NiceName",
                                           Width =
                                               new GridLength(
                                               3,
                                               GridUnitType.Star)
                                       },
                                   new ColumnDescriptor
                                       {
                                           Header = Printing.Club,
                                           Path = "Participant.Couple.Country",
                                           Width =
                                               new GridLength(
                                               3,
                                               GridUnitType.Star)
                                       },
                               };
            this.DoCreateTableHeaders = true;

            this.page = this.PrintTable(this.page);

            this.HeaderHeigthInDots += 10;

            this.InsertVerticalLine(this.page);

            this.HeaderHeigthInDots += 10;
        }

        private void CreateNumberOfCouples()
        {
            if (!this.Competition.StartTime.HasValue)
            {
                this.Competition.StartTime = DateTime.Now;
            }

            if (!this.Competition.EndTime.HasValue)
            {
                this.Competition.EndTime = DateTime.Now;
            }

            var typedList = new List<Tuple<string, string>>
                                {
                                    new Tuple<string, string>(
                                        Printing.CompetitionDateStarted,
                                        this.Competition.StartTime.Value.ToShortTimeString()),
                                    new Tuple<string, string>(
                                        Printing.CompetitionDateEnded,
                                        this.Competition.EndTime.Value.ToShortTimeString()),
                                };

            foreach (var round in this.Competition.Rounds.OrderBy(r => r.Number))
            {
                typedList.Add(new Tuple<string, string>(round.Name, round.Qualifieds.Count.ToString()));
            }

            this.list = typedList;

            this.DoCreateTableHeaders = false;

            this.page = this.PrintTable(this.page);

            this.HeaderHeigthInDots += 10;

            this.InsertVerticalLine(this.page);
        }

        private void CreateOfficials()
        {
            this.list = this.Competition.Officials.OrderBy(o => o.Role.Id).ToList();

            this.AddText(this.page, Printing.Officials, CmFromDots(this.HeaderHeigthInDots), this.LeftPageMargin, 12);

            this.HeaderHeigthInDots += 18;

            this.columns = new List<ColumnDescriptor>
                               {
                                   new ColumnDescriptor
                                       {
                                           Header = Printing.Role,
                                           Path = "Role.RoleLong",
                                           Width =
                                               new GridLength(
                                               2,
                                               GridUnitType.Star)
                                       },
                                   new ColumnDescriptor
                                       {
                                           Header = Printing.Official,
                                           Path = "Official.NiceName",
                                           Width =
                                               new GridLength(
                                               3,
                                               GridUnitType.Star)
                                       },
                                   new ColumnDescriptor
                                       {
                                           Header = Printing.Club,
                                           Path = "Official.Country",
                                           Width =
                                               new GridLength(
                                               3,
                                               GridUnitType.Star)
                                       },
                                   new ColumnDescriptor
                                       {
                                           Header = Printing.Region,
                                           Path = "Official.Region",
                                           Width =
                                               new GridLength(
                                               1,
                                               GridUnitType.Star)
                                       },
                               };

            this.page = this.PrintTable(this.page);

            this.HeaderHeigthInDots += 10;

            this.InsertVerticalLine(this.page);

            this.HeaderHeigthInDots += 10;
        }

        private void CreateReportAppendix()
        {
            this.page = this.CreatePage();

            this.CreateCompetitionOverview();

            this.CreateNumberOfCouples();

            this.CreateOfficials();

            this.CreateSpecialRemarks();

            this.CreateFinalResult();

            if (this.Competition.ClimbUps.Count > 0)
            {
                this.CreateClimbUps();
            }

            this.CreateSignatureFields();
        }

        private void CreateSignatureFields()
        {
            var signatures = new SignatureChairpersonScrutineer() { Width = this.UseablePageWidthInDots };

            this.page = this.AddControlToPage(this.page, signatures);
        }

        private void CreateSpecialRemarks()
        {
            var stack = new StackPanel { Orientation = Orientation.Horizontal };

            stack.Children.Add(new TextBlock { Text = Printing.NoteableEvents });
            stack.Children.Add(
                new TextBlock
                    {
                        Text = this.Competition.Report,
                        TextWrapping = TextWrapping.Wrap,
                        Height = Double.NaN,
                        Margin = new Thickness(20, 0, 0, 0)
                    });

            stack.Width = DotsFromCm(this.PageWidth) - DotsFromCm(this.LeftPageMargin)
                          - DotsFromCm(this.RightPageMargin);

            this.page = this.AddControlToPage(this.page, stack);

            this.HeaderHeigthInDots += 10;

            this.InsertVerticalLine(this.page);

            this.HeaderHeigthInDots += 10;
        }

        #endregion
    }
}