﻿// // TPS.net TPS8 Scrutinus
// // RegisteredCouplesPrinter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Documents;
using DataModel;
using DataModel.Models;
using Scrutinus.Localization;
using Scrutinus.Reports.Attributes;

namespace Scrutinus.Reports.BasicPrinting.CompetitionPrinting
{
    [CanPrintBeforeFirstRound(true)]
    [CanPrintBeforeQualificationRound(true)]
    [CanPrintBeforeFinalRound(true)]
    public class RegisteredCouplesPrinter : GenericTablePrinter<Participant>
    {
        #region Fields

        private FixedPage page;

        #endregion

        #region Constructors and Destructors

        public RegisteredCouplesPrinter(IEnumerable<Competition> competitions)
        {
            this.ShowComplexHeader = false;
            this.ShowSimpleHeader = true;

            this.CreateDocument();

            foreach (var competition in competitions)
            {
                this.Competition = competition;

                this.CompetitionTitle = competition.Title;
                this.RoundTitle = "";

                if (competition.Section != null && competition.Section.IsTeam)
                {
                    this.DocumentTitle = LocalizationService.Resolve(() => Printing.RegisteredTeams);
                }
                else
                {
                    this.DocumentTitle = LocalizationService.Resolve(() => Printing.RegisteredCouples);
                }

                this.CreateCustomContent();
            }

            this.FinalizeDocument();
        }

        #endregion

        #region Methods

        protected override void CreateCustomContent()
        {
            this.dataList = this.Competition.Participants.OrderBy(p => p.Number).ToList();

            this.columns = new List<ColumnDescriptor<Participant>>
                               {
                                   new ColumnDescriptor<Participant>
                                       {
                                           Header = "",
                                           ValueFunc =
                                               p =>
                                               p
                                                   .StarString,
                                           Width =
                                               new GridLength
                                               (
                                               1,
                                               GridUnitType
                                               .Auto)
                                       },
                                   new ColumnDescriptor<Participant>
                                       {
                                           Header = "",
                                           ValueFunc =
                                               p =>
                                               p.Number,
                                           Width =
                                               new GridLength
                                               (
                                               1,
                                               GridUnitType
                                               .Star)
                                       },
                                   new ColumnDescriptor<Participant>
                                       {
                                           Header = "",
                                           ValueFunc =
                                               p =>
                                               p.Couple
                                                   .NiceName,
                                           Width =
                                               new GridLength
                                               (
                                               5,
                                               GridUnitType
                                               .Star)
                                       },
                                   new ColumnDescriptor<Participant>
                                       {
                                           Header = "",
                                           ValueFunc =
                                               p =>
                                               p.Couple
                                                   .Country,
                                           Width =
                                               new GridLength
                                               (
                                               2,
                                               GridUnitType
                                               .Star)
                                       },
                                    new ColumnDescriptor<Participant>
                                       {
                                           Header = "",
                                           ValueFunc = p => p.Couple.Region,
                                           Width =
                                               new GridLength
                                               (
                                               1,
                                               GridUnitType
                                               .Star)
                                       },
                                   new ColumnDescriptor<Participant>
                                       {
                                           Header = "",
                                           ValueFunc =
                                               p =>
                                               p
                                                   .StateString,
                                           Width =
                                               new GridLength
                                               (
                                               2,
                                               GridUnitType
                                               .Star)
                                       },
                               };
            this.DoCreateHeader = false;

            this.HeaderHeigthInDots += DotsFromCm(0.5) + 30;

            this.page = this.CreatePage();

            var firstPage = this.page;

            this.page = this.PrintTable(this.page);

            if (this.Doc.Pages.Any())
            {
                this.AddText(
                    firstPage,
                    string.Format(
                        LocalizationService.Resolve(() => Text.NumberCouples),
                        this.Competition.Participants.Count,
                        this.Competition.Participants.Count(p => p.State == CoupleState.Dancing)),
                    2,
                    this.LeftPageMargin,
                    12);
            }
        }

        #endregion
    }
}