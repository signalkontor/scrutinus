﻿// // TPS.net TPS8 Scrutinus
// // CorrectedStartListPrinter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Documents;
using DataModel;
using DataModel.Models;
using Scrutinus.Localization;
using Scrutinus.Reports.Attributes;

namespace Scrutinus.Reports.BasicPrinting.CompetitionPrinting
{
    [CanPrintBeforeFirstRound(true)]
    public class CorrectedStartListPrinter : GenericTablePrinter<Participant>
    {
        #region Fields

        private FixedPage page;

        #endregion

        #region Constructors and Destructors

        public CorrectedStartListPrinter(IEnumerable<Competition> competitions)
        {
            this.ShowComplexHeader = false;
            this.ShowSimpleHeader = true;

            this.CreateDocument();

            foreach (var competition in competitions)
            {
                this.Competition = competition;

                this.CompetitionTitle = competition.Title;
                this.RoundTitle = string.Format(Printing.NumberOfParticipants, competition.Participants.Count(p => p.State == CoupleState.Dancing));

                if (competition.Section.IsTeam)
                {
                    this.DocumentTitle = LocalizationService.Resolve(() => Printing.RegisteredTeams);
                }
                else
                {
                    this.DocumentTitle = LocalizationService.Resolve(() => Printing.CorrectedStartList);
                }

                this.CreateCustomContent();
            }

            this.FinalizeDocument();
        }

        #endregion

        #region Methods

        protected override void CreateCustomContent()
        {
            // 
            this.dataList =
                this.Competition.Participants.Where(
                    p => p.State != CoupleState.Excused && p.State != CoupleState.Missing)
                    .OrderBy(p => p.Number)
                    .ToList();

            this.columns = new List<ColumnDescriptor<Participant>>
                               {
                                   new ColumnDescriptor<Participant>
                                       {
                                           Header = "",
                                           ValueFunc =
                                               p =>
                                               p
                                                   .StarString,
                                           Width =
                                               new GridLength
                                               (
                                               1,
                                               GridUnitType
                                               .Auto)
                                       },
                                   new ColumnDescriptor<Participant>
                                       {
                                           Header =
                                               LocalizationService
                                               .Resolve
                                               (
                                                   ()
                                                   =>
                                                   Printing
                                                       .NumberShort),
                                           ValueFunc =
                                               p =>
                                               p
                                                   .Number,
                                           Width =
                                               new GridLength
                                               (
                                               40,
                                               GridUnitType
                                               .Pixel)
                                       },
                                   new ColumnDescriptor<Participant>
                                       {
                                           Header =
                                               LocalizationService
                                               .Resolve
                                               (
                                                   ()
                                                   =>
                                                   Printing
                                                       .Name),
                                           ValueFunc =
                                               p =>
                                               p
                                                   .Couple
                                                   .NiceName
                                               + "\r\n"
                                               + p
                                                     .Couple
                                                     .Country,
                                           Width =
                                               new GridLength
                                               (
                                               3,
                                               GridUnitType
                                               .Star)
                                       },
                                   new ColumnDescriptor<Participant>()
                                   {
                                       Header = LocalizationService.Resolve(() => Printing.Region),
                                       ValueFunc = p => p.Couple.Region,
                                       Width = new GridLength(1, GridUnitType.Star)
                                   },
#if DTV
                                    new ColumnDescriptor<Participant>()
                                       {
                                           Header = "Laufzettel",
                                           ValueFunc = p => p.Startbuch != null ? (p.Startbuch.PrintLaufzettel ? "LZ" : "") : "",
                                           Width = new GridLength(1, GridUnitType.Auto)
                                       },
                                    new ColumnDescriptor<Participant>()
                                       {
                                           Header = "Punkte",
                                           ValueFunc = p => p.OriginalPoints != null && p.Startbuch != null ? p.OriginalPoints + " / " + p.Startbuch.TargetPoints : "",
                                           Width = new GridLength(1, GridUnitType.Auto)
                                       },
                                    new ColumnDescriptor<Participant>()
                                       {
                                           Header = "Platzierungen",
                                           ValueFunc = p => p.OriginalPlacings != null && p.Startbuch != null ? p.OriginalPlacings + " / " + p.Startbuch.TargetPlacings : "",
                                           Width = new GridLength(1, GridUnitType.Auto)
                                       },
                                    new ColumnDescriptor<Participant>()
                                       {
                                           Header = "Schrittbegr.",
                                           ValueFunc = p => p.Startbuch != null ? (p.Startbuch.Sylabus ? "W" : "") : "",
                                           Width = new GridLength(1, GridUnitType.Auto)
                                       },
#endif
                               };
            this.DoCreateHeader = false;
#if DTV
            this.DoCreateHeader = true;
#endif
            this.page = this.CreatePage();

            this.HeaderHeigthInDots += DotsFromCm(0.5);

            this.page = this.PrintTable(this.page);
        }

        #endregion
    }
}