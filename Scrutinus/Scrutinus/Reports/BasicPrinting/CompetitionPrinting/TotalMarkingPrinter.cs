﻿// // TPS.net TPS8 Scrutinus
// // TotalMarkingPrinter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using DataModel;
using DataModel.Models;
using Scrutinus.Localization;
using Scrutinus.Reports.Attributes;

namespace Scrutinus.Reports.BasicPrinting.CompetitionPrinting
{
    [CanPrintAfterFinalRound(true)]
    public class TotalMarkingPrinter : AbstractPrinter
    {
        private double fontSize = 14;

        #region Constructors and Destructors

        public TotalMarkingPrinter(Competition competition, ScrutinusContext context)
            : base(competition)
        {
            this.ShowComplexHeader = true;
            this.ShowSimpleHeader = false;

            this.context = context;

            this.CompetitionTitle = this.Competition.Title;

            this.DocumentTitle = LocalizationService.Resolve(() => Printing.MarkingTableTotal);

            this.LeftPageMargin = 0.5;
            this.RightPageMargin = 0.5;

            this.CreateContent();
        }

        #endregion

        #region Fields

        private ScrutinusContext context;

        private int currentGridLine;

        private FixedPage page;

        #endregion

        #region Methods

        protected override void AddCommenContent(FixedPage page)
        {
        }

        protected override void CreateCustomContent()
        {
            this.page = this.CreatePage();
            this.HeaderHeigthInDots += 20;
            // We create a Grid for all dances containing the judges markings for this round

            var viewBox = this.CreateGrid();

            var participants = this.Competition.Participants.OrderBy(p => p.Number).ToList();

            for (var i = 0; i < participants.Count(); i++)
            {
                this.AddMarkingToGrid((Grid)viewBox.Child, participants[i]);

                if (!this.ControlFitsOnPage(this.page, viewBox))
                {
                    var rounds =
                        this.Competition.Rounds.Where(
                            r => r.Qualifieds.Any(q => q.Participant.Id == participants[i].Id)).ToList();

                    for (var j = 0; j <= rounds.Count; j++)
                    {
                        this.RemoveLastLine((Grid)viewBox.Child);
                    }

                    this.AddControlToPage(this.page, viewBox);

                    // Create a new page
                    this.page = this.CreatePage();
                    this.HeaderHeigthInDots += 20;
                    viewBox = this.CreateGrid();

                    // Add the last participant again
                    this.AddMarkingToGrid((Grid)viewBox.Child, participants[i]);
                }
            }
            this.AddControlToPage(this.page, viewBox);
        }

        private void AddHeaderToGrid(Grid grid)
        {
            grid.RowDefinitions.Add(new RowDefinition());
            grid.RowDefinitions.Add(new RowDefinition());

            var number = new TextBlock { Text = LocalizationService.Resolve(() => Printing.NumberAndCouple), FontSize = this.fontSize, Margin = new Thickness(10, 0, 10, 0) };
            grid.Children.Add(number);
            Grid.SetColumn(number, 0);
            Grid.SetRow(number, 0);

            var roundText = new TextBlock { Text = LocalizationService.Resolve(() => Printing.Round), FontSize = this.fontSize, Margin = new Thickness(10, 0, 10, 0) };
            grid.Children.Add(roundText);
            Grid.SetColumn(roundText, 1);
            Grid.SetRow(roundText, 0);

            var sumText = new TextBlock { Text = LocalizationService.Resolve(() => Printing.Sum), FontSize = this.fontSize, Margin = new Thickness(10,0,10,0)};
            grid.Children.Add(sumText);
            Grid.SetColumn(sumText, this.Competition.GetJudges().Count + 2);
            Grid.SetRow(sumText, 0);

            var placeText = new TextBlock { Text = LocalizationService.Resolve(() => Printing.Rank), FontSize = this.fontSize, Margin = new Thickness(10, 0, 10, 0) };
            grid.Children.Add(placeText);
            Grid.SetColumn(placeText, this.Competition.GetJudges().Count + 3);
            Grid.SetRow(placeText, 0);

            var columnIndex = 2;

            foreach (var judge in this.Competition.GetJudges().OrderBy(j => j.Sign))
            {
                var textblock = new TextBlock { Text = judge.Sign, FontSize = this.fontSize, HorizontalAlignment = HorizontalAlignment.Center};
                grid.Children.Add(textblock);
                Grid.SetRow(textblock, 0);
                Grid.SetColumn(textblock, columnIndex);

                columnIndex++;
            }

            this.currentGridLine = 2;
        }

        private void AddMarkingToGrid(Grid grid, Participant participant)
        {
            var rounds =
                this.Competition.Rounds.Where(r => r.Qualifieds.Any(q => q.Participant.Id == participant.Id)).ToList();
            var judges = this.Competition.GetJudges().OrderBy(j => j.Sign).ToList();

            if (rounds.Count == 0)
            {
                // This couple did not dance ...
                return;
            }

            var rowSpan = rounds.Count;
            // The grid line for our border:
            grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(5) });
            // The lines for all others
            for (var i = 0; i < rowSpan; i++)
            {
                grid.RowDefinitions.Add(new RowDefinition());
            }

            // Add two lines:
            var border = new Border
                             {
                                 Height = 2,
                                 VerticalAlignment = VerticalAlignment.Stretch,
                                 BorderBrush = Brushes.Black,
                                 BorderThickness = new Thickness(2)
                             };

            grid.Children.Add(border);
            Grid.SetColumn(border, 0);
            Grid.SetColumnSpan(border, judges.Count + 4);
            Grid.SetRow(border, this.currentGridLine);
            Panel.SetZIndex(border, 99);

            this.currentGridLine++;

            var number = new TextBlock
                             {
                                 Text =
                                     participant.Number + "\r\n"
                                     + participant.Couple.NiceName.Replace(" / ", " /\r\n"),
                                 VerticalAlignment = VerticalAlignment.Center,
                                 TextWrapping = TextWrapping.Wrap,
                                 FontSize = this.fontSize
            };
            grid.Children.Add(number);
            Grid.SetColumn(number, 0);
            Grid.SetRow(number, this.currentGridLine);
            Grid.SetRowSpan(number, rowSpan);

            var columnIndex = 2;

            for (var i = 0; i < judges.Count; i++)
            {
                var currentRow = this.currentGridLine;

                foreach (var round in rounds.OrderBy(r => r.Number))
                {
                    if (i == 0)
                    {
                        var roundName = new TextBlock { Text = round.ShortName };
                        grid.Children.Add(roundName);
                        Grid.SetColumn(roundName, 1);
                        Grid.SetRow(roundName, currentRow);
                    }

                    var marking = new TextBlock { Text = this.GetMarkString(round, participant, judges[i]), Margin = new Thickness(3, 0, 3, 0), FontSize = this.fontSize };

                    var cellBorder = new Border
                                         {
                                             BorderThickness = new Thickness(1, 0, 0, 0),
                                             BorderBrush = Brushes.Black,
                                             Child = marking,
                                         };

                    grid.Children.Add(cellBorder);
                    Grid.SetColumn(cellBorder, columnIndex);
                    Grid.SetRow(cellBorder, currentRow);

                    currentRow++;
                }

                columnIndex++;
            }
            // Create to sums

            var curRow = this.currentGridLine;

            foreach (var round in rounds.OrderBy(r => r.Number))
            {
                var sumText = new TextBlock
                                  {
                                      Text = this.GetTotalMarkString(round, participant),
                                      VerticalAlignment = VerticalAlignment.Top,
                                      FontSize = this.fontSize,
                                      Margin = new Thickness(10, 0, 10, 0),
                };
                grid.Children.Add(sumText);
                Grid.SetColumn(sumText, judges.Count + 2);
                Grid.SetRow(sumText, curRow);
                curRow++;
            }

            var placeText = new TextBlock { Text = participant.Place, VerticalAlignment = VerticalAlignment.Center, FontSize = this.fontSize, Margin = new Thickness(10, 0, 10, 0) };
            grid.Children.Add(placeText);
            Grid.SetRow(placeText, this.currentGridLine);
            Grid.SetRowSpan(placeText, rounds.Count);
            Grid.SetColumn(placeText, judges.Count + 3);

            this.currentGridLine += rounds.Count;
        }

        private Viewbox CreateGrid()
        {
            var grid = new Grid();
            // Couples Number
            grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(4, GridUnitType.Auto) });

            // Round Name
            grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(65, GridUnitType.Pixel) });

            for (var i = 0; i < this.Competition.GetJudges().Count; i++)
            {
                grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(0, GridUnitType.Auto) });
            }
            // Column for the sum of all marks
            grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(2, GridUnitType.Auto) });
            // Column for the total place
            grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Auto) });

            this.AddHeaderToGrid(grid);
            var availableWidth = DotsFromCm(this.PageWidth) - DotsFromCm(this.LeftPageMargin) - DotsFromCm(this.RightPageMargin);

            this.MeasureControl(grid);
            if (grid.Width > availableWidth)
            {
                return new Viewbox() { Child = grid, Stretch = Stretch.Fill, Width = DotsFromCm(this.PageWidth) - DotsFromCm(this.LeftPageMargin) - DotsFromCm(this.RightPageMargin) };
            }
            else
            {
                grid.Width = DotsFromCm(this.PageWidth) - DotsFromCm(this.LeftPageMargin) -
                                     DotsFromCm(this.RightPageMargin);
                return new Viewbox() { Child = grid, Stretch = Stretch.Fill, Width = grid.Width };
            }        
        }

        private string GetMarkString(Round round, Participant participant, Official judge)
        {
            if (round.MarksInputType == MarkingTypes.Marking || round.MarksInputType == MarkingTypes.MarksSumOnly)
            {
                return
                    round.Markings.Where(m => m.Participant.Id == participant.Id && m.Judge.Id == judge.Id)
                        .Sum(m => m.Mark)
                        .ToString();
            }

            if (round.MarksInputType == MarkingTypes.NewJudgingSystemV2)
            {
                return
                    round.Markings.Where(m => m.Participant.Id == participant.Id && m.Judge.Id == judge.Id)
                        .Sum(m => m.Mark20)
                        .ToString("#0.00");
            }

            if (round.MarksInputType == MarkingTypes.Skating)
            {
                var marks =
                    round.Markings.Where(m => m.Participant.Id == participant.Id && m.Judge.Id == judge.Id)
                        .OrderBy(m => m.Dance.DefaultOrder)
                        .Select(m => m.Mark.ToString()).ToList();

                return string.Join("", marks);
            }

            return "?";
        }

        private string GetTotalMarkString(Round round, Participant participant)
        {
            if (round.MarksInputType == MarkingTypes.Marking || round.MarksInputType == MarkingTypes.MarksSumOnly)
            {
                return round.Markings.Where(m => m.Participant.Id == participant.Id).Sum(m => m.Mark).ToString();
            }

            if (round.MarksInputType == MarkingTypes.NewJudgingSystemV2)
            {
                return round.Qualifieds.Single(q => q.Participant.Id == participant.Id).Points.ToString("#0.00");
            }

            if (round.MarksInputType == MarkingTypes.Skating)
            {
                return participant.Points.ToString();
            }

            return "?";
        }

        private void RemoveLastLine(Grid grid)
        {
            // grid to heigh -> remove last added line + children of this line:
            var uIElements =
                grid.Children.Cast<UIElement>().Where(e => Grid.GetRow(e) == this.currentGridLine - 1).ToList();

            foreach (var uiElement in uIElements)
            {
                grid.Children.Remove(uiElement);
            }

            grid.RowDefinitions.RemoveAt(grid.RowDefinitions.Count - 1);

            this.currentGridLine--;
        }

        #endregion
    }
}