﻿// // TPS.net TPS8 Scrutinus
// // SeededCouples.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Documents;
using DataModel.Models;
using Scrutinus.Localization;
using Scrutinus.Reports.Attributes;

namespace Scrutinus.Reports.BasicPrinting.CompetitionPrinting
{
    [CanPrintBeforeFirstRound(true)]
    [CanPrintBeforeQualificationRound(true)]
    [CanPrintAfterQualificationRound(true)]
    [CanPrintBeforeFinalRound(true)]
    [CanPrintAfterFinalRound(true)]
    public class SeededCouples : GenericTablePrinter<Participant>
    {
        #region Fields

        private FixedPage page;

        #endregion

        #region Constructors and Destructors

        public SeededCouples(Competition competition)
        {
            this.ShowComplexHeader = false;
            this.ShowSimpleHeader = true;
            this.Competition = competition;

            this.CompetitionTitle = competition.Title;
            this.RoundTitle = "";
            this.DocumentTitle = LocalizationService.Resolve(() => Printing.SeededCouples);

            this.CreateContent();
        }

        #endregion

        #region Methods

        protected override void CreateCustomContent()
        {
            this.dataList = this.Competition.Participants.Where(p => p.Star > 0).OrderBy(p => p.OriginalPlacings).ToList();

            this.columns = new List<ColumnDescriptor<Participant>>
                               {
                                   new ColumnDescriptor<Participant>
                                       {
                                           Header = "",
                                           ValueFunc = (p) => p.StarString,
                                           Width =
                                               new GridLength(
                                               40,
                                               GridUnitType.Pixel)
                                       },
                                       new ColumnDescriptor<Participant>
                                       {
                                           Header = "",
                                           ValueFunc = (p) => p.OriginalPlacings.HasValue ? p.OriginalPlacings.Value.ToString() + "." : "",
                                           Width =
                                               new GridLength(
                                               1,
                                               GridUnitType.Star)
                                       },
                                   new ColumnDescriptor<Participant>
                                       {
                                           Header = "",
                                           ValueFunc = (p) => p.Number,
                                           Width =
                                               new GridLength(
                                               1,
                                               GridUnitType.Star)
                                       },
                                   new ColumnDescriptor<Participant>
                                       {
                                           Header = "",
                                           ValueFunc = (p) => p.Couple.NiceName,
                                           Width =
                                               new GridLength(
                                               3,
                                               GridUnitType.Star)
                                       },
                                   new ColumnDescriptor<Participant>
                                       {
                                           Header = "",
                                           ValueFunc = (p) => p.Couple.Country,
                                           Width =
                                               new GridLength(
                                               2,
                                               GridUnitType.Star)
                                       },
                                   new ColumnDescriptor<Participant>
                                       {
                                           Header = "",
                                           ValueFunc = (p) => p.StateString,
                                           Width =
                                               new GridLength(
                                               2,
                                               GridUnitType.Star)
                                       },
                               };
            this.DoCreateHeader = false;

            this.HeaderHeigthInDots += DotsFromCm(0.5);

            this.page = this.PrintTable(this.page);
        }

        #endregion
    }
}