﻿// // TPS.net TPS8 Scrutinus
// // FinalResultPrinter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Documents;
using DataModel;
using DataModel.Models;
using Helpers.Skating;
using Scrutinus.Localization;
using Scrutinus.Reports.Attributes;
using Scrutinus.Reports.BasicPrinting.RoundPrinting;

namespace Scrutinus.Reports.BasicPrinting.CompetitionPrinting
{
    [CanPrintAfterFinalRound(true)]
    public class FinalResultPrinter : GenericTablePrinter<CouplesResultPrintModel>
    {
        #region Constructors and Destructors

        public FinalResultPrinter(Competition competition)
        {
            this.ShowComplexHeader = false;
            this.ShowSimpleHeader = true;
            this.Competition = competition;

            this.CompetitionTitle = competition.Title;
            this.RoundTitle = LocalizationService.Resolve(() => Printing.FinalResult);
            this.context = new ScrutinusContext();

            var finalRound = this.Competition.Rounds.FirstOrDefault(r => r.RoundType.Id == RoundTypes.Final);

            if (finalRound == null)
            {
                return;
            }

            this.DocumentTitle = LocalizationService.Resolve(() => Printing.CouplesResults);

            this.model = SkatingHelper.CreateViewModel(this.context, competition.Rounds.First(r => r.RoundType.Id == RoundTypes.Final));


            this.CreateContent();
        }

        #endregion

        #region Fields

        private readonly ScrutinusContext context;

        private readonly SkatingViewModel model;

        private FixedPage page;

        #endregion

        #region Methods

        protected override void CreateCustomContent()
        {
            var finalRound = this.Competition.Rounds.FirstOrDefault(r => r.RoundType.Id == RoundTypes.Final);

            if (finalRound == null)
            {
                return;
            }

            this.dataList =
                finalRound.Qualifieds.OrderBy(p => p.Participant.PlaceFrom)
                    .Select(
                        p =>
                        new CouplesResultPrintModel
                            {
                                Participant = p.Participant,
                                Star = p.Participant.StarString,
                                RankingPoints = p.Participant.RankingPoints.ToString(),
                                Number = p.Participant.Number,
                                Place = p.Participant.Place,
                                SumInFinal = p.Participant.Points,
                                StateString = p.Participant.StateString,
                                IsClimbUp = p.Participant.ClimbUp ? "Aufstieg!" : ""
                            })
                    .ToList();

            this.columns = new List<ColumnDescriptor<CouplesResultPrintModel>>
                               {
                                   new ColumnDescriptor
                                       <CouplesResultPrintModel>
                                       {
                                           Header
                                               =
                                               "",
                                           ValueFunc
                                               =
                                               q
                                               =>
                                               q
                                                   .Star,
                                           Width
                                               =
                                               new GridLength
                                               (
                                               1,
                                               GridUnitType
                                               .Auto),
                                           FontSize = 18
                                       },
                                   new ColumnDescriptor
                                       <CouplesResultPrintModel>
                                       {
                                           Header
                                               =
                                               "",
                                           ValueFunc
                                               =
                                               q
                                               =>
                                               q
                                                   .Number,
                                           Width
                                               =
                                               new GridLength
                                               (
                                               1,
                                               GridUnitType
                                               .Star),
                                           FontSize = 18
                                       },
                                   new ColumnDescriptor
                                       <CouplesResultPrintModel>
                                       {
                                           Header
                                               =
                                               "",
                                           ValueFunc
                                               =
                                               q
                                               =>
                                               q
                                                   .Place,
                                           Width
                                               =
                                               new GridLength
                                               (
                                               1,
                                               GridUnitType
                                               .Star),
                                           FontSize = 18
                                       },
                                   new ColumnDescriptor
                                       <CouplesResultPrintModel>
                                       {
                                           Header
                                               =
                                               "",
                                           ValueFunc
                                               =
                                               q
                                               =>
                                               q
                                                   .Participant
                                                   .Couple
                                                   .NiceName
                                               + "\r\n"
                                               + this
                                                     .GetFinalPlacesPerDance
                                                     (
                                                         q
                                                     .Participant),
                                           Width
                                               =
                                               new GridLength
                                               (
                                               5,
                                               GridUnitType
                                               .Star),
                                           FontSize = 18
                                       },
                                   new ColumnDescriptor
                                       <CouplesResultPrintModel>
                                       {
                                           Header
                                               =
                                               "",
                                           ValueFunc
                                               =
                                               q
                                               =>
                                               q
                                                   .Participant
                                                   .Couple
                                                   .Country,
                                           Width
                                               =
                                               new GridLength
                                               (
                                               2,
                                               GridUnitType
                                               .Star),
                                           FontSize = 18
                                       },
                                   new ColumnDescriptor<CouplesResultPrintModel>()
                                   {
                                       Header = "",
                                       ValueFunc = q => q.SumInFinal,
                                       Width
                                               =
                                               new GridLength
                                               (
                                               1,
                                               GridUnitType
                                               .Star),
                                       FontSize = 18
                                   },
#if DTV
                new ColumnDescriptor
                                       <CouplesResultPrintModel>
                                       {
                                           Header
                                               =
                                               "",
                                           ValueFunc
                                               =
                                               q
                                               =>
                                               q
                                                   .RankingPoints,
                                           Width
                                               =
                                               new GridLength
                                               (
                                               1,
                                               GridUnitType
                                               .Star)
                                       },
                                   new ColumnDescriptor
                                       <CouplesResultPrintModel>
                                       {
                                           Header
                                               =
                                               "",
                                           ValueFunc
                                               =
                                               q
                                               =>
                                               q
                                                   .IsClimbUp,
                                           Width
                                               =
                                               new GridLength
                                               (
                                               1,
                                               GridUnitType
                                               .Star)
                                       }
#endif
                               };

            if (this.Competition.CombinedExternalId != null)
            {
                this.columns.Insert(
                    3,
                    new ColumnDescriptor<CouplesResultPrintModel>
                        {
                            Header = "Pl. Kombi",
                            ValueFunc = q => q.Participant.PlaceOwnCompetition
                        });
                this.DoCreateHeader = true;
            }
            else
            {
                this.DoCreateHeader = false;
            }

            this.HeaderHeigthInDots += DotsFromCm(0.5);

            this.page = this.PrintTable(this.page);
        }


        private string GetFinalPlacesPerDance(Participant participant)
        {
            var results = new List<string>();

            foreach (var skatingFinalDanceViewModel in this.model.DanceSkating)
            {
                var dance = this.context.Dances.First(d => d.Id == skatingFinalDanceViewModel.DanceInRound.Dance.Id);

                results.Add(
                    dance.ShortName + ": "
                    + skatingFinalDanceViewModel.CoupleResults.First(p => p.Participant.Id == participant.Id).Place);
            }

            return string.Join(", ", results);
        }

        #endregion
    }
}