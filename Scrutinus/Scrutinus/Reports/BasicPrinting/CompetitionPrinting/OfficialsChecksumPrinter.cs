﻿// // TPS.net TPS8 Scrutinus
// // OfficialsChecksumPrinter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Documents;
using DataModel;
using DataModel.Models;
using DtvEsvModule;

namespace Scrutinus.Reports.BasicPrinting.CompetitionPrinting
{
    public class CheckSumPrintModel
    {
        #region Public Properties

        public string CheckSum { get; set; }

        public Competition Competition { get; set; }

        public OfficialInCompetition Official { get; set; }

        #endregion
    }

    public class OfficialsChecksumPrinter : GenericTablePrinter<CheckSumPrintModel>
    {
        #region Constructors and Destructors

        public OfficialsChecksumPrinter(Official official, ScrutinusContext context)
        {
            this.context = context;
            this.official = official;
            this.ShowComplexHeader = false;
            this.ShowSimpleHeader = true;

            this.Competition = context.Competitions.First();
            // this.Competition.StartTime = DateTime.Now;
            this.CompetitionTitle = official.Sign + ") " + official.NiceName;
            this.RoundTitle = "";
            this.DocumentTitle = "Prüfsummen";

            this.CreateContent();
        }

        #endregion

        #region Fields

        private readonly ScrutinusContext context;

        private readonly Official official;

        private FixedPage page;

        #endregion

        #region Methods

        protected override void CreateCustomContent()
        {
            this.page = this.CreatePage();
            this.PrintCheckSums();
        }

        private void PrintCheckSums()
        {
            var competitions =
                this.context.Competitions.Where(
                    c =>
                    c.Officials.Any(
                        o =>
                        o.Official.Id == this.official.Id && o.Role.Id != Roles.Judge && o.Role.Id != Roles.Scrutineer))
                    .ToList();

            var model = new List<CheckSumPrintModel>();
            var apiImporter = new DtvApiImporter("", "", "", this.context);

            foreach (var competition in competitions.Where(c => c.State == CompetitionState.Finished))
            {
                if (competition.DtvJson == null)
                {
                    apiImporter.CalculateChecksum(competition);

                    this.context.SaveChanges();

                    model.Add(
                        new CheckSumPrintModel
                            {
                                CheckSum = competition.DtvChecksum.Substring(0, 6),
                                Competition = competition,
                                Official =
                                    competition.Officials.FirstOrDefault(
                                        r => r.Official.Id == this.official.Id)
                            });
                }
                else
                {
                    model.Add(
                        new CheckSumPrintModel
                            {
                                CheckSum = competition.DtvChecksum.Substring(0, 6),
                                Competition = competition,
                                Official =
                                    competition.Officials.FirstOrDefault(
                                        r => r.Official.Id == this.official.Id)
                            });
                }
            }

            this.dataList = model;

            this.columns = new List<ColumnDescriptor<CheckSumPrintModel>>
                               {
                                   new ColumnDescriptor<CheckSumPrintModel>
                                       {
                                           Header
                                               =
                                               "Turnier",
                                           ValueFunc
                                               =
                                               q
                                               =>
                                               q
                                                   .Competition
                                                   .Title,
                                           Width
                                               =
                                               new GridLength
                                               (
                                               4,
                                               GridUnitType
                                               .Star)
                                       },
                                   new ColumnDescriptor<CheckSumPrintModel>
                                       {
                                           Header
                                               =
                                               "DTV Nummer",
                                           ValueFunc
                                               =
                                               q
                                               =>
                                               q
                                                   .Competition
                                                   .ExternalId,
                                           Width
                                               =
                                               new GridLength
                                               (
                                               2,
                                               GridUnitType
                                               .Star)
                                       },
                                   new ColumnDescriptor<CheckSumPrintModel>
                                       {
                                           Header
                                               =
                                               "Rolle",
                                           ValueFunc
                                               =
                                               q
                                               =>
                                               q
                                                   .Official
                                                   .Role
                                                   .RoleLong,
                                           Width
                                               =
                                               new GridLength
                                               (
                                               2,
                                               GridUnitType
                                               .Star)
                                       },
                                   new ColumnDescriptor<CheckSumPrintModel>
                                       {
                                           Header
                                               =
                                               "Prüfsumme",
                                           ValueFunc
                                               =
                                               q
                                               =>
                                               q
                                                   .CheckSum
                                                   .Substring
                                                   (
                                                       0,
                                                       6),
                                           Width
                                               =
                                               new GridLength
                                               (
                                               3,
                                               GridUnitType
                                               .Star)
                                       },
                               };

            this.DoCreateHeader = true;

            this.HeaderHeigthInDots += DotsFromCm(0.5);
            this.TableFontSize = 14;
            this.page = this.PrintTable(this.page);
        }

        #endregion
    }
}