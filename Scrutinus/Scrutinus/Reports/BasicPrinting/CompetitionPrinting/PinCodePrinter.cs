﻿// // TPS.net TPS8 Scrutinus
// // PinCodePrinter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Documents;
using DataModel;
using DataModel.Models;
using Scrutinus.Localization;

namespace Scrutinus.Reports.BasicPrinting.CompetitionPrinting
{
    internal class PinCodePrinter : GenericTablePrinter<Official>
    {
        private readonly ScrutinusContext context;

        private FixedPage page;

        public PinCodePrinter(ScrutinusContext context)
        {
            this.ShowComplexHeader = false;
            this.ShowSimpleHeader = false;

            this.context = context;

            this.CompetitionTitle = "";
            this.RoundTitle = "";
            this.DocumentTitle = LocalizationService.Resolve(() => Printing.SeededCouples);

            this.CreateContent();
        }

        protected override void CreateCustomContent()
        {
            this.dataList = this.context.Officials.Where(o => o.Roles.Any(r => r.Id == Roles.Judge)).ToList();

            this.columns = new List<ColumnDescriptor<Official>>
                               {
                                                   new ColumnDescriptor<Official>()
                                       {
                                           Header = "",
                                           ValueFunc = (o) => o.Sign,
                                           Width = new GridLength(1, GridUnitType.Star)
                                       },
                                   new ColumnDescriptor<Official>()
                                       {
                                           Header = "",
                                           ValueFunc = (o) => o.NiceName,
                                           Width = new GridLength(3, GridUnitType.Star)
                                       },
                                   new ColumnDescriptor<Official>()
                                       {
                                            Header  = "",
                                            ValueFunc = (o) => "PIN: " + o.PinCode,
                                            Width = new GridLength(1, GridUnitType.Star)
                                       } 
                               };

            this.DoCreateHeader = false;

            this.HeaderHeigthInDots += DotsFromCm(0.5);
            this.TableFontSize = 16;
            this.TableRowHeight = new GridLength(60, GridUnitType.Pixel);

            this.page = this.PrintTable(this.page);
        }
    }
}
