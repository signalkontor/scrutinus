﻿// // TPS.net TPS8 Scrutinus
// // CouplesResultOrderedByPlace.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Documents;
using DataModel;
using DataModel.Models;
using Scrutinus.Localization;
using Scrutinus.Reports.Attributes;
using Scrutinus.Reports.BasicPrinting.RoundPrinting;

namespace Scrutinus.Reports.BasicPrinting.CompetitionPrinting
{
    [CanPrintAfterFinalRound(true)]
    [CanPrintAfterQualificationRound(true)]
    public class CouplesResultOrderedByPlace : GenericTablePrinter<CouplesResultPrintModel>
    {
        #region Constructors and Destructors

        public CouplesResultOrderedByPlace(Competition competition, bool onlyFinalResult)
        {
            this.ShowComplexHeader = false;
            this.ShowSimpleHeader = true;
            this.Competition = competition;

            this.CompetitionTitle = competition.Title;
            this.RoundTitle = "";

            this.printOnlyFinal = onlyFinalResult;

            this.DocumentTitle = onlyFinalResult
                                     ? LocalizationService.Resolve(() => Printing.FinalResult)
                                     : LocalizationService.Resolve(() => Printing.ResultOrderedByPlace);

            this.CreateContent();
        }

        #endregion

        #region Fields

        private readonly bool printOnlyFinal;

        private FixedPage page;

        #endregion

        #region Methods

        protected override void CreateCustomContent()
        {
            if (this.printOnlyFinal)
            {
                this.CreateCustomContentFinal();
            }
            else
            {
                this.CreateCustomContentCompetition();
            }
        }

        private void CreateCustomContentCompetition()
        {
            var lists =
                this.Competition.Participants.Where(p => p.QualifiedRound != null).OrderBy(p => p.PlaceFrom)
                    .Select(
                        p =>
                        new CouplesResultPrintModel
                            {
                                Participant = p,
                                Star = p.StarString,
                                RankingPoints = p.RankingPoints.ToString(),
                                IsClimbUp = p.ClimbUp ? "Aufstieg!" : "",
                                Number = p.Number,
                                Place = p.Place,
                                StateString = p.StateString,
                                LastLastRoundQualified = p.QualifiedRound,
                            })
                    .ToList()
                    .GroupBy(p => p.LastLastRoundQualified)
                    .OrderByDescending(g => g.Key.Number)
                    .ToList();

            this.columns = new List<ColumnDescriptor<CouplesResultPrintModel>>
                               {
                                   new ColumnDescriptor<CouplesResultPrintModel>
                                       {
                                           Header = "",
                                           ValueFunc = (p) => p.Star,
                                           Width =
                                               new GridLength(
                                               75,
                                               GridUnitType.Pixel)
                                       },
                                   new ColumnDescriptor<CouplesResultPrintModel>
                                       {
                                           Header = "",
                                           ValueFunc = (p) => p.Number,
                                           Width =
                                               new GridLength(
                                               1,
                                               GridUnitType.Star)
                                       },
                                   new ColumnDescriptor<CouplesResultPrintModel>
                                       {
                                           Header = "",
                                           ValueFunc = (p) => p.Participant.Couple.NiceName,
                                           Width =
                                               new GridLength(
                                               5,
                                               GridUnitType.Star)
                                       },
                                   new ColumnDescriptor<CouplesResultPrintModel>
                                       {
                                           Header = "",
                                           ValueFunc = (p) => p.Participant.Couple.Country + (string.IsNullOrEmpty(p.Participant.Couple.Region) ? "" : $" ({p.Participant.Couple.Region})" ),
                                           Width =
                                               new GridLength(
                                               3,
                                               GridUnitType.Star)
                                       },
                                   new ColumnDescriptor<CouplesResultPrintModel>
                                       {
                                           Header = "Platz",
                                           ValueFunc = (p) => p.Place,
                                           Width =
                                               new GridLength(
                                               1,
                                               GridUnitType.Star)
                                       },
                                   new ColumnDescriptor<CouplesResultPrintModel>
                                       {
                                           Header = "Punkte",
                                           ValueFunc = (p) => p.RankingPoints,
                                           Width =
                                               new GridLength(
                                               1,
                                               GridUnitType.Star)
                                       },
                                   new ColumnDescriptor<CouplesResultPrintModel>
                                       {
                                           Header = "A.",
                                           ValueFunc = (p) => p.IsClimbUp,
                                           Width =
                                               new GridLength(
                                               1,
                                               GridUnitType.Star)
                                       }
                               };

            if (this.Competition.CombinedExternalId != null)
            {
                this.columns.Insert(
                    5,
                    new ColumnDescriptor<CouplesResultPrintModel>
                        {
                            Header = "Pl. Kombi",
                            ValueFunc = (p) => p.Participant.PlaceOwnCompetition,
                            Width = new GridLength(1, GridUnitType.Star)
                        });
            }

            if (this.Competition.CombinedExternalId != null)
            {
                this.DoCreateHeader = true;
            }
            else
            {
                this.DoCreateHeader = false;
            }
            this.page = this.CreatePage();

            this.HeaderHeigthInDots += DotsFromCm(0.3);

            foreach (var group in lists.Where(g => g.Key != null))
            {
                // we add the name of this round:
                this.AddText(this.page, group.Key.Name, CmFromDots(this.HeaderHeigthInDots), this.LeftPageMargin, 14);
                this.HeaderHeigthInDots += DotsFromCm(0.5);

                this.dataList = new List<CouplesResultPrintModel>();
                foreach (var couplesResultPrintModel in group)
                {
                    this.dataList.Add(couplesResultPrintModel);
                }

                this.page = this.PrintTable(this.page);

                this.HeaderHeigthInDots += DotsFromCm(0.3);
            }
        }

        private void CreateCustomContentFinal()
        {
            var finalRound = this.Competition.Rounds.SingleOrDefault(r => r.RoundType.Id == RoundTypes.Final);

            if (finalRound == null)
            {
                // nothing to print without a final ...
                return;
            }

            var resultList =
                finalRound.Qualifieds.Select(q => q.Participant)
                    .OrderBy(p => p.PlaceFrom)
                    .Select(
                        p =>
                        new CouplesResultPrintModel
                            {
                                Participant = p,
                                Star = p.StarString,
                                RankingPoints = p.RankingPoints.ToString(),
                                Number = p.Number,
                                Place = p.Place,
                                StateString = p.StateString,
                                LastLastRoundQualified = p.QualifiedRound,
                            })
                    .ToList();

            this.columns = new List<ColumnDescriptor<CouplesResultPrintModel>>
                               {
                                   new ColumnDescriptor<CouplesResultPrintModel>
                                       {
                                           Header = "",
                                           ValueFunc = (p) => p.Star,
                                           Width =
                                               new GridLength(
                                               40,
                                               GridUnitType.Pixel)
                                       },
                                   new ColumnDescriptor<CouplesResultPrintModel>
                                       {
                                           Header = "",
                                           ValueFunc = (p) => p.Number,
                                           Width =
                                               new GridLength(
                                               1,
                                               GridUnitType.Star)
                                       },
                                   new ColumnDescriptor<CouplesResultPrintModel>
                                       {
                                           Header = "",
                                           ValueFunc = (p) => p.Participant.Couple.NiceName,
                                           Width =
                                               new GridLength(
                                               3,
                                               GridUnitType.Star)
                                       },
                                   new ColumnDescriptor<CouplesResultPrintModel>
                                       {
                                           Header = "",
                                           ValueFunc = (p) => p.Participant.Couple.Country,
                                           Width =
                                               new GridLength(
                                               2,
                                               GridUnitType.Star)
                                       },
                                   new ColumnDescriptor<CouplesResultPrintModel>
                                       {
                                           Header = "",
                                           ValueFunc = (p) => p.Place,
                                           Width =
                                               new GridLength(
                                               1,
                                               GridUnitType.Star)
                                       },
                                   new ColumnDescriptor<CouplesResultPrintModel>
                                       {
                                           Header = "",
                                           ValueFunc = (p) => p.RankingPoints,
                                           Width =
                                               new GridLength(
                                               1,
                                               GridUnitType.Star)
                                       }
                               };

            if (this.Competition.CombinedExternalId != null)
            {
                this.columns.Insert(
                    5,
                    new ColumnDescriptor<CouplesResultPrintModel>
                        {
                            Header = "Pl. Kombi",
                            ValueFunc = (p) => p.Participant.PlaceOwnCompetition,
                            Width = new GridLength(1, GridUnitType.Star)
                        });
            }

            this.DoCreateHeader = false;

            this.page = this.CreatePage();

            this.HeaderHeigthInDots += DotsFromCm(0.3);

            this.dataList = new List<CouplesResultPrintModel>();
            foreach (var couplesResultPrintModel in resultList)
            {
                this.dataList.Add(couplesResultPrintModel);
            }

            this.page = this.PrintTable(this.page);

            this.HeaderHeigthInDots += DotsFromCm(0.3);
        }

        #endregion
    }
}