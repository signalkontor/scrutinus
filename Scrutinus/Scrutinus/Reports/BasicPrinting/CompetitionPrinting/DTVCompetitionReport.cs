﻿// // TPS.net TPS8 Scrutinus
// // DTVCompetitionReport.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using DataModel.Models;
using Scrutinus.Reports.Attributes;

namespace Scrutinus.Reports.BasicPrinting.CompetitionPrinting
{
    [CanPrintAfterFinalRound(true)]
    public class DTVCompetitionReport : GenericTablePrinter<OfficialInCompetition>
    {
        #region Constructors and Destructors

        public DTVCompetitionReport(ScrutinusContext context)
        {
            this.context = context;

            this.ShowFederationHeader = true;
            this.ShowSimpleHeader = false;
            this.DocumentTitle = "Anlage zum Turnierbericht";

            this.theEvent = context.Events.FirstOrDefault();

            this.CreateContent();
        }

        #endregion

        #region Fields

        private readonly ScrutinusContext context;

        private readonly Event theEvent;

        private FixedPage page;

        #endregion

        #region Methods

        protected override void CreateCustomContent()
        {
            this.page = this.CreatePage();

            this.CreateCompetitionReport();
        }

        private void AddBasicEventInfos()
        {
            var grid = this.CreateValueGrid();

            this.AddToGrid(grid, new TextBlock { Text = "Ausrichter", FontSize = 10 }, 0, 0);
            this.AddToGrid(grid, new TextBlock { Text = this.theEvent.Organizer, FontSize = 12 }, 0, 1);

            this.AddToGrid(grid, new TextBlock { Text = "Titel", FontSize = 10 }, 1, 0);
            this.AddToGrid(grid, new TextBlock { Text = this.theEvent.Title, FontSize = 12 }, 1, 1);

            this.MeasureControl(grid);

            this.page = this.AddControlToPage(this.page, grid);

            this.HeaderHeigthInDots += grid.ActualHeight + 10;

            this.InsertVerticalLine(this.page);

            this.HeaderHeigthInDots += 10;
        }

        private void AddFloor()
        {
            var grid = this.CreateValueGrid();

            this.AddToGrid(grid, new TextBlock { Text = "Material Tanzfläche", FontSize = 10 }, 0, 0);
            this.AddToGrid(grid, new TextBlock { Text = "", FontSize = 12 }, 0, 1);

            this.AddToGrid(grid, new TextBlock { Text = "Größe Tanzfläche", FontSize = 10 }, 1, 0);
            this.AddToGrid(
                grid,
                new TextBlock
                    {
                        Text = this.theEvent.SizeDanceFloorLength + " X " + this.theEvent.SizeDanceFloorWidth,
                        FontSize = 12
                    },
                1,
                1);

            this.MeasureControl(grid);

            this.page = this.AddControlToPage(this.page, grid);

            this.HeaderHeigthInDots += grid.ActualHeight + 10;

            this.InsertVerticalLine(this.page);

            this.HeaderHeigthInDots += 10;
        }

        private int AddOfficialByRole(Grid grid, int line, int role, string roleString)
        {
            var officials = this.context.Officials.Where(o => o.Roles.Any(r => r.Id == role));
            return this.AddOfficialsFromList(officials, grid, line, roleString);
        }

        private void AddOfficials()
        {
            this.columns = new List<ColumnDescriptor<OfficialInCompetition>>()
            {
                new ColumnDescriptor<OfficialInCompetition>()
                {
                    Header = "Sign",
                    ValueFunc = o => o.Official.Sign,
                    Width = new GridLength(1, GridUnitType.Star) 
                },
                new ColumnDescriptor<OfficialInCompetition>()
                {
                    Header = "Name",
                    ValueFunc = o => o.Official.NiceName,
                    Width = new GridLength(3, GridUnitType.Star)
                },
                new ColumnDescriptor<OfficialInCompetition>()
                {
                    Header = "Club",
                    ValueFunc = o => o.Official.Club,
                    Width = new GridLength(3, GridUnitType.Star)
                },
                new ColumnDescriptor<OfficialInCompetition>()
                {
                    Header = "LTV",
                    ValueFunc = o => o.Official.Region,
                    Width = new GridLength(1, GridUnitType.Star)
                },
                new ColumnDescriptor<OfficialInCompetition>()
                {
                    Header = "Country",
                    ValueFunc = o => o.Official.Nationality?.LongName ?? "",
                    Width = new GridLength(1, GridUnitType.Star)
                },
            };

            this.groupedDataList = this.context.Competitions.ToList()
                    .SelectMany(c => c.Officials)
                    .GroupBy(g => new { g.Official.Sign, g.Role.Id } )
                    .Select(g => g.First())
                    .OrderBy(o => o.Role.RoleLong)
                    .GroupBy(o => o.Role.RoleLong);

            this.HeaderHeigthInDots += 30;

            this.page = this.PrintTabledGrouped(this.page);

            this.HeaderHeigthInDots += 10;
            
            this.InsertVerticalLine(this.page);

            this.HeaderHeigthInDots += 10;
        }

        private int AddOfficialsFromList(IEnumerable<Official> officials, Grid grid, int line, string officialType)
        {
            foreach (var official in officials)
            {
                this.AddToGrid(grid, new TextBlock { Text = officialType, FontSize = 10 }, line, 0);
                this.AddToGrid(
                    grid,
                    new TextBlock { Text = official.Sign + ") " + official.NiceName, FontSize = 12 },
                    line,
                    1);
                line++;
                this.AddToGrid(grid, new TextBlock { Text = "Club", FontSize = 10 }, line, 0);
                this.AddToGrid(grid, new TextBlock { Text = official.Club, FontSize = 12 }, line, 1);
                line++;
                this.AddToGrid(grid, new TextBlock { Text = "LTV", FontSize = 10 }, line, 0);
                this.AddToGrid(grid, new TextBlock { Text = "", FontSize = 12 }, line, 1);
                line++;

                // Todo: Hier eine neue Seite beginnen, wenn das Control nicht mehr auf die Seite passt:
                if (!this.ControlFitsOnPage(this.page, grid))
                {
                    
                }
            }

            return line;
        }

        private void AddToGrid(Grid grid, FrameworkElement control, int row, int column)
        {
            grid.Children.Add(control);
            Grid.SetColumn(control, column);
            Grid.SetRow(control, row);

            while (row > grid.RowDefinitions.Count - 1)
            {
                grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(20) });
            }
        }

        private void CreateCompetitionReport()
        {
            this.AddBasicEventInfos();

            this.AddFloor();

            this.AddOfficials();
        }

        private Grid CreateValueGrid()
        {
            var grid = new Grid();
            grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(300) });
            grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            grid.Width = DotsFromCm(this.PageWidth) - DotsFromCm(this.LeftPageMargin) - DotsFromCm(this.RightPageMargin);

            return grid;
        }

        #endregion
    }
}