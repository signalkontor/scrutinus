﻿// // TPS.net TPS8 Scrutinus
// // OfficialsPrinter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Documents;
using DataModel;
using DataModel.Models;
using Scrutinus.Localization;
using Scrutinus.Reports.Attributes;

namespace Scrutinus.Reports.BasicPrinting.CompetitionPrinting
{
    [CanPrintBeforeFirstRound(true)]
    [CanPrintBeforeQualificationRound(true)]
    [CanPrintBeforeFinalRound(true)]
    public class OfficialsPrinter : GenericTablePrinter<OfficialInCompetition>
    {
        #region Fields

        private FixedPage page;

        #endregion

        #region Constructors and Destructors

        public OfficialsPrinter(Competition competition)
        {
            this.ShowComplexHeader = true;
            this.ShowSimpleHeader = false;
            this.PrintFloor = true;

            this.Competition = competition;

            this.CompetitionTitle = competition.Title;
            this.RoundTitle = "";

            this.DocumentTitle = LocalizationService.Resolve(() => Printing.Officials) + " " + competition.Title;

            this.CreateContent();
        }

        #endregion

        #region Methods

        protected override void CreateCustomContent()
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            this.PrintJudges();
            Debug.WriteLine("Create Table Judges: " + stopwatch.ElapsedMilliseconds);
            this.PrintOtherOfficials();
            Debug.WriteLine("Create Table Officials: " + stopwatch.ElapsedMilliseconds);
            stopwatch.Stop();
        }

        private void PrintJudges()
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            this.page = this.CreatePage();
            Debug.WriteLine("Create Page: " + stopwatch.ElapsedMilliseconds);
            this.dataList = this.Competition.Officials.Where(o => o.Role.Id == Roles.Judge).OrderBy(j => j.Official.Sign).ToList();

            this.columns = new List<ColumnDescriptor<OfficialInCompetition>>
                               {
                                   new ColumnDescriptor<OfficialInCompetition>
                                       {
                                           Header = "",
                                           ValueFunc = (o) => o.Official.Sign ,
                                           Width =
                                               new GridLength(
                                               1,
                                               GridUnitType.Star)
                                       },
                                   new ColumnDescriptor<OfficialInCompetition>
                                       {
                                           Header = "",
                                           ValueFunc = (o) => o.Official.NiceName,
                                           Width =
                                               new GridLength(
                                               3,
                                               GridUnitType.Star)
                                       },
                                   new ColumnDescriptor<OfficialInCompetition>
                                       {
                                           Header = "",
                                           ValueFunc = (o) => o.Official.Club,
                                           Width =
                                               new GridLength(
                                               3,
                                               GridUnitType.Star)
                                       },
                                   new ColumnDescriptor<OfficialInCompetition>
                                   {
                                       Header = "",
                                       ValueFunc = (o) => o.Official.Nationality?.LongName ?? "",
                                       Width =
                                           new GridLength(
                                               2,
                                               GridUnitType.Star)
                                   },
                               };

            this.DoCreateHeader = false;

            this.HeaderHeigthInDots += DotsFromCm(0.5);

            this.AddText(
                this.page,
                LocalizationService.Resolve(() => Printing.Adjudicator),
                CmFromDots(this.HeaderHeigthInDots),
                this.LeftPageMargin,
                14);

            this.HeaderHeigthInDots += DotsFromCm(0.5);
            Debug.WriteLine("Before PrintTable: " + stopwatch.ElapsedMilliseconds);
            this.page = this.PrintTable(this.page);
            Debug.WriteLine("PrintTable: " + stopwatch.ElapsedMilliseconds);
        }

        private void PrintOtherOfficials()
        {
            this.HeaderHeigthInDots += DotsFromCm(0.5);

            this.AddText(
                this.page,
                LocalizationService.Resolve(() => Printing.OtherOfficials),
                CmFromDots(this.HeaderHeigthInDots),
                this.LeftPageMargin,
                14);

            this.HeaderHeigthInDots += DotsFromCm(0.5);

            this.columns = new List<ColumnDescriptor<OfficialInCompetition>>
                               {
                                   new ColumnDescriptor<OfficialInCompetition>
                                       {
                                           Header = "",
                                           ValueFunc = (o) => o.Official.Sign,
                                           Width =
                                               new GridLength(
                                               1,
                                               GridUnitType.Star)
                                       },
                                   new ColumnDescriptor<OfficialInCompetition>
                                       {
                                           Header = "",
                                           ValueFunc = (o) => o.Official.NiceName,
                                           Width =
                                               new GridLength(
                                               3,
                                               GridUnitType.Star)
                                       },
                                   new ColumnDescriptor<OfficialInCompetition>
                                       {
                                           Header = "",
                                           ValueFunc = (o) => o.Official.Club != null ? $"{o.Official.Club} ({o.Official.Nationality?.LongName})" : o.Official.Nationality?.LongName,
                                           Width =
                                               new GridLength(
                                               3,
                                               GridUnitType.Star)
                                       },
                                   new ColumnDescriptor<OfficialInCompetition>
                                       {
                                           Header = "",
                                            ValueFunc = (o) => o.Role.RoleLong,
                                           Width =
                                               new GridLength(
                                               2,
                                               GridUnitType.Star)
                                       }
                               };

            this.dataList = this.Competition.Officials.Where(o => o.Role.Id != Roles.Judge).ToList();

            this.page = this.PrintTable(this.page);
        }

       

        #endregion
    }
}