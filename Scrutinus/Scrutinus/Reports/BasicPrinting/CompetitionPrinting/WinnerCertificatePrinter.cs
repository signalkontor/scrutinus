﻿// // TPS.net TPS8 Scrutinus
// // WinnerCertificatePrinter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using DataModel;
using DataModel.Models;
using Scrutinus.Dialogs.DialogViewModels;
using Scrutinus.Localization;
using Scrutinus.Reports.Attributes;
using Image = System.Drawing.Image;

namespace Scrutinus.Reports.BasicPrinting.CompetitionPrinting
{
    [CanPrintAfterFinalRound(true)]
    public class WinnerCertificatePrinter : AbstractPrinter
    {
        #region Constructors and Destructors

        public WinnerCertificatePrinter(
            Round round,
            WinnerCertificateDefinition winnerCertificate,
            IEnumerable<Participant> participantsToPrint)
            : base(round.Competition)
        {
            this.ShowComplexHeader = false;
            this.ShowSimpleHeader = false;
            this.ShowPageNumbers = false;
            this.PrintCopyRight = false;

            this.winnerCertificateDefinition = winnerCertificate;
            this.participants = participantsToPrint;

            this.CreateContent();
        }

        #endregion

        #region Fields

        private readonly IEnumerable<Participant> participants;

        private readonly WinnerCertificateDefinition winnerCertificateDefinition;

        private FixedPage currentPage;

        #endregion

        #region Methods

        protected override void AddCommenContent(FixedPage page)
        {
            // Nothing here to print
        }

        protected override void CreateCustomContent()
        {
            foreach (var participant in this.participants.OrderByDescending(p => p.PlaceFrom))
            {
                this.currentPage = this.CreatePage();

                foreach (var winnerCertificateElement in this.winnerCertificateDefinition.Elements)
                {
                    this.AddElementToPage(winnerCertificateElement, participant);
                }
            }
        }

        private void AddElementToPage(WinnerCertificateElement element, Participant participant)
        {
            if (element.ElementType == WinnerCertificateElementTypes.Picture)
            {
                this.AddImageElementToPage(element);
            }
            else
            {
                this.AddTextElementToPage(element, participant);
            }
        }

        private void AddImageElementToPage(WinnerCertificateElement element)
        {
            if (!File.Exists(element.Text))
            {
                MainWindow.MainWindowHandle.ShowMessage(
                    string.Format(LocalizationService.Resolve(() => Text.ImageDoesNotExists), element.Text));
                return;
            }

            using (Stream bitmapStream = File.Open(element.Text, FileMode.Open))
            {
                try
                {
                    var img = Image.FromStream(bitmapStream);
                    var bitmapImage = this.ConvertBitmapToBitmapImage(img);

                    var image = new System.Windows.Controls.Image { Source = bitmapImage };
                    image.Width = element.WidthInPixel;
                    image.Height = element.HeightInPixel;
                    image.Stretch = Stretch.Fill;

                    this.AddControlToPage(this.currentPage, image);

                    FixedPage.SetLeft(image, DotsFromCm(element.Left));
                    FixedPage.SetTop(image, DotsFromCm(element.Top));
                }
                catch (Exception)
                {
                    MainWindow.MainWindowHandle.ShowMessage(
                        string.Format(LocalizationService.Resolve(() => Text.ErrorLoadingImage), element.Text));
                }
            }
        }

        private void AddTextElementToPage(WinnerCertificateElement element, Participant participant)
        {
            var text = "";

            switch (element.ElementType)
            {
                case WinnerCertificateElementTypes.AgeGroup:
                    text = this.Competition.CombinedAgeGroup == null
                               ? this.Competition.AgeGroup.ShortName
                               : participant.AgeGroup.ShortName;
                    break;
                case WinnerCertificateElementTypes.Class:
                    text = this.Competition.CombinedClass == null
                               ? this.Competition.StartGroup
                               : participant.AgeGroup.ShortName + " " + participant.Class.ClassShortName;
                    break;
                case WinnerCertificateElementTypes.CompetitionTitle:
                    text = this.Competition.Title;
                    break;
                case WinnerCertificateElementTypes.CoupleName:
                    text = participant.Couple.NiceName;
                    break;
                case WinnerCertificateElementTypes.CoupleNameWomanMan:
                    var c = participant.Couple;
                    text = $"{c.FirstWoman} {c.LastWoman} / {c.FirstMan} {c.LastMan}";
                    break;
                case WinnerCertificateElementTypes.Date:
                    text = participant.Competition.StartTime.HasValue
                               ? participant.Competition.StartTime.Value.ToShortDateString()
                               : DateTime.Now.ToShortDateString();
                    break;
                case WinnerCertificateElementTypes.EventName:
                    text = this.Competition.Event.Title;
                    break;
                case WinnerCertificateElementTypes.EventPlace:
                    text = this.Competition.Event.Place;
                    break;
                case WinnerCertificateElementTypes.NameOfSupervisor:
                    var supervisor =
                        this.Competition.Officials.FirstOrDefault(o => o.Role.Id == Roles.Supervisor);
                    if (supervisor != null)
                    {
                        text = supervisor.Official.NiceName;
                    }
                    break;
                case WinnerCertificateElementTypes.Place:
                    text = participant.Place;
                    break;
                case WinnerCertificateElementTypes.StaticText:
                    text = element.Text;
                    break;
                case WinnerCertificateElementTypes.AgeGroupLong:
                    text = participant.Competition.AgeGroup.AgeGroupName;
                    break;
                case WinnerCertificateElementTypes.GroupClassKind:
                    text = this.Competition.AgeGroup.ShortName + " " + this.Competition.Class.ClassShortName + " "
                           + this.Competition.Section.ShortName;
                    break;
                case WinnerCertificateElementTypes.GroupClassKindLong:
                    text = this.Competition.AgeGroup.AgeGroupName + " " + this.Competition.Class.ClassShortName + " "
                           + this.Competition.Section.SectionName;
                    break;
                case WinnerCertificateElementTypes.ManName:
                    text = participant.Couple.MansName;
                    break;
                case WinnerCertificateElementTypes.ManFirstName:
                    text = participant.Couple.FirstMan;
                    break;
                case WinnerCertificateElementTypes.ManLastName:
                    text = participant.Couple.LastMan;
                    break;
                case WinnerCertificateElementTypes.WomanName:
                    text = participant.Couple.WomansName;
                    break;
                case WinnerCertificateElementTypes.WomanFirstName:
                    text = participant.Couple.FirstWoman;
                    break;
                case WinnerCertificateElementTypes.WomanLastName:
                    text = participant.Couple.LastWoman;
                    break;
                case WinnerCertificateElementTypes.CompetitionKind:
                    text = this.Competition.Section.SectionName;
                    break;
                case WinnerCertificateElementTypes.CompetitionKindShort:
                    text = this.Competition.Section.ShortName;
                    break;
                case WinnerCertificateElementTypes.PlaceOwnCompetition:
                    text = participant.PlaceFromOwnCompetition.HasValue
                               ? participant.PlaceOwnCompetition
                               : participant.Place;
                    break;
                case WinnerCertificateElementTypes.NameOfAssociate:
                    var associate =
                        this.Competition.Officials.FirstOrDefault(o => o.Role.Id == Roles.Associate);
                    if (associate != null)
                    {
                        text = associate.Official.NiceName;
                    }
                    break;
                case WinnerCertificateElementTypes.NameOfChairperson:
                    var chairperson =
                        this.Competition.Officials.FirstOrDefault(o => o.Role.Id == Roles.Chairman);
                    if (chairperson != null)
                    {
                        text = chairperson.Official.NiceName;
                    }
                    break;
                case WinnerCertificateElementTypes.CountryClub:
                    text = participant.Couple.Country;
                    break;
            }

            var textBlock = new TextBlock
                                {
                                    FontFamily = new FontFamily(element.FontFamilyString),
                                    FontWeight = FontWeight.FromOpenTypeWeight(element.FontWeight),
                                    FontSize = element.FontSize,
                                    Foreground = new SolidColorBrush(element.Color),
                                    HorizontalAlignment = element.HorizontalAlignment,
                                    TextAlignment = TextAlignment.Center,
                                    Text = text,
                                    VerticalAlignment = element.VerticalAlignment
                                };

            switch (element.HorizontalAlignment)
            {
                case HorizontalAlignment.Center:
                    textBlock.TextAlignment = TextAlignment.Center;
                    break;
                case HorizontalAlignment.Left:
                    textBlock.TextAlignment = TextAlignment.Left;
                    break;
                case HorizontalAlignment.Right:
                    textBlock.TextAlignment = TextAlignment.Right;
                    break;
            }

            this.AddControlToPage(this.currentPage, textBlock);

            if (element.Rotation > 0)
            {
                textBlock.LayoutTransform = new RotateTransform(element.Rotation);
            }

            if (element.HorizontalAlignment == HorizontalAlignment.Center)
            {
                textBlock.Width = DotsFromCm(this.PageWidth - this.winnerCertificateDefinition.LeftMargin - this.winnerCertificateDefinition.RightMargin);
                FixedPage.SetLeft(textBlock, DotsFromCm(this.winnerCertificateDefinition.LeftMargin));
                FixedPage.SetTop(textBlock, DotsFromCm(element.Top));
            }
            else
            {
                FixedPage.SetLeft(textBlock, DotsFromCm(element.Left));
                FixedPage.SetTop(textBlock, DotsFromCm(element.Top));
            }

        }

        private BitmapImage ConvertBitmapToBitmapImage(Image bitmap)
        {
            var memoryStream = new MemoryStream();
            bitmap.Save(memoryStream, ImageFormat.Png);
            var bitmapImage = new BitmapImage();
            bitmapImage.BeginInit();
            bitmapImage.StreamSource = new MemoryStream(memoryStream.ToArray());
            bitmapImage.EndInit();

            return bitmapImage;
        }

        #endregion
    }
}