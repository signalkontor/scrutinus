﻿// // TPS.net TPS8 Scrutinus
// // PageTitleControl.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows.Controls;
using Scrutinus.Reports.BasicPrinting.PrintModels;

namespace Scrutinus.Reports.BasicPrinting.Controls
{
    /// <summary>
    ///     Interaction logic for PageTitleControl.xaml
    /// </summary>
    public partial class PageTitleControl : UserControl
    {
        #region Constructors and Destructors

        public PageTitleControl(PageTitlePrintModel model)
        {
            this.InitializeComponent();

            this.DataContext = model;
        }

        #endregion
    }
}