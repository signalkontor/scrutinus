﻿// // TPS.net TPS8 Scrutinus
// // AusrichterBelegControlBSW.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Windows.Controls;
using DataModel.Models;

namespace Scrutinus.Reports.BasicPrinting.Controls
{
    /// <summary>
    ///     Interaction logic for AusrichterBelegControl.xaml
    /// </summary>
    public partial class AusrichterBelegControlBSW : UserControl
    {
        #region Constructors and Destructors

        public AusrichterBelegControlBSW(Participant participant)
        {
            this.Participant = participant;

            this.NextClass = this.Participant.Startbuch.AgeGroup.ShortName + " "
                             + (participant.NewClass != null
                                    ? participant.NewClass.ClassShortName
                                    : participant.Class.ClassShortName);

            this.PointsAndPlacings = (participant.NewPoints.HasValue
                                          ? participant.NewPoints
                                          : participant.OriginalPoints) + " / "
                                     + (participant.NewPlacings.HasValue
                                            ? participant.NewPlacings
                                            : participant.OriginalPlacings);

            if (participant.TimeStampLastPointUpdate.HasValue)
            {
                this.TimeStamp = participant.Competition.ExternalId + " am "
                                 + participant.TimeStampLastPointUpdate.Value.ToString("dd.MM.yyyy HH:mm");
            }
            else
            {
                this.TimeStamp = participant.Competition.ExternalId + " am " + DateTime.Now.ToString("dd.MM.yyyy HH:mm");
            }

            var missingPoints = participant.NewPoints.HasValue
                                     ? participant.TargetPoints - participant.NewPoints.Value
                                     : participant.TargetPoints - participant.OriginalPoints;
            if (missingPoints < 0)
            {
                missingPoints = 0;
            }

            var missingPlacings = participant.NewPlacings.HasValue
                                       ? participant.TargetPlacings - participant.NewPlacings
                                       : participant.TargetPlacings - participant.OriginalPlacings;
            if (missingPlacings < 0)
            {
                missingPlacings = 0;
            }

            this.MissingPointsAndPlaced = string.Format(
                "{0} Punkte / {1} Platzierung(en) 1. - {2}. Platz",
                missingPoints,
                missingPlacings,
                participant.PlacingsUpto);

            this.InitializeComponent();
        }

        #endregion

        #region Public Properties

        public string MissingPointsAndPlaced { get; set; }

        public string NextClass { get; set; }

        public Participant Participant { get; set; }

        public string PointsAndPlacings { get; set; }

        public string TimeStamp { get; set; }

        #endregion
    }
}