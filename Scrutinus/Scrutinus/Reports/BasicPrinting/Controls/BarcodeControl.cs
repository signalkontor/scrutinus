﻿// // TPS.net TPS8 Scrutinus
// // BarcodeControl.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Zen.Barcode;
using Brushes = System.Windows.Media.Brushes;

namespace Scrutinus.Reports.BasicPrinting.Controls
{
    public class Ean128BarcodeControl : FrameworkElement
    {
        #region Constructors and Destructors

        static Ean128BarcodeControl()
        {
            ClipToBoundsProperty.OverrideMetadata(typeof(Ean128BarcodeControl), new FrameworkPropertyMetadata(true));
        }

        #endregion

        #region Public Properties

        public string Barcode
        {
            get
            {
                return (string)this.GetValue(BarcodeProperty);
            }
            set
            {
                this.SetValue(BarcodeProperty, value);
            }
        }

        #endregion

        #region Static Fields

        public static readonly DependencyProperty BarcodeProperty = DependencyProperty.Register(
            "Barcode",
            typeof(string),
            typeof(Ean128BarcodeControl),
            new PropertyMetadata(null, BarcodePropertyChangedCallback));

        private static readonly BarcodeDraw BarcodeDraw = BarcodeDrawFactory.Code128WithChecksum;

        #endregion

        // Using a DependencyProperty as the backing store for Barcode.  This enables animation, styling, binding, etc…

        #region Methods

        protected override void OnRender(DrawingContext drawingContext)
        {
            var size = new Rect(0, 0, this.ActualWidth, this.ActualHeight);

            drawingContext.DrawRectangle(Brushes.White, null, size);

            if (!string.IsNullOrEmpty(this.Barcode))
            {
                var image = BarcodeDraw.Draw(this.Barcode, new BarcodeMetrics1d(2, 4, 60));
                drawingContext.DrawImage(
                    this.GetImageSource(image),
                    new Rect(0, 0, this.ActualWidth, this.ActualHeight));
            }
        }

        private static void BarcodePropertyChangedCallback(
            DependencyObject sender,
            DependencyPropertyChangedEventArgs e)
        {
            var ctl = sender as Ean128BarcodeControl;
            if (ctl == null)
            {
                return;
            }
            ctl.InvalidateVisual();
        }

        private ImageSource GetImageSource(Image image)
        {
            var ms = new MemoryStream();
            image.Save(ms, ImageFormat.Png);
            ms.Position = 0;
            var bi = new BitmapImage();
            bi.BeginInit();
            bi.StreamSource = ms;
            bi.EndInit();

            return bi;
        }

        #endregion
    }
}