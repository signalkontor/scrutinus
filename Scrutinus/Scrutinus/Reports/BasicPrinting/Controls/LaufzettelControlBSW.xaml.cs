﻿// // TPS.net TPS8 Scrutinus
// // LaufzettelControlBSW.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Linq;
using System.Windows.Controls;
using DataModel.Models;

namespace Scrutinus.Reports.BasicPrinting.Controls
{
    /// <summary>
    ///     Interaction logic for LaufzettelControl.xaml
    /// </summary>
    public partial class LaufzettelControlBSW : UserControl
    {
        #region Constructors and Destructors

        public LaufzettelControlBSW(Participant participant)
        {
            this.Participant = participant;

            this.NextClass = this.Participant.Startbuch.AgeGroup.ShortName + " "
                             + (participant.NewClass != null
                                    ? participant.NewClass.ClassShortName
                                    : participant.Class.ClassShortName);

            if (participant.ClimbUp)
            {
                this.PointsAndPlacings = "0 / 0";
            }
            else
            {
                this.PointsAndPlacings = (participant.NewPoints.HasValue
                                              ? participant.NewPoints
                                              : participant.OriginalPoints) + " / "
                                         + (participant.NewPlacings.HasValue
                                                ? participant.NewPlacings
                                                : participant.OriginalPlacings);
            }

            if (participant.TimeStampLastPointUpdate.HasValue)
            {
                this.TimeStamp = participant.Competition.ExternalId + " am "
                                 + participant.TimeStampLastPointUpdate.Value.ToString("dd.MM.yyyy HH:mm");
            }
            else
            {
                this.TimeStamp = participant.Competition.ExternalId + " am " + DateTime.Now.ToString("dd.MM.yyyy HH:mm");
            }

            var missingPoints = 0;
            var missingPlacings = 0;
            var context = new ScrutinusContext();

            var climbupentry =
                context.ClimbUpTables.SingleOrDefault(
                    c =>
                    c.Class.Id == participant.Class.Id && c.AgeGroup.Id == participant.AgeGroup.Id
                    && c.LTV == participant.Couple.Region && c.Section.Id == participant.Competition.Section.Id);

            if (participant.ClimbUp)
            {
                if (climbupentry != null)
                {
                    missingPoints = climbupentry.Points;
                    missingPlacings = climbupentry.Placings;
                }
            }
            else
            {
                missingPoints = participant.NewPoints.HasValue
                                    ? participant.TargetPoints.Value - participant.NewPoints.Value
                                    : participant.TargetPoints.Value - participant.OriginalPoints.Value;

                missingPlacings = participant.NewPlacings.HasValue
                                      ? participant.TargetPlacings.Value - participant.NewPlacings.Value
                                      : participant.TargetPlacings.Value - participant.OriginalPlacings.Value;
            }

            if (missingPoints < 0)
            {
                missingPoints = 0;
            }

            if (missingPlacings < 0)
            {
                missingPlacings = 0;
            }

            this.MissingPointsAndPlaced = string.Format(
                "{0} Punkte / {1} Platzierung(en) 1. - {2}. Platz",
                missingPoints,
                missingPlacings,
                climbupentry != null ? climbupentry.MinimumPlace : 0);

            this.BarcodeData = this.GetBarcodeData();

            this.InitializeComponent();
        }

        #endregion

        #region Public Properties

        public string BarcodeData { get; set; }

        public string MissingPointsAndPlaced { get; set; }

        public string NextClass { get; set; }

        public Participant Participant { get; set; }

        public string PointsAndPlacings { get; set; }

        public string TimeStamp { get; set; }

        #endregion

        #region Methods

        private string GetBarcodeData()
        {
            var barcode = "$";

            barcode += this.Participant.Startbuch.Section.SectionName == "Standard" ? 1 : 2;
            barcode += this.GetClassCode(this.Participant.NewClass ?? this.Participant.Startbuch.Class);
            var points = this.Participant.NewPoints ?? this.Participant.OriginalPoints;
            var placings = this.Participant.NewPlacings ?? this.Participant.OriginalPlacings;

            barcode += string.Format("{0:0000}", points);
            barcode += string.Format("{0:00}", placings);
            barcode += this.GetClassCode(this.Participant.TargetClass);
            barcode += string.Format("{0:000}", this.Participant.TargetPoints);
            barcode += string.Format("{0:00}", this.Participant.TargetPlacings);
            barcode += this.Participant.MinimumPoints;
            barcode += this.Participant.PlacingsUpto;
            barcode += "$";

            return barcode;
        }

        private string GetClassCode(Class cl)
        {
            switch (cl.ClassShortName)
            {
                case null:
                    return "0";
                case "BSW":
                    return "7";
                case "D":
                    return "1";
                case "C":
                    return "2";
                case "B":
                    return "3";
                case "A":
                    return "4";
                case "S":
                    return "5";
            }

            return "-";
        }

        #endregion
    }
}