﻿// // TPS.net TPS8 Scrutinus
// // FinalTablePrintControl.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using DataModel.Models;
using Helpers.Skating;
using Scrutinus.Localization;
using Scrutinus.Pages;
using Printing = Scrutinus.Localization.Printing;

namespace Scrutinus.Reports.BasicPrinting.Controls
{
    /// <summary>
    ///     Interaction logic for FinalTablePrintControl.xaml
    /// </summary>
    public partial class FinalTablePrintControl : UserControl
    {
        #region Public Methods and Operators

        public void CreateGrids(
            DanceInRound dance,
            Dictionary<int, double> sums)
        {
            var numRows = this.model.Participants.Count + 1;
            var numColumns = this.model.Judges.Count + this.model.Participants.Count + 4; // Couple Number, Place and Total-Sum
            var grid = this.FinalTableGrid;

            // Add Rows and Columns
            for (var i = 0; i < numColumns; i++)
            {
                grid.ColumnDefinitions.Add(new ColumnDefinition());
            }
            for (var i = 0; i < numRows; i++)
            {
                grid.RowDefinitions.Add(new RowDefinition());
            }
            // Add Fields
            var row = 0;
            var col = 0;
            var judges = dance.Round.Competition.GetJudges().OrderBy(j => j.Sign).ToList();

            foreach (var judge in judges)
            {
                col++;
                // Headline: Sign of Judge
                grid.SetCellUiElement(new TextBlock { Text = judge.Sign, Foreground = Brushes.Blue }, col, 0);
                row = 0;
                var qualifieds = dance.Round.Qualifieds;
                foreach (var qualified in qualifieds.OrderBy(q => q.Participant.Number))
                {
                    row++;

                    var box = new TextBlock();

                    // if our input order is per couple, we set the tab index here:

                    var binding = new Binding("Mark")
                                      {
                                          Converter = new IntegerConverter(),
                                          ConverterParameter = this.model.Participants.Count
                                      };
                    var bindingError = new Binding("HasError")
                                           {
                                               Converter = new ErrorToBackgroundConverter(),
                                               Mode = BindingMode.OneWay
                                           };

                    box.SetBinding(TextBox.TextProperty, binding);
                    box.SetBinding(BackgroundProperty, bindingError);

                    var participant = this.model.DanceSkating.Single(d => d.DanceInRound.Dance.Id == dance.Dance.Id)
                            .CoupleResults.Single(p => p.Participant.Id == qualified.Participant.Id);

                    var mark =
                        participant.Marks.Single(
                            m =>
                            m.Participant.Id == qualified.Participant.Id && m.Dance.Id == dance.Dance.Id
                            && m.Judge.Id == judge.Id);

                    if (mark == null)
                    {
                        throw new Exception("Could not find Mark for a couple.");
                    }

                    box.Text = mark.Mark.ToString();

                    grid.SetCellUiElement(box, col, row);
                }
            }

            col += this.model.Participants.Count + 1;
            grid.SetCellUiElement(
                new TextBlock { Text = LocalizationService.Resolve(() => Printing.Rank), Foreground = Brushes.Blue },
                col,
                0);
            grid.SetCellUiElement(
                new TextBlock { Text = LocalizationService.Resolve(() => Printing.Sum), Foreground = Brushes.Blue },
                col + 1,
                0);
            grid.SetCellUiElement(
                new TextBlock
                    {
                        Text = LocalizationService.Resolve(() => Printing.NumberShort),
                        Foreground = Brushes.Blue
                    },
                0,
                0);

            var qualifiedList = dance.Round.Qualifieds.OrderBy(q => q.Participant.Number).ToList();

            for (var i = 0; i < qualifiedList.Count; i++)
            {
                grid.SetCellUiElement(new TextBlock { Text = qualifiedList[i].Participant.Number.ToString() }, 0, i + 1);

                var danceResult = this.model.DanceSkating.Single(m => m.DanceInRound.Dance.Id == dance.Dance.Id)
                        .CoupleResults.Single(p => p.Participant.Id == qualifiedList[i].Participant.Id);

                // var danceResult = model.Results.Single(r => r.DanceId == dance.DanceId && r.ParticipantId == qualifiedList[i].ParticipantId);

                // Add the Label for the Result:
                var label3 = new TextBlock { Text = danceResult.Place.ToString() };

                grid.SetCellUiElement(label3, this.model.Judges.Count + this.model.Participants.Count + 1, i + 1);

                var label4 = new TextBlock { Text = sums[qualifiedList[i].Participant.Number].ToString() };
                grid.SetCellUiElement(label4, this.model.Judges.Count + this.model.Participants.Count + 2, i + 1);

                for (var place = 1; place <= qualifiedList.Count; place++)
                {
                    var headPlaces = place == 1 ? "1." : string.Format("1.-{0}.", place);

                    col = this.model.Judges.Count + place;

                    grid.SetCellUiElement(new TextBlock { Text = headPlaces, Foreground = Brushes.Blue }, col, 0);

                    var label2 = new TextBlock
                                     {
                                         Text =
                                             danceResult.NumberPlaces[place - 1] > 0
                                                 ? danceResult.GetSkatingLabel(place - 1)
                                                 : ""
                                     };

                    grid.SetCellUiElement(label2, place + this.model.Judges.Count, i + 1);
                }
            }
            grid.UpdateLayout();
        }

        #endregion

        #region Constructors and Destructors

        private readonly SkatingViewModel model;

        public FinalTablePrintControl(
            SkatingViewModel model,
            DanceInRound danceRound,
            Dictionary<int, double> sums)
        {
            this.model = model;
            this.InitializeComponent();

            this.CreateGrids(danceRound, sums);
        }

        #endregion
    }
}