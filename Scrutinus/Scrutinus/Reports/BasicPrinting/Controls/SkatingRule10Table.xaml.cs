﻿// // TPS.net TPS8 Scrutinus
// // SkatingRule10Table.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows.Controls;

namespace Scrutinus.Reports.BasicPrinting.Controls
{
    /// <summary>
    ///     Interaction logic for SkatingRule10Table.xaml
    /// </summary>
    public partial class SkatingRule10Table : UserControl
    {
        #region Constructors and Destructors

        public SkatingRule10Table()
        {
            this.InitializeComponent();
        }

        #endregion
    }
}