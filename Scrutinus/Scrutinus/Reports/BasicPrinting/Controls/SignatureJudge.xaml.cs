﻿// // TPS.net TPS8 Scrutinus
// // SignatureJudge.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using DataModel.Models;

namespace Scrutinus.Reports.BasicPrinting.Controls
{
    /// <summary>
    ///     Interaction logic for SignatureJudge.xaml
    /// </summary>
    public partial class SignatureJudge : UserControl
    {
        #region Constructors and Destructors

        public SignatureJudge(string name, IEnumerable<Coordinate> signatureCoordinate)
        {
            this.SignatureName = name;

            this.InitializeComponent();

            if (signatureCoordinate != null && signatureCoordinate.Any())
            {
                this.DrawSignature(signatureCoordinate);
            }
            else
            {
                this.Canvas.Visibility = Visibility.Collapsed;
            }
        }

        #endregion

        #region Public Properties

        public string SignatureName { get; set; }

        private void DrawSignature(IEnumerable<Coordinate> coordinates)
        {
            foreach (var coordinate in coordinates)
            {
                var line = new Line()
                               {
                                   X1 = coordinate.X1 / 1.33,
                                   X2 = coordinate.X2 / 1.33,
                                   Y1 = coordinate.Y1 / 1.33,
                                   Y2 = coordinate.Y2 / 1.33,
                                   Stroke = Brushes.Black
                               };

                this.Canvas.Children.Add(line);
            }
           
        }

        #endregion
    }
}