﻿// // TPS.net TPS8 Scrutinus
// // SkatingRule11Table.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows.Controls;

namespace Scrutinus.Reports.BasicPrinting.Controls
{
    /// <summary>
    ///     Interaction logic for SkatingRule11Table.xaml
    /// </summary>
    public partial class SkatingRule11Table : UserControl
    {
        #region Constructors and Destructors

        public SkatingRule11Table()
        {
            this.InitializeComponent();
        }

        #endregion
    }
}