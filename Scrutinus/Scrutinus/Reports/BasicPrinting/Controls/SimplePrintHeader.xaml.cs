﻿// // TPS.net TPS8 Scrutinus
// // SimplePrintHeader.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Threading;
using System.Windows.Controls;
using System.Windows.Markup;
using Scrutinus.Reports.BasicPrinting.PrintModels;

namespace Scrutinus.Reports.BasicPrinting.Controls
{
    /// <summary>
    ///     Interaction logic for SimplePrintHeader.xaml
    /// </summary>
    public partial class SimplePrintHeader : UserControl
    {
        #region Constructors and Destructors

        public SimplePrintHeader(ComplexHeaderPrintModel model)
        {
            this.Language = XmlLanguage.GetLanguage(Thread.CurrentThread.CurrentCulture.Name);

            this.InitializeComponent();

            this.DataContext = model;
        }

        #endregion
    }
}