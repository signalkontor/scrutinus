﻿// // TPS.net TPS8 Scrutinus
// // SkatingRuleTable.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using Helpers.Skating;
using Scrutinus.Localization;

namespace Scrutinus.Reports.BasicPrinting.Controls
{
    public enum SkatingPrintTableType
    {
        Rule9,

        Rule10,

        Rule11
    }

    /// <summary>
    ///     Interaction logic for SkatingRuleTable.xaml
    /// </summary>
    public partial class SkatingRuleTable : UserControl
    {
        #region Constructors and Destructors

        public SkatingRuleTable(
            int numberOfCouples,
            int numberOfDances,
            IEnumerable<CoupleResult> model,
            SkatingPrintTableType tableType)
        {
            this.InitializeComponent();
            this.numberOfCouples = numberOfCouples;
            this.numberOfDances = numberOfDances;
            this.model = model;
            this.tableType = tableType;

            this.CreateTable();
        }

        #endregion

        #region Fields

        private readonly IEnumerable<CoupleResult> model;

        private readonly int numberOfCouples;

        private readonly int numberOfDances;

        private readonly SkatingPrintTableType tableType;

        #endregion

        #region Methods

        private void CreateTable()
        {
            // First, we need NumberOfCouple + 2 Line
            // And NumberOfCouples + 4 Columns
            for (var i = 0; i < this.numberOfCouples + 2; i++)
            {
                this.SkatingGrid.RowDefinitions.Add(new RowDefinition());
            }

            var columnCount = this.tableType == SkatingPrintTableType.Rule9
                                  ? this.numberOfDances + 1
                                  : this.numberOfCouples;
            columnCount += 3;

            for (var i = 0; i < columnCount; i++)
            {
                this.SkatingGrid.ColumnDefinitions.Add(new ColumnDefinition());
            }

            // Create the common parts
            this.SkatingGrid.SetCellUiElement(
                new TextBlock { Text = LocalizationService.Resolve(() => Printing.NumberShort) },
                0,
                0,
                1,
                2);
            if (this.tableType == SkatingPrintTableType.Rule9)
            {
                this.SkatingGrid.SetCellUiElement(
                    new TextBlock { Text = LocalizationService.Resolve(() => Printing.Sum) },
                    columnCount - 3,
                    1);
            }

            this.SkatingGrid.SetCellUiElement(
                new TextBlock { Text = LocalizationService.Resolve(() => Printing.Place) },
                columnCount - 2,
                0,
                1,
                2);
            this.SkatingGrid.SetCellUiElement(
                new TextBlock { Text = LocalizationService.Resolve(() => Printing.Resolved) },
                columnCount - 1,
                0,
                1,
                2);

            switch (this.tableType)
            {
                case SkatingPrintTableType.Rule9:
                    this.SkatingGrid.SetCellUiElement(
                        new TextBlock { Text = LocalizationService.Resolve(() => Printing.PlacesAllDances) },
                        1,
                        0,
                        this.numberOfDances + 1);
                    this.CreateTableRule9And11();
                    break;
                case SkatingPrintTableType.Rule10:
                    this.SkatingGrid.SetCellUiElement(
                        new TextBlock { Text = LocalizationService.Resolve(() => Printing.Rule10Long) },
                        1,
                        0,
                        this.numberOfCouples + 1);
                    this.CreateTableRule9And11();
                    break;
                case SkatingPrintTableType.Rule11:
                    this.SkatingGrid.SetCellUiElement(
                        new TextBlock { Text = LocalizationService.Resolve(() => Printing.Rule11Long) },
                        1,
                        0,
                        this.numberOfCouples + 1);
                    this.CreateTableRule9And11();
                    break;
            }

            if (this.tableType == SkatingPrintTableType.Rule9)
            {
                for (var i = 1; i <= this.numberOfDances; i++)
                {
                    this.SkatingGrid.SetCellUiElement(new TextBlock { Text = i.ToString() }, i, 1);
                }
            }
            else
            {
                for (var i = 1; i <= this.numberOfCouples; i++)
                {
                    this.SkatingGrid.SetCellUiElement(
                        new TextBlock { Text = i == 1 ? "1." : string.Format("1.-{0}.", i) },
                        i,
                        1);
                }
            }
        }

        private void CreateTableRule9And11()
        {
            if (this.model.Count() == 0)
            {
                return;
            }

            var rowOffset = 2;
            var colOffset = 1;

            var row = rowOffset;

            var maxIndex = this.numberOfCouples;

            foreach (var coupleResult in this.model.OrderBy(o => o.Participant.Number))
            {
                var number = new TextBlock { Text = coupleResult.Participant.Number.ToString() };
                this.SkatingGrid.SetCellUiElement(number, 0, row);

                var resolved = new TextBlock();
                var place = new TextBlock();
                

                if (coupleResult.IsResolved)
                {
                    resolved.Text = "X";
                    place.Text = coupleResult.Place.ToString();
                }
                else
                {
                    resolved.Text = ">";
                    place.Text = "";
                }

                this.SkatingGrid.SetCellUiElement(resolved, this.SkatingGrid.ColumnDefinitions.Count - 1, row);
                this.SkatingGrid.SetCellUiElement(place, this.SkatingGrid.ColumnDefinitions.Count - 2, row);

                for (var col = 0; col < maxIndex; col++)
                {
                    TextBlock textBlock = null;

                    var formatString = coupleResult.Sums[col] > 0
                                                ? string.Format(
                                                    "{0:#0} ({1:#0.0})",
                                                    coupleResult.NumberPlaces[col],
                                                    coupleResult.Sums[col])
                                                : string.Format("{0:#0}", coupleResult.NumberPlaces[col]);

                    textBlock = new TextBlock
                                    {
                                        Text =
                                            coupleResult.NumberPlaces[col] > 0
                                                ? formatString
                                                : ""
                                    };
                    

                    this.SkatingGrid.SetCellUiElement(textBlock, col + colOffset, row);
                }
                row++;
            }
        }

        #endregion
    }
}