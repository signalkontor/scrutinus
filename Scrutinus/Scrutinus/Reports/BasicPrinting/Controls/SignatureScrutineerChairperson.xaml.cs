﻿// // TPS.net TPS8 Scrutinus
// // SignatureScrutineerChairperson.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows.Controls;

namespace Scrutinus.Reports.BasicPrinting.Controls
{
    /// <summary>
    ///     Interaction logic for SignatureAssociateSupervisor.xaml
    /// </summary>
    public partial class SignatureScrutineerChairperson : UserControl
    {
        #region Constructors and Destructors

        public SignatureScrutineerChairperson()
        {
            this.InitializeComponent();
        }

        #endregion
    }
}