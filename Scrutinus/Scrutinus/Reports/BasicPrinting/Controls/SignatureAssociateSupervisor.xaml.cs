﻿// // TPS.net TPS8 Scrutinus
// // SignatureAssociateSupervisor.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows.Controls;

namespace Scrutinus.Reports.BasicPrinting.Controls
{
    /// <summary>
    ///     Interaction logic for SignatureAssociateSupervisor.xaml
    /// </summary>
    public partial class SignatureAssociateSupervisor : UserControl
    {
        #region Constructors and Destructors

        public SignatureAssociateSupervisor()
        {
            this.InitializeComponent();
        }

        #endregion
    }
}