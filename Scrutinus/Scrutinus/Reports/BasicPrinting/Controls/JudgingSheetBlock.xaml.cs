﻿// // TPS.net TPS8 Scrutinus
// // JudgingSheetBlock.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using DataModel;
using DataModel.Models;
using Scrutinus.Localization;
using Scrutinus.Reports.BasicPrinting.RoundPrinting;

namespace Scrutinus.Reports.BasicPrinting.Controls
{
    /// <summary>
    ///     Interaction logic for JudgingSheetBlock.xaml
    /// </summary>
    public partial class JudgingSheetBlock : UserControl
    {
        private readonly int numberParticipants = 15;

        private readonly bool showSum;

        private readonly Official official;
        private readonly Round round;

        #region Constructors and Destructors

        public JudgingSheetBlock(
            IEnumerable<Dance> dances,
            IEnumerable<Participant> participants,
            Round round,
            Official judge,
            bool showSum,
            bool showHeader)
        {
            this.InitializeComponent();

            this.round = round;
            this.official = judge;
            this.showSum = showSum;

            this.GenerateRowsAndColumns(dances.Count());

            this.CreateHeader(participants);

            this.CreateDances(dances, participants);

            if (showHeader)
            {
                this.HeaderControl.Content = new JudgingSheetHeader(round, judge);
            }
        }

        #endregion

        #region Methods

        private void CreateDances(IEnumerable<Dance> dances, IEnumerable<Participant> participants)
        {
            var row = 1;

            foreach (var dance in dances)
            {
                var danceBlock = new TextBlock
                                     {
                                         Text = dance.DanceName,
                                         FontSize = 14,
                                         VerticalAlignment = VerticalAlignment.Center,
                                         Margin= new Thickness(5, 0, 0, 5)
                                     };

                if(this.round.Markings.Any())
                {
                    var column = 1;

                    foreach (var participant in participants.OrderBy(p => p.Number))
                    {
                        var mark = this.round.Markings.FirstOrDefault(m => m.Dance != null && m.Dance.Id == dance.Id && m.Judge.Id == this.official.Id && m.Participant.Id == participant.Id);
                        if (mark != null && mark.Mark > 0)
                        {
                            var textblock = new TextBlock() { FontSize = 20, HorizontalAlignment = HorizontalAlignment.Center, VerticalAlignment = VerticalAlignment.Center};
                            this.JudgingBlockGrid.Children.Add(textblock);
                            textblock.SetValue(Grid.RowProperty, row);
                            textblock.SetValue(Grid.ColumnProperty, column);

                            if (this.round.MarksInputType == MarkingTypes.Marking)
                            {
                                textblock.Text = "X";
                            }
                            else
                            {
                                textblock.Text = mark.Mark.ToString();
                            }
                        }

                        column++;
                    }
                }

                this.JudgingBlockGrid.Children.Add(danceBlock);
                Grid.SetColumn(danceBlock, 0);
                Grid.SetRow(danceBlock, row);
                row++;
            }

            if (this.showSum)
            {
                var sumBlock = new TextBlock
                                   {
                                       Text = LocalizationService.Resolve(() => Printing.Sum),
                                       FontSize = 14,
                                       VerticalAlignment = VerticalAlignment.Center,
                                       Margin = new Thickness(5, 0, 0, 5)
                                    };

                this.JudgingBlockGrid.Children.Add(sumBlock);
                Grid.SetColumn(sumBlock, 0);
                Grid.SetRow(sumBlock, row);

                if (this.round.Markings.Any(m => m.Mark > 0))
                {
                    var column = 1;

                    foreach (var participant in participants.OrderBy(p => p.Number))
                    {
                        var mark = 0;
                        if (this.round.MarksInputType == MarkingTypes.Marking)
                        {
                            mark =
                                this.round.Markings.Count(
                                    m =>
                                        m.Mark > 0 && m.Judge.Id == this.official.Id &&
                                        m.Participant.Id == participant.Id);
                        }
                        else
                        {
                            mark =
                                this.round.Markings.Where(
                                    m =>
                                        m.Mark > 0 && m.Judge.Id == this.official.Id &&
                                        m.Participant.Id == participant.Id).Sum(m => m.Mark);
                        }   
                                             
                        var textblock = new TextBlock() { FontSize = 20, HorizontalAlignment = HorizontalAlignment.Center, VerticalAlignment = VerticalAlignment.Center };
                        this.JudgingBlockGrid.Children.Add(textblock);
                        textblock.SetValue(Grid.RowProperty, row);
                        textblock.SetValue(Grid.ColumnProperty, column);

                        textblock.Text = mark.ToString();
    
                        column++;
                    }
                }
            }
        }

        private void CreateHeader(IEnumerable<Participant> participants)
        {
            var column = 1;

            foreach (var participant in participants.OrderBy(p => p.Number))
            {
                var numberBlock = new TextBlock
                                      {
                                          Text = participant.Number.ToString(),
                                          FontSize = 14,
                                          HorizontalAlignment = HorizontalAlignment.Center,
                                          VerticalAlignment = VerticalAlignment.Center
                                      };
                this.JudgingBlockGrid.Children.Add(numberBlock);
                Grid.SetColumn(numberBlock, column);
                Grid.SetRow(numberBlock, 0);
                column++;
            }
        }

        private void GenerateRowsAndColumns(int numberDances)
        {
            this.JudgingBlockGrid.ColumnDefinitions.Add(
                new ColumnDefinition { Width = new GridLength(120, GridUnitType.Pixel) });

            for (var i = 0; i < this.numberParticipants; i++)
            {
                this.JudgingBlockGrid.ColumnDefinitions.Add(
                    new ColumnDefinition { Width = new GridLength(40, GridUnitType.Pixel) });
            }

            // i <= numberDances: We need two extra line for the header and sum!
            var rows = numberDances + 2;
            if (!this.showSum)
            {
                rows = numberDances + 1;
            }

            for (var i = 0; i < rows; i++)
            {
                this.JudgingBlockGrid.RowDefinitions.Add(
                    new RowDefinition { Height = new GridLength(26, GridUnitType.Pixel) });
            }

            for (var i = 0; i < this.numberParticipants + 1; i++)
            {
                this.AddVerticalLine(i);
            }
            this.AddVerticalLine(this.numberParticipants, HorizontalAlignment.Right);

            this.AddHorizonatalLine(0, VerticalAlignment.Top);
            for (var i = 0; i < rows; i++)
            {
                this.AddHorizonatalLine(i);
            }
        }

        private void AddHorizonatalLine(int row, VerticalAlignment alignment = VerticalAlignment.Bottom)
        {
            var border = new Border() { BorderBrush = Brushes.Black, Height = 2, BorderThickness = new Thickness(1), VerticalAlignment = alignment };

            Grid.SetColumn(border, 0);
            Grid.SetRow(border, row);
            Grid.SetColumnSpan(border, 1 + this.numberParticipants);

            this.JudgingBlockGrid.Children.Add(border);
        }

        private void AddVerticalLine(int column, HorizontalAlignment alignment = HorizontalAlignment.Left)
        {
            var border = new Border() { BorderBrush = Brushes.Black, Width = 2, BorderThickness = new Thickness(1), HorizontalAlignment = alignment };

            var rowCount = this.round.DancesInRound.Count + 1;
            if (this.showSum)
            {
                rowCount++;
            }

            Grid.SetColumn(border, column);
            Grid.SetRow(border, 0);
            Grid.SetRowSpan(border, rowCount);

            this.JudgingBlockGrid.Children.Add(border);
        }

        #endregion
    }
}