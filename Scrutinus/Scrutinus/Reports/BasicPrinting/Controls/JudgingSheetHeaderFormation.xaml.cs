﻿// // TPS.net TPS8 Scrutinus
// // JudgingSheetHeaderFormation.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows.Controls;
using DataModel;
using DataModel.Models;
using Scrutinus.Localization;

namespace Scrutinus.Reports.BasicPrinting.RoundPrinting
{
    /// <summary>
    ///     Interaction logic for JudgingSheetHeader.xaml
    /// </summary>
    public partial class JudgingSheetHeaderFormation : UserControl
    {
        #region Constructors and Destructors

        public JudgingSheetHeaderFormation(Round round, Official judge)
        {
            this.Round = round;
            this.Judge = judge;
            this.RoundCode = string.Format(
                "{0} # {1} # {2}",
                this.Round.Competition.ExternalId,
                this.Round.Number,
                this.Judge.ExternalId);

            if (round.MarksInputType == MarkingTypes.Marking || round.MarksInputType == MarkingTypes.MarksSumOnly)
            {
                var markString = round.MarksFrom == round.MarksTo
                                        ? round.MarksFrom.ToString()
                                        : round.MarksFrom + " - " + round.MarksTo;

                this.MarkString = string.Format(LocalizationService.Resolve(() => Printing.MarkingString), markString);
            }

            this.InitializeComponent();
        }

        #endregion

        #region Public Properties

        public Official Judge { get; set; }

        public string MarkString { get; set; }

        public Round Round { get; set; }

        public string RoundCode { get; set; }

        #endregion
    }
}