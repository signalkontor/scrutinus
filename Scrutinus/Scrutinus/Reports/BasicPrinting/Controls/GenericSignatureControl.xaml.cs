﻿// // TPS.net TPS8 Scrutinus
// // GenericSignatureControl.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows.Controls;

namespace Scrutinus.Reports.BasicPrinting.Controls
{
    /// <summary>
    /// Interaction logic for GenericSignatureControl.xaml
    /// </summary>
    public partial class GenericSignatureControl : UserControl
    {
        public GenericSignatureControl()
        {
            this.InitializeComponent();
        }
    }
}
