﻿// // TPS.net TPS8 Scrutinus
// // SkatingRule9Table.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using DataModel.Models;
using Helpers.Skating;
using Scrutinus.Localization;

namespace Scrutinus.Reports.BasicPrinting.Controls
{
    /// <summary>
    ///     Interaction logic for SkatingRule9Table.xaml
    /// </summary>
    public partial class SkatingRule9Table : UserControl
    {
        #region Constructors and Destructors

        public SkatingRule9Table(IEnumerable<Rule9ViewModel> model, IEnumerable<DanceInRound> dances)
        {
            this.InitializeComponent();

            this.model = model;
            this.dances = dances.ToList();

            this.CreateTable();
        }

        #endregion

        #region Methods

        private void CreateTable()
        {
            // First, we need NumberOfCouple + 2 Line
            // And NumberOfCouples + 4 Columns
            for (var i = 0; i < this.model.Count() + 2; i++)
            {
                this.SkatingGrid.RowDefinitions.Add(new RowDefinition());
            }

            var columnCount = this.model.First().PointsInDances.Count;
            columnCount += 4;

            for (var i = 0; i < columnCount; i++)
            {
                this.SkatingGrid.ColumnDefinitions.Add(new ColumnDefinition());
            }

            // Create Columns
            this.SkatingGrid.SetCellUiElement(
                new TextBlock { Text = LocalizationService.Resolve(() => Printing.NumberShort) },
                0,
                0,
                1,
                2);

            this.SkatingGrid.SetCellUiElement(
                new TextBlock { Text = LocalizationService.Resolve(() => Printing.Sum) },
                columnCount - 3,
                1);
            

            this.SkatingGrid.SetCellUiElement(
                new TextBlock { Text = LocalizationService.Resolve(() => Printing.Place) },
                columnCount - 2,
                0,
                1,
                2);

            this.SkatingGrid.SetCellUiElement(
                new TextBlock { Text = LocalizationService.Resolve(() => Printing.Resolved) },
                columnCount - 1,
                0,
                1,
                2);

            this.SkatingGrid.SetCellUiElement(
                new TextBlock { Text = LocalizationService.Resolve(() => Printing.PlacesAllDances) },
                1,
                0,
                this.dances.Count() + 1);


            for (var i = 1; i <= this.dances.Count(); i++)
            {
                this.SkatingGrid.SetCellUiElement(new TextBlock { Text = this.dances[i-1].Dance.ShortName }, i, 1);
            }

            var row = 2;

            foreach (var rule9ViewModel in this.model.OrderBy(m => m.Participant.Number))
            {
                var number = new TextBlock { Text = rule9ViewModel.Participant.Number.ToString() };
                this.SkatingGrid.SetCellUiElement(number, 0, row);

                var resolved = new TextBlock();
                var place = new TextBlock();
                var sum = new TextBlock();

                sum.Text = rule9ViewModel.Sum.ToString();

                if (rule9ViewModel.IsResolved)
                {
                    resolved.Text = "X";
                    place.Text = rule9ViewModel.Place.ToString();
                }
                else
                {
                    resolved.Text = ">";
                    place.Text = "";
                }

                this.SkatingGrid.SetCellUiElement(resolved, this.SkatingGrid.ColumnDefinitions.Count - 1, row);
                this.SkatingGrid.SetCellUiElement(place, this.SkatingGrid.ColumnDefinitions.Count - 2, row);
                this.SkatingGrid.SetCellUiElement(sum, this.SkatingGrid.ColumnDefinitions.Count - 3, row);

                for (var i = 0; i < this.dances.Count; i++)
                {
                    var textBlock = new TextBlock
                    {
                        Text = rule9ViewModel.PointsInDances[i] > 0 ? rule9ViewModel.PointsInDances[i].ToString("#0.0")
                                                        : ""
                    };

                    this.SkatingGrid.SetCellUiElement(textBlock, i + 1, row);
                }

                row++;
            }

        }

        #endregion

        #region Fields

        private readonly IEnumerable<Rule9ViewModel> model;

        private readonly List<DanceInRound> dances;

        #endregion
    }
}