﻿// // TPS.net TPS8 Scrutinus
// // SignatureSupervisorAssociate.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows.Controls;

namespace Scrutinus.Reports.BasicPrinting.Controls
{
    /// <summary>
    ///     Interaction logic for SignatureSupervisorAssociate.xaml
    /// </summary>
    public partial class SignatureSupervisorAssociate : UserControl
    {
        #region Constructors and Destructors

        public SignatureSupervisorAssociate()
        {
            this.InitializeComponent();
        }

        #endregion
    }
}