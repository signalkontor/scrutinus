﻿// // TPS.net TPS8 Scrutinus
// // AusrichterBelegControl.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Windows.Controls;
using DataModel.Models;

namespace Scrutinus.Reports.BasicPrinting.Controls
{
    /// <summary>
    ///     Interaction logic for AusrichterBelegControl.xaml
    /// </summary>
    public partial class AusrichterBelegControl : UserControl
    {
        #region Constructors and Destructors

        public AusrichterBelegControl(Participant participant)
        {
            this.Participant = participant;

            ClimbUpTable climbUpTable;
            this.NextClass = LaufzettelControl.GetNextClassString(participant, out climbUpTable);

            this.PointsAndPlacings = LaufzettelControl.GetPointsAndPlacingString(participant);

            if (participant.TimeStampLastPointUpdate.HasValue)
            {
                this.TimeStamp = participant.Competition.ExternalId + " am "
                                 + participant.TimeStampLastPointUpdate.Value.ToString("dd.MM.yyyy HH:mm");
            }
            else
            {
                this.TimeStamp = participant.Competition.ExternalId + " am " + DateTime.Now.ToString("dd.MM.yyyy HH:mm");
            }

            if (climbUpTable != null)
            {
                var missingPoints = LaufzettelControl.GetMissingPoints(participant, climbUpTable);
                var missingPlacings = LaufzettelControl.GetMissingPlacings(participant, climbUpTable);

                this.MissingPointsAndPlaced = string.Format(
                    "{0} Punkte / {1} Platzierung(en) 1. - {2}. Platz",
                    missingPoints,
                    missingPlacings,
                    climbUpTable.MinimumPlace);
            }
            this.InitializeComponent();
        }

        #endregion

        #region Public Properties

        public string MissingPointsAndPlaced { get; set; }

        public string NextClass { get; set; }

        public Participant Participant { get; set; }

        public string PointsAndPlacings { get; set; }

        public string TimeStamp { get; set; }

        #endregion
    }
}