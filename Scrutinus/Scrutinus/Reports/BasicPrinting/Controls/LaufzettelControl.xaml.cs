﻿// // TPS.net TPS8 Scrutinus
// // LaufzettelControl.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Linq;
using System.Windows.Controls;
using DataModel.Models;
using DtvEsvModule;

namespace Scrutinus.Reports.BasicPrinting.Controls
{
    /// <summary>
    ///     Interaction logic for LaufzettelControl.xaml
    /// </summary>
    public partial class LaufzettelControl : UserControl
    {
        #region Constructors and Destructors

        public LaufzettelControl(Participant participant)
        {
            this.Participant = participant;

            ClimbUpTable climbUpTable;
            this.NextClass = GetNextClassString(participant, out climbUpTable);

            this.PointsAndPlacings = GetPointsAndPlacingString(participant);

            if (participant.TimeStampLastPointUpdate.HasValue)
            {
                this.TimeStamp = participant.Competition.ExternalId + " am "
                                 + participant.TimeStampLastPointUpdate.Value.ToString("dd.MM.yyyy HH:mm");
            }
            else
            {
                this.TimeStamp = participant.Competition.ExternalId + " am " + DateTime.Now.ToString("dd.MM.yyyy HH:mm");
            }

            if (climbUpTable != null)
            {
                var missingPoints = GetMissingPoints(participant, climbUpTable);
                var missingPlacings = GetMissingPlacings(participant, climbUpTable);

                this.MissingPointsAndPlaced = string.Format(
                    "{0} Punkte / {1} Platzierung(en) 1. - {2}. Platz",
                    missingPoints,
                    missingPlacings,
                    climbUpTable != null ? climbUpTable.MinimumPlace : 0);
            }

            this.BarcodeData = this.GetBarcodeData();

            this.InitializeComponent();
        }

        #endregion

        #region Public Properties

        public string BarcodeData { get; set; }

        public string MissingPointsAndPlaced { get; set; }

        public string NextClass { get; set; }

        public Participant Participant { get; set; }

        public string PointsAndPlacings { get; set; }

        public string TimeStamp { get; set; }

        #endregion

        #region Public Methods and Operators

        public static int GetMissingPlacings(Participant participant, ClimbUpTable clt)
        {
            if (participant.ClimbUp)
            {
                return clt.Placings;
            }

            var missingPlacings = participant.NewPlacings.HasValue
                                       ? participant.TargetPlacings.Value - participant.NewPlacings
                                       : participant.TargetPlacings.Value - participant.OriginalPlacings;

            if (missingPlacings < 0)
            {
                missingPlacings = 0;
            }

            return missingPlacings.Value;
        }

        public static int GetMissingPoints(Participant participant, ClimbUpTable clt)
        {
            if (participant.ClimbUp)
            {
                return clt.Points;
            }

            var missingPoints = participant.NewPoints.HasValue
                                    ? participant.TargetPoints.Value - participant.NewPoints.Value
                                    : participant.TargetPoints.Value - participant.OriginalPoints.Value;

            if (missingPoints < 0)
            {
                missingPoints = 0;
            }

            return missingPoints;
        }

        public static string GetNextClassString(Participant participant, out ClimbUpTable climbUpTable)
        {
            var context = new ScrutinusContext();
            var result = "";
            AgeGroup OlderAgeGroup = null;
            Class lowerClass = null;

            ClimbUpTable normalTable = null;

            var classToLook = participant.ClimbUp ? participant.Class.Id + 1 : participant.Class.Id;

            normalTable =
                context.ClimbUpTables.SingleOrDefault(
                    c =>
                    c.Class.Id == classToLook && c.AgeGroup.Id == participant.Startbuch.AgeGroup.Id
                    && c.LTV == participant.Couple.Region && c.Section.Id == participant.Competition.Section.Id);

            if (normalTable == null)
            {
                normalTable = DtvApiImporter.GetAufstiegstabelleifNotFound(context, participant);

                if (participant.Startbuch.AgeGroup.Id < 7)
                {
                    OlderAgeGroup = context.AgeGroups.SingleOrDefault(a => a.Id == participant.AgeGroup.Id + 1);
                    lowerClass = context.Classes.SingleOrDefault(c => c.Id == participant.Class.Id - 1);
                }
            }
            if (lowerClass != null)
            {
                result = participant.Startbuch.AgeGroup.ShortName + " " + lowerClass.ClassShortName + " "
                         + participant.Startbuch.Section.SectionName + " / ";
            }

            if (normalTable != null)
            {
                result += normalTable.AgeGroup.ShortName + " " + normalTable.Class.ClassShortName + " "
                          + normalTable.Section.SectionName;
            }

            if (OlderAgeGroup != null)
            {
                result += OlderAgeGroup.ShortName + " " + participant.Startbuch.Class.ClassShortName + " "
                          + participant.Startbuch.Section.SectionName;
            }

            if (lowerClass == null && normalTable == null && OlderAgeGroup == null)
            {
                result += participant.Startbuch.AgeGroup.ShortName + " " + participant.Startbuch.Class.ClassShortName + " "
                          + participant.Startbuch.Section.SectionName;
            }

            climbUpTable = normalTable;

            return result;
        }

        public static string GetPointsAndPlacingString(Participant participant)
        {
            if (participant.ClimbUp)
            {
                return "0 / 0";
            }

            return string.Format(
                "{0} / {1}",
                participant.NewPoints.HasValue ? participant.NewPoints.Value : participant.OriginalPoints.Value,
                participant.NewPlacings.HasValue ? participant.NewPlacings.Value : participant.OriginalPlacings.Value);
        }

        #endregion

        #region Methods

        private string GetBarcodeData()
        {
            var barcode = "$";

            barcode += this.Participant.Startbuch.Section.SectionName == "Standard" ? 1 : 2;
            barcode += this.GetClassCode(this.Participant.NewClass ?? this.Participant.Startbuch.Class);
            var points = this.Participant.NewPoints ?? this.Participant.OriginalPoints;
            var placings = this.Participant.NewPlacings ?? this.Participant.OriginalPlacings;

            barcode += string.Format("{0:0000}", points);
            barcode += string.Format("{0:00}", placings);
            barcode += this.GetClassCode(this.Participant.TargetClass);
            barcode += string.Format("{0:000}", this.Participant.TargetPoints);
            barcode += string.Format("{0:00}", this.Participant.TargetPlacings);
            barcode += this.Participant.MinimumPoints;
            barcode += this.Participant.PlacingsUpto;
            barcode += "$";

            return barcode;
        }

        private string GetClassCode(Class cl)
        {
            if (cl == null)
            {
                return "0";
            }

            switch (cl.ClassShortName)
            {
                case null:
                    return "0";
                case "BSW":
                    return "7";
                case "D":
                    return "1";
                case "C":
                    return "2";
                case "B":
                    return "3";
                case "A":
                    return "4";
                case "S":
                    return "5";
            }

            return "-";
        }

        #endregion
    }
}