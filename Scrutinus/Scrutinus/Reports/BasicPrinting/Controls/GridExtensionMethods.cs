﻿// // TPS.net TPS8 Scrutinus
// // GridExtensionMethods.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows;
using System.Windows.Controls;

namespace Scrutinus.Reports.BasicPrinting.Controls
{
    public static class GridExtensionMethods
    {
        #region Public Methods and Operators

        public static void SetCellUiElement(this Grid g, UIElement element, int column, int row, int colSpan = 1)
        {
            Grid.SetColumn(element, column);
            Grid.SetRow(element, row);
            Grid.SetColumnSpan(element, colSpan);

            if (!g.Children.Contains(element))
            {
                g.Children.Add(element);
            }
        }

        public static void SetCellUiElement(
            this Grid g,
            UIElement element,
            int column,
            int row,
            int colSpan,
            int rowSpan)
        {
            Grid.SetColumn(element, column);
            Grid.SetRow(element, row);
            Grid.SetColumnSpan(element, colSpan);
            Grid.SetRowSpan(element, rowSpan);

            if (!g.Children.Contains(element))
            {
                g.Children.Add(element);
            }
        }

        #endregion
    }
}