﻿// // TPS.net TPS8 Scrutinus
// // JudgingSheetBlockFormation.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using DataModel.Models;
using Scrutinus.Localization;
using Scrutinus.Reports.BasicPrinting.RoundPrinting;

namespace Scrutinus.Reports.BasicPrinting.Controls
{
    /// <summary>
    ///     Interaction logic for JudgingSheetBlock.xaml
    /// </summary>
    public partial class JudgingSheetBlockFormation : UserControl
    {
        private Official official;

        private readonly IEnumerable<Participant> participants;
        private readonly Round round;

        #region Constructors and Destructors

        public JudgingSheetBlockFormation(
            IEnumerable<Participant> participants,
            Round round,
            Official judge,
            bool showHeader)
        {
            this.InitializeComponent();

            this.round = round;
            this.official = judge;
            this.participants = participants;

            this.CreateGrid();

            if (showHeader)
            {
                if (this.round.Competition.Section.IsTeam)
                {
                    this.HeaderControl.Content = new JudgingSheetHeaderFormation(round, judge);
                }
                else
                {
                    this.HeaderControl.Content = new JudgingSheetHeader(round, judge);
                }
            }
        }


        private void CreateGrid()
        {
            this.JudgingBlockGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Auto), SharedSizeGroup = "B" });
            this.AddVerticalLine(0);

            for (var i = 0; i < this.participants.Count(); i++)
            {
                this.JudgingBlockGrid.ColumnDefinitions.Add(new ColumnDefinition() { MinWidth = 30, SharedSizeGroup = "A" });
                this.AddVerticalLine(i + 1);
            }

            this.AddVerticalLine(this.participants.Count(), HorizontalAlignment.Right);

            for (var i = 0; i < 6; i++)
            {
                this.JudgingBlockGrid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(60)});
                this.AddHorizonatalLine(i);
            }

            this.AddHorizonatalLine(0, VerticalAlignment.Top);

            this.JudgingBlockGrid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(120) });
            this.AddHorizonatalLine(6);

            this.FillNumbers();
            this.FillLabels();
        }

        private void FillNumbers()
        {
            var column = 1;

            foreach (var participant in this.participants.OrderBy(p => p.Number))
            {
                this.CreateTextBlock(participant.Number.ToString(), 0, column);

                column++;
            }
        }

        private void FillLabels()
        {
            var text = this.CreateTextBlock(LocalizationService.Resolve(() => Printing.Music), 1, 0);
            text.HorizontalAlignment = HorizontalAlignment.Left;
            text.Margin = new Thickness(3, 0, 3, 0);
            text = this.CreateTextBlock(LocalizationService.Resolve(() => Printing.DancingPerformance), 2, 0);
            text.HorizontalAlignment = HorizontalAlignment.Left;
            text.Margin = new Thickness(3, 0, 3, 0);
            text = this.CreateTextBlock(LocalizationService.Resolve(() => Printing.PerformanceCheoraphy), 3, 0);
            text.HorizontalAlignment = HorizontalAlignment.Left;
            text.Margin = new Thickness(3, 0, 3, 0);
            text = this.CreateTextBlock(LocalizationService.Resolve(() => Printing.Characteristic), 4, 0);
            text.HorizontalAlignment = HorizontalAlignment.Left;
            text.Margin = new Thickness(3, 0, 3, 0);
            text = this.CreateTextBlock(LocalizationService.Resolve(() => Printing.SummeOfPoints), 5, 0);
            text.HorizontalAlignment = HorizontalAlignment.Left;
            text.Margin = new Thickness(3, 0, 3, 0);

            var signature = new GenericSignatureControl();
            signature.HorizontalAlignment = HorizontalAlignment.Center;
            signature.VerticalAlignment = VerticalAlignment.Bottom;
            Grid.SetColumn(signature, 0);
            Grid.SetRow(signature, 6);
            this.JudgingBlockGrid.Children.Add(signature);

            text = this.CreateTextBlock(LocalizationService.Resolve(() => Printing.MarkOrPlace), 6, 0);
            text.FontSize = 12;
            text.VerticalAlignment = VerticalAlignment.Top;
            text.HorizontalAlignment = HorizontalAlignment.Right;
            text.Margin = new Thickness(0, 3, 3, 0);
        }

        private TextBlock CreateTextBlock(string text, int row, int column)
        {
            var textBlock = new TextBlock()
            {
                Text = text,
                FontSize = 16,
                VerticalAlignment = VerticalAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Center
            };

            Grid.SetColumn(textBlock, column);
            Grid.SetRow(textBlock, row);
            this.JudgingBlockGrid.Children.Add(textBlock);

            return textBlock;
        }

        private void AddHorizonatalLine(int row, VerticalAlignment alignment = VerticalAlignment.Bottom)
        {
            var border =new Border() { BorderBrush = Brushes.Black, Height = 2, BorderThickness = new Thickness(1), VerticalAlignment = alignment};

            Grid.SetColumn(border, 0);
            Grid.SetRow(border, row);
            Grid.SetColumnSpan(border, 1 + this.participants.Count());

            this.JudgingBlockGrid.Children.Add(border);
        }

        private void AddVerticalLine(int column, HorizontalAlignment alignment = HorizontalAlignment.Left)
        {
            var border = new Border() { BorderBrush = Brushes.Black, Width = 2, BorderThickness = new Thickness(1), HorizontalAlignment = alignment };

            Grid.SetColumn(border, column);
            Grid.SetRow(border, 0);
            Grid.SetRowSpan(border, 8);

            this.JudgingBlockGrid.Children.Add(border);
        }

        #endregion

        #region Methods

        #endregion
    }
}