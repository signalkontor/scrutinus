﻿// // TPS.net TPS8 Scrutinus
// // SignatureChairpersonScrutineer.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows.Controls;

namespace Scrutinus.Reports.BasicPrinting.Controls
{
    /// <summary>
    ///     Interaction logic for SignatureAssociateSupervisor.xaml
    /// </summary>
    public partial class SignatureChairpersonScrutineer : UserControl
    {
        #region Constructors and Destructors

        public SignatureChairpersonScrutineer()
        {
            this.InitializeComponent();
        }

        #endregion
    }
}