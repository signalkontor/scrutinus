﻿// // TPS.net TPS8 Scrutinus
// // TablePrintHelper.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using Scrutinus.Reports.BasicPrinting.Controls;

namespace Scrutinus.Reports.BasicPrinting
{
    public class ColumnDescriptor<T>
    {
        public ColumnDescriptor()
        {
            this.FontSize = 0;
        }

        #region Public Properties

        public string Header { get; set; }

        public Func<T, object> ValueFunc { get; set; }

        public GridLength Width { get; set; }

        public double FontSize { get; set; }

        #endregion
    }

    public static class TablePrintHelper
    {
        #region Public Methods and Operators

        public static FixedPage PrintTable<T>(
            FixedPage page,
            IList<T> data,
            IList<ColumnDescriptor<T>> columns,
            Func<FixedPage> newPageFunc,
            bool createHeader,
            Size pageSize,
            double leftMargin,
            double rightMargin,
            double top,
            double pageHeaderHeight,
            out double height)
        {
            height = 0;

            if (data == null)
            {
                return page;
            }

            var index = 0;
            var newPageCreated = false;

            FixedPage newPage = null;

            if (data.Count == 0)
            {
                CreateGrid(
                    index,
                    page,
                    columns,
                    data,
                    createHeader,
                    out newPage,
                    newPageFunc,
                    pageSize,
                    leftMargin,
                    rightMargin,
                    top,
                    out height,
                    out newPageCreated);

                return newPage;
            }

            while (index < data.Count)
            {
                index = CreateGrid(
                    index,
                    page,
                    columns,
                    data,
                    createHeader,
                    out newPage,
                    newPageFunc,
                    pageSize,
                    leftMargin,
                    rightMargin,
                    top,
                    out height,
                    out newPageCreated);

                if (newPageCreated)
                {
                    top = pageHeaderHeight + 20;
                }

                page = newPage;
            }

            return newPage;
        }

        #endregion

        #region Methods

        private static FixedPage AddControlToPage(
            FixedPage page,
            FrameworkElement control,
            Size pageSize,
            double left,
            double top,
            Func<FixedPage> newPageFunc)
        {
            FixedPage.SetLeft(control, AbstractPrinter.DotsFromCm(left)); // left margin
            FixedPage.SetTop(control, AbstractPrinter.DotsFromCm(top)); // top margin
            page.Children.Add(control);

            if (!ControlFitsOnPage(page, control, pageSize, top))
            {
                page.Children.Remove(control);
                page = newPageFunc();
                FixedPage.SetLeft(control, AbstractPrinter.DotsFromCm(left)); // left margin
                FixedPage.SetTop(control, top); // top margin
                page.Children.Add(control);
            }

            return page;
        }

        private static void AddDataRow<T>(
            int index,
            int currentGridRowNumber,
            Grid grid,
            IList<ColumnDescriptor<T>> columns,
            IList<T> data)
        {
            grid.RowDefinitions.Add(new RowDefinition());

            for (var column = 0; column < columns.Count; column++)
            {
                var textValue = columns[column].ValueFunc(data[index]);

                var text = new TextBlock
                               {
                                   Text = textValue != null ? textValue.ToString() : "",
                                   Margin = new Thickness(4),
                                   TextWrapping = TextWrapping.WrapWithOverflow
                               };
                grid.SetCellUiElement(text, column, currentGridRowNumber);
            }
        }

        private static bool ControlFitsOnPage(FixedPage page, FrameworkElement control, Size pageSize, double top)
        {
            MeasurePage(page, pageSize);

            return top + control.ActualHeight < pageSize.Height;
        }

        private static void CreateColumns<T>(IList<ColumnDescriptor<T>> columns, Grid grid)
        {
            foreach (var t in columns)
            {
                grid.ColumnDefinitions.Add(new ColumnDefinition { Width = t.Width });
            }
        }

        private static int CreateGrid<T>(
            int startIndex,
            FixedPage currentPage,
            IList<ColumnDescriptor<T>> columns,
            IList<T> data,
            bool createHeader,
            out FixedPage page,
            Func<FixedPage> createPageFunc,
            Size pageSize,
            double leftMarginInCm,
            double rightMarginInCm,
            double topInDots,
            out double controlHeight,
            out bool newPageCreated)
        {
            if (currentPage == null)
            {
                page = createPageFunc();
            }
            else
            {
                page = currentPage;
            }

            var grid = new Grid
                           {
                               Width =
                                   pageSize.Width - AbstractPrinter.DotsFromCm(leftMarginInCm)
                                   - AbstractPrinter.DotsFromCm(rightMarginInCm)
                           };

            FixedPage.SetLeft(grid, AbstractPrinter.DotsFromCm(leftMarginInCm)); // left margin
            FixedPage.SetTop(grid, topInDots); // top margin

            CreateColumns(columns, grid);

            page = AddControlToPage(
                page,
                grid,
                pageSize,
                leftMarginInCm,
                AbstractPrinter.CmFromDots(topInDots),
                createPageFunc);

            var currentGridRowNumber = 0;

            if (createHeader)
            {
                CreateHeader(grid, columns);
                currentGridRowNumber = 1;
            }

            while (startIndex < data.Count && ControlFitsOnPage(page, grid, pageSize, topInDots))
            {
                AddDataRow(startIndex, currentGridRowNumber, grid, columns, data);
                currentGridRowNumber++;
                startIndex++;
            }

            if (startIndex < data.Count)
            {
                // grid to heigh -> remove last added line + children of this line:
                if (startIndex > 0)
                {
                    var uIElements =
                        grid.Children.Cast<UIElement>().Where(e => Grid.GetRow(e) == currentGridRowNumber - 1).ToList();

                    foreach (var uiElement in uIElements)
                    {
                        grid.Children.Remove(uiElement);
                    }

                    grid.RowDefinitions.RemoveAt(grid.RowDefinitions.Count - 1);
                    startIndex--;
                }

                page = createPageFunc();
                controlHeight = grid.ActualHeight;
                newPageCreated = true;
                return startIndex;
            }

            MeasurePage(page, pageSize);
            controlHeight = grid.ActualHeight;
            newPageCreated = false;

            return startIndex;
        }

        private static void CreateHeader<T>(Grid grid, IList<ColumnDescriptor<T>> columns)
        {
            grid.RowDefinitions.Add(new RowDefinition());

            for (var index = 0; index < columns.Count; index++)
            {
                var border = new Border
                                 {
                                     Background = new SolidColorBrush(Colors.LightGray),
                                     Height = 30,
                                     BorderBrush = new SolidColorBrush(Colors.Transparent)
                                 };
                border.Child = new TextBlock
                                   {
                                       Text = columns[index].Header,
                                       Margin = new Thickness(4, 0, 0, 0),
                                       VerticalAlignment = VerticalAlignment.Center,
                                   };
                grid.SetCellUiElement(border, index, 0);
            }
        }

        private static void MeasurePage(FixedPage page, Size pageSize)
        {
            page.Measure(pageSize);
            page.Arrange(new Rect(new Point(), pageSize));
            page.UpdateLayout();
        }

        #endregion
    }
}