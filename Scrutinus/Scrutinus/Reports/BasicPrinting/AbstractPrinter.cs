﻿// // TPS.net TPS8 Scrutinus
// // AbstractPrinter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Printing;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Markup;
using System.Windows.Media;
using DataModel.Models;
using MahApps.Metro.Controls;
using Scrutinus.Localization;
using Scrutinus.Reports.BasicPrinting.Controls;
using Scrutinus.Reports.BasicPrinting.PrintModels;

namespace Scrutinus.Reports.BasicPrinting
{
    public abstract class AbstractPrinter
    {
        protected Competition Competition;

        protected FixedDocument Doc;

        protected double HeaderHeigthInDots;


        protected double pageHeaderHeight;
        /// <summary>
        /// Setzt Seitengröße auf A4 und erzeugt ein leeres Dokument
        /// </summary>
        protected AbstractPrinter(Competition competition)
        {
            // Set default values
            this.PageHeight = 29.7;
            this.PageWidth = 21;
            this.LeftPageMargin = 1;
            this.RightPageMargin = 1;
            this.TopPageMargin = 1;
            this.ButtomPageMargin = 0.8;
            this.PrintCopyRight = true;

            this.Competition = competition;
        }

        protected AbstractPrinter(Competition competition, bool landscapeMode) : this(competition)
        {
            if (landscapeMode)
            {
                this.PageHeight = 21;
                this.PageWidth = 29.7;
            }
        }

        public FixedDocument Document
        {
            get { return this.Doc; }
            internal set { this.Doc = value; }
        }

        public double CurrentPageContentStart { get; set; }

        public int CurrentPageNumber { get; set; }

        public bool ShowPageNumbers { get; set; }

        public bool ShowComplexHeader { get; set; }

        public bool ShowSimpleHeader { get; set; }

        public bool ShowFederationHeader { get; set; }

        public string DocumentTitle { get; set; }

        public string CompetitionTitle { get; set; }

        public string RoundTitle { get; set; }

        public bool PrintFloor { get; set; }

        public bool PrintCopyRight { get; set; }

        public string PlaceOfCompetition { get; set; }

        public string OrganizerOfCompetition { get; set; }

        public string ExecutorOfCompetition { get; set; }

        public string KindOfCompetition { get; set; }

        public string GroupAndClassOfCompetition { get; set; }

        public string SectionOfCompetition { get; set; }

        public double PageWidth { get; set; }

        public double PageHeight { get; set; }

        public double LeftPageMargin { get; set; }

        public double RightPageMargin { get; set; }

        public double TopPageMargin { get; set; }

        public double ButtomPageMargin { get; set; }

        public bool PrintLogo { get; set; }

        public double UseablePageWidthInDots
        {
            get { return DotsFromCm(this.PageWidth - this.LeftPageMargin - this.RightPageMargin); }
        }

        public double UseablePageWidthInCm
        {
            get { return this.PageWidth - this.LeftPageMargin - this.RightPageMargin; }
        }

        protected void CreateDocument()
        {
            this.Doc = new FixedDocument();

            this.Doc.Language = XmlLanguage.GetLanguage(Thread.CurrentThread.CurrentCulture.Name);

            this.Doc.DocumentPaginator.PageSize = new Size(
                DotsFromCm(this.PageWidth),
                DotsFromCm(this.PageHeight));

        }

        protected void CreateContent()
        {
            this.CreateDocument();

            this.CreateCustomContent();

            this.FinalizeDocument();
        }

        protected abstract void CreateCustomContent();

        /// <summary>
        /// Wird aufgerufen nachdem eine Seite erzeugt wurde. Hier kann dann auf die Seite geschrieben werden
        /// wass auf jeder Seite drauf stehen soll wie z.B. eine Überschrift
        /// </summary>
        /// <param name="page"></param>
        protected abstract void AddCommenContent(FixedPage page);

        /// <summary>
        /// Erzeugt eine Seite im Standardlayout (Logo, Adresse, ...)
        /// </summary>
        /// <returns></returns>
        public FixedPage CreatePage()
        {
            this.CurrentPageNumber++;

            var pageContent = new PageContent()
            {
                Height = DotsFromCm(this.PageHeight),
                Width = DotsFromCm(this.PageWidth)
            };

            var fixedPage = new FixedPage();

            this.HeaderHeigthInDots = 0;
            pageContent.Child = fixedPage;

            // ((IAddChild)pageContent).AddChild(fixedPage);
            this.Doc.Pages.Add(pageContent);
            fixedPage.Background = Brushes.White;

            if (this.PrintFloor && this.Competition != null && !string.IsNullOrWhiteSpace(this.Competition.Floor))
            {
                this.AddFloor(fixedPage, this.Competition.Floor);
            }

            if (this.Doc.Pages.Count > 1 && this.Competition != null)
            {
                if (this.ShowComplexHeader)
                {
                    this.AddComplexHeader(fixedPage);
                }
                else
                {
                    this.AddHeaderOf2ndPage(fixedPage);
                }
            }
            else
            {
                if (this.ShowFederationHeader)
                {
                    this.AddDtvHeader(fixedPage);
                }
                else
                {
                    if (this.ShowComplexHeader)
                    {
                        this.AddComplexHeader(fixedPage);
                    }
                    else if (this.ShowSimpleHeader)
                    {
                        this.AddSimpleHeader(fixedPage);
                    }
                }

            }

            if (this.HeaderHeigthInDots == 0)
            {
                this.HeaderHeigthInDots = DotsFromCm(this.TopPageMargin);
            }

            if (MainWindow.MainWindowHandle != null && this.PrintCopyRight)
            {
                // Add the copyright footer:
                this.AddText(
                    fixedPage,
                    string.Format(
                        LocalizationService.Resolve(() => Printing.CopyRightString),
                        DateTime.Now.Year,
                        MainWindow.License.LicenseHolder),
                    29,
                    this.LeftPageMargin,
                    12);

                this.AddCommenContent(fixedPage);
            }

            return fixedPage;
        }

        protected void AddFloor(FixedPage page, string floor)
        {
            var border = this.AddText(
                page,
                floor,
                7,
                this.LeftPageMargin,
                300, this.PageWidth - this.LeftPageMargin - this.RightPageMargin,
                TextAlignment.Center,
                FontWeights.Bold,
                0) as Border;

            var element = border.Child as TextBlock;
            element.Foreground = Brushes.LightGray;
        }

        protected void AddComplexHeader(FixedPage page)
        {
            this.HeaderHeigthInDots = DotsFromCm(this.TopPageMargin);

            var header =
                new ComplexPrintHeader(
                    new ComplexHeaderPrintModel()
                    {
                        CompetitionTitle = this.CompetitionTitle,
                        RoundTitle = this.RoundTitle,
                        CompetitionDate =
                            this.Competition.StartTime.HasValue
                                ? this.Competition.StartTime.Value
                                : DateTime.Now,
                        CompetitionType = this.Competition.CompetitionType.TypeString,
                        CompetitonKind = this.Competition.CompetitionType.CompetitionKind,
                        GroupClass = this.Competition.AgeGroup.ShortName + " / " + this.Competition.Class.ClassShortName,
                        Host = this.Competition.Event.Executor,
                        Organizer = this.Competition.Event.Organizer,
                        Section = this.Competition.Section.SectionName,
                        Place = this.Competition.Event.Place
                    });

            header.Width = this.UseablePageWidthInDots;
            this.MeasureControl(header);

            FixedPage.SetLeft(header, DotsFromCm(this.LeftPageMargin)); // left margin
            FixedPage.SetTop(header, DotsFromCm(this.TopPageMargin)); // top margin

            page.Children.Add(header);

            this.HeaderHeigthInDots += header.ActualHeight;
            this.AddTitleBar(page);
            this.pageHeaderHeight = this.HeaderHeigthInDots;
        }

        protected void AddTitleBar(FixedPage page)
        {
            if (string.IsNullOrEmpty(this.DocumentTitle))
            {
                return;
            }

            this.HeaderHeigthInDots += 10; // small margin to top
            var title = new PageTitleControl(
                new PageTitlePrintModel()
                {
                    CompetitionCode = this.Competition?.Id.ToString() ?? "",
                    PageNumber = this.CurrentPageNumber,
                    Title = this.DocumentTitle
                }) {Width = this.UseablePageWidthInDots};

            FixedPage.SetLeft(title, DotsFromCm(this.LeftPageMargin)); // left margin
            FixedPage.SetTop(title, this.HeaderHeigthInDots); // top margin

            this.MeasureControl(title);
            page.Children.Add(title);

            this.HeaderHeigthInDots += title.ActualHeight;
        }

        protected void MeasurePageXX(FixedPage page)
        {
            var pageSize = this.Doc.DocumentPaginator.PageSize;
            page.Measure(pageSize);
            page.Arrange(new Rect(new Point(), pageSize));
            page.UpdateLayout();
        }

        protected void MeasureControl(UIElement uiElement)
        {
            var pageSize = this.Doc.DocumentPaginator.PageSize;
            var stackpanel = new StackPanel();
            stackpanel.Children.Add(uiElement);
            stackpanel.Measure(pageSize);
            stackpanel.Arrange(new Rect(new Point(), pageSize));
            stackpanel.UpdateLayout();
            stackpanel.Children.Remove(uiElement);
        }

        private TextBlock AddTextToGrid(
            Grid grid,
            string text,
            FontWeight fontWeight,
            int fontSize,
            int row,
            int column,
            HorizontalAlignment horizontalAlignment
        )
        {
            var textBlock = this.CreateTextBlock(text, fontSize, TextAlignment.Left, fontWeight);
            textBlock.HorizontalAlignment = horizontalAlignment;
            Grid.SetColumn(textBlock, column);
            Grid.SetRow(textBlock, row);
            grid.Children.Add(textBlock);

            return textBlock;
        }

        protected bool ControlFitsOnPage(FixedPage page, FrameworkElement control)
        {
            var maxPageSize = this.Document.DocumentPaginator.PageSize.Height - DotsFromCm(this.TopPageMargin)
                              - DotsFromCm(this.ButtomPageMargin);

            this.MeasureControl(control);

            return this.HeaderHeigthInDots + control.ActualHeight < maxPageSize;
        }


        protected FixedPage AddControlToPage(FixedPage page, FrameworkElement control)
        {
            return this.AddControlToPage(page, control, this.LeftPageMargin, CmFromDots(this.HeaderHeigthInDots));
        }

        protected FixedPage AddControlToPage(FixedPage page, FrameworkElement control, double left, double top)
        {
            FixedPage.SetLeft(control, DotsFromCm(left)); // left margin
            FixedPage.SetTop(control, DotsFromCm(top)); // top margin

            if (!this.ControlFitsOnPage(page, control))
            {
                page = this.CreatePage();
                FixedPage.SetLeft(control, DotsFromCm(this.LeftPageMargin)); // left margin
                FixedPage.SetTop(control, this.HeaderHeigthInDots); // top margin
                page.Children.Add(control);
            }
            else
            {
                page.Children.Add(control);
            }

            this.HeaderHeigthInDots += control.ActualHeight;
            return page;
        }

        protected void AddDtvHeader(FixedPage page)
        {
            this.HeaderHeigthInDots = DotsFromCm(1);

            var grid = new Grid();
            grid.ColumnDefinitions.Add(new ColumnDefinition() {Width = new GridLength(1, GridUnitType.Star)});
            grid.ColumnDefinitions.Add(new ColumnDefinition() {Width = new GridLength(1, GridUnitType.Star)});
            grid.ColumnDefinitions.Add(new ColumnDefinition() {Width = new GridLength(1, GridUnitType.Star)});
            grid.Width = DotsFromCm(this.PageWidth) - DotsFromCm(this.LeftPageMargin)
                         - DotsFromCm(this.RightPageMargin);

            this.AddTextToGrid(grid, "DTV", FontWeights.Bold, 30, 0, 0, HorizontalAlignment.Left);
            this.AddTextToGrid(grid, "Turnierbericht Teil 1", FontWeights.Bold, 20, 0, 1, HorizontalAlignment.Center);

            this.AddTextToGrid(grid, DateTime.Now.ToShortDateString(), FontWeights.Normal, 12, 0, 2,
                HorizontalAlignment.Right);

            this.AddControlToPage(page, grid);

            this.HeaderHeigthInDots += 30;
            this.pageHeaderHeight = grid.ActualHeight + DotsFromCm(this.TopPageMargin);
        }

        protected void AddSimpleHeader(FixedPage page)
        {
            this.HeaderHeigthInDots = DotsFromCm(this.TopPageMargin);

            if (this.Competition == null)
            {
                return;
            }

            var header = new SimplePrintHeader(
                new ComplexHeaderPrintModel
                {
                    CompetitionTitle = this.CompetitionTitle,
                    RoundTitle = this.RoundTitle,
                    CompetitionDate =
                        this.Competition.StartTime.HasValue ? this.Competition.StartTime.Value : DateTime.Now,
                    CompetitionType = this.Competition.CompetitionType.ShortName,
                    GroupClass = this.Competition.AgeGroup.ShortName + " / " + this.Competition.Class.ClassShortName,
                    CompetitonKind = this.Competition.CompetitionType.CompetitionKind,
                    Host = this.Competition.Event.Executor,
                    Organizer = this.Competition.CompetitionType.Federation,
                    Section = this.Competition.Section.SectionName,
                    Place = this.Competition.Event.Place,
                    PrintLogo = this.PrintLogo,
                }) {Width = this.UseablePageWidthInDots,};

            FixedPage.SetLeft(header, DotsFromCm(this.LeftPageMargin)); // left margin
            FixedPage.SetTop(header, DotsFromCm(this.TopPageMargin)); // top margin

            this.MeasureControl(header);

            this.AddControlToPage(page, header);

            this.HeaderHeigthInDots += 10;
            
            this.AddTitleBar(page);

            this.pageHeaderHeight = this.HeaderHeigthInDots;
        }

        public void AddHeaderOf2ndPage(FixedPage page)
        {
            if (this.ShowSimpleHeader || this.ShowComplexHeader)
            {
                this.AddSimpleHeader(page);
            }
        }

        protected void InsertVerticalLine(FixedPage page)
        {
            this.AddHorizontalLine(
                page,
                CmFromDots(this.HeaderHeigthInDots),
                this.LeftPageMargin,
                this.UseablePageWidthInCm);

            this.HeaderHeigthInDots += 10;
        }

        public static double DotsFromCm(double centimeter)
        {
            return centimeter * 96 / 2.54;
        }

        public static double CmFromDots(double dots)
        {
            return dots * 2.54 / 96;
        }

        protected FrameworkElement AddText(
            FixedPage Page,
            string Text,
            double Top,
            double Left,
            int FontSize,
            FontWeight fontWeight)
        {
            return this.AddText(Page, Text, Top, Left, FontSize, 0, TextAlignment.Left, fontWeight, 0);
        }

        /// <summary>
        /// Text einer Seite hinzufügen
        /// </summary>
        /// <param name="Page">Die Seire</param>
        /// <param name="Text">Text</param>
        /// <param name="Top">in Cm</param>
        /// <param name="Left">in Cm</param>
        /// <param name="FontSize">in Punkten</param>
        /// <param name="Width"></param>
        /// <param name="alignment"></param>
        protected FrameworkElement AddText(
            FixedPage Page,
            string Text,
            double Top,
            double Left,
            int FontSize,
            double Width,
            TextAlignment alignment)
        {
            return this.AddText(Page, Text, Top, Left, FontSize, Width, alignment, FontWeights.Normal, 0);
        }

        /// <summary>
        /// Fügt einen Text hinzu mit der Möglichkeit eines Rahmnens
        /// </summary>
        /// <param name="Page"></param>
        /// <param name="Text"></param>
        /// <param name="Top"></param>
        /// <param name="Left"></param>
        /// <param name="FontSize"></param>
        /// <param name="Width"></param>
        /// <param name="alignment"></param>
        /// <param name="Weight"></param>
        /// <param name="Border"></param>
        protected FrameworkElement AddText(
            FixedPage Page,
            string Text,
            double Top,
            double Left,
            int FontSize,
            double Width,
            TextAlignment alignment,
            FontWeight Weight,
            int Border)
        {
            var text = this.CreateTextBlock(Text, FontSize, alignment, Weight, Width);

            if (Border > 0)
            {
                text.Padding = new Thickness(4, 2, 2, 2);
            }

            var b = new Border();
            b.BorderThickness = new Thickness(Border);
            b.BorderBrush = Brushes.Black;
            b.Child = text;
            FixedPage.SetLeft(b, DotsFromCm(Left)); // left margin
            FixedPage.SetTop(b, DotsFromCm(Top)); // top margin
            Page.Children.Add(b);

            return b;
        }

        protected TextBlock CreateTextBlock(
            string Text,
            int FontSize,
            TextAlignment alignment,
            FontWeight Weight,
            double width = 0)
        {
            var text = new TextBlock();
            text.TextWrapping = TextWrapping.Wrap;
            text.Text = Text;
            text.FontSize = FontSize;
            text.FontFamily = new FontFamily("Arial");
            text.TextAlignment = alignment;
            text.FontWeight = Weight;

            if (width > 0)
            {
                text.Width = DotsFromCm(width);
            }

            return text;
        }

        /// <summary>
        /// Fügt einen Rahmen hinzu
        /// </summary>
        /// <param name="Page"></param>
        /// <param name="Top"></param>
        /// <param name="Left"></param>
        /// <param name="Heigh"></param>
        /// <param name="Width"></param>
        protected Border AddBox(FixedPage Page, double Top, double Left, double Height, double Width, Brush background)
        {
            var b = new Border();
            b.BorderThickness = new Thickness(1);
            b.BorderBrush = Brushes.Black;
            b.Background = background;
            b.Width = DotsFromCm(Width);
            b.Height = DotsFromCm(Height);
            FixedPage.SetLeft(b, DotsFromCm(Left)); // left margin
            FixedPage.SetTop(b, DotsFromCm(Top)); // top margin
            Page.Children.Add(b);
            return b;
        }

        /// <summary>
        /// Fügt eine horizontale Linie hinzu
        /// </summary>
        /// <param name="Page"></param>
        /// <param name="Top"></param>
        /// <param name="Left"></param>
        /// <param name="Width"></param>
        protected UIElement AddHorizontalLine(FixedPage Page, double Top, double Left, double Width)
        {
            var b = new Border();
            b.BorderThickness = new Thickness(1);
            b.BorderBrush = Brushes.Black;
            b.Width = Width * 96 / 2.54;
            b.Height = 1;
            FixedPage.SetLeft(b, 96 * Left / 2.54); // left margin
            FixedPage.SetTop(b, 96 / 2.54 * Top); // top margin
            Page.Children.Add((UIElement) b);

            return b;
        }

        /// <summary>
        /// Vertikale Linie
        /// </summary>
        /// <param name="Page"></param>
        /// <param name="Top"></param>
        /// <param name="Left"></param>
        /// <param name="Height"></param>
        protected UIElement AddVerticalLine(FixedPage Page, double Top, double Left, double Height)
        {
            var b = new Border();
            b.BorderThickness = new Thickness(1);
            b.BorderBrush = Brushes.Black;
            b.Height = Height * 96 / 2.54;
            b.Width = 1;
            FixedPage.SetLeft(b, 96 * Left / 2.54); // left margin
            FixedPage.SetTop(b, 96 / 2.54 * Top); // top margin
            Page.Children.Add((UIElement) b);

            return b;
        }

        protected FrameworkElement AddText(FixedPage Page, string Text, double Top, double Left, int FontSize)
        {
            return this.AddText(Page, Text, Top, Left, FontSize, this.UseablePageWidthInCm, TextAlignment.Left);
        }

        /// <summary>
        /// Wird unmittelbar vor dem Druck aufgerufen und fügt beispielsweise Seitennummern hinzu
        /// </summary>
        protected void FinalizeDocument()
        {
            foreach (var page in this.Document.Pages)
            {
                var content = page.Child as FixedPage;

                var textblocks = content.FindChildren<TextBlock>();

                foreach (var textblock in textblocks)
                {
                    textblock.Text = textblock.Text.Replace("[Pages]", this.Document.Pages.Count.ToString());
                }

                this.MeasurePageXX(content);
            }
        }

        /// <summary>
        /// Druckt die Datei aus bzw. erzeugt eine XPS Datei
        /// </summary>
        /// <param name="showPrintDlg">if set to <c>true</c> [show print dialog].</param>
        /// <param name="printQueue">The print queue.</param>
        /// <param name="orientation">The orientation.</param>
        /// <param name="copies">The copies.</param>
        /// <param name="printTitle">The print title.</param>
        /// <param name="paperTray">The paper tray.</param>
        public void PrintDocument(bool showPrintDlg, PrintQueue printQueue, PageOrientation orientation, int copies,
            string printTitle, string paperTray = null)
        {
            this.FinalizeDocument();

            var printDialog = new PrintDialog();

            printDialog.PrintTicket.PageOrientation = orientation;
            printDialog.PrintTicket.CopyCount = copies;
            printDialog.PrintTicket.InputBin = InputBin.AutoSelect;

            if (showPrintDlg)
            {
                if (printDialog.ShowDialog().Value == false)
                {
                    return;
                }
            }
            else
            {
                printDialog.PrintQueue = printQueue;

                if (paperTray != null)
                {
                    var nameSpaceURI = string.Empty;
                    var selectedBin = XpsPrinterUtils.GetInputBinName(printQueue, paperTray, out nameSpaceURI);
                    try
                    {
                        if (!string.IsNullOrWhiteSpace(selectedBin))
                        {
                            printDialog.PrintTicket = XpsPrinterUtils.ModifyPrintTicket(printDialog.PrintTicket,
                                    "psk:JobInputBin", selectedBin, nameSpaceURI);
                        }
                    }
                    catch (Exception ex)
                    {
                        MainWindow.MainWindowHandle.ShowMessage("Error creating print ticket: " + ex.Message);
                    }
                } else
                {
                    var printer = new System.Drawing.Printing.PrinterSettings {PrinterName = printDialog.PrintQueue.Name};
                    var nameSpaceURI = string.Empty;
                    var selectedBin = XpsPrinterUtils.GetInputBinName(printQueue, printer.DefaultPageSettings.PaperSource.SourceName, out nameSpaceURI);
                    try
                    {
                        if (!string.IsNullOrWhiteSpace(selectedBin))
                        {
                            printDialog.PrintTicket = XpsPrinterUtils.ModifyPrintTicket(printDialog.PrintTicket,
                                "psk:JobInputBin", selectedBin, nameSpaceURI);
                        }
                    }
                    catch (Exception ex)
                    {
                        MainWindow.MainWindowHandle.ShowMessage("Error creating print ticket: " + ex.Message);
                    }
                }

            }

            printDialog.PrintDocument(this.Doc.DocumentPaginator, printTitle);
        }

    }
}