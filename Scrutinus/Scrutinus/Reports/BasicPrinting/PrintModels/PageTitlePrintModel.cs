﻿// // TPS.net TPS8 Scrutinus
// // PageTitlePrintModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using GalaSoft.MvvmLight;
using Scrutinus.Localization;

namespace Scrutinus.Reports.BasicPrinting.PrintModels
{
    public class PageTitlePrintModel : ViewModelBase
    {
        #region Constructors and Destructors

        public PageTitlePrintModel()
        {
            if (this.IsInDesignMode)
            {
                this.Title = "Test Title";
                this.PageNumber = 1;
                this.CompetitionCode = "05 / 1202";
            }
        }

        #endregion

        #region Public Properties

        public string CompetitionCode { get; set; }

        public int PageNumber { get; set; }

        public string PageNumberLabel
        {
            get
            {
                return string.Format(
                    "{0} {1} / [Pages]",
                    LocalizationService.Resolve(() => Printing.Page),
                    this.PageNumber);
            }
        }

        public string Title { get; set; }

        #endregion
    }
}