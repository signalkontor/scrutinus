﻿// // TPS.net TPS8 Scrutinus
// // ComplexHeaderPrintModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.IO;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using GalaSoft.MvvmLight;

namespace Scrutinus.Reports.BasicPrinting.PrintModels
{
    public class ComplexHeaderPrintModel : ViewModelBase
    {
        #region Constructors and Destructors

        public ComplexHeaderPrintModel()
        {
            if (this.IsInDesignMode)
            {
                this.Place = "The Place";
                this.CompetitionDate = DateTime.Now;
                this.CompetitionTitle = "WDSF World Open Standard";
                this.Organizer = "DanceGroup";
                this.Host = "Dance Host";
                this.Section = "Standard";
                this.GroupClass = "Adult / -";
                this.RoundTitle = "Round 1";
            }
        }

        #endregion

        #region Public Properties

        public DateTime CompetitionDate { get; set; }

        public string CompetitionTitle { get; set; }

        public string CompetitionType { get; set; }

        public string CompetitonKind { get; set; }

        public string GroupClass { get; set; }

        public string Host { get; set; }

        public ImageSource ImageSource
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(Settings.Default.LogoPath))
                {
                    if (File.Exists(Settings.Default.LogoPath))
                    {
                        return new BitmapImage(new Uri(Settings.Default.LogoPath));
                    }
                }

                return null;
            }
        }

        public string Organizer { get; set; }

        public string Place { get; set; }

        public bool PrintLogo { get; set; }

        public string RoundTitle { get; set; }

        public string Section { get; set; }

        #endregion
    }
}