﻿// // TPS.net TPS8 Scrutinus
// // CoverPagePrinter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Documents;
using DataModel;
using DataModel.Models;
using DataModel.Models.Mapping;
using Scrutinus.Localization;

namespace Scrutinus.Reports.BasicPrinting
{
    public class CoverPagePrinter : GenericTablePrinter<Official>
    {
        #region Constructors and Destructors

        public CoverPagePrinter(Event eventData, ScrutinusContext context)
        {
            this.eventData = eventData;
            this.context = context;
            this.ShowComplexHeader = false;
            this.ShowSimpleHeader = false;

            this.CreateContent();
        }

        #endregion

        #region Fields

        private readonly ScrutinusContext context;

        private readonly Event eventData;

        #endregion

        #region Methods

        protected override void CreateCustomContent()
        {
            var page = this.CreatePage();
            // Add Title of Event etc.
            this.PrintEventTitle(page);

            page = this.PrintCompetitions(page);

            page = this.PrintJudges(page);

            this.PrintOfficials(page);
        }

        internal FixedPage PrintCompetitions(FixedPage page)
        {
            this.HeaderHeigthInDots += 15;

            this.AddText(
                page,
                LocalizationService.Resolve(() => Printing.Competitions),
                CmFromDots(this.HeaderHeigthInDots),
                this.LeftPageMargin,
                12,
                FontWeights.Bold);
            this.HeaderHeigthInDots += 16;

            var competitions = string.Join(", ", this.eventData.Competitions.Select(c => c.Title));

            var element = this.AddText(
                page,
                competitions,
                CmFromDots(this.HeaderHeigthInDots),
                this.LeftPageMargin,
                12,
                this.PageWidth - this.LeftPageMargin - this.RightPageMargin,
                TextAlignment.Left,
                FontWeights.Normal,
                0);

            this.MeasurePageXX(page);

            this.HeaderHeigthInDots += element.ActualHeight + 40;

            return page;
        }

        internal void PrintEventTitle(FixedPage page)
        {
            this.HeaderHeigthInDots = DotsFromCm(this.TopPageMargin);

            this.AddText(
                page,
                this.eventData.Title,
                CmFromDots(this.HeaderHeigthInDots),
                this.LeftPageMargin,
                16,
                this.UseablePageWidthInCm,
                TextAlignment.Center);
            this.HeaderHeigthInDots += 35;

            var dateString = "";
            if (this.eventData.DateFrom.HasValue && this.eventData.DateTo.HasValue)
            {
                dateString = string.Format(
                    LocalizationService.Resolve(() => Printing.HeaderCompetitionDateTwoDays),
                    this.eventData.DateFrom,
                    this.eventData.DateTo);
            }
            else if (this.eventData.DateFrom.HasValue)
            {
                dateString = string.Format(
                    LocalizationService.Resolve(() => Printing.HeaderCompetitionDate),
                    this.eventData.DateFrom);
            }

            this.AddText(
                page,
                dateString,
                CmFromDots(this.HeaderHeigthInDots),
                this.LeftPageMargin,
                16,
                this.UseablePageWidthInCm,
                TextAlignment.Center);
            this.HeaderHeigthInDots += 35;

            this.AddText(
                page,
                this.eventData.Place,
                CmFromDots(this.HeaderHeigthInDots),
                this.LeftPageMargin,
                16,
                this.UseablePageWidthInCm,
                TextAlignment.Center);
            this.HeaderHeigthInDots += 35;

            this.AddText(
                page,
                LocalizationService.Resolve(() => Printing.Organizer),
                CmFromDots(this.HeaderHeigthInDots),
                this.LeftPageMargin,
                14,
                this.UseablePageWidthInCm,
                TextAlignment.Center);
            this.HeaderHeigthInDots += 20;

            this.AddText(
                page,
                this.eventData.Federation,
                CmFromDots(this.HeaderHeigthInDots),
                this.LeftPageMargin,
                14,
                this.UseablePageWidthInCm,
                TextAlignment.Center);

            this.HeaderHeigthInDots += 25;
            this.AddText(
                page,
                LocalizationService.Resolve(() => Printing.Host),
                CmFromDots(this.HeaderHeigthInDots),
                this.LeftPageMargin,
                14,
                this.UseablePageWidthInCm,
                TextAlignment.Center);
            this.HeaderHeigthInDots += 20;

            this.AddText(
                page,
                this.eventData.Organizer,
                CmFromDots(this.HeaderHeigthInDots),
                this.LeftPageMargin,
                14,
                this.UseablePageWidthInCm,
                TextAlignment.Center);
            this.HeaderHeigthInDots += 20;
        }

        internal FixedPage PrintJudges(FixedPage page)
        {
            return this.PrintOfficials(page, Roles.Judge, LocalizationService.Resolve(() => Printing.Adjudicator));
        }

        internal FixedPage PrintOfficials(FixedPage page)
        {
            page = this.PrintOfficials(page, Roles.Chairman, LocalizationService.Resolve(() => Printing.Chairman));
            page = this.PrintOfficials(page, Roles.Supervisor, LocalizationService.Resolve(() => Printing.Supervisor));
            page = this.PrintOfficials(page, Roles.Associate, LocalizationService.Resolve(() => Printing.Associate));
            page = this.PrintOfficials(page, Roles.Scrutineer, LocalizationService.Resolve(() => Printing.Scrutineer));

            return page;
        }

        internal FixedPage PrintOfficials(FixedPage page, int role, string headLine)
        {
            var officials = this.context.Officials.Where(o => o.Roles.Any(r => r.Id == role));
            if (!officials.Any())
            {
                return page;
            }
            this.HeaderHeigthInDots += 15;
            this.AddText(page, headLine, CmFromDots(this.HeaderHeigthInDots), this.LeftPageMargin, 12, FontWeights.Bold);
            this.HeaderHeigthInDots += 2;

            this.dataList = officials.OrderBy(o => o.Sign).ToList();
            this.DoCreateHeader = false;

            this.columns = new List<ColumnDescriptor<Official>>
                               {
                                   new ColumnDescriptor<Official>
                                       {
                                           Header = "Sign",
                                           ValueFunc = o => o.Sign,
                                           Width =
                                               new GridLength(
                                               1,
                                               GridUnitType.Star)
                                       },
                                   new ColumnDescriptor<Official>
                                       {
                                           Header = "Judge",
                                           ValueFunc = o => o.NiceName,
                                           Width =
                                               new GridLength(
                                               2,
                                               GridUnitType.Star)
                                       },
                                   new ColumnDescriptor<Official>
                                       {
                                           Header = "Club / Country",
                                           ValueFunc = o => !string.IsNullOrEmpty(o.Club) ? $"{o.Club} / {o.Nationality?.LongName ?? "n/a"}" : o.Nationality?.LongName ?? "",
                                           Width =
                                               new GridLength(
                                               4,
                                               GridUnitType.Star)
                                       },
                               };

            this.HeaderHeigthInDots += 20;

            return this.PrintTable(page);
        }

        #endregion
    }
}