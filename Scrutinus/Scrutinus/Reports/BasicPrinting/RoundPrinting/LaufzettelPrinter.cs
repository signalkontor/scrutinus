﻿// // TPS.net TPS8 Scrutinus
// // LaufzettelPrinter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows.Documents;
using DataModel.Models;
using Scrutinus.Reports.Attributes;
using Scrutinus.Reports.BasicPrinting.Controls;

namespace Scrutinus.Reports.BasicPrinting.RoundPrinting
{ 
    [CanPrintAfterQualificationRound(true)]
    [CanPrintAfterFinalRound(true)]
    public class LaufzettelPrinter : AbstractPrinter
    {
        #region Constructors and Destructors

        public LaufzettelPrinter(Competition competition, Participant participant)
            : base(competition)
        {
            this.participant = participant;

            this.ShowComplexHeader = false;
            this.ShowSimpleHeader = false;

            this.CreateContent();
        }

        #endregion

        #region Fields

        private readonly Participant participant;

        private FixedPage page;

        #endregion

        #region Methods

        protected override void AddCommenContent(FixedPage page)
        {
        }

        protected override void CreateCustomContent()
        {
            this.page = this.CreatePage();

            if (this.participant.ClimbUp && this.participant.Class.ClassShortName == "BSW")
            {
                var bswClimbup = new LaufzettelControlBSW(this.participant);
                bswClimbup.Width = DotsFromCm(this.PageWidth - this.LeftPageMargin - this.RightPageMargin);
                this.page = this.AddControlToPage(this.page, bswClimbup);
            }
            else
            {
                var laufzettelControl = new LaufzettelControl(this.participant);
                laufzettelControl.Width = DotsFromCm(this.PageWidth - this.LeftPageMargin - this.RightPageMargin);
                this.page = this.AddControlToPage(this.page, laufzettelControl);
            }

            // Add a line in the middle from left to right
            this.AddHorizontalLine(
                this.page,
                this.TopPageMargin + (this.PageHeight - this.TopPageMargin - this.ButtomPageMargin) / 2,
                this.LeftPageMargin,
                this.PageWidth - this.LeftPageMargin - this.RightPageMargin);

            this.HeaderHeigthInDots =
                DotsFromCm(this.TopPageMargin + (this.PageHeight - this.TopPageMargin - this.ButtomPageMargin) / 2) + 20;

            // We check for BSW as they cannot start in C and get a special laufzettel version.
            if (this.participant.ClimbUp && this.participant.Class.ClassShortName == "BSW")
            {
                var ausrichterAufstieg = new AusrichterBelegControlBSW(this.participant);
                ausrichterAufstieg.Width = DotsFromCm(this.PageWidth - this.LeftPageMargin - this.RightPageMargin);
                this.AddControlToPage(this.page, ausrichterAufstieg);
            }
            else
            {
                var ausrichterBeleg = new AusrichterBelegControl(this.participant);
                ausrichterBeleg.Width = DotsFromCm(this.PageWidth - this.LeftPageMargin - this.RightPageMargin);
                this.AddControlToPage(this.page, ausrichterBeleg);
            }
        }

        #endregion
    }
}