﻿// // TPS.net TPS8 Scrutinus
// // CouplesDroppedOutPrinter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Documents;
using DataModel;
using DataModel.Models;
using Scrutinus.Localization;
using Scrutinus.Reports.Attributes;

namespace Scrutinus.Reports.BasicPrinting.RoundPrinting
{
    [CanPrintAfterQualificationRound(true)]
    [CanPrintAfterFinalRound(true)]
    public class CouplesDroppedOutPrinter : GenericTablePrinter<Participant>
    {
        #region Constructors and Destructors

        public CouplesDroppedOutPrinter(Round round, ScrutinusContext context)
        {
            this.round = round;
            this.context = context;
            this.ShowComplexHeader = false;
            this.ShowSimpleHeader = true;
            this.PrintLogo = true;
            this.Competition = round.Competition;
            this.TableFontSize = 13;
            this.CompetitionTitle = round.Competition.Title;
            this.RoundTitle = round.Name;
            this.DocumentTitle = LocalizationService.Resolve(() => Printing.CouplesDroppedOut);

            this.CreateContent();
        }

        #endregion

        #region Fields

        private readonly ScrutinusContext context;

        private readonly Round round;

        private FixedPage page;

        #endregion

        #region Methods

        protected override void CreateCustomContent()
        {
            this.page = this.CreatePage();
            this.PrintDroppedOut();
        }

        private void PrintDroppedOut()
        {
            var droppedOut =
                this.context.Qualifieds.Where(q => q.Round.Id == this.round.Id && q.QualifiedNextRound == false)
                    .Select(q => q.Participant)
                    .OrderBy(o => o.Number)
                    .ToList();

            this.columns = new List<ColumnDescriptor<Participant>>
                               {
                                   new ColumnDescriptor<Participant>
                                       {
                                           Header = "",
                                           ValueFunc = p => p.StarString,
                                           Width = new GridLength(0, GridUnitType.Auto)
                                       },
                                   new ColumnDescriptor<Participant>
                                       {
                                           Header = "",
                                           ValueFunc =
                                               p =>
                                               p.Number,
                                           Width =
                                               new GridLength
                                               (
                                               1,
                                               GridUnitType
                                               .Star)
                                       },
                                   new ColumnDescriptor<Participant>
                                       {
                                           Header =
                                               LocalizationService
                                               .Resolve
                                               (
                                                   ()
                                                   =>
                                                   Printing
                                                       .Name),
                                           ValueFunc =
                                               p =>
                                               p
                                                   .Couple
                                                   .NiceName
                                               + "\r\n"
                                               + p
                                                     .Couple
                                                     .Country,
                                           Width =
                                               new GridLength
                                               (
                                               8,
                                               GridUnitType
                                               .Star)
                                       },
                                       new ColumnDescriptor<Participant>
                                       {
                                           Header = this.round.MarksInputType == MarkingTypes.Skating ? Printing.Sum : Printing.Marks,
                                           ValueFunc = p => p.Points,
                                           Width = new GridLength(0, GridUnitType.Auto)
                                       },
                                   new ColumnDescriptor<Participant>
                                       {
                                           Header = LocalizationService.Resolve(() => Printing.Place),
                                           ValueFunc =
                                               p =>
                                               p.Place,
                                           Width = new GridLength(0, GridUnitType.Auto)
                                       },
                               };

            if (this.Competition.CombinedExternalId != null)
            {
                this.columns.Add(
                    new ColumnDescriptor<Participant>
                        {
                            Header = "Pl. Kombi",
                            ValueFunc = p => p.PlaceOwnCompetition,
                            Width = new GridLength(1, GridUnitType.Star)
                        });
            }
#if DTV

            this.columns.Add(
                new ColumnDescriptor<Participant>()
                    {
                        Header = "Punkte",
                        ValueFunc = (p) => p.RankingPoints,
                        Width = new GridLength(0, GridUnitType.Auto)
                    });
            this.columns.Add(
                new ColumnDescriptor<Participant>()
                {
                    Header = "Platzierung",
                    ValueFunc = p => p.NewPlacings > p.OriginalPlacings ? "Ja" : "",
                    Width = new GridLength(0, GridUnitType.Auto)
                });
            this.columns.Add(
                new ColumnDescriptor<Participant>()
                    {
                        Header = "Aufstieg",
                        ValueFunc = (p) => p.ClimbUpString,
                        Width = new GridLength(0, GridUnitType.Auto)
                    });
            
#endif
            if (this.Competition.Stars == 0 && !this.round.Qualifieds.Any(q => q.Participant.IsWinnerCouple))
            {
                this.columns.RemoveAt(0);
            }

            this.dataList = droppedOut;

            this.DoCreateHeader = true;

            this.TableFontSize = 16;

            this.HeaderHeigthInDots += DotsFromCm(0.5);

            this.page = this.PrintTable(this.page);
        }

        #endregion
    }
}