﻿// // TPS.net TPS8 Scrutinus
// // CouplesByHeatPrinter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Documents;
using DataModel.ModelHelper;
using DataModel.Models;
using Scrutinus.Localization;
using Scrutinus.Reports.Attributes;

namespace Scrutinus.Reports.BasicPrinting.RoundPrinting
{
    [CanPrintBeforeFirstRound(true)]
    [CanPrintBeforeQualificationRound(true)]
    [CanPrintBeforeFinalRound(true)]
    public class CouplesByHeatPrinter : GenericTablePrinter<Participant>
    {
        #region Constructors and Destructors

        public CouplesByHeatPrinter(Round round, ScrutinusContext context)
        {
            this.round = round;
            this.context = context;
            this.ShowComplexHeader = false;
            this.ShowSimpleHeader = true;
            this.PrintFloor = true;

            this.Competition = round.Competition;

            this.CompetitionTitle = this.Competition.Title;
            this.RoundTitle = this.round.Name;

            this.DocumentTitle = LocalizationService.Resolve(() => Printing.HeatsWithNames);

            this.CreateContent();
        }

        #endregion

        #region Fields

        private readonly ScrutinusContext context;

        private readonly Round round;

        private FixedPage page;

        #endregion

        #region Methods

        protected override void CreateCustomContent()
        {
            this.page = this.CreatePage();
            this.PrintDrawing(this.page);
        }

        private FixedPage PrintDrawing(FixedPage page)
        {
            var heatsDances = HeatsHelper.GetHeats(
                this.context,
                this.round.Id);

            // we print the heats simply for the first dance:
            var firstDance = heatsDances.Keys.FirstOrDefault();
            if (firstDance == null)
            {
                return page;
            }

            var heats = heatsDances[firstDance];

            this.columns = new List<ColumnDescriptor<Participant>>
                               {
                                   new ColumnDescriptor<Participant>
                                       {
                                           Header =
                                               LocalizationService.Resolve(
                                                   () => Printing.Number),
                                           ValueFunc = (p) => p.Number,
                                           Width =
                                               new GridLength(
                                               1,
                                               GridUnitType.Star)
                                       },
                                   new ColumnDescriptor<Participant>
                                       {
                                           Header =
                                               LocalizationService.Resolve(
                                                   () => Printing.Name),
                                           ValueFunc = (p) => p.Couple.NiceName,
                                           Width =
                                               new GridLength(
                                               3,
                                               GridUnitType.Star)
                                       },
                                   new ColumnDescriptor<Participant>
                                       {
                                           Header =
                                               LocalizationService.Resolve(
                                                   () => Printing.ClubCountry),
                                           ValueFunc = (p) => p.Couple.Country,
                                           Width =
                                               new GridLength(
                                               3,
                                               GridUnitType.Star)
                                       },
                               };

            this.HeaderHeigthInDots += 20;

            for (var i = 0; i < heats.Count; i++)
            {
                this.HeaderHeigthInDots += 15;

                var headLine = firstDance.Dance.DanceName + " " + LocalizationService.Resolve(() => Printing.Heat)
                                  + " " + (i + 1);

                this.AddText(
                    page,
                    headLine,
                    CmFromDots(this.HeaderHeigthInDots),
                    this.LeftPageMargin,
                    12,
                    FontWeights.Bold);

                this.HeaderHeigthInDots += 20;

                this.dataList = heats[i].OrderBy(p => p.Number).ToList();

                page = this.PrintTable(page);
            }

            return page;
        }

        #endregion
    }
}