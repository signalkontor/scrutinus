﻿// // TPS.net TPS8 Scrutinus
// // RoundDrawingPrinter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using DataModel.ModelHelper;
using DataModel.Models;
using Scrutinus.Localization;
using Scrutinus.Reports.Attributes;

namespace Scrutinus.Reports.BasicPrinting.RoundPrinting
{
    [CanPrintBeforeFirstRound(true)]
    [CanPrintBeforeQualificationRound(true)]
    [CanPrintBeforeFinalRound(true)]
    public class RoundDrawingPrinter : TablePrinter
    {
        #region Constructors and Destructors

        public RoundDrawingPrinter(Round round, ScrutinusContext context)
        {
            this.round = round;
            this.context = context;
            this.ShowComplexHeader = false;
            this.ShowSimpleHeader = true;
            this.PrintLogo = true;
            this.PrintFloor = true;
            this.Competition = round.Competition;

            this.CompetitionTitle = this.Competition.Title;
            this.RoundTitle = this.round.Name;

            this.DocumentTitle = LocalizationService.Resolve(() => Printing.RoundDrawing);

            this.CreateContent();
        }

        #endregion

        #region Fields

        private readonly ScrutinusContext context;

        private readonly Round round;

        private FixedPage page;

        #endregion

        #region Methods

        protected override void CreateCustomContent()
        {
            if (this.round.Competition.Section.Id == 3)
            {
                this.PrintDrawOfDances(1, 5);
                this.PrintDrawOfDances(6, 10);
                return;
            }

            this.PrintDrawOfDances(1, 99);
        }

        private void PrintDrawOfDances(int minId, int maxId)
        {
            this.page = this.CreatePage();

            var heats = HeatsHelper.GetHeats(this.context, this.round.Id);

            var maxCouples = heats.Values.Any() ? heats.Values.Max(h => h.Max(l => l.Count)) : 0;

            this.HeaderHeigthInDots += 20;

            foreach (var danceInRound in heats.Keys.Where(k => k.Dance.Id >= minId && k.Dance.Id <= maxId))
            {
                this.AddText(
                    this.page,
                    danceInRound.Dance.DanceName,
                    CmFromDots(this.HeaderHeigthInDots),
                    this.LeftPageMargin,
                    16,
                    FontWeights.Bold);

                this.HeaderHeigthInDots += 20;

                var grid = this.CreateDrawingGrid(maxCouples + 1);

                this.FillGrid(grid, heats[danceInRound]);

                this.page = this.AddControlToPage(this.page, grid);

                this.HeaderHeigthInDots += 20;
            }
        }

        private Grid CreateDrawingGrid(int columns)
        {
            var grid = new Grid();

            grid.ColumnDefinitions.Add(
                new ColumnDefinition { Width = new GridLength(100, GridUnitType.Pixel), MaxWidth = 120 });

            for (var i = 1; i < columns; i++)
            {
                grid.ColumnDefinitions.Add(
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star), MaxWidth = 60 });
            }

            grid.Width = DotsFromCm(this.PageWidth) - DotsFromCm(this.LeftPageMargin) - DotsFromCm(this.RightPageMargin);

            return grid;
        }

        private void FillGrid(Grid grid, List<List<Participant>> list)
        {
            for (var line = 0; line < list.Count; line++)
            {
                grid.RowDefinitions.Add(new RowDefinition());

                var text = new TextBlock
                               {
                                   Text = LocalizationService.Resolve(() => Printing.Heat) + " " + (line + 1),
                                   FontSize = 16,
                                   Margin = new Thickness(0, 0, 20, 0)
                               };

                grid.Children.Add(text);
                Grid.SetRow(text, line);
                Grid.SetColumn(text, 0);

                var particpants = list[line].OrderBy(p => p.Number).ToList();

                for (var column = 0; column < list[line].Count; column++)
                {
                    text = new TextBlock { Text = particpants[column].Number.ToString(), FontSize = 16 };

                    grid.Children.Add(text);
                    Grid.SetRow(text, line);
                    Grid.SetColumn(text, column + 1);
                }
            }
        }

        #endregion
    }
}