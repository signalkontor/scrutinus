﻿// // TPS.net TPS8 Scrutinus
// // FinalTablePrinter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Documents;
using DataModel.Models;
using Helpers.Skating;
using Scrutinus.Localization;
using Scrutinus.Reports.Attributes;
using Scrutinus.Reports.BasicPrinting.Controls;

namespace Scrutinus.Reports.BasicPrinting.RoundPrinting
{
    [CanPrintAfterFinalRound(true)]
    public class FinalTablePrinter : AbstractPrinter
    {
        #region Constructors and Destructors

        public FinalTablePrinter(ScrutinusContext context, Round round)
            : base(round.Competition)
        {
            this.model = SkatingHelper.CreateViewModel(context, round);
            this.competition = round.Competition;
            this.ShowComplexHeader = true;
            this.DocumentTitle = LocalizationService.Resolve(() => Printing.FinalMarkingTable);

            this.CreateContent();
        }

        #endregion

        #region Fields

        private readonly Competition competition;

        private readonly SkatingViewModel model;

        private Dictionary<int, double> sumDictionary;

        #endregion

        #region Methods

        protected override void AddCommenContent(FixedPage page)
        {
        }

        protected override void CreateCustomContent()
        {
            this.sumDictionary = new Dictionary<int, double>();

            if (this.competition.CurrentRound.DancesInRound.Count > 5)
            {
                this.CreateFinalTables(1, 5, false);
                this.CreateFinalTables(6, 10, true);
            }
            else
            {
                this.CreateFinalTables(0, 99, true);
            }
        }

        protected void CreateFinalTables(int fromId, int toId, bool addSignature)
        {
            var page = this.CreatePage();

            var context = new ScrutinusContext();

            var currentRound = context.Rounds.Single(r => r.Id == this.competition.CurrentRound.Id);

            foreach (var danceRound in currentRound.DancesInRound.Where(d => d.Dance.Id >= fromId && d.Dance.Id <= toId))
            {
                var danceResult = this.model.DanceSkating.Single(m => m.DanceInRound.Dance.Id == danceRound.Dance.Id);

                foreach (var resultParticipant in danceResult.CoupleResults)
                {
                    if (!this.sumDictionary.ContainsKey(resultParticipant.Participant.Number))
                    {
                        this.sumDictionary.Add(resultParticipant.Participant.Number, 0);
                    }

                    this.sumDictionary[resultParticipant.Participant.Number] += resultParticipant.Place;
                }


                var finalTable = new FinalTablePrintControl(this.model, danceRound, this.sumDictionary)
                                     {
                                         Width =
                                             this
                                             .UseablePageWidthInDots
                                     };

                this.AddText(
                    page,
                    danceRound.Dance.DanceName,
                    (this.HeaderHeigthInDots + 10) / 96 * 2.54,
                    this.LeftPageMargin,
                    16,
                    this.PageWidth - this.LeftPageMargin - this.RightPageMargin,
                    TextAlignment.Left);

                this.HeaderHeigthInDots += 35;

                FixedPage.SetLeft(finalTable, DotsFromCm(this.LeftPageMargin)); // left margin
                FixedPage.SetTop(finalTable, this.HeaderHeigthInDots); // top margin
                this.MeasureControl(finalTable);
                page.Children.Add(finalTable);

                this.HeaderHeigthInDots += finalTable.ActualHeight;
            }

            if (addSignature)
            {
                var signatures = new SignatureAssociateSupervisor {Width = this.UseablePageWidthInDots};

                FixedPage.SetLeft(signatures, DotsFromCm(this.LeftPageMargin)); // left margin
                FixedPage.SetTop(signatures, this.HeaderHeigthInDots); // top margin
                page.Children.Add(signatures);
            }
        }

        #endregion
    }
}