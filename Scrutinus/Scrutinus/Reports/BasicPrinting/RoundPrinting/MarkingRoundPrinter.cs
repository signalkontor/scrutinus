﻿// // TPS.net TPS8 Scrutinus
// // MarkingRoundPrinter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using DataModel.Models;
using Scrutinus.Reports.Attributes;

namespace Scrutinus.Reports.BasicPrinting.RoundPrinting
{
    [CanPrintAfterQualificationRound(true)]
    [CanPrintAfterFinalRound(true)]
    public class MarkingRoundPrinter : AbstractPrinter
    {
        #region Constructors and Destructors

        public MarkingRoundPrinter(Round round, ScrutinusContext context)
            : base(round.Competition, true)
        {
            this.round = round;
            this.context = context;
            this.ShowComplexHeader = false;
            this.ShowSimpleHeader = true;
            this.Competition = round.Competition;

            this.CompetitionTitle = this.Competition.Title;
            this.RoundTitle = this.round.Name;

            this.CreateContent();
        }

        #endregion

        #region Fields

        private readonly ScrutinusContext context;

        private readonly Round round;

        private int currentGridLine;

        private FixedPage page;

        #endregion

        #region Methods

        protected override void AddCommenContent(FixedPage page)
        {
        }

        protected override void CreateCustomContent()
        {
            if (this.round.DancesInRound.Count > 5)
            {
                this.CreateCustomContent(this.round.DancesInRound.Where(d => d.Dance.Id < 6).ToList());
                this.CreateCustomContent(this.round.DancesInRound.Where(d => d.Dance.Id > 5).ToList());
            }
            else
            {
                this.CreateCustomContent(this.round.DancesInRound);
            }
        }

        private void CreateCustomContent(ICollection<DanceInRound> dances)
        {
            this.page = this.CreatePage();

            // We create a Grid for all dances containing the judges markings for this round

            var grid = this.CreateGrid(dances);

            var qualifiedsRound = this.round.Qualifieds.OrderBy(q => q.Participant.Number).ToList();

            for (var i = 0; i < qualifiedsRound.Count(); i++)
            {
                this.AddMarkingToGrid(grid, qualifiedsRound[i], dances);

                if (!this.ControlFitsOnPage(this.page, grid))
                {
                    this.RemoveLastLine(grid);
                    this.AddControlToPage(this.page, grid);
                    this.page = this.CreatePage();
                    grid = this.CreateGrid(dances);
                    this.AddMarkingToGrid(grid, qualifiedsRound[i], dances);
                }
            }

            this.AddControlToPage(this.page, grid);
        }

        private void AddHeaderToGrid(Grid grid, ICollection<DanceInRound> dances)
        {
            var columnIndex = 1;

            grid.RowDefinitions.Add(new RowDefinition());
            grid.RowDefinitions.Add(new RowDefinition());

            var judgeIndex = 0;

            foreach (var danceInRound in dances)
            {
                var judgeGrid = new Grid();
                judgeGrid.RowDefinitions.Add(new RowDefinition());

                judgeIndex = 0;

                foreach (var judge in this.round.Competition.GetJudges())
                {
                    judgeGrid.ColumnDefinitions.Add(new ColumnDefinition());
                    var textBlock = new TextBlock { Text = judge.Sign };
                    judgeGrid.Children.Add(textBlock);
                    Grid.SetColumn(textBlock, judgeIndex);
                    Grid.SetRow(textBlock, 0);

                    judgeIndex++;
                }
                // Add the Sum column
                judgeGrid.ColumnDefinitions.Add(new ColumnDefinition());
                var text = new TextBlock { Text = "S" };
                judgeGrid.Children.Add(text);
                Grid.SetColumn(text, judgeIndex);
                Grid.SetRow(text, 0);

                var danceNameTextBlock = new TextBlock
                                             {
                                                 Text = danceInRound.Dance.DanceName,
                                                 HorizontalAlignment = HorizontalAlignment.Center
                                             };
                grid.Children.Add(danceNameTextBlock);
                Grid.SetColumn(danceNameTextBlock, columnIndex);
                Grid.SetRow(danceNameTextBlock, 0);

                grid.Children.Add(judgeGrid);
                Grid.SetColumn(judgeGrid, columnIndex);
                Grid.SetRow(judgeGrid, 1);

                columnIndex++;
            }

            var total = new TextBlock { Text = "Total" };
            grid.ColumnDefinitions.Add(new ColumnDefinition());
            grid.Children.Add(total);
            Grid.SetColumn(total, columnIndex);
            Grid.SetRow(total, 1);

            this.currentGridLine = 2;
        }

        private void AddMarkingToGrid(Grid grid, Qualified qualified, ICollection<DanceInRound> dances)
        {
            grid.RowDefinitions.Add(new RowDefinition());

            var number = new TextBlock { Text = qualified.Participant.Number.ToString() };

            grid.Children.Add(number);
            Grid.SetColumn(number, 0);
            Grid.SetRow(number, this.currentGridLine);
            // now we add all the marks

            var columnIndex = 1;

            foreach (var danceInRound in dances)
            {
                var judgeGrid = new Grid();
                var judgeIndex = 0;
                foreach (var judge in this.round.Competition.GetJudges())
                {
                    judgeGrid.ColumnDefinitions.Add(new ColumnDefinition());
                    // do we have a mark?
                    var marking =
                        this.context.Markings.SingleOrDefault(
                            m =>
                            m.Round.Id == this.round.Id && m.Dance.Id == danceInRound.Dance.Id
                            && m.Participant.Id == qualified.Participant.Id && m.Judge.Id == judge.Id);

                    var text = "";

                    if (marking != null)
                    {
                        if (marking.Mark > 0)
                        {
                            text = "+";
                        }
                    }

                    var textblock = new TextBlock { Text = text };
                    judgeGrid.Children.Add(textblock);
                    Grid.SetColumn(textblock, judgeIndex);
                    Grid.SetRow(textblock, 0);

                    judgeIndex++;
                }

                // Column for the sum
                judgeGrid.ColumnDefinitions.Add(new ColumnDefinition());

                // Add th total of this dance
                var totalList =
                    this.context.Markings.Where(
                        m =>
                        m.Round.Id == this.round.Id && m.Dance.Id == danceInRound.Dance.Id
                        && m.Participant.Id == qualified.Participant.Id).ToList();

                var totalSum = totalList.Sum(m => m.Mark);

                var total = new TextBlock { Text = totalSum.ToString() };
                judgeGrid.Children.Add(total);
                Grid.SetColumn(total, judgeIndex);
                Grid.SetRow(total, 0);

                grid.Children.Add(judgeGrid);
                Grid.SetColumn(judgeGrid, columnIndex);
                Grid.SetRow(judgeGrid, this.currentGridLine);

                columnIndex++;
            }

            // Add Total of all
            var grandList =
                this.context.Markings.Where(
                    m => m.Round.Id == this.round.Id && m.Participant.Id == qualified.Participant.Id).ToList();
            var grandTotal = grandList.Sum(s => s.Mark);

            var grandTotalText = new TextBlock { Text = grandTotal.ToString() };
            grid.Children.Add(grandTotalText);
            Grid.SetColumn(grandTotalText, columnIndex);
            Grid.SetRow(grandTotalText, this.currentGridLine);

            this.currentGridLine++;
        }

        private Grid CreateGrid(ICollection<DanceInRound> dances)
        {
            var grid = new Grid();

            grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(30, GridUnitType.Pixel) });

            for (var i = 0; i < dances.Count; i++)
            {
                grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            }

            grid.Width = DotsFromCm(this.PageWidth) - DotsFromCm(this.LeftPageMargin) - DotsFromCm(this.RightPageMargin);

            this.AddHeaderToGrid(grid, dances);

            return grid;
        }

        private void RemoveLastLine(Grid grid)
        {
            // grid to heigh -> remove last added line + children of this line:
            var uIElements =
                grid.Children.Cast<UIElement>().Where(e => Grid.GetRow(e) == this.currentGridLine - 1).ToList();

            foreach (var uiElement in uIElements)
            {
                grid.Children.Remove(uiElement);
            }

            grid.RowDefinitions.RemoveAt(grid.RowDefinitions.Count - 1);
        }

        #endregion
    }
}