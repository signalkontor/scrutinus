﻿// // TPS.net TPS8 Scrutinus
// // DrawingCouples.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Documents;
using DataModel.Models;
using Scrutinus.Localization;
using Scrutinus.Reports.Attributes;

namespace Scrutinus.Reports.BasicPrinting.RoundPrinting
{
    public class DrawingPerCouple
    {
        #region Public Properties

        public int[] HeatsNumbers { get; set; }

        public Participant Participant { get; set; }

        #endregion
    }

    [CanPrintBeforeFirstRound(true)]
    [CanPrintBeforeQualificationRound(true)]
    [CanPrintBeforeFinalRound(true)]
    public class DrawingCouples : GenericTablePrinter<DrawingPerCouple>
    {
        #region Constructors and Destructors

        public DrawingCouples(Round round, ScrutinusContext context)
        {
            this.round = round;
            this.context = context;
            this.ShowComplexHeader = false;
            this.ShowSimpleHeader = true;
            this.PrintFloor = true;
            this.Competition = round.Competition;

            this.CompetitionTitle = round.Competition.Title;
            this.RoundTitle = round.Name;
            this.DocumentTitle = LocalizationService.Resolve(() => Printing.DrawingCouples);

            this.CreateContent();
        }

        #endregion

        #region Fields

        private readonly ScrutinusContext context;

        private readonly Round round;

        private FixedPage page;

        #endregion

        #region Methods

        protected override void CreateCustomContent()
        {
            if (this.round.Competition.Section.Id == 3)
            {
                this.PrintCoupleDrawing(1, 5, LocalizationService.Resolve(() => Printing.DrawingCouples) + " " + LocalizationService.Resolve(() => Printing.Standard));
                this.PrintCoupleDrawing(6, 10, LocalizationService.Resolve(() => Printing.DrawingCouples) + " " + LocalizationService.Resolve(() => Printing.Latin));
                return;
            }
            this.PrintCoupleDrawing(1, 99, LocalizationService.Resolve(() => Printing.DrawingCouples));
        }

        private void PrintCoupleDrawing(int minId, int maxId, string title)
        {
            this.DocumentTitle = title;
            this.page = this.CreatePage();

            var data = new List<DrawingPerCouple>();

            foreach (var qualified in this.round.Qualifieds.OrderBy(q => q.Participant.Number))
            {
                var drawing = new DrawingPerCouple
                                  {
                                      Participant = qualified.Participant,
                                      HeatsNumbers = new int[this.round.DancesInRound.Count]
                                  };
                data.Add(drawing);

                var index = 0;
                foreach (var danceInRound in this.round.DancesInRound.Where(d => d.Dance.Id >= minId && d.Dance.Id <= maxId))
                {
                    var heat =
                        this.context.Drawings.SingleOrDefault(
                            d =>
                            d.Round.Id == this.round.Id && d.Participant.Id == qualified.Participant.Id
                            && d.DanceRound.Dance.Id == danceInRound.Dance.Id);
                    drawing.HeatsNumbers[index] = heat.Heat + 1; // heat is 0-based, but we want to print 1-based
                    index++;
                }
            }

            this.dataList = data;

            // Create table-data
            this.columns = new List<ColumnDescriptor<DrawingPerCouple>>
                               {
                                   new ColumnDescriptor<DrawingPerCouple>
                                       {
                                           Header = LocalizationService.Resolve(() => Printing.Number),
                                           ValueFunc = p => p.Participant.Number,
                                           Width =
                                               new GridLength(
                                               80,
                                               GridUnitType.Pixel),
                                           FontSize = 18
                                       },
                               };
            var i = 0;
            foreach (var danceInRound in this.round.DancesInRound.Where(d => d.Dance.Id >= minId && d.Dance.Id <= maxId))
            {
                var i1 = i;
                this.columns.Add(
                    new ColumnDescriptor<DrawingPerCouple>
                        {
                            Header = danceInRound.Dance.ShortName,
                            ValueFunc = p => p.HeatsNumbers[i1],
                            Width = new GridLength(50, GridUnitType.Pixel),
                            FontSize = 18
                        });
                i++;
            }

            this.DoCreateHeader = true;

            this.HeaderHeigthInDots += DotsFromCm(0.5);

            this.page = this.PrintTable(this.page);
        }

        #endregion
    }
}