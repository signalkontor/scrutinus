﻿// // TPS.net TPS8 Scrutinus
// // MarkingSumsPrinter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using DataModel.Models;
using Scrutinus.Localization;

namespace Scrutinus.Reports.BasicPrinting.RoundPrinting
{
    public class MarkingSumsPrinter : AbstractPrinter
    {
        #region Constructors and Destructors

        public MarkingSumsPrinter(Round round, ScrutinusContext context)
            : base(round.Competition, false)
        {
            this.round = round;
            this.context = context;
            this.ShowComplexHeader = false;
            this.ShowSimpleHeader = true;
            this.Competition = round.Competition;

            this.CompetitionTitle = this.Competition.Title;
            this.RoundTitle = this.round.Name;
            this.DocumentTitle = LocalizationService.Resolve(() => Printing.MarkingLastRound);

            this.CreateContent();
        }

        #endregion

        #region Fields

        private readonly ScrutinusContext context;

        private readonly Round round;

        private int currentGridLine;

        private FixedPage page;

        #endregion

        #region Methods

        protected override void AddCommenContent(FixedPage page)
        {
        }

        protected override void CreateCustomContent()
        {
            this.page = this.CreatePage();

            // We create a Grid for all dances containing the judges markings for this round

            var grid = this.CreateGrid();

            var qualifiedsRound = this.round.Qualifieds.OrderBy(q => q.Participant.Number).ToList();

            for (var i = 0; i < qualifiedsRound.Count(); i++)
            {
                this.AddMarkingToGrid(grid, qualifiedsRound[i]);

                if (!this.ControlFitsOnPage(this.page, grid))
                {
                    this.RemoveLastLine(grid);
                    this.AddControlToPage(this.page, grid);
                    this.page = this.CreatePage();
                    grid = this.CreateGrid();
                    this.AddMarkingToGrid(grid, qualifiedsRound[i]);
                }
            }

            this.AddControlToPage(this.page, grid);
        }

        private void AddHeaderToGrid(Grid grid)
        {
            grid.RowDefinitions.Add(new RowDefinition());
            grid.RowDefinitions.Add(new RowDefinition());

            var judgeIndex = 1;

            foreach (var judge in this.round.Competition.GetJudges().OrderBy(o => o.Sign))
            {
                grid.ColumnDefinitions.Add(new ColumnDefinition());
                var textBlock = new TextBlock { Text = judge.Sign };
                grid.Children.Add(textBlock);
                Grid.SetColumn(textBlock, judgeIndex);
                Grid.SetRow(textBlock, 0);

                judgeIndex++;
            }
            // Add the Sum column
            grid.ColumnDefinitions.Add(new ColumnDefinition());
            var text = new TextBlock { Text = "Sum" };
            grid.Children.Add(text);
            Grid.SetColumn(text, judgeIndex);
            Grid.SetRow(text, 0);

            this.currentGridLine = 2;
        }

        private void AddMarkingToGrid(Grid grid, Qualified qualified)
        {
            grid.RowDefinitions.Add(new RowDefinition());

            var number = new TextBlock { Text = qualified.Participant.Number.ToString() };

            grid.Children.Add(number);
            Grid.SetColumn(number, 0);
            Grid.SetRow(number, this.currentGridLine);
            // now we add all the marks

            var columnIndex = 1;

            var judges = qualified.Round.Competition.GetJudges();

            var sum = 0;

            foreach (var official in judges.OrderBy(j => j.Sign))
            {
                var marking =
                    this.context.Markings.FirstOrDefault(
                        m =>
                        m.Judge.Id == official.Id && m.Participant.Id == qualified.Participant.Id
                        && m.Round.Id == qualified.Round.Id && m.Dance == null);

                var textBlock = new TextBlock { Text = marking != null ? marking.Mark.ToString() : "" };
                sum += marking != null ? marking.Mark : 0;

                grid.Children.Add(textBlock);
                Grid.SetColumn(textBlock, columnIndex);
                Grid.SetRow(textBlock, this.currentGridLine);

                columnIndex++;
            }

            var sumtextBlock = new TextBlock { Text = sum.ToString() };

            grid.Children.Add(sumtextBlock);
            Grid.SetColumn(sumtextBlock, columnIndex);
            Grid.SetRow(sumtextBlock, this.currentGridLine);

            this.currentGridLine++;
        }

        private Grid CreateGrid()
        {
            var grid = new Grid();

            grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(30, GridUnitType.Pixel) });

            for (var i = 0; i < this.round.Competition.GetJudges().Count; i++)
            {
                grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            }

            grid.Width = DotsFromCm(this.PageWidth) - DotsFromCm(this.LeftPageMargin) - DotsFromCm(this.RightPageMargin);

            this.AddHeaderToGrid(grid);

            return grid;
        }

        private void RemoveLastLine(Grid grid)
        {
            // grid to heigh -> remove last added line + children of this line:
            var uIElements =
                grid.Children.Cast<UIElement>().Where(e => Grid.GetRow(e) == this.currentGridLine - 1).ToList();

            foreach (var uiElement in uIElements)
            {
                grid.Children.Remove(uiElement);
            }

            grid.RowDefinitions.RemoveAt(grid.RowDefinitions.Count - 1);
        }

        #endregion
    }
}