﻿// // TPS.net TPS8 Scrutinus
// // JS2MarkingPrinter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using DataModel.ReportModels;
using Scrutinus.Reports.Attributes;

namespace Scrutinus.Reports.BasicPrinting.RoundPrinting
{
    [CanPrintAfterQualificationRound(true)]
    [CanPrintAfterFinalRound(true)]
    public class Js2MarkingPrinter : TablePrinter
    {
        #region Fields

        private MarkingV2Model model;

        #endregion

        #region Constructors and Destructors

        public Js2MarkingPrinter(MarkingV2Model model)
        {
            this.model = model;
        }

        #endregion
    }
}