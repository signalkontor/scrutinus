﻿// // TPS.net TPS8 Scrutinus
// // CouplesResultPrinter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Documents;
using DataModel.Models;
using Scrutinus.Localization;
using Scrutinus.Reports.Attributes;

namespace Scrutinus.Reports.BasicPrinting.RoundPrinting
{
    public class CouplesResultPrintModel
    {
        #region Public Properties

        public string IsClimbUp { get; set; }

        public Round LastLastRoundQualified { get; set; }

        public bool Laufzettel { get; set; }

        public int Number { get; set; }

        public Participant Participant { get; set; }

        public string Place { get; set; }

        public double SumInFinal { get; set; }

        public string PlaceInRegion { get; set; }

        public bool Placing { get; set; }

        public string RankingPoints { get; set; }

        public string Star { get; set; }

        public string StateString { get; set; }

        #endregion
    }

    [CanPrintAfterQualificationRound(true)]
    [CanPrintAfterFinalRound(true)]
    public class CouplesResultPrinter : GenericTablePrinter<CouplesResultPrintModel>
    {
        #region Fields

        private FixedPage page;

        #endregion

        #region Constructors and Destructors

        public CouplesResultPrinter(Competition competition)
        {
            this.ShowComplexHeader = false;
            this.ShowSimpleHeader = true;
            this.Competition = competition;

            this.CompetitionTitle = competition.Title;
            this.RoundTitle = "";

            this.DocumentTitle = LocalizationService.Resolve(() => Printing.CouplesResults);

            this.CreateContent();
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Creates the content of the custom.
        /// </summary>
        protected override void CreateCustomContent()
        {
            this.dataList =
                this.Competition.Participants.OrderBy(p => p.Number)
                    .Select(
                        p =>
                        new CouplesResultPrintModel
                            {
                                Participant = p,
                                Star = p.StarString,
                                RankingPoints = p.RankingPoints.ToString(),
                                Number = p.Number,
                                SumInFinal = p.Points,
                                Place = p.Place,
                                StateString = p.StateString,
                                Placing = p.PlaceFrom.HasValue && p.PlacingsUpto.HasValue && p.RankingPoints.HasValue && p.PlaceFrom.Value <= p.PlacingsUpto.Value && p.RankingPoints >= 2 ? true : false,
                                Laufzettel = p.Startbuch != null && p.Startbuch.PrintLaufzettel,
                                IsClimbUp = p.ClimbUp ? "Aufst!" : ""
                            })
                    .ToList();

            this.columns = new List<ColumnDescriptor<CouplesResultPrintModel>>
                               {
                                   new ColumnDescriptor
                                       <CouplesResultPrintModel>
                                       {
                                           Header = "",
                                           ValueFunc = x => x.Star,
                                           Width = new GridLength(1,GridUnitType.Auto)
                                       },
                                   new ColumnDescriptor
                                       <CouplesResultPrintModel>
                                       {
                                           Header = "",
                                           ValueFunc = x => x.Number,
                                           Width = new GridLength
                                               (
                                               1,
                                               GridUnitType
                                               .Star)
                                       },
                                   new ColumnDescriptor
                                       <CouplesResultPrintModel>
                                       {
                                           Header
                                               =
                                               "",
                                           ValueFunc
                                               =
                                               x
                                               =>
                                               x
                                                   .Participant
                                                   .Couple
                                                   .NiceName
                                               + "\r\n"
                                               + x
                                                     .Participant
                                                     .Couple
                                                     .Country,
                                           Width
                                               =
                                               new GridLength
                                               (
                                               3,
                                               GridUnitType
                                               .Star)
                                       },
                                   new ColumnDescriptor
                                       <CouplesResultPrintModel>
                                       {
                                           Header
                                               =
                                               "",
                                           ValueFunc
                                               =
                                               x
                                               =>
                                               x
                                                   .Place,
                                           Width
                                               =
                                               new GridLength
                                               (
                                               1,
                                               GridUnitType
                                               .Star)
                                       },
#if DTV
                                     new ColumnDescriptor<CouplesResultPrintModel>()
                                       {
                                           Header = "LZ",
                                           ValueFunc = x => x.Laufzettel ? "Ja" : "",
                                           Width =
                                               new GridLength(
                                               1,
                                               GridUnitType.Star)
                                       },
                                    new ColumnDescriptor<CouplesResultPrintModel>()
                                       {
                                           Header = "Pu.",
                                           ValueFunc = x => x.RankingPoints,
                                           Width =
                                               new GridLength(
                                               1,
                                               GridUnitType.Star)
                                       },
                                    new ColumnDescriptor<CouplesResultPrintModel>()
                                       {
                                           Header = "Pl.",
                                           ValueFunc = x => x.Placing ? "Ja" : "",
                                           Width =
                                               new GridLength(
                                               1,
                                               GridUnitType.Star)
                                       },
                                    new ColumnDescriptor<CouplesResultPrintModel>()
                                       {
                                           Header = "Auf.",
                                           ValueFunc = x => x.IsClimbUp,
                                           Width =
                                               new GridLength(
                                               1,
                                               GridUnitType.Star)
                                       }
#endif
                               };
            this.DoCreateHeader = false;
#if DTV
            this.DoCreateHeader = true;
#endif
            this.HeaderHeigthInDots += DotsFromCm(0.5);

            this.page = this.PrintTable(this.page);
        }

        #endregion
    }
}