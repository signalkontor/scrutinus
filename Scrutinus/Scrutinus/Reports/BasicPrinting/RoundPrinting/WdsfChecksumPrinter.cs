﻿// // TPS.net TPS8 Scrutinus
// // WdsfChecksumPrinter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Documents;
using DataModel.Models;
using Helpers.Checksum;
using Scrutinus.Reports.Attributes;
using Scrutinus.Reports.BasicPrinting.Controls;

namespace Scrutinus.Reports.BasicPrinting.RoundPrinting
{
    public class ChecksumPrintModel
    {
        public string Checksum { get; set; }

        public string RoundName { get; set; }

        public string CompetitionName { get; set; }

        public Official Official { get; set; }
    }

    [CanPrintAfterQualificationRound(true)]
    [CanPrintAfterFinalRound(true)]
    public class WdsfChecksumPrinter : GenericTablePrinter<ChecksumPrintModel>
    {
        private FixedPage page = null;
        private readonly Round round;

        public WdsfChecksumPrinter(Round round)
        {
            this.DocumentTitle = "Wdsf Checksum of " + round.Competition.Title + " / " + round.Name;
            this.ShowComplexHeader = false;
            this.ShowSimpleHeader = true;
            this.Competition = round.Competition;

            this.round = round;

            this.CreateContent();
        }

        protected override void CreateCustomContent()
        {
            this.dataList = this.CreateChecksums();

            this.TableRowHeight = new GridLength(40);

            this.columns = new List<ColumnDescriptor<ChecksumPrintModel>>()
                               {
                                   new ColumnDescriptor<ChecksumPrintModel>()
                                       {
                                           Header = "Judge",
                                           Width = new GridLength(1, GridUnitType.Star),
                                           ValueFunc = p => p.Official.NiceName,
                                           FontSize = 16
                                       },
                                       new ColumnDescriptor<ChecksumPrintModel>()
                                       {
                                           Header = "Signature",
                                           Width = new GridLength(300, GridUnitType.Pixel),
                                           ValueFunc = p => "_____________________________",
                                           FontSize = 16
                                       },
                                   new ColumnDescriptor<ChecksumPrintModel>()
                                       {
                                           FontSize = 16,
                                           Header = "Checksum",
                                           ValueFunc = p => p.Checksum,
                                           Width = GridLength.Auto
                                       }
                               };
            
            this.TableRowHeight = new GridLength(60);

            this.page = this.PrintTable(this.page);

            this.HeaderHeigthInDots += 80;
            var signtatureControl = new SignatureScrutineerChairperson();
            signtatureControl.Width = DotsFromCm(this.PageWidth - this.LeftPageMargin - this.RightPageMargin);

            this.AddControlToPage(this.page, signtatureControl);
        }

        private IList<ChecksumPrintModel> CreateChecksums()
        {
            var judges = this.round.Competition.GetJudges();

            return judges.Select(official => new ChecksumPrintModel() { Official = official, Checksum = ChecksumHelper.CalculateChecksum(this.round, official) }).ToList();
        }
    }
}
