﻿// // TPS.net TPS8 Scrutinus
// // UniversalJudgingSheetPrinter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Documents;
using DataModel.Models;
using Scrutinus.Localization;
using Scrutinus.Reports.BasicPrinting.Controls;

namespace Scrutinus.Reports.BasicPrinting.RoundPrinting
{
    public class UniversalJudgingSheetPrinter : GenericTablePrinter<Participant>
    {
        private readonly Round round;

        private FixedPage page;

        public UniversalJudgingSheetPrinter(Round round)
        {
            this.round = round;
            this.ShowComplexHeader = false;
            this.ShowSimpleHeader = false;
            this.Competition = round.Competition;
            this.round = round;
            this.CompetitionTitle = this.Competition.Title;
            this.RoundTitle = "";

            this.CreateContent();
        }

        protected override void CreateCustomContent()
        {
            this.dataList = this.round.Qualifieds.OrderBy(q => q.Participant.Number).Select(q => q.Participant).ToList();

            this.columns = new List<ColumnDescriptor<Participant>>
                               {
                                   new ColumnDescriptor<Participant>
                                       {
                                           Header = LocalizationService.Resolve(() => Printing.CoupleOrTeam),
                                           ValueFunc = q => q.Number.ToString(),
                                           Width = new GridLength(1, GridUnitType.Star),
                                           FontSize = 20
                                       },
                                   new ColumnDescriptor<Participant>
                                       {
                                           Header = "Comp. 1",
                                           ValueFunc = q => "",
                                           Width = new GridLength(80, GridUnitType.Pixel)
                                       },
                                   new ColumnDescriptor<Participant>
                                       {
                                           Header = "Comp. 2",
                                           ValueFunc = q => "",
                                           Width = new GridLength(80, GridUnitType.Pixel)
                                       },
                                   new ColumnDescriptor<Participant>
                                       {
                                           Header = "Comp. 3",
                                           ValueFunc = q => "",
                                           Width = new GridLength(80, GridUnitType.Pixel)
                                       },
                                    new ColumnDescriptor<Participant>
                                    {
                                        Header = "Comp. 4",
                                        ValueFunc = q => "",
                                        Width = new GridLength(80, GridUnitType.Pixel)
                                    },
                                    new ColumnDescriptor<Participant>
                                    {
                                        Header = LocalizationService.Resolve(() => Printing.Sum),
                                        ValueFunc = q => "",
                                        Width = new GridLength(1, GridUnitType.Star)
                                    },
                                    new ColumnDescriptor<Participant>
                                    {
                                        Header = LocalizationService.Resolve(() => Printing.PlaceOrMark),
                                        ValueFunc = q => "",
                                        Width = new GridLength(80, GridUnitType.Pixel)
                                    }
                               };

            foreach (var judge in this.Competition.GetJudges())
            {
                this.page = this.CreatePage();

                var header = new JudgingSheetHeader(this.round, judge);
                header.Width = DotsFromCm(this.PageWidth - this.LeftPageMargin - this.RightPageMargin);
                this.AddControlToPage(this.page, header, this.LeftPageMargin, CmFromDots(this.HeaderHeigthInDots + 20));
                this.HeaderHeigthInDots += 40;

                this.DoCreateHeader = true;
                this.TableRowHeight = new GridLength(50);
                this.ShowGridLines = true;
                this.DocumentTitle = judge.NiceName;
                this.CompetitionTitle = judge.NiceName;
                
                this.page = this.PrintTable(this.page);

                var ejudgeData= this.round.EjudgeData.FirstOrDefault(e => e.Judge.Id == judge.Id);

                this.HeaderHeigthInDots += 50;
                this.AddControlToPage(this.page, new SignatureJudge(judge.NiceName, ejudgeData?.SignatureCoordinates));
            }
        }
    }
}
