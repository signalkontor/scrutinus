﻿// // TPS.net TPS8 Scrutinus
// // JudgingSheetsPrinter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Documents;
using DataModel;
using DataModel.Models;
using General.Extensions;
using Scrutinus.Localization;
using Scrutinus.Reports.Attributes;
using Scrutinus.Reports.BasicPrinting.Controls;

namespace Scrutinus.Reports.BasicPrinting.RoundPrinting
{
    [CanPrintBeforeFirstRound(true)]
    [CanPrintBeforeQualificationRound(true)]
    [CanPrintAfterQualificationRound(true)]
    [CanPrintBeforeFinalRound(true)]
    [CanPrintAfterFinalRound(true)]
    public class JudgingSheetsPrinter : AbstractPrinter
    {
        #region Constructors and Destructors

        public JudgingSheetsPrinter(Competition competition, Round round)
            : base(competition)
        {
            this.ShowComplexHeader = false;
            this.ShowSimpleHeader = false;
            this.Competition = competition;
            this.round = round;
            this.CompetitionTitle = competition.Title;
            this.RoundTitle = "";

            this.DocumentTitle = string.Empty;

            this.CreateContent();
        }

        #endregion

        #region Fields

        private readonly Round round;

        private Official currentPrintedJudge;

        private FixedPage page;

        private int position;

        #endregion

        #region Methods

        protected override void AddCommenContent(FixedPage page)
        {
        }

        protected override void CreateCustomContent()
        {
            foreach (var judge in this.Competition.GetJudges().OrderBy(j => j.Sign))
            {
                this.currentPrintedJudge = judge;
                this.position++;

                if (this.round.MarksInputType == MarkingTypes.Skating
                    || this.round.MarksInputType == MarkingTypes.MarksSumOnly)
                {
                    // if we have more then 15 couples, this is a big event and
                    // we print the judging sheets on single pages.
                    if (this.page == null || this.round.Qualifieds.Count > 15
                        || Settings.Default.PrintJudgingSheetsOnSinglePage)
                    {
                        this.page = this.CreatePage();
                    }
                    else
                    {
                        this.HeaderHeigthInDots += 5;
                    }

                    var dances = this.round.DancesInRound.Select(d => d.Dance);

                    if (this.Competition.Section.IsTeam)
                    {
                        this.CreateJudgingSheetFormation();
                    }
                    else
                    {
                        this.CreateJudgingSheet(dances, this.round.MarksInputType == MarkingTypes.MarksSumOnly);
                    }
                    
                }

                if (this.round.MarksInputType == MarkingTypes.Marking)
                {
                    // Print Dance by Dance
                    foreach (var danceInRound in this.round.DancesInRound)
                    {
                        var dances = new List<Dance> { danceInRound.Dance };
                        if (this.page == null || Settings.Default.PrintJudgingSheetsOnSinglePage)
                        {
                            this.page = this.CreatePage();
                        }

                        if (this.Competition.Section.IsTeam)
                        {
                            this.CreateJudgingSheetFormation();
                        }
                        else
                        {
                            this.CreateJudgingSheet(dances, false);
                        }
                        
                    }
                }
            }
        }


        private void CreateJudgingSheetFormation()
        {
            var chunks =
                this.round.Qualifieds.OrderBy(p => p.Participant.Number).Select(p => p.Participant).Chunk(15);

            var printHeader = true;
            foreach (var chunk in chunks)
            {
                var block = new JudgingSheetBlockFormation(
                    chunk,
                    this.round,
                    this.currentPrintedJudge,
                    printHeader);

                printHeader = false; // print header only on top of the page with first chunk
                block.Width = DotsFromCm(this.PageWidth - this.LeftPageMargin - this.RightPageMargin);

                this.page = this.AddControlToPage(this.page, block);
                this.HeaderHeigthInDots += 10;
            }
        }

        private void CreateJudgingSheet(IEnumerable<Dance> dances, bool printSumRow)
        {
            IEnumerable<IEnumerable<Participant>> chunks;

            if (dances.ToList().Count() > 1)
            {
                // this will be sum only or final
                // we print ordered by couple numbers
                chunks = this.round.Qualifieds.OrderBy(p => p.Participant.Number).Select(p => p.Participant).Chunk(15);
            }
            else
            {
                // this is dance by dance, we print by heat draw:
                var drawings =
                    this.round.Drawings.Where(d => d.DanceRound.Dance.Id == dances.First().Id).GroupBy(h => h.Heat);
                var lists = new List<IEnumerable<Participant>>();
                foreach (var heat in drawings)
                {
                    lists.Add(heat.Select(p => p.Participant).OrderBy(p => p.Number));
                }
                chunks = lists;
            }

            var printHeader = true;
            foreach (var chunk in chunks)
            {
                var block = new JudgingSheetBlock(
                    dances,
                    chunk,
                    this.round,
                    this.currentPrintedJudge,
                    printSumRow,
                    printHeader);

                printHeader = false; // print header only on top of the page with first chunk
                block.Width = DotsFromCm(this.PageWidth - this.LeftPageMargin - this.RightPageMargin);
                this.page = this.AddControlToPage(this.page, block);
                this.HeaderHeigthInDots += 10;
            }

            var eJudgeData = this.round.EjudgeData.FirstOrDefault(e => e.Judge.Id == this.currentPrintedJudge.Id);

            this.AddText(this.page, $"{Printing.Position}: {this.position}", CmFromDots(this.HeaderHeigthInDots), this.LeftPageMargin, 15, this.PageWidth - this.LeftPageMargin - this.RightPageMargin, TextAlignment.Right);

            var signature = new SignatureJudge(this.currentPrintedJudge.Sign + ") " + this.currentPrintedJudge.NiceName, eJudgeData?.SignatureCoordinates);
            this.page = this.AddControlToPage(this.page, signature);
            // Spacing to next block
            this.HeaderHeigthInDots += 9;
        }

        #endregion
    }
}