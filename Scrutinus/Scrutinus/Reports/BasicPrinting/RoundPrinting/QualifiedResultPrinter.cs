﻿// // TPS.net TPS8 Scrutinus
// // QualifiedCouplesPrinter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Documents;
using DataModel.Models;
using Scrutinus.Localization;
using Scrutinus.Reports.Attributes;

namespace Scrutinus.Reports.BasicPrinting.RoundPrinting
{
    [CanPrintBeforeQualificationRound(true)]
    [CanPrintBeforeFinalRound(true)]
    public class QualifiedResultPrinter : GenericTablePrinter<Qualified>
    {
        #region Constructors and Destructors

        public QualifiedResultPrinter(Round round)
        {
            this.round = round;
            this.ShowComplexHeader = false;
            this.ShowSimpleHeader = true;
            this.PrintFloor = true;
            this.PrintLogo = true;
            this.Competition = round.Competition;

            this.CompetitionTitle = round.Competition.Title;
            this.RoundTitle = round.Name;
            this.DocumentTitle =
                $"{LocalizationService.Resolve(() => Printing.ResultOfRound)} ({round.Qualifieds.Count} {LocalizationService.Resolve(() => Printing.Couples)})";

            this.CreateContent();
        }

        #endregion

        #region Fields

        private readonly Round round;

        private FixedPage page;

        #endregion

        #region Methods

        protected override void CreateCustomContent()
        {
            this.page = this.CreatePage();
            this.PrintQualified();
        }

        private void PrintQualified()
        {
            this.dataList = this.round.Qualifieds.OrderBy(q => q.PlaceFrom).ThenBy(q => q.Participant.Number) .ToList();

            this.columns = new List<ColumnDescriptor<Qualified>>
                               {
                                   new ColumnDescriptor<Qualified>
                                       {
                                           Header = "",
                                           ValueFunc =
                                               q =>
                                               q.Participant
                                                   .StarString,
                                           Width =
                                               new GridLength
                                               (
                                               1,
                                               GridUnitType
                                               .Auto)
                                       },
                                   new ColumnDescriptor<Qualified>
                                   {
                                       Header = Printing.Placing,
                                       ValueFunc = q => q.Place,
                                       Width =
                                           new GridLength
                                           (
                                               1,
                                               GridUnitType
                                                   .Auto)
                                   },
                                   new ColumnDescriptor<Qualified>
                                       {
                                           Header = Printing.Number,
                                           ValueFunc =
                                               q =>
                                               q.Participant
                                                   .Number,
                                           Width =
                                               new GridLength
                                               (
                                               1,
                                               GridUnitType
                                               .Star)
                                       },
                                   new ColumnDescriptor<Qualified>
                                       {
                                           Header = Printing.Couple,
                                           ValueFunc =
                                               q =>
                                               q.Participant
                                                   .Couple
                                                   .NiceName,
                                           Width =
                                               new GridLength
                                               (
                                               3,
                                               GridUnitType
                                               .Star)
                                       },
                                   new ColumnDescriptor<Qualified>
                                       {
                                           Header = Printing.ClubCountry,
                                           ValueFunc =
                                               q =>
                                               q.Participant
                                                   .Couple
                                                   .Country,
                                           Width =
                                               new GridLength
                                               (
                                               2,
                                               GridUnitType
                                               .Star)
                                       },
                                   new ColumnDescriptor<Qualified>()
                                       {
                                           Header = Printing.Marks,
                                           ValueFunc = q => q.Points,
                                           Width = new GridLength(1, GridUnitType.Star)
                                       },

                               };
            this.DoCreateHeader = true;

            this.HeaderHeigthInDots += DotsFromCm(0.5);

            this.page = this.PrintTable(this.page);
        }

        #endregion
    }
}