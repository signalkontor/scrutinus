﻿// // TPS.net TPS8 Scrutinus
// // FinalSkatingTablePrinter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Linq;
using System.Windows.Documents;
using DataModel.Models;
using Helpers.Skating;
using Scrutinus.Localization;
using Scrutinus.Reports.Attributes;
using Scrutinus.Reports.BasicPrinting.Controls;

namespace Scrutinus.Reports.BasicPrinting.RoundPrinting
{
    [CanPrintAfterFinalRound(true)]
    public class FinalSkatingTablePrinter : AbstractPrinter
    {
        #region Fields

        private readonly SkatingViewModel model;

        #endregion

        #region Constructors and Destructors

        public FinalSkatingTablePrinter(ScrutinusContext context, Round round)
            : base(round.Competition)
        {
            this.model = SkatingHelper.CreateViewModel(context, round);
            this.ShowComplexHeader = true;
            this.DocumentTitle = LocalizationService.Resolve(() => Printing.SkatingReportTitle);

            this.CreateContent();
        }

        #endregion

        #region Methods

        protected override void AddCommenContent(FixedPage page)
        {
        }

        protected override void CreateCustomContent()
        {

            var page = this.CreatePage();

            var rule9Table = new SkatingRule9Table(

                this.model.Rule9ViewModels,
                this.model.DanceSkating.Select(d => d.DanceInRound)) { Width = this.UseablePageWidthInDots };

            this.HeaderHeigthInDots += 20;

            FixedPage.SetLeft(rule9Table, DotsFromCm(this.LeftPageMargin)); // left margin
            FixedPage.SetTop(rule9Table, this.HeaderHeigthInDots); // top margin

            this.MeasureControl(rule9Table);
            page.Children.Add(rule9Table);
            this.HeaderHeigthInDots += rule9Table.ActualHeight;
            this.HeaderHeigthInDots += 20;

            var rule10Table = new SkatingRuleTable(
                this.model.Participants.Count,
                this.model.DanceSkating.Count,
                this.model.Rule10ViewModel,
                SkatingPrintTableType.Rule10) { Width = this.UseablePageWidthInDots };

            this.HeaderHeigthInDots += 20;

            FixedPage.SetLeft(rule10Table, DotsFromCm(this.LeftPageMargin)); // left margin
            FixedPage.SetTop(rule10Table, this.HeaderHeigthInDots); // top margin
            this.MeasureControl(rule10Table);
            page.Children.Add(rule10Table);

            this.HeaderHeigthInDots += rule10Table.ActualHeight;
            this.HeaderHeigthInDots += 20;

            var rule11Table = new SkatingRuleTable(
                this.model.Participants.Count,
                this.model.DanceSkating.Count,
                this.model.Rule11ViewModel,
                SkatingPrintTableType.Rule11) { Width = this.UseablePageWidthInDots };

            this.HeaderHeigthInDots += 20;

            FixedPage.SetLeft(rule11Table, DotsFromCm(this.LeftPageMargin)); // left margin
            FixedPage.SetTop(rule11Table, this.HeaderHeigthInDots); // top margin
            this.MeasureControl(rule11Table);
            page.Children.Add(rule11Table);

            this.HeaderHeigthInDots += rule11Table.ActualHeight;
            this.HeaderHeigthInDots += 20;

            var signatures = new SignatureAssociateSupervisor { Width = this.UseablePageWidthInDots };

            FixedPage.SetLeft(signatures, DotsFromCm(this.LeftPageMargin)); // left margin
            FixedPage.SetTop(signatures, this.HeaderHeigthInDots); // top margin
            page.Children.Add(signatures);
        }

        #endregion
    }
}