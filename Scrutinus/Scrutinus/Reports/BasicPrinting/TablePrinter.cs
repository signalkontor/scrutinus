﻿// // TPS.net TPS8 Scrutinus
// // TablePrinter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using DataModel.Models;
using Scrutinus.Reports.BasicPrinting.Controls;

namespace Scrutinus.Reports.BasicPrinting
{
    public class ColumnDescriptor
    {
        #region Public Properties

        public string Header { get; set; }

        public string Path { get; set; }

        public GridLength Width { get; set; }

        #endregion
    }

    public class TablePrinter : AbstractPrinter
    {
        #region Public Properties

        public bool DoCreateTableHeaders { get; set; }

        #endregion

        #region Public Methods and Operators

        public void CreateReport()
        {
            this.CreateContent();
        }

        #endregion

        #region Fields

        protected IList<ColumnDescriptor> columns;

        protected FixedPage currentPage;

        protected IList list;

        private int currentRowNumber;

        private Grid grid;

        #endregion

        #region Constructors and Destructors

        public TablePrinter()
            : base(null)
        {
            this.DoCreateTableHeaders = false;
        }

        public TablePrinter(Competition competition, IList listData, IList<ColumnDescriptor> columns)
            : base(competition)
        {
            this.list = listData;
            this.columns = columns;
            this.DoCreateTableHeaders = true;
        }

        #endregion

        #region Methods

        protected override void AddCommenContent(FixedPage page)
        {
            // Add copyright at end of page ..
        }

        protected override void CreateCustomContent()
        {
            this.PrintTable(this.CreatePage());
        }

        protected string GetIndexedValue(object obj, string path)
        {
            var valueName = path.Substring(0, path.IndexOf("["));
            var length = path.IndexOf("]") - path.IndexOf("[") - 1;
            var indexStr = path.Substring(path.IndexOf("[") + 1, length);
            var index = int.Parse(indexStr);

            var property = obj.GetType().GetProperty(valueName);
            var array = (Array)property.GetValue(obj, null);

            return array.GetValue(index).ToString();
        }

        protected FixedPage PrintTable(FixedPage page)
        {
            if (this.list == null)
            {
                return page;
            }

            var index = 0;

            FixedPage newPage = null;

            if (this.list.Count == 0)
            {
                this.CreateGrid(index, page, out newPage);
                return newPage;
            }

            while (index < this.list.Count)
            {
                index = this.CreateGrid(index, page, out newPage);
                page = newPage;
            }

            return newPage;
        }

        private void AddDataRow(int index)
        {
            this.grid.RowDefinitions.Add(new RowDefinition());

            for (var column = 0; column < this.columns.Count; column++)
            {
                var text = new TextBlock
                               {
                                   Text = this.GetValueFromObject(this.list[index], this.columns[column].Path),
                                   Margin = new Thickness(4),
                                   TextWrapping = TextWrapping.WrapWithOverflow
                               };
                this.grid.SetCellUiElement(text, column, this.currentRowNumber);
            }

            this.currentRowNumber++;
        }

        private void CreateColumns()
        {
            foreach (var t in this.columns)
            {
                this.grid.ColumnDefinitions.Add(new ColumnDefinition { Width = t.Width });
            }
        }

        private int CreateGrid(int startIndex, FixedPage currentPage, out FixedPage page)
        {
            if (currentPage == null)
            {
                page = this.CreatePage();
            }
            else
            {
                page = currentPage;
            }

            this.grid = new Grid { Width = this.UseablePageWidthInDots };

            FixedPage.SetLeft(this.grid, DotsFromCm(this.LeftPageMargin)); // left margin
            FixedPage.SetTop(this.grid, this.HeaderHeigthInDots); // top margin

            this.CreateColumns();

            this.currentRowNumber = 0;

            if (this.DoCreateTableHeaders)
            {
                this.CreateHeader();
                this.currentRowNumber = 1;
            }

            var lineAdded = false;

            while (startIndex < this.list.Count && this.ControlFitsOnPage(page, this.grid))
            {
                this.AddDataRow(startIndex);
                startIndex++;
                lineAdded = true;
            }

            if (startIndex < this.list.Count)
            {
                // grid to heigh -> remove last added line + children of this line:
                var uIElements =
                    this.grid.Children.Cast<UIElement>()
                        .Where(e => Grid.GetRow(e) == this.currentRowNumber - 1)
                        .ToList();
                foreach (var uiElement in uIElements)
                {
                    this.grid.Children.Remove(uiElement);
                }

                if (lineAdded)
                {
                    this.grid.RowDefinitions.RemoveAt(this.grid.RowDefinitions.Count - 1);
                    startIndex--;
                }

                this.AddControlToPage(page, this.grid);
                page = this.CreatePage();

                return startIndex;
            }

            this.MeasureControl(this.grid);
            page = this.AddControlToPage(page, this.grid);
            

            return startIndex;
        }

        private void CreateHeader()
        {
            this.grid.RowDefinitions.Add(new RowDefinition());

            for (var index = 0; index < this.columns.Count; index++)
            {
                var border = new Border
                                 {
                                     Background = new SolidColorBrush(Colors.LightGray),
                                     Height = 30,
                                     BorderBrush = new SolidColorBrush(Colors.Transparent)
                                 };
                border.Child = new TextBlock
                                   {
                                       Text = this.columns[index].Header,
                                       Margin = new Thickness(4, 0, 0, 0),
                                       VerticalAlignment = VerticalAlignment.Center,
                                   };
                this.grid.SetCellUiElement(border, index, 0);
            }
        }

        private string GetValueFromObject(object obj, string propertyPath)
        {
            var type = obj.GetType();
            PropertyInfo prop = null;

            var parts = propertyPath.Split('.');
            var value = obj;

            foreach (var part in parts)
            {
                if (part.EndsWith("]"))
                {
                    return this.GetIndexedValue(obj, part);
                }

                prop = type.GetProperty(part);
                if (prop == null)
                {
                    return null;
                }

                var x = prop.GetValue(value, null);

                if (x is IList)
                {
                    value = (x as IList)[0];
                    type = value.GetType();
                    continue;
                }
                if (x == null)
                {
                    return null;
                }

                type = x.GetType();

                value = prop.GetValue(value, null);
            }

            return value.ToString();
        }

        #endregion
    }
}