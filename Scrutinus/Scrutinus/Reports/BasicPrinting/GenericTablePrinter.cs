﻿// // TPS.net TPS8 Scrutinus
// // GenericTablePrinter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using Scrutinus.Reports.BasicPrinting.Controls;

namespace Scrutinus.Reports.BasicPrinting
{
    public class GenericTablePrinter<T> : AbstractPrinter
    {
        #region Constructors and Destructors

        public GenericTablePrinter()
            : base(null)
        {
            this.DoCreateHeader = false;
            this.TableFontSize = 12;
            this.TableRowHeight = new GridLength();
            this.ShowGridLines = false;
        }

        #endregion

        #region Public Methods and Operators

        public void CreateReport()
        {
            this.CreateContent();
        }

        #endregion

        #region Fields

        protected IList<ColumnDescriptor<T>> columns;

        protected FixedPage currentPage;

        protected IList<T> dataList;

        protected IEnumerable<IGrouping<string, T>> groupedDataList;

        private int currentRowNumber;

        private Grid grid;

        #endregion

        #region Public Properties

        public bool DoCreateHeader { get; set; }

        public double TableFontSize { get; set; }

        public GridLength TableRowHeight { get; set; }

        public bool ShowGridLines { get; set; }

        #endregion

        #region Methods

        protected override void AddCommenContent(FixedPage page)
        {
            // Add copyright at end of page ..
        }

        protected override void CreateCustomContent()
        {
            this.PrintTable(this.CreatePage());
        }

        protected FixedPage PrintTabledGrouped(FixedPage page)
        {
            foreach (var group in this.groupedDataList)
            {
                this.AddGroupRow(group.Key);
                this.dataList = group.ToList();
                page = this.PrintTable(page, true);
            }

            this.AddControlToPage(page, this.grid);
            this.MeasurePageXX(page);
            this.HeaderHeigthInDots += this.grid.ActualHeight;

            return page;
        }

        protected FixedPage PrintTable(FixedPage page, bool multipleCalls = false)
        {
            this.grid = null;

            if (page == null)
            {
                page = this.CreatePage();
            }

            if (this.dataList == null)
            {
                return page;
            }

            var index = 0;

            FixedPage newPage = null;

            if (this.dataList.Count == 0)
            {
                this.FillGrid(index, page, multipleCalls, out newPage);
                return newPage ?? page;
            }

            while (index < this.dataList.Count)
            {
                this.grid = null;
                index = this.FillGrid(index, page, multipleCalls, out newPage);
                page = newPage;
            }

            return page;
        }

        private void AddGroupRow(string group)
        {
            if (this.grid == null)
            {
                this.CreateGrid();
            }

            this.grid.RowDefinitions.Add(new RowDefinition() { Height = this.TableRowHeight });

            var text = new TextBlock
            {
                Text = group,
                Margin = new Thickness(4),
                TextWrapping = TextWrapping.WrapWithOverflow,
                FontSize = this.TableFontSize + 2,
                FontWeight = FontWeights.Bold,
                VerticalAlignment = VerticalAlignment.Center
            };

            this.grid.SetCellUiElement(text, 0, this.currentRowNumber, this.columns.Count);

            this.currentRowNumber++;
        }

        private void AddDataRow(int index)
        {
            if (index >= this.dataList.Count)
            {
                return;
            }

            this.grid.RowDefinitions.Add(new RowDefinition() {Height = this.TableRowHeight});

            for (var column = 0; column < this.columns.Count; column++)
            {
                var text = new TextBlock
                               {
                                   Text =
                                       this.columns[column].ValueFunc(this.dataList[index]) != null
                                           ? this.columns[column].ValueFunc(this.dataList[index]).ToString()
                                           : "",
                                   Margin = new Thickness(4),
                                   TextWrapping = TextWrapping.WrapWithOverflow,
                                   FontSize = this.columns[column].FontSize > 0 ? this.columns[column].FontSize : this.TableFontSize,
                                   VerticalAlignment = VerticalAlignment.Center
                               };

                this.grid.SetCellUiElement(text, column, this.currentRowNumber);
            }

            this.currentRowNumber++;
        }

        private void CreateColumns()
        {
            foreach (var t in this.columns)
            {
                this.grid.ColumnDefinitions.Add(new ColumnDefinition { Width = t.Width });
            }
        }

        private void CreateGrid()
        {
            this.grid = new Grid { Width = this.UseablePageWidthInDots, ShowGridLines = this.ShowGridLines };

            FixedPage.SetLeft(this.grid, DotsFromCm(this.LeftPageMargin)); // left margin
            FixedPage.SetTop(this.grid, this.HeaderHeigthInDots); // top margin

            this.CreateColumns();

            this.currentRowNumber = 0;

            if (this.DoCreateHeader)
            {
                this.CreateHeader();
                this.currentRowNumber = 1;
            }
        }

        private int FillGrid(int startIndex, FixedPage currentPage, bool multipleCalls, out FixedPage page)
        {
            page = currentPage ?? this.CreatePage();

            if (this.grid == null)
            {
                this.CreateGrid();
            }

            while (startIndex < this.dataList.Count && this.ControlFitsOnPage(page, this.grid))
            {
                this.AddDataRow(startIndex);
                startIndex++;
            }


            if (startIndex < this.dataList.Count || !this.ControlFitsOnPage(page, this.grid))
            {
                // grid to heigh -> remove last added line + children of this line:
                var uIElements =
                    this.grid.Children.Cast<UIElement>()
                        .Where(e => Grid.GetRow(e) == this.currentRowNumber - 1)
                        .ToList();

                foreach (var uiElement in uIElements)
                {
                    this.grid.Children.Remove(uiElement);
                }

                if (this.grid.RowDefinitions.Count > 0)
                {
                    this.grid.RowDefinitions.RemoveAt(this.grid.RowDefinitions.Count - 1);
                    if (startIndex > 0)
                    {
                        startIndex--;
                    }
                }
                this.AddControlToPage(page, this.grid);
                // create the next page
                this.CreateGrid();
                page = this.CreatePage();

                return startIndex;
            }

            if (!multipleCalls)
            {
                page = this.AddControlToPage(page, this.grid);
                this.MeasurePageXX(page);
            }

            return startIndex;
        }


        private void CreateHeader()
        {
            this.grid.RowDefinitions.Add(new RowDefinition());

            for (var index = 0; index < this.columns.Count; index++)
            {
                var border = new Border
                                 {
                                     Background = new SolidColorBrush(Colors.LightGray),
                                     Height = 30,
                                     BorderBrush = new SolidColorBrush(Colors.Transparent)
                                 };
                border.Child = new TextBlock
                                   {
                                       Text = this.columns[index].Header,
                                       Margin = new Thickness(4, 0, 0, 0),
                                       VerticalAlignment = VerticalAlignment.Center,
                                       FontSize = this.TableFontSize,
                                   };
                this.grid.SetCellUiElement(border, index, 0);
            }
        }

        #endregion
    }
}