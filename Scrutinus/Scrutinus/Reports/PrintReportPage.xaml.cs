﻿// // TPS.net TPS8 Scrutinus
// // PrintReportPage.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Diagnostics;
using System.Linq;
using System.Printing;
using System.Windows.Controls;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using MahApps.Metro.SimpleChildWindow;
using Scrutinus.Localization;
using Scrutinus.Reports.BasicPrinting;

namespace Scrutinus.Reports
{
    /// <summary>
    ///     Interaction logic for PrintReportPage.xaml
    /// </summary>
    public partial class PrintReportPage : ChildWindow
    {
        public PrintReportPage()
        {
            this.InitializeComponent();

            this.CloseDocumentViewCommand = new RelayCommand(this.CloseCloseChildWindow);
        }

        #region Public Properties

        public ICommand CloseDocumentViewCommand { get; set; }

        #endregion

        #region Public Methods and Operators

        public void Print(
            AbstractPrinter printer,
            bool showPrintDlg,
            bool showPreview,
            string printerName,
            int copies,
            string printTitel,
            string paperTray = null,
            bool landscape = false)
        {
            var document = printer.Document;
            this.documentViewer.Document = document;

            // Find the printing queue
            // print document...
            if (!showPreview)
            {
                PrintQueue queue = null;

                if (showPrintDlg)
                {
                    var dlg = new PrintDialog();
                    var res = dlg.ShowDialog();
                    if (res.HasValue && res.Value)
                    {
                        queue = dlg.PrintQueue;
                    }
                }
                else
                {
                    // get default printer or get printer by the name
                    if (printerName == null || printerName == LocalizationService.Resolve(() => Text.DefaultPrinter)
                        || printerName.Contains("[Default]"))
                    {
                        try
                        {
                            queue = LocalPrintServer.GetDefaultPrintQueue();
                        }
                        catch (Exception)
                        {
                            MainWindow.MainWindowHandle.ShowMessage(LocalizationService.Resolve(() => Text.NoPrinterInstalled));
                            return;
                        }
                        
                    }
                    else
                    {
                        try
                        {
                            var printServer = new LocalPrintServer();
                            EnumeratedPrintQueueTypes[] enumerationFlags = {EnumeratedPrintQueueTypes.Local,
                                                EnumeratedPrintQueueTypes.Connections};
                            var queues = printServer.GetPrintQueues(enumerationFlags);

                            queue = queues.FirstOrDefault(q => q.Name == printerName);
                            if (queue == null)
                            {
                                queue = LocalPrintServer.GetDefaultPrintQueue();
                            }
                        }
                        catch (Exception)
                        {
                            try
                            {
                                queue = LocalPrintServer.GetDefaultPrintQueue();
                            }
                            catch (Exception)
                            {
                                MainWindow.MainWindowHandle.ShowMessage(LocalizationService.Resolve(() => Text.NoPrinterInstalled));
                                return;
                            }
                        }
                    }
                }

                if (queue != null)
                {
                    printer.PrintDocument(
                        false,
                        queue,
                        landscape ? PageOrientation.Landscape : PageOrientation.Portrait,
                        copies,
                        printTitel,
                        paperTray);
                }
            }
        }

        #endregion

        #region Methods

        private void CloseCloseChildWindow()
        {
            // Go back to print dialog
            this.Close();
        }

        #endregion
    }
}