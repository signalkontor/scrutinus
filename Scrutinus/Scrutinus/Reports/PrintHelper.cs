﻿// // TPS.net TPS8 Scrutinus
// // PrintHelper.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Controls;
using System.Windows.Forms;
using DataModel;
using DataModel.Models;
using DataModel.ReportModels;
using Scrutinus.Dialogs;
using Scrutinus.Dialogs.DialogViewModels;
using Scrutinus.Localization;
using Scrutinus.PrintModels;
using Scrutinus.Reports.BasicPrinting;
using Scrutinus.Reports.BasicPrinting.CompetitionPrinting;
using Scrutinus.Reports.BasicPrinting.EventPrinting;
using Scrutinus.Reports.BasicPrinting.RoundPrinting;

namespace Scrutinus.Reports
{
    public class PrintHelper
    {
        public static Page SourcePage { get; set; }

        #region Public Methods and Operators

        public static void DoPrintJob(
            ScrutinusContext context,
            Competition competition,
            Round round,
            PrintingViewModel model,
            bool preview = false)
        {
            if (model.FinalResult.Print && model.FinalResult.Enabled)
            {
                var printer = new FinalResultPrinter(competition);
                PrintReport(
                    model.FinalResult.Printer,
                    model.FinalResult.Copies,
                    preview,
                    printer,
                    "Couples Result Ordered By Place");
#if DTV
                PrintCheckSumsForCompetition(model.FinalResult.Printer, competition, context);
#endif
            }

            if (model.ResultOfRound.Print)
            {
                PrintHelper.PrintPlacingsOfQualifiedCouples(round, model.ResultOfRound.Printer, model.ResultOfRound.Copies, preview);
            }

            if (model.JudgesWorkPlan.Print)
            {
                var printer = new JudgesWorkPlanPrinter(null, false); //new CompetitionsByJudgePrinter(context);
                PrintReport(model.JudgesWorkPlan.Printer, model.JudgesWorkPlan.Copies, preview, printer, "Judges Workplan");
            }

            if (model.WdsfChecksums.Print)
            {
                var roundToPrint = round;
                if (!round.Markings.Any() && round.Number > 1)
                {
                    // no markings, we try to get the previous round
                    roundToPrint =
                        context.Rounds.First(
                            r => r.Competition.Id == round.Competition.Id && r.Number == round.Number - 1);
                }

                var printer = new WdsfChecksumPrinter(roundToPrint);
                PrintReport(model.WdsfChecksums.Printer, model.WdsfChecksums.Copies, preview, printer, "WDSF Checksums");
            }

            if (model.WdsfChecksumOfEvent.Print)
            {
               

                var printer = new WdsfChecksumsEventPrinter(context.Competitions.ToList().Where(c => c.Title.Contains("WDSF")), context.Officials);
                PrintReport(model.WdsfChecksums.Printer, model.WdsfChecksums.Copies, preview, printer, "WDSF Event Checksums");
            }

            if (model.WinnerCertificate.Print && model.WinnerCertificate.Enabled)
            {
                var participants =
                    competition.Participants.Count(p => p.State == CoupleState.DroppedOut) < 36
                        ? competition.Participants.Where(p => p.State == CoupleState.DroppedOut)
                              .OrderByDescending(p => p.PlaceFrom)
                        : round.Qualifieds.Select(q => q.Participant);

                var certificateDialog = new CertificateDialog(participants, round, model.WinnerCertificate.Printer);
                MainWindow.MainWindowHandle.ShowDialog(certificateDialog, CertificateDialogClosed, 300, 300);
            }

            if (model.RegisteredCouples.Print && model.RegisteredCouples.Enabled)
            {
                PrintRegisteredCouples(
                    competition,
                    model.RegisteredCouples.Printer,
                    model.RegisteredCouples.Copies,
                    preview);
            }
            if (model.CouplesDroppedOut.Print && model.CouplesDroppedOut.Enabled)
            {
                PrintDropedOut(context, round, model.CouplesDroppedOut.Printer, model.CouplesDroppedOut.Copies, preview);
            }

            if (model.CouplesResult.Print && model.CouplesResult.Enabled)
            {
                PrintCouplesResult(competition, model.CouplesResult.Printer, model.CouplesResult.Copies, preview);
            }

            if (model.CouplesResultSorted.Print && model.CouplesResultSorted.Enabled)
            {
                PrintCouplesResultSorted(
                    competition,
                    model.CouplesResultSorted.Printer,
                    model.CouplesResultSorted.Copies,
                    preview);
            }

            if (model.CorrectedStartList.Print && model.CorrectedStartList.Enabled)
            {
                PrintCorrectedStartList(
                    competition,
                    model.CorrectedStartList.Printer,
                    model.CorrectedStartList.Copies,
                    preview);
            }

            if (model.CoverPage.Print && model.CoverPage.Enabled)
            {
                PrintCoverPage(competition, model.CoverPage.Printer, model.CoverPage.Copies, preview);
            }

            if (model.Laufzettel.Print && model.Laufzettel.Enabled)
            {
                PrintLaufzettel(round, model.Laufzettel.Printer, model.Laufzettel.Copies, preview);
            }

            if (model.Checksums.Print && model.Checksums.Enabled)
            {
                PrintCheckSums(model.Checksums.Printer, model.Checksums.Copies, preview, context);
            }

            if (model.ResultSortedByRegions.Print && model.ResultSortedByRegions.Enabled)
            {
                PrintResultByRegions(
                    competition,
                    model.ResultSortedByRegions.Printer,
                    model.ResultSortedByRegions.Copies,
                    preview);
            }

            if (model.DTVAnnex.Print && model.DTVAnnex.Enabled)
            {
                PrintDTVAnnex(competition, model.DTVAnnex.Printer, model.DTVAnnex.Copies, preview);
            }

            if (model.DTVPart1.Print && model.DTVPart1.Enabled)
            {
                PrintDTVPart1(context, model.DTVPart1.Printer, model.DTVPart1.Copies, preview);
            }

            if (model.Drawing.Print && model.Drawing.Enabled)
            {
                PrintRoundDrawing(context, round, model.Drawing.Printer, model.Drawing.Copies, preview);
            }

            if (model.DrawingCouples.Print && model.DrawingCouples.Enabled)
            {
                PrintDrawingCouples(context, round, model.DrawingCouples.Printer, model.DrawingCouples.Copies, preview);
            }

            if (model.EmptyJudgingSheets.Print && model.EmptyJudgingSheets.Enabled)
            {
                PrintEmptyJudgingSheets(
                    competition,
                    round,
                    model.EmptyJudgingSheets.Printer,
                    model.EmptyJudgingSheets.Copies,
                    preview);
            }

            if (model.FinalTable.Print && model.FinalTable.Enabled)
            {
                PrintFinalTable(context, competition, round, model.FinalTable.Printer, model.FinalTable.Copies, preview);
            }

            if (model.FinalSkatingTable.Print && model.FinalSkatingTable.Enabled)
            {
                PrintSkatingReport(
                    context,
                    competition,
                    round,
                    model.FinalSkatingTable.Printer,
                    model.FinalSkatingTable.Copies,
                    preview);
            }

            if (model.MarkingRound.Print && model.MarkingRound.Enabled)
            {
                PrintMarkingRound(
                    context,
                    competition,
                    round,
                    model.MarkingRound.Printer,
                    model.MarkingRound.Copies,
                    preview);
            }

            if (model.MarkingLastRound.Print && model.MarkingLastRound.Enabled)
            {
                // Find the last Round:

                var lastround =
                    context.Rounds.SingleOrDefault(
                        r => r.Competition.Id == competition.Id && r.Number == round.Number - 1);

                if (lastround != null)
                {
                    PrintMarkingRound(
                        context,
                        competition,
                        lastround,
                        model.MarkingRound.Printer,
                        model.MarkingRound.Copies,
                        preview);
                }
            }

            if (model.MarkingTableTotal.Print && model.MarkingTableTotal.Enabled)
            {
                PrintMarkingTableTotal(
                    context,
                    competition,
                    model.MarkingTableTotal.Printer,
                    model.MarkingTableTotal.Copies,
                    preview);
            }

            if (model.Officals.Print && model.Officals.Enabled)
            {
                PrintOfficals(competition, model.Officals.Printer, model.Officals.Copies, preview);
            }

            if (model.Qualified.Print && model.Qualified.Enabled)
            {
                PrintQualified(context, round, model.Qualified.Printer, model.Qualified.Copies, preview);
                if (round.IsRedanceRound)
                {
                    // get the second round as well and print it
                    var secondRound =
                        context.Rounds.SingleOrDefault(
                            r => r.Competition.Id == round.Competition.Id && r.Number == round.Number + 1);
                    if (secondRound != null)
                    {
                        PrintQualified(context, secondRound, model.Qualified.Printer, model.Qualified.Copies, preview);
                    }
                }
            }

            if (model.RoundWithNames.Print && model.RoundWithNames.Enabled)
            {
                PrintRoundWithNames(context, round, model.RoundWithNames.Printer, model.RoundWithNames.Copies, preview);
            }

            if (model.SeededCouples.Print && model.SeededCouples.Enabled)
            {
                PrintSeededCouples(competition, model.SeededCouples.Printer, model.SeededCouples.Copies, preview);
            }

            if (model.ResultService.Print && model.ResultService.Enabled)
            {
                PrintResultService(
                    context,
                    competition,
                    model.ResultService.Printer,
                    model.ResultService.Copies,
                    preview);
            }

            if (model.EventStatistics.Print && model.EventStatistics.Enabled)
            {
                var eventData = context.Events.FirstOrDefault();
                PrintEventStatistics(
                    model.EventStatistics.Printer,
                    model.EventStatistics.Copies,
                    preview,
                    context,
                    eventData);
            }

            if (model.EventOfficials.Print && model.EventOfficials.Enabled)
            {
                var eventData = context.Events.FirstOrDefault();
                PrintEventOfficials(eventData, model.EventOfficials.Printer, model.EventOfficials.Copies, preview);
            }

            if (model.CompetitionOverview.Print && model.CompetitionOverview.Enabled)
            {
                var eventData = context.Events.FirstOrDefault();
                PrintCompetitionOverview(eventData, context, model.EventOfficials.Printer, model.EventOfficials.Copies, preview);
            }

            if (model.EventNumberOverview.Print && model.EventNumberOverview.Enabled)
            {
                var competitions = context.Competitions.ToList();
                PrintStartnumberOverview(competitions, model.EventNumberOverview.Printer, model.EventNumberOverview.Copies, preview);
            }

            if (model.EventCollectedReports.Print && model.EventCollectedReports.Enabled)
            {
                foreach (var comp in context.Competitions)
                {
                    PrintResultService(
                        context,
                        comp,
                        model.EventCollectedReports.Printer,
                        model.EventCollectedReports.Copies,
                        preview);
                }
            }

            if (model.PinCodeJudges.Print && model.PinCodeJudges.Enabled)
            {
                PrintPinCodes(model.PinCodeJudges.Printer, model.PinCodeJudges.Copies, context, preview);
            }

            if (model.EventResultOrderedByPlace.Print && model.EventResultOrderedByPlace.Enabled)
            {
                foreach (var comp in context.Competitions)
                {
                    var printer = new CouplesResultOrderedByPlace(comp, false);
                    PrintReport(
                        model.EventResultOrderedByPlace.Printer,
                        model.EventResultOrderedByPlace.Copies,
                        preview,
                        printer,
                        "Couples Result Ordered By Place");
                }
            }

            if (model.EventCorrectedStartList.Print && model.EventCorrectedStartList.Enabled)
            {
                var printer = new CorrectedStartListPrinter(context.Competitions.OrderBy(c => c.StartTime));
                PrintReport(
                    model.EventCorrectedStartList.Printer,
                    model.EventCorrectedStartList.Copies,
                    preview,
                    printer,
                    "Corrected Startlists Event");
            }

            if (model.EventParticipants.Print && model.EventParticipants.Enabled)
            {
                var printer = new RegisteredCouplesPrinter(context.Competitions);

                PrintReport(
                    model.EventParticipants.Printer,
                    model.EventParticipants.Copies,
                    preview,
                    printer,
                    "Registered Couples");
            }

            if (model.EventProgramBook.Print && model.EventProgramBook.Enabled)
            {
                var printer = new EventProgramBookPrinter();

                PrintReport(model.EventProgramBook.Printer, model.EventProgramBook.Copies, preview, printer, "Program Book");
            }
        }



        public static void PrintCheckSums(string printerName, int copies, bool preview, ScrutinusContext context)
        {
            return;

            // Get a list of officials that are not assigned to competitions anymore
            // that are not yet finished:
            //var officials =
            //    context.Competitions.Where(c => c.State == CompetitionState.Finished)
            //        .SelectMany(o => o.Officials)
            //        .Where(
            //            o =>
            //            o.Role.Id != Roles.Judge && o.Role.Id != Roles.Scrutineer
            //            && o.Role.Id != Roles.TechnicalDelegate)
            //        .Select(o => o.Official)
            //        .Distinct()
            //        .ToList();

            //// Step 2: Nur die offiziellen, die keinen Einsatz mehr haben...

            //foreach (var official in officials)
            //{
            //    var printer = new OfficialsChecksumPrinter(official, context);
            //    PrintReport(printerName, copies, preview, printer, "Prüfsummen");
            //}
        }

        /// <summary>
        ///     Prints the check sums for competition.
        /// </summary>
        /// <param name="printerName">Name of the printer.</param>
        /// <param name="competition">The competition.</param>
        /// <param name="context">The context.</param>
        public static void PrintCheckSumsForCompetition(
            string printerName,
            Competition competition,
            ScrutinusContext context)
        {
            return; 

            //var officials = competition.Officials.Where(o => o.Role.Id != Roles.Judge);

            //// Check that they are not planed anymore
            //foreach (var officialInCompetition in officials)
            //{
            //    var unfinished =
            //        context.Competitions.Where(
            //            c => c.State != CompetitionState.Finished && !c.Canceled && c.Id != competition.Id);

            //    var comp =
            //        unfinished.FirstOrDefault(
            //            c => c.Officials.Any(o => o.Official.Id == officialInCompetition.Official.Id));

            //    if (comp == null)
            //    {
            //        var printer = new OfficialsChecksumPrinter(officialInCompetition.Official, context);
            //        PrintReport(printerName, 1, false, printer, "Prüfsummen");
            //    }
            //}
        }

        public static void PrintCorrectedStartList(
            Competition competition,
            string printerName,
            int copies,
            bool preview = false)
        {
            var printer = new CorrectedStartListPrinter(new List<Competition> { competition });

            PrintReport(printerName, copies, preview, printer, "Corrected Start list");
        }

        public static void PrintCouplesResult(
            Competition competition,
            string printerName,
            int copies,
            bool preview = false)
        {
            var printer = new CouplesResultPrinter(competition);

            PrintReport(printerName, copies, preview, printer, "Couples Results");
        }

        public static void PrintCouplesResultSorted(
            Competition competition,
            string printerName,
            int copies,
            bool preview = false)
        {
            var printer = new CouplesResultOrderedByPlace(competition, false);
            PrintReport(printerName, copies, preview, printer, "Couples Result Ordered");
        }

        public static void PrintCoverPage(Competition competition, string printerName, int copies, bool preview = false)
        {
            var printer = new CoverPagePrinter(competition.Event, new ScrutinusContext());
            PrintReport(printerName, copies, preview, printer, "Cover page");
        }

        /// <summary>
        ///     Prints the DTV annex.
        /// </summary>
        /// <param name="competition">The competition.</param>
        /// <param name="printerName">Name of the printer.</param>
        /// <param name="copies">The copies.</param>
        /// <param name="preview">if set to <c>true</c> [preview].</param>
        public static void PrintDTVAnnex(Competition competition, string printerName, int copies, bool preview = false)
        {
            var printer = new CompetitionReportAppendix(competition);

            PrintReport(printerName, copies, preview, printer, "DTV Anhang");
        }

        private static void PrintCompetitionOverview(Event eventData, ScrutinusContext context, string eventOfficialsPrinter, int eventOfficialsCopies, bool preview)
        {
            var printer = new CompetitionOverviewPrinter(eventData, context);
            PrintReport(eventOfficialsPrinter, eventOfficialsCopies, preview, printer, "Overview of Competition");
        }

        public static void PrintStartnumberOverview(IList<Competition> competitions, string printerName, int copies, bool preview = false)
        {
            var printer = new EventStartNumberOverviewPrinter(competitions);
            PrintReport(printerName, copies, preview, printer, "Overview Startnumbers");
        }

        /// <summary>
        ///     Prints the DTV part1.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="printerName">Name of the printer.</param>
        /// <param name="copies">The copies.</param>
        /// <param name="preview">if set to <c>true</c> [preview].</param>
        public static void PrintDTVPart1(ScrutinusContext context, string printerName, int copies, bool preview = false)
        {
            var printer = new DTVCompetitionReport(context);

            PrintReport(printerName, copies, preview, printer, "DTV Bericht Teil 1");
        }

        public static void PrintDrawingCouples(
            ScrutinusContext context,
            Round round,
            string printerName,
            int copies,
            bool preview = false)
        {
            var printer = new DrawingCouples(round, context);

            PrintReport(printerName, copies, preview, printer, "Drawing");
        }

        public static void PrintDropedOut(
            ScrutinusContext context,
            Round round,
            string printerName,
            int copies,
            bool preview = false)
        {
            if (round == null)
            {
                MainWindow.MainWindowHandle.ShowMessage(
                    LocalizationService.Resolve(() => Text.CouldNotFindRoundToPrint));
                return;
            }

            var printer = new CouplesDroppedOutPrinter(round, context);

            PrintReport(printerName, copies, preview, printer, "Dropped Out");
        }

        public static void PrintEmptyJudgingSheets(
            Competition competition,
            Round round,
            string printerName,
            int copies,
            bool preview = false)
        {
            AbstractPrinter printer;

            if (round.MarksInputType == MarkingTypes.PointsGeneral)
            {
                printer = new UniversalJudgingSheetPrinter(round);
            }
            else
            {
                printer = new JudgingSheetsPrinter(competition, round);
            }

            PrintReport(printerName, copies, preview, printer, "Judging Sheets");
        }

        public static void PrintEventStatistics(
            string printerName,
            int copies,
            bool preview,
            ScrutinusContext context,
            Event eventData)
        {
            if (!context.Competitions.Any())
            {
                MainWindow.MainWindowHandle.ShowMessage(LocalizationService.Resolve(() => Text.EventHasNoCompetitions));
                return;
            }

            var printPage = new EventStatisticsPrinter(eventData, context);

            PrintReport(printerName, copies, preview, printPage, "Event Staticstics");
        }

        public static void PrintEventOfficials(
            Event eventData,
            string printerName,
            int copies,
            bool preview)
        {


            var printPage = new EventOfficialsPrinter(eventData);;

            PrintReport(printerName, copies, preview, printPage, "Event Officials");
        }

        public static void PrintFinalTable(
            ScrutinusContext context,
            Competition competition,
            Round round,
            string printerName,
            int copies,
            bool preview = false)
        {
            if (!round.Qualifieds.All(q => q.PlaceFrom > 0))
            {
                return;
            }

            var printer = new FinalTablePrinter(context, round);

            PrintReport(printerName, copies, preview, printer, "Final Table");
        }

        public static void PrintLaufzettel(Round round, string printerName, int copies, bool preview = false)
        {
            if (round == null)
            {
                MainWindow.MainWindowHandle.ShowMessage(
                    LocalizationService.Resolve(() => Text.CouldNotFindRoundToPrint));
                return;
            }

            var participants =
                round.Qualifieds.Where(
                    q =>
                    !q.QualifiedNextRound && q.Participant.Startbuch != null && q.Participant.Startbuch.PrintLaufzettel
                    && !q.Participant.Startbuch.IsSelfGenerated).Select(q => q.Participant);

            foreach (var participant in participants)
            {
                PrintLaufzettel(participant.Competition, participant, printerName, copies, preview);
            }
        }

        public static void PrintLaufzettel(
            Competition competition,
            Participant participant,
            string printerName,
            int copies,
            bool preview)
        {
            var printer = new LaufzettelPrinter(competition, participant);
            PrintReport(printerName, copies, preview, printer, "Laufzettel");
        }

        public static void PrintMarkingRound(
            ScrutinusContext context,
            Competition competition,
            Round round,
            string Printer,
            int Copies,
            bool preview = false)
        {
            switch (round.MarksInputType)
            {
                case MarkingTypes.NewJudgingSystemV2:
                    PrintMarkingRoundVersion2(context, competition, round, Printer, Copies, preview);
                    break;
                case MarkingTypes.Marking:
                    PrintMarkingsRoundMarks(context, round, Printer, Copies, preview);
                    break;
                case MarkingTypes.MarksSumOnly:
                    PrintMarkingsRoundMarksSum(context, round, Printer, Copies, preview);
                    break;
                case MarkingTypes.Skating:
                    PrintFinalTable(context, competition, round, Printer, Copies, preview);
                    break;
            }
        }

        public static void PrintMarkingTableTotal(
            ScrutinusContext context,
            Competition competition,
            string printerName,
            int copies,
            bool preview = false)
        {
            var markingPrinter = new TotalMarkingPrinter(competition, context);

            PrintReport(printerName, copies, preview, markingPrinter, "Marking table total");
        }

        public static void PrintOfficals(Competition competition, string printerName, int copies, bool preview = false)
        {
            var officialsPrinter = new OfficialsPrinter(competition);

            PrintReport(printerName, copies, preview, officialsPrinter, "Officials");
        }

        public static void PrintQualified(
            ScrutinusContext context,
            Round round,
            string printerName,
            int copies,
            bool preview = false)
        {
            var qualifiedPrinter = new QualifiedCouplesPrinter(round, context);

            PrintReport(printerName, copies, preview, qualifiedPrinter, "Qualified Couples");
        }

        public static void PrintReport(
            string printerName,
            int copies,
            bool preview,
            AbstractPrinter printer,
            string printTitle,
            bool landscape = false)
        {
            var printPage = new PrintReportPage();

            try
            {
                printPage.Print(printer, false, preview, printerName, copies, printTitle, null, landscape);

                if (preview)
                {
                    MainWindow.MainWindowHandle.ShowChildWindow(printPage);
                }
            }
            catch (Exception ex)
            {
                MainWindow.MainWindowHandle.ShowMessage("Error printing: " + ex.Message);
            }
        }

        public static void PrintResultByRegions(
            Competition competition,
            string printerName,
            int copies,
            bool preview = false)
        {
            var printer = new ResultOrderedByRegions(competition);
            PrintReport(printerName, copies, preview, printer, "Result by Regions");
        }

        public static void PrintPinCodes(
            string printerName,
            int copies,
            ScrutinusContext context,
            bool preview = false)
        {
            var printer = new PinCodePrinter(context);
            PrintReport(printerName, copies, preview, printer, "PIN Codes");
        }

        /// <summary>
        ///     Prints the result service.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="competition">The competition.</param>
        /// <param name="round">The round.</param>
        /// <param name="Printer">The printer.</param>
        /// <param name="Copies">The copies.</param>
        /// <param name="preview">if set to <c>true</c> [preview].</param>
        public static void PrintResultService(
            ScrutinusContext context,
            Competition competition,
            string Printer,
            int copies,
            bool preview = false)
        {
            NumberOfCopiesCollectedReports collectedReportDefinition;
            // Get the settings for this:
            var bytes = Convert.FromBase64String(Settings.Default.CollectedReportDefinition);

            if (bytes.Length == 0)
            {
                MainWindow.MainWindowHandle.ShowMessage(LocalizationService.Resolve(() => Text.NoCollectedReports));
                return;
            }
            var stream = new MemoryStream(bytes);

            var bf = new BinaryFormatter();
            collectedReportDefinition = (NumberOfCopiesCollectedReports)bf.Deserialize(stream);

            var somethingPrinted = false;

            if (collectedReportDefinition.NumberDtvAppendix > 0)
            {
                PrintDTVAnnex(competition, Printer, collectedReportDefinition.NumberDtvAppendix * copies, false);
                somethingPrinted = true;
            }

            if (collectedReportDefinition.NumberDtvPart1 > 0)
            {
                PrintDTVPart1(context, Printer, collectedReportDefinition.NumberDtvPart1 * copies, false);
                somethingPrinted = true;
            }

            if (collectedReportDefinition.NumberFirstPage > 0)
            {
                PrintCoverPage(competition, Printer, collectedReportDefinition.NumberFirstPage * copies, false);
                somethingPrinted = true;
            }

            if (collectedReportDefinition.NumberResultTotal > 0)
            {
                PrintMarkingTableTotal(
                    context,
                    competition,
                    Printer,
                    collectedReportDefinition.NumberResultTotal * copies,
                    false);
                somethingPrinted = true;
            }

            if (collectedReportDefinition.NumberSkatingTable > 0)
            {
                var final = competition.Rounds.FirstOrDefault(r => r.RoundType.IsFinal);
                somethingPrinted = true;
                if (final != null)
                {
                    PrintFinalTable(
                        context,
                        competition,
                        final,
                        Printer,
                        collectedReportDefinition.NumberSkatingTable * copies,
                        false);
                    PrintSkatingReport(
                        context,
                        competition,
                        final,
                        Printer,
                        collectedReportDefinition.NumberSkatingTable * copies,
                        false);
                }
            }

            if (collectedReportDefinition.NumberSortedByResult > 0)
            {
                PrintCouplesResultSorted(
                    competition,
                    Printer,
                    collectedReportDefinition.NumberSortedByResult * copies,
                    false);
                somethingPrinted = true;
            }

            if (collectedReportDefinition.NumberStartListWithResult > 0)
            {
                PrintCouplesResult(
                    competition,
                    Printer,
                    collectedReportDefinition.NumberStartListWithResult * copies,
                    false);
                somethingPrinted = true;
            }

            if (!somethingPrinted)
            {
                MainWindow.MainWindowHandle.ShowMessage(LocalizationService.Resolve(() => Text.NoCollectedReports));
            }
        }

        public static void PrintRoundDrawing(
            ScrutinusContext context,
            Round roundToPrint,
            string printerName,
            int copies,
            bool preview = false)
        {
            var printer = new RoundDrawingPrinter(roundToPrint, context);

            PrintReport(printerName, copies, preview, printer, "Round Drawing");
        }

        public static void PrintRoundWithNames(
            ScrutinusContext context,
            Round round,
            string printerName,
            int copies,
            bool preview = false)
        {
            var printer = new CouplesByHeatPrinter(round, context);

            PrintReport(printerName, copies, preview, printer, "Heats with names");
        }

        public static void PrintSeededCouples(Competition competition, string printer, int copies, bool preview = false)
        {
            var seededCouplesPrinter = new SeededCouples(competition);

            PrintReport(printer, copies, preview, seededCouplesPrinter, "Seeded Couples");
        }

        public static void PrintSkatingReport(
            ScrutinusContext context,
            Competition competition,
            Round round,
            string printerName,
            int copies,
            bool preview = false)
        {
            if (!round.Qualifieds.All(p => p.PlaceFrom > 0))
            {
                return;
            }

            var skatingReport = new FinalSkatingTablePrinter(context, round);
            PrintReport(printerName, copies, preview, skatingReport, "Skating Report");
        }

        #endregion

        #region Methods

        private static void CertificateDialogClosed(object obj)
        {
            var sender = obj as CertificateDialog;

            var viewModel = sender.DataContext as PrintCertificateDialogViewModel;

            if (viewModel.Result == DialogResult.OK)
            {
                PrintCertificate(viewModel);
            }
        }

        private static double GetTotalArea(List<double> list)
        {
            var marklist = new List<double>();
            marklist.AddRange(list);
            marklist.AddRange(list);
            marklist.Sort();
            marklist.RemoveAt(marklist.Count - 1);
            marklist.RemoveAt(0);
            return marklist.Sum() / marklist.Count;
        }

        private static void PrintCertificate(PrintCertificateDialogViewModel viewModel)
        {
            var certificatePrinter = new WinnerCertificatePrinter(
                viewModel.Round,
                viewModel.SelectedDefinition,
                viewModel.Participants);

            var printPage = new PrintReportPage();
            printPage.Print(certificatePrinter, false, false, viewModel.Printer, viewModel.Copies, "Certificates");
         }

        private static void PrintMarkingRoundVersion2(
            ScrutinusContext context,
            Competition competition,
            Round round,
            string Printer,
            int Copies,
            bool preview = false)
        {
            // We generate our print model
            var printModel = new MarkingV2Model { Markings = new Dictionary<string, List<MarkingV2PrintModel>>() };

            var couples = round.Qualifieds.OrderBy(q => q.PlaceFrom);

            foreach (var danceRound in round.DancesInRound)
            {
                var list = new List<MarkingV2PrintModel>();
                printModel.Markings.Add(danceRound.Dance.DanceName, list);
                foreach (var qualified in couples)
                {
                    var markings = new MarkingV2PrintModel { Couple = qualified.Participant.Number, };
                    list.Add(markings);
                    // Set Marks for Area 1:
                    var marks =
                        context.Markings.Where(
                            m =>
                            m.Dance.Id == danceRound.Dance.Id && m.Round.Id == round.Id
                            && m.Participant.Id == qualified.Participant.Id && m.MarkingComponent == 1)
                            .OrderBy(m => m.Judge.Sign)
                            .ToList();
                    if (marks.Count < 3)
                    {
                        continue;
                    }
                    markings.JudgeArea1_1 = marks[0].Judge.Sign;
                    markings.JudgeArea1_2 = marks[1].Judge.Sign;
                    markings.JudgeArea1_3 = marks[2].Judge.Sign;
                    markings.Mark1_1 = marks[0].Mark20;
                    markings.Mark1_2 = marks[1].Mark20;
                    markings.Mark1_3 = marks[2].Mark20;
                    var total1 = GetTotalArea(marks.Select(p => p.Mark20).ToList());
                    // Marks Area 2
                    marks =
                        context.Markings.Where(
                            m =>
                            m.Dance.Id == danceRound.Dance.Id && m.Round.Id == round.Id
                            && m.Participant.Id == qualified.Participant.Id && m.MarkingComponent == 2)
                            .OrderBy(m => m.Judge.Sign)
                            .ToList();
                    if (marks.Count < 3)
                    {
                        continue;
                    }
                    markings.JudgeArea2_1 = marks[0].Judge.Sign;
                    markings.JudgeArea2_2 = marks[1].Judge.Sign;
                    markings.JudgeArea2_3 = marks[2].Judge.Sign;
                    markings.Mark2_1 = marks[0].Mark20;
                    markings.Mark2_2 = marks[1].Mark20;
                    markings.Mark2_3 = marks[2].Mark20;
                    var total2 = GetTotalArea(marks.Select(p => p.Mark20).ToList());
                    // Marks Area 3
                    marks =
                        context.Markings.Where(
                            m =>
                            m.Dance.Id == danceRound.Dance.Id && m.Round.Id == round.Id
                            && m.Participant.Id == qualified.Participant.Id && m.MarkingComponent == 3)
                            .OrderBy(m => m.Judge.Sign)
                            .ToList();
                    if (marks.Count < 3)
                    {
                        continue;
                    }
                    markings.JudgeArea3_1 = marks[0].Judge.Sign;
                    markings.JudgeArea3_2 = marks[1].Judge.Sign;
                    markings.JudgeArea3_3 = marks[2].Judge.Sign;
                    markings.Mark3_1 = marks[0].Mark20;
                    markings.Mark3_2 = marks[1].Mark20;
                    markings.Mark3_3 = marks[2].Mark20;
                    var total3 = GetTotalArea(marks.Select(p => p.Mark20).ToList());
                    // Marks Area 4
                    marks =
                        context.Markings.Where(
                            m =>
                            m.Dance.Id == danceRound.Dance.Id && m.Round.Id == round.Id
                            && m.Participant.Id == qualified.Participant.Id && m.MarkingComponent == 4)
                            .OrderBy(m => m.Judge.Sign)
                            .ToList();
                    if (marks.Count < 3)
                    {
                        continue;
                    }
                    markings.JudgeArea4_1 = marks[0].Judge.Sign;
                    markings.JudgeArea4_2 = marks[1].Judge.Sign;
                    markings.JudgeArea4_3 = marks[2].Judge.Sign;
                    markings.Mark4_1 = marks[0].Mark20;
                    markings.Mark4_2 = marks[1].Mark20;
                    markings.Mark4_3 = marks[2].Mark20;
                    var total4 = GetTotalArea(marks.Select(p => p.Mark20).ToList());
                    // Calculate Total:
                    markings.Total = total1 + total2 + total3 + total4;
                }
                // We calculate the places:
                var sorted = list.OrderByDescending(o => o.Total).ToList();
                var place = 1;
                for (var i = 0; i < sorted.Count() - 1; i++)
                {
                    sorted[i].Place = place + ".";
                    if (sorted[i] != sorted[i + 1])
                    {
                        place++;
                    }
                }
                sorted[sorted.Count - 1].Place = place + ".";
            }

            MainWindow.MainWindowHandle.ShowMessage("This feature is not yet implemented");
        }

        private static void PrintMarkingsRoundMarks(
            ScrutinusContext context,
            Round round,
            string printerName,
            int copies,
            bool preview = false)
        {
            var markingPrinter = new MarkingRoundPrinter(round, context);

            PrintReport(printerName, copies, preview, markingPrinter, "Markings");
        }

        public static void PrintPlacingsOfQualifiedCouples(
            Round round,
            string printerName,
            int copies,
            bool preview = false)
        {
            var qualifiedResultPrinter = new QualifiedResultPrinter(round);

            PrintReport(printerName, copies, preview, qualifiedResultPrinter, "Placings after Round");
        }

        private static void PrintMarkingsRoundMarksSum(
            ScrutinusContext context,
            Round round,
            string printerName,
            int copies,
            bool preview = false)
        {
            var markingPrinter = new MarkingSumsPrinter(round, context);

            PrintReport(printerName, copies, preview, markingPrinter, "Markings");
        }

        private static void PrintRegisteredCouples(
            Competition competition,
            string printerName,
            int copies,
            bool preview = false)
        {
            var printer = new RegisteredCouplesPrinter(new List<Competition> { competition });

            PrintReport(printerName, copies, preview, printer, "Registered Couples");
        }

        #endregion
    }
}