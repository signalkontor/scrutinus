﻿// // TPS.net TPS8 Scrutinus
// // PrintTicketHelper.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.IO;
using System.Printing;
using System.Xml;

namespace Scrutinus.Reports
{
    public static class XpsPrinterUtils
    {
        public static string GetInputBinName(PrintQueue printQueue, string binName, out string nameSpaceURI)
        {
            var binNameOut = string.Empty;

            // get PrintCapabilities of the printer
            var printerCapXmlStream = printQueue.GetPrintCapabilitiesAsXml();
            // read the JobInputBins out of the PrintCapabilities
            var xmlDoc = new XmlDocument();
            xmlDoc.Load(printerCapXmlStream);
            // create NamespaceManager and add PrintSchemaFrameWork-Namespace (should be on DocumentElement of the PrintTicket)
            // Prefix: psf  NameSpace: xmlDoc.DocumentElement.NamespaceURI = "http://schemas.microsoft.com/windows/2003/08/printing/printschemaframework"
            var manager = new XmlNamespaceManager(xmlDoc.NameTable);
            manager.AddNamespace(xmlDoc.DocumentElement.Prefix, xmlDoc.DocumentElement.NamespaceURI);
            nameSpaceURI = xmlDoc.ChildNodes[1].GetNamespaceOfPrefix("ns0000");
            // and select all nodes of the bins
            var nodeList = xmlDoc.SelectNodes("//psf:Feature[@name='psk:JobInputBin']/psf:Option", manager);
            // fill Dictionary with the bin-names and values
           
            for (int binIndex = 0; binIndex < nodeList.Count; binIndex++)
            {
                if (nodeList[binIndex].InnerText.Equals(binName))
                {
                    binNameOut = nodeList[binIndex].Attributes["name"].Value;
                    break;
                }
            }
                      
            return binNameOut;
        }

        public static PrintTicket ModifyPrintTicket(PrintTicket ticket, string featureName, string newValue, string nameSpaceURI)
        {
            if (ticket == null)
            {
                throw new ArgumentNullException("ticket");
            }
            // read Xml of the PrintTicket
            var xmlDoc = new XmlDocument();
            xmlDoc.Load(ticket.GetXmlStream());
            // create NamespaceManager and add PrintSchemaFrameWork-Namespace (should be on DocumentElement of the PrintTicket)
            // Prefix: psf  NameSpace: xmlDoc.DocumentElement.NamespaceURI = "http://schemas.microsoft.com/windows/2003/08/printing/printschemaframework"
            var manager = new XmlNamespaceManager(xmlDoc.NameTable);
            manager.AddNamespace(xmlDoc.DocumentElement.Prefix, xmlDoc.DocumentElement.NamespaceURI);
            // search node with desired feature we're looking for and set newValue for it

            var xpath = string.Format("//psf:Feature[@name='{0}']/psf:Option", featureName);
            var node = xmlDoc.SelectSingleNode(xpath, manager);
            if (node != null)
            {
                if (newValue.StartsWith("ns0000"))
                {
                    // add namespace to xml doc
                    var namespaceAttribute = xmlDoc.CreateAttribute("xmlns:ns0000");
                    namespaceAttribute.Value = nameSpaceURI;
                    xmlDoc.DocumentElement.Attributes.Append(namespaceAttribute);
                }
                node.Attributes["name"].Value = newValue;
            }
            // create a new PrintTicket out of the XML
            var printTicketStream = new MemoryStream();
            xmlDoc.Save(printTicketStream);
            printTicketStream.Position = 0;
            var modifiedPrintTicket = new PrintTicket(printTicketStream);
            // for testing purpose save the printticket to file
            //FileStream stream = new FileStream("modPrintticket.xml", FileMode.CreateNew, FileAccess.ReadWrite);
            //modifiedPrintTicket.GetXmlStream().WriteTo(stream);
            return modifiedPrintTicket;
        }
    }

}
