﻿// // TPS.net TPS8 Scrutinus
// // CanPrintBeforeFinalRoundAttribute.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;

namespace Scrutinus.Reports.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class CanPrintBeforeFinalRoundAttribute : Attribute
    {
        public CanPrintBeforeFinalRoundAttribute(bool value)
        {
            this.Value = value;
        }

        public bool Value { get; set; }
    }
}
