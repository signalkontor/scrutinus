﻿// // TPS.net TPS8 Scrutinus
// // StatisticPrintModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using DataModel;
using DataModel.Models;

namespace Scrutinus.Reports.PrintModels
{
    public class CompetitionStatistic
    {
        #region Public Properties

        public Competition Competition { get; set; }

        public string Text { get; set; }

        public string Floor { get; set; }

        public int Dancing { get; set; }

        public int Excused { get; set; }

        public int Missing { get; set; }

        public int Registered { get; set; }

        #endregion
    }

    public class StatisticPrintModel
    {
        #region Constructors and Destructors

        public StatisticPrintModel(ScrutinusContext context)
        {
            this.CompetitionStatistics = new List<CompetitionStatistic>();

            foreach (var competition in context.Competitions)
            {
                var statistic = new CompetitionStatistic
                                    {
                                        Competition = competition,
                                        Floor = competition.Floor,
                                        Registered = competition.Participants.Count,
                                        Excused =
                                            competition.Participants.Count(
                                                p => p.State == CoupleState.Excused),
                                        Missing =
                                            competition.Participants.Count(
                                                p => p.State == CoupleState.Missing),
                                    };
                statistic.Dancing = statistic.Registered - statistic.Excused - statistic.Missing;

                this.CompetitionStatistics.Add(statistic);
            }

            this.CouplesByRegions =
                context.Participants.OrderBy(p => p.Couple.Region).GroupBy(p => p.Couple.Region).ToList();
        }

        #endregion

        #region Public Properties

        public List<CompetitionStatistic> CompetitionStatistics { get; set; }

        public List<IGrouping<string, Participant>> CouplesByRegions { get; set; }

        #endregion
    }
}