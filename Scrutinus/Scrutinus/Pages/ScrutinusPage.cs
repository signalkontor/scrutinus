﻿// // TPS.net TPS8 Scrutinus
// // ScrutinusPage.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using DataModel.Models;
using Scrutinus.ViewModels;

namespace Scrutinus.Pages
{
    public abstract class ScrutinusPage : Page, IDisposable
    {
        #region Public Properties

        public Competition Competition
        {
            get
            {
                if (this.competition == null)
                {
                    this.competition = this.GetCompetition();
                }

                return this.competition;
            }
        }

        #endregion

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }


        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.context != null)
                {
                    this.context.Dispose();
                    this.context = null;
                    this.competition = null;
                    this.round = null;
                }
            }
        }

        #region Fields

        protected int competitionId;

        protected ScrutinusContext context;

        protected int currentRoundId;

        private Competition competition;

        private Round round;

        private bool isVisible;

        #endregion

        #region Constructors and Destructors

        public ScrutinusPage()
        {
            this.context = new ScrutinusContext();
            this.ShowsNavigationUI = false;
        }

        protected ScrutinusPage(int competitionId, int roundId)
        {
            this.competitionId = competitionId;
            this.currentRoundId = roundId;
            this.context = new ScrutinusContext();
            this.SetViewModel();
        }

        #endregion

        #region Properties

        public Round CurrentRound
        {
            get
            {
                if (this.round == null)
                {
                    this.round = this.GetCurrentRound();
                }
                return this.round;
            }
        }

        public bool IsPageVisible
        {
            get { return this.isVisible; }
            set
            {
                this.isVisible = value;
                if (this.ViewModel != null)
                {
                    this.ViewModel.IsVisible = value;
                }
            }
        }

        protected virtual BaseViewModel ViewModel { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Is it allowed to leave the page (to go to a different competition)
        ///     This Handler is called whenever the user intends to display a different page
        /// </summary>
        /// <returns></returns>
        public abstract bool AllowToLeave();

        /// <summary>
        ///     Called if the user clicks Next to confirm next is
        ///     allowed at this state
        /// </summary>
        /// <returns></returns>
        public abstract bool CanMoveNext();

        public virtual VerticalAlignment FrameVerticalContentAllignment => VerticalAlignment.Stretch;

        /// <summary>
        ///     Can be overridden to clear up any mess we have made in memory
        /// </summary>
        public virtual void OnLeft()
        {
            // this.Visibility = Visibility.Hidden;
        }

        public void OnNext()
        {
            // We check if the competition has changes while we have
            // been on this page...
            if (this.context != null && this.Competition != null && MainWindow.IsInNetworkMode)
            {
                using (var dbContext = new ScrutinusContext())
                {
                    var dbCompetition = dbContext.Competitions.Single(c => c.Id == this.Competition.Id);

                    if (dbCompetition.State != this.Competition.State)
                    {
                        this.CompetitionHasChanged();
                        return;
                    }

                    if (this.Competition.CurrentRound != null && dbCompetition.CurrentRound != null)
                    {
                        if (dbCompetition.CurrentRound.Id != this.Competition.CurrentRound.Id
                            || dbCompetition.CurrentRound.State != this.Competition.CurrentRound.State)
                        {
                            this.CompetitionHasChanged();
                        }
                    }
                }
            }
            

            this.OnNextInternal();

            this.context = null;
            this.competition = null;
            this.round = null;
        }

        private void CompetitionHasChanged()
        {
            // MainWindow.MainWindowHandle.ShowMessage(LocalizationService.Resolve(() => Text.DataHasBeenChanged));

            // throw new AccessViolationException("Network changed exception");
        }

        public void OnActivated()
        {
            
        }

        /// <summary>
        ///     Called befor the next page is displayed
        ///     Use to save data or to tidy up your mess
        /// </summary>
        protected abstract void OnNextInternal();

        #endregion

        #region Methods

        protected Competition GetCompetition()
        {
            return this.context?.Competitions
                .SingleOrDefault(c => c.Id == this.competitionId);
        }

        protected Round GetCurrentRound()
        {
            return this.context.Rounds.SingleOrDefault(r => r.Id == this.currentRoundId);
        }

        protected abstract void SetViewModel();

        #endregion
    }
}