﻿// // TPS.net TPS8 Scrutinus
// // TeamsViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.ObjectModel;
using System.Windows.Input;
using DataModel.Models;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace Scrutinus.Pages.Teams
{
    public class TeamsViewModel : ViewModelBase
    {
        #region Constructors and Destructors

        public TeamsViewModel()
        {
            this.dataContext = new ScrutinusContext();

            this.Teams = new ObservableCollection<Team>(this.dataContext.Teams);

            this.AddTeamCommand = new RelayCommand(this.AddTeam);
            this.DeleteTeamCommand = new RelayCommand(this.DeleteTeam);
            this.SaveCommand = new RelayCommand(this.Save);
        }

        #endregion

        #region Fields

        private readonly ScrutinusContext dataContext;

        private Team selectedTeam;

        #endregion

        #region Public Properties

        public ICommand AddTeamCommand { get; set; }

        public ICommand DeleteTeamCommand { get; set; }

        public ICommand SaveCommand { get; set; }

        public Team SelectedTeam
        {
            get
            {
                return this.selectedTeam;
            }
            set
            {
                this.selectedTeam = value;
                this.RaisePropertyChanged(() => this.SelectedTeam);
            }
        }

        public ObservableCollection<Team> Teams { get; set; }

        #endregion

        #region Methods

        private void AddTeam()
        {
            var team = new Team();
            this.dataContext.Teams.Add(team);
            this.Teams.Add(team);
            this.SelectedTeam = team;
        }

        private void DeleteTeam()
        {
            if (this.SelectedTeam == null)
            {
                return;
            }

            this.Teams.Remove(this.SelectedTeam);
            this.dataContext.Teams.Remove(this.SelectedTeam);
            this.SelectedTeam = null;
        }

        private void Save()
        {
            this.dataContext.SaveChanges();
        }

        #endregion
    }
}