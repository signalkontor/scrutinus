﻿// // TPS.net TPS8 Scrutinus
// // Teams.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows.Controls;

namespace Scrutinus.Pages.Teams
{
    /// <summary>
    ///     Interaction logic for Teams.xaml
    /// </summary>
    public partial class Teams : Page
    {
        #region Constructors and Destructors

        public Teams()
        {
            this.InitializeComponent();
        }

        #endregion
    }
}