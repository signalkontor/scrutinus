﻿// // TPS.net TPS8 Scrutinus
// // EditParticipants.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.ComponentModel;
using System.Threading;
using System.Windows.Markup;
using Scrutinus.ViewModels;

namespace Scrutinus.Pages
{
    /// <summary>
    ///     Interaction logic for EditParticipants.xaml
    /// </summary>
    public partial class EditParticipants : ScrutinusPage
    {
        private readonly PropertyChangedEventHandler viewModelChangedEventHandler;
        private readonly EditParticipantsViewModel viewModel;

        #region Constructors and Destructors

        public EditParticipants(int competitionId, int roundId)
            : base(competitionId, roundId)
        {
            this.InitializeComponent();
            this.Language = XmlLanguage.GetLanguage(Thread.CurrentThread.CurrentCulture.Name);

            this.viewModel = this.DataContext as EditParticipantsViewModel;

            

            this.viewModel.PropertyChanged += this.viewModelChangedEventHandler;
        }

        #endregion

        #region Methods

        protected override void SetViewModel()
        {
            // Viewmodel has been set directly in the view...
        }

        #endregion

        #region Public Methods and Operators

        public override bool AllowToLeave()
        {
            return true;
        }

        public override bool CanMoveNext()
        {
            return false;
        }

        protected override void OnNextInternal()
        {
            this.viewModel.PropertyChanged -= this.viewModelChangedEventHandler;
        }

        #endregion
    }
}