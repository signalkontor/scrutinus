﻿// // TPS.net TPS8 Scrutinus
// // EnterMarking.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.Entity;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Threading;
using DataModel;
using DataModel.Export;
using DataModel.ModelHelper;
using DataModel.Models;
using GalaSoft.MvvmLight.Messaging;
using General;
using mobileControl.Model;
using MahApps.Metro.Controls.Dialogs;
using MobileControlLib.EjudgeMessages;
using Scrutinus.Controls;
using Scrutinus.Converters;
using Scrutinus.Dialogs;
using Scrutinus.Dialogs.DialogViewModels;
using Scrutinus.Dialogs.SettingsDialogs;
using Scrutinus.ExternalApi;
using Scrutinus.Localization;
using Scrutinus.MyHeats;

namespace Scrutinus.Pages
{
    /// <summary>
    ///     Interaction logic for EnterMarking.xaml
    /// </summary>
    public partial class EnterMarking : ScrutinusPage, INotifyPropertyChanged
    {
        private FileSystemWatcher fileSystemWatcher;
        #region Constructors and Destructors

        public EnterMarking(int competitionId, int roundId)
            : base(competitionId, roundId)
        {
            this.Qualifieds = new ObservableCollection<Qualified>(this.context.Qualifieds.Where(q => q.Round.Id == roundId).AsNoTracking());

            this.InitializeComponent();

            MainWindow.MainWindowHandle.ViewModel.MarkingInputEnabled = this.context.GetParameter(
                "MarkingInputEnabled",
                true);

            this.judgements = new List<JudgementViewModel>();
            // Add the Tabs for each Judge
            this.CreateTabs(null);

            this.TxtImportFileName.Text = $"{this.Competition.Id:0000}_{this.CurrentRound.Number}_##_Wt.dat";

            if (this.CurrentRound.Number > 1)
            {
                this.QuickAddCoupleButton.Visibility = Visibility.Collapsed;
                this.RemoveCoupleButton.Visibility = Visibility.Collapsed;
            }

            Messenger.Default.Register<MarkMessage>(this, this.OnMarkMessage);
            Messenger.Default.Register<JudgeLogonMessage>(this, this.OnJudgeLogon);
            Messenger.Default.Register<EjudgeMessage>(this, this.OnEjudgeMessage);
            
            // Register FileSystemWatcher:
            if (Settings.Default.MobileControlExchange != null &&
                this.fileSystemWatcher != null &&
                Directory.Exists(Settings.Default.MobileControlExchange))
            {
                this.fileSystemWatcher.Path = Settings.Default.MobileControlExchange;
                this.fileSystemWatcher.Filter = string.Format("{0:0000}_{1}_##_done.dat",
                    this.Competition.Id,
                    this.CurrentRound.Number);
                this.fileSystemWatcher.EnableRaisingEvents = true;
                this.fileSystemWatcher.Created += this.EjudgeSignaturesDone;
            }

            // Write eJudge data to our path
            var filename = string.Format(
                "{2}\\{0:0000}_{1}_{3}_Sl.dat",
                this.Competition.Id,
                this.CurrentRound.Number,
                Settings.Default.MobileControlExchange,
                "##");

            this.EjudgeDataExists = File.Exists(filename);

            if (!EjudgeDataExists && Directory.Exists(Settings.Default.MobileControlExchange))
            {
                try
                {
                    this.BtnWriteData_OnClick(null, null);
                }
                catch (Exception ex)
                {
                    ScrutinusContext.WriteLogEntry(ex);
                }
            }

            if (Settings.Default.MyHeatsUploadAutomatically)
            {
                Task.Factory.StartNew(() =>
                {
                    this.OnSendToMyHeats(null, null);
                });
            }
        }

        private void EjudgeSignaturesDone(object sender, FileSystemEventArgs e)
        {
            BtnReadData_OnClick(this.ButtonReadData, null);
        }

        #endregion

        #region Public Events

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        private void OnMarkMessage(MarkMessage markMessage)
        {
            if (this.context == null || markMessage.RoundCode != this.CurrentRound.EjudgeName)
            {
                return;
            }

            var eJudgeData = this.CurrentRound.EjudgeData.First(j => j.DeviceId == markMessage.Sender);

            var judgement =
                this.judgements.FirstOrDefault(
                    j =>
                    j.Participant.Number == markMessage.Number && j.Dance.SortOrder == markMessage.DanceIndex
                    && j.Official.Id == eJudgeData.Judge.Id);

            if (judgement != null)
            {
                judgement.Mark = markMessage.Mark == "X";
                markMessage.IsHandled = true;
            }

            var qualified = this.Qualifieds.Single(q => q.Participant.Number == markMessage.Number);
            qualified.Points = this.judgements.Count(j => j.Participant.Number == qualified.Participant.Number && j.Mark);

            this.RaisePropertyChanged("NumberOfMarks");
        }

        private void OnCompleteMarking(CompleteMarkingMessage message)
        {
            if (this.context == null || message.RoundCode != this.CurrentRound.EjudgeName)
            {
                return;
            }

            var stopwatch = new Stopwatch();
            stopwatch.Start();

            var eJudgeData = this.CurrentRound.EjudgeData.First(j => j.DeviceId == message.Sender);

            var judgementsOfJudge = this.judgements.Where(j => j.Official.Id == eJudgeData.Judge.Id).ToList();

            foreach (var number in message.Marks.Keys)
            {
                var list = message.Marks[number];

                for (var index = 1; index < list.Count; index++)
                {
                    var judgement =
                        judgementsOfJudge.FirstOrDefault(
                            j => j.Participant.Number == number && j.Dance.SortOrder == index);

                    if (judgement != null)
                    {
                        judgement.Mark = list[index - 1] == "X";
                    }
                    else
                    {
                        Debug.WriteLine("Why is this null");
                    }
                }
            }

            this.RaisePropertyChanged("NumberOfMarks");

            message.IsHandled = true;

            stopwatch.Stop();
            Debug.WriteLine("Laufzeit OnCompleteMarking:" + stopwatch.ElapsedMilliseconds);
        }

        private void OnSignatureMessage(SignatureMessage message)
        {
            if (this.context == null || message.RoundCode != this.CurrentRound.EjudgeName)
            {
                return;
            }

            var ejudgeData = this.CurrentRound.EjudgeData.FirstOrDefault(e => e.DeviceId == message.Sender);

            foreach (var tuple in message.Coordinates)
            {
                ejudgeData.SignatureCoordinates.Add(new Coordinate() {X1 = tuple.Item1, Y1 = tuple.Item2, X2 = tuple.Item3, Y2 = tuple.Item4});
            }

            this.context.SaveChanges();

            message.IsHandled = true;
        }

        private void OnJudgeLogon(JudgeLogonMessage judgeLogonMessage)
        {
            if (this.context == null || !this.CurrentRound.EjudgeEnabled)
            {
                return;
            }

            if (judgeLogonMessage.Round.Id != this.CurrentRound.Id)
            {
                return;
            }

            var ejudgeData = this.CurrentRound.EjudgeData.FirstOrDefault(e => e.Judge.Sign == judgeLogonMessage.Judge.Sign);

            if (ejudgeData != null)
            {
                // find device:
                var device = MainWindow.MainWindowHandle.ViewModel.Devices.First(d => d.DeviceId == judgeLogonMessage.DeviceId);

                if (device != null)
                {
                    ejudgeData.Device = device;
                    ejudgeData.DeviceId = judgeLogonMessage.DeviceId;
                    device.Round = this.CurrentRound;
                    judgeLogonMessage.IsHandled = true;
                }
            }
        }

        private void OnEjudgeMessage(EjudgeMessage message)
        {
            if (message is MarkMessage)
            {
                this.OnMarkMessage((MarkMessage)message);
            }

            if (message is CompleteMarkingMessage)
            {
                this.OnCompleteMarking((CompleteMarkingMessage)message);
            }

            if (message is SignatureMessage)
            {
                this.OnSignatureMessage((SignatureMessage) message);

                // We check if we now have all signatures:
                if(this.CurrentRound.EjudgeData.All(e => e.Device.DeviceState == DeviceStates.StartOldData || e.Device.DeviceState == DeviceStates.Signature))
                {
                    // we try to move forward:
                    // If we do not have enough marks this will fail.
                    MainWindow.MainWindowHandle.NextState(this.competitionId);
                }
            }
        }

        private void RaisePropertyChanged(string propertyName)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #region Fields

        private readonly List<JudgementViewModel> judgements;

        private readonly List<Grid> markingGrids = new List<Grid>();

        private bool doNotSaveJudgements;

        private bool ejudgeDataExists;

        #endregion

        #region Public Properties

        public Round Round
        {
            get
            {
                return this.CurrentRound;
            }
        }

        public int NumberOfMarks
        {
            get
            {
                return 0;
            }
        }

        public ObservableCollection<Qualified> Qualifieds { get; set; }

        public ScrutinusContext Context
        {
            get
            {
                return this.context;
            }   
        }

        public bool EjudgeDataExists
        {
            get
            {
                return ejudgeDataExists; 
            }
            set
            {
                this.ejudgeDataExists = value;
                this.RaisePropertyChanged(nameof(EjudgeDataExists));
            }
        }

        #endregion

        #region Public Methods and Operators

        public static void CalculatePlaces(List<Qualified> qualifieds, int placeOffset)
        {
            // Calculate Places:
            foreach (var qualified in qualifieds)
            {
                qualified.PlaceFrom = 0;
                qualified.PlaceTo = 0;
            }

            var sorted = qualifieds.OrderByDescending(q => q.Sum).ToList();
            var placefrom = 1;
            for (var i = 0; i < sorted.Count - 1; i++)
            {
                sorted[i].PlaceFrom = placefrom + placeOffset;
                sorted[i].PlaceTo = placefrom + placeOffset;
                if (sorted[i + 1].Sum == sorted[i].Sum)
                {
                    var index = i + 1;
                    var placecount = 1;
                    while (index < sorted.Count && sorted[i].Sum == sorted[index].Sum)
                    {
                        index++;
                        placecount++;
                    }
                    for (var j = i; j < index; j++)
                    {
                        sorted[j].PlaceFrom = placefrom + placeOffset;
                        sorted[j].PlaceTo = placefrom + placecount + placeOffset - 1;
                    }
                    i = index - 1;
                    placefrom += placecount - 1;
                }
                placefrom++;
            }
            if (sorted[sorted.Count - 1].PlaceFrom == 0)
            {
                sorted[sorted.Count - 1].PlaceFrom = placefrom + placeOffset;
                sorted[sorted.Count - 1].PlaceTo = placefrom + placeOffset;
            }
        }

        public override bool AllowToLeave()
        {
            return true;
        }

        public override bool CanMoveNext()
        {
            // We check, if we have markings in the right number ...
            foreach (var judge in this.CurrentRound.Competition.GetJudges())
            {
                foreach (var danceInRound in this.CurrentRound.DancesInRound)
                {
                    var marks =
                        this.judgements.Count(j => j.Dance.Id == danceInRound.Id && j.Official.Id == judge.Id && j.Mark);

                    if (marks > this.CurrentRound.MarksTo)
                    {
                        var errorText = string.Format(
                            LocalizationService.Resolve(() => Text.ErrorJudgeToManyMarks),
                            judge.Sign,
                            marks,
                            danceInRound.Dance.DanceName);

                        var result = MainWindow.MainWindowHandle.ShowMessage(
                            errorText,
                            LocalizationService.Resolve(() => Text.WrongNumberOfMarks),
                            MessageDialogStyle.AffirmativeAndNegative);

                        if (result == MessageDialogResult.Affirmative)
                        {
                            return true;
                        }
                        return false;
                    }

                    if (marks < this.CurrentRound.MarksFrom)
                    {
                        var errorText = string.Format(
                            LocalizationService.Resolve(() => Text.ErrorJudgeToLessMarks),
                            judge.Sign,
                            marks,
                            danceInRound.Dance.DanceName);
                        var result = MainWindow.MainWindowHandle.ShowMessage(
                            errorText,
                            LocalizationService.Resolve(() => Text.WrongNumberOfMarks),
                            MessageDialogStyle.AffirmativeAndNegative);

                        if (result == MessageDialogResult.Affirmative)
                        {
                            return true;
                        }

                        return false;
                    }
                }
            }

            return true;
        }

        public override void OnLeft()
        {
            this.judgements.Clear();
        }

        protected override void OnNextInternal()
        {
            // Save Markings to database
            // We only save the given marks
            // delete old marking in database

            MainWindow.MainWindowHandle.ReportStatus("Saving data ...", 0, 0);

            this.context.Database.ExecuteSqlCommand(
                TransactionalBehavior.DoNotEnsureTransaction,
                "Delete from Markings where Round_Id={0} And Mark = 0",
                this.currentRoundId);

            try
            {
                this.context.SaveChanges(); // Save Markings to database
            }
            catch (Exception ex)
            {
                ScrutinusContext.WriteLogEntry(ex);
                MainWindow.MainWindowHandle.ShowMessageAsync(Text.ErrorSavingData);
            }

            var placeOffset = this.GetCurrentRound().PlaceOffset;
            var watch = new Stopwatch();
            watch.Start();

            MainWindow.MainWindowHandle.ReportStatus("Calculating Places ...", 0, 0);
            // Update Sum of Marking to display in list to select qualified couples
            var qualifieds = this.context.Qualifieds.Where(q => q.Round.Id == this.currentRoundId).ToList();
            foreach (var qualified in qualifieds)
            {
                qualified.Sum = this.judgements.Count(j => j.Participant.Id == qualified.Participant.Id && j.Mark);
                qualified.Points = qualified.Sum;
            }

            // Special: Two second first rounds:
            if (this.Competition.FirstRoundType == RoundTypes.SecondFirstRound && this.CurrentRound.Number == 2)
            {
                this.AddMarksFormFirstRound(qualifieds);
            }

            Debug.WriteLine("Updated Sums: {0} ms", watch.ElapsedMilliseconds);

            CalculatePlaces(qualifieds, placeOffset);

            try
            {
                this.context.SaveChanges(); // Save Markings to database
            }
            catch (Exception ex)
            {
                ScrutinusContext.WriteLogEntry(ex);
                MainWindow.MainWindowHandle.ShowMessageAsync(Text.ErrorSavingData);
            }

            MainWindow.MainWindowHandle.ReportStatus("", 0, 0);
        }

        #endregion

        #region Methods

        protected override void SetViewModel()
        {
        }

        protected override void Dispose(bool disposing)
        {
            // remove all event handlers in this.judgements:
            // Prevent memeory leaks ...

            foreach (var judgementViewModel in this.judgements)
            {
                judgementViewModel.PropertyChanged -= this.JudgementPropertyChanged;
                judgementViewModel.Dispose();
            }

            this.judgements.Clear();

            // Clear all bindings:
            foreach (var grid in this.markingGrids)
            {
                foreach (var child in grid.Children)
                {
                    if (child is MarkEditor)
                    {
                        BindingOperations.ClearAllBindings(child as MarkEditor);
                    }
                }
            }

            Messenger.Default.Unregister(this);

            this.markingGrids.Clear();
        }

        private void AddMarksFormFirstRound(IEnumerable<Qualified> qualifieds)
        {
            // find the first round:
            var round =
                this.context.Rounds.SingleOrDefault(r => r.Competition.Id == this.competitionId && r.Number == 1);
            if (round == null)
            {
                MainWindow.MainWindowHandle.ShowMessage(LocalizationService.Resolve(() => Text.ErrorFirstRoundNotFound));
                return;
            }

            foreach (var qualified in qualifieds)
            {
                qualified.Sum +=
                    this.context.Markings.Count(
                        m => m.Participant.Id == qualified.Participant.Id && m.Round.Id == round.Id && m.Mark == 1);
                qualified.Points = qualified.Sum;
            }
        }

        /// <summary>
        ///     Write data exchange for eJudge / mobileControl
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnReadData_OnClick(object sender, RoutedEventArgs e)
        {
            var filename = String.Format(
                "{0}\\{1}",
                Settings.Default.MobileControlExchange,
                this.TxtImportFileName.Text);

            this.ButtonReadData.IsEnabled = false;

            var task = new Task(
                () =>
                    {
                        MainWindow.MainWindowHandle.ReportStatus("Removing existing markings", 0, 100);
                        // We clear any mark before we read them back from disk
                        //this.context.Database.ExecuteSqlCommand(
                        //    TransactionalBehavior.DoNotEnsureTransaction,
                        //    "Delete from Markings where Round_Id={0}",
                        //    this.currentRoundId);

                        //foreach (var j in this.judgements)
                        //{
                        //    j.Mark = false;
                        //}

                        this.context.Markings.RemoveRange(this.Round.Markings);
                        this.Round.Markings.Clear();
                        this.context.SaveChanges();

                        MainWindow.MainWindowHandle.ReportStatus("Reading marking", 0, 100);

                        if (this.Competition.Section.Id == 3)
                        {
                            this.ReadDataFromFile(filename.Replace("##", "L"));
                            this.ReadDataFromFile(filename.Replace("##", "S"));
                        }
                        else
                        {
                            this.ReadDataFromFile(filename);
                        }

                        this.context.SaveChanges();

                        this.Dispatcher.Invoke(new Action(() =>
                        {
                            this.ButtonReadData.IsEnabled = true; 
                            MainWindow.MainWindowHandle.NextState(this.competitionId);
                        }));
                    });
            task.Start();

            task.ContinueWith(t =>
            {
                Dispatcher.Invoke(() =>
                {
                    MainWindow.MainWindowHandle.RemoveStatusReport();
                    if (task.IsFaulted)
                    {
                        foreach (var exception in task.Exception.InnerExceptions)
                        {
                            ScrutinusContext.WriteLogEntry(exception);
                        }

                        string error = string.Join(", ", task.Exception.InnerExceptions.Select(ex => ex.Message));

                        MainWindow.MainWindowHandle.ShowMessage(
                            $"Reading the eJudge data finished with errors. Please check the logfie for details.\r\n{error}");
                    }
                });
            });
        }

        private void BtnWdsfApi_OnClick(object sender, RoutedEventArgs e)
        {
            var wdsfApiClient = new WdsfApiClient(this.context, this.Competition);

            // We have to send the previous round not this one as it contains no data yet
            var roundToSend =
                this.context.Rounds.SingleOrDefault(
                    r => r.Competition.Id == this.competitionId && r.Number == this.CurrentRound.Number - 1);
            if (roundToSend == null)
            {
                return;
            }

            var task = Task.Factory.StartNew(
                () =>
                    {
                        var clearData = roundToSend.Number == 1
                                         || roundToSend.Number == 2
                                         && roundToSend.Competition.Rounds.Any(
                                             r => r.RoundType.Id == RoundTypes.Redance);

                        if (clearData)
                        {
                            wdsfApiClient.ClearOfficials();
                            wdsfApiClient.ClearAllParticipants();
                            foreach (var qualified in roundToSend.Qualifieds)
                            {
                                qualified.Participant.ExternalId = null;
                            }
                        }

                        wdsfApiClient.SendResults(roundToSend);
                    });
            task.ContinueWith(this.CheckTaskError);
        }

        private void BtnWriteData_OnClick(object sender, RoutedEventArgs e)
        {
            if (this.Competition.Section.Id == 3)
            {
                // Write Standard and Latin as seperate rounds
                this.WriteEjudgeData("S", 1, 5, "Standard");
                this.WriteEjudgeData("L", 6, 10, "Latin");
            }
            else
            {
                this.WriteEjudgeData("##", 1, 99, this.Competition.Section.ShortName);
            }
        }

        private void CheckTaskError(Task task)
        {
            if (task.Exception != null)
            {
                var exception = (Exception)task.Exception;
                while (exception.InnerException != null)
                {
                    exception = exception.InnerException;
                }

                MainWindow.MainWindowHandle.ShowMessage(exception.Message);
            }
        }

        private void CreateTabs(object sender)
        {
            var heats = HeatsHelper.GetHeats(
                this.context,
                this.currentRoundId);
            var list = heats[heats.Keys.First()];
            var lines = (list.Count + 2) * heats.Count;
            var columns = heats.Values.Max(v => v.Max(p => p.Count)) + 1;

            var judges =
                this.GetCompetition().Officials.Where(o => o.Role.Id == Roles.Judge).OrderBy(o => o.Official.Sign);

            this.MarkingTab.Items.Clear();
            this.judgements.Clear();

            var task = new Task(
                () => this.Dispatcher.Invoke(
                    new Action(
                          () =>
                              {
                                  // using (var disabledContext = this.Dispatcher.DisableProcessing())
                                  {
                                      var watch = new Stopwatch();
                                      watch.Start();

                                      var index = 0;

                                      foreach (var judge in judges.OrderBy(j => j.Official.Sign))
                                      {
                                          MainWindow.MainWindowHandle.ReportStatus(
                                              "Creating Tabs",
                                              index,
                                              judges.Count());

                                          this.PopulateTab(judge.Official, heats, lines, columns);

                                          index++;
                                      }
                                      Debug.WriteLine("PopulateTab: {0}", watch.ElapsedMilliseconds);

                                      watch.Stop();

                                      this.FillJudgements();

                                      foreach (var qualified in this.Qualifieds)
                                      {
                                          qualified.Points =
                                              this.judgements.Count(
                                                  j => j.Participant.Number == qualified.Participant.Number && j.Mark);
                                      }
                                  }
                              }), DispatcherPriority.Background));
            task.Start();
        }

        private void FillJudgements()
        {
            var marks =
                this.context.Markings.Where(m => m.Round.Id == this.currentRoundId && m.Mark == 1);

            var count = marks.Count();

            Debug.WriteLine("{0} marks found in database", count);
            var watch = new Stopwatch();
            watch.Start();

            var index = 0;

            this.doNotSaveJudgements = true;

            foreach (var marking in marks.Where(m => m.Dance != null))
            {
                index++;
                MainWindow.MainWindowHandle.ReportStatus("Updating data from database", index, count);
                var judgement =
                    this.judgements.SingleOrDefault(
                        j =>
                        j.Dance.Dance.Id == marking.Dance.Id && j.Participant.Id == marking.Participant.Id
                        && j.Official.Id == marking.Judge.Id);

                if (judgement != null)
                {
                    judgement.Mark = marking.Mark == 1;
                }
            }
            watch.Stop();
            this.doNotSaveJudgements = false;
            Debug.WriteLine("FillJudgements took {0} ms", watch.ElapsedMilliseconds);
            MainWindow.MainWindowHandle.ReportStatus("", 0, 0);

            this.RaisePropertyChanged("NumberOfMarks");
        }

        private void OnEditStartbuchClicked(object sender, RoutedEventArgs e)
        {
            var dialog = new ChangeStartbookData(this.CurrentRound, this.context);
            MainWindow.MainWindowHandle.ShowDialog(dialog, null, 300, 300);
        }

        private void OnUnlockButtonClick(object sender, RoutedEventArgs e)
        {
#if DTV
            MainWindow.MainWindowHandle.ViewModel.MarkingInputEnabled = true;
            return;
#endif
            var page = new UnlockMarkingInput();
            MainWindow.MainWindowHandle.ShowDialog(page, null, 200, 200);
        }

        private void PopulateTab(
            Official official,
            Dictionary<DanceInRound, List<List<Participant>>> heats,
            int rows,
            int columns)
        {
            var watch = new Stopwatch();
            watch.Start();

            var marksSumConverter = new MarksSumPerHeatConverter(this.judgements, official, this.CurrentRound);

            var item = new TabItem { Header = official.Sign };
            
            var grid = new Grid();

            this.markingGrids.Add(grid);

            var dockPanel = new DockPanel();

            var officialLabel = new Label { Content = official.NiceName };
            officialLabel.FontSize = 18;
            officialLabel.Margin = new Thickness(0, 10, 0, 10);
            DockPanel.SetDock(officialLabel, Dock.Top);
            dockPanel.Children.Add(officialLabel);

            var scrollviewer = new ScrollViewer { CanContentScroll = true };
            DockPanel.SetDock(scrollviewer, Dock.Top);
            scrollviewer.Content = grid;
            dockPanel.Children.Add(scrollviewer);

            item.Content = dockPanel;

            // Now populate our grid...
            grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(55) });
            for (var i = 1; i < rows + 1; i++)
            {
                grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(45) });
            }
            // First Column is Heat ^number, all others are for markings
            grid.ColumnDefinitions.Add(new ColumnDefinition());
            for (var i = 1; i < columns; i++)
            {
                // Add a column for marking with fixed length
                grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(45) });
            }

            Debug.WriteLine("Populate Tab-1: " + watch.ElapsedMilliseconds);

            // Now add the Marking Controls for this judge
            var line = 0;

            foreach (var dance in heats.Keys)
            {
                // add the Dance line
                var danceLabel = new Label { Content = dance.Dance.DanceName };
                danceLabel.FontSize = 15;
                this.SetCellLabel(grid, danceLabel, 0, line, columns);
                line++;
                var heatsOfDance = heats[dance];
                for (var i = 0; i < heatsOfDance.Count; i++)
                {
                    this.SetCellLabel(
                        grid,
                        new Label
                            {
                                Content =
                                    string.Format(
                                        "{0}. {1}",
                                        i + 1,
                                        LocalizationService.Resolve(() => Text.Heat))
                            },
                        0,
                        line);
                    for (var col = 1; col <= heatsOfDance[i].Count; col++)
                    {
                        var couples = heatsOfDance[i].OrderBy(p => p.Number).ToList();
                        var markeditor = new MarkEditor { Height = 40, Width = 40 };
                        // DataContext for marking
                        var judgement = new JudgementViewModel
                                            {
                                                Dance = dance,
                                                Participant = couples[col - 1],
                                                Official = official,
                                                Mark = false
                                            };
                        this.judgements.Add(judgement);
                        judgement.PropertyChanged += this.JudgementPropertyChanged;
                        // Prüfen, ob wir da schon was inder Datenbank haben
                        markeditor.DataContext = judgement;
                        markeditor.SetBinding(MarkEditor.MarkProperty, new Binding("Mark"));
                        markeditor.SetBinding(MarkEditor.NumberProperty, new Binding("Participant.Number"));
                        markeditor.SetBinding(
                            IsEnabledProperty,
                            new Binding
                                {
                                    Source = MainWindow.MainWindowHandle.ViewModel,
                                    Path = new PropertyPath("MarkingInputEnabled")
                                });

                        this.SetCellElement(grid, markeditor, col, line);
                    }
                    line++;
                }

                // Add a label for the sums:
                var stackpanel = new StackPanel { Orientation = Orientation.Horizontal };
                var sumLabel = new Label { Content = LocalizationService.Resolve(() => Text.NumberOfMarks), FontSize = 16 };
                stackpanel.Children.Add(sumLabel);

                var sumCount = new Label { FontSize = 16 };
                
                var binding = new Binding
                                  {
                                      Source = this,
                                      Path = new PropertyPath("NumberOfMarks"),
                                      Converter = marksSumConverter,
                                      ConverterParameter = dance.Dance.Id,
                                  };

                sumCount.SetBinding(ContentProperty, binding);

                stackpanel.Children.Add(sumCount);

                this.SetCellElement(grid, stackpanel, 0, line);
                Grid.SetColumnSpan(stackpanel, 3);
                line++;

                Debug.WriteLine("Populate Tab-2: " + watch.ElapsedMilliseconds);
            }

            this.MarkingTab.Items.Add(item);

            Debug.WriteLine("Populate Tab-3: " + watch.ElapsedMilliseconds);
            grid.UpdateLayout();

            
        }

        private void QuickAddCouple_OnClick(object sender, RoutedEventArgs e)
        {
            var page = new QuickAddCoupleDialog(
                this.competitionId,
                this.currentRoundId,
                this.Competition.LateEntriesAllowed);

            MainWindow.MainWindowHandle.ShowDialog(page, this.CreateTabs, 600, 600);
        }

        private void ReadDataFromFile(string filename)
        {
            if (!File.Exists(filename))
            {
                MainWindow.MainWindowHandle.ShowMessage(
                    LocalizationService.Resolve(() => Text.FileDoesNotExistsNoDataToRead));
                return;
            }

            using (var inStr = new StreamReader(filename, Encoding.UTF8))
            {
                // We handle saving to db ourself
                this.doNotSaveJudgements = true;
                this.context.Configuration.AutoDetectChangesEnabled = false;

                var danceMinimum = 0;
                var danceMaxium = 10;

                // If 10 dance, we need to set the danceOffset in Latin:
                if (this.Competition.Section.Id == 3)
                {
                    if (filename.Contains("_L_"))
                    {
                        danceMinimum = 6;
                        danceMaxium = 10;
                    }
                    else
                    {
                        danceMinimum = 1;
                        danceMaxium = 5;
                    }
                }

                var lines = File.ReadAllLines(filename);
                var index = 0;

                var judges = this.Competition.GetJudges().ToDictionary(o => o.Sign);

                var stopwatch = new Stopwatch();
                stopwatch.Start();

                var currentRound = this.CurrentRound;
                var qualified = this.CurrentRound.Qualifieds.ToList();

                while (index < lines.Length)
                {
                    Debug.WriteLine("Stopwatch after line {0}: {1}", index, stopwatch.ElapsedMilliseconds);
                    MainWindow.MainWindowHandle.ReportStatus("Reading marking", index, lines.Length);
                    var data = CSVHelper.CSVSplitter(inStr.ReadLine());
                    // -> data[0] is couple number
                    var num = 0;
                    if (!Int32.TryParse(data[0], out num))
                    {
                        index++;
                        continue;
                    }

                    var couple = qualified.SingleOrDefault(c => c.Participant.Number == num);
                    if (couple == null)
                    {
                        var msg = LocalizationService.Resolve(() => Text.CoupleNotFound);
                        MainWindow.MainWindowHandle.ShowMessage(string.Format(msg, num));
                        index++;
                        continue;
                    }
                    if (!Int32.TryParse(data[1], out num))
                    {
                        MainWindow.MainWindowHandle.ShowMessage(
                            LocalizationService.Resolve(() => Text.NumberOfJudgesWrongFormat));
                        index++;
                        continue;
                    }
                    for (var i = 0; i < num; i++)
                    {
                        var stopwatch2 = new Stopwatch();
                        stopwatch2.Start();

                        var sign = data[2 + i * 3];

                        if (!judges.ContainsKey(sign))
                        {
                            MainWindow.MainWindowHandle.ShowMessage(
                                string.Format(LocalizationService.Resolve(() => Text.UnknownJudgeSign), sign));
                            index++;
                            continue;
                        }

                        var judge = judges[sign];

                        var judgments = data[3 + i * 3].Replace("\"", "").Split(';');
                        var dances =
                            currentRound.DancesInRound.Where(
                                d => d.Dance.Id >= danceMinimum && d.Dance.Id <= danceMaxium)
                                .OrderBy(o => o.SortOrder)
                                .ToList();

                        for (var j = 0; j < dances.Count; j++)
                        {
                            var dance = dances[j];

                            var judgement =
                                this.judgements.SingleOrDefault(
                                    jud =>
                                    jud.Official.Sign == judge.Sign
                                    && jud.Participant.Number == couple.Participant.Number
                                    && jud.Dance.Dance == dance.Dance);

                            if (judgement == null)
                            {
                                var msg = LocalizationService.Resolve(() => Text.MismatchInputFile);
                                MainWindow.MainWindowHandle.ShowMessage(
                                    string.Format(msg, judge.Sign, couple.Participant.Number, dance.Dance.ShortName));
                                continue;
                            }

                            if (j < judgments.Length && judgments[j] == "X")
                            {
                                // We have a mark. Lets get the Judgements


                                judgement.Mark = true;
                                // We add a mark to the database
                                var marking = new Marking(couple.Participant, currentRound, dance.Dance, judge)
                                {
                                    Created = DateTime.Now,
                                    HasError = false,
                                    Mark = 1,
                                };

                                this.context.Markings.Add(marking);

                            }
                            else
                            {
                                judgement.Mark = false;
                            }
                        }
                        stopwatch2.Stop();
                        // Debug.WriteLine("Lookup per judge: {0}", stopwatch2.ElapsedMilliseconds);
                    }
                    index++;
                }
                ;
            }
            MainWindow.MainWindowHandle.ReportStatus("Saving data ...", 0, 0);

            this.context.ChangeTracker.DetectChanges();
            this.context.SaveChanges();
            this.context.Configuration.AutoDetectChangesEnabled = true;
            this.doNotSaveJudgements = false;

            MainWindow.MainWindowHandle.ReportStatus("", 0, 0);
        }

        private void RemoveCouple(object sender, RoutedEventArgs e)
        {
            if (this.CurrentRound.Number != 1)
            {
                MainWindow.MainWindowHandle.ShowMessage(
                    LocalizationService.Resolve(() => Text.RemoveCoupleOnlyInFirstRound));
                return;
            }

            var page = new RemoveCouple(new RemoveCoupleViewModel(this.CurrentRound, this.context));
            MainWindow.MainWindowHandle.ShowDialog(page, this.CreateTabs, 120, 100);
        }

        private void SetCellElement(Grid g, FrameworkElement mark, int column, int row, int colSpan = 1)
        {
            Grid.SetColumn(mark, column);
            Grid.SetRow(mark, row);
            Grid.SetColumnSpan(mark, colSpan);

            if (!g.Children.Contains(mark))
            {
                g.Children.Add(mark);
            }

            // g.UpdateLayout();
        }

        private void SetCellLabel(Grid g, Label label, int column, int row, int colSpan = 1)
        {
            Grid.SetColumn(label, column);
            Grid.SetRow(label, row);
            Grid.SetColumnSpan(label, colSpan);

            if (!g.Children.Contains(label))
            {
                g.Children.Add(label);
            }

            // g.UpdateLayout();
        }

        private void WDSGExport_OnClick(object sender, RoutedEventArgs e)
        {
            var task = new Task(
                () =>
                    {
                        // we export the last round
                        var round = this.GetCurrentRound();
                        if (round.Number == 1)
                        {
                            // This is first round, we only export qualified of this round
                            ExportWDSG.ExportOverall(Settings.Default.WDSGExportFolder, round, this.context);
                            return;
                        }
                        var sendRound =
                            this.context.Rounds.SingleOrDefault(
                                r => r.Competition.Id == round.Competition.Id && r.Number == round.Number - 1);

                        ExportWDSG.ExportWDSGFile(this.context, sendRound.Id, Settings.Default.WDSGExportFolder);
                        ExportWDSG.ExportOverall(Settings.Default.WDSGExportFolder, round, this.context);
                    });
            task.Start();
        }

        private void WriteEjudgeData(string suffix, int minDanceId, int maxDanceId, string roundSection)
        {
            if (!Directory.Exists(Settings.Default.MobileControlExchange))
            {
                MainWindow.MainWindowHandle.ShowMessageAsync(string.Format(Text.PathDoesNotExist,
                    Settings.Default.MobileControlExchange));
                return;
            }

            // Write eJudge data to our path
            var filename = string.Format(
                "{2}\\{0:0000}_{1}_{3}_Sl.dat",
                this.Competition.Id,
                this.CurrentRound.Number,
                Settings.Default.MobileControlExchange,
                suffix);

            if (File.Exists(filename))
            {
                // Check to overwrite exitsing data:
                var res =
                    MainWindow.MainWindowHandle.ShowMessage(
                        LocalizationService.Resolve(() => Text.DataAllreadyExistsOverwrite),
                        string.Empty,
                        MessageDialogStyle.AffirmativeAndNegative);

                if (res == MessageDialogResult.Negative)
                {
                    return;
                }
            }

            var outStr = new StreamWriter(filename, false, Encoding.UTF8);

            foreach (var qualified in this.CurrentRound.Qualifieds.OrderBy(q => q.Participant.Number))
            {
                outStr.WriteLine(
                    "{0};{1};{2};{3};{4};{5};{6}",
                    qualified.Participant.Number,
                    qualified.Participant.Couple.FirstMan,
                    qualified.Participant.Couple.LastMan,
                    qualified.Participant.Couple.FirstWoman,
                    qualified.Participant.Couple.LastWoman,
                    qualified.Participant.Couple.Country,
                    "\"\";0;0;0;\"\"");
            }
            outStr.Close();
            filename = String.Format(
                "{2}\\{0:0000}_{1}_{3}_Tanz.dat",
                this.Competition.Id,
                this.CurrentRound.Number,
                Settings.Default.MobileControlExchange,
                suffix);

            outStr = new StreamWriter(filename, false, Encoding.UTF8);

            foreach (var dance in
                this.CurrentRound.DancesInRound.Where(d => d.Dance.Id >= minDanceId && d.Dance.Id <= maxDanceId)
                    .OrderBy(d => d.SortOrder))
            {
                outStr.WriteLine("{0};{1};{1};{1};{1}", dance.Dance.ShortName, dance.Dance.DanceName);
            }
            outStr.Close();

            // Set the flags to control behavior of mobile devices
            var flags = eJudgeSettings.GetEjudgeFlags();

            filename = String.Format(
                "{2}\\{0:0000}_{1}_{3}_Td.dat",
                this.Competition.Id,
                this.CurrentRound.Number,
                Settings.Default.MobileControlExchange,
                suffix);

            outStr = new StreamWriter(filename, false, Encoding.UTF8);

            outStr.WriteLine(
                "{0};{1};{2};{3};\"{4}\";\"{5}\";\"{6}. Round {19}\";{7};{8};{9};{10};{11};{12};\"{13}\";{14};{15};{16};{17};{18}",
                "1",
                "EN",
                this.Competition.AgeGroup.ShortName,
                this.Competition.Event.DateFrom.HasValue
                    ? this.Competition.Event.DateFrom.Value.ToShortDateString()
                    : "\"\"",
                this.Competition.Event.Place,
                this.Competition.Title,
                this.CurrentRound.Number,
                "Z",
                1,
                this.CurrentRound.MarksFrom,
                this.CurrentRound.MarksTo,
                0,
                0,
                "",
                0,
                0,
                2,
                flags,
                "1;1;3;0",
                roundSection);
            outStr.Close();

            filename = String.Format(
                "{2}\\{0:0000}_{1}_{3}_Rl.dat",
                this.Competition.Id,
                this.CurrentRound.Number,
                Settings.Default.MobileControlExchange,
                suffix);

            outStr = new StreamWriter(filename, false, Encoding.UTF8);

            // Write Round Drawings
            foreach (var danceRound in
                this.CurrentRound.DancesInRound.Where(d => d.Dance.Id >= minDanceId && d.Dance.Id <= maxDanceId)
                    .OrderBy(d => d.SortOrder))
            {
                var heats =
                    this.CurrentRound.Drawings.Where(d => d.DanceRound.Dance.Id == danceRound.Dance.Id).Max(m => m.Heat)
                    + 1;
                outStr.Write("{0}", heats);
                for (var i = 0; i < heats; i++)
                {
                    var participants =
                        this.CurrentRound.Drawings.Where(
                            d => d.DanceRound.Dance.Id == danceRound.Dance.Id && d.Heat == i)
                            .OrderBy(o => o.Participant.Number);

                    var heatStr = participants.Aggregate(
                        "",
                        (current, participant) => current + (participant.Participant.Number + ";"));
                    outStr.Write(";" + "\"" + heatStr + "\"");
                }
                outStr.WriteLine("");
            }
            outStr.Close();

            filename = String.Format(
                "{2}\\{0:0000}_{1}_{3}_Stat.dat",
                this.Competition.Id,
                this.CurrentRound.Number,
                Settings.Default.MobileControlExchange,
                suffix);

            outStr = new StreamWriter(filename, false, Encoding.UTF8);

            outStr.WriteLine("Created");
            outStr.Close();

            filename = String.Format(
                "{2}\\{0:0000}_{1}_{3}_Wr.dat",
                this.Competition.Id,
                this.CurrentRound.Number,
                Settings.Default.MobileControlExchange,
                suffix);

            outStr = new StreamWriter(filename, false, Encoding.UTF8);

            var judges = this.Competition.GetJudges();
            foreach (var judge in judges.OrderBy(j => j.Sign))
            {
                outStr.WriteLine(
                    "{0};{1};{2};{3};\"\";\"{4}\";{5};\"\";\"\";\"\";\"\"",
                    judge.Sign,
                    judge.Firstname,
                    judge.Lastname,
                    judge.Club,
                    judge.DeviceName,
                    judge.DefaultLanguage + 1);
            }

            outStr.Close();

            EjudgeDataExists = true;
        }

        private void JudgementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (this.doNotSaveJudgements)
            {
                return;
            }

            var judgement = sender as JudgementViewModel;
            if (judgement == null)
            {
                return;
            }

            var dbJudgement =
                this.context.Markings.SingleOrDefault(
                    m =>
                    m.Round.Id == this.currentRoundId && m.Dance.Id == judgement.Dance.Dance.Id
                    && m.Judge.Id == judgement.Official.Id && m.Participant.Id == judgement.Participant.Id);

            // Save to database
            if (judgement.Mark)
            {
                if (dbJudgement == null)
                {
                    this.context.Markings.Add(
                        new Marking(judgement.Participant, this.CurrentRound, judgement.Dance.Dance, judgement.Official)
                            {
                                Created = DateTime.Now,
                                Mark = 1
                            });
                }
                else
                {
                    dbJudgement.Mark = 1;
                }
            }
            else
            {
                if (dbJudgement != null)
                {
                    this.context.Markings.Remove(dbJudgement);
                }
            }

            this.context.SaveChanges();

            var qualified = this.Qualifieds.Single(q => q.Participant.Number == judgement.Participant.Number);
            qualified.Points = this.judgements.Count(j => j.Participant.Number == qualified.Participant.Number && j.Mark);

            this.RaisePropertyChanged("NumberOfMarks");
        }

        #endregion

        private void OnSendToMyHeats(object sender, RoutedEventArgs e)
        {
            var myHeatsService = new MyHeatsService();

            myHeatsService.UploadCompetitions(new int[] { this.competitionId });

            if (Settings.Default.MyHeatsUploadPlaces)
            {
                myHeatsService.UploadPlacings();
            }
        }
    }
}