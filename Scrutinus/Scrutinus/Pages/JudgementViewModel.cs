﻿// // TPS.net TPS8 Scrutinus
// // JudgementViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.ComponentModel;
using DataModel.Models;

namespace Scrutinus.Pages
{
    public class JudgementViewModel : INotifyPropertyChanged, IDisposable
    {
        #region Fields

        private bool mark;

        #endregion

        #region Public Events

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Public Properties

        public DanceInRound Dance { get; set; }

        public bool Mark
        {
            get
            {
                return this.mark;
            }
            set
            {
                if (this.mark == value)
                {
                    return;
                }
                this.mark = value;
                this.RaiseEvent("Mark");
            }
        }

        public Official Official { get; set; }

        public Participant Participant { get; set; }

        #endregion

        #region Public Methods and Operators

        public void RaiseEvent(string proptery)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(proptery));
            }
        }

        public void Dispose()
        {
            if (this.PropertyChanged == null)
            {
                return;
            }

            var delegates = this.PropertyChanged.GetInvocationList();
            foreach (PropertyChangedEventHandler d in delegates)
            {
                this.PropertyChanged -= d;
            }
        }

        #endregion
    }
}