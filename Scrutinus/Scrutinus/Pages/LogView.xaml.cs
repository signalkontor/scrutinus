﻿// // TPS.net TPS8 Scrutinus
// // LogView.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

namespace Scrutinus.Pages
{
    /// <summary>
    /// Interaction logic for LogView.xaml
    /// </summary>
    public partial class LogView : ScrutinusPage
    {
        public LogView()
        {
            this.InitializeComponent();
        }

        public override bool AllowToLeave()
        {
            return true;
        }

        public override bool CanMoveNext()
        {
            return false;
        }

        protected override void OnNextInternal()
        {
            
        }

        protected override void SetViewModel()
        {
            
        }
    }
}
