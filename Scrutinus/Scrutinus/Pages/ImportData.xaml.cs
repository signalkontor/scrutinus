﻿// // TPS.net TPS8 Scrutinus
// // ImportData.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using DataModel;
using DataModel.Import;
using DataModel.Messages;
using DataModel.Models;
using GalaSoft.MvvmLight.Messaging;
using Scrutinus.Localization;

namespace Scrutinus.Pages
{
    public class InitialCoupleState
    {
        #region Public Properties

        public string State { get; set; }

        public CoupleState StateCode { get; set; }

        #endregion
    }

    /// <summary>
    ///     Interaction logic for ImportData.xaml
    /// </summary>
    public partial class ImportData
    {
        private string selectedFile;

        #region Constructors and Destructors

        public ImportData()
            : base(0, 0)
        {
            this.InitializeComponent();

            if (File.Exists(Settings.Default.LastImportFile))
            {
                this.FileSelector.SelectedFile = Settings.Default.LastImportFile;
            }

            this.DefaultCompetitionList.ItemsSource = this.context.Competitions.ToList();

            var stateItems = this.GetStateCodes();
            this.InitialState.ItemsSource = stateItems;

            this.InitialState.SelectedItem = stateItems.First();
        }

        #endregion

        #region Public Methods and Operators

        public override VerticalAlignment FrameVerticalContentAllignment => VerticalAlignment.Top;

        public string SelectedFile
        {
            get { return this.selectedFile; }
            set
            {
                this.selectedFile = value;
                this.LoadData(value);
            }
        }

        public override bool AllowToLeave()
        {
            return true;
        }

        public override bool CanMoveNext()
        {
            return false;
        }

        protected override void OnNextInternal()
        {
        }

        #endregion

        #region Methods

        protected override void SetViewModel()
        {
        }

        private void BtnCancel_OnClick(object sender, RoutedEventArgs e)
        {
            MainWindow.MainWindowHandle.GotoLastPage();
        }

        private void BtnOK_OnClick(object sender, RoutedEventArgs e)
        {
            if (!(this.ListImport.ItemsSource is List<ImportViewModel>))
            {
                return;
            }

            var initialState = ((InitialCoupleState)this.InitialState.SelectedItem).StateCode;

            var task = new Task(
                () =>
                    {
                        var list = this.ListImport.ItemsSource as List<ImportViewModel>;

                        if (list == null)
                        {
                            return;
                        }

                        var count = 0;

                        foreach (var import in list)
                        {
                            MainWindow.MainWindowHandle.ReportStatus("Importing", count, list.Count);
                            count++;

                            if ((import.State & 2) == 2)
                            {
                                // double check, the couple is new
                                var existing =
                                    this.context.Couples.FirstOrDefault(
                                        c =>
                                            c.FirstMan == import.Couple.FirstMan && c.LastMan == import.Couple.LastMan &&
                                            c.FirstWoman == import.Couple.FirstWoman &&
                                            c.LastWoman == import.Couple.LastWoman);

                                if (existing == null)
                                {
                                    import.Couple.IsReadOnly = false;
                                    this.context.Couples.Add(import.Couple);
                                }
                                else
                                {
                                    import.Couple = existing;
                                }
                            }

                            Participant participant = null;
                            if (import.Competition != null)
                            {
                                participant =
                                    this.context.Participants.FirstOrDefault(
                                        p =>
                                        p.Competition != null && p.Competition.Id == import.Competition.Id
                                        && p.Couple.Id == import.Couple.Id);
                            }

                            if (participant == null && import.Competition != null)
                            {
                                participant = new Participant
                                                  {
                                                      Competition = import.Competition,
                                                      AgeGroup = import.Competition.AgeGroup,
                                                      Class = import.Competition.Class,
                                                      Couple = import.Couple
                                                  };
                                this.context.Participants.Add(participant);
                            }

                            if (participant != null)
                            {
                                participant.Number = import.Number;
                                participant.State = initialState;
                            }

                            import.Couple.WdsfIdMan = import.Couple.MINMan;
                            import.Couple.WdsfIdWoman = import.Couple.MINWoman;

                            this.context.SaveChanges();
                        }

                        foreach (var comp in this.context.Competitions)
                        {
                            Messenger.Default.Send(new StartlistChangesMessage { CompetitionId = comp.Id });
                        }

                        MainWindow.MainWindowHandle.RemoveStatusReport();
                    });
            task.Start();
        }


        private ObservableCollection<InitialCoupleState> GetStateCodes()
        {
            var result = new ObservableCollection<InitialCoupleState>
                             {
                                 new InitialCoupleState
                                     {
                                         State =
                                             LocalizationService
                                             .Resolve(
                                                 () =>
                                                 Text
                                                     .Dancing),
                                         StateCode =
                                             CoupleState
                                             .Dancing
                                     },
                                 new InitialCoupleState
                                     {
                                         State =
                                             LocalizationService
                                             .Resolve(
                                                 () =>
                                                 Text
                                                     .Excused),
                                         StateCode =
                                             CoupleState
                                             .Excused
                                     },
                                 new InitialCoupleState
                                     {
                                         State =
                                             LocalizationService
                                             .Resolve(
                                                 () =>
                                                 Text
                                                     .Missing),
                                         StateCode =
                                             CoupleState
                                             .Missing
                                     }
                             };

            return result;
        }

        private void LoadData(string filename)
        {
            var importer = FileImporterFactory.GetFileImporter(filename, MainWindow.MainWindowHandle);
            if (importer == null)
            {
                MainWindow.MainWindowHandle.ShowMessage(LocalizationService.Resolve(() => Text.UnknownFileExtension));
                return;
            }

            try
            {
                this.ListImport.ItemsSource = importer.ImportFile(
                    this.context,
                    filename,
                    this.DefaultCompetitionList.SelectedItem as Competition);
            }
            catch (Exception ex)
            {
                MainWindow.MainWindowHandle.ShowMessage(ex.Message);
            }
        }

        #endregion
    }
}