﻿// // TPS.net TPS8 Scrutinus
// // ProgramSettings.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using MahApps.Metro.SimpleChildWindow;
using Scrutinus.Dialogs;
using Scrutinus.Dialogs.SettingsDialogs;
using Scrutinus.Localization;

namespace Scrutinus.Pages
{
    public enum ApplicationThemes
    {
        BlackBlue,

        BlackRed,

        WhiteBlue,

        WhiteRed
    }

    /// <summary>
    ///     Interaction logic for ProgramSettings.xaml
    /// </summary>
    public partial class ProgramSettings : ScrutinusPage
    {
        #region Constructors and Destructors

        public ProgramSettings()
            : base(0, 0)
        {
            this.InitializeComponent();

            this.ShowEjudgeSettingsCommand = new RelayCommand(this.ShowEjudgeSettings);
            this.ShowProgramSettingsCommand = new RelayCommand(this.ShowProgramSettings);
            this.ShowDefineCollectedReportsCommand = new RelayCommand(this.ShowDefineCollectedReports);
            this.ShowWinnerCertificateDesignerCommand = new RelayCommand(this.ShowWinnerCertificateDesigner);
            this.ShowDeleteRoundCommand = new RelayCommand(this.ShowDeleteRound);
            this.ShowAssignJudgesCommand = new RelayCommand(this.ShowAssignJudges);
            this.ShowCompetitionStatesCommand = new RelayCommand(this.ShowCompetitionStates);
            this.ShowAboutCommand = new RelayCommand(this.ShowAbout);
            this.ShowNancyControlCommand = new RelayCommand(this.ShowNancyControl, () => MainWindow.License.TabletServer);
            this.ShowImportLegacyTpsCommand = new RelayCommand(this.ShowImportLegacyTps);
            this.ShowEventStateCommand = new RelayCommand(this.ShowEventState);
            this.ShowImportDtvCommand = new RelayCommand(this.ShowImportDtv);
            this.SqlServerWizardCommand = new RelayCommand(this.SqlServerWizard, () => MainWindow.License.SQLServer);
            this.ShowImportFileCommand = new RelayCommand(this.ShowImportFromFile);
            this.ShowEjudgeDevicesCommand = new RelayCommand(this.ShowEjudgeDevices);
            this.ShowPrintManagerCommand = new RelayCommand(this.ShowPrintManager);
            this.ShowLogEntriesCommand = new RelayCommand(this.ShowLogEntries);
            this.ShowProjectorControlCommand = new RelayCommand(this.ShowProjectorControl);
            this.ShowFtsCreateEventCommand = new RelayCommand(this.ShowFtsCreateEvent);
            this.ShowParticipantFinderCommand= new RelayCommand(this.ShowParticipantFinder);
        }

        #endregion

        #region Public Properties

        public string ProgramVersion
        {
            get
            {
                var attributes =
                    Assembly.GetExecutingAssembly()
                        .GetCustomAttributes(false)
                        .Where(t => t is AssemblyFileVersionAttribute)
                        .Cast<AssemblyFileVersionAttribute>();
                if (attributes.Any())
                {
                    return attributes.First().Version;
                }
                return "#1.2.0";
            }
        }

        public ICommand ShowAboutCommand { get; set; }

        public ICommand ShowAssignJudgesCommand { get; set; }

        public ICommand ShowCompetitionStatesCommand { get; set; }

        public ICommand ShowDefineCollectedReportsCommand { get; set; }

        public ICommand ShowDeleteRoundCommand { get; set; }

        public ICommand ShowEjudgeSettingsCommand { get; set; }

        public ICommand ShowEventStateCommand { get; set; }

        public ICommand ShowImportDtvCommand { get; set; }

        public ICommand ShowImportLegacyTpsCommand { get; set; }

        public ICommand ShowNancyControlCommand { get; set; }

        public ICommand ShowEjudgeDevicesCommand { get; set; }

        public ICommand ShowOfficialDbCommand { get; set; }

        public ICommand ShowProgramSettingsCommand { get; set; }

        public ICommand ShowWinnerCertificateDesignerCommand { get; set; }

        public ICommand SqlServerWizardCommand { get; set; }

        public ICommand ShowImportFileCommand { get; set; }

        public ICommand ShowPrintManagerCommand { get; set; }

        public ICommand ShowLogEntriesCommand { get; set; }

        public ICommand ShowProjectorControlCommand { get; set; }

        public ICommand ShowFtsCreateEventCommand { get; set; }

        public ICommand ShowParticipantFinderCommand { get; set; }

        #endregion

        #region Public Methods and Operators

        public override bool AllowToLeave()
        {
            return true;
        }

        public override bool CanMoveNext()
        {
            return false;
        }

        protected override void OnNextInternal()
        {
            // nothing to do
        }

        #endregion

        #region Methods

        protected override void SetViewModel()
        {
        }

        private void ShowAbout()
        {
            var page = new AboutDialog();
            MainWindow.MainWindowHandle.ShowDialog(page, null, 300, 300);
        }

        private void ShowAssignJudges()
        {
            var page = new AssignJudgesToCompetiton(0, 0);
            MainWindow.MainWindowHandle.SetActivePage(page);
        }

        private void ShowParticipantFinder()
        {
            var page = new ParticipantFinder();
            MainWindow.MainWindowHandle.SetActivePage(page);
        }

        private void ShowCompetitionStates()
        {
            var control = new CompetitionStatesControl();
            var childWindow = new ChildWindow() {ShowCloseButton = true, Padding = new Thickness(20), Content = control};
            MainWindow.MainWindowHandle.ShowChildWindow(childWindow, LocalizationService.Resolve(() => Text.TPSStateOfCompetitions));
        }

        private void ShowDefineCollectedReports()
        {
            var dialog = new DefineResultService();
            MainWindow.MainWindowHandle.ShowDialog(dialog, null, 300, 300);
        }

        private void ShowDeleteRound()
        {
            var dialog = new DeleteRoundDialog();
            MainWindow.MainWindowHandle.ShowDialog(dialog, null, 300, 300);
        }

        private void ShowEjudgeSettings()
        {
            var dialog = new eJudgeSettings();
            MainWindow.MainWindowHandle.ShowDialog(dialog, null, 300, 300);
        }

        private void ShowEventState()
        {
            var page = new EventState();
            MainWindow.MainWindowHandle.SetActivePage(page);
        }

        private void ShowImportDtv()
        {
            var page = new ImportDtvCompetition();
            MainWindow.MainWindowHandle.ShowDialog(page, null, 300, 300);
        }

        private void ShowImportLegacyTps()
        {
            var page = new ImportLegacyTpsView();
            MainWindow.MainWindowHandle.ShowDialog(page, null, 300, 300);
        }

        private void ShowNancyControl()
        {
            var page = new NancyControl();
            MainWindow.MainWindowHandle.ShowDialog(page, null, 300, 300);
        }

        private void ShowProgramSettings()
        {
            var dialog = new Dialogs.SettingsDialogs.ProgramSettings();
            MainWindow.MainWindowHandle.ShowDialog(dialog, null, 300, 300);
        }

        private void ShowWinnerCertificateDesigner()
        {
            var dialog = new DesignWinnerCertificate();
            MainWindow.MainWindowHandle.SetActivePage(dialog);
        }

        private void ShowProjectorControl()
        {
            MainWindow.MainWindowHandle.SetActivePage(new ProjectorControl());
        }

        private void ShowEjudgeDevices()
        {
            var page = new EjudgePage();
            MainWindow.MainWindowHandle.SetActivePage(page);
        }

        private void ShowImportFromFile()
        {
            var page = new ImportData();
            MainWindow.MainWindowHandle.SetActivePage(page);
        }

        private void SqlServerWizard()
        {
            var page = new SqlServerWizardDialog();
            MainWindow.MainWindowHandle.ShowDialog(page, null, 300, 300);
        }

        private void ShowPrintManager()
        {
            var dialog = new PrintManager();
            MainWindow.MainWindowHandle.ShowChildWindow(dialog, LocalizationService.Resolve(() => Text.PrintManager));
        }

        private void ShowLogEntries()
        {
            var page = new LogView();
            MainWindow.MainWindowHandle.SetActivePage(page);
        }

        private void ShowFtsCreateEvent()
        {
            var page = new FtsCreateEventDialog();
            MainWindow.MainWindowHandle.ShowChildWindow(page);
        }

        #endregion
    }
}