﻿// // TPS.net TPS8 Scrutinus
// // StartList.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using DataModel;
using DataModel.Models;
using Scrutinus.Dialogs;
using Scrutinus.Navigation;
using Scrutinus.ViewModels;

namespace Scrutinus.Pages
{
    /// <summary>
    ///     Interaction logic for StartList.xaml
    /// </summary>
    public partial class StartList : ScrutinusPage
    {
        #region Constructors and Destructors

        public StartList(int competitionId, int roundId)
            : base(competitionId, roundId)
        {
            this.InitializeComponent();

            this.SetViewModel();
        }

        #endregion

        #region Public Methods and Operators

        public override bool AllowToLeave()
        {
            return true;
        }

        public override bool CanMoveNext()
        {
            return this.ViewModel.Validate();
        }

        protected override void OnNextInternal()
        {
            // Set the actual start time of this competition
            if (!this.Competition.ActualStartTime.HasValue)
            {
                this.Competition.ActualStartTime = DateTime.Now;
            }

            // All couples that are dancing the first round are now been selected 
            var participants =
                this.context.Participants.Where(
                    c => c.Competition.Id == this.competitionId && c.State == CoupleState.Dancing && c.Star == 0);

            var round = this.context.Rounds.SingleOrDefault(r => r.Competition.Id == this.competitionId && r.Number == 1);

            if (round == null)
            {
                round = NavigationHelper.CreateRound(
                    this.context,
                    this.context.RoundTypes.First().Id,
                    1,
                    this.context.Participants.Count(p => p.Competition.Id == this.competitionId && p.Star > 0 && p.State == CoupleState.Dancing),
                    this.Competition.Id,
                    null);
            }
            else
            {
                round.PlaceOffset = this.context.Participants.Count(p => p.Competition.Id == this.competitionId && p.Star > 0 && p.State == CoupleState.Dancing);
                // Delete Qualified for this round:
                foreach (var qualified in this.context.Qualifieds.Where(q => q.Round.Id == round.Id).ToList())
                {
                    this.context.Qualifieds.Remove(qualified);
                }
                this.context.SaveChanges();
            }

            this.context.SaveChanges();

            foreach (var participant in participants)
            {
                participant.QualifiedRound = round;
                participant.PlaceFrom = null;
                participant.PlaceTo = null;

                // We add a qualified Couple for this Round:
                if (!this.context.Qualifieds.Any(q => q.Participant.Id == participant.Id && q.Round.Id == round.Id))
                {
                    this.context.Qualifieds.Add(
                        new Qualified
                            {
                                Participant = participant,
                                Round = round,
                                PlaceFrom = 0,
                                PlaceTo = 0,
                                QualifiedNextRound = false,
                                Sum = 0
                            });
                }
            }
            var competition = this.GetCompetition();
            competition.CurrentRound = round;
            competition.State = CompetitionState.Startlist; // It should be anyway ...

            if (MainWindow.MainWindowHandle.ViewModel.CurrentCompetition != null)
            {
                MainWindow.MainWindowHandle.ViewModel.CurrentCompetition.CurrentRound = round;
            }

            this.context.SaveChanges();
        }

        #endregion

        #region Methods

        protected override void SetViewModel()
        {
            this.ViewModel = new StartListViewModel(this.competitionId, this.currentRoundId);
            this.DataContext = this.ViewModel;
        }

        private void BtnImportWDSF_OnClick(object sender, RoutedEventArgs e)
        {
            var importDlg = new ImportFromWDSF(this.context, this.Competition);

            MainWindow.MainWindowHandle.ShowDialog(importDlg, this.ImportWDSFClosed, 400, 400);
        }

        private void ImportWDSFClosed(object sender)
        {
            var model = this.ViewModel as StartListViewModel;
            model.SyncCommand.Execute(null);
        }

        private void StartListGrid_OnBeginningEdit(object sender, DataGridBeginningEditEventArgs e)
        {
            e.Cancel = true;
        }

        #endregion
    }
}