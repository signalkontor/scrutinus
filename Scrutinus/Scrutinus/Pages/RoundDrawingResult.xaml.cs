﻿// // TPS.net TPS8 Scrutinus
// // RoundDrawingResult.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Linq;
using System.Windows;
using System.Windows.Controls;
using DataModel.ModelHelper;
using Scrutinus.Localization;

namespace Scrutinus.Pages
{
    /// <summary>
    ///     Interaction logic for RoundDrawingResult.xaml
    /// </summary>
    public partial class RoundDrawingResult : ScrutinusPage
    {
        #region Constructors and Destructors

        public RoundDrawingResult(int competitionId, int roundId)
            : base(competitionId, roundId)
        {
            this.InitializeComponent();

            var heats = HeatsHelper.GetHeats(
                this.context,
                this.currentRoundId);

            // We need to know how many rows and columns we need:
            var list = heats[heats.Keys.First()];
            var lines = (list.Count + 1) * heats.Count;
            var columns = heats.Values.Max(h => h.Max(l => l.Count)) + 1;
            // Add Heat and Column Definitions
            for (var i = 0; i < lines; i++)
            {
                this.Drawing.RowDefinitions.Add(new RowDefinition());
            }
            for (var i = 0; i < columns; i++)
            {
                this.Drawing.ColumnDefinitions.Add(new ColumnDefinition());
            }
            // Now show our Drawing:
            var line = 0;
            foreach (var dance in heats.Keys)
            {
                // add the Dance line
                var danceLabel = new Label { Content = dance.Dance.DanceName };
                danceLabel.FontSize = 15;
                this.SetCellLabel(this.Drawing, danceLabel, 0, line, columns);
                line++;
                var heatsOfDance = heats[dance];
                for (var i = 0; i < heatsOfDance.Count; i++)
                {
                    this.SetCellLabel(
                        this.Drawing,
                        new Label { Content = i + 1 + ". " + LocalizationService.Resolve(() => Text.Heat) },
                        0,
                        line);
                    for (var col = 1; col <= heatsOfDance[i].Count; col++)
                    {
                        this.SetCellLabel(
                            this.Drawing,
                            new Label { Content = heatsOfDance[i][col - 1].Number.ToString() },
                            col,
                            line);
                    }
                    line++;
                }
            }
        }

        #endregion

        public override VerticalAlignment FrameVerticalContentAllignment => VerticalAlignment.Top;

        #region Public Methods and Operators

        public override bool AllowToLeave()
        {
            return true;
        }

        public override bool CanMoveNext()
        {
            return true;
        }

        protected override void OnNextInternal()
        {
            // nothing to do here
        }

        #endregion

        #region Methods

        protected override void SetViewModel()
        {
        }

        private void SetCellLabel(Grid g, Label label, int column, int row, int colSpan = 1)
        {
            Grid.SetColumn(label, column);
            Grid.SetRow(label, row);
            Grid.SetColumnSpan(label, colSpan);

            if (!g.Children.Contains(label))
            {
                g.Children.Add(label);
            }

            g.UpdateLayout();
        }

        #endregion

        private void ForwardButtonClicked(object sender, RoutedEventArgs e)
        {
            MainWindow.MainWindowHandle.NextState(this.competitionId);
        }
    }
}