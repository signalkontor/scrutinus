﻿// // TPS.net TPS8 Scrutinus
// // EditOfficials.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using DataModel.Models;
using GalaSoft.MvvmLight.Messaging;
using Scrutinus.ExternalApi;
using Scrutinus.ViewModels;

namespace Scrutinus.Pages
{
    /// <summary>
    ///     Interaction logic for EditOfficials.xaml
    /// </summary>
    public partial class EditOfficials : ScrutinusPage
    {

        #region Fields

        private readonly EditOfficialsViewModel viewModel;

        /// <summary>
        ///     The do not sync.
        /// </summary>


        #endregion


        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="EditOfficials" /> class.
        /// </summary>
        /// <param name="competitionId">
        ///     The competition id.
        /// </param>
        /// <param name="roundId">
        ///     The round id.
        /// </param>
        public EditOfficials(int competitionId, int roundId)
            : base(competitionId, roundId)
        {
            this.InitializeComponent();

            this.viewModel = this.DataContext as EditOfficialsViewModel;

            this.viewModel.SelectedRoles = this.RoleList.SelectedItems;

            Messenger.Default.Register<NewOfficialAddedMessage>(this, this.NewOfficialAdded);

#if DTV
    // We remove the last column as this is the WDSF API State ...
            this.OfficialsGrid.Columns.RemoveAt(this.OfficialsGrid.Columns.Count - 1);
#endif
        }

        #endregion

        #region Public Properties

        public ObservableCollection<Official> Officials { get; set; }

        #endregion

        #region Fields

       

        /// <summary>
        ///     The do not sync.
        /// </summary>
        private bool doNotSync;

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The allow to leave.
        /// </summary>
        /// <returns>
        ///     The <see cref="bool" />.
        /// </returns>
        public override bool AllowToLeave()
        {
            this.viewModel.SaveCommand.Execute(null);

            return true;
        }

        /// <summary>
        ///     The can move next.
        /// </summary>
        /// <returns>
        ///     The <see cref="bool" />.
        /// </returns>
        public override bool CanMoveNext()
        {
            return false;
        }

        /// <summary>
        ///     The on next.
        /// </summary>
        protected override void OnNextInternal()
        {
        }

        #endregion

        #region Methods

        /// <summary>
        ///     The get view model.
        /// </summary>
        /// <returns>
        ///     The <see cref="BaseViewModel" />.
        /// </returns>
        protected override void SetViewModel()
        {
        }

        private void NewOfficialAdded(NewOfficialAddedMessage obj)
        {
            this.OfficialSign.Focus();
        }


        private void RegisterAtWdsf(object sender, RoutedEventArgs e)
        {
            var client = new WdsfApiClient(this.context, null);

            var task = new Task(
                () =>
                    {
                        try
                        {
                            client.CheckOfficials(this.Officials);
                            this.context.SaveChanges();
                        }
                        catch (Exception ex)
                        {
                            var message = ex.Message
                                             + (ex.InnerException != null ? " : " + ex.InnerException.Message : "");
                            MainWindow.MainWindowHandle.ShowMessage("WDSF API failed: " + message);
                        }
                    });
            task.Start();
        }

        /// <summary>
        ///     The role list_ selection changed.
        /// </summary>
        /// <param name="sender">
        ///     The sender.
        /// </param>
        /// <param name="e">
        ///     The e.
        /// </param>
        private void RoleListSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!(this.OfficialsGrid.SelectedItem is Official) || this.doNotSync)
            {
                return;
            }

            var official = (Official)this.OfficialsGrid.SelectedItem;

            foreach (var removedItem in e.RemovedItems)
            {
                if (!(removedItem is Role))
                {
                    continue;
                }

                var role = (Role)removedItem;
                official.Roles.Remove(role);
            }

            foreach (var addedItem in e.AddedItems)
            {
                if (!(addedItem is Role))
                {
                    continue;
                }

                var role = (Role)addedItem;
                official.Roles.Add(role);
            }
        }


        #endregion
    }
}