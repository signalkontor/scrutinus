﻿// // TPS.net TPS8 Scrutinus
// // FinalMarking.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using DataModel;
using DataModel.Models;
using DtvEsvModule;
using GalaSoft.MvvmLight.Messaging;
using General;
using General.Rules.PointCalculation;
using Helpers.Extensions;
using Helpers.Skating;
using mobileControl.Helper;
using MahApps.Metro.Controls.Dialogs;
using MobileControlLib.EjudgeMessages;
using Scrutinus.Annotations;
using Scrutinus.Converters;
using Scrutinus.Dialogs;
using Scrutinus.Dialogs.SettingsDialogs;
using Scrutinus.ExternalApi;
using Scrutinus.Localization;
using Scrutinus.MyHeats;

namespace Scrutinus.Pages
{
    public class ErrorToBackgroundConverter : IValueConverter
    {
        #region Public Methods and Operators

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool)
            {
                var b = (bool)value;
                if (b)
                {
                    return Brushes.IndianRed;
                }
                return Brushes.White;
            }

            return Brushes.White;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }

        #endregion
    }

    public class IntegerConverter : IValueConverter
    {
        #region Public Methods and Operators

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var minValue = 0;
            var maxValue = 10;

            if (parameter is int)
            {
                maxValue = (int)parameter;
            }

            if (value is int?)
            {
                var ival = value as int?;
                if (ival > maxValue)
                {
                    ival = 0;
                }

                return ival <= minValue ? "" : ival.ToString();
            }

            if (value is string)
            {
                var ival = 0;
                if (Int32.TryParse(value as string, out ival))
                {
                    return ival.ToString();
                }
            }

            return "";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is string))
            {
                return 0;
            }

            var s = value as string;
            var i = 0;
            if (!Int32.TryParse(s, out i))
            {
                return 0;
            }
            return i;
        }

        #endregion
    }

    /// <summary>
    ///     Interaction logic for FinalMarking.xaml
    /// </summary>
    public partial class FinalMarking : ScrutinusPage, INotifyPropertyChanged
    {
        #region Fields

        private SkatingViewModel skatingViewModel;

        private List<Grid> grids;

        private bool readingResultFile = false;

        private readonly List<Marking> allMarksList = new List<Marking>();

        private bool ejudgeDataExists;

        private ProgressDialogController progressControler;

        #endregion

        #region Constructors and Destructors

        public FinalMarking()
            : base(0, 0)
        {
        }

        public FinalMarking(int competitionId, int roundId)
            : base(competitionId, roundId)
        {
            this.InitializeComponent();

            Messenger.Default.Register<MarkMessage>(this, this.OnMarkMessage);
            Messenger.Default.Register<JudgeLogonMessage>(this, this.OnJudgeLogon);
            Messenger.Default.Register<EjudgeMessage>(this, this.OnEjudgeMessage);

            // Write eJudge data to our path
            var filename = string.Format(
                "{2}\\{0:0000}_{1}_{3}_Sl.dat",
                this.Competition.Id,
                this.CurrentRound.Number,
                Settings.Default.MobileControlExchange,
                "##");

            this.EjudgeDataExists = File.Exists(filename);

            if (!EjudgeDataExists && Directory.Exists(Settings.Default.MobileControlExchange))
            {
                try
                {
                    this.BtneJudgeWrite_Click(null, null);
                }
                catch (Exception ex) { }
            }

            if (Settings.Default.MyHeatsUploadAutomatically)
            {
                Task.Factory.StartNew(() =>
                {
                    this.OnSendToMyHeats(null, null);
                });
            }
        }

        #endregion

        #region Public Properties

        public Round Round
        {
            get
            {
                return this.CurrentRound;
            }
        }

        public SkatingViewModel SkatingViewModel
        {
            get
            {
                return this.skatingViewModel;
            }
            set
            {
                this.skatingViewModel = value;
                this.OnPropertyChanged();
            }
        }


        public bool EjudgeDataExists
        {
            get
            {
                return ejudgeDataExists;
            }
            set
            {
                this.ejudgeDataExists = value;
                this.OnPropertyChanged(nameof(EjudgeDataExists));
            }
        }

        public ScrutinusContext Context
        {
            get
            {
                return this.context;
            }
        }

        #endregion

        #region Public Methods and Operators

        public override bool AllowToLeave()
        {
            return true;
        }

        public override bool CanMoveNext()
        {
            return this.CheckAllMarkingsValid();
        }

        protected override void OnNextInternal()
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            this.context.SaveChanges();

            this.context = new ScrutinusContext();
            var round = this.context.Rounds.Single(r => r.Id == this.currentRoundId);

            // disable ejudge as we are ready with this competition
            
            // Lets recalculate the skating model on what we have:
            var recalculated = SkatingHelper.CreateViewModel(this.context, round);

            this.context.SaveChanges();
            Debug.WriteLine("Recalculated: " + stopwatch.ElapsedMilliseconds);
            // Set all qualified to Droped Out

            foreach (var qualified in round.Qualifieds)
            {
                qualified.Participant.State = CoupleState.DroppedOut;

                var participant = recalculated.Participants.Single(p => p.Id == qualified.Participant.Id);
                qualified.Participant.PlaceFrom = participant.PlaceFrom;
                qualified.Participant.PlaceTo = participant.PlaceTo;
                qualified.PlaceFrom = participant.PlaceFrom;
                qualified.PlaceTo = participant.PlaceTo;

                if (qualified.Participant.PlaceTo == null)
                {
                    qualified.Participant.PlaceTo = qualified.Participant.PlaceFrom;
                }
            }
            
            // Calculate all points for all participants
            var pointCalculator = PointCalculatorFactory.GetCalculator(round.Competition);
            pointCalculator.CalculateRankingPoints(round.Competition.Participants, this.context);

            if (round.Competition.Participants.Any(p => p.ClimbUp))
            {
                foreach (var participant in round.Competition.Participants.Where(p => p.ClimbUp))
                {
                    var otherStarts =
                        participant.Couple.Participants.Where(
                            p =>
                            p.Competition.Section.Id == this.Competition.Section.Id && p.Id != participant.Id
                            && p.State == CoupleState.Dancing);
                    // Check, if this participant has other competitions
                    if (otherStarts.Any())
                    {
                        foreach (var otherStart in otherStarts)
                        {
                            otherStart.State = CoupleState.Excused;
                        }
                    }

                    MainWindow.MainWindowHandle.ShowMessage(
                        "Es gibt Aufsteiger in diesem Turnier! Doppelstarter wurden auf Entschuldigt gesetzt.");
                }
            }
            Debug.WriteLine("Climbup: " + stopwatch.ElapsedMilliseconds);

            if (this.Competition.ParentCompetition != null)
            {
                foreach (var qualified in round.Qualifieds)
                {
                    var participant = round.Competition.ParentCompetition.Participants.FirstOrDefault(p => p.Number == qualified.Participant.Number);
                    if (participant != null)
                    {
                        participant.PlaceFrom = qualified.PlaceFrom + this.Competition.PlaceOffset;
                        participant.PlaceTo = qualified.PlaceTo.HasValue
                                                  ? qualified.PlaceTo + this.Competition.PlaceOffset
                                                  : qualified.PlaceFrom + this.Competition.PlaceOffset;

                    }
                }
            }

            this.context.SaveChanges();

            // Remove Notify Changed Handler:
            // so we can get rid of this object ...
            foreach (var marking in this.allMarksList)
            {
                marking.PropertyChanged -= this.MarkPropertyChanged;
            }

            if (this.CurrentRound.EjudgeEnabled)
            {
                this.CurrentRound.EjudgeEnabled = false;
                ScrutinusCommunicationHandler.ClearDevices(this.CurrentRound);
                this.context.SaveChanges();
            }

            Debug.WriteLine("Done: " + stopwatch.ElapsedMilliseconds);
#if DTV
            if (this.Competition.Event.RuleName == "DTV National")
            {

                var progressControllerThread = MainWindow.MainWindowHandle.ShowProgressAsync(
                    "DTV Daten",
                    "Generiere DTV Datensatz",
                    false);

                Task.Factory.StartNew(
                    new Action( () => {
                        try
                        {
                            var dbContext = new ScrutinusContext();

                            var competition = dbContext.Competitions.FirstOrDefault(c => c.Id == round.Competition.Id);
                            if (competition == null)
                            {
                                throw new Exception("Das Turnier wurde nicht gefunden");
                            }

                            var apiImporter = new DtvApiImporter("", "", "", dbContext);
                            Debug.WriteLine("new apiImporter: " + stopwatch.ElapsedMilliseconds);
                            var oldChecksum = competition.DtvChecksum;
                            apiImporter.CalculateChecksum(competition);
                            Debug.WriteLine("Calculate Checksum: " + stopwatch.ElapsedMilliseconds);

                            this.Dispatcher.Invoke(new Action(() => { dbContext.SaveChanges(); }));

                            while (!progressControllerThread.IsCompleted)
                            {
                                Thread.Sleep(500);
                            }

                            var closeThread = progressControllerThread.Result.CloseAsync();

                            if (oldChecksum != null && oldChecksum != competition.DtvChecksum)
                            {
                                while (!closeThread.IsCompleted)
                                {
                                    Thread.Sleep(100);
                                }

                                MainWindow.MainWindowHandle.ShowMessage(
                                    "Die DTV Checksumme für dieses Turnier hat sich geändert. Falls bereits Checksummen für die Funktionäre gedruckt wurden, müssen diese nun neu gedruckt werden!");
                            }
                        }
                        catch (Exception ex)
                        {
                            while (!progressControllerThread.IsCompleted)
                            {
                                Thread.Sleep(500);
                            }

                            if (progressControllerThread.Result.IsOpen)
                            {
                                progressControllerThread.Result.CloseAsync();
                            }

                            MainWindow.MainWindowHandle.ShowMessage(
                                "Leider ist bei der Generierung der DTV Daten ein Fehler aufgetreten: " + ex.Message);
                        }
                    }));

                

               
            }
            Debug.WriteLine("Checksum: " + stopwatch.ElapsedMilliseconds);
#endif
            
        }

        #endregion

        #region Methods

        protected override void SetViewModel()
        {
            var stopwatch = new Stopwatch();
            var task = new Task(
                () =>
                {
                    
                    Debug.WriteLine("Step 1: " + stopwatch.ElapsedMilliseconds);
                    this.Dispatcher.Invoke(DispatcherPriority.Background, new Action(() =>
                    {
                        this.CreateViewModel();
                        Debug.WriteLine("Step 2: " + stopwatch.ElapsedMilliseconds);
                        this.OnPropertyChanged(nameof(this.SkatingViewModel));
                        Debug.WriteLine("Step 3: " + stopwatch.ElapsedMilliseconds);
                        this.DataContext = this.skatingViewModel;
                        Debug.WriteLine("Step 4: " + stopwatch.ElapsedMilliseconds);
                        this.CreateGrids();
                        Debug.WriteLine("Step 5: " + stopwatch.ElapsedMilliseconds);
                    }));
                });
            task.ContinueWith(this.ViewCreated);
            task.Start();
        }

        private void ViewCreated(Task task)
        {
            if (task.IsFaulted)
            {
                ScrutinusContext.WriteLogEntry(task.Exception);
            }
        }

        protected override void Dispose(bool disposing)
        {
            Messenger.Default.Unregister(this);
        }

        private void BtnWdsfApi_OnClick(object sender, RoutedEventArgs e)
        {
            var wdsfApiClient = new WdsfApiClient(this.context, this.Competition);

            // Find the last round befor our final:
            var round = this.context.Rounds.FirstOrDefault(r => r.Number == this.CurrentRound.Number - 1 
                                                                && r.Competition.Id == this.CurrentRound.Competition.Id);

            if (round != null)
            {
                wdsfApiClient.SendResults(round);
            }
            
        }

        private void OnSendToMyHeats(object sender, RoutedEventArgs e)
        {
            var myHeatsService = new MyHeatsService();

            myHeatsService.UploadCompetitions(new int[] { this.competitionId });

            if (Settings.Default.MyHeatsUploadPlaces)
            {
                myHeatsService.UploadPlacings();
            }
        }

        private void BtneJudgeGet_Click(object sender, RoutedEventArgs e)
        {
            var filename = string.Format(
                "{2}\\{0:0000}_{1}_##_Wt.dat",
                this.Competition.Id,
                this.CurrentRound.Number,
                Settings.Default.MobileControlExchange);

            if (!File.Exists(filename) && this.Competition.Section.Id != 3)
            {
                MainWindow.MainWindowHandle.ShowMessage(LocalizationService.Resolve(() => Text.NoDataToRead));
                return;
            }

            var model = (SkatingViewModel)this.DataContext;

            foreach (var grid in this.grids)
            {
                grid.Visibility = Visibility.Hidden;
            }

            var task = new Task(
                () =>
                    {
                        progressControler = MainWindow.MainWindowHandle.OpenProgressBar("eJudge", "reading eJudge data...");
                        // To be sure to have clean data:
                        // this.RemoveAllZeroMarks();
                        this.readingResultFile = true;

                        if (this.Competition.Section.Id == 3)
                        {
                            filename = string.Format(
                                "{2}\\{0:0000}_{1}_##_Wt.dat",
                                this.Competition.Id,
                                this.CurrentRound.Number  + "L",
                                Settings.Default.MobileControlExchange);

                            if (File.Exists(filename))
                            {
                                this.ReadMarking(filename, model, 2);
                            }

                            filename = string.Format(
                                "{2}\\{0:0000}_{1}_##_Wt.dat",
                                this.Competition.Id,
                                this.CurrentRound.Number + "S",
                                Settings.Default.MobileControlExchange);

                            if (File.Exists(filename))
                            {
                                this.ReadMarking(filename, model, 1);
                            }

                        }
                        else
                        {
                            this.ReadMarking(filename, model, 0);
                        }

                        this.readingResultFile = false;

                        this.Dispatcher.Invoke(
                            new Action(
                                () =>
                                    {
                                        this.MarkPropertyChanged(model.DanceSkating.First().CoupleResults.First(), new PropertyChangedEventArgs("Marking"));
                                    }));

                        this.Dispatcher.Invoke(
                            new Action(
                                () =>
                                {
                                    foreach (var grid in this.grids)
                                    {
                                        grid.Visibility = Visibility.Visible;
                                    }

                                    progressControler.CloseAsync().WaitWithoutBlockingUI(this.Dispatcher);
                                    progressControler = null;

                                    MainWindow.MainWindowHandle.NextState(this.competitionId);

                                }), DispatcherPriority.ApplicationIdle);

                    });

            task.Start();

            task.ContinueWith((t) => {
                Dispatcher.Invoke(() =>
                {
                    progressControler?.CloseAsync().WaitWithoutBlockingUI(this.Dispatcher);

                    if (task.IsFaulted && task.Exception != null)
                    {
                        foreach (var exception in task.Exception.InnerExceptions)
                        {
                            ScrutinusContext.WriteLogEntry(exception);
                        }

                        string error = string.Join(", ", task.Exception.InnerExceptions.Select(ex => ex.Message));

                        MainWindow.MainWindowHandle.ShowMessage(
                            $"Reading the eJudge data finished with errors. Please check the logfie for details.\r\n{error}");
                    }
                });
            });
        }


        private void BtneJudgeWrite_Click(object sender, RoutedEventArgs e)
        {
            if (this.Competition.Section.Id == 3)
            {
                this.WriteEjudgeData(this.CurrentRound.Number + "S", 1, 5);
                this.WriteEjudgeData(this.CurrentRound.Number + "L", 6, 10);
            }
            else
            {
                this.WriteEjudgeData(this.CurrentRound.Number.ToString(), 1, 10);
            }

            this.EjudgeDataExists = true;
        }

        private void WriteEjudgeData(string prefix, int danceFrom, int danceTo)
        {
            // Write Data for eJudge ...
            // Write eJudge data to our path
            var filename = string.Format(
                "{2}\\{0:0000}_{1}_##_Sl.dat",
                this.Competition.Id,
                prefix,
                Settings.Default.MobileControlExchange);

            if (File.Exists(filename))
            {
                // Check to overwrite exitsing data:
                var res =
                    MainWindow.MainWindowHandle.ShowMessage(
                        LocalizationService.Resolve(() => Text.DataAllreadyExistsOverwrite), "Overwrite", MessageDialogStyle.AffirmativeAndNegative);

                if (res == MessageDialogResult.Negative)
                {
                    return;
                }
            }

            var outStr = new StreamWriter(filename, false, Encoding.UTF8);

            foreach (var qualified in this.CurrentRound.Qualifieds.OrderBy(o => o.Participant.Number))
            {
                outStr.WriteLine(
                    "{0};{1};{2};{3};{4};{5};{6}",
                    qualified.Participant.Number,
                    qualified.Participant.Couple.FirstMan,
                    qualified.Participant.Couple.LastMan,
                    qualified.Participant.Couple.FirstWoman,
                    qualified.Participant.Couple.LastWoman,
                    qualified.Participant.Couple.Country,
                    "\"\";0;0;0;\"\"");
            }
            outStr.Close();

            filename = string.Format(
                "{2}\\{0:0000}_{1}_##_Tanz.dat",
                this.Competition.Id,
                prefix,
                Settings.Default.MobileControlExchange);

            outStr = new StreamWriter(filename);

            foreach (var dance in this.CurrentRound.DancesInRound.Where(d => d.Dance.Id >= danceFrom && d.Dance.Id <= danceTo))
            {
                outStr.WriteLine("{0};{1};{1};{1};{1}", dance.Dance.ShortName, dance.Dance.DanceName);
            }
            outStr.Close();

            filename = string.Format(
                "{2}\\{0:0000}_{1}_##_Td.dat",
                this.Competition.Id,
                prefix,
                Settings.Default.MobileControlExchange);
            outStr = new StreamWriter(filename, false, Encoding.UTF8);

            var flags = eJudgeSettings.GetEjudgeFlags();

            outStr.WriteLine(
                "{0};{1};{2};{3};\"{4}\";\"{5}\";\"{6}. Round\";{7};{8};{9};{10};{11};{12};\"{13}\";{14};{15};{16};{17};{18}",
                "2",
                "EN",
                this.Competition.AgeGroup.ShortName,
                this.Competition.Event.DateFrom.HasValue
                    ? this.Competition.Event.DateFrom.Value.ToShortDateString()
                    : "\"\"",
                this.Competition.Event.Place,
                this.Competition.Title,
                this.CurrentRound.Number,
                "Z",
                1,
                0,
                0,
                0,
                0,
                "",
                0,
                0,
                2,
                flags,
                "1;1;3;0");
            outStr.Close();

            filename = string.Format(
                "{2}\\{0:0000}_{1}_##_Stat.dat",
                this.Competition.Id,
                prefix,
                Settings.Default.MobileControlExchange);
            outStr = new StreamWriter(filename, false, Encoding.UTF8);
            outStr.WriteLine("Created");
            outStr.Close();

            filename = string.Format(
                "{2}\\{0:0000}_{1}_##_Wr.dat",
                this.Competition.Id,
                prefix,
                Settings.Default.MobileControlExchange);
            outStr = new StreamWriter(filename, false, Encoding.UTF8);

            var judges = this.Competition.GetJudges();
            foreach (var judge in judges.OrderBy(j => j.Sign))
            {
                outStr.WriteLine(
                    "{0};{1};{2};{3};\"\";\"{4}\";{5};\"\";\"\";\"\";\"\"",
                    judge.Sign,
                    judge.Firstname,
                    judge.Lastname,
                    judge.Club,
                    judge.DeviceName,
                    judge.DefaultLanguage + 1);
            }
            outStr.Close();
        }

        private void RemoveAllZeroMarks()
        {
            var zeroMarks =
                    this.context.Markings.Where(
                        m =>
                        m.Round.Id == this.currentRoundId && m.Mark == 0)
                        .ToList();

            this.context.Markings.RemoveRange(zeroMarks);
            this.context.SaveChanges();
        }

        private bool CheckAllMarkingsValid()
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            this.context.SaveChanges();

            if (this.context.Markings.Any(m => m.Round.Id == this.currentRoundId && m.Mark == 0))
            {
                MainWindow.MainWindowHandle.ShowMessage(LocalizationService.Resolve(() => Text.NotAllPlacesSet));

                return false;
            }
            

            var requiredMarks = this.CurrentRound.Qualifieds.Count * this.CurrentRound.Competition.GetJudges().Count
                                * this.CurrentRound.DancesInRound.Count;

            if (this.context.Markings.Count(m => m.Round.Id == this.currentRoundId) < requiredMarks)
            {
                MainWindow.MainWindowHandle.ShowMessage(LocalizationService.Resolve(() => Text.NotAllPlacesSet));
                return false;
            }

            // Check. each judge and dance gave place only once
            var message = "";

            var grouped = this.context.Markings.Where(m => m.Round.Id == this.currentRoundId).GroupBy(m => new { Mark = m.Mark, Judge = m.Judge, Dance = m.Dance });

            if (grouped.Any(g => g.Count() > 1))
            {
                var group = grouped.First(g => g.Count() > 1);

                message += string.Format(
                    LocalizationService.Resolve(() => Text.DoubledPlaces) + "\r\n",
                    group.Key.Dance.ShortName,
                    group.Key.Judge.Sign);
            }


            if (!string.IsNullOrEmpty(message))
            {
                message = message + "\r\n\r\n" + LocalizationService.Resolve(() => Text.DoubledPlacesByIntention);

                var result = MainWindow.MainWindowHandle.ShowMessage(
                    message,
                    LocalizationService.Resolve(() => Text.DoubledPlacesShort),
                    MessageDialogStyle.AffirmativeAndNegative);

                if (result == MessageDialogResult.Negative)
                {
                    return false;
                }
            }

            Debug.WriteLine("CheckAllMarkingsValid " + stopwatch.ElapsedMilliseconds);

            return true;
        }

        private void CreateGrids()
        {

            var participantCount = this.skatingViewModel.Participants.Count;
            var judgesCount = this.skatingViewModel.Judges.Count;

            var stopwatch = new Stopwatch();
            stopwatch.Start();

            var model = this.skatingViewModel;

            var numRows = model.Participants.Count + 1;
            var numColumns = judgesCount + participantCount + 3; // Couple Number, Place and Total-Sum

            this.grids = new List<Grid>();

            var round = this.GetCurrentRound();
            var judges = this.GetCompetition().GetJudges().OrderBy(j => j.Sign);
            var qualifieds = round.Qualifieds.OrderBy(q => q.Participant.Number).ToList();

            var danceCount = -1;

            foreach (var dance in round.DancesInRound.OrderBy(d => d.SortOrder))
            {
                danceCount++;
                this.MarkingPanel.Children.Add(new Label { FontSize = 15, Content = dance.Dance.DanceName, Margin = new Thickness(0,30,0,0)});
                var grid = new Grid();
                this.grids.Add(grid);

                this.MarkingPanel.Children.Add(grid);
                // Add Rows and Columns
                for (var i = 0; i < numColumns; i++)
                {
                    grid.ColumnDefinitions.Add(new ColumnDefinition());
                }
                for (var i = 0; i < numRows; i++)
                {
                    grid.RowDefinitions.Add(new RowDefinition());
                }
                // Add Fields
                var row = 0;
                var col = 0;

                foreach (var judge in judges)
                {
                    col++;
                    // Headline: Sign of Judge
                    this.SetCellLabel(
                        grid,
                        new Label
                            {
                                Content = judge.Sign,
                                FontSize = 15,
                                Foreground = (Brush) this.FindResource("AccentColorBrush")
                            },
                        col,
                        0);
                    row = 0;

                    foreach (var qualified in qualifieds)
                    {
                        row++;

                        var box = new TextBox();
                        box.Tag = row * participantCount + col;
                        box.Foreground = Brushes.Black;
                        box.MaxLength = participantCount > 9 ? 2 : 1;

                        // if our input order is per couple, we set the tab index here:
                        //if (!Settings.Default.FinalEnterPerJudge)
                        //{
                        box.TabIndex = danceCount * judges.Count() * qualifieds.Count() + row * judges.Count()
                                        + col;
                        //}

                        var binding = new Binding("Mark")
                                            {
                                                Converter = new IntegerConverter(),
                                                ConverterParameter = participantCount
                                            };

                        var bindingError = new Binding("HasError")
                                                {
                                                    Converter = new ErrorToBackgroundConverter(),
                                                    Mode = BindingMode.OneWay
                                                };

                        box.SetBinding(TextBox.TextProperty, binding);
                        box.SetBinding(Control.BackgroundProperty, bindingError);
                        box.SetBinding(
                            IsEnabledProperty,
                            new Binding("MarkingInputEnabled") { Source = MainWindow.MainWindowHandle.ViewModel });

                        box.GotFocus += this.MarkingTextbox_GotFocus;
                        box.CaretBrush = new SolidColorBrush(Colors.Black);

                        box.TextChanged += (sender, args) =>
                            {
                                var textBox = sender as TextBox;
                                if (textBox == null)
                                {
                                    return;
                                }

                                if (textBox.Text.Length > 0)
                                {
                                    textBox.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
                                }
                            };

                        box.PreviewKeyDown += (sender, args) =>
                            {
                                var textBox = sender as TextBox;
                                if (textBox == null)
                                {
                                    return;
                                }

                                if (args.Key == Key.Left || args.Key == Key.Down || args.Key == Key.Right
                                    || args.Key == Key.Up)
                                {
                                    args.Handled = true;
                                    this.HandleCursorKeys(textBox, args.Key);
                                }
                            };

                        var participant =
                            model.DanceSkating.Single(d => d.DanceInRound.Dance.Id == dance.Dance.Id)
                                .CoupleResults.Single(p => p.Participant.Number == qualified.Participant.Number);

                        var mark =
                            participant.Marks.Single(
                                m =>
                                m.Participant.Id == qualified.Participant.Id && m.Dance.Id == dance.Dance.Id
                                && m.Judge.Id == judge.Id);

                        mark.PropertyChanged += this.MarkPropertyChanged;

                        this.allMarksList.Add(mark);

                        box.DataContext = mark;
                        this.SetCellTextBox(grid, box, col, row);
                    }
                }

                Debug.WriteLine("CreateGrids Step 1: " + stopwatch.ElapsedMilliseconds);

                var qualifiedList =
                    this.CurrentRound.Qualifieds.OrderBy(o => o.Participant.Number).ToList();

                for (var i = 0; i < qualifiedList.Count; i++)
                {
                    this.SetCellLabel(grid, new Label { Content = qualifiedList[i].Participant.Number }, 0, i + 1);

                    var coupleResultFinal =
                        model.DanceSkating.Single(d => d.DanceInRound.Dance.Id == dance.Dance.Id)
                            .CoupleResults.Single(p => p.Participant.Number == qualifiedList[i].Participant.Number);

                    // Add the Label for the Result:
                    var label3 = new Label() { DataContext = coupleResultFinal };
                    label3.SetBinding(ContentControl.ContentProperty, new Binding("Place"));
                    this.SetCellLabel(grid, label3, model.Judges.Count + model.Participants.Count + 1, i + 1);

                    for (var place = 1; place <= qualifiedList.Count; place++)
                    {
                        var headPlaces = place == 1 ? "1." : string.Format("1.-{0}.", place);

                        col = model.Judges.Count + place;

                        this.SetCellLabel(
                            grid,
                            new Label
                                {
                                    Content = headPlaces,
                                    FontSize = 12,
                                    Foreground = (Brush) this.FindResource("AccentColorBrush")
                                },
                            col,
                            0);

                        var label2 = new Label() { DataContext = coupleResultFinal };

                        var binding = new Binding("ItSelf")
                                            {
                                                Converter = new SkatingPlacesAndSumsConverter(),
                                                ConverterParameter = place,
                                                Mode = BindingMode.OneWay
                                            };

                        label2.SetBinding(ContentControl.ContentProperty, binding);

                        this.SetCellLabel(grid, label2, place + model.Judges.Count, i + 1);
                    }
                }

                // Header 'Place'
                this.SetCellLabel(
                    grid,
                    new Label
                        {
                            Content = "Place",
                            FontSize = 12,
                            Foreground = (Brush) this.FindResource("AccentColorBrush")
                        },
                    model.Judges.Count + model.Participants.Count + 1,
                    0);

                Debug.WriteLine("CreateGrids Step 2: " + stopwatch.ElapsedMilliseconds);
                // grid.UpdateLayout();
                Debug.WriteLine("Step 3: " + stopwatch.ElapsedMilliseconds);
            }
            Debug.WriteLine("CreateGrids Grids Created in " + stopwatch.ElapsedMilliseconds);

            // Setup the HasError-Flag that is not saved to the database
            foreach (var official in judges)
            {
                foreach (var danceInRound in this.Round.DancesInRound)
                {
                    this.SetHasErrorFlag(this.Round.Id, official.Id, danceInRound.Dance.Id);
                }
            }

            Debug.WriteLine("SetHasError>Flag: " + stopwatch.ElapsedMilliseconds);

            stopwatch.Stop();
        }

        private void CreateViewModel()
        {
            Debug.WriteLine("RoundId: " + this.currentRoundId);

            var stopwatch = new Stopwatch();
            stopwatch.Start();

            this.RemoveAllZeroMarks();

            this.skatingViewModel = SkatingHelper.CreateViewModel(this.context, this.CurrentRound);
           
            Debug.WriteLine("SetViewModel: " + stopwatch.ElapsedMilliseconds);

            stopwatch.Stop();
        }

        private void EnterCloseMarking(object sender, RoutedEventArgs e)
        {
            var dialog = new EnterClosedMarkingInFinal(this.skatingViewModel);
            MainWindow.MainWindowHandle.ShowDialog(dialog, null, 300, 300);
        }

        private void HandleCursorKeys(TextBox textBox, Key key)
        {
            var tabIndex = (int)textBox.Tag;

            switch (key)
            {
                case Key.Up:
                    tabIndex = tabIndex - this.skatingViewModel.Participants.Count;
                    break;
                case Key.Down:
                    tabIndex = tabIndex + this.skatingViewModel.Participants.Count;
                    break;
                case Key.Left:
                    tabIndex--;
                    break;
                case Key.Right:
                    tabIndex++;
                    break;
            }
            var grid = textBox.Parent as Grid;
            if (grid == null)
            {
                return;
            }

            var element = grid.Children.OfType<TextBox>().FirstOrDefault(t => (int)t.Tag == tabIndex);

            if (element != null)
            {
                element.Focus();
                element.SelectAll();
            }
        }

        private void MarkingTextbox_GotFocus(object sender, RoutedEventArgs e)
        {
            var box = sender as TextBox;
            box.SelectAll();
        }

        private void OnEditRankingPoints(object sender, RoutedEventArgs e)
        {
            var dialog = new ChangeStartbookData(this.CurrentRound, this.context);
            MainWindow.MainWindowHandle.ShowDialog(dialog, null, 300, 300);
        }

        private void OnUnlockMarkingClick(object sender, RoutedEventArgs e)
        {
#if DTV
            MainWindow.MainWindowHandle.ViewModel.MarkingInputEnabled = true;
            return;
#endif
            var page = new UnlockMarkingInput();
            MainWindow.MainWindowHandle.ShowDialog(page, null, 200, 200);
        }

        /// <summary>
        ///     Reads the marking.
        /// </summary>
        /// <param name="file">The file.</param>
        private void ReadMarking(string filename, SkatingViewModel model, int sectionId)
        {
            using (var inStr = new StreamReader(filename, Encoding.UTF8))
            {
                List<DanceInRound> dances;
                var currentRound = this.CurrentRound;

                if (sectionId == 0)
                {
                    dances = currentRound.DancesInRound.ToList();
                }
                else
                {
                    dances = sectionId == 1 ? currentRound.DancesInRound.Where(d => d.Dance.Id < 6).ToList()
                                            : currentRound.DancesInRound.Where(d => d.Dance.Id > 5).ToList();
                }

                foreach (var danceInRound in dances)
                {
                    var dance = model.DanceSkating.First(d => d.DanceInRound.Id == danceInRound.Id);

                    foreach (var coupleResults in dance.CoupleResults)
                    {
                        foreach (var marking in coupleResults.Marks)
                        {
                            marking.Mark = 0;
                        }
                    }
                }

               
                var qualifieds = currentRound.Qualifieds.ToList();
                var judges = this.Competition.GetJudges();

                var stopwatch = new Stopwatch();
                stopwatch.Start();

                while (!inStr.EndOfStream)
                {
                    Debug.WriteLine("While: " + stopwatch.Elapsed);
                    var data = CSVHelper.CSVSplitter(inStr.ReadLine());
                    // -> data[0] is couple number
                    var num = 0;
                    if (!Int32.TryParse(data[0], out num))
                    {
                        continue;
                    }

                    var couple = qualifieds.SingleOrDefault(c => c.Participant.Number == num);

                    if (couple == null)
                    {
                        continue;
                    }

                    if (!int.TryParse(data[1], out num))
                    {
                        continue;
                    }

                    for (var i = 0; i < num; i++)
                    {
                        var sign = data[2 + i * 3];
                        var judge = judges.SingleOrDefault(ju => ju.Sign == sign);

                        if (judge == null)
                        {
                            continue;
                        }

                        var judgments = data[3 + i * 3].Replace("\"", "").Split(';');
                        

                        for (var j = 0; j < dances.Count; j++)
                        {
                            var dance = dances[j];
                            var danceModel = model.DanceSkating.Single(
                                d => d.DanceInRound.Dance.Id == dance.Dance.Id);

                            var participant = danceModel.CoupleResults.Single(p => p.Participant.Id == couple.Participant.Id);

                            if (participant.Marks.Any(m => m.Judge == null))
                            {
                                Debug.WriteLine(participant.Participant.NiceNameWithNumber);
                                continue;
                            }

                            var judgement = participant.Marks.Single(r => r.Judge.Id == judge.Id);

                            var mark = 0;
                            if (int.TryParse(judgments[j], out mark))
                            {
                                judgement.Mark = mark;
                            }
                        }
                    }
                }
                Debug.WriteLine("Ende schleife : " + stopwatch.ElapsedMilliseconds);
                this.context.SaveChanges();
                this.skatingViewModel.DoCalculations();
            }
        }

        private void SetCellLabel(Grid g, UIElement label, int column, int row, int colSpan = 1)
        {
            Grid.SetColumn(label, column);
            Grid.SetRow(label, row);
            Grid.SetColumnSpan(label, colSpan);

            if (!g.Children.Contains(label))
            {
                g.Children.Add(label);
            }
        }

        private void SetCellTextBox(Grid g, TextBox label, int column, int row, int colSpan = 1)
        {
            Grid.SetColumn(label, column);
            Grid.SetRow(label, row);
            Grid.SetColumnSpan(label, colSpan);

            if (!g.Children.Contains(label))
            {
                g.Children.Add(label);
            }
        }

        private void SetHasErrorFlag(int roundId, int judgeId, int danceId)
        {
            var marks =
                this.context.Markings.Where(
                    m => m.Dance.Id == danceId && m.Judge.Id == judgeId && m.Round.Id == roundId && m.Mark > 0).ToList();

            foreach (var marking in marks)
            {
                marking.HasError = false;

                var sameValues = marks.Where(m => m.Mark == marking.Mark && m.Id != marking.Id);
                if (sameValues.Any())
                {
                    marking.HasError = true;
                    foreach (var sameValue in sameValues)
                    {
                        sameValue.HasError = true;
                    }
                }
            }
        }


        private void MarkPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (this.readingResultFile)
            {
                return;
            }

            if (e.PropertyName != "Mark")
            {
                return;
            }

            if (!(sender is Marking))
            {
                return;
            }

            var mark = sender as Marking;

            // Do we have a same value from the same judge?
            this.SetHasErrorFlag(this.Round.Id, mark.Judge.Id, mark.Dance.Id);

            this.context.SaveChanges();
            // Do we have all markings for this dance? then lets's calculate places
            var danceModel = this.skatingViewModel.DanceSkating.Single(d => d.DanceInRound.Dance.Id == mark.Dance.Id);

            if (danceModel.HasAllMarks())
            {
                foreach (var result in danceModel.CoupleResults)
                {
                    result.PlaceFrom = 0;
                    result.PlaceTo = 0;
                    result.Place = 0;
                }
                this.skatingViewModel.DoCalculations();
            }
        }

        private void OnMarkMessage(MarkMessage markMessage)
        {
            if (this.context == null || markMessage.Sender == null || markMessage.RoundCode != this.CurrentRound.EjudgeName)
            {
                return;
            }

            var eJudgeData = this.CurrentRound.EjudgeData.FirstOrDefault(j => j.DeviceId == markMessage.Sender);

            if (eJudgeData == null)
            {
                return;
            }

            var participant = this.CurrentRound.Qualifieds.First(q => q.Participant.Number == markMessage.Number).Participant;

            var dance = this.CurrentRound.DancesInRound.OrderBy(d => d.SortOrder).ToList()[markMessage.DanceIndex - 1];

            var mark =
                this.allMarksList.First(
                    m =>
                    m.Judge.Id == eJudgeData.Judge.Id && m.Participant.Id == participant.Id
                    && m.Dance.Id == dance.Dance.Id);

            if (mark != null)
            {
                mark.Mark = int.Parse(markMessage.Mark);
                markMessage.IsHandled = true;
            }
        }

        private void OnCompleteMarking(CompleteMarkingMessage message)
        {
            if (this.context == null || message.RoundCode != this.CurrentRound.EjudgeName)
            {
                return;
            }

            var eJudgeData = this.CurrentRound.EjudgeData.FirstOrDefault(j => j.DeviceId == message.Sender);
            if(eJudgeData == null)
            {
                // why did we get data from a device we do not know?
                return;
            }

            var judgementsOfJudge = this.allMarksList.Where(m => m.Judge.Id == eJudgeData.Judge.Id).OrderBy(m => m.Dance.DefaultOrder).ToList();

            var danceList = this.CurrentRound.DancesInRound.OrderBy(d => d.SortOrder).ToList();

            foreach (var number in message.Marks.Keys)
            {
                var list = message.Marks[number];

                for (var index = 1; index < list.Count; index++)
                {
                    var dance = danceList[index - 1];

                    var judgement =
                        judgementsOfJudge.FirstOrDefault(
                            j => j.Participant.Number == number && j.Dance.Id == dance.Dance.Id);

                    if (judgement != null)
                    {
                        judgement.Mark = int.Parse(list[index - 1]);
                    }
                    else
                    {
                        Debug.WriteLine("Why is this null?");
                    }
                    
                }
            }

            message.IsHandled = true;
        }

        private void OnSignatureMessage(SignatureMessage message)
        {
            if (this.context == null || message.RoundCode != this.CurrentRound.EjudgeName)
            {
                return;
            }

            var ejudgeData = this.CurrentRound.EjudgeData.First(e => e.DeviceId == message.Sender);

            foreach (var tuple in message.Coordinates)
            {
                ejudgeData.SignatureCoordinates.Add(new Coordinate() { X1 = tuple.Item1, Y1 = tuple.Item2, X2 = tuple.Item3, Y2 = tuple.Item4 });
            }

            this.context.SaveChanges();

            message.IsHandled = true;
        }

        private void OnJudgeLogon(JudgeLogonMessage judgeLogonMessage)
        {
            if (this.context == null || !this.CurrentRound.EjudgeEnabled)
            {
                return;
            }

            if (judgeLogonMessage.Round.Id != this.CurrentRound.Id)
            {
                return;
            }

            var ejudgeData = this.CurrentRound.EjudgeData.FirstOrDefault(e => e.Judge.Sign == judgeLogonMessage.Judge.Sign);

            if (ejudgeData != null)
            {
                // find device:
                var device = MainWindow.MainWindowHandle.ViewModel.Devices.First(d => d.DeviceId == judgeLogonMessage.DeviceId);

                if (device != null)
                {
                    ejudgeData.Device = device;
                    ejudgeData.DeviceId = judgeLogonMessage.DeviceId;
                    device.Round = this.CurrentRound;
                    judgeLogonMessage.IsHandled = true;
                }
            }
        }


        private void OnEjudgeMessage(EjudgeMessage message)
        {
            if (message is MarkMessage)
            {
                this.OnMarkMessage((MarkMessage)message);
            }

            if (message is CompleteMarkingMessage)
            {
                this.OnCompleteMarking((CompleteMarkingMessage)message);
            }

            if (message is SignatureMessage)
            {
                this.OnSignatureMessage((SignatureMessage)message);
            }

        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
    #endregion
}