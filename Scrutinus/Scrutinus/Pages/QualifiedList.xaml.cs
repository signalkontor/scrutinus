﻿// // TPS.net TPS8 Scrutinus
// // QualifiedList.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows;
using Scrutinus.Dialogs;
using Scrutinus.ViewModels;

namespace Scrutinus.Pages
{
    /// <summary>
    ///     Interaction logic for QualifiedList.xaml
    /// </summary>
    public partial class QualifiedList : ScrutinusPage
    {
        #region Constructors and Destructors

        public QualifiedList(int competitionId, int roundId)
            : base(competitionId, roundId)
        {
            this.InitializeComponent();
            this.DataContext = this.ViewModel;
        }

        #endregion

        #region Public Methods and Operators

        public override bool AllowToLeave()
        {
            return true;
        }

        public override bool CanMoveNext()
        {
            return true; // nothing to do or select here, just info...
        }

        protected override void OnNextInternal()
        {
        }

        #endregion

        #region Methods

        protected override void SetViewModel()
        {
            this.ViewModel = new QualifiedListViewModel(this.context, this.currentRoundId);
            this.DataContext = this.ViewModel;
        }

        private void BtnCheckSeeded_Click(object sender, RoutedEventArgs e)
        {
            var page = new StatusOfSeededCouples(this.competitionId, this.currentRoundId);
            MainWindow.MainWindowHandle.ShowDialog(page, null, 400, 300);
        }

        #endregion
    }
}