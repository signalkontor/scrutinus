﻿// // TPS.net TPS8 Scrutinus
// // ImportEventFromWDSF.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using DataModel.Models;
using Wdsf.Api.Client;

namespace Scrutinus.Pages
{
    /// <summary>
    ///     Interaction logic for ImportEventFromWDSF.xaml
    /// </summary>
    public partial class ImportEventFromWDSF : Page
    {
        #region Fields

        private readonly ScrutinusContext _context;

        #endregion

        #region Constructors and Destructors

        public ImportEventFromWDSF(ScrutinusContext context)
        {
            this.InitializeComponent();
            this._context = context;
        }

        #endregion

        #region Methods

        private void BtnOK_OnClick(object sender, RoutedEventArgs e)
        {
            var id = 0;
            if (Int32.TryParse(this.WDSFEventId.Text, out id))
            {
                var task = new Task(() => this.ImportEvent(id));
                task.Start();
            }
        }

        private void ImportEvent(int eventId)
        {
#if DEBUG
            var apiClient = new Client(
                Settings.Default.WDSFApiUser,
                Settings.Default.WDSFApiPassword,
                WdsfEndpoint.Services);
#else
            var apiClient = new Client(
                Settings.Default.WDSFApiUser,
                Settings.Default.WDSFApiPassword,
                WdsfEndpoint.Services);
#endif
            var filter = new Dictionary<string, string> { { "EventId", eventId.ToString() } };
            var wdsfEvent = apiClient.GetCompetitions(filter);

            foreach (var competition in wdsfEvent)
            {
                var competitionDetails = apiClient.GetCompetition(competition.Id);

                var newCompettion = new Competition
                                        {
                                            AgeGroup =
                                                this._context.AgeGroups.Single(
                                                    a => a.Id == 7),
                                            Class =
                                                this._context.Classes.Single(
                                                    c => c.Id == 8),
                                            CompetitionType =
                                                this._context.CompetitionTypes.Single(
                                                    c => c.Id == 1),
                                            Section =
                                                this._context.Sections.Single(
                                                    s => s.Id == 1),
                                            Stars = 0,
                                            State = 0,
                                            ExternalId = competitionDetails.Id.ToString()
                                        };

                this._context.Competitions.Add(new Competition());
            }

            this._context.SaveChanges();
        }

        #endregion
    }
}