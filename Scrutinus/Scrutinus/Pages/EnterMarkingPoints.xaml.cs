﻿// // TPS.net TPS8 Scrutinus
// // EnterMarkingPoints.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using General.Extensions;
using Scrutinus.Controls;
using Scrutinus.ViewModels;

namespace Scrutinus.Pages
{


    
    /// <summary>
    ///     Interaction logic for EnterMarkingSumOnly.xaml
    /// </summary>
    public partial class EnterMarkingPoints : ScrutinusPage
    {
        #region Constructors and Destructors

        public EnterMarkingPoints(int competitionId, int roundId)
            : base(competitionId, roundId)
        {
            this.InitializeComponent();

            this.CreateTabControl();
        }

        #endregion

        #region Public Methods and Operators

        public override bool AllowToLeave()
        {
            return true;
        }

        public override bool CanMoveNext()
        {
            return this.ViewModel.Validate();
        }

        protected override void OnNextInternal()
        {
            this.ViewModel.Validate();
            this.context.SaveChanges();
        }

        #endregion

        #region Methods

        protected override void SetViewModel()
        {
            this.ViewModel = new EnterMarkingSumsViewModel(this.context, this.CurrentRound.Id, CalculationMode.Points);
            this.DataContext = this.ViewModel;
        }

        private void CreateTabControl()
        {
            var viewModel = this.ViewModel as EnterMarkingSumsViewModel;
            // Create a new Tab-Page
            foreach (var marking in viewModel.Markings)
            {
                // Add a new tab page
                var tab = new TabItem { Header = marking.Judge.Sign };
                this.MarkingTab.Items.Add(tab);
                // our root is a stackpanel holding all input controls:
                var stackPanel = new StackPanel();

                var officialLabel = new Label { Content = marking.Judge.NiceName };
                officialLabel.FontSize = 15;
                officialLabel.Margin = new Thickness(0, 10, 0, 10);

                stackPanel.Children.Add(officialLabel);

                var blocks =
                    marking.CouplesMarks.OrderBy(m => m.Participant.Number).Chunk(12);

                var last = blocks.Last();
                foreach (var block in blocks)
                {
                    stackPanel.Children.Add(new MarksSumEditor(block, this.CurrentRound.DancesInRound.Count, CalculationMode.Points, ReferenceEquals(block, last)));
                    stackPanel.Children.Add(new Border { Height = 30 });
                }

                var totalStackPanel = new StackPanel { Orientation = Orientation.Horizontal };

                stackPanel.Children.Add(totalStackPanel);

                var totalSum = new Label { FontSize = 16 };
                var binding = new Binding("TotalPoints") { Source = marking };
                totalSum.SetBinding(ContentControl.ContentProperty, binding);
                totalStackPanel.Children.Add(totalSum);

                int? lowerMax = 0;
                int? upperMax = 99;

                var text = lowerMax == upperMax ? lowerMax.ToString() : lowerMax + " - " + upperMax;
                var marksLabel = new Label { FontSize = 16, Content = text, Margin = new Thickness(20, 0, 0, 0) };
                totalStackPanel.Children.Add(marksLabel);

                tab.Content = stackPanel;
            }
        }

        #endregion
    }
}