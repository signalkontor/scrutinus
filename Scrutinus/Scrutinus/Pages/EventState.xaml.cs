﻿// // TPS.net TPS8 Scrutinus
// // EventState.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using Scrutinus.ViewModels;

namespace Scrutinus.Pages
{
    /// <summary>
    ///     Interaction logic for EventState.xaml
    /// </summary>
    public partial class EventState : ScrutinusPage
    {
        #region Constructors and Destructors

        public EventState()
            : base(0, 0)
        {
            this.InitializeComponent();
        }

        #endregion

        #region Methods

        protected override void SetViewModel()
        {
        }

        #endregion

        #region Public Methods and Operators

        public override bool AllowToLeave()
        {
            var viewModel = this.DataContext as EventStateViewModel;
            viewModel.StopUpdateThread();
            return true;
        }

        public override bool CanMoveNext()
        {
            return false;
        }

        protected override void OnNextInternal()
        {
            var viewModel = this.DataContext as EventStateViewModel;
            viewModel.StopUpdateThread();
        }

        #endregion
    }
}