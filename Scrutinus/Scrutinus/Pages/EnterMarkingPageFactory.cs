﻿// // TPS.net TPS8 Scrutinus
// // EnterMarkingPageFactory.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using DataModel;
using Scrutinus.ViewModels;

namespace Scrutinus.Pages
{
    public static class EnterMarkingPageFactory
    {
        #region Public Methods and Operators

        public static ScrutinusPage MarkingPage(CompetitionElement competition)
        {
            ScrutinusPage page = null;

            switch (competition.CurrentRound.MarksInputType)
            {
                case MarkingTypes.Marking:
                    page = new EnterMarking(competition.Competition.Id, competition.CurrentRound.Id);
                    break;
                case MarkingTypes.NewJudgingSystemV2:
                    page = new NewJudgingMarking(competition.Competition.Id, competition.CurrentRound.Id);
                    break;
                case MarkingTypes.NewJudgingSystemV1:
                    page = new JudgingSystem1_0(competition.Competition.Id, competition.CurrentRound.Id);
                    break;
                case MarkingTypes.Skating:
                    page = new FinalMarking(competition.Competition.Id, competition.CurrentRound.Id);
                    break;
                case MarkingTypes.MarksSumOnly:
                    page = new EnterMarkingSumOnly(competition.Competition.Id, competition.CurrentRound.Id);
                    break;
                case MarkingTypes.TeamMatch:
                case MarkingTypes.PointsGeneral:
                    page = new EnterMarkingPoints(competition.Competition.Id, competition.CurrentRound.Id);
                    break;
            }

            return page;
        }

        #endregion
    }
}