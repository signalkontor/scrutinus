﻿// // TPS.net TPS8 Scrutinus
// // PrintingPrintManager.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using DataModel.Models;
using Scrutinus.Helper;

namespace Scrutinus.Pages
{
    public class PrintReportViewModel
    {
        public bool Print { get; set; }

        public int Copies { get; set; }

        public PrintReportDefinition PrintDefinition { get; set; }
    }
    /// <summary>
    /// Interaction logic for PrintingPrintManager.xaml
    /// </summary>
    public partial class PrintingPrintManager : ScrutinusPage
    {
        public PrintingPrintManager(int competitionId, int roundId)
            : base(competitionId, roundId)
        {
            var previousRound = this.context.Rounds.FirstOrDefault(r => r.Competition.Id == this.CurrentRound.Competition.Id && r.Number == this.CurrentRound.Number - 1);

            this.PrintReportDefinitions = new ObservableCollection<PrintReportViewModel>(PrintManagerHelper.GetPrintJobs(this.context, this.Competition, this.CurrentRound, previousRound));

            this.InitializeComponent();
        }

        public override VerticalAlignment FrameVerticalContentAllignment => VerticalAlignment.Top;

        public ObservableCollection<PrintReportViewModel> PrintReportDefinitions { get; set; }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            PrintManagerHelper.Print(this.context, this.Competition, this.CurrentRound, this.PrintReportDefinitions);

            MainWindow.MainWindowHandle.NextState(this.competitionId);
        }

        public override bool AllowToLeave()
        {
            return true;
        }

        public override bool CanMoveNext()
        {
            return true;
        }

        protected override void OnNextInternal()
        {
            
        }

        protected override void SetViewModel()
        {
            
        }

        private void PrintDlgButton_OnClick(object sender, RoutedEventArgs e)
        {
            MainWindow.MainWindowHandle.ShowPrintDialog();
        }
    }
}
