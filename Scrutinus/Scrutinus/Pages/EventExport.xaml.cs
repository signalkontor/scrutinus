﻿// // TPS.net TPS8 Scrutinus
// // EventExport.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH
namespace Scrutinus.Pages
{
    /// <summary>
    ///     Interaction logic for EventExport.xaml
    /// </summary>
    public partial class EventExport : ScrutinusPage
    {
        #region Constructors and Destructors

        public EventExport()
            : base(0, 0)
        {
            this.InitializeComponent();
        }

        #endregion

        #region Methods

        protected override void SetViewModel()
        {
        }

        #endregion

        #region Public Methods and Operators

        public override bool AllowToLeave()
        {
            return true;
        }

        public override bool CanMoveNext()
        {
            // This page has no next page
            return false;
        }

        protected override void OnNextInternal()
        {
        }

        #endregion
    }
}