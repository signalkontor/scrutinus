﻿// // TPS.net TPS8 Scrutinus
// // JudgingSystem1_0.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using DataModel;
using DataModel.Export;
using DataModel.ModelHelper;
using DataModel.Models;
using General.Extensions;
using General.WdsfApi;
using MahApps.Metro.Controls.Dialogs;
using Scrutinus.Localization;
using Wdsf.Api.Client;

namespace Scrutinus.Pages
{
    public class JudgingSystem1ViewModel : INotifyPropertyChanged
    {
        #region Constructors and Destructors

        public JudgingSystem1ViewModel(List<JudgingComponent> judgingComponents)
        {
            this.components = judgingComponents;
        }

        #endregion

        #region Public Events

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Methods

        private void RaiseEvent(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region Fields

        private readonly List<Marking> _markings = new List<Marking>();

        private readonly List<JudgingComponent> components;

        #endregion

        #region Public Properties

        public double ComponentA { get; set; }

        public double ComponentB { get; set; }

        public double ComponentC { get; set; }

        public double ComponentD { get; set; }

        public Dance Dance { get; set; }

        public Participant Participant { get; set; }

        public String Place
        {
            get
            {
                if (this.PlaceFrom == this.PlaceTo)
                {
                    return this.PlaceFrom + ".";
                }
                return String.Format("{0}. - {1}.", this.PlaceFrom, this.PlaceTo);
            }
        }

        // public double ComponentE { get; set; }

        public int PlaceFrom { get; set; }

        public int PlaceTo { get; set; }

        public double Total { get; set; }

        #endregion

        #region Public Methods and Operators

        public void AddMarking(Marking mark)
        {
            this._markings.Add(mark);
            mark.PropertyChanged += (sender, args) => this.Recalculate();
        }

        public void ClearMarking()
        {
            this._markings.Clear();
        }

        public void Recalculate()
        {
            // Recalucualte Totals
            // if (_markings.Any(m => m.MarkA == 0)) return;

            if (this._markings.Count == 0)
            {
                return;
            }
            this.ComponentA = (this._markings.Sum(s => s.MarkA) - this._markings.Min(s => s.MarkA)
                               - this._markings.Max(m => m.MarkA)) / (this._markings.Count - 2);
            this.ComponentB = (this._markings.Sum(s => s.MarkB) - this._markings.Min(s => s.MarkB)
                               - this._markings.Max(m => m.MarkB)) / (this._markings.Count - 2);
            this.ComponentC = (this._markings.Sum(s => s.MarkC) - this._markings.Min(s => s.MarkC)
                               - this._markings.Max(m => m.MarkC)) / (this._markings.Count - 2);
            this.ComponentD = (this._markings.Sum(s => s.MarkD) - this._markings.Min(s => s.MarkD)
                               - this._markings.Max(m => m.MarkD)) / (this._markings.Count - 2);
            //ComponentE = (_markings.Sum(s => s.MarkE) - _markings.Min(s => s.MarkE) - _markings.Max(m => m.MarkE)) /
            //        (_markings.Count - 2);

            this.Total = this.ComponentA * (this.components.Count > 0 ? this.components[0].Weight : 1d)
                         + this.ComponentB * (this.components.Count > 1 ? this.components[1].Weight : 1d)
                         + this.ComponentC * (this.components.Count > 2 ? this.components[2].Weight : 1d)
                         + this.ComponentD * (this.components.Count > 3 ? this.components[3].Weight : 1d);
            // +ComponentE;

            this.Total = Math.Round(this.Total, 3);

            this.RaiseEvent("ComponentA");
            this.RaiseEvent("ComponentB");
            this.RaiseEvent("ComponentC");
            this.RaiseEvent("ComponentD");
            //RaiseEvent("ComponentE");
            this.RaiseEvent("Total");
        }

        #endregion
    }

    public class TotalViewModel : INotifyPropertyChanged
    {
        #region Public Events

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Public Methods and Operators

        public void RaiseEvent(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region Fields

        private int _placeFrom;

        private int _placeTo;

        private double _total;

        #endregion

        #region Public Properties

        public Participant Participant { get; set; }

        public string Place
        {
            get
            {
                if (this._placeFrom == this.PlaceTo)
                {
                    return this._placeFrom + ".";
                }
                return String.Format("{0}. - {1}.", this._placeFrom, this._placeTo);
            }
        }

        public int PlaceFrom
        {
            get
            {
                return this._placeFrom;
            }
            set
            {
                this._placeFrom = value;
                this.RaiseEvent("PlaceFrom");
                this.RaiseEvent("Place");
            }
        }

        public int PlaceTo
        {
            get
            {
                return this._placeTo;
            }
            set
            {
                this._placeTo = value;
                this.RaiseEvent("PlaceTo");
                this.RaiseEvent("Place");
            }
        }

        public double Total
        {
            get
            {
                return this._total;
            }
            set
            {
                this._total = value;
                this.RaiseEvent("Total");
            }
        }

        #endregion
    }

    public partial class JudgingSystem1_0 : ScrutinusPage
    {
        #region Constructors and Destructors

        public JudgingSystem1_0(int competitionId, int roundId)
            : base(competitionId, roundId)
        {
            this.InitializeComponent();
            this.BuildDataModel();
            this.ShowView();

            this.ImportPath.Text = Settings.Default.NewJudgingSystemInputFolder + "\\" + competitionId + "_"
                                   + this.CurrentRound.Number;
        }

        #endregion

        #region Fields

        private List<TotalViewModel> _totalList = new List<TotalViewModel>();

        private List<JudgingSystem1ViewModel> _viewData = new List<JudgingSystem1ViewModel>();

        #endregion

        #region Public Methods and Operators

        public override bool AllowToLeave()
        {
            return true;
        }

        public override bool CanMoveNext()
        {
            // Check, we have markings and leave
            // We should have Couples * Judges * Dances Marks:
            var should = this.GetCurrentRound().Qualifieds.Count * this.GetCurrentRound().Competition.GetJudges().Count
                         * this.GetCurrentRound().DancesInRound.Count;
            var count = this.context.Markings.Count(m => m.Round.Id == this.currentRoundId);
            if (count < should)
            {
                var res =
                    MainWindow.MainWindowHandle.ShowMessage(
                        string.Format(LocalizationService.Resolve(() => Text.MarkingsShouldButIs), should, count),
                        "Continue",
                        MessageDialogStyle.AffirmativeAndNegative);

                if (res == MessageDialogResult.Negative)
                {
                    return false;
                }
            }

            return true;
        }

        protected override void OnNextInternal()
        {
            var round = this.GetCurrentRound();
            // calculate places, writeback
            foreach (var participant in this._totalList)
            {
                var qualified =
                    this.context.Qualifieds.SingleOrDefault(
                        q => q.Participant.Id == participant.Participant.Id && q.Round.Id == this.currentRoundId);

                if (qualified == null)
                {
                    continue;
                }

                qualified.Points = participant.Total;
                qualified.PlaceFrom = participant.PlaceFrom;
                qualified.PlaceTo = participant.PlaceTo;
                // We also update Participants, to have all data
                qualified.Participant.Points = participant.Total;
                qualified.Participant.PlaceFrom = participant.PlaceFrom;
                qualified.Participant.PlaceTo = participant.PlaceTo;

                if (round.RoundType.Id == RoundTypes.Final)
                {
                    qualified.Participant.State = CoupleState.DroppedOut;
                }
            }

            if (this.CurrentRound.RoundType.IsFinal)
            {
                this.GetCompetition().State = CompetitionState.Finished;
            }

            this.context.SaveChanges();
        }

        #endregion

        #region Methods

        protected override void SetViewModel()
        {
        }

        private void BtnExportEjudge_OnClick(object sender, RoutedEventArgs e)
        {
            var path = Settings.Default.NewJudgingSystemInputFolder
                          + String.Format("\\{0}_{1}", this.Competition.Id, this.CurrentRound.Number);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            var outStr = new StreamWriter(path + "\\data.txt");
            outStr.WriteLine("V1"); // Version String 1
            // WDSGExportFolder + PlaceOffset:
            outStr.WriteLine("Final;0");
            // Judging Components
            // Write Judging Areas to disk:
            outStr.WriteLine(this.Competition.CompetitionType.JudgingComponents.Count);
            foreach (
                var component in this.Competition.CompetitionType.JudgingComponents.OrderBy(o => o.Order))
            {
                outStr.WriteLine("{0};{1};{2}", component.Code, component.Weight, component.Description);
            }
            // Event Data
            outStr.WriteLine(
                "{0};{1};{2};{3}",
                this.Competition.ExternalId,
                this.Competition.Id,
                this.CurrentRound.Number,
                this.Competition.Title);
            // Dances
            foreach (var danceRound in this.CurrentRound.DancesInRound)
            {
                outStr.Write(danceRound.Dance.ShortName + ";");
                outStr.Write(danceRound.Dance.DanceName + ";");
            }
            // Empty Line to signal end of dance block
            outStr.WriteLine("");
            // Judges
            var judges = this.Competition.GetJudges().OrderBy(j => j.Sign);
            foreach (var judge in judges)
            {
                outStr.WriteLine(
                    "Judge;{0};{1};{2};{3};{4}",
                    judge.Sign,
                    judge.Sign,
                    judge.NiceName,
                    judge.Club,
                    judge.MIN);
            }
            outStr.WriteLine(";");
            foreach (var qualified in this.CurrentRound.Qualifieds)
            {
                var country =
                    this.context.CountryCodes.SingleOrDefault(c => c.LongName == qualified.Participant.Couple.Country);
                var couple = qualified.Participant.Couple;
                var nameStr = couple.FirstMan + "_" + couple.LastMan + "/" + couple.FirstWoman + "_"
                                 + couple.LastWoman;

                outStr.WriteLine(
                    "{0};{1};{2};{3}",
                    qualified.Participant.Number,
                    nameStr,
                    country != null ? country.IocCode : qualified.Participant.Couple.Country,
                    qualified.Participant.Couple.ExternalId);
            }
            // End Marker
            // Heats are now in an extra file
            outStr.WriteLine(";");

            outStr.Close();

            var heats = HeatsHelper.GetHeats(
                this.context,
                this.currentRoundId);

            if (heats.Count > 0)
            {
                outStr = new StreamWriter(path + "\\heats.txt");
                foreach (var danceRound in this.CurrentRound.DancesInRound)
                {
                    outStr.Write(danceRound.Dance.ShortName);

                    var heatsOfDance = heats[danceRound];
                    outStr.Write(";{0}", heatsOfDance.Count);
                    foreach (var heat in heatsOfDance)
                    {
                        outStr.Write(";{0}", heat.Count);
                        foreach (var participant in heat)
                        {
                            outStr.Write(";{0}", participant.Number);
                        }
                    }
                    outStr.WriteLine("");
                }
                outStr.Close();
            }
        }

        private void BtnExportWDSG_Click(object sender, RoutedEventArgs e)
        {
            var task = new Task(
                () =>
                    {
                        // we export the last round
                        var round = this.GetCurrentRound();
                        if (round.Number == 1)
                        {
                            // This is first round, we only export qualified of this round
                            ExportWDSG.ExportWDSGFile(this.context, round.Id, Settings.Default.WDSGExportFolder);
                            return;
                        }
                        var sendRound =
                            this.context.Rounds.SingleOrDefault(
                                r => r.Competition.Id == round.Competition.Id && r.Number == round.Number - 1);

                        ExportWDSG.ExportWDSGFile(this.context, sendRound.Id, Settings.Default.WDSGExportFolder);
                    });
            task.Start();
        }

        private void BtnWdsfApi_OnClick(object sender, RoutedEventArgs e)
        {
#if DEBUG
            var apiClient = new Client("guest", "guest", WdsfEndpoint.Sandbox);
#else
            var apiClient = new Client(
                Settings.Default.WDSFApiUser,
                Settings.Default.WDSFApiPassword,
                WdsfEndpoint.Services);
#endif
            var helper = new WdsfResultSender(apiClient, this.context, MainWindow.MainWindowHandle);
            // Upload asynchron
            var task = new Task(() => helper.SendResult(this.CurrentRound.Id));
            task.Start();
        }

        private void BuildDataModel()
        {
            var round = this.GetCurrentRound();

            this._viewData = new List<JudgingSystem1ViewModel>();
            this._totalList = new List<TotalViewModel>();

            foreach (var qualified in round.Qualifieds)
            {
                var total = new TotalViewModel
                                {
                                    Participant = qualified.Participant,
                                    Total = 0,
                                    PlaceFrom = 0,
                                    PlaceTo = 0
                                };
                this._totalList.Add(total);
                total.PropertyChanged += this.UpdatePlaces;
            }
            var components =
                this.Competition.CompetitionType.JudgingComponents.OrderBy(o => o.Order).ToList();

            foreach (var danceRound in round.DancesInRound)
            {
                foreach (var qualified in round.Qualifieds)
                {
                    var v = new JudgingSystem1ViewModel(components)
                                {
                                    ComponentA = 0,
                                    ComponentB = 0,
                                    ComponentC = 0,
                                    ComponentD = 0,
                                    // ComponentE = 0,
                                    Dance = danceRound.Dance,
                                    Participant = qualified.Participant
                                };
                    this._viewData.Add(v);
                    v.PropertyChanged += this.UpdateTotal;
                    if (this.context.Markings.Any(m => m.Dance.Id == danceRound.Id && m.Round.Id == this.currentRoundId))
                    {
                        // We have some markings and we add it now to the view if possible
                        var marks =
                            this.context.Markings.Where(
                                m =>
                                m.Dance.Id == danceRound.Id && m.Round.Id == this.currentRoundId
                                && m.Participant.Id == qualified.Participant.Id);
                        foreach (var mark in marks)
                        {
                            v.AddMarking(mark);
                        }
                    }
                }
            }
        }

        private void ImportData_OnClick(object sender, RoutedEventArgs e)
        {
            var path = this.ImportPath.Text;
            if (!Directory.Exists(path))
            {
                MainWindow.MainWindowHandle.ShowMessage(
                    LocalizationService.Resolve(() => Text.ImportFolderDoesNotExist));
                return;
            }

            // Delete any marking
            foreach (var model in this._viewData)
            {
                model.ClearMarking();
            }

            foreach (var source in this.context.Markings.Where(m => m.Round.Id == this.currentRoundId).ToList())
            {
                this.context.Markings.Remove(source);
            }
            this.context.SaveChanges();

            // Now import the data
            var files = Directory.GetFiles(this.ImportPath.Text, "Result_*.txt");
            foreach (var file in files)
            {
                this.ImportFile(file);
            }

            foreach (var judgingSystem1ViewModel in this._viewData)
            {
                judgingSystem1ViewModel.Recalculate();
            }
        }

        private void ImportFile(string file)
        {
            var reader = new StreamReader(file);
            var judges = this.Competition.GetJudges();

            var decChar = Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator;

            while (!reader.EndOfStream)
            {
                var data = reader.ReadLine().Split(';');

                if (data.Length < 8)
                {
                    MainWindow.MainWindowHandle.ShowMessage(
                        string.Format(LocalizationService.Resolve(() => Text.FileHasWrongFormat), file));

                    reader.Close();
                    return;
                }

                var sign = data[0];

                var danceSign = data[1];
                var judge = judges.SingleOrDefault(o => o.Sign == sign);
                var dance = this.context.Dances.SingleOrDefault(d => d.ShortName == danceSign);
                var number = Int32.Parse(data[2]);
                var participant =
                    this.context.Participants.SingleOrDefault(
                        p => p.Number == number && p.Competition.Id == this.competitionId);

                if (participant == null)
                {
                    MainWindow.MainWindowHandle.ShowMessage(
                        string.Format(LocalizationService.Resolve(() => Text.ParticipantWithNumberNotFound), number));
                    reader.Close();
                    return;
                }
                if (judge == null)
                {
                    MainWindow.MainWindowHandle.ShowMessage(
                        string.Format(LocalizationService.Resolve(() => Text.JudgeWithSignNotFound), sign));
                    reader.Close();
                    return;
                }
                if (dance == null)
                {
                    MainWindow.MainWindowHandle.ShowMessage(
                        string.Format(LocalizationService.Resolve(() => Text.DanceWithShortNameNotFound), danceSign));

                    reader.Close();
                    return;
                }

                var marking = new Marking(participant, this.CurrentRound, dance, judge)
                                  {
                                      Created = DateTime.Now,
                                      MarkA = data[3].SaveParse(),
                                      MarkB = data[4].SaveParse(),
                                      MarkC = data[5].SaveParse(),
                                      MarkD = data[6].SaveParse(),
                                      MarkE = data[7].SaveParse()
                                  };
                this.context.Markings.Add(marking);
                var couple =
                    this._viewData.SingleOrDefault(p => p.Participant.Number == number && p.Dance.Id == dance.Id);
                couple.AddMarking(marking);
            }
            this.context.SaveChanges();

            reader.Close();
        }

        private void ShowView()
        {
            var panel = this.Results;
            var round = this.GetCurrentRound();

            foreach (var danceRound in round.DancesInRound)
            {
                panel.Children.Add(new Label { Content = danceRound.Dance.DanceName });
                var grid = new DataGrid
                               {
                                   AutoGenerateColumns = false,
                                   CanUserAddRows = false,
                                   CanUserDeleteRows = false
                               };
                panel.Children.Add(grid);
                grid.Columns.Add(
                    new DataGridTextColumn
                        {
                            IsReadOnly = true,
                            Header = "No.",
                            Binding = new Binding("Participant.Number")
                        });
                grid.Columns.Add(
                    new DataGridTextColumn { IsReadOnly = true, Header = "A", Binding = new Binding("ComponentA") });
                grid.Columns.Add(
                    new DataGridTextColumn { IsReadOnly = true, Header = "B", Binding = new Binding("ComponentB") });
                grid.Columns.Add(
                    new DataGridTextColumn { IsReadOnly = true, Header = "C", Binding = new Binding("ComponentC") });
                grid.Columns.Add(
                    new DataGridTextColumn { IsReadOnly = true, Header = "D", Binding = new Binding("ComponentD") });
                //grid.Columns.Add(new DataGridTextColumn { Header = "E", Binding = new Binding("ComponentE") });
                grid.Columns.Add(
                    new DataGridTextColumn { IsReadOnly = true, Header = "Total", Binding = new Binding("Total") });
                grid.Columns.Add(
                    new DataGridTextColumn { IsReadOnly = true, Header = "Place", Binding = new Binding("Place") });
                grid.ItemsSource =
                    this._viewData.Where(v => v.Dance.Id == danceRound.Dance.Id).OrderBy(o => o.Participant.Number);
            }
            // Add the total Table
            panel.Children.Add(new Label { Content = "Total" });
            var gridTotal = new DataGrid
                                {
                                    AutoGenerateColumns = false,
                                    CanUserAddRows = false,
                                    CanUserDeleteRows = false
                                };
            panel.Children.Add(gridTotal);
            gridTotal.Columns.Add(
                new DataGridTextColumn
                    {
                        IsReadOnly = true,
                        Header = "No.",
                        Binding = new Binding("Participant.Number")
                    });
            gridTotal.Columns.Add(
                new DataGridTextColumn { IsReadOnly = true, Header = "Total", Binding = new Binding("Total") });
            gridTotal.Columns.Add(
                new DataGridTextColumn { IsReadOnly = true, Header = "Place", Binding = new Binding("Place") });
            gridTotal.ItemsSource = this._totalList;
        }

        private void UpdatePlaces(object sender, PropertyChangedEventArgs args)
        {
            if (args.PropertyName != "Total")
            {
                return;
            }

            // Here we sort by total and give the right places:
            var sorted = this._totalList.OrderByDescending(t => t.Total).ToList();

            var placefrom = 1;
            // Reset all places
            foreach (var total in sorted)
            {
                total.PlaceFrom = 0;
                total.PlaceTo = 0;
            }

            for (var i = 0; i < sorted.Count - 1; i++)
            {
                sorted[i].PlaceFrom = placefrom;
                sorted[i].PlaceTo = placefrom;
                if (sorted[i + 1].Total == sorted[i].Total)
                {
                    var index = i + 1;
                    var placecount = 1;
                    while (index < sorted.Count && sorted[i].Total == sorted[index].Total)
                    {
                        index++;
                        placecount++;
                    }
                    for (var j = i; j < index; j++)
                    {
                        sorted[j].PlaceFrom = placefrom;
                        sorted[j].PlaceTo = placefrom + placecount - 1;
                    }
                    i = index - 1;
                    placefrom += placecount - 1;
                }
                placefrom++;
            }
            if (sorted[sorted.Count - 1].PlaceFrom == 0)
            {
                sorted[sorted.Count - 1].PlaceFrom = placefrom;
                sorted[sorted.Count - 1].PlaceTo = placefrom;
            }
        }

        private void UpdateTotal(object sender, PropertyChangedEventArgs e)
        {
            var model = sender as JudgingSystem1ViewModel;
            if (model == null)
            {
                return;
            }

            var total = this._totalList.Single(t => t.Participant.Id == model.Participant.Id);
            // Update all totals:
            total.Total = this._viewData.Where(p => p.Participant.Id == model.Participant.Id).Sum(v => v.Total);
        }

        #endregion
    }
}