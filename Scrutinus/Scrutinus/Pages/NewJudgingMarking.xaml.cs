﻿// // TPS.net TPS8 Scrutinus
// // NewJudgingMarking.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Xml;
using DataModel;
using DataModel.Export;
using DataModel.ModelHelper;
using DataModel.Models;
using General.Extensions;
using General.NewJudgingSystem;
using Scrutinus.ExternalApi;
using Scrutinus.Localization;
using Scrutinus.ViewModels;

namespace Scrutinus.Pages
{
    public class BindingParameter
    {
        #region Public Properties

        public Dance Dance { get; set; }

        public Official Judge { get; set; }

        #endregion
    }

    public class ViewModelConverter : IValueConverter
    {
        #region Public Methods and Operators

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var Marking = value as Dictionary<Dance, Dictionary<Official, Marking>>;
            var bindingParameter = parameter as BindingParameter;
            return Marking[bindingParameter.Dance][bindingParameter.Judge].Mark20.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is string))
            {
                return value;
            }

            double dvalue = 0;
            if (Double.TryParse(value as string, out dvalue))
            {
                return dvalue;
            }
            return 0d;
        }

        #endregion
    }

    public class NewJudgingMarkingViewModel : BaseViewModel
    {
        #region Public Properties

        public Dictionary<Dance, Dictionary<Official, Marking>> Marking { get; set; }

        public Participant Participant { get; set; }

        #endregion

        #region Public Methods and Operators

        public void RaiseMarkingEvent()
        {
            this.RaisePropertyChanged(() => this.Marking);
        }

        public override bool Validate()
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    /// <summary>
    ///     Interaction logic for NewJudgingMarking.xaml
    /// </summary>
    public partial class NewJudgingMarking : ScrutinusPage
    {
        #region Fields

        private List<NewJudgingMarkingViewModel> _viewModel;

        #endregion

        #region Constructors and Destructors

        public NewJudgingMarking(int competitionId, int roundId)
            : base(competitionId, roundId)
        {
            this.InitializeComponent();

            // Set Import Path in Textbox
            this.ImportPath.Text = Settings.Default.NewJudgingSystemInputFolder + "\\" + competitionId + "_"
                                   + this.GetRoundNumber();

            this.context.Database.ExecuteSqlCommand("DELETE FROM Markings WHERE Round_Id = " + roundId);
            this.context = new ScrutinusContext();

            var task = new Task(
                () =>
                    {
                        MainWindow.MainWindowHandle.ReportStatus("Removing old Judgments", 0, 100);
                        var round = this.context.Rounds.Single(r => r.Id == this.currentRoundId);

                        this.GenerateViewData(round);

                        MainWindow.MainWindowHandle.ReportStatus("Building Grid", 0, 100);

                        this.Dispatcher.Invoke(
                            new Action(
                                () =>
                                    {
                                        this.TotalGrid.ItemsSource = round.Qualifieds;

                                        var gridList = new List<DataGrid>
                                                           {
                                                               this.Dance1,
                                                               this.Dance2,
                                                               this.Dance3,
                                                               this.Dance4,
                                                               this.Dance5,
                                                               this.Dance6,
                                                               this.Dance7,
                                                               this.Dance8,
                                                               this.Dance9,
                                                               this.Dance10
                                                           };
                                        var labelList = new List<Label>
                                        {
                                            this.DanceName1,
                                            this.DanceName2,
                                            this.DanceName3,
                                            this.DanceName4,
                                            this.DanceName5,
                                            this.DanceName6,
                                            this.DanceName7,
                                            this.DanceName8,
                                            this.DanceName9,
                                            this.DanceName10,
                                        };

                                        MainWindow.MainWindowHandle.ReportStatus("Generating Data-Grid", 0, 100);
                                        var index = 0;
                                        foreach (var dance in round.DancesInRound.OrderBy(d => d.SortOrder))
                                        {
                                            this.GenerateDataGrid(gridList[index], dance);
                                            gridList[index].ItemsSource = this._viewModel;
                                            index++;
                                        }

                                        for (var i = round.DancesInRound.Count; i < 10; i++)
                                        {
                                            gridList[i].Visibility = Visibility.Collapsed;
                                            labelList[i].Visibility = Visibility.Collapsed;
                                        }

                                        MainWindow.MainWindowHandle.ReportStatus("", 0, 100);
                                    }));
                    });
            task.Start();

            // Set Dance Names :
            var dances = this.CurrentRound.DancesInRound.ToList();
            this.DanceName1.Content = dances[0].Dance.DanceName;
            this.DanceName2.Content = dances.Count > 1 ? dances[1].Dance.DanceName : "";
            this.DanceName3.Content = dances.Count > 2 ? dances[2].Dance.DanceName : "";
            this.DanceName4.Content = dances.Count > 3 ? dances[3].Dance.DanceName : "";
            this.DanceName5.Content = dances.Count > 4 ? dances[4].Dance.DanceName : "";
            this.DanceName6.Content = dances.Count > 5 ? dances[5].Dance.DanceName : "";
            this.DanceName7.Content = dances.Count > 6 ? dances[6].Dance.DanceName : "";
            this.DanceName8.Content = dances.Count > 7 ? dances[7].Dance.DanceName : "";
            this.DanceName9.Content = dances.Count > 8 ? dances[8].Dance.DanceName : "";
            this.DanceName10.Content = dances.Count > 9 ? dances[9].Dance.DanceName : "";



        }

        #endregion

        #region Public Methods and Operators

        public override bool AllowToLeave()
        {
            return true;
        }

        public override bool CanMoveNext()
        {
            // Check, if we have markings from all judges and dances for 
            // While we have zeros, we do not have all markings.
            var canMoveNext = true; //!this.context.Markings.Any(m => m.Round.Id == this.currentRoundId && m.Mark20 == 0.0);
            if (!canMoveNext)
            {
                MainWindow.MainWindowHandle.ShowMessage(
                    LocalizationService.Resolve(() => Text.NotAllMarksSet_Cannotmove));
            }
            return canMoveNext;
        }

        protected override void OnNextInternal()
        {
            // If this was a final, we update all participant data with places and point
            var round = this.GetCurrentRound();

            foreach (var qualified in round.Qualifieds)
            {
                qualified.Participant.Points = qualified.Points;
                qualified.Participant.PlaceFrom = qualified.PlaceFrom;
                qualified.Participant.PlaceTo = qualified.PlaceTo;
                if (round.RoundType.Id == RoundTypes.Final)
                {
                    qualified.Participant.State = CoupleState.DroppedOut;
                }
            }

            if (round.RoundType.Id == RoundTypes.Final)
            {
                round.Competition.State = CompetitionState.Finished;
                round.State = CompetitionState.Finished;
            }

            this.context.SaveChanges();
        }

        #endregion

        #region Methods

        protected override void SetViewModel()
        {
        }

        private void BtnExportEjudge_OnClick(object sender, RoutedEventArgs e)
        {
            if (this.Competition.Section.Id == 3)
            {
                this.ExportEjudgeData("S", 1, 5);
                this.ExportEjudgeData("L", 6, 10);
            }
            else
            {
                this.ExportEjudgeData("", 1, 999);
            }
        }

        private void BtnExportWDSG_Click(object sender, RoutedEventArgs e)
        {
            var task = new Task(
                () =>
                    {
                        // we export the last round
                        var round = this.GetCurrentRound();
                        if (round.Number == 1)
                        {
                            // This is first round, we only export qualified of this round
                            ExportWDSG.ExportOverall(Settings.Default.WDSGExportFolder, round, this.context);
                            return;
                        }
                        var sendRound =
                            this.context.Rounds.SingleOrDefault(
                                r => r.Competition.Id == round.Competition.Id && r.Number == round.Number - 1);

                        ExportWDSG.ExportWDSGFile(this.context, sendRound.Id, Settings.Default.WDSGExportFolder);
                    });
            task.Start();
        }

        private void BtnWdsfApi_OnClick(object sender, RoutedEventArgs e)
        {
            var wdsfApiClient = new WdsfApiClient(this.context, this.Competition);
            wdsfApiClient.SetStatus("InProgress");
            // Upload asynchron
            // we send the previous round here ...
            var roundToSend =
                this.context.Rounds.SingleOrDefault(
                    r => r.Competition.Id == this.competitionId && r.Number == this.CurrentRound.Number - 1);
            if (roundToSend == null)
            {
                return;
            }

            var task = new Task(
                () =>
                    {
                        var clearData = roundToSend.Number == 1
                                         || roundToSend.Number == 2
                                         && roundToSend.Competition.Rounds.Any(
                                             r => r.RoundType.Id == RoundTypes.Redance);

                        if (clearData)
                        {
                            wdsfApiClient.ClearAllParticipants();
                            foreach (var qualified in roundToSend.Qualifieds)
                            {
                                qualified.Participant.ExternalId = null;
                            }
                        }

                        wdsfApiClient.SendResults(roundToSend);
                    });
            task.Start();
            task.ContinueWith(this.CheckErrorResult);
        }

        private void CalculatePointsAndPlaces()
        {
            // Für jedes Paar berechnen wir für die 4 Wertungsgebiete die Werte und dann die Summe über alles
            var components =
                this.Competition.CompetitionType.JudgingComponents.OrderBy(o => o.Order).ToList();
            var round = this.context.Rounds.Single(r => r.Id == this.currentRoundId);

            this.TotalGrid.ItemsSource =
                PointPlaceCalculatorFactory.GetPointPlaceCalculator(NewJudgingSystemCaluclationType.JS3, round.Js3CutOffValue)
                    .CalculatePointsAndPlaces(this.context, round, components);
        }

        private void CheckErrorResult(Task task)
        {
            if (task.Exception != null)
            {
                var exception = (Exception)task.Exception;
                while (exception.InnerException != null)
                {
                    exception = exception.InnerException;
                }

                MainWindow.MainWindowHandle.ShowMessage(exception.Message);
            }
        }

        /// <summary>
        ///     Exports the ejudge data.
        /// </summary>
        /// <param name="section">The section to write</param>
        /// <param name="minDanceId">The minimum dance identifier.</param>
        /// <param name="maxDanceId">The maximum dance identifier.</param>
        private void ExportEjudgeData(string section, int minDanceId, int maxDanceId)
        {
            var path = Settings.Default.NewJudgingSystemInputFolder
                          + String.Format(
                              "\\{0}{2}_{1}",
                              this.Competition.Id,
                              this.CurrentRound.RoundType.IsFinal ? "F" : this.GetRoundNumber(),
                              section);

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            this.WriteJsConfiguration(path);
            this.WriteXmlData(path, minDanceId, maxDanceId, section);
        }

        private void GenerateDataGrid(DataGrid DanceGrid, DanceInRound dance_Round)
        {
            var comp = this.context.Competitions.Single(c => c.Id == this.competitionId);
            var judges = comp.GetJudges();

            DanceGrid.Columns.Add(
                new DataGridTextColumn { Header = "No.", Binding = new Binding("Participant.Number") });

            var converter = new ViewModelConverter();

            foreach (var judge in judges)
            {
                var column = new DataGridTextColumn
                                 {
                                     Header = judge.Sign,
                                     Binding =
                                         new Binding("Marking")
                                             {
                                                 Converter = converter,
                                                 ConverterParameter =
                                                     new BindingParameter
                                                         {
                                                             Dance =
                                                                 dance_Round
                                                                 .Dance,
                                                             Judge =
                                                                 judge
                                                         }
                                             }
                                 };
                DanceGrid.Columns.Add(column);
            }
        }

        private void GenerateViewData(Round round)
        {
            this._viewModel = new List<NewJudgingMarkingViewModel>();
            var qualified = round.Qualifieds;
            var counter = 0;

            this.context.Configuration.AutoDetectChangesEnabled = false;

            foreach (var qual in qualified)
            {
                MainWindow.MainWindowHandle.ReportStatus("Generatring View - Data", counter, qualified.Count);
                counter++;

                var coupleMarking = new NewJudgingMarkingViewModel
                                        {
                                            Participant = qual.Participant,
                                            Marking =
                                                new Dictionary
                                                <Dance, Dictionary<Official, Marking>>()
                                        };
                this._viewModel.Add(coupleMarking);

                // Add a Marking per Dance and Judge for this qualified
                foreach (var dance in round.DancesInRound)
                {
                    var judge_marking = new Dictionary<Official, Marking>();
                    coupleMarking.Marking.Add(dance.Dance, judge_marking);
                    var judges = round.Competition.GetJudges();
                    foreach (var judge in judges)
                    {
                        var marking =
                            this.context.Markings.SingleOrDefault(
                                m =>
                                m.Round.Id == round.Id && m.Participant.Id == qual.Participant.Id
                                && m.Judge.Id == judge.Id && m.Dance.Id == dance.Dance.Id);
                        if (marking == null)
                        {
                            marking = new Marking(qual.Participant, round, dance.Dance, judge)
                                          {
                                              Round = round,
                                              Participant = qual.Participant,
                                              Dance = dance.Dance,
                                              Judge = judge,
                                              Mark20 = 0,
                                          };
                            this.context.Markings.Add(marking);
                        }
                        judge_marking.Add(judge, marking);
                    }
                }
            }
            MainWindow.MainWindowHandle.ReportStatus("Saving...", 0, 100);
            this.context.ChangeTracker.DetectChanges();
            this.context.SaveChanges();
            this.context.Configuration.AutoDetectChangesEnabled = true;
            Debug.WriteLine("Saving data ... done ");
        }

        private void ImportData_OnClick(object sender, RoutedEventArgs e)
        {
            if (!Directory.Exists(this.ImportPath.Text))
            {
                return;
            }

            var files = Directory.GetFiles(this.ImportPath.Text);

            var task = new Task(
                delegate
                    {
                        try
                        {
                            var resultfiles = files.Where(f => f.Contains("Result_")).ToList();
                            var counter = 0;
                            foreach (var file in resultfiles)
                            {
                                counter++;
                                MainWindow.MainWindowHandle.ReportStatus("Reading File", counter, resultfiles.Count);
                                this.ImportFile(file);
                            }

                            MainWindow.MainWindowHandle.ReportStatus("Calculating Places", 0, 100);

                            this.Dispatcher.Invoke(
                                new Action(
                                    () =>
                                        {
                                            foreach (var viewModel in this._viewModel)
                                            {
                                                viewModel.RaiseMarkingEvent();
                                            }

                                            this.CalculatePointsAndPlaces();

                                        }));
                            MainWindow.MainWindowHandle.ReportStatus("", 0, 100);
                        }
                        catch (Exception ex)
                        {
                            MainWindow.MainWindowHandle.ShowMessage("Could not import file :" + ex.Message);
                        }
                    });
            task.Start();
        }

        private void ImportFile(string file)
        {
            var markNotFound = false;
            var reader = new StreamReader(file);
            var lastError = "";

            while (!reader.EndOfStream)
            {
                var data = reader.ReadLine().Split(';');
                var danceShortName = data[1];
                var judgeSign = data[0];
                var dance = this.context.Dances.Single(d => d.ShortName == danceShortName);
                var judge = this.context.Officials.Single(o => o.Sign == judgeSign);
                var number = Int32.Parse(data[2]);
                var area = Int32.Parse(data[3]);

                var mark = data[4].SaveParse();

                // Set the value in our ViewData:
                var marks = this._viewModel.SingleOrDefault(v => v.Participant.Number == number);
                if (marks != null)
                {
                    if (!marks.Marking.ContainsKey(dance))
                    {
                        lastError = LocalizationService.Resolve(() => Text.DanceNotFound);
                        continue;
                    }

                    var markssdance = marks.Marking[dance];

                    switch (area)
                    {
                        case 1:
                            markssdance[judge].MarkA = mark;
                            break;
                        case 2:
                            markssdance[judge].MarkB = mark;
                            break;
                        case 3:
                            markssdance[judge].MarkC = mark;
                            break;
                        case 4:
                            markssdance[judge].MarkD = mark;
                            break;
                    }

                    markssdance[judge].Mark20 = mark;

                    markssdance[judge].MarkingComponent = area;
                }
                else
                {
                    markNotFound = true;
                }
            }

            if (lastError != "")
            {
                MainWindow.MainWindowHandle.ShowMessage(lastError);
            }

            this.context.SaveChanges();

            if (markNotFound)
            {
                MainWindow.MainWindowHandle.ShowMessage(LocalizationService.Resolve(() => Text.CouldNotFindAllCouples));
            }
        }

        private void WriteJsConfiguration(string path)
        {
            if (!Competition.CompetitionType.JudgingComponents.Any())
            {
                MainWindow.MainWindowHandle.ShowMessage(
                    "No components configures. Select the right competition type or enter components manually in the jsConfiguration.xml file");
            }

            var xmlDoc = new XmlDocument();

            var root = xmlDoc.CreateElement("JSConfiguration");
            xmlDoc.AppendChild(root);

            var version = xmlDoc.CreateElement("JSVersion");
            version.SetAttribute("Version", "JS3");
            version.SetAttribute(
                "IsFormation",
                this.Competition.Section.Id == 6 || this.Competition.Section.Id == 7 ? "true" : "false");
            version.SetAttribute("Section", this.Competition.Section.SectionName);
            root.AppendChild(version);

            var judgingData = xmlDoc.CreateElement("JudgingData");
            judgingData.SetAttribute("MinScore", "0");
            judgingData.SetAttribute("MaxScore", "10");
            judgingData.SetAttribute("Intervall", "0.25");
            judgingData.SetAttribute("CutOff", this.CurrentRound.Js3CutOffValue.ToString());
            root.AppendChild(judgingData);

            var components = xmlDoc.CreateElement("Components");
            root.AppendChild(components);

            foreach (
                var component in this.Competition.CompetitionType.JudgingComponents.OrderBy(o => o.Order))
            {
                var xmlComponent = xmlDoc.CreateElement("Component");
                xmlComponent.SetAttribute("ShortName", component.Code);
                xmlComponent.SetAttribute("Name", component.Code);
                xmlComponent.SetAttribute("Weight", component.Weight.ToString());
                xmlComponent.InnerText = component.Description;
                components.AppendChild(xmlComponent);
            }

            var stream = new StreamWriter(path + "\\JsConfiguration.xml");
            var xmlWriter = new XmlTextWriter(stream);
            xmlWriter.Formatting = Formatting.Indented;
            xmlWriter.IndentChar = '\t';
            xmlWriter.Indentation = 1;

            xmlDoc.WriteContentTo(xmlWriter);

            stream.Close();
        }

        private void WriteXmlData(string path, int minDanceId, int maxDanceId, string folderPostfix)
        {
            var xmlDoc = new XmlDocument();

            var root = xmlDoc.CreateElement("Data");
            xmlDoc.AppendChild(root);

            var eventData = xmlDoc.CreateElement("Event");
            root.AppendChild(eventData);

            eventData.SetAttribute("Id", this.Competition.Id.ToString() + folderPostfix);
            eventData.SetAttribute("WdsfId", this.Competition.ExternalId);
            eventData.SetAttribute("Section", this.Competition.Section.SectionName);
            eventData.AppendChild(
                this.createXmlElement(
                    xmlDoc,
                    "RoundNumber",
                    this.GetRoundNumber()));
            eventData.AppendChild(
                this.createXmlElement(
                    xmlDoc,
                    "IsFinal",
                    this.Competition.CurrentRound.RoundType.IsFinal ? "true" : "false"));
            eventData.AppendChild(this.createXmlElement(xmlDoc, "Title", this.Competition.Title));
            eventData.AppendChild(
                this.createXmlElement(xmlDoc, "PlaceOffset", this.CurrentRound.PlaceOffset.ToString()));

            var dances = xmlDoc.CreateElement("Dances");
            root.AppendChild(dances);

            foreach (
                var danceInRound in
                    this.CurrentRound.DancesInRound.Where(d => d.Dance.Id >= minDanceId && d.Dance.Id <= maxDanceId)
                        .OrderBy(d => d.SortOrder))
            {
                var danceXml = xmlDoc.CreateElement("Dance");
                dances.AppendChild(danceXml);
                danceXml.SetAttribute("ShortName", danceInRound.Dance.ShortName);
                danceXml.SetAttribute("LongName", danceInRound.Dance.DanceName);
            }

            var judges = xmlDoc.CreateElement("Judges");
            root.AppendChild(judges);

            foreach (var official in this.Competition.GetJudges().OrderBy(j => j.Sign))
            {
                var xmlOfficial = xmlDoc.CreateElement("Judge");
                judges.AppendChild(xmlOfficial);
                xmlOfficial.SetAttribute("Name", official.NiceName);
                xmlOfficial.SetAttribute("Country", official.Club);
                xmlOfficial.SetAttribute("MIN", official.MIN.HasValue ? official.MIN.ToString() : "");
                xmlOfficial.SetAttribute("Sign", official.Sign);
                xmlOfficial.SetAttribute("Pin", official.PinCode ?? "");
                xmlOfficial.SetAttribute("WdsfSign", official.Sign);
            }

            var couples = xmlDoc.CreateElement("Couples");
            root.AppendChild(couples);

            foreach (var qualified in this.CurrentRound.Qualifieds)
            {
                var couple = xmlDoc.CreateElement("Couple");
                couples.AppendChild(couple);

                var nameStr = "";
                if (qualified.Participant.Couple is Team)
                {
                    var team = qualified.Participant.Couple as Team;
                    nameStr = string.Format("{0}|{1}|{2}", team.TeamName, team.Theme, team.Trainer);
                }
                else
                {
                    nameStr = qualified.Participant.Couple.FirstMan + "_" + qualified.Participant.Couple.LastMan + "/"
                              + qualified.Participant.Couple.FirstWoman + "_" + qualified.Participant.Couple.LastWoman;
                }

                var country =
                    this.context.CountryCodes.SingleOrDefault(c => c.LongName == qualified.Participant.Couple.Country);

                couple.SetAttribute("Name", nameStr);
                couple.SetAttribute("Country", country != null ? country.IocCode : qualified.Participant.Couple.Country);
                // todo: change back to MIN!!!
                couple.SetAttribute("MIN", qualified.Participant.Couple.Team_Id);
                couple.SetAttribute("Number", qualified.Participant.Number.ToString());
            }

            var heats = HeatsHelper.GetHeats(
                this.context,
                this.currentRoundId);
            if (heats.Keys.Count > 0)
            {
                var heatsXml = xmlDoc.CreateElement("Heats");
                root.AppendChild(heatsXml);

                foreach (
                    var danceRound in
                        this.CurrentRound.DancesInRound.Where(d => d.Dance.Id >= minDanceId && d.Dance.Id <= maxDanceId)
                            .OrderBy(d => d.SortOrder))
                {
                    var key = heats.Keys.Single(d => d.Dance.Id == danceRound.Dance.Id);
                    var heatsOfDance = heats[key];

                    foreach (var heat in heatsOfDance)
                    {
                        var numbers = heat.Select(h => h.Number).ToList();
                        var heatString = string.Join(",", numbers);
                        var heatXml = xmlDoc.CreateElement("Heat");
                        heatsXml.AppendChild(heatXml);
                        heatXml.SetAttribute("Couples", heatString);
                        heatXml.SetAttribute("Dance", danceRound.Dance.ShortName);
                    }
                }
            }

            var stream = new StreamWriter(path + "\\data.xml");
            var xmlWriter = new XmlTextWriter(stream);
            xmlWriter.Formatting = Formatting.Indented;
            xmlWriter.IndentChar = '\t';
            xmlWriter.Indentation = 1;

            xmlDoc.WriteContentTo(xmlWriter);

            stream.Close();
        }

        private string GetRoundNumber()
        {
            if (this.CurrentRound.RoundType.Id == RoundTypes.Redance)
            {
                return "R";
            }

            if (this.CurrentRound.RoundType.IsFinal)
            {
                return "F";
            }

            if (this.Competition.Rounds
                .Where(r => r.Number < this.CurrentRound.Number && r.RoundType.Id == RoundTypes.Redance).Any())
            {
                return (this.CurrentRound.Number - 1).ToString();
            }

            return this.CurrentRound.Number.ToString();

        }

        private XmlElement createXmlElement(XmlDocument doc, string name, string value)
        {
            var element = doc.CreateElement(name);
            element.InnerText = value;

            return element;
        }

        #endregion
    }
}