﻿// // TPS.net TPS8 Scrutinus
// // DefineHeat.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using DataModel;
using DataModel.ModelHelper;
using General.Drawing;
using Scrutinus.Localization;
using Scrutinus.ViewModels;

namespace Scrutinus.Pages
{
    public partial class DefineHeat : ScrutinusPage
    {
        #region Constructors and Destructors

        public DefineHeat(int competitionId, int roundId)
            : base(competitionId, roundId)
        {
            this.InitializeComponent();
            this.viewModel = (DefineHeatViewModel)this.ViewModel;
            this.DataContext = this.viewModel;

            this.numberOfCouplesQualified = this.context.Qualifieds.Count(q => q.Round.Id == roundId);
        }

        #endregion

        #region Fields

        private readonly int numberOfCouplesQualified;

        private readonly DefineHeatViewModel viewModel;

        #endregion

        #region Public Methods and Operators

        public override bool AllowToLeave()
        {
            return true;
        }

        public override bool CanMoveNext()
        {
            if (!this.CheckCanMoveOn())
            {
                return false;
            }

            return true;
        }

        protected override void OnNextInternal()
        {
            var viewModel = this.ViewModel as DefineHeatViewModel;

            viewModel.SaveData();

            IRoundDrawer drawer = null;

            if (viewModel.Round.DrawingType != null)
            {
                // Create a Round Drawing
                switch (viewModel.Round.DrawingType.Value)
                {
                    case DrawingTypes.Random:
                    case DrawingTypes.FixedPerDance:
                        drawer = new RandomDrawer();
                        break;
                    case DrawingTypes.FixedHeatsAsc:
                        drawer = new OrderedDrawer(true);
                        break;
                    case DrawingTypes.FixedHeatsDesc:
                        drawer = new OrderedDrawer(false);
                        break;
                    case DrawingTypes.GrandSlamFinal:
                        drawer = new GrandSlamDrawing();
                        break;
                }

                if (drawer == null)
                {
                    MainWindow.MainWindowHandle.ShowMessage(
                        LocalizationService.Resolve(() => Text.DrawingNotImplemented));
                    return;
                }
            }

            var participants = this.CurrentRound.Qualifieds.Select(q => q.Participant).ToList();

            if (viewModel.DrawingExists)
            {
                // just save and continue, do not create a new drawing.
                this.context.SaveChanges();
                return;
            }

            if (drawer != null)
            {
                var drawing = drawer.CreateDrawing(
                    participants,
                    viewModel.Round.DrawingType == DrawingTypes.FixedPerDance
                        ? null
                        : viewModel.Round.DancesInRound.ToList(),
                    viewModel.NumberOfHeats);
                // befor saving the new data remove any drawing we might have ...
                // _context.Drawings.Delete(d => d.RoundId == _round.Id);
                foreach (
                    var drawingObj in this.context.Drawings.Where(r => r.Round.Id == viewModel.Round.Id).ToList())
                {
                    try
                    {
                        this.context.Drawings.Remove(drawingObj);
                    }
                    catch (Exception)
                    {
                        // throw;
                    }
                }

                this.context.SaveChanges();
                // we have to save the drawing to our database
                HeatsHelper.SaveHeats(this.context, drawing, viewModel.Round.DancesInRound, viewModel.Round.Id);
            }

            this.context.SaveChanges();
        }

        #endregion

        #region Methods

        protected override void SetViewModel()
        {
            this.ViewModel = new DefineHeatViewModel(this.context, this.currentRoundId);
        }

        private bool CheckCanMoveOn()
        {
            return this.ViewModel.Validate();
        }

        private void CouplesPerHeat_TextChanged(object sender, TextChangedEventArgs e)
        {
            // todo: move to viewmodel.
            if (this.NumberHeatString == null)
            {
                return;
            }

            var numberOfHeats = 0;

            if (!Int32.TryParse(this.NumberOfHeats.Text, out numberOfHeats))
            {
                return;
            }

            var coupleCount = 0;

            if (numberOfHeats > 0)
            {
                coupleCount = this.numberOfCouplesQualified / numberOfHeats;
                if (this.numberOfCouplesQualified % numberOfHeats > 0)
                {
                    coupleCount++;
                }

                this.NumberHeatString.Text =
                    String.Format(LocalizationService.Resolve(() => Text.NumberOfCouplesPerHeat), coupleCount);
            }
        }

        private void MarksManual_OnChecked(object sender, RoutedEventArgs e)
        {
            this.MarksFrom.Visibility = Visibility.Visible;
            this.MarksTo.Visibility = Visibility.Visible;
        }

        private void MarksManual_OnUnchecked(object sender, RoutedEventArgs e)
        {
            this.MarksFrom.Visibility = Visibility.Hidden;
            this.MarksTo.Visibility = Visibility.Hidden;
        }

        private void btnContinue_Click(object sender, RoutedEventArgs e)
        {
            if (!this.ViewModel.Validate())
            {
                return;
            }

            MainWindow.MainWindowHandle.NextState(this.competitionId);
        }

        #endregion
    }
}