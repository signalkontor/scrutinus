﻿// // TPS.net TPS8 Scrutinus
// // Printing.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Linq;
using System.Windows;
using DataModel;
using DataModel.Models;
using MahApps.Metro.Controls.Dialogs;
using Scrutinus.PrintModels;
using Scrutinus.Reports;

namespace Scrutinus.Pages
{
    /// <summary>
    ///     Interaction logic for Printing.xaml
    /// </summary>
    public partial class Printing : ScrutinusPage
    {
        #region Constructors and Destructors

        public Printing(int competitionId, int roundId)
            : base(competitionId, roundId)
        {
            this.InitializeComponent();
        }

        #endregion

        #region Fields

        private PrintingViewModel model;

        private string printConfiguration;

        #endregion

        #region Public Methods and Operators

        public override bool AllowToLeave()
        {
            return true;
        }

        public override bool CanMoveNext()
        {
            return true;
        }

        protected override void OnNextInternal()
        {
            // Save the current settings:
            this.model.SaveConfiguration(this.printConfiguration);
#if DTV
            if (this.model.Laufzettel.Print)
            {
                PrintLaufzettel(false, this.CurrentRound, this.model);
            }
#endif
        }

        #endregion

        #region Methods

        protected override void SetViewModel()
        {
            this.model = new PrintingViewModel();

            // depending on the current round type and state, we print different reports
            if (this.CurrentRound.RoundType.Id == RoundTypes.QualificationRound)
            {
                this.printConfiguration = "PrintingQualificationRound";
            }
            else if (this.CurrentRound.RoundType.IsFinal)
            {
                if (this.CurrentRound.Qualifieds.All(q => q.Participant.State == CoupleState.DroppedOut))
                {
                    this.printConfiguration = "PrintingAfterFinal";
                }
                else
                {
                     this.printConfiguration = "PrintingBeforeFinal";
                }
            }
            else
            {
                this.printConfiguration = "Printing";
            }

            this.model.LoadSettings(this.printConfiguration);

            // enable all the ones that are generally enabled:
            var previousRound =
                this.context.Rounds.SingleOrDefault(
                    r =>
                    r.Competition.Id == this.CurrentRound.Competition.Id && r.Number == this.CurrentRound.Number - 1);

            if (this.CurrentRound.Competition.State == CompetitionState.Finished)
            {
                previousRound =
                    this.context.Rounds.SingleOrDefault(
                        r => r.Competition.Id == this.CurrentRound.Competition.Id && r.RoundType.Id == RoundTypes.Final);
            }

            this.model.Qualified.Enabled = true;
            this.model.EmptyJudgingSheets.Enabled = true;
            
            this.model.MarkingLastRound.Enabled = this.CurrentRound.Number > 1;
            this.model.CoupleState.Enabled = true;
            this.model.WinnerCertificate.Enabled = true;
            this.model.Officals.Enabled = true;

            this.model.Laufzettel.Enabled = previousRound != null
                                            && previousRound.Qualifieds.Any(
                                                q =>
                                                q.Participant.State == CoupleState.DroppedOut
                                                && q.Participant.Startbuch != null
                                                && q.Participant.Startbuch.PrintLaufzettel
                                                && !q.Participant.Startbuch.IsSelfGenerated);
            this.model.Laufzettel.Print = this.model.Laufzettel.Enabled;
            // Set Default values for ViewData
            // No Drawings in Finals:
            if (this.GetCurrentRound().RoundType.Id == RoundTypes.QualificationRound
                || this.GetCurrentRound().RoundType.Id == RoundTypes.Redance
                || this.GetCurrentRound().RoundType.Id == RoundTypes.SecondFirstRound)
            {
                this.model.FinalResult.Enabled = false;
                this.model.FinalSkatingTable.Enabled = false;

                this.model.Drawing.Enabled = true;
                this.model.DrawingCouples.Enabled = true;
                this.model.RoundWithNames.Enabled = true;
            }

            if (this.CurrentRound.RoundType.Id == RoundTypes.Final)
            {
                // is the final finished?
                if (this.CurrentRound.Qualifieds.All(p => p.Participant.PlaceFrom > 0))
                {
                    this.model.FinalResult.Enabled = true;
                }
                else
                {
                    this.model.FinalResult.Enabled = false;
                    this.model.FinalSkatingTable.Enabled = false;
                    this.model.FinalTable.Enabled = false;
                }
            }

            if (this.GetCurrentRound().DrawingType == null)
            {
                this.model.Drawing.Enabled = false;
                this.model.DrawingCouples.Enabled = false;
                this.model.RoundWithNames.Enabled = false;
            }

            this.model.SeededCouples.Enabled = this.Competition.Stars > 0;

            this.model.SelectedRound = this.context.Rounds.Single(r => r.Id == this.currentRoundId);
            // we have to enable dropped out if we had a last round
            this.model.CouplesDroppedOut.Enabled = this.CurrentRound.Number > 1;

            this.ViewModel = this.model;
            this.DataContext = this.ViewModel;
        }

        private void BtnContinue_OnClick(object sender, RoutedEventArgs e)
        {
            MainWindow.MainWindowHandle.NextState();
        }

        private void BtnPrint_OnClick(object sender, RoutedEventArgs ea)
        {
            this.Print(false);
            var model = this.DataContext as PrintingViewModel;

            model.Laufzettel.Print = false;
        }

        private void PreviewButton_OnClick(object sender, RoutedEventArgs e)
        {
            this.Print(true);
        }

        private void Print(bool preview)
        {
            var model = this.ViewModel as PrintingViewModel;

            if (model.CouplesDroppedOut.Print || model.Laufzettel.Print)
            {
                // Handle special: Print data from last round...
                Round printRound;
                if (this.Competition.State == CompetitionState.Finished)
                {
                    printRound = this.Competition.CurrentRound;
                }
                else
                {
                    printRound =
                        this.context.Rounds.SingleOrDefault(
                            r => r.Competition.Id == this.Competition.Id && r.Number == this.CurrentRound.Number - 1);
                }

                if (printRound != null && model.CouplesDroppedOut.Print)
                {
                    PrintHelper.SourcePage = this;
                    PrintHelper.PrintDropedOut(
                        this.context,
                        printRound,
                        model.CouplesDroppedOut.Printer,
                        model.CouplesDroppedOut.Copies,
                        preview);
                    model.CouplesDroppedOut.Print = false; // don't print again ...
                }

                if (printRound != null && model.Laufzettel.Print)
                {
                    PrintLaufzettel(preview, printRound, model);
                }
            }

            PrintHelper.SourcePage = this;
            PrintHelper.DoPrintJob(this.context, this.GetCompetition(), this.GetCurrentRound(), model, preview);
        }

        private static void PrintLaufzettel(bool preview, Round printRound, PrintingViewModel model)
        {
            var res =
                MainWindow.MainWindowHandle.ShowMessage(
                    "Es müssen Laufzettel gedruckt werden. Laufzettel jetzt drucken?",
                    "Laufzetteldruck",
                    MessageDialogStyle.AffirmativeAndNegative);

            if (res == MessageDialogResult.Affirmative)
            {
                PrintHelper.PrintLaufzettel(
                    printRound,
                    model.Laufzettel.Printer,
                    model.Laufzettel.Copies,
                    preview);
            }

            model.Laufzettel.Print = false; // don't print again ...
        }

        #endregion
    }
}