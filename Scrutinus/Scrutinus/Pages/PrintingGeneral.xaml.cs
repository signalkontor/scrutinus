﻿// // TPS.net TPS8 Scrutinus
// // PrintingGeneral.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Linq;
using System.Windows;
using DataModel;
using Scrutinus.Dialogs;
using Scrutinus.Localization;
using Scrutinus.PrintModels;
using Scrutinus.Reports;

namespace Scrutinus.Pages
{
    /// <summary>
    ///     Interaction logic for PrintingGeneral.xaml
    /// </summary>
    public partial class PrintingGeneral : ScrutinusPage
    {
        #region Fields

        private readonly PrintingViewModel viewModel;

        #endregion

        #region Constructors and Destructors

        public PrintingGeneral(int competitionId, int roundId)
            : base(competitionId, roundId)
        {
            this.InitializeComponent();

            this.viewModel = this.DataContext as PrintingViewModel;

            if (this.CurrentRound != null)
            {
                this.PrintingTabControl.SelectedIndex = 1;
            }
            else if (this.Competition != null)
            {
                this.PrintingTabControl.SelectedIndex = 0;
            }
            else
            {
                this.PrintingTabControl.SelectedIndex = 2;
            }
        }

        #endregion

        #region Public Methods and Operators

        public override VerticalAlignment FrameVerticalContentAllignment => VerticalAlignment.Top;

        public override bool AllowToLeave()
        {
            return this.viewModel.Validate();
        }

        public override bool CanMoveNext()
        {
            return false;
        }

        protected override void OnNextInternal()
        {
            MainWindow.MainWindowHandle.ShowMessage(LocalizationService.Resolve(() => Text.PrintingCanNotMoveNext));
        }

        #endregion

        #region Methods

        protected override void SetViewModel()
        {
            var model = new PrintingViewModel();

            model.LoadSettings("PrintingGeneral");

            model.Officals.Enabled = true;
            model.RegisteredCouples.Enabled = true;
            model.SeededCouples.Enabled = this.Competition != null && this.Competition.Stars > 0;
            model.DTVAnnex.Enabled = true;
            model.DTVPart1.Enabled = this.Competition != null && this.Competition.State == CompetitionState.Finished;
            model.CoverPage.Enabled = true;
            model.ResultSortedByRegions.Enabled = true;
            model.EventCorrectedStartList.Enabled = true;
            model.CorrectedStartList.Enabled = true;

            if (this.CurrentRound != null)
            {
                var rounds =
                    this.context.Rounds.Where(r => r.Competition.Id == this.competitionId)
                        .OrderBy(r => r.Number)
                        .ToList();

                model.RoundSelectionList = rounds;
                model.CouplesDroppedOut.Enabled = true;
                model.CouplesResult.Enabled = true;
                model.CouplesResultSorted.Enabled = true;
                model.Drawing.Enabled = true;
                model.DrawingCouples.Enabled = true;
                model.FinalTable.Enabled = true;
                model.MarkingRound.Enabled = true;
                model.MarkingTableTotal.Enabled = true;
                model.RoundWithNames.Enabled = true;
                model.Qualified.Enabled = true;
                model.ResultService.Enabled = true;
                model.EmptyJudgingSheets.Enabled = true;
                model.WinnerCertificate.Enabled = true;
                model.FinalSkatingTable.Enabled = true;
                // Preselect to current Round
                model.SelectedRound = rounds.SingleOrDefault(r => r.Id == this.currentRoundId);
            }
            else
            {
                model.CouplesDroppedOut.Enabled = false;
                model.CouplesResult.Enabled = false;
                model.CouplesResultSorted.Enabled = false;
                model.Drawing.Enabled = false;
                model.DrawingCouples.Enabled = false;
                model.FinalTable.Enabled = false;
                model.MarkingRound.Enabled = false;
                model.MarkingTableTotal.Enabled = false;
                model.RoundWithNames.Enabled = false;
                model.Qualified.Enabled = false;
                model.ResultService.Enabled = false;
                model.EmptyJudgingSheets.Enabled = false;
                model.WinnerCertificate.Enabled = false;
                model.FinalSkatingTable.Enabled = false;
                model.ResultSortedByRegions.Enabled = false;
                model.FinalResult.Enabled = false;
                model.WdsfChecksums.Enabled = false;
            }

            if (this.Competition == null)
            {
                model.CorrectedStartList.Enabled = false;
                model.DTVAnnex.Enabled = false;
                model.CoupleState.Enabled = false;
                model.Officals.Enabled = false;
                model.SeededCouples.Enabled = false;
                model.MarkingTableTotal.Enabled = false;
                model.RegisteredCouples.Enabled = false;
                model.ResultSortedByRegions.Enabled = false;
                model.CoverPage.Enabled = false;
                model.Laufzettel.Enabled = false;
            }
            else
            {
                model.CorrectedStartList.Enabled = true;
                model.DTVAnnex.Enabled = true;
                model.CoupleState.Enabled = true;
                model.Officals.Enabled = true;
                model.SeededCouples.Enabled = true;
                model.MarkingTableTotal.Enabled = true;
                model.RegisteredCouples.Enabled = true;
                model.ResultSortedByRegions.Enabled = true;
                model.CoverPage.Enabled = true;
                model.Laufzettel.Enabled = true;
            }
            // We remove any checkboxes because in print general we do not want to remember what we printed:
            model.WithAllPrintDefinitions(p => p.Print = false);

            this.ViewModel = model;
            this.DataContext = this.ViewModel;
        }

        private void BtnContinue_OnClick(object sender, RoutedEventArgs e)
        {
            MainWindow.MainWindowHandle.GotoActiveCompetition();
        }

        private void BtnPrint_OnClick(object sender, RoutedEventArgs e)
        {
            var model = this.ViewModel as PrintingViewModel;
            model.SaveConfiguration("PrintingGeneral");

            var round = model.SelectedRound;

            var preview = sender == this.BtnPreView;

            if (model.Laufzettel.Print)
            {
                var dialog = new PrintLaufzettelDialog(
                    this.Competition,
                    model.Laufzettel.Printer,
                    model.Laufzettel.Copies);

                MainWindow.MainWindowHandle.ShowDialog(dialog, null, 300, 300);

                model.Laufzettel.Print = false;
            }

            PrintHelper.SourcePage = this;
            PrintHelper.DoPrintJob(
                this.context,
                this.GetCompetition(),
                round,
                (PrintingViewModel)this.ViewModel,
                preview);
        }

        #endregion
    }
}