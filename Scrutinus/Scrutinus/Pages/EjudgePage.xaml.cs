﻿// // TPS.net TPS8 Scrutinus
// // EjudgePage.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows.Controls;

namespace Scrutinus.Pages
{
    /// <summary>
    /// Interaction logic for EjudgePage.xaml
    /// </summary>
    public partial class EjudgePage : Page
    {
        public EjudgePage()
        {
            this.InitializeComponent();
        }
    }
}
