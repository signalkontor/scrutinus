﻿// // TPS.net TPS8 Scrutinus
// // CompetitionResult.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using DataModel.Export;
using Scrutinus.ViewModels;

namespace Scrutinus.Pages
{
    /// <summary>
    ///     Interaction logic for CompetitionResult.xaml
    /// </summary>
    public partial class CompetitionResult : ScrutinusPage
    {
        #region Fields

        private readonly CompetitionResultViewModel viewModel;

        #endregion

        #region Constructors and Destructors

        public CompetitionResult(int competitionId, int roundId)
            : base(competitionId, roundId)
        {
            this.InitializeComponent();
            this.viewModel = new CompetitionResultViewModel(roundId);
            this.DataContext = this.viewModel;
        }

        #endregion

        #region Public Methods and Operators

        public override bool AllowToLeave()
        {
            return true;
        }

        public override bool CanMoveNext()
        {
            return false; // we are done, nothing to go to now...
        }

        protected override void OnNextInternal()
        {
            // nothing to do
        }

        #endregion

        #region Methods

        protected override void SetViewModel()
        {
        }

        private void BtnExportWDSG_Click(object sender, RoutedEventArgs e)
        {
            var task = new Task(
                () =>
                    {
                        // Wir exportieren die letzte Runde
                        var round = this.GetCurrentRound();

                        // Evtl von dieser hier dann die Qualifizierten
                        ExportWDSG.ExportWDSGFile(this.context, round.Id, Settings.Default.WDSGExportFolder);
                    });
            task.Start();
        }

        private void BtnWDSFAPI_Click(object sender, RoutedEventArgs e)
        {
        }

        private void Results_BeginningEdit(object sender, DataGridBeginningEditEventArgs e)
        {
            e.Cancel = true;
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            this.context.SaveChanges();
        }

        #endregion
    }
}