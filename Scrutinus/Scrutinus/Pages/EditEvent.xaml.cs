﻿// // TPS.net TPS8 Scrutinus
// // EditEvent.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Markup;
using DataModel.Models;

namespace Scrutinus.Pages
{
    /// <summary>
    ///     Interaction logic for EditEvent.xaml
    /// </summary>
    public partial class EditEvent : ScrutinusPage
    {
        #region Constructors and Destructors

        public EditEvent()
            : base(0, 0)
        {
            this.Language = XmlLanguage.GetLanguage(Thread.CurrentThread.CurrentCulture.Name);

            this.InitializeComponent();

            if (!this.context.Events.Any())
            {
                var ev = new Event { Title = "" };
                this.context.Events.Add(ev);

                foreach (var competition in this.context.Competitions)
                {
                    competition.Event = ev;
                }

                this.context.SaveChanges();

                this.DataContext = ev;
            }
            else
            {
                this.DataContext = this.context.Events.First();
            }
        }

        #endregion

        #region Public Methods and Operators

        public override bool AllowToLeave()
        {
            return true;
        }

        public override bool CanMoveNext()
        {
            return true;
        }

        protected override void OnNextInternal()
        {
        }

        #endregion

        #region Methods

        protected override void SetViewModel()
        {
        }

        private void BtnSave_OnClick(object sender, RoutedEventArgs e)
        {
            this.context.SaveChanges();

            MainWindow.MainWindowHandle.CloseDialog();
        }

        private void CancelButtonClick(object sender, RoutedEventArgs e)
        {
            MainWindow.MainWindowHandle.CloseDialog();
        }

        #endregion
    }
}