﻿// // TPS.net TPS8 Scrutinus
// // AssignJudgesToCompetiton.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows;

namespace Scrutinus.Pages
{
    /// <summary>
    ///     Interaction logic for AssignJudgesToCompetiton.xaml
    /// </summary>
    public partial class AssignJudgesToCompetiton : ScrutinusPage
    {
        #region Constructors and Destructors

        public AssignJudgesToCompetiton(int competitionId, int CurrentRoundId)
            : base(0, 0)
        {
            this.InitializeComponent();
        }

        #endregion

        #region Methods

        protected override void SetViewModel()
        {
            this.DataContext = this.ViewModel;
        }

        #endregion

        #region Public Methods and Operators

        public override VerticalAlignment FrameVerticalContentAllignment => VerticalAlignment.Top;

        public override bool AllowToLeave()
        {
            return true;
        }

        public override bool CanMoveNext()
        {
            return true;
        }

        protected override void OnNextInternal()
        {
        }

        #endregion
    }
}