﻿// // TPS.net TPS8 Scrutinus
// // ProjectorControl.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Scrutinus.ViewModels;

namespace Scrutinus.Pages
{
    /// <summary>
    /// Interaction logic for ProjectorControl.xaml
    /// </summary>
    public partial class ProjectorControl : Page, IDisposable
    {
        private ProjectorControlViewModel viewModel;

        public ProjectorControl()
        {
            this.InitializeComponent();

            if (this.DataContext is ProjectorControlViewModel viewModel)
            {
                this.viewModel = viewModel;
                viewModel.LeftWebBrowser = this.LeftBrowser;
                viewModel.RightWebBrowser = this.RightBrowser;
            }
        }

        private void ButtonRight_OnClick(object sender, RoutedEventArgs e)
        {
            this.Slider.Value = 0;
        }

        private void ButtonLeft_OnClick(object sender, RoutedEventArgs e)
        {
            this.Slider.Value = 1;
        }

        private void Slider_OnValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            viewModel.CurrentSliderValue = e.NewValue;

            if (e.NewValue < 0.5)
            {
                BorderRight.BorderBrush = Brushes.DarkGreen;
                BorderLeft.BorderBrush = Brushes.Red;
            }
            else
            {
                BorderRight.BorderBrush = Brushes.Red;
                BorderLeft.BorderBrush = Brushes.DarkGreen;
            }
        }


        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    this.viewModel.Dispose();
                }

                disposedValue = true;
            }
        }


        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
