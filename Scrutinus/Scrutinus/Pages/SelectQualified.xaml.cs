﻿// // TPS.net TPS8 Scrutinus
// // SelectQualified.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows.Input;
using Scrutinus.ViewModels;

namespace Scrutinus.Pages
{
    /// <summary>
    ///     Interaction logic for SelectQualified.xaml
    /// </summary>
    public partial class SelectQualified : ScrutinusPage
    {
        #region Constructors and Destructors

        public SelectQualified(int competitionId, int roundId)
            : base(competitionId, roundId)
        {
            this.InitializeComponent();
            this.DataContext = this.ViewModel;
        }

        #endregion

        #region Public Methods and Operators

        public override bool AllowToLeave()
        {
            return true;
        }

        public override bool CanMoveNext()
        {
            return this.ViewModel.Validate();
        }

        protected override void OnNextInternal()
        {
            this.ViewModel.SaveData();
        }

        #endregion

        #region Methods

        protected override void SetViewModel()
        {
            this.ViewModel = new SelectQualifiedViewModel(this.currentRoundId);
        }

        private void Couples_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var model = (SelectQualifiedViewModel)this.ViewModel;
            model.SelectCouplesNextRound();
        }

        #endregion
    }
}