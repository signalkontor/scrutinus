﻿// // TPS.net TPS8 Scrutinus
// // EnterMarkingSumOnly.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using DataModel.Models;
using GalaSoft.MvvmLight;
using General.Extensions;
using Scrutinus.Controls;
using Scrutinus.ViewModels;

namespace Scrutinus.Pages
{
    public class CoupleMark
    {
        #region Public Properties

        public Marking Mark { get; set; }

        public string Number { get; set; }

        #endregion
    }

    public class MarkingViewModel : ViewModelBase
    {
        #region Fields

        private ObservableCollection<Marking> couplesMarks;

        private double totalPoints;

        private readonly CalculationMode calculationMode;

        public MarkingViewModel(CalculationMode calculationMode)
        {
            this.calculationMode = calculationMode;
        }

        #endregion

        #region Public Properties

        public ObservableCollection<Marking> CouplesMarks
        {
            get
            {
                return this.couplesMarks;
            }
            set
            {
                this.couplesMarks = value;
                this.couplesMarks.CollectionChanged += this.CouplesMarksOnCollectionChanged;
                this.MarkingOnPropertyChanged(this, null);
            }
        }

        // public string JudgeSign { get; set; }

        public Official Judge { get; set; }

        public double TotalPoints
        {
            get
            {
                return this.totalPoints;
            }
            set
            {
                this.totalPoints = value;
                this.RaisePropertyChanged(() => this.TotalPoints);
            }
        }

        #endregion

        #region Methods

        private void CouplesMarksOnCollectionChanged(
            object sender,
            NotifyCollectionChangedEventArgs notifyCollectionChangedEventArgs)
        {
            foreach (var newItem in notifyCollectionChangedEventArgs.NewItems)
            {
                var marking = newItem as Marking;
                marking.PropertyChanged += this.MarkingOnPropertyChanged;
                if (marking.Mark != 0 || marking.MarkA != 0)
                {
                    this.MarkingOnPropertyChanged(marking, null);
                }
            }
        }

        private void MarkingOnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            if (this.calculationMode == CalculationMode.Marks)
            {
                this.TotalPoints = this.CouplesMarks.Sum(s => s.Mark);
            }
            else
            {
                this.TotalPoints = this.CouplesMarks.Sum(s => s.MarkA);
            }
        }

        #endregion
    }

    /// <summary>
    ///     Interaction logic for EnterMarkingSumOnly.xaml
    /// </summary>
    public partial class EnterMarkingSumOnly : ScrutinusPage
    {
        private readonly List<Tuple<MarksSumEditor, int>> editorTabIndexDictionary = new List<Tuple<MarksSumEditor, int>>();

        #region Constructors and Destructors

        public EnterMarkingSumOnly(int competitionId, int roundId)
            : base(competitionId, roundId)
        {
            this.InitializeComponent();

            this.CreateTabControl();
        }

        #endregion

        #region Public Methods and Operators

        public override bool AllowToLeave()
        {
            return true;
        }

        public override bool CanMoveNext()
        {
            return this.ViewModel.Validate();
        }

        protected override void OnNextInternal()
        {
            this.ViewModel.Validate();
            this.context.SaveChanges();
        }

        #endregion

        #region Methods

        protected override void SetViewModel()
        {
            this.ViewModel = new EnterMarkingSumsViewModel(this.context, this.CurrentRound.Id, CalculationMode.Marks);
            this.DataContext = this.ViewModel;
        }

        private void CreateTabControl()
        {
            this.MarkingTab.SelectionChanged += this.MarkingTabOnSelectionChanged;

            var viewModel = this.ViewModel as EnterMarkingSumsViewModel;
            // Create a new Tab-Page
            var tabindex = 0;
            foreach (var marking in viewModel.Markings)
            {
                // Add a new tab page
                var tab = new TabItem { Header = marking.Judge.Sign };
                this.MarkingTab.Items.Add(tab);
                
                // our root is a stackpanel holding all input controls:
                var stackPanel = new StackPanel();

                var officialLabel = new Label { Content = marking.Judge.NiceName };
                officialLabel.FontSize = 15;
                officialLabel.Margin = new Thickness(0, 10, 0, 10);

                stackPanel.Children.Add(officialLabel);

                var blocks = marking.CouplesMarks.OrderBy(m => m.Participant.Number).Chunk(12).ToList();

                var last = blocks.Last();
                foreach (var block in blocks)
                {
                    var editor = new MarksSumEditor(block, this.CurrentRound.DancesInRound.Count, CalculationMode.Marks, ReferenceEquals(block, last));
                    editor.OnLastBoxLostFocus = this.OnLastBoxLostFocus;
                    stackPanel.Children.Add(editor);
                    stackPanel.Children.Add(new Border { Height = 30 });
                    this.editorTabIndexDictionary.Add(new Tuple<MarksSumEditor, int>(editor, tabindex));
                }

                var totalStackPanel = new StackPanel { Orientation = Orientation.Horizontal };

                stackPanel.Children.Add(totalStackPanel);

                var totalSum = new Label { FontSize = 16 };
                var binding = new Binding("TotalPoints") { Source = marking };
                totalSum.SetBinding(ContentControl.ContentProperty, binding);
                totalStackPanel.Children.Add(totalSum);

                var lowerMax = this.CurrentRound.MarksFrom * this.CurrentRound.DancesInRound.Count;
                var upperMax = this.CurrentRound.MarksTo * this.CurrentRound.DancesInRound.Count;

                var text = lowerMax == upperMax ? lowerMax.ToString() : lowerMax + " - " + upperMax;
                var marksLabel = new Label { FontSize = 16, Content = text, Margin = new Thickness(20, 0, 0, 0) };
                totalStackPanel.Children.Add(marksLabel);

                tab.Content = stackPanel;

                tabindex++;
            }
        }

        private void MarkingTabOnSelectionChanged(object sender, SelectionChangedEventArgs selectionChangedEventArgs)
        {
            var tuple = this.editorTabIndexDictionary.First(t => t.Item2 == this.MarkingTab.SelectedIndex);
            tuple.Item1.SetFocus(null);
        }

        private void OnLastBoxLostFocus(MarksSumEditor marksSumEditor)
        {
            var tuple = this.editorTabIndexDictionary.FirstOrDefault(t => ReferenceEquals(t.Item1, marksSumEditor));
            // find the next element:
            var index = this.editorTabIndexDictionary.IndexOf(tuple);
            index++;
            if (index == this.editorTabIndexDictionary.Count)
            {
                // go to the next step
                MainWindow.MainWindowHandle.NextState(this.competitionId);
                return;
            }

            tuple = this.editorTabIndexDictionary[index];
            this.MarkingTab.SelectedIndex = tuple.Item2;
            // tuple.Item1.SetFocus();
        }

        #endregion
    }
}