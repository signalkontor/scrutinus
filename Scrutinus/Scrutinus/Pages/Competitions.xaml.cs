﻿// // TPS.net TPS8 Scrutinus
// // Competitions.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Linq;
using System.Windows.Controls;
using Scrutinus.ViewModels;

namespace Scrutinus.Pages
{
    /// <summary>
    ///     Interaction logic for Competitions.xaml
    /// </summary>
    public partial class Competitions : ScrutinusPage
    {
        #region Fields

        private CompetitionsViewModel competitionsViewModel;

        #endregion

        #region Constructors and Destructors

        public Competitions()
            : base(0, 0)
        {
            this.InitializeComponent();

            // Setup our View Models
            var complist = this.context.Competitions.ToList();

            this.DataContext = this.competitionsViewModel;

            if (complist.Count > 0)
            {
                this.CompetitionList.SelectedIndex = 0;
            }
        }

        #endregion

        /* Part of Scrutinus Page Implementatino */

        #region Public Methods and Operators

        public override bool AllowToLeave()
        {
            if (!this.competitionsViewModel.Validate())
            {
                return false;
            }

            //if (this.CompetitionDetailControl.DataChanged)
            //{
            //    var result =
            //        MainWindow.MainWindowHandle.ShowMessage(
            //            LocalizationService.Resolve(() => Text.DataHasChangedMessage),
            //            LocalizationService.Resolve(() => Text.DataHasChangedCaption),
            //            MessageDialogStyle.AffirmativeAndNegative);

            //    return result == MessageDialogResult.Affirmative;
            //}

            return true;
        }

        public override bool CanMoveNext()
        {
            return true;
        }

        protected override void OnNextInternal()
        {
            MainWindow.MainWindowHandle.UpdateNavigationList();
        }

        #endregion

        #region Methods

        protected override void SetViewModel()
        {
            this.competitionsViewModel = new CompetitionsViewModel(this.context);
        }

        private void CompetitionList_OnBeginningEdit(object sender, DataGridBeginningEditEventArgs e)
        {
            e.Cancel = true;
        }

        private void SearchBox_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            // this.competitionsViewModel.SearchTextChanges(this.SearchBox.Text);
        }

        #endregion
    }
}