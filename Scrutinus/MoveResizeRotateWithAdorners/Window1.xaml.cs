﻿// // TPS.net TPS8 MoveResizeRotateWithAdorners
// // Window1.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace DiagramDesigner
{
    public partial class Window1 : Window
    {
        public Window1()
        {
            this.InitializeComponent();
        }

        private void OnClick(object sender, RoutedEventArgs args)
        {
            var selectionCheckBox = sender as CheckBox;
            if (selectionCheckBox != null && selectionCheckBox.IsChecked == true)
            {
                foreach (Control child in this.DesignerCanvas.Children)
                {
                    Selector.SetIsSelected(child, true);
                }
            }
            else
            {
                foreach (Control child in this.DesignerCanvas.Children)
                {
                    Selector.SetIsSelected(child, false);
                }
            }
        }
    }
}
