﻿// // TPS.net TPS8 DtvEsvModule
// // DtvApiImporter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DataModel;
using DataModel.Models;
using DataModel.Rules;
using DtvEsvModule.Model;
using General;
using General.Interfaces;
using Helpers.Skating;
using MahApps.Metro.Controls.Dialogs;
using Club = DtvEsvModule.Model.Club;
using Person = DtvEsvModule.Model.Person;
using Startbuch = DataModel.Models.Startbuch;
using Team = DataModel.Models.Team;

namespace DtvEsvModule
{
    public class DtvApiImporter
    {
        private readonly Dictionary<string, int> altersgruppen = new Dictionary<string, int>
                                                                     {
                                                                         { "KinI", 1 },
                                                                         { "KinII", 2 },
                                                                         { "Kin", 3 },
                                                                         { "KinI/II", 3 },
                                                                         { "JunI", 4 },
                                                                         { "JunII", 5 },
                                                                         { "Jug", 6 },
                                                                         { "Hgr", 7 },
                                                                         { "HgrII", 8 },
                                                                         { "SenI", 10 },
                                                                         { "SenII", 11 },
                                                                         { "SenIII", 12 },
                                                                         { "SenIV", 13 },
                                                                         { "U21", 16 },
                                                                         { "MCI", 14 },
                                                                         { "MCII", 15 },
                                                                         { "G55", 22 },
                                                                         { "Ls66", 23 },
                                                                         { "SenV", 24 }
                                                                     };

        private readonly ApiClient apiClient;

        private readonly ScrutinusContext context;

        private readonly Dictionary<string, int> danceDictionary = new Dictionary<string, int>
                                                                       {
                                                                           { "LW", 1 },
                                                                           { "TG", 2 },
                                                                           { "WW", 3 },
                                                                           { "SF", 4 },
                                                                           { "QU", 5 },
                                                                           { "SB", 6 },
                                                                           { "CC", 7 },
                                                                           { "RB", 8 },
                                                                           { "PD", 9 },
                                                                           { "JV", 10 },
                                                                           { "SA", 12 },
                                                                           { "DF", 11 },
                                                                           { "LAT", 16 },
                                                                           { "STD", 17 },
                                                                           { "JMD", 13 }
                                                                       };

        private readonly Dictionary<string, int> klassen = new Dictionary<string, int>
                                                               {
                                                                   { "D", 1 },
                                                                   { "C", 2 },
                                                                   { "B", 3 },
                                                                   { "A", 4 },
                                                                   { "S", 5 },
                                                                   { "BSW", 7 },
                                                                   { "PD", 8 },
                                                                   { "1BL", 9 },
                                                                   { "2BL", 10 },
                                                                   { "RL", 11 },
                                                                   { "OL", 12 },
                                                                   { "LL", 13 },
                                                                   { "VL", 14 }
                                                               };

        private readonly Dictionary<string, int> turnierFormDictionary = new Dictionary<string, int>
                                                                             {
                                                                                 { "OT", 4 },
                                                                                 { "ET", 9 },
                                                                                 { "DM", 5 },
                                                                                 { "DP", 11 },
                                                                                 { "DC", 12 },
                                                                                 { "RLT", 10 },
                                                                                 { "RSRLT", 13 },
                                                                                 { "GM", 14 },
                                                                                 { "LM", 6 },
                                                                                 { "IET", 15 },
                                                                                 { "IM", 16 },
                                                                                 { "IT", 17 },
                                                                                 { "PDT", 18 },
                                                                                 { "LT", 19 },
                                                                                 { "IMK", 20 },
                                                                                 { "MK", 21 },
                                                                                 { "RM", 22 }
                                                                             };

        private readonly Dictionary<string, int> TurniertArtDictionary = new Dictionary<string, int>
                                                                             {
                                                                                 { "Std", 1 },
                                                                                 { "Lat", 2 },
                                                                                 { "Kmb", 3 },
                                                                                 { "SD-Std", 5 },
                                                                                 { "SD-Lat", 6 },
                                                                                 { "JMD", 8 },
                                                                                 { "Jazz", 10 }
                                                                             };

        //private IReportProgress progressReporter;

        private SkatingViewModel skatingViewModel;

        public DtvApiImporter(string url, string user, string password, ScrutinusContext context)
        {
            this.apiClient = new ApiClient(url, user, password);
            this.context = context;
        }

        public Task ImportAsync(IReportProgress reportProgress, Veranstaltung ev, MarkingTypes defaultMarkingType)
        {
            return Task.Factory.StartNew(new Action(() => this.Import(reportProgress, ev, defaultMarkingType)));
        }

        private void Import(IReportProgress progressReporter, Veranstaltung ev, MarkingTypes defaultMarkingType)
        {
            ProgressDialogController progressController = null;

            try
            {
                progressController = progressReporter.OpenProgressBar("Import DTV Daten", "Initialisierung");

                // Import the selected event:
                var eventData = this.context.Events.First();

                if (eventData.DtvDownloadDataValid)
                {
                    progressReporter.ShowMessage("Die Aufstiegsdaten sind bereits gültig, ein weiterer Download ist daher nicht möglich!");
                    progressReporter.RemoveStatusReport();
                    return;
                }

                if (string.IsNullOrEmpty(eventData.Title))
                {
                    eventData.Place = ev.turnierstaette.name;
                    eventData.Title = ev.titel;
                }

                eventData.DateFrom = ev.datumVon;
                eventData.DateTo = ev.datumBis;
                eventData.Organizer = ev.veranstalter.name;
                eventData.OrgnizerRegion = ev.veranstalter.ltv != null ? ev.veranstalter.ltv.name : "";
                eventData.Executor = ev.ausrichter.name;
                eventData.ExecutorRegion = ev.ausrichter.ltv != null ? ev.ausrichter.ltv.name : "";
                eventData.ExternalId = ev.id.ToString();

                if (ev.flaechen != null && ev.flaechen.Length > 0)
                {
                    eventData.DanceFloor = ev.flaechen[0].typ;
                    eventData.SizeDanceFloorLength = ev.flaechen[0].laenge.ToString();
                    eventData.SizeDanceFloorWidth = ev.flaechen[0].breite.ToString();
                }

                // Save this now:
                this.context.SaveChanges();
                // Create the competitions:
                var index = 0;
                var maxCount = ev.turniere.Count();

                foreach (var comp in ev.turniere)
                {
                    index++;
                    progressController.SetMessage("Importiere Turnier " + comp.titel);
                    progressController.SetProgress(Convert.ToDouble(index) / Convert.ToDouble(maxCount));

                    var dtvCompetitionId = comp.id.ToString();

                    var competition = this.context.Competitions.FirstOrDefault(c => c.ExternalId == dtvCompetitionId);

                    if (competition == null)
                    {
                        competition = new Competition
                                          {
                                              AgeGroup = this.GetAgeGroup(comp.startgruppe),
                                              Class = this.GetClass(comp.startklasseLiga),
                                              StartTime = new DateTime(comp.datumVon.Year, comp.datumVon.Month, comp.datumVon.Day),
                                              Section = this.GetCompetitionSection(comp.turnierart),
                                              CompetitionType = this.GetCompetitionType(comp.turnierform),
                                              DefaultCouplesPerHeat = 6,
                                              Event = eventData,
                                              ExternalId = comp.id.ToString(),
                                              FirstRoundType = RoundTypes.QualificationRound,
                                              FinalType = MarkingTypes.Skating,
                                              MarkingType = defaultMarkingType,
                                              LastDtvDownload = DateTime.Now,
                                              LateEntriesAllowed = false,
                                              HasRankingPoints = comp.aufstiegsturnier
                                          };

                        var danceHelper = new DanceRules(this.context);

                        var dances = danceHelper.GetDances(
                            competition.Section.Id,
                            competition.AgeGroup.Id,
                            competition.Class.Id);

                        var order = 1;

                        foreach (var dance in dances)
                        {
                            competition.Dances.Add(
                                new DancesInCompetition { Competition = competition, Dance = dance, DanceOrder = order });
                            order++;
                        }

                        this.context.Competitions.Add(competition);
                    }

                    competition.HasRankingPoints = comp.aufstiegsturnier;
                    competition.LastDtvDownload = DateTime.Now;
                    this.TryUpdateStartTime(competition, comp.startzeitPlan);
                }

                var sign = 1;
                SyncJudges(this.context, ev, ref sign);

                SyncOfficials(this.context, ev, ref sign);

                this.ImportOfficials(progressController);

                this.SyncClimbUpTables(progressController);
                // Import StartList, Officials if not existing:
                progressController.SetMessage("Lade Startliste");
                var startlist = this.apiClient.GetStarterByVeranstaltung(ev.id);

                eventData.DtvDownloadDataValid = startlist.status == 2;

                foreach (var competition in this.context.Competitions)
                {
                    competition.LateEntriesAllowed = false;
                }

                foreach (var competitionId in startlist.nachmeldungen)
                {
                    var competition = this.context.Competitions.FirstOrDefault(c => c.ExternalId == competitionId);
                    if (competition != null)
                    {
                        competition.LateEntriesAllowed = true;
                    }
                    this.context.SaveChanges();
                }

                if (this.context.Competitions.Any(c => c.LateEntriesAllowed))
                {
                    try
                    {
                        this.ImportCompleteStarterList(progressController);
                    }
                    catch (Exception ex)
                    {
                       progressReporter.ShowMessage("Die Startliste Gesamt konnte nicht herunter geladen werden. Dies ist aber nur dann ein Problem, wenn es Turniere gibt, die eine Vor-Ort Meldung von nicht regstrierten Paaren erlaubt. Dies ist meist nicht der Fall.");
                    }
                }

                index = 0;
                maxCount = startlist.starter.Count();
                foreach (var starter in startlist.starter)
                {
                    if (!starter.id.HasValue)
                    {
                        continue;
                    }

                    index++;
                    progressController.SetMessage("Importiere Startliste");
                    progressController.SetProgress(Convert.ToDouble(index) / Convert.ToDouble(maxCount));

                    var starterDtvId = starter.id.Value.ToString(CultureInfo.InvariantCulture);

                    if (starter.team != null)
                    {
                        this.CreateOrUpdateTeam(starterDtvId, starter);
                    }
                    else
                    {
                        this.CreateOrUpdateCouple(starterDtvId, starter);
                    }

                    this.context.SaveChanges();
                }

                
                progressController.SetMessage("Überprüfe Startbücher");
                // Wir checken alle Participants für Startbücher.
                // wenn ein Participant kein Startbuch hat, erzeugen wir ein Temporäres
                // Check allerdings nur für Einzelwettbewerbe

                foreach (var participant in this.context.Participants.ToList().Where(p => p.Competition.CompetitionType != null && !p.Competition.CompetitionType.IsFormation && !p.Competition.CompetitionType.IsJMD))
                {
                    if (participant.Startbuch == null)
                    {
                        var table =
                            this.context.ClimbUpTables.FirstOrDefault(
                                t =>
                                    t.Section.Id == participant.Competition.Section.Id && t.LTV == participant.Couple.Region
                                    && t.AgeGroup.Id == participant.AgeGroup.Id && t.Class.Id == participant.Class.Id) ?? GetAufstiegstabelleifNotFound(this.context, participant);

                        var startbuch = new Startbuch
                                            {
                                                AgeGroup = participant.AgeGroup,
                                                Class = participant.Class,
                                                OriginalPlacings = 0,
                                                OriginalPoints = 0,
                                                Blocked = false,
                                                Sylabus = false,
                                                PrintLaufzettel = false,
                                                Section = participant.Competition.Section,
                                                IsSelfGenerated = true,
                                                MinimumPoints = table?.MinPoints ?? 0,
                                                Points = 0,
                                                Placings = 0,
                                                PlacingsUpto = table?.MinimumPlace ?? 0,
                                                NextClass = table?.TargetClass,
                                                TargetPlacings = table?.Placings ?? 0,
                                                TargetPoints = table?.Points ?? 0,
                                                Couple = participant.Couple
                                            };
                        participant.Couple.Startbooks.Add(startbuch);
                        this.context.Startbuecher.Add(startbuch);
                    }

                    participant.OriginalPlacings = participant.Startbuch.OriginalPlacings;
                    participant.OriginalPoints = participant.Startbuch.OriginalPoints;
                    participant.TargetPlacings = participant.Startbuch.TargetPlacings;
                    participant.TargetPoints = participant.Startbuch.TargetPoints;
                    participant.TargetClass = participant.Startbuch.NextClass;
                    participant.MinimumPoints = participant.Startbuch.MinimumPoints;
                    participant.PlacingsUpto = participant.Startbuch.PlacingsUpto;
                }

                this.context.SaveChanges();

                var closingTask = progressController.CloseAsync();
                while (!closingTask.IsCompleted)
                {
                    Thread.Sleep(250);
                }
            }
            catch (DbEntityValidationException dbException)
            {
                progressController?.CloseAsync();

                var message = "";
                foreach (var error in dbException.EntityValidationErrors)
                {
                    foreach (var dbValidationError in error.ValidationErrors)
                    {
                        message += dbValidationError.PropertyName + ": " + dbValidationError.ErrorMessage;
                    }
                }

                progressReporter.ShowMessage(message);
            }
            catch (Exception ex)
            {
                progressController?.CloseAsync().ContinueWith((t) =>
                {
                    progressReporter.ShowMessage("Fehler beim DTV Import: " + ex.Message);
                });
            }

            progressReporter.RemoveStatusReport();

            
        }

        private void SetNames(Couple couple, Person[] personen)
        {
            Person man;
            Person woman;

            if (personen.Any(p => p.geschlecht == null))
            {
                man = personen[0];
                woman = personen[1];
            }
            else
            {
                man = personen.FirstOrDefault(p => p.geschlecht == "m") ?? personen[0];
                woman = personen.FirstOrDefault(p => p.geschlecht == "w" || p.geschlecht == null || p.geschlecht == "") ??
                        personen[1];
            }
            

            if (man.nachname.StartsWith("Dr. "))
            {
                couple.LastMan = man.nachname.Replace("Dr. ", "");
                couple.FirstMan = "Dr. " + man.vorname;
            }
            else
            {
                couple.LastMan = man.nachname;
                couple.FirstMan = man.vorname;
            }

            if (woman.nachname.StartsWith("Dr. "))
            {
                couple.LastWoman = woman.nachname.Replace("Dr. ", "");
                couple.FirstWoman = "Dr. " + woman.vorname;
            }
            else
            {
                couple.LastWoman = woman.nachname;
                couple.FirstWoman = woman.vorname;
            }
        }

        private void TryUpdateStartTime(Competition competition, string startzeitPlan)
        {
            if (string.IsNullOrEmpty(startzeitPlan))
            {
                return;
            }

            if (competition.StartTime.HasValue && competition.StartTime.Value.Hour != 0)
            {
                // The default time of the competition has been changed and we do 
                // not update this to not overwrite user's data
                return;
            }

            var times = startzeitPlan.Split(':');
            if (times.Length != 2)
            {
                return;
            }

            try
            {
                var hour = int.Parse(times[0]);
                var minutes = int.Parse(times[1]);
                competition.StartTime = new DateTime(competition.StartTime.Value.Year, competition.StartTime.Value.Month, competition.StartTime.Value.Day, hour, minutes, 0);
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        private void ImportCompleteStarterList(ProgressDialogController progressController)
        {
            progressController.SetMessage("Importiere Gesamt-Startliste");
            var list = this.apiClient.GetStarterByCompetitionType("Einzel");

            var index = 0;
            var max = Convert.ToDouble(list.starter.Length);

            foreach (var starter in list.starter)
            {
                progressController.SetProgress(Convert.ToDouble(index) / max);

                foreach (var startbuch in starter.startbuch)
                {
                    var section = this.GetCompetitionSection(startbuch.turnierart);
                    var idMan = starter.personen[0].id;
                    var idWoman = starter.personen[1].id;

                    var book =
                        this.context.SimpleStartbucher.FirstOrDefault(
                            b => b.IdMan == idMan && b.IdWoman == idWoman && b.Section.Id == section.Id);

                    if (book == null)
                    {
                        book = new SimpleStartbuch
                        {
                            StarterId = starter.id.Value,
                            IdMan = starter.personen[0].id,
                            IdWoman = starter.personen[1].id,
                            Section = section
                        };

                        this.context.SimpleStartbucher.Add(book);
                    }

                    book.AgeGroup = this.GetAgeGroup(startbuch.startgruppe);
                    book.Class = this.GetClass(startbuch.startklasse);
                    book.NextClass = this.GetClass(startbuch.naechsteStartklasse);
                    book.OriginalPoints = startbuch.punkte.ist;
                    book.TargetPoints = startbuch.punkte.ziel ?? 0;
                    book.OriginalPlacings = startbuch.platzierungen.ist;
                    book.TargetPlacings = startbuch.platzierungen.ziel ?? 0;
                    book.MinimumPoints = startbuch.regeln.minPunkte;
                    book.PlacingsUpto = startbuch.regeln.platzierungBis;
                    book.Sylabus = startbuch.flags.verwarnungSchrittbegrenzung;
                    book.PrintLaufzettel = startbuch.flags.laufzettel;

                    if (starter.club != null)
                    {
                        book.Club = starter.club.name;
                        book.LTV = starter.club.ltv.name;
                    }
                }
                index++;
            }

            this.context.SaveChanges();
        }

        public void ImportOfficials(ProgressDialogController progressController)
        {
            progressController.SetMessage("Importiere Gesamtliste Funktionäre");           

            var list = this.apiClient.GetAllFunktionaere().ToList();

            var max = list.Count();
            var index = 0;

            foreach (var funktionaer in list)
            {
                progressController.SetProgress(Convert.ToDouble(index) / Convert.ToDouble(max));

                var existing = this.context.DtvLicenseHolders.FirstOrDefault(o => o.DtvId == funktionaer.id);
                if (existing == null)
                {
                    existing = new DtvLicenseHolder() {
                        DtvId = funktionaer.id,
                        WdsfMin = funktionaer.wdsfMin,
                        FirstName = funktionaer.vorname,
                        LastName = funktionaer.nachname,
                        Club = funktionaer.club?.name,
                        Nationality = this.context.CountryCodes.FirstOrDefault(c => c.IocCode == funktionaer.staat)
                    };

                    this.context.DtvLicenseHolders.Add(existing);
                }

                existing.FirstName = funktionaer.vorname;
                existing.LastName = funktionaer.nachname;
                existing.Club = funktionaer.club?.name;
                existing.Nationality = this.context.CountryCodes.FirstOrDefault(c => c.IocCode == funktionaer.staat);
                existing.Licenses = funktionaer.lizenzen;
                index++;
            }

            this.context.SaveChanges();
        }

        public static ClimbUpTable GetAufstiegstabelleifNotFound(ScrutinusContext context, Participant participant)
        {
            // Aufstieg in die nächste Alterststufe. Dieser Fall extistiert nur bis Jugend.
            if (participant.AgeGroup.Id > 6)
            {
                return null;
            }

            if (participant.AgeGroup.Id == 3)
            {
                // Kinder I/II, we search for Kinder II:
                var table =
                    context.ClimbUpTables.FirstOrDefault(
                        t =>
                        t.AgeGroup.Id == 2 && t.Section.Id == participant.Competition.Section.Id
                        && t.LTV == participant.Couple.Region && t.Class.Id == participant.Class.Id);

                if (table != null)
                {
                    return table;
                }
            }

            var olderAgeGroupId = participant.AgeGroup.Id == 2 ? 4 : participant.AgeGroup.Id + 1;

            var olderAgeGroup = context.AgeGroups.FirstOrDefault(a => a.Id == olderAgeGroupId);
            if (olderAgeGroup != null)
            {
                var table =
                    context.ClimbUpTables.FirstOrDefault(
                        t =>
                        t.AgeGroup.Id == olderAgeGroup.Id && t.Section.Id == participant.Competition.Section.Id
                        && t.LTV == participant.Couple.Region && t.Class.Id == participant.Class.Id);

                return table;
            }

            return null;
        }

        public void SyncClimbUpTables(ProgressDialogController progressController)
        {
            // Clear all the old tables:
            this.context.ClimbUpTables.RemoveRange(this.context.ClimbUpTables);
            this.context.SaveChanges();

            progressController.SetMessage("Lade Aufstiegsdaten");

            var tabelle = this.apiClient.GetAufstiegstabelle();

            var max = 1500;

            var current = 0;

            foreach (var tab in tabelle)
            {
                foreach (var aufstiegstabelle in tab.tabellen)
                {
                    foreach (var klasse in aufstiegstabelle.startgruppen)
                    {
                        foreach (var kriterien in aufstiegstabelle.kriterien)
                        {
                            foreach (var ltv in tab.ltv)
                            {
                                var ageGroup = this.GetAgeGroup(klasse);
                                var startklasse = this.GetClass(kriterien.klasse);
                                var section = this.GetCompetitionSection(aufstiegstabelle.turnierart);

                                var existing =
                                    this.context.ClimbUpTables.FirstOrDefault(
                                        t =>
                                        t.Section.Id == section.Id && t.AgeGroup.Id == ageGroup.Id
                                        && t.Class.Id == startklasse.Id && t.LTV == ltv);

                                if (existing != null)
                                {
                                    this.context.ClimbUpTables.Remove(existing);
                                }

                                var climbUpEntry = new ClimbUpTable
                                                       {
                                                           AgeGroup = ageGroup,
                                                           Class = startklasse,
                                                           TargetClass = this.GetClass(kriterien.zielklasse),
                                                           MinimumPlace = kriterien.bisPlatz,
                                                           MinPoints = tabelle[0].minPunkte,
                                                           Section = section,
                                                           Placings = kriterien.plaetze,
                                                           Points = kriterien.punkte,
                                                           TargetInDifferentAgeGroup = kriterien.doppelstart,
                                                           LTV = ltv
                                                       };

                                this.context.ClimbUpTables.Add(climbUpEntry);

                                current++;

                                progressController.SetProgress(max > current ? Convert.ToDouble(current) / Convert.ToDouble(max) : 1);
                            }
                        }
                    }
                }
            }
            this.context.SaveChanges();
        }

        public string[] SaveResult(Competition competition)
        {
            if (competition.Canceled)
            {
                if (!context.Competitions.Any(c =>
                    c.Id != competition.Id && c.ExternalId.Contains(competition.ExternalId)))
                {
                    return
                        this.apiClient.SendCancledCompetition(
                            new AusgefallenesTurnier
                            {
                                vorkommnisse =
                                    string.IsNullOrEmpty(competition.Report)
                                        ? ""
                                        : competition.Report
                            },
                            competition.ExternalId);
                }
                else
                {
                    return new string[] { $"Info: Das ausgefallenes Turnier {competition.Title} wurde nicht hochgeladen da es kombinert wurde."};
                }
            }

            if (competition.DtvJson == null)
            {
                this.CalculateChecksum(competition);
            }
            
            var dtvId = competition.ExternalId;
            if (competition.CombinedExternalId != null)
            {
                dtvId = dtvId.Replace(competition.CombinedExternalId, "").Replace("/", "");
            }

            if (competition.Participants.Any(p => p.State == CoupleState.Dancing))
            {
                return new string[] { "Mindestens ein Paar ist noch auf Status 'Tanzen'. Falls dieses Turnier abgeschlossen ist: Gehen Sie bitte an den Anfang zurück und dann wieder zurück zum Endergebnis" };
            }

            return this.apiClient.SaveResult(competition.DtvJson, competition.DtvChecksum, dtvId);
        }


        public void CalculateChecksum(Competition competition)
        {
            competition.DtvJson = JsonHelper.SerializeObject(this.CreateResult(competition));

            competition.DtvChecksum = this.apiClient.CalculateHash(competition.DtvJson);
        }


        private static void SyncJudges(ScrutinusContext context, Veranstaltung ev, ref int sign)
        {
            // Import Judges and officials:
            var roleJudge = context.Roles.FirstOrDefault(r => r.Id == Roles.Judge);
            foreach (var judge in ev.wertungsrichter)
            {
                var dtvId = judge.id;
                var official = context.Officials.FirstOrDefault(o => o.ExternalId == dtvId);
                if (official == null)
                {
                    official = new Official
                                   {
                                       Club = judge.club != null ? judge.club.name : judge.staat,
                                       Region = judge.club != null ? judge.club.ltv.name : "",
                                       Nationality = context.CountryCodes.FirstOrDefault(c => c.IocCode == judge.staat),
                                       Firstname = judge.vorname,
                                       Lastname = judge.nachname,
                                       ExternalId = dtvId,
                                       Sign = sign.ToString()
                                   };

                    official.Roles.Add(roleJudge);
                    context.Officials.Add(official);
                }

                sign++;
            }

            context.SaveChanges();
        }

        private static void SyncOfficials(ScrutinusContext context, Veranstaltung ev, ref int sign)
        {
            foreach (var judge in ev.funktionaere)
            {
                var dtvId = judge.id;
                var official = context.Officials.FirstOrDefault(o => o.ExternalId == dtvId);
                if (official == null)
                {
                    official = new Official
                                   {
                                       Club = judge.club != null ? judge.club.name : judge.staat,
                                       Region = judge.club != null ? judge.club.ltv.name : "",
                                       Nationality = context.CountryCodes.FirstOrDefault(c => c.IocCode == judge.staat),
                                       Firstname = judge.vorname,
                                       Lastname = judge.nachname,
                                       ExternalId = dtvId,
                                       Sign = sign.ToString()
                                   };

                    context.Officials.Add(official);
                }
                sign++;
            }

            context.SaveChanges();
        }

        private Gesamtergebnis CreateResult(Competition competition)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            var result = new Gesamtergebnis();

            if (competition.ActualStartTime == null)
            {
                competition.ActualStartTime = competition.StartTime ?? DateTime.Now;
            }

            if (competition.EndTime == null)
            {
                competition.EndTime = DateTime.Now;
            }

            // Create the result of this competition:
            result.vorkommnisse = competition.Report;
            result.beginn = competition.ActualStartTime.Value;
            result.ende = competition.EndTime.Value;
            result.zuschauer = competition.Visitors;
            result.starter = new StarterMitErgebnis[competition.Participants.Count];
            result.wr = competition.GetJudges().OrderBy(o => o.Sign).Select(GetFunktionaerForOfficial).ToArray();
            result.kombiniertMit = competition.CombinedExternalId;

            var tl = competition.Officials.Where(o => o.Role.Id == Roles.Supervisor).ToList().Select(o => GetFunktionaerForOfficial(o.Official)).ToArray();
            var bs = competition.Officials.Where(o => o.Role.Id == Roles.Associate).ToList().Select(o => GetFunktionaerForOfficial(o.Official)).ToArray();

            result.tl = tl;
            result.bs = bs;

            if (competition.Officials.Any(o => o.Role.Id == Roles.Chairman))
            {
                var cm = competition.Officials.Where(o => o.Role.Id == Roles.Chairman).ToList().Select(o => GetFunktionaerForOfficial(o.Official)).ToArray();
                result.ch = cm;
            }

            Debug.WriteLine("Step-1: " + stopwatch.ElapsedMilliseconds);
            result.taenze = this.GetTaenze(competition.Dances);
            Debug.WriteLine("Step-2: " + stopwatch.ElapsedMilliseconds);
            result.ablauf = this.GetAblauf(competition);
            Debug.WriteLine("Step-3: " + stopwatch.ElapsedMilliseconds);
            var index = 0;

            foreach (var participant in competition.Participants.OrderBy(o => o.Number))
            {
                var status = 1;
                if (participant.State == CoupleState.Excused)
                {
                    status = 2;
                }
                if (participant.State == CoupleState.Missing)
                {
                    status = 3;
                }

                var dtvId = 0;
                if (participant.ExternalId != null && participant.ExternalId.Contains("_"))
                {
                    var data = participant.ExternalId.Split('_');
                    int.TryParse(data[1], out dtvId);
                }

                var starter = new StarterMitErgebnis
                                  {
                                      id = dtvId,
                                      status = status,
                                      meldungsTyp = (int)participant.RegistrationFlag,
                                      ergebnis =
                                          status == 1
                                              ? GetErgebnisOfParticipant(participant)
                                              : null,
                                      personen = GetPersonenOfParticipant(participant),
                                      startNr = participant.Number,
                                      staat = participant.Couple.Country2
                                  };

                result.starter[index] = starter;
                result.starter[index].wertung = this.GetWertungen(participant);
                index++;
            }
            
            var final = competition.Rounds.SingleOrDefault(r => r.RoundType.Id == RoundTypes.Final);

            if (final != null)
            {
                var skatingViewModel = SkatingHelper.CreateViewModel(this.context, final);
                // Calculate the skating tables
                result.endrundentabellen = this.GetEndrundenTabellen(skatingViewModel);
                result.skatingtabellen = this.GetSkatingTabellen(skatingViewModel);
            }
            
            return result;
        }

        private Skatingtabellen[] GetSkatingTabellen(SkatingViewModel viewModel)
        {
            var result = new Skatingtabellen[1];
            result[0] = new Skatingtabellen();

            if (viewModel.Rule10ViewModel.Count == 0 && viewModel.Rule11ViewModel.Count == 0)
            {
                return null;
            }

            var numberParticipants = viewModel.Participants.Count;

            result[0].regel10 = new List<List<object>>();
            foreach (var participant in viewModel.Participants)
            {
                var participantEntry = new List<object>();
                result[0].regel10.Add(participantEntry);

                var participantSkating = new List<Endrundentabellen>();
                participantEntry.Add(participantSkating);

                var skating = viewModel.Rule10ViewModel.SingleOrDefault(r => r.Participant.Id == participant.Id);

                if (skating == null)
                {
                    for (var place = 1; place <= numberParticipants; place++)
                    {
                        participantSkating.Add(null);
                    }
                }
                else
                {
                    for (var place = 1; place <= numberParticipants; place++)
                    {
                        if (skating.NumberPlaces[place - 1] > 0)
                        {
                            participantSkating.Add(
                                new Endrundentabellen
                                    {
                                        anzahl = (int)skating.NumberPlaces[place - 1],
                                        summe = skating.Sums[place - 1]
                                    });
                        }
                        else
                        {
                            participantSkating.Add(null);
                        }
                    }
                }

                if (skating != null && skating.IsResolved)
                {
                    participantEntry.Add(skating.Place);
                }
                else
                {
                    participantEntry.Add(null);
                }
            }

            // Do the same with rule 11
            result[0].regel11 = new List<List<object>>();
            foreach (var participant in viewModel.Participants)
            {
                var participantEntry = new List<object>();
                result[0].regel11.Add(participantEntry);

                var participantSkating = new List<Endrundentabellen>();
                participantEntry.Add(participantSkating);

                var skating = viewModel.Rule11ViewModel.FirstOrDefault(r => r.Participant.Id == participant.Id);
                if (skating == null)
                {
                    for (var place = 1; place <= numberParticipants; place++)
                    {
                        participantSkating.Add(null);
                    }
                }
                else
                {
                    for (var place = 1; place <= numberParticipants; place++)
                    {
                        
                        if (skating.NumberPlaces[place - 1] > 0)
                        {
                            participantSkating.Add(
                                new Endrundentabellen
                                    {
                                        anzahl = (int)skating.NumberPlaces[place - 1],
                                        summe = skating.Sums[place - 1]
                                    });
                        }
                        else
                        {
                            participantSkating.Add(null);
                        }
                    }
                }

                if (skating != null && skating.IsResolved)
                {
                    participantEntry.Add(skating.Place);
                }
                else
                {
                    participantEntry.Add(null);
                }
            }

            return result;
        }

        private List<List<List<List<Endrundentabellen>>>> GetEndrundenTabellen(SkatingViewModel viewModel)
        {
            var result = new List<List<List<List<Endrundentabellen>>>>();

            var table = new List<List<List<Endrundentabellen>>>();
            result.Add(table);

            foreach (var danceSkating in viewModel.DanceSkating)
            {
                var danceTable = new List<List<Endrundentabellen>>();
                table.Add(danceTable);
                foreach (var participant in danceSkating.CoupleResults)
                {
                    var participantTable = new List<Endrundentabellen>();
                    danceTable.Add(participantTable);

                    for (var place = 1; place <= viewModel.Participants.Count; place++)
                    {
                        if (participant.NumberPlaces[place - 1] > 0)
                        {
                            participantTable.Add(
                                new Endrundentabellen
                                    {
                                        anzahl = participant.NumberPlaces[place - 1],
                                        summe = participant.Sums[place - 1] > 0 ? ((double?)participant.Sums[place - 1]) : null
                                    });
                        }
                        else
                        {
                            participantTable.Add(null);
                        }
                    }
                }
            }

            return result;
        }

        public void SendJudgingSheets(Competition competition)
        {
            var files = competition.JudgingSheets.Select(s => s.FileName);

            this.apiClient.UploadJudgingSheets(files, competition.ExternalId);
        }

        public string[] SendDigitalJudgingSheets(Competition competition)
        {
            var judges = competition.GetJudges();
            var ablauf = this.GetAblauf(competition);
            var index = 0;
            var resultlist = new List<string>();

            foreach (var round in competition.Rounds.OrderBy(r => r.Number))
            {
                if (!round.EjudgeData.Any())
                {
                    continue;
                }

                var result = new DigitalJudgingSheet[judges.Count];

                var judgeIndex = 0;
                foreach (var official in judges)
                {
                    result[judgeIndex] = new DigitalJudgingSheet();
                    result[judgeIndex].kreuzvorgabeVon = round.MarksFrom;
                    result[judgeIndex].kreuzvorgabeBis = round.MarksTo;
                    result[judgeIndex].unterschrift = this.GetSignatureAsBase64(round.EjudgeData.First(e => e.Judge.Id == official.Id));
                    result[judgeIndex].starter = this.GetResultForDigitalSheet(round, official.Id);
                    judgeIndex++;
                }

                resultlist.AddRange(this.apiClient.UploadDigitalJudgingSheets(result, competition.ExternalId, ablauf[index].runde));

                index++;
            }

            return resultlist.ToArray();
        
        }

        public string[] SendDigitalJudgingSheetsFromMobileControl(string pathToEJudge, Competition competition)
        {
            var judges = competition.GetJudges();
            var ablauf = this.GetAblauf(competition);
            var index = 0;
            var resultlist = new List<string>();

            foreach (var round in competition.Rounds.OrderBy(r => r.Number))
            {
                var file = $"{competition.Id:0000}_{round.Number}_##_Wr.dat";

                var pathToFile = Path.Combine(pathToEJudge, file);

                if (!File.Exists(pathToFile))
                {
                    continue;
                }

                var result = new DigitalJudgingSheet[judges.Count];

                var judgeIndex = 0;
                foreach (var official in judges)
                {
                    result[judgeIndex] = new DigitalJudgingSheet
                    {
                        kreuzvorgabeVon = round.MarksFrom,
                        kreuzvorgabeBis = round.MarksTo,
                        unterschrift = this.GetSignatureAsBase64(pathToEJudge, file, official.Sign),
                        starter = this.GetResultForDigitalSheet(round, official.Id)
                    };
                    judgeIndex ++;
                }

                resultlist.AddRange(
                    this.apiClient.UploadDigitalJudgingSheets(result, competition.ExternalId, ablauf[index].runde));

                index++;
            }

            return resultlist.ToArray();
        }

        private StarterDigitalSheet[] GetResultForDigitalSheet(Round round, int judgeId)
        {
            var result = new StarterDigitalSheet[round.Qualifieds.Count];
            var index = 0;
            foreach (var qualified in round.Qualifieds)
            {
                result[index] = new StarterDigitalSheet
                                    {
                                        startNr = qualified.Participant.Number,
                                        wertungen = new string[round.Competition.Dances.Count]
                                    };

                var danceIndex = 0;
                foreach (var danceInCompetition in round.Competition.Dances)
                {
                    if (round.DancesInRound.All(d => d.Dance.Id != danceInCompetition.Dance.Id))
                    {
                        // The dance was not danced in this round -> we set it to null and continue.
                        result[index].wertungen[danceIndex] = null;
                        continue;
                    }

                    var marking =
                        this.context.Markings.SingleOrDefault(
                            m =>
                            m.Judge.Id == judgeId && m.Dance.Id == danceInCompetition.Dance.Id && m.Round.Id == round.Id
                            && m.Participant.Id == qualified.Participant.Id);

                    if (marking != null)
                    {
                        result[index].wertungen[danceIndex] = marking.Lift
                                                                  ? "L"
                                                                  : marking.Mark.ToString(CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        result[index].wertungen[danceIndex] = "0";
                    }

                    danceIndex++;
                }

                if (round.MarksInputType == MarkingTypes.Marking || round.MarksInputType == MarkingTypes.MarksSumOnly)
                {
                    result[index].summe =
                        this.context.Markings.Count(
                            m =>
                            m.Mark > 0 && m.Round.Id == round.Id && m.Judge.Id == judgeId
                            && m.Participant.Id == qualified.Participant.Id).ToString(CultureInfo.InvariantCulture);
                }
                else
                {
                    result[index].summe = null;
                }

                index++;
            }

            return result;
        }

        private string GetSignatureAsBase64(EjudgeData ejudgeData)
        {
            var points = new List<Tuple<Point, Point>>();
            var coordinates = new Tuple<string, List<Tuple<Point, Point>>>(ejudgeData.Judge.Sign, points);

            foreach (var point in ejudgeData.SignatureCoordinates)
            {
                points.Add(new Tuple<Point, Point>(new Point(Convert.ToInt32(point.X1), Convert.ToInt32(point.Y1)), new Point(Convert.ToInt32(point.X2), Convert.ToInt32(point.Y2))));
            }

            return this.GetSignatureFromCoordinates(coordinates);
        }

        private string GetSignatureAsBase64(string pathToEJudge, string file, string sign)
        {
            var coordinates = this.ReadSignatures(Path.Combine(pathToEJudge, file));

            var coordinate = coordinates.SingleOrDefault(i => i.Item1 == sign);

            return this.GetSignatureFromCoordinates(coordinate);
        }

        private string GetSignatureFromCoordinates(Tuple<string, List<Tuple<Point, Point>>> coordinate)
        {
            using (var b = new Bitmap(400, 400))
            {
                using (var g = Graphics.FromImage(b))
                {
                    g.Clear(Color.White);

                    if (coordinate != null)
                    {
                        foreach (var tuple in coordinate.Item2)
                        {
                            g.DrawLine(Pens.Black, tuple.Item1, tuple.Item2);
                        }
                    }
                }

                var memoryStream = new MemoryStream();

                b.Save(memoryStream, ImageFormat.Png);

                // Base64 Encoding:
                return Convert.ToBase64String(memoryStream.GetBuffer());
            }
        }

        private IEnumerable<Tuple<string, List<Tuple<Point, Point>>>> ReadSignatures(string file)
        {
            var result = new List<Tuple<string, List<Tuple<Point, Point>>>>();

            using (var stream = new StreamReader(file))
            {
                while (!stream.EndOfStream)
                {
                    var data = TPSSplitter(stream.ReadLine());
                    if (data.Length < 10)
                    {
                        continue;
                    }

                    var sign = data[0];

                    var pointsData = TPSSplitter(data[7]);

                    if (pointsData.Length < 3)
                    {
                        continue;
                    }

                    var points = int.Parse(pointsData[2]);

                    var list = new List<Tuple<Point, Point>>();

                    result.Add(new Tuple<string, List<Tuple<Point, Point>>>(sign, list));

                    for (var i = 0; i < points * 4; i += 4)
                    {
                        var x1 = int.Parse(pointsData[i + 3]) * 2;
                        var y1 = int.Parse(pointsData[i + 4]) * 2;
                        var x2 = int.Parse(pointsData[i + 5]) * 2;
                        var y2 = int.Parse(pointsData[i + 6]) * 2;
                        var tuple = new Tuple<Point, Point>(new Point(x1, y1), new Point(x2, y2));
                        list.Add(tuple);
                    }
                }
            }

            return result;
        }

        private static string[] TPSSplitter(string data)
        {
            var tokens = new List<string>();
            var upmode = false;
            var token = "";

            for (var i = 0; i < data.Length; i++)
            {
                if (data[i] == ';' && !upmode)
                {
                    tokens.Add(token);
                    token = "";
                    continue;
                }

                if (data[i] == '"')
                {
                    upmode = !upmode;
                    continue;
                }

                token += data[i];
            }
            // Add the last one at end of line
            tokens.Add(token);

            return tokens.ToArray();
        }

        private object[] GetWertungen(Participant participant)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            var result = new List<object>();

            var rounds = participant.Competition.Rounds.OrderBy(r => r.Number); // Qualifieds.Select(q => q.Round).OrderBy(r => r.Number);

            foreach (var round in rounds)
            {
                // Was the participant qualified for this round:
                var wasQualified = round.Qualifieds.Any(q => q.Participant.Id == participant.Id);

                if (!wasQualified)
                {
                    if (round.Number == 1 && participant.Star > 0)
                    {
                        result.Add(null);
                        continue;
                    }
                    else
                    {
                        break;
                    }
                }

                if (round.MarksInputType == MarkingTypes.Marking)
                {
                    result.Add(GetMarkings(round, participant));
                }
                if (round.MarksInputType == MarkingTypes.MarksSumOnly)
                {
                    result.Add(GetMarkingsSum(round, participant));
                }
                if (round.MarksInputType == MarkingTypes.Skating)
                {
                    result.Add(this.GetFinalWertungen(round, participant));
                }
            }
            Debug.WriteLine("GetWertungen: " + stopwatch.ElapsedMilliseconds);
            return result.ToArray();
        }

        private object[] GetFinalWertungen(Round round, Participant participant)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            var judges = participant.Competition.GetJudges();
            var dances = participant.Competition.Dances;

            if (this.skatingViewModel == null || this.skatingViewModel.Round.Id != round.Id)
            {
                this.skatingViewModel = SkatingHelper.CreateViewModel(this.context, round);
            }
            
            Debug.WriteLine("finalViewModel created: " + stopwatch.ElapsedMilliseconds);
            var result = new object[dances.Count];
            double sum = 0;
            var danceIndex = 0;
            foreach (var dance in dances)
            {
                result[danceIndex] = new object[judges.Count + 2];

                var judgeIndex = 0;
                foreach (var judge in judges)
                {
                    var marking =
                        round.Markings.SingleOrDefault(
                            m =>
                            m.Participant.Id == participant.Id && m.Dance.Id == dance.Dance.Id && m.Judge.Id == judge.Id);
                    ((object[])result[danceIndex])[judgeIndex] = marking?.Mark ?? 0;
                    judgeIndex++;
                }

                var resultDance = this.skatingViewModel.DanceSkating.Single(d => d.DanceInRound.Dance.Id == dance.Dance.Id);
                var pa = resultDance.CoupleResults.SingleOrDefault(p => p.Participant.Id == participant.Id);
                var rank = pa.Place;
                sum += rank;

                ((object[])result[danceIndex])[judgeIndex] = rank; // Platz im Tanz
                ((object[])result[danceIndex])[judgeIndex + 1] = sum;

                danceIndex++;
            }

            Debug.WriteLine("GetFinalMarking: " + stopwatch.ElapsedMilliseconds);
            return result;
        }

        private static object[] GetMarkings(Round round, Participant participant)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            var judges = participant.Competition.GetJudges();
            var dances = participant.Competition.Dances.ToList();

            var result = new object[judges.Count + 1];

            var judgeIndex = 0;
            foreach (var judge in judges)
            {
                result[judgeIndex] = new object[dances.Count + 1];
                var danceIndex = 0;
                foreach (var dance in dances)
                {
                    ((object[])result[judgeIndex])[danceIndex] =
                        round.Markings.Count(
                            m =>
                            m.Participant.Id == participant.Id && m.Judge.Id == judge.Id && m.Dance.Id == dance.Dance.Id);
                    danceIndex++;
                }

                ((object[])result[judgeIndex])[danceIndex] =
                    round.Markings.Count(m => m.Participant.Id == participant.Id && m.Judge.Id == judge.Id);

                judgeIndex++;
            }

            result[judgeIndex] = round.Markings.Count(m => m.Participant.Id == participant.Id);

            Debug.WriteLine("GetMarkings: " + stopwatch.ElapsedMilliseconds);
            return result;
        }

        private static object[] GetMarkingsSum(Round round, Participant participant)
        {
            var judges = participant.Competition.GetJudges();
            var dances = participant.Competition.Dances;

            var result = new object[judges.Count + 1];

            var judgeIndex = 0;
            foreach (var judge in judges)
            {
                result[judgeIndex] = new object[dances.Count + 1];
                var danceIndex = 0;
                foreach (var dance in dances)
                {
                    ((object[])result[judgeIndex])[danceIndex] = null;
                    danceIndex++;
                }

                ((object[])result[judgeIndex])[danceIndex] =
                    round.Markings.Where(m => m.Participant.Id == participant.Id && m.Judge.Id == judge.Id)
                        .Sum(p => p.Mark);

                judgeIndex++;
            }

            result[judgeIndex] = round.Markings.Where(m => m.Participant.Id == participant.Id).Sum(p => p.Mark);

            return result;
        }

        private Ablauf[] GetAblauf(Competition competition)
        {
            var result = new Ablauf[competition.Rounds.Count];
            var index = 0;
            foreach (var round in competition.Rounds.OrderBy(r => r.Number))
            {
                if (round.RoundType.Id == RoundTypes.Final)
                {
                    result[index] = new Ablauf { runde = "F", typ = GetDtvMarkingType(round.MarksInputType) };
                }
                else
                {
                    result[index] = new Ablauf
                                        {
                                            runde = round.Number.ToString(),
                                            typ = GetDtvMarkingType(round.MarksInputType)
                                        };
                }
                index++;
            }

            return result;
        }

        private static string GetDtvMarkingType(MarkingTypes markingTypes)
        {
            switch (markingTypes)
            {
                case MarkingTypes.Marking:
                case MarkingTypes.MarksSumOnly:
                    return "K";
                case MarkingTypes.TeamMatch:
                    return "M";
                case MarkingTypes.Skating:
                    return "P";
                case MarkingTypes.NewJudgingSystemV2:
                    return "JS";
            }

            throw new Exception("Unknown Marking Type for DTV Competition");
        }

        private string[] GetTaenze(ICollection<DancesInCompetition> collection)
        {
            var result = new string[collection.Count];
            var index = 0;

            foreach (var dancesInCompetition in collection)
            {
                var code = this.danceDictionary.FirstOrDefault(d => d.Value == dancesInCompetition.Dance.Id);
                result[index] = code.Key;
                index++;
            }

            return result;
        }

        private static Person[] GetPersonenOfParticipant(Participant participant)
        {
            var result = new Person[2];

            result[0] = new Person
            {
                                geschlecht = "m",
                                id = participant.Couple.MINMan,
                                vorname = participant.Couple.FirstMan,
                                nachname = participant.Couple.LastMan
                            };
            result[1] = new Person
            {
                                geschlecht = "w",
                                id = participant.Couple.MINWoman,
                                vorname = participant.Couple.FirstWoman,
                                nachname = participant.Couple.LastWoman
                            };

            return result;
        }

        private static Funktionaer GetFunktionaerForOfficial(Official official)
        {
            var funktionaer = new Funktionaer
                                  {
                                      id = official.ExternalId,
                                      nachname = official.Lastname,
                                      vorname = official.Firstname,
                                      club = new Club { name = official.Club },
                                      staat = official.Nationality?.IocCode ?? "GER"
                                  };

            return funktionaer;
        }

        private static Ergebnis GetErgebnisOfParticipant(Participant participant)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            // Wenn das Startbuch nicht vom DTV kam sondern selbst angelegt wurde
            // übertragen wir eine null für GesamtPunkte und Platzierungen.
            int? punkteGesamt = null;
            int? platzierungenGesamt = null;
            if (participant.Startbuch != null && !participant.Startbuch.IsSelfGenerated)
            {
                punkteGesamt = participant.NewPoints.HasValue
                                   ? participant.NewPoints.Value
                                   : (participant.OriginalPoints.HasValue ? participant.OriginalPoints.Value : 0);
                platzierungenGesamt = participant.NewPlacings.HasValue
                                          ? participant.NewPlacings.Value
                                          : (participant.OriginalPlacings.HasValue
                                                 ? participant.OriginalPlacings.Value
                                                 : 0);
            }

            var vd = 0;
            if (participant.DisqualificationFlag != DtvDisqualificationFlag.None)
            {
                vd = (int)participant.DisqualificationFlag;
            }

            var climbUpEntry = participant.Competition.ClimbUps.FirstOrDefault(
                c => c.Couple.Id == participant.Couple.Id);

            var ergebnis = new Ergebnis
                               {
                                   vd = vd,
                                   aufstieg = climbUpEntry != null ? (int)climbUpEntry.ClimbupType : 0,
                                   laufzettel = participant.Startbuch != null && participant.Startbuch.PrintLaufzettel,
                                   platzGesamtBis = participant.PlaceTo ?? 0,
                                   platzTurnierBis = participant.PlaceToOwnCompetition,
                                   platzGesamtVon = participant.PlaceFrom ?? 0,
                                   platzTurnierVon = participant.PlaceFromOwnCompetition,
                                   punkte = participant.RankingPoints ?? 0,
                                   punkteGesamt = punkteGesamt,
                                   platzierungenGesamt = platzierungenGesamt
                               };
            Debug.WriteLine("GetErgebnis: " + stopwatch.ElapsedMilliseconds);
            return ergebnis;
        }

        private CompetitionType GetCompetitionType(string dtvCompetitionTypeString)
        {
            if (this.turnierFormDictionary.ContainsKey(dtvCompetitionTypeString))
            {
                var id = this.turnierFormDictionary[dtvCompetitionTypeString];
                return this.context.CompetitionTypes.Single(s => s.Id == id);
            }

            throw new Exception("Unbekannte Turnier-Form " + dtvCompetitionTypeString);
        }

        private Section GetCompetitionSection(string dtvCompetitionSectionString)
        {
            if (dtvCompetitionSectionString == null)
            {
                return this.context.Sections.Single(s => s.Id == 1);
            }

            if (this.TurniertArtDictionary.ContainsKey(dtvCompetitionSectionString))
            {
                var id = this.TurniertArtDictionary[dtvCompetitionSectionString];
                return this.context.Sections.Single(s => s.Id == id);
            }

            throw new Exception("Unbekannte Turnier-Form " + dtvCompetitionSectionString);
        }

        private AgeGroup GetAgeGroup(string dtvClass)
        {
            if (dtvClass == null)
            {
                return this.context.AgeGroups.Single(c => c.Id == 7); // No class
            }

            if (this.altersgruppen.ContainsKey(dtvClass))
            {
                var id = this.altersgruppen[dtvClass];
                return this.context.AgeGroups.Single(c => c.Id == id);
            }

            throw new Exception("Unbekannte Altersgruppe: " + dtvClass);
        }

        private Class GetClass(string dtvClass)
        {
            if (dtvClass == null)
            {
                return this.context.Classes.Single(c => c.Id == 6);
            }

            if (this.klassen.ContainsKey(dtvClass))
            {
                var id = this.klassen[dtvClass];
                return this.context.Classes.Single(c => c.Id == id);
            }

            return this.context.Classes.First();            // throw new Exception("Unbekannte Klasse: " + dtvClass);
        }

        private Couple CreateOrUpdateCouple(string starterDtvId, Starter starter)
        {
            var couple = this.context.Couples.FirstOrDefault(c => c.ExternalId == starterDtvId);

            if (couple == null)
            {
                couple = new Couple
                {
                    ExternalId = starterDtvId,
                    Region = starter.club != null && starter.club.ltv != null
                                         ? starter.club.ltv.name
                                         : "",
                    MINMan = starter.personen[0].id,
                    MINWoman = starter.personen[1].id,
                    WdsfIdMan = starter.personen[0].wdsfMin,
                    WdsfIdWoman = starter.personen[1].wdsfMin,
                    Country = starter.club != null ? starter.club.name : starter.staat,
                    Country2 = starter.staat,
                    IsReadOnly = true
                };

                this.SetNames(couple, starter.personen);

                this.context.Couples.Add(couple);
                // Save to fill the ID:
                this.context.SaveChanges();
            }

            foreach (var meldung in starter.meldungen)
            {
                var dtvCompetitionId = meldung.turnierId.ToString(CultureInfo.InvariantCulture);
                var competition = this.context.Competitions.First(c => c.ExternalId == dtvCompetitionId);
                var dtvId = meldung.turnierId + "_" + starter.id;
                var participant = this.context.Participants.FirstOrDefault(p => p.ExternalId == dtvId);

                if (participant != null)
                {
                    // update status if needed:
                    if (participant.State != CoupleState.Excused)
                    {
                        participant.State = meldung.meldung ? CoupleState.Dancing : CoupleState.Excused;
                    }
                    if (meldung.startsperre)
                    {
                        // we have to remove this couple from the list
                        this.context.Participants.Remove(participant);
                        this.context.SaveChanges();
                    }
                }
                else if (!meldung.startsperre)
                {
                    // Create a new participant for the competition
                    participant = new Participant
                    {
                        Couple = couple,
                        Competition = competition,
                        Number =
                                              meldung.startNr != null
                                                  ? int.Parse(meldung.startNr)
                                                  : couple.NumberInEvent.HasValue
                                                        ? couple.NumberInEvent.Value
                                                        : 0,
                        Class = competition.Class,
                        AgeGroup = competition.AgeGroup,
                        ExternalId = dtvId,
                        State =
                                              meldung.meldung
                                                  ? CoupleState.Dancing
                                                  : CoupleState.Excused
                    };

                    this.context.Participants.Add(participant);
                    this.context.SaveChanges();
                }
            }

            foreach (var startbuch in starter.startbuch.Where(s => s != null))
            {
                var section = this.GetCompetitionSection(startbuch.turnierart);
                var book = couple.Startbooks.FirstOrDefault(b => b.Section.Id == section.Id);

                if (book == null)
                {
                    book = new Startbuch { Couple = couple };
                    this.context.Startbuecher.Add(book);
                    couple.Startbooks.Add(book);

                    this.context.SaveChanges();
                }
                // Set the values:
                book.Section = section;
                book.AgeGroup = this.GetAgeGroup(startbuch.startgruppe);
                book.Class = this.GetClass(startbuch.startklasse);
                book.NextClass = this.GetClass(startbuch.naechsteStartklasse);
                book.OriginalPoints = startbuch.punkte != null ? startbuch.punkte.ist : 0;
                book.TargetPoints = startbuch.punkte != null && startbuch.punkte.ziel.HasValue
                                        ? startbuch.punkte.ziel.Value
                                        : 0;
                book.OriginalPlacings = startbuch.platzierungen != null ? startbuch.platzierungen.ist : 0;
                book.TargetPlacings = startbuch.platzierungen != null && startbuch.platzierungen.ziel.HasValue
                                          ? startbuch.platzierungen.ziel.Value
                                          : 0;
                book.MinimumPoints = startbuch.regeln != null ? startbuch.regeln.minPunkte : 0;
                book.PlacingsUpto = startbuch.regeln != null ? startbuch.regeln.platzierungBis : 0;
                book.Sylabus = startbuch.flags.verwarnungSchrittbegrenzung;
                book.PrintLaufzettel = startbuch.flags.laufzettel;
                book.IsSelfGenerated = startbuch.IsSelfGenerated;
                // Update all participants for this book ...
                var participants =
                    this.context.Participants.Where(
                        p => p.Couple.Id == couple.Id && p.Competition.Section.Id == book.Section.Id);
                foreach (var participant in participants)
                {
                    if (participant.State != CoupleState.Disqualified
                        && participant.State != CoupleState.DroppedOut)
                    {
                        participant.Class = book.Class;
                        participant.OriginalPlacings = book.OriginalPlacings;
                        participant.OriginalPoints = book.OriginalPoints;
                        participant.TargetPlacings = book.TargetPlacings;
                        participant.TargetPoints = book.TargetPoints;
                        participant.MinimumPoints = book.MinimumPoints;
                        participant.PlacingsUpto = book.PlacingsUpto;
                        participant.TargetClass = book.NextClass;
                        participant.SylabusWarning = book.Sylabus;
                    }
                }

                this.context.SaveChanges();
            }


            return couple;
        }

        private void CreateOrUpdateTeam(string starterDtvId, Starter starter)
        {
            // Check, if we have the team:
            var team = this.context.Teams.FirstOrDefault(t => t.ExternalId == starterDtvId);

            if (team == null)
            {
                team = new Team()
                {
                    ExternalId = starterDtvId,
                    TeamName = starter.team.name,
                    TeamCaptn = starter.team.kapitaen
                };

                this.context.Teams.Add(team);
                this.context.SaveChanges();
            }

            // import all persons and update startaufstellung
            foreach (var person in starter.personen)
            {
                var existing = this.context.People.FirstOrDefault(p => p.ExternalId == person.id);

                if (existing == null)
                {
                    existing = new DataModel.Models.Person();
                    this.context.People.Add(existing);
                }

                existing.FirstName = person.vorname;
                existing.LastName = person.nachname;
                existing.Gender = person.geschlecht == "m" ? Gender.Male : Gender.Female;
                existing.ExternalId = person.id;
                existing.Nationality = person.nationalitaet;
                existing.Dancing = person.startaufstellung;
                existing.Team = team;
            }

            // create particispant
            foreach (var meldung in starter.meldungen)
            {
                var dtvId = meldung.turnierId + "_" + starter.id;
                var participant = this.context.Participants.FirstOrDefault(p => p.ExternalId == dtvId);
                var dtvCompetitionId = meldung.turnierId.ToString(CultureInfo.InvariantCulture);
                var competition = this.context.Competitions.First(c => c.ExternalId == dtvCompetitionId);

                if (participant == null)
                {
                    participant = new Participant()
                    {
                        ExternalId = dtvId,
                        Competition = competition,
                        Couple = team,
                        Number = 0,
                        State = meldung.meldung ? CoupleState.Dancing : CoupleState.Excused
                    };

                    this.context.Participants.Add(participant);
                }

                var startNumber = 0;

                if (int.TryParse(meldung.startNr, out startNumber))
                {
                    participant.Number = startNumber;
                }
            }



            this.context.SaveChanges();
        }
    }
}