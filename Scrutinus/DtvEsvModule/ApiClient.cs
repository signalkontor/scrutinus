﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Windows;

using DataModel.Models;

using DtvEsvModule.Model;

using General;

using NLog;

namespace DtvEsvModule
{

    public class ApiResult
    {
        public int code { get; set; }
        public string fehler { get; set; }
        public string detail { get; set; }
    }


    public class ApiClient
    {
        private Logger logger = LogManager.GetCurrentClassLogger();

        private const string SoftwareName = "TPS.net";

        private readonly string baseUrl;

        private readonly string userName;

        private readonly string password;

        private readonly string token = "37f9c37a301583c22288f7019913e559";

        public ApiClient(string baseUrl, string userName, string password)
        {
            this.baseUrl = baseUrl;
            this.userName = userName;
            this.password = password;

            if (baseUrl.ToLower().StartsWith("https:"))
            {
                token = "a5cb9640395ddf844139651737bf0790";
            }
        }

        public IEnumerable<Funktionaer> GetAllFunktionaere()
        {
            var url = "/api/v1/funktionaere";

            return this.GetResource<IEnumerable<Funktionaer>>(url);
        }

        /// <summary>
        /// /api/v1/startliste/veranstaltung/{veranstaltungsId}
        /// </summary>
        /// <param name="eventId"></param>
        /// <returns></returns>
        public Startliste GetStarterByVeranstaltung(int eventId)
        {
            var url = "/api/v1/startliste/veranstaltung/" + eventId;

            return this.GetResource<Startliste>(url);
        }

        //
        public Startliste GetStarterByCompetitionType(string competitionType)
        {
            var url = "/api/v1/startliste/wettbewerbsart/" + competitionType;

            return this.GetResource<Startliste>(url);
        }

        public IEnumerable<EventListElement> GetEvents()
        {
            var url = "/api/v1/veranstaltungen";

            return this.GetResource<List<EventListElement>>(url);
        }

        public Veranstaltung GetVeranstaltung(int eventId)
        {
            var url = "/api/v1/turniere/" + eventId;

            return this.GetResource<Veranstaltung>(url);
        }

        public IEnumerable<Funktionaer> GetFunktionaere()
        {
            var url = "/api/v1/funktionaere";

            return this.GetResource<IEnumerable<Funktionaer>>(url);
        }

        public Funktionaer GetFunktionaer(string dtvId)
        {
            var url = "/api/v1/funktionaer/" + dtvId;

            try
            {
                return this.GetResource<Funktionaer>(url);
            }
            catch (WebException ex)
            {
                if (ex.Response is HttpWebResponse)
                {
                    var response = (HttpWebResponse)ex.Response;
                    if (response.StatusCode == HttpStatusCode.NotFound)
                    {
                        return null;
                    }
                }

                throw;
            }
        }

        public Aufstiegstabelle[] GetAufstiegstabelle()
        {
            var url = "/api/v1/aufstiegstabelle/einzel/" + DateTime.Now.Year;

            try
            {
                return this.GetResource<Aufstiegstabelle[]>(url);
            }
            catch (Exception)
            {
                return new Aufstiegstabelle[0];
            }
        }

        public string[] SaveResult(string json, string checksum, string dtvId)
        {
            var url = "/api/v1/ergebnis/" + dtvId;

            return this.SaveJson(json, url, checksum);
        }

        public void UploadJudgingSheets(IEnumerable<string> files, string dtvId)
        {
            var url = "/api/v1/wrzettelpdf/" + dtvId;

            this.UploadFile(url, files);
        }

        public string[] SendCancledCompetition(AusgefallenesTurnier turnier, string dtvId)
        {
            var url = "/api/v1/ausgefallen/" + dtvId;

            return this.SaveResource(turnier, url, false);
        }

        public string[] UploadDigitalJudgingSheets(DigitalJudgingSheet[] sheets, string dtvId, string round)
        {
            var url = "/api/v1/wrzetteldigi/" + dtvId + "/" + round;

            return this.SaveResource(sheets, url, false);
        }

        private T GetResource<T>(string path)
        {
            using (var client = new WebClient())
            {
                this.SetupProxy(client);

                var byteArray = Encoding.ASCII.GetBytes($"{this.userName}:{this.password}");

                client.Headers.Add("user-agent", $"{SoftwareName}/1.0; Token={token}");
                client.Headers.Add("Authorization", $"Basic {Convert.ToBase64String(byteArray)}");
                var urlToCall = this.baseUrl + path;

                string content = "";

                try
                {
                    content = client.DownloadString(urlToCall);
                }
                catch (WebException webException)
                {
                    string errorJson = "";

                    try
                    {
                        var stream = webException.Response.GetResponseStream();
                        var reader = new StreamReader(stream);
                        errorJson = reader.ReadToEnd();
                        logger.Error(errorJson);

                    } catch(Exception ex) { }

                    if (!string.IsNullOrWhiteSpace(errorJson))
                    {
                        throw new Exception("DTV Server hat einen Fehler gemeldet: " + errorJson);
                    }

                    throw;
                }
               

                if (Directory.Exists(@"C:\Temp\json"))
                {
                    var filename = urlToCall.Replace("/", "_").Replace(":", "");
                    try
                    {
                        File.WriteAllText(@"C:\Temp\json\" + filename, content);
                    }
                    catch (Exception ex)
                    {
                        ScrutinusContext.WriteLogEntry(ex);
                    }
                }

                return JsonHelper.DeserializeData<T>(content);
            }
        }

        private string[] SaveResource(object objectToSave, string path, bool addDtvPruefsumme)
        {
            using (var client = new WebClient())
            {
                this.SetupProxy(client);

                var byteArray = Encoding.ASCII.GetBytes($"{this.userName}:{this.password}");
                client.Headers.Add("user-agent", $"{SoftwareName}/1.0; Token={token}");
                client.Headers.Add("Authorization", $"Basic {Convert.ToBase64String(byteArray)}");
                var urlToCall = this.baseUrl + path;

                var json = JsonHelper.SerializeObject(objectToSave, null);

                if (addDtvPruefsumme)
                {
                    var hash = this.CalculateHash(json);
                    client.Headers.Add("X-DTV-Pruefsumme", hash.Substring(0, 6));
                }

                byte[] returnStr = null;

                try
                {
                    returnStr = client.UploadData(urlToCall, Encoding.UTF8.GetBytes(json));
                    var jsonString = Encoding.UTF8.GetString(returnStr);

                    try
                    {
                        var apiResult = JsonHelper.DeserializeData<ApiResult>(jsonString);

                        return new string[] { apiResult.fehler };
                    }
                    catch (Exception)
                    {
                        return new string[] { jsonString };
                    }
                }
                catch (WebException webException)
                {
                    ScrutinusContext.WriteLogEntry(webException);

                    if (webException.Response != null)
                    {
                        var jsonStream = webException.Response.GetResponseStream();
                        var reader = new StreamReader(jsonStream);
                        var errorJson = reader.ReadToEnd();

                        try
                        {
                            return JsonHelper.DeserializeData<string[]>(errorJson);
                        }
                        catch (Exception ex2)
                        {
                            ScrutinusContext.WriteLogEntry(ex2);
                            return new string[] { "Der DTV Server hat einen Fehler zurück geliefert: " + errorJson };
                        }
                    }

                    return new string[] { "Fehler beim Zugriff auf das DTV Portal: " + webException.Message };
                }

            }
        }

        private string[] SaveJson(string json, string path, string dtvPruefsumme)
        {
            using (var client = new WebClient())
            {
                this.SetupProxy(client);

                var byteArray = Encoding.ASCII.GetBytes($"{this.userName}:{this.password}");
                client.Headers.Add("user-agent", $"{SoftwareName}/1.0; Token={token}");
                client.Headers.Add("Authorization", $"Basic {Convert.ToBase64String(byteArray)}");
                var urlToCall = this.baseUrl + path;

                client.Headers.Add("X-DTV-Pruefsumme", dtvPruefsumme.Substring(0, 6));

                byte[] returnStr = null;

                try
                {
                    returnStr = client.UploadData(urlToCall, Encoding.UTF8.GetBytes(json));
                    var jsonString = Encoding.UTF8.GetString(returnStr);

                    if (jsonString.Contains("Ergebnis wurde gespeichert"))
                    {
                        return new string[1] { jsonString };
                    }


                    var apiResult = JsonHelper.DeserializeData<ApiResult>(jsonString);

                    return new string[] { apiResult.fehler };
                }
                catch (WebException ex)
                {
                    ScrutinusContext.WriteLogEntry(ex);

                    if (ex.Response != null)
                    {
                        var jsonStream = ex.Response.GetResponseStream();
                        var reader = new StreamReader(jsonStream);
                        var errorJson = reader.ReadToEnd();

                        try
                        {
                            return JsonHelper.DeserializeData<string[]>(errorJson);
                        }
                        catch (Exception)
                        {
                            return new string[] { "Der DTV Server hat einen Fehler zurück geliefert: " + errorJson };
                        }
                    }
                    else
                    {
                        return new string[] { "Fehler beim Zugriff auf das DTV Portal: " + ex.Message };
                    }
                }
            }
        }

        public string CalculateHash(string json)
        {
            if (Directory.Exists(@"c:\temp\json"))
            {
                File.WriteAllText(@"c:\temp\json.json", json);
            }
            // SHA-1 Hash berechnen:
            return this.Hash(json);
        }

        public void UploadFile(string path, IEnumerable<string> files)
        {
            using (var client = new WebClient())
            {
                this.SetupProxy(client);

                var byteArray = Encoding.ASCII.GetBytes($"{this.userName}:{this.password}");
                var boundary = "Boundary121314";
                client.Headers.Add("user-agent", $"{SoftwareName}/1.0; Token={token}");
                client.Headers.Add("Authorization", $"Basic {Convert.ToBase64String(byteArray)}");
                client.Headers.Add("Content-Type", $"multipart/form-data; boundary={boundary}");

                var dataStream = new MemoryStream();
                var streamWriter = new StreamWriter(dataStream);

                var urlToCall = this.baseUrl + path;

                var index = 1;
                foreach (var file in files)
                {
                    streamWriter.WriteLine("--" + boundary);
                    streamWriter.WriteLine("Content-Type: application/pdf");
                    streamWriter.WriteLine(string.Format("Content-Disposition: form-data; name=\"wr{0}\"; filename=\"WRZettel{0}.pdf\"", index));
                    streamWriter.WriteLine("");
                    index++;
                    // Add the form-data:
                    var data = File.ReadAllBytes(file);
                    dataStream.Write(data, 0, data.Length);
                }

                streamWriter.WriteLine("--{0}--\r\n", boundary);
                streamWriter.Flush();
                string returnStr = null;

                try
                {
                    returnStr = ASCIIEncoding.ASCII.GetString(client.UploadData(urlToCall, dataStream.GetBuffer()));
                }
                catch (WebException ex)
                {
                    var jsonStream = ex.Response.GetResponseStream();
                    var reader = new StreamReader(jsonStream);
                    var errorJson = reader.ReadToEnd();

                    throw new Exception("Fehler beim Hochladen der Wertungsrichter-Zettel (PDF): " + errorJson);
                }
            }
        }

        private string Hash(string input)
        {
            using (SHA1Managed sha1 = new SHA1Managed())
            {
                var hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(input));
                var sb = new StringBuilder(hash.Length * 2);

                foreach (byte b in hash)
                {
                    // can be "x2" if you want lowercase
                    sb.Append(b.ToString("x2"));
                }

                return sb.ToString();
            }
        }

        private void SetupProxy(WebClient client)
        {
            var proxy = WebRequest.DefaultWebProxy;

            if (proxy != null)
            {
                var proxyUri = proxy.GetProxy(new Uri(this.baseUrl));
                if (proxyUri != null && !proxyUri.AbsoluteUri.StartsWith(this.baseUrl))
                {
                    client.Proxy = new WebProxy() { Address = proxyUri, Credentials = proxy.Credentials };
                }           
            }
        }
    }
}
