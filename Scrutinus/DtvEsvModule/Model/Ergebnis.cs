﻿// // TPS.net TPS8 DtvEsvModule
// // Ergebnis.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;

namespace DtvEsvModule.Model
{

    public class Gesamtergebnis
    {
        public string[] taenze { get; set; }
        public Funktionaer[] wr { get; set; }
        public Funktionaer[] tl { get; set; }
        public Funktionaer[] bs { get; set; }
        public Funktionaer[] ch { get; set; }
        public string vorkommnisse { get; set; }
        public DateTime beginn { get; set; }
        public DateTime ende { get; set; }
        public Ablauf[] ablauf { get; set; }
        public object kombiniertMit { get; set; }
        public int zuschauer { get; set; }
        public StarterMitErgebnis[] starter { get; set; }
        public List<List<List<List<Endrundentabellen>>>> endrundentabellen { get; set; }
        public Skatingtabellen[] skatingtabellen { get; set; }
    }

    public class Skatingtabellen
    {
        public List<List<object>> regel10 { get; set; }
        public List<List<object>> regel11 { get; set; }
    }

    public class DigitalJudgingSheet
    {
        public int? kreuzvorgabeVon { get; set; }
        public int? kreuzvorgabeBis { get; set; }
        public StarterDigitalSheet[] starter { get; set; }
        public string unterschrift { get; set; }
    }

    public class StarterDigitalSheet
    {
        public int startNr { get; set; }
        public string[] wertungen { get; set; }
        public string summe { get; set; }
    }


    public class Ablauf
    {
        public string runde { get; set; }
        public string typ { get; set; }
    }


    public class StarterMitErgebnis
    {
        public int id { get; set; }
        public int startNr { get; set; }
        public Person[] personen { get; set; }
        public string staat { get; set; }
        public int status { get; set; }
        public int meldungsTyp { get; set; }
        public int sterne { get; set; }
        public Ergebnis ergebnis { get; set; }
        public object[] wertung { get; set; }
    }

    public class Ergebnis
    {
        public int platzGesamtVon { get; set; }
        public int platzGesamtBis { get; set; }
        public int? platzTurnierVon { get; set; }
        public int? platzTurnierBis { get; set; }
        public int punkte { get; set; }
        public int? platzierungenGesamt { get; set; }
        public int? punkteGesamt { get; set; }
        public int aufstieg { get; set; }
        public bool laufzettel { get; set; }
        public int vd { get; set; }
    }

    public class Endrundentabellen
    {
        public int anzahl { get; set; }
        public double? summe { get; set; }
    }

    public class AusgefallenesTurnier
    {
        public string vorkommnisse { get; set; }
    }
}
