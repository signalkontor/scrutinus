﻿// // TPS.net TPS8 DtvEsvModule
// // Rangliste.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;

namespace DtvEsvModule.Model
{

    public class Rangliste
    {
        public string id { get; set; }
        public DateTime stand { get; set; }
        public RanglistenStarter[] starter { get; set; }
    }

    public class RanglistenStarter
    {
        public int id { get; set; }
        public Rang rang { get; set; }
        public Person[] personen { get; set; }
        public Club club { get; set; }
        public int punkte { get; set; }
        public int anzahlTurniere { get; set; }
    }

    public class Rang
    {
        public int rl { get; set; }
        public int dm { get; set; }
    }

}
