﻿// // TPS.net TPS8 DtvEsvModule
// // Funktionäre.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

namespace DtvEsvModule.Model
{

    public class FunktionaerListElement
    {
        public string id { get; set; }
        public int wdsfMin { get; set; }
        public int lizenzNr { get; set; }
        public object titel { get; set; }
        public object vorname { get; set; }
        public object nachname { get; set; }
        public Club club { get; set; }
        public string staat { get; set; }
        public string[] lizenzen { get; set; }
    }

    
    public class Funktionaer
    {
        public string id { get; set; }
        public int? wdsfMin { get; set; }
        public int? lizenzNr { get; set; }
        public string titel { get; set; }
        public string vorname { get; set; }
        public string nachname { get; set; }
        public Club club { get; set; }
        public string staat { get; set; }
        public string[] lizenzen { get; set; }
    }


}
