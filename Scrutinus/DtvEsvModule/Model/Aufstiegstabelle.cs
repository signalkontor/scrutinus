﻿// // TPS.net TPS8 DtvEsvModule
// // Aufstiegstabelle.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

namespace DtvEsvModule.Model
{
    public class Aufstiegstabelle
    {
        public string[] ltv { get; set; }
        public int minPunkte { get; set; }
        public Tabellen[] tabellen { get; set; }
    }

    public class Tabellen
    {
        public string[] startgruppen { get; set; }
        public string turnierart { get; set; }
        public Kriterien[] kriterien { get; set; }
    }

    public class Kriterien
    {
        public string klasse { get; set; }
        public string zielklasse { get; set; }
        public int punkte { get; set; }
        public int plaetze { get; set; }
        public int bisPlatz { get; set; }
        public bool doppelstart { get; set; }
    }
}
