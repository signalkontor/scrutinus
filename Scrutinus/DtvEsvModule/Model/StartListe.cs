﻿// // TPS.net TPS8 DtvEsvModule
// // Startliste.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;

namespace DtvEsvModule.Model
{

    public class Startliste
    {
        private int? id { get; set; }
        public int? status { get; set; }
        public string[] nachmeldungen { get; set; }
        public Starter[] starter { get; set; }
    }

    public class Starter
    {
        public int? id { get; set; }
        public Team team { get; set; }
        public Person[] personen { get; set; }
        public Club club { get; set; }
        public string staat { get; set; }
        public Meldung[] meldungen { get; set; }
        public Startbuch[] startbuch { get; set; }
    }

    public class Team
    {
        public string name { get; set; }

        public string kapitaen { get; set; }
    }

    public class Club
    {
        public int? id { get; set; }
        public string name { get; set; }
        public Ltv ltv { get; set; }
    }

    public class Aufstellung
    {
        public int min { get; set; }
        public int max { get; set; }
        public int? ausland { get; set; }
    }

    public class Person
    {
        public string id { get; set; }
        public string titel { get; set; }
        public string vorname { get; set; }
        public string nachname { get; set; }
        public string geschlecht { get; set; }
        public string wdsfMin { get; set; }
        public string nationalitaet { get; set; }
        public bool startaufstellung { get; set; }
    }

    public class Meldung
    {
        public int turnierId { get; set; }
        public bool meldung { get; set; }
        public DateTime bestaetigt { get; set; }
        public string startNr { get; set; }
        public bool startsperre { get; set; }
    }

    public class Startbuch
    {
        public string turnierart { get; set; }
        public string startgruppe { get; set; }
        public object startliga { get; set; }
        public string startklasse { get; set; }
        public string naechsteStartklasse { get; set; }
        public Punkte punkte { get; set; }
        public Platzierungen platzierungen { get; set; }
        public Regeln regeln { get; set; }
        public Flags flags { get; set; }
        public bool IsSelfGenerated { get; set; }
    }

    public class Punkte
    {
        public int ist { get; set; }
        public int? ziel { get; set; }
    }

    public class Platzierungen
    {
        public int ist { get; set; }
        public int? ziel { get; set; }
    }

    public class Regeln
    {
        public int minPunkte { get; set; }
        public int platzierungBis { get; set; }
    }

    public class Flags
    {
        public bool laufzettel { get; set; }
        public bool verwarnungSchrittbegrenzung { get; set; }
    }

}
