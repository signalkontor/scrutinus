﻿// // TPS.net TPS8 DtvEsvModule
// // EventListElement.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;

namespace DtvEsvModule.Model
{
    public class EventListElement
    {
        public int id { get; set; }

        public DateTime datumVon { get; set; }

        public DateTime datumBis { get; set; }

        public string ort { get; set; }

        public string titel { get; set; }
    }


    public class Veranstaltung
    {
        public int id { get; set; }
        public DateTime datumVon { get; set; }
        public DateTime datumBis { get; set; }
        public Turnierstaette turnierstaette { get; set; }
        public Veranstalter veranstalter { get; set; }
        public Ausrichter ausrichter { get; set; }
        public string titel { get; set; }
        public object bemerkungen { get; set; }
        public DtvOfficial[] wertungsrichter { get; set; }
        public DtvOfficial[] funktionaere { get; set; }
        public Flaechen[] flaechen { get; set; }
        public Turnier[] turniere { get; set; }
    }

    public class Turnierstaette
    {
        public string name { get; set; }
        public string anschrift { get; set; }
        public string plz { get; set; }
        public string ort { get; set; }
    }

    public class Veranstalter
    {
        public int id { get; set; }
        public string name { get; set; }
        public Ltv ltv { get; set; }
    }

    public class Ausrichter
    {
        public int id { get; set; }
        public string name { get; set; }
        public Ltv ltv { get; set; }
    }

    public class Flaechen
    {
        public string id { get; set; }
        public string typ { get; set; }
        public float laenge { get; set; }
        public float breite { get; set; }
    }

    public class Turnier
    {
        public int id { get; set; }
        public DateTime datumVon { get; set; }
        public DateTime datumBis { get; set; }
        public string startzeitPlan { get; set; }
        public object startzeitPlanKorrigiert { get; set; }
        public object titel { get; set; }
        public Veranstalter veranstalter { get; set; }
        public Ausrichter ausrichter { get; set; }
        public string flaechenId { get; set; }
        public string wettbewerbsart { get; set; }
        public string turnierform { get; set; }
        public string startgruppe { get; set; }
        public string startklasseLiga { get; set; }
        public string turnierart { get; set; }
        public string[] zulassung { get; set; }
        public bool wanderpokal { get; set; }
        public int turnierrang { get; set; }
        public bool aufstiegsturnier { get; set; }
        public object ranglistenId { get; set; }
        public object wdsfTurnierId { get; set; }
        public string startgebuehr { get; set; }
        public object bemerkungen { get; set; }
        public object wertungsrichter { get; set; }
        public object turnierleiter { get; set; }
        public object beisitzer { get; set; }
        public object chairman { get; set; }
    }


    public class DtvOfficial
    {
        public string id { get; set; }
        public string wdsfMin { get; set; }
        public int lizenzNr { get; set; }
        public string titel { get; set; }
        public string vorname { get; set; }
        public string nachname { get; set; }
        public Club club { get; set; }
        public string staat { get; set; }
        public object[] lizenzen { get; set; }
    }


    public class Ltv
    {
        public int id { get; set; }
        public string name { get; set; }
    }


}
