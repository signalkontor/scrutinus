﻿// // TPS.net TPS8 mobileControl
// // SelectDevices.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows;
using System.Windows.Controls;
using mobileControl.Model;
using mobileControl.ViewModels;

namespace mobileControl
{
    /// <summary>
    /// Interaction logic for SelectDevices.xaml
    /// </summary>
    public partial class SelectDevices : Window
    {
        private readonly SelectDevicesViewModel viewModel;

        public SelectDevices(Competition competition)
        {
            this.InitializeComponent();

            this.viewModel = (SelectDevicesViewModel) this.DataContext;

            // Setup data
            this.viewModel.SelectedDevices = competition.SelectedDevices;
            this.viewModel.Devices.Clear();
            foreach (var device in MainWindow.Context.Devices)
            {
                if (!this.viewModel.SelectedDevices.Contains(device))
                {
                    this.viewModel.Devices.Add(device);    
                }
                
            }
        }

        private void Selector_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            foreach (var item in e.AddedItems)
            {
                var device = item as Device;
                if (device != null)
                {
                    this.viewModel.SelectionListAdd.Add(device);
                }
            }

            foreach (var item in e.RemovedItems)
            {
                var device = item as Device;
                if (device != null)
                {
                    this.viewModel.SelectionListAdd.Remove(device);
                }
            }
        }

        private void SelectdDevices_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            foreach (var item in e.AddedItems)
            {
                var device = item as Device;
                if (device != null)
                {
                    this.viewModel.SelectionListRemove.Add((Device)device);
                }
            }

            foreach (var item in e.RemovedItems)
            {
                var device = item as Device;
                if (device != null)
                {
                    this.viewModel.SelectionListRemove.Remove((Device)device);
                }
            }
        }

        private void CloseOnClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
