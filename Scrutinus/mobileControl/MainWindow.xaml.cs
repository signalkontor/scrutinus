﻿// // TPS.net TPS8 mobileControl
// // MainWindow.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.IO;
using System.Linq;
using System.Messaging;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using com.signalkontor.XD_Server;
using mobileControl.Helper;
using mobileControl.Model;
using mobileControl.Nancy;

namespace mobileControl
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static Model.DataModel Context;

        public MessageQueue _inQueue;

        private readonly Thread _thread;

        private readonly ServerCore serverCore;

        public MainWindow()
        {
            try
            {
                this.InitializeComponent();

                Instance = this;

                AppDomain.CurrentDomain.UnhandledException += this.CurrentDomain_UnhandledException;

                if (!MessageQueue.Exists(@".\private$\tps.ejudge.unprocessed"))
                {
                    MessageQueue.Create(@".\private$\tps.ejudge.unprocessed", false);
                }

                if (!MessageQueue.Exists(@".\private$\tps.ejudge.in"))
                {
                    MessageQueue.Create(@".\private$\tps.ejudge.in", false);
                }

                if (!MessageQueue.Exists(@".\private$\tps.ejudge.out"))
                {
                    MessageQueue.Create(@".\private$\tps.ejudge.out", false);
                }

                // Check, ob Daten-Verzeichnis überhaupt existiert:
                if (!Directory.Exists(mobileControlSettings.Default.DataPath))
                {
                    MessageBox.Show(
                        "Data Directory does not exist, please check the mobilecontrol.exe.config file and set Parameter DataPath");
                }

                Context = (Model.DataModel) this.DataContext;

                this.StartMessageQueue();
                this._thread = new Thread(this.ProcessUnhandeledMessages) {IsBackground = true};
                this._thread.Start();

                var path = mobileControlSettings.Default.XdServerDevicesFile;

                if (path.StartsWith("%Documents%"))
                {
                    path = path.Replace("%Documents%", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments));
                }

                this.serverCore = new ServerCore(mobileControlSettings.Default.XdServerPort,
                    mobileControlSettings.Default.XdServerConfigPort,
                    mobileControlSettings.Default.XdServerAccessPoints,
                    path,
                    mobileControlSettings.Default.XdServerInQueue,
                    mobileControlSettings.Default.XdServerOutQueue);

                this.serverCore.NewLogMessageEvent += (sender, message) => Context.AppendLogMessage(message);

                // Start the Nancy Web-Server
                var uris = NancyHttpServer.GetUriList(mobileControlSettings.Default.NancyPort);
                var nancyServer = new NancyHttpServer(uris, Context, null, Thread.CurrentThread.CurrentCulture);
                nancyServer.StartHost();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                MessageBox.Show(ex.StackTrace);
            }
        }

        public ICommand ClearDeviceCommand { get; set; }

        public static MainWindow Instance { get; private set; }


        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            var ex = e.ExceptionObject as Exception;

            while (ex != null)
            {
                MessageBox.Show(String.Format("unhandelt Error: {0}\r\nStack:\r\n{1}", ex.Message, ex.StackTrace));
                ex = ex.InnerException;
            }
        }

        /// <summary>
        /// Starts reading from the message queue
        /// </summary>
        private void StartMessageQueue()
        {
            try
            {
                this._inQueue = new MessageQueue(@".\private$\TPS.eJudge.in");
                this._inQueue.ReceiveCompleted += this.QueueReceiveCompleted;
                this._inQueue.Formatter = new XmlMessageFormatter(new[] { typeof(TMQeJSMsg) });
                this._inQueue.BeginReceive();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }

        private void ProcessUnhandeledMessages()
        {
            var errorQueue = new MessageQueue(@".\private$\TPS.eJudge.unprocessed");
            errorQueue.Formatter = new XmlMessageFormatter(new[] { typeof(TMQeJSMsg) });
            while (true)
            {           
                try
                {
                    var msg = errorQueue.Receive();
                    if (msg.Body is TMQeJSMsg)
                    {
                        var tps = (TMQeJSMsg)msg.Body;
                        
                        // Check the time stamp. If the message is to old,
                        // we can delete it (TTL exeeded)
                        // This code checks for not older then 2 hours.
                        if (tps.TimeStamp.AddHours(2) > DateTime.Now)
                        {
                            this.Dispatcher.Invoke(
                                DispatcherPriority.Normal,
                                (Action) (() => { this.ProcessMessage(tps); })
                                );
                        }
                    }
                    Thread.Sleep(10000);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
            }
            
        }

        private void QueueReceiveCompleted(object sender, ReceiveCompletedEventArgs e)
        {
            try
            {
                var msg = this._inQueue.EndReceive(e.AsyncResult);

                if (msg.Body is TMQeJSMsg)
                {
                    var tps = (TMQeJSMsg)msg.Body;

                    this.Dispatcher.Invoke(
                        DispatcherPriority.Normal,
                        (Action)(() => this.ProcessMessage(tps))
                        );

                }
            }
            catch(MessageQueueException e1)
            {
                Console.WriteLine(e1.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }

            this._inQueue.BeginReceive();
        }

        private void ProcessMessage(TMQeJSMsg tps)
        {
            var dev = Context.Devices.SingleOrDefault(d => d.Id == tps.Sender);
            if (dev == null)
            {
                dev = new Device
                          {
                              Battery = 0,
                              Id = tps.Sender,
                              State = DeviceStates.StartNoData,
                              Online = true,
                              IsCharging = false,
                              CompetitionLoaded = "",
                              TimeStamp = tps.TimeStamp
                          };
                
                Context.AddDevice(dev);
                Context.SaveAvailableDevices(mobileControlSettings.Default.PathToTPSSettings);
            }
            else
            {
                dev.TimeStamp = tps.TimeStamp;
            }

            if(tps.TargetId == 500)
            {
                Console.WriteLine(tps.MsgData);
            }

            if(tps.TargetId !=  2)
            {
                dev.CompetitionLoaded = tps.DoCmd;
            }

            // ToDo: was machen wir, wenn das Fenster nicht offen ist. Nachricht einfach verwerfen???
 	        if(tps.DoCmd != null && Context.Windows != null && Context.Windows.ContainsKey(tps.DoCmd))
 	        {
 	            Context.Windows[tps.DoCmd].HandleMessage(tps);
 	        }

            switch(tps.TargetId)
            {
                case 1:
                    this.UpdateDeviceState(dev, tps.MsgData);
                    dev.Online = true;
                    break;
                case 2:
                    dev.Online = true;
                    break;
                case 3:
                    dev.Online = false;
                    break;

            }
        }

        private void UpdateDeviceState(Device dev, string stateData)
        {
            var data = TpsHelper.TPSSplitter(stateData);
 
            dev.Battery = Int32.Parse(data[0]);
            var WindowId = Int32.Parse(data[1]);
            var WindowState = Int32.Parse(data[2]);
            dev.IsCharging = data[6] != "battery";

            // Anhängig von beiden Werten setzen wir unseren internen Status
            switch(WindowId)
            {
                case 0: // Start-Screen
                    switch(WindowState)
                    {
                        case 0: dev.State = DeviceStates.StartNoData;
                            break;
                        case 1: dev.State = DeviceStates.StartDataLoaded;
                            break;
                        case 2: dev.State = DeviceStates.StartRelease;
                            break;
                        case 3: dev.State = DeviceStates.StartOldData;
                            break;
                    }
                    break;
                case 1: // Eingabe Vor/Zwischenrunden
                    switch(WindowState)
                    {
                        case 0:
                        case 1:
                            dev.State = DeviceStates.MarkingQualification;
                            break;
                        case 2: dev.State = DeviceStates.QualfificationHelpmarks;
                            break;
                        case 8: dev.State = DeviceStates.Signature;
                            break;
                    }
                    break;
                case 2: // Finale
                    switch(WindowState)
                    {
                        case 0: dev.State = DeviceStates.MarkingFinal;
                            break;
                        case 1: dev.State = DeviceStates.FinalAllPlacesSet;
                            break;
                        case 2: dev.State = DeviceStates.FinalDanceConfirmed;
                            break;
                        case 8: dev.State = DeviceStates.Signature;
                            break;
                    }
                    break;
                case 4:
                    dev.State = DeviceStates.LanguageSelection;
                    break;
                case 5:
                    dev.State = DeviceStates.Message;
                    break;


            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (!(sender is Button))
            {
                return;
            }

            var button = (Button) sender;
            switch (button.Name)
            {
                case "BtnShutdown":
                    this.Shutdown();
                    break;
                case "BtnSettings":
                    var settings = new Settings() {Owner = this};
                    settings.ShowDialog();
                    break;
                case "BtnOpen":
                    this.OpenCompetition();
                    break;

            }
        }

        private void OpenCompetition()
        {
            var oc = new OpenCompetition(FileEncodingFactory.GetFileEncoding()) { Owner = this };
            var res = oc.ShowDialog();
            if(res.HasValue && res.Value == true)
            {
                if (Context.Windows.ContainsKey(oc.Competition.Id))
                {
                    var existing = Context.Windows[oc.Competition.Id];
                    existing.BringIntoView();
                   
                    return;
                }
                // Window does not yet exist:
                var win = new CompetitionWindow(oc.Competition.Id);
                Context.Pages.Add(new PageModel(){ CompetitionId = oc.Competition.Id, TabCaption = oc.Competition.Title, IsActive = true, TabContent = win});               
            }

        }

        private void Shutdown()
        {
            // Send Shutdown Command to all devices
            this.Close();
        }

        private void ListDeviceState_OnBeginningEdit(object sender, DataGridBeginningEditEventArgs e)
        {
            e.Cancel = true;
        }

        private void BtnEditJudges_OnClick(object sender, RoutedEventArgs e)
        {
            var dlg = new EditJudgesDialog() { Owner = this };
            dlg.ShowDialog();
        }

        private void BtnSelectUris_OnClick(object sender, RoutedEventArgs e)
        {
            var dialog = new SelectNancyUris() {Owner = this};
            dialog.ShowDialog();
        }

        private void TabItem_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            var tabItem = e.Source as TabItem;

            if (tabItem == null)
            {
                return;
            }

            if (Mouse.PrimaryDevice.LeftButton == MouseButtonState.Pressed)
            {
                DragDrop.DoDragDrop(tabItem, tabItem, DragDropEffects.All);
            }
        }


        private void TabItem_Drop(object sender, DragEventArgs e)
        {
            var tabItemTarget = e.Source as TabItem;

            var tabItemSource = e.Data.GetData(typeof(TabItem)) as TabItem;

            if (tabItemTarget == null || tabItemSource == null)
            {
                return;
            }

            if (!tabItemTarget.Equals(tabItemSource))
            {
                var sourcePage = Context.Pages.FirstOrDefault(p => p.TabContent == tabItemSource.Content);
                var targetPage = Context.Pages.FirstOrDefault(p => p.TabContent == tabItemTarget.Content);

                if (sourcePage == null || targetPage == null)
                {
                    return;
                }

                var sourceIndex = Context.Pages.IndexOf(sourcePage);
                var targetIndex = Context.Pages.IndexOf(targetPage);

                if (sourceIndex < 0 || targetIndex < 0)
                {
                    return;
                }

                var firstPage = Context.Pages[sourceIndex];
                Context.Pages.RemoveAt(sourceIndex);
                Context.Pages.Insert(targetIndex, firstPage);
            }
        }
    }
}
