﻿// // TPS.net TPS8 mobileControl
// // AddMoveCouple.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using mobileControl.Helper;
using mobileControl.Model;
using mobileControl.XDServer;

namespace mobileControl
{
    /// <summary>
    /// Interaction logic for AddMoveCouple.xaml
    /// </summary>
    public partial class AddMoveCouple : Window
    {
        private readonly Competition _competition;
        private readonly bool _modeAdd;

        public AddMoveCouple(Competition competition, bool AddCouple)
        {
            this.InitializeComponent();

            this._competition = competition;

            this.ListDance.ItemsSource = competition.Dances;
            this.ListDance.DisplayMemberPath = "Name";
            this.ListDance.SelectedValuePath = "SortOrder";

            if(AddCouple)
            {
                this._modeAdd = true;
                this.Title = "Add a Couple";
            }
            else
            {
                this._modeAdd = false;
                this.Title = "Move a Couple";
            }

            // Number of heats?
            var heatCount = 0;
            if (competition.HeatStrings.Count > 0)
            {
                var data = TpsHelper.TPSSplitter(competition.HeatStrings[0]);
                heatCount = data.Count() - 1;
            }
            if (heatCount > 0)
            {
                var list = new List<string>();
                for (var i = 1; i <= heatCount; i++)
                {
                    list.Add(String.Format("{0}. Heat", i));
                }
                this.ListHeat.ItemsSource = list;
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            if (this.ListDance.SelectedIndex == -1)
            {
                this.ListDance.BorderBrush = new SolidColorBrush(Colors.Red);
                return;
            }

            if(this.ListHeat.SelectedIndex == -1)
            {
                this.ListHeat.BorderBrush = new SolidColorBrush(Colors.Red);
                return;
            }

            if(this._modeAdd)
            {
                int number;
                if(!Int32.TryParse(this.txtNumber.Text, out number))
                {
                    this.txtNumber.BorderBrush = new SolidColorBrush(Colors.Red);
                    return;
                }

                var couple = this._competition.Couples.SingleOrDefault(c => c.Number == number);
                if (couple == null)
                {
                    this._competition.Couples.Add(new Couple(this._competition.Type == 1 ? null : this._competition.Dances) {Number = number});
                }
                else
                {
                    MessageBox.Show("Couple allredy exists, but will resend couple to the devices",
                                    "Allready existing couple", MessageBoxButton.OK, MessageBoxImage.Information);
                }

                CommunicationHandler.AddCouple(this._competition, this.txtNumber.Text, this.ListDance.SelectedIndex + 1, this.ListHeat.SelectedIndex + 1);
            }
            else
            {
                int number;
                if (!Int32.TryParse(this.txtNumber.Text, out number))
                {
                    this.txtNumber.BorderBrush = new SolidColorBrush(Colors.Red);
                    return;
                }
                CommunicationHandler.MoveCouple(this._competition, this.txtNumber.Text, this.ListDance.SelectedIndex + 1, this.ListHeat.SelectedIndex + 1);
            }

            this.Close();
        }
    }
}
