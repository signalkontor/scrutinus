﻿// // TPS.net TPS8 mobileControl
// // ChangeJudgeData.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH


using System;
using System.Collections.ObjectModel;
using System.Windows;
using mobileControl.Model;
using mobileControl.XDServer;

namespace mobileControl
{
    /// <summary>
    /// Interaction logic for ChangeJudgeData.xaml
    /// </summary>
    public partial class ChangeJudgeData : Window
    {
        private readonly Competition competition;
        private readonly ObservableCollection<Device> devices;
        private readonly Judge judge;

        public ChangeJudgeData(Judge judge, Competition competition)
        {
            this.InitializeComponent();
            try
            {
                this.judge = judge;
                this.competition = competition;
                this.DataContext = judge;

                this.devices = new ObservableCollection<Device>();
                foreach (var device in MainWindow.Context.Devices)
                {
                    this.devices.Add(device);
                }
                // Remove the devices in use:
                foreach (var j in competition.Judges)
                {
                    if (j.Device != null)
                    {
                        this.devices.Remove(j.Device);
                    }
                }
                this.ListDevices.ItemsSource = this.devices;
            }
            catch (Exception)
            {
                //what shall we do?
            }

        }

        private void btnAssignNew_Click(object sender, RoutedEventArgs e)
        {
            if (this.ListDevices.SelectedItem == null)
            {
                return;
            }

            if (this.judge.Device != null)
            {
                this.devices.Add(this.judge.Device);
            }

            this.judge.Device = (Device) this.ListDevices.SelectedItem;
            this.judge.DeviceId = this.judge.Device.Id;
            this.devices.Remove(this.judge.Device);
        }

        private void btnClearDevice_Click(object sender, RoutedEventArgs e)
        {
            CommunicationHandler.ClearDevice(this.judge.DeviceId, this.competition.Id);
        }

        private void btnSendData_Click(object sender, RoutedEventArgs e)
        {
            CommunicationHandler.SendData(this.competition, this.judge, false);
            CommunicationHandler.SendStart(this.judge.DeviceId, this.competition.Id);
        }

        private void bntMoveJudge_Click(object sender, RoutedEventArgs e)
        {
            var moveWin = new MoveJudge(this.competition, this.judge);
            moveWin.ShowDialog();
        }

        private void bntClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnDeleteAssign_OnClick(object sender, RoutedEventArgs e)
        {
            if (this.judge.Device != null)
            {
                this.devices.Add(this.judge.Device);
            }

            this.judge.DeviceId = null;
            this.judge.Device = null;
            
        }
    }
}
