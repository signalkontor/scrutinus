﻿// // TPS.net TPS8 mobileControl
// // EditJudgesDialog.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.ComponentModel;
using System.Windows;
using mobileControl.ViewModels;

namespace mobileControl
{
    /// <summary>
    /// Interaction logic for EditJudgesDialog.xaml
    /// </summary>
    public partial class EditJudgesDialog : Window
    {
        private readonly EditOfficalsViewModel viewModel;

        public EditJudgesDialog()
        {
            this.InitializeComponent();
            this.viewModel = (EditOfficalsViewModel)this.DataContext;
        }

        private void EditJudgesDialog_OnClosing(object sender, CancelEventArgs e)
        {
            if (this.viewModel.DataChanged)
            {
                var res = MessageBox.Show(
                    "Data has changed, close anyway?",
                    "Data Changed",
                    MessageBoxButton.YesNo,
                    MessageBoxImage.Question);

                if (res == MessageBoxResult.No)
                {
                    e.Cancel = true;
                }
            }
        }
    }
}
