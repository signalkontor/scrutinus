﻿// // TPS.net TPS8 mobileControl
// // FileEncodingFactory.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Text;

namespace mobileControl.Helper
{
    internal class FileEncodingFactory
    {
        public static Encoding GetFileEncoding()
        {
            if (mobileControlSettings.Default.FileCodePage > 0)
            {

                var codePage = mobileControlSettings.Default.FileCodePage;
                return Encoding.GetEncoding(codePage);
            }

            return Encoding.UTF8;
        }
    }
}
