﻿// // TPS.net TPS8 mobileControl
// // DeviceAssigning.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using mobileControl.Model;

namespace mobileControl
{
    /// <summary>
    /// Interaction logic for DeviceAssigning.xaml
    /// </summary>
    public partial class DeviceAssigning : Window
    {
        private readonly ObservableCollection<Judge> _judges;
        private readonly ObservableCollection<Device> devices;

        public DeviceAssigning(ObservableCollection<Judge> judges )
        {
            this.InitializeComponent();
            this._judges = judges;
            this.ListJudges.ItemsSource = judges;
            this.devices = new ObservableCollection<Device>();
            foreach (var device in MainWindow.Context.Devices)
            {
                this.devices.Add(device);
            }
            // Remove the devices in use:
            foreach (var judge in judges)
            {
                if (judge.Device != null)
                {
                    this.devices.Remove(judge.Device);
                }
            }
            this.ListDevices.ItemsSource = this.devices;
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }


        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var name = "";

            if (!(sender is Button))
            {
                if (sender is DataGrid)
                {
                    name = ((DataGrid) sender).Name;
                }
            }
            else
            {
                name = ((Button) sender).Name;
            }

            switch (name)
            {
                case  "ListDevices":
                case "btnAssignFrom":
                    if(this.ListDevices.SelectedItem == null)
                    {
                        break;
                    }
                    var index = this.devices.IndexOf((Device) this.ListDevices.SelectedItem);
                    var remove = new List<Device>();
                    foreach (var judge in this._judges)
                    {
                        judge.DeviceId = this.devices[index].Id;
                        judge.Device = this.devices[index];
                        remove.Add(this.devices[index]);
                        index++;
                        if (index == this.devices.Count)
                        {
                            foreach (var dev in remove)
                            {
                                this.devices.Remove(dev);
                            }
                            break;
                        }
                        
                    }
                    foreach (var dev in remove)
                    {
                        this.devices.Remove(dev);
                    }
                    break;
                case "btnUnassignAll":
                    foreach (var judge in this._judges)
                    {
                        if (judge.Device != null)
                        {
                            this.devices.Add(judge.Device);
                        } 
                        
                        judge.DeviceId = null;
                        judge.Device = null;
                    }
                    break;
                case "btnAssign":
                    if (this.ListDevices.SelectedItem == null)
                    {
                        break;
                    }
                    if (this.ListJudges.SelectedItem == null)
                    {
                        break;
                    }
                    var judge2 = (Judge) this.ListJudges.SelectedItem;
                    var device = (Device) this.ListDevices.SelectedItem;
                    judge2.DeviceId = device.Id;
                    judge2.Device = device;
                    this.devices.Remove(device);
                    break;
                case "btnUnsassign":
                    if (this.ListJudges.SelectedItem == null)
                    {
                        break;
                    }
                    var judge3 = (Judge) this.ListJudges.SelectedItem;
                    this.devices.Add(judge3.Device);
                    judge3.DeviceId = null;
                    judge3.Device = null;
                    break;
            }
        }
    }
}
