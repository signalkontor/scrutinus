﻿// // TPS.net TPS8 mobileControl
// // CompetitionWindow.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using com.signalkontor.XD_Server;
using General.Extensions;
using mobileControl.Helper;
using mobileControl.Model;
using mobileControl.Printing;
using mobileControl.XDServer;

namespace mobileControl
{
    /// <summary>
    ///     Interaction logic for CompetitionWindow.xaml
    /// </summary>
    public partial class CompetitionWindow : ContentControl
    {
        private Competition competition;

        public CompetitionWindow(string competitionId)
        {
            this.InitializeComponent();

            this.LoadData(competitionId);

            this.CompetitionId = competitionId;

            if (MainWindow.Context.Windows.ContainsKey(competitionId))
            {
                MainWindow.Context.Windows.Remove(competitionId);
            }

            MainWindow.Context.Windows.Add(competitionId, this);
            this.DanceSelect.ItemsSource = this.competition.Dances;
            this.DanceSelect.SelectedIndex = 0;

            this.CreateBindings();

            this.RoundName.Content = this.competition.Title + " " + this.competition.Round;

            // Do our Data Bindings
            this.JudgesGrid.ItemsSource = this.competition.Judges;
            this.ResultGrid.ItemsSource = this.competition.Couples;

            if (this.competition.Type == 1)
            {
                this.SelectDanceStackPanel.Visibility = Visibility.Collapsed;
            }

            MainWindow.Context.DeviceStateChanged += this.Context_DeviceStateChanged;

            // check if all judges have an device.
            var allHaveDevice = !this.competition.Judges.Any(j => string.IsNullOrWhiteSpace(j.DeviceId));

            this.competition.ReadyToLoad = allHaveDevice;
        }

        public string CompetitionId { get; set; }

        private void CompetitionWindowClosed(object sender, EventArgs e)
        {
            this.competition.SaveCompetitionData();
            // Detach our handlers:
            this.competition = null;
            MainWindow.Context.DeviceStateChanged -= this.Context_DeviceStateChanged;
        }

        private void CreateBindings()
        {
            foreach (var judge in this.competition.Judges)
            {
                var binding = new Binding("SumMarking")
                                  {
                                      Mode = BindingMode.OneWay,
                                      ConverterParameter =
                                          this.competition.Type == 1
                                              ? judge.Sign
                                              : judge.Sign + "_" + this.DanceSelect.SelectedValue,
                                      Converter = new DictionaryIntItemConverter()
                                  };
                this.ResultGrid.Columns.Add(
                    new DataGridTextColumn { Header = judge.Sign, Binding = binding, CanUserSort = false });

                if (this.competition.Type == 12 || this.competition.Type == 14)
                {
                    var bindingA = new Binding("MarkingsA")
                                       {
                                           Mode = BindingMode.OneWay,
                                           ConverterParameter = judge.Sign,
                                           Converter = new DictionaryDoubleItemConverter()
                                       };

                    var bindingB = new Binding("MarkingsB")
                                       {
                                           Mode = BindingMode.OneWay,
                                           ConverterParameter = judge.Sign,
                                           Converter = new DictionaryDoubleItemConverter()
                                       };
                    this.ResultGrid.Columns.Add(
                        new DataGridTextColumn { Header = judge.Sign + " / A", CanUserSort = false, Binding = bindingA });
                    this.ResultGrid.Columns.Add(
                        new DataGridTextColumn { Header = judge.Sign + " / B", CanUserSort = false, Binding = bindingB });
                }
            }
            // Binding for Total-Sum
            var bindingTotal = new Binding("TotalSumMarks") { Mode = BindingMode.OneWay };

            this.ResultGrid.Columns.Add(new DataGridTextColumn { Header = "Total", Binding = bindingTotal });
        }

        private void Context_DeviceStateChanged(object sender, Device device)
        {
            if (device.State == DeviceStates.StartRelease || device.State == DeviceStates.MarkingQualification)
            {
                return;
            }

            Judge judge = null;

            try
            {
                judge = this.competition.Judges.SingleOrDefault(d => d.DeviceId == device.Id);
            }
            catch (Exception)
            {
                MessageBox.Show("Assigned Devices seem not to be unique!");
                return;
            }

            if (device.State == DeviceStates.StartNoData && mobileControlSettings.Default.AutoLoad)
            {
                if (device.CompetitionLoaded == this.competition.Id)
                {
                    return;
                }

                if (!this.competition.ReadyToLoad)
                {
                    return;
                }
                // Suchen, haben wir das Gerät in unserer Liste?

                if (judge != null)
                {
                    CommunicationHandler.SendData(this.competition, judge, false);
                    if (mobileControlSettings.Default.AutoStart)
                    {
                        CommunicationHandler.SendStart(device.Id, this.competition.Id);
                    }
                }
            }

            if (device.State == DeviceStates.Signature || device.State == DeviceStates.StartOldData)
            {
                if (this.competition.Judges.All(j => j.SignatureReceived))
                {
                    // write a file to indicate that all signatures are ready
                    // TPS.net can use this flag to read the date
                    var path = string.Format("{0:0000}_{1}_##_done.dat",
                        this.competition.Id,
                        this.competition.Round);

                    File.WriteAllText(path, "OK");
                }
            }

            if (device.State == DeviceStates.StartOldData && mobileControlSettings.Default.AutoPrint
                && device.CompetitionLoaded == this.competition.Id)
            {
                if (judge != null)
                {
                    this.PrintJudgingSheet(judge);
                }
            }

            if (device.State == DeviceStates.StartOldData && mobileControlSettings.Default.AutoClear)
            {
                if (judge != null && judge.CompleteMarking && judge.SignatureReceived)
                {
                    // Check markings are here and complete
                    if (this.CheckMarks(judge.Device))
                    {
                        CommunicationHandler.ClearDevice(judge.Device.Id, this.competition.Id);
                        judge.CompleteMarking = false;
                        judge.SignatureReceived = false;
                        this.competition.ReadyToLoad = false; // wir wollen nicht noch mal neu geladen werden...
                    }
                }
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="device"></param>
        /// <returns></returns>
        private bool CheckMarks(Device device)
        {
            // get the judge:
            var judge = this.competition.Judges.SingleOrDefault(j => j.DeviceId == device.Id);
            if (judge == null)
            {
                return false;
            }
            // Prüfen, ob die Anzahl der Marks den Vorgaben entsprechen:
            if (!this.competition.MarksStrict)
            {
                return true;
            }

            // ToDo: hier sollten noch alle Kreuze gezählt werden. Dies könnte bei der Verarbeitung
            // des Gesamtresulates erfolgen
            var expectedMin = this.competition.MarksFrom * this.competition.Dances.Count;

            if (judge.SumOfMarks >= expectedMin)
            {
                return true;
            }

            return false;
        }

        private void PrintJudgingSheet(Judge judge)
        {
            var printer = new JudgingSheet();
            printer.Print(this.competition, judge);
        }

        public string GetDataForJudgePin(string pin, string deviceId)
        {
            var judge = this.competition.Judges.FirstOrDefault(j => j.Pin == pin);

            if (judge != null && string.IsNullOrEmpty(judge.DeviceId))
            {
                var device = MainWindow.Context.Devices.SingleOrDefault(d => d.Id == deviceId);
                judge.Device = device;
                judge.DeviceId = device != null ? device.Id : "";

                var data = CommunicationHandler.GetDataForJudge(this.competition, judge, true);

                // check if all judges have a device:
                if (this.competition.Judges.All(j => j.Device != null))
                {
                    this.competition.SaveCompetitionData();
                }

                return data;
            }

            return null;
        }

        public string GetMarkingsForJudge(string pin)
        {
            var judge = this.competition.Judges.FirstOrDefault(j => j.Pin == pin);

            if (judge != null)
            {
                return CommunicationHandler.GetMarksForJudge(this.competition, judge);
            }

            return null;
        }

        public void HandleMessage(TMQeJSMsg Message)
        {
            Debug.WriteLine(
                "Device: " + Message.Sender + ", Message: " + Message.MsgData + "Target: " + Message.TargetId);

            switch (Message.TargetId)
            {
                case 1:
                    this.UpdateStateDevice(Message);
                    break;
                case 11:
                case 21:
                    this.SetMarking(Message);
                    break;
                case 12:
                case 22:
                    this.UpdateCompleteMarking(Message);
                    break;
                case 23:
                    this.UpdateSingleMarksShowdanceFinalWdc(Message);
                    break;
                case 24:
                    this.UpdateCompleteMarksFinalShowdanceWDC(Message);
                    break;
                case 31:
                    this.ProcessSignatureMessage(Message);
                    break;
                case 35:
                    this.SetJudgeFromDeviceMessage(Message);
                    break;
            }

            if (Message.TargetId != 1)
            {
                var device = MainWindow.Context.Devices.SingleOrDefault(d => d.Id == Message.Sender);
                device.LastMessageType = Message.TargetId;
            }
        }

        private void UpdateCompleteMarking(TMQeJSMsg Message)
        {
            var judge = this.competition.Judges.SingleOrDefault(d => d.Device != null && d.Device.Id == Message.Sender);

            if (judge == null)
            {
                return;
            }

            if (Message.TargetId == 12)
            {
                this.SetTotalMarking(Message);
            }
            else
            {
                this.SetMarkingFinal(Message);
            }

            this.competition.SaveResults();
            judge.CompleteMarking = true;
        }

        private void UpdateSingleMarksShowdanceFinalWdc(TMQeJSMsg message)
        {
            var decChar = Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator;

            var judge = this.competition.Judges.SingleOrDefault(d => d.Device != null && d.Device.Id == message.Sender);

            if (judge == null)
            {
                return;
            }

            var dataMarks = TpsHelper.TPSSplitter(message.MsgData);
            var marksShow = dataMarks[1].Split(';');
            var cNo = int.Parse(dataMarks[0]);
            var couple = this.competition.Couples.SingleOrDefault(c => c.Number == cNo);

            var markA = marksShow[0].SaveParse();
            var markB = marksShow[1].SaveParse();
            var place = int.Parse(marksShow[2]);
            var dance = this.competition.Dances[0].Code;

            couple.UpdateMarkingShowFinal(this.competition, dance, judge.Sign, markA, markB, place);
        }

        private void UpdateCompleteMarksFinalShowdanceWDC(TMQeJSMsg message)
        {
            var decChar = Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator;

            var judge = this.competition.Judges.SingleOrDefault(d => d.Device != null && d.Device.Id == message.Sender);

            if (judge == null)
            {
                return;
            }

            var dataMarks2 = TpsHelper.TPSSplitter(message.MsgData);
            var count = int.Parse(dataMarks2[0]);

            for (var i = 0; i < count; i++)
            {
                var marksShow2 = dataMarks2[i * 2 + 2].Split(';');
                var cNo2 = int.Parse(dataMarks2[i * 2 + 1]);
                var couple2 = this.competition.Couples.Single(c => c.Number == cNo2);
                var markA2 = marksShow2[0].SaveParse();
                var markB2 = marksShow2[1].SaveParse();
                var place2 = int.Parse(marksShow2[2]);
                var dance2 = this.competition.Dances[0].Code;
                couple2.UpdateMarkingShowFinal(this.competition, dance2, judge.Sign, markA2, markB2, place2);
            }
        }

        private void ProcessSignatureMessage(TMQeJSMsg Message)
        {
            var judge = this.competition.Judges.SingleOrDefault(d => d.Device != null && d.Device.Id == Message.Sender);
            if (judge == null)
            {
                return;
            }

            // Lets save it:
            var data = TpsHelper.TPSSplitter(Message.MsgData);

            judge.Signature = new List<Coordinate>();
            for (var i = 3; i < data.Length; i++)
            {
                var c = TpsHelper.TPSSplitter(data[i]);
                if (c.Length > 3)
                {
                    judge.Signature.Add(
                        new Coordinate
                            {
                                X1 = double.Parse(c[0]),
                                Y1 = double.Parse(c[1]),
                                X2 = double.Parse(c[2]),
                                Y2 = double.Parse(c[3])
                            });
                }
            }
            if (this.competition.Type == 2)
            {
                var marks =
                    this.competition.Couples.Sum(
                        c => c.Judgements.Count(j => j.Judge.Sign == judge.Sign && j.Value > 0));
                var should = this.competition.Dances.Count * this.competition.Couples.Count;
                // Just to make sure we do not clear device automatically while we have not the complete result
                judge.CompleteMarking = marks == should;
            }

            try
            {
                this.competition.SaveJudgeData();
                this.competition.SaveResults();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Could not save signature because the file was in use");
            }


            // this is for testing -> so we can be sure nothing get's los
            if (mobileControlSettings.Default.SaveJudgingSheets)
            {
                var printer = new JudgingSheet();
                printer.Print(
                    this.competition,
                    judge,
                    false,
                    mobileControlSettings.Default.PathToSaveJudgingSheets + "\\" + this.competition.Id + "_"
                    + judge.Sign + ".xps");
            }
            judge.SignatureReceived = true; // Everything is done, so now set to true---
        }

        private void SetJudgeFromDeviceMessage(TMQeJSMsg Message)
        {
            var sign = Message.MsgData;

            if (Message.Sender == "offline")
            {
                return;
            }

            var newDevice = MainWindow.Context.Devices.SingleOrDefault(d => d.Id == Message.Sender);

            if (newDevice == null)
            {
                newDevice = new Device
                                {
                                    Battery = 0,
                                    CompetitionLoaded = Message.DoCmd,
                                    Id = Message.Sender,
                                    IsCharging = false,
                                    LastMessageType = 0,
                                    Online = true,
                                    State = DeviceStates.StartRelease,
                                    TimeStamp = DateTime.Now
                                };
                MainWindow.Context.Devices.Add(newDevice);
            }

            var judge = this.competition.Judges.SingleOrDefault(j => j.Sign == sign);
            if (judge != null)
            {
                judge.DeviceId = newDevice.Id;
                judge.Device = newDevice;

                this.competition.SaveCompetitionData();
            }
            else
            {
                MessageBox.Show(string.Format("Received a message from unknown judge with sign {0}", sign));
            }
        }

        private void SetTotalMarking(TMQeJSMsg Message)
        {
            var judge = this.competition.Judges.SingleOrDefault(j => j.DeviceId == Message.Sender);
            var data = TpsHelper.TPSSplitter(Message.MsgData);
            judge.SumOfMarks = 0;

            var couples = int.Parse(data[0]);
            for (var i = 0; i < couples; i++)
            {
                var number = int.Parse(data[i * 2 + 1]);
                var couple = this.competition.Couples.SingleOrDefault(c => c.Number == number);
                if (couple == null)
                {
                    continue; // Problem: Ein Paar das wir nicht kennen???
                }
                // Alle Wertungen des WR's löschen, diese werden hier neu gesetzt.
                // Könnte man das sicherer machen? Wenn Die Kiste hier abtsürzt, ist alles verloren.
                // Oder besser: von einem WR die Wertungen ...
                couple.TotalSumMarks = 0;
                couple.Judgements.RemoveAll(d => d.Judge == judge);
                foreach (var key in couple.SumMarking.Keys)
                {
                    if (couple.SumMarking[key].ContainsKey(judge.Sign))
                    {
                        couple.SumMarking[key].Remove(judge.Sign);
                    }
                }

                // Now, get the marking from the complete result:
                var marking = TpsHelper.TPSSplitter(data[i * 2 + 2]);
                for (var dance = 0; dance < marking.Length - 1; dance++)
                {
                    couple.UpdateMarking(
                        this.competition,
                        this.competition.Dances[dance].Code,
                        judge.Sign,
                        marking[dance]);
                    if (marking[dance] == "X")
                    {
                        judge.SumOfMarks++;
                    }
                }
            }
        }

        private void SetMarkingFinal(TMQeJSMsg Message)
        {
            var data = TpsHelper.TPSSplitter(Message.MsgData);
            var judge = this.competition.Judges.SingleOrDefault(j => j.Device != null && j.Device.Id == Message.Sender);
            if (judge == null)
            {
                return;
            }
            var num = int.Parse(data[0]);
            for (var i = 0; i < num; i++)
            {
                var index = i * 2 + 1;
                int cNo;
                if (data[index].Contains(" "))
                {
                    var d = data[index].Split(' ');
                    cNo = int.Parse(d[1]);
                }
                else
                {
                    cNo = int.Parse(data[index]);
                }
                
                var judgements = TpsHelper.TPSSplitter(data[index + 1]);
                var couple = this.competition.Couples.SingleOrDefault(c => c.Number == cNo);
                if (couple == null)
                {
                    return;
                }

                couple.Judgements.RemoveAll(d => d.Judge.Sign == judge.Sign);
                if (couple.SumMarking.ContainsKey(judge.Sign))
                {
                    couple.SumMarking.Remove(judge.Sign);
                }

                for (var dance = 0; dance < judgements.Length - 1; dance++)
                {
                    couple.UpdateMarking(
                        this.competition,
                        this.competition.Dances[dance].Code,
                        judge.Sign,
                        judgements[dance]);
                }
            }
        }

        private void UpdateStateDevice(TMQeJSMsg message)
        {
            var device = MainWindow.Context.Devices.SingleOrDefault(d => d.Id == message.Sender);
            if (device == null)
            {
                return;
            }
            var data = TpsHelper.TPSSplitter(message.MsgData);
            var index = int.Parse(data[3]);

            device.UpdateState(
                index > 0 ? this.competition.Dances[int.Parse(data[3]) - 1].Name : "",
                int.Parse(data[4]),
                int.Parse(data[5]));
        }

        private void SetMarking(TMQeJSMsg Message)
        {
            // Message -> Get Judge, get Dance
            var judge = this.competition.Judges.SingleOrDefault(j => j.DeviceId == Message.Sender);
            var data = TpsHelper.TPSSplitter(Message.MsgData);

            int number = 0;

            if (data[0].Contains(" "))
            {
                var d = data[0].Split(' ');
                number = int.Parse(d[1]);
            }
            else
            {
                number = int.Parse(data[0]);
            }

            var danceNo = int.Parse(data[1]);
            var couple = this.competition.Couples.SingleOrDefault(c => c.Number == number);
            if (judge == null || couple == null)
            {
                return; // -> Kennen wir nicht, können wir nichts mit anfangen
            }
            couple.UpdateMarking(this.competition, this.competition.Dances[danceNo - 1].Code, judge.Sign, data[2]);
        }

        private void LoadWT(string file)
        {
            var stream = new StreamReader(file, FileEncodingFactory.GetFileEncoding(), true);
            var useEncryption = false;
            var lineCount = 0;
            byte[] encryptionArray = { 5, 2, 6, 12, 11, 16, 21 };
            while (!stream.EndOfStream)
            {
                var line = stream.ReadLine();
                if (line.StartsWith("use encryption"))
                {
                    line = stream.ReadLine();
                    useEncryption = true;
                }
                // Encryption of this file ...
                if (useEncryption)
                {
                    var newLine = "";
                    var index2 = lineCount % 6;
                    for (var i = 0; i < line.Length; i++)
                    {
                        newLine += (char)((byte)line[i] - encryptionArray[index2]);
                        index2++;
                        if (index2 >= encryptionArray.Length)
                        {
                            index2 = 0;
                        }
                    }
                    line = newLine;
                }

                var data = TpsHelper.TPSSplitter(line);
                var number = int.Parse(data[0]);
                var couple = this.competition.Couples.SingleOrDefault(c => c.Number == number);
                if (couple == null)
                {
                    couple = new Couple(this.competition.Dances) { Number = number };
                    this.competition.Couples.Add(couple);
                }
                var numJudges = int.Parse(data[1]);
                var index = 2;
                for (var i = 0; i < numJudges; i++)
                {
                    var judge = this.competition.Judges.SingleOrDefault(j => j.Sign == data[index]);

                    if (judge == null)
                    {
                        MessageBox.Show(
                            "It looks like there are old judgements available but for annother competition. Please clear the competition folder");
                        return;
                    }

                    var marks = TpsHelper.TPSSplitter(data[index + 1]);
                    for (var dance = 0; dance < this.competition.Dances.Count; dance++)
                    {
                        couple.UpdateMarking(
                            this.competition,
                            this.competition.Dances[dance].Code,
                            judge.Sign,
                            marks[dance]);
                    }
                    index += 3;
                }
                lineCount++;
            }
            stream.Close();
        }

        private void LoadData(string Id)
        {
            // Load Competition Data
            var file = string.Format("{0}\\{1}_Td.dat", mobileControlSettings.Default.DataPath, Id);
            var stream = new StreamReader(file, FileEncodingFactory.GetFileEncoding(), true);
            this.competition = new Competition(Id, stream.ReadLine(), false);
            stream.Close();

            file = string.Format("{0}\\{1}_Wr.dat", mobileControlSettings.Default.DataPath, Id);
            stream = new StreamReader(file, FileEncodingFactory.GetFileEncoding(), true);
            this.competition.Judges = new ObservableCollection<Judge>();
            while (!stream.EndOfStream)
            {
                var judge = new Judge(this.competition.Id, stream.ReadLine());
                this.competition.Judges.Add(judge);
                if (!string.IsNullOrWhiteSpace(judge.DeviceId))
                {
                    var dev = MainWindow.Context.Devices.SingleOrDefault(d => d.Id == judge.DeviceId);

                    judge.Device = dev;
                }
            }

            stream.Close();

            // Load the couples
            this.competition.Couples = new ObservableCollection<Couple>();

            
            LoadCoupleData(this.competition.Id);

            // Load Dances
            file = string.Format("{0}\\{1}_Tanz.dat", mobileControlSettings.Default.DataPath, this.competition.Id);
            var counter = 1;
            stream = new StreamReader(file, FileEncodingFactory.GetFileEncoding(), true);
            while (!stream.EndOfStream)
            {
                var data = TpsHelper.TPSSplitter(stream.ReadLine());
                this.competition.Dances.Add(new Dance { Code = data[0], Name = data[1], SortOrder = counter });
                counter++;
            }
            stream.Close();
            // Load Results/Markings if Available:
            file = string.Format("{0}\\{1}_Wt.dat", mobileControlSettings.Default.DataPath, this.competition.Id);
            if (File.Exists(file))
            {
                try
                {
                    this.LoadWT(file);
                }
                catch (Exception)
                {
                    MessageBox.Show("Could not load judgements, maybe old data exists that does not match?");
                }
                
            }
            // Load Pin's when existing:
            var pinFile = mobileControlSettings.Default.PathToTPSSettings + "\\PinsJudges.csv";
            if (File.Exists(pinFile))
            {
                stream = new StreamReader(pinFile);
                while (!stream.EndOfStream)
                {
                    var data = stream.ReadLine().Split(';');
                    var judge = this.competition.Judges.SingleOrDefault(j => j.Sign == data[0]);
                    if (judge != null)
                    {
                        judge.Pin = data[2];
                        if (data.Length > 3 && !string.IsNullOrEmpty(data[3]))
                        {
                            var device = MainWindow.Context.Devices.SingleOrDefault(d => d.Id == data[3]);
                            if (device != null)
                            {
                                judge.Device = device;
                                judge.DeviceId = data[3];
                            }
                        }
                    }
                }
                stream.Close();
            }
            // Als letztes brauchen wir noch die Rundenauslosung für die einzelnen Paare:
            // Wenn es die Datei nicht gibt, ist es wohl eine Finale ...
            file = string.Format("{0}\\{1}_Rl.dat", mobileControlSettings.Default.DataPath, this.competition.Id);
            if (!File.Exists(file))
            {
                this.competition.HeatStrings = null;
                return;
            }
            stream = new StreamReader(file, FileEncodingFactory.GetFileEncoding(), true);
            while (!stream.EndOfStream)
            {
                this.competition.HeatStrings.Add(stream.ReadLine());
            }
            stream.Close();

            this.competition.ReadSelectdDevices(MainWindow.Context);
        }

        private void LoadCoupleData(string competitionId)
        {
            var file = string.Format("{0}\\{1}_Sl.dat", mobileControlSettings.Default.DataPath, competitionId);

            var stream = new StreamReader(file, FileEncodingFactory.GetFileEncoding(), true);
            while (!stream.EndOfStream)
            {
                if (this.competition.Type == 1)
                {
                    this.competition.Couples.Add(new Couple(stream.ReadLine(), null, competitionId));
                }
                else
                {
                    this.competition.Couples.Add(new Couple(stream.ReadLine(), this.competition.Dances, competitionId));
                }
            }

            stream.Close();
        }

        private void CheckCanLoad()
        {
            this.competition.ReadyToLoad =
                this.competition.Judges.All(judge => !string.IsNullOrWhiteSpace(judge.DeviceId));
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (!(sender is Button))
            {
                return;
            }

            var button = (Button)sender;

            switch (button.Name)
            {
                case "btnAssign":
                    var win = new DeviceAssigning(this.competition.Judges) { Owner = MainWindow.Instance };
                    win.ShowDialog();
                    this.CheckCanLoad();
                    this.competition.SaveCompetitionData();
                    break;
                case "btnSelectDevives":
                    if (this.competition.Judges.Any(j => string.IsNullOrEmpty(j.Pin)))
                    {
                        MessageBox.Show(
                            "Not all judges have a PIN-Code set. Please check PinsJudges.csv or set PINs for Judges in TPS.net");
                        break;
                    }
                    var selectDevicesDialog = new SelectDevices(this.competition) {Owner = MainWindow.Instance};
                    selectDevicesDialog.ShowDialog();
                    this.competition.SaveCompetitionData();
                    break;
                case "btnSettings":
                    var winSetting = new SettingsCompetition(this.competition) {Owner = MainWindow.Instance};
                    winSetting.ShowDialog();
                    break;
                case "btnSetJudge":
                    var winMove = new MoveJudge(this.competition, null) {Owner = MainWindow.Instance};
                    winMove.ShowDialog();
                    break;
                case "btnAddCouple":
                    var winAdd = new AddMoveCouple(this.competition, true) {Owner = MainWindow.Instance};
                    winAdd.ShowDialog();
                    break;
                case "btnMoveCouple":
                    var winMoveCouple = new AddMoveCouple(this.competition, false) {Owner = MainWindow.Instance};
                    winMoveCouple.ShowDialog();
                    break;
                case "btnRemoveCouple":
                    var winRemove = new RemoveCouple(this.competition) {Owner = MainWindow.Instance};
                    winRemove.ShowDialog();
                    break;
                case "btnClear":
                    if (MessageBox.Show("Are you sure to clear all devices", "Clear devices?", MessageBoxButton.OKCancel) 
                            == MessageBoxResult.Cancel)
                    {
                        return;
                    }

                    this.competition.ReadyToLoad = false; // sonst senden wir uns gleich wieder die Daten ...
                    CommunicationHandler.ClearDevices(this.competition);
                    break;
                case "btnSendData":
                    CommunicationHandler.SendData(this.competition);
                    if (mobileControlSettings.Default.AutoStart)
                    {
                        CommunicationHandler.SendStart(this.competition);
                    }
                    break;
                case "btnSaveCmd":
                    CommunicationHandler.SendSaveCmd(this.competition);
                    break;
                case "btnStart":
                    CommunicationHandler.SendStart(this.competition);
                    break;
                case "btnAddCompetition":
                    var openCompetitionDialog = new OpenCompetition(FileEncodingFactory.GetFileEncoding()) { Owner = null };
                    var res = openCompetitionDialog.ShowDialog();
                    if (res.HasValue && res.Value == true)
                    {
                        this.LoadCoupleData(openCompetitionDialog.Competition.Id);
                    }

                    break;
            }
        }

        private void JudgesGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            // Wir zeigen einen neuen Dialog für den angeklickten WR an:
            // wer war angeklickt?
            var judge = (Judge)this.JudgesGrid.SelectedItem;
            var win = new ChangeJudgeData(judge, this.competition);
            win.ShowDialog();
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            this.competition.SaveCompetitionData();
        }

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            // Noch mal speichern:
            try
            {
                this.Save_Click(null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error while saving data: " + ex.Message);
            }
            // Beim MainWindow abmelden
        }

        private void Print_Click(object sender, RoutedEventArgs e)
        {
            var task = Task.Factory.StartNew(new Action(() =>
                        {
                            var sheet = new JudgingSheet();
                            sheet.Print(this.competition, this.Dispatcher);
                        }));

        }

        private void DanceSelect_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // Change the Bindings of our grid
            foreach (var judge in this.competition.Judges)
            {
                var binding = new Binding("SumMarking")
                                  {
                                      Mode = BindingMode.OneWay,
                                      ConverterParameter =
                                          this.competition.Type == 1
                                              ? judge.Sign
                                              : judge.Sign + "_" + this.DanceSelect.SelectedValue,
                                      Converter = new DictionaryIntItemConverter()
                                  };

                var column = (DataGridTextColumn)this.ResultGrid.Columns.SingleOrDefault(c => c.Header.ToString() == judge.Sign);
                if (column == null)
                {
                    continue;
                }

                column.Binding = binding;
            }
        }

        private void CompetitionWindow_OnClosing(object sender, CancelEventArgs e)
        {
            try
            {
                this.Save_Click(null, null);
                MainWindow.Context.Windows.Remove(this.competition.Id);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error while saving data: " + ex.Message);
            }
        }

        private void ResultGrid_OnBeginningEdit(object sender, DataGridBeginningEditEventArgs e)
        {
            // We do not allow edit
            e.Cancel = true;
        }

        private void OnClickClose(object sender, RoutedEventArgs e)
        {
            if (
                this.competition.Judges.Any(
                    j =>
                    j.Device != null && j.Device.State == DeviceStates.StartOldData
                    && j.Device.CompetitionLoaded == this.competition.Id))
            {
                var result = MessageBox.Show(
                    "Should the devices get cleared?",
                    "Clear devices?",
                    MessageBoxButton.YesNo);

                if (result == MessageBoxResult.Yes)
                {
                    this.competition.ReadyToLoad = false; // sonst senden wir uns gleich wieder die Daten ...
                    CommunicationHandler.ClearDevices(this.competition);
                }
            }
            var page = MainWindow.Context.Pages.SingleOrDefault(p => p.CompetitionId == this.competition.Id);
            MainWindow.Context.Pages.Remove(page);
            if (MainWindow.Context.Windows.ContainsKey(this.competition.Id))
            {
                MainWindow.Context.Windows.Remove(this.competition.Id);
            }

            this.CompetitionWindowClosed(sender, e);
        }

        private void OnTestDataClicked(object sender, RoutedEventArgs e)
        {
            var place = 1;

            foreach (var couple in this.competition.Couples)
            {
                foreach (var dance in this.competition.Dances)
                {
                    foreach (var judge in this.competition.Judges)
                    {
                        couple.UpdateMarking(this.competition, dance.Code, judge.Sign, place.ToString());
                    }
                }

                place++;
            }
        }
    }
}