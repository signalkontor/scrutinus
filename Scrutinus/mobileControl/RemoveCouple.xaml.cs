﻿// // TPS.net TPS8 mobileControl
// // RemoveCouple.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using mobileControl.Model;
using mobileControl.XDServer;

namespace mobileControl
{
    /// <summary>
    /// Interaction logic for RemoveCouple.xaml
    /// </summary>
    public partial class RemoveCouple : Window
    {
        private readonly Competition _competition;

        public RemoveCouple(Competition competition)
        {
            this.InitializeComponent();

            this._competition = competition;

            this.ListDance.ItemsSource = competition.Dances;
            this.ListDance.DisplayMemberPath = "Name";
            this.ListDance.SelectedValuePath = "SortOrder";
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            if (this.ListDance.SelectedIndex == -1)
            {
                this.ListDance.BorderBrush = new SolidColorBrush(Colors.Red);
                return;
            }

            int number;
            if (!Int32.TryParse(this.txtNumber.Text, out number))
            {
                this.txtNumber.BorderBrush = new SolidColorBrush(Colors.Red);
                return;
            }
            var couple = this._competition.Couples.SingleOrDefault(n => n.Number == number);

            if(couple == null)
            {
                MessageBox.Show("Couple not found in list but remove command will send (again?) to the devices",
                                "Missing", MessageBoxButton.OK, MessageBoxImage.Information);
            }

            CommunicationHandler.RemoveCouple(this._competition, this.txtNumber.Text, this.ListDance.SelectedIndex + 1);

            // Remove couple from list only when first dance was selected, otherwise the couple could have marks
            if (couple != null && this.ListDance.SelectedIndex == 0)
            {
                this._competition.Couples.Remove(couple);
            }

            this.Close();
        }
    }
}
