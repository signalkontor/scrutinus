﻿// // TPS.net TPS8 mobileControl
// // ScrutinusUserMapper.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using Nancy;
using Nancy.Authentication.Forms;
using Nancy.Security;
using Nancy.TinyIoc;

namespace NancyServer.Authentification
{
    internal class ScrutinusUserMapper : IUserMapper
    {
        public IUserIdentity GetUserFromIdentifier(Guid identifier, NancyContext context)
        {
            var userService = TinyIoCContainer.Current.Resolve<INancyUserManager>();

            return userService.GetUserIdentity(identifier);
        }
    }
}
