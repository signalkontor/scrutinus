﻿// // TPS.net TPS8 mobileControl
// // HttpServer.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Net.Sockets;
using System.Windows;
using Nancy.Hosting.Self;
using NancyServer;

namespace mobileControl.Nancy
{
    public class NancyHttpServer
    {
        private readonly NancyHost host;

        private bool isRunning = false;

        public NancyHttpServer(Uri[] uris, Model.DataModel context, INancyUserManager userManager, CultureInfo culture)
        {
            var hostConfigs = new HostConfiguration();
            CurrentCultureInfo = culture;
            hostConfigs.UrlReservations.CreateAutomatically = true;
            hostConfigs.RewriteLocalhost = true;

            // this is needed in 
            hostConfigs.AllowChunkedEncoding = false;

            Context = context;

            this.host = new NancyHost(new ApplicationBootstrapper(userManager), hostConfigs, uris);
        }

        internal static Model.DataModel Context { get; set; }

        internal static CultureInfo CurrentCultureInfo { get; set; }

        public bool IsRunning
        {
            get
            {
                return this.isRunning;
            }
        }

        public void StartHost()
        {
            try
            {
                this.host.Start();
                this.isRunning = true;
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("HTTP Server could not started, try to run the application as Administrator! " + ex.Message);
            }
        }

        public void StopHost()
        {
            this.host.Stop();
            this.isRunning = false;
        }

        public static Uri[] GetUriList(int port)
        {
            var uriParams = new List<Uri>();
            var hostName = Dns.GetHostName();

            // Host name URI
            var hostNameUri = string.Format("http://{0}:{1}", Dns.GetHostName(), port);
            uriParams.Add(new Uri(hostNameUri));

            // Host address URI(s)
            var hostEntry = Dns.GetHostEntry(hostName);
            foreach (var ipAddress in hostEntry.AddressList)
            {
                if (ipAddress.AddressFamily == AddressFamily.InterNetwork)  // IPv4 addresses only
                {
                    var addrBytes = ipAddress.GetAddressBytes();
                    var hostAddressUri = string.Format("http://{0}.{1}.{2}.{3}:{4}",
                    addrBytes[0], addrBytes[1], addrBytes[2], addrBytes[3], port);
                    uriParams.Add(new Uri(hostAddressUri));
                }
            }

            // Localhost URI
            uriParams.Add(new Uri(string.Format("http://localhost:{0}", port)));
            return uriParams.ToArray();
        }
    }
}
