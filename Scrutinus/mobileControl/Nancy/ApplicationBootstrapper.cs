﻿// // TPS.net TPS8 mobileControl
// // ApplicationBootstrapper.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using Nancy;
using Nancy.Authentication.Forms;
using Nancy.Bootstrapper;
using Nancy.Conventions;
using Nancy.TinyIoc;
using NancyServer;
using NancyServer.Authentification;

namespace mobileControl.Nancy
{
    public class ApplicationBootstrapper : DefaultNancyBootstrapper
    {
        public ApplicationBootstrapper(INancyUserManager userManager)
        {
            TinyIoCContainer.Current.Register<INancyUserManager>(userManager);
        }

        protected override void ConfigureConventions(NancyConventions nancyConventions)
        {
            base.ConfigureConventions(nancyConventions);
            
            nancyConventions.StaticContentsConventions.Add(StaticContentConventionBuilder.AddDirectory("content", "content"));
            nancyConventions.StaticContentsConventions.Add(StaticContentConventionBuilder.AddDirectory("scripts", "scripts"));

        }

        protected override void ApplicationStartup(TinyIoCContainer container, IPipelines pipelines)
        {
            container.Register<IUserMapper>(new ScrutinusUserMapper());

            base.ApplicationStartup(container, pipelines);

            // Enable Forms
            var formsAuthConfiguration =
                new FormsAuthenticationConfiguration()
                {
                    RedirectUrl = "~/Login/login",
                    UserMapper = container.Resolve<IUserMapper>(),
                };

            FormsAuthentication.Enable(pipelines, formsAuthConfiguration);

            // This is just for login purpose:
            pipelines.BeforeRequest += (ctx) =>
                {
                    Console.WriteLine(
                        "[{0}] {1}: {2}",
                        DateTime.Now.ToShortTimeString(),
                        ctx.Request.UserHostAddress,
                        ctx.Request.Path);
                    return null; 
                };
        }
    }
}
