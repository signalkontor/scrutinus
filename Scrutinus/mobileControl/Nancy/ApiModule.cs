﻿// // TPS.net TPS8 mobileControl
// // ApiModule.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Text;
using General;
using Nancy;

namespace mobileControl.Nancy
{
    public class ApiModule : NancyModule
    {
        public ApiModule()
        {
            this.Get["/GetData/{Pin}"] = request =>
                {
                    var pin = request.Pin.ToString();
                    var deviceId = this.Request.Query["device"];
                    // Try to get the Judge Data:

                    var data = "";

                    foreach (var window in NancyHttpServer.Context.Windows.Values)
                    {
                        data = window.GetDataForJudgePin(pin, deviceId);

                        if (data != null)
                        {
                            data = data.Replace('|', '-');
                            data = window.CompetitionId + "|" + data + "|" + window.GetMarkingsForJudge(pin);
                            return data;
                        }
                    }

                    return HttpStatusCode.NotFound;
                };
        }

        public Response GetJsonReponse(object obj)
        {
            var json = JsonHelper.SerializeObject(obj);

            var jsonBytes = Encoding.UTF8.GetBytes(json);
            return new Response
            {
                ContentType = "application/json",
                Contents = s => s.Write(jsonBytes, 0, jsonBytes.Length)
            };
        }
    }
}
