﻿// // TPS.net TPS8 mobileControl
// // Settings.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows;

namespace mobileControl
{
    /// <summary>
    /// Interaction logic for Settings.xaml
    /// </summary>
    public partial class Settings : Window
    {
        public Settings()
        {
            this.InitializeComponent();

            this.DataContext = mobileControlSettings.Default;
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            mobileControlSettings.Default.AutoLoad = this.chkAutoLoad.IsChecked.HasValue && this.chkAutoLoad.IsChecked.Value;
            mobileControlSettings.Default.AutoPrint = this.chkAutoPrint.IsChecked.HasValue && this.chkAutoPrint.IsChecked.Value;
            mobileControlSettings.Default.AutoStart = this.chkAutoStart.IsChecked.HasValue && this.chkAutoStart.IsChecked.Value;

            mobileControlSettings.Default.Save();

            this.Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
