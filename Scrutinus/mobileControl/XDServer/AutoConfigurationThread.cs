﻿// // TPS.net TPS8 mobileControl
// // AutoConfigurationThread.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using mobileControl;

namespace com.signalkontor.XD_Server
{
    internal class AutoConfigThread
    {
        private readonly List<String> _accessPoints = new List<string>();
        private readonly string _autoconfigfile;
        private readonly Dictionary<string, string> _deviceList;
        private readonly int _firstDevice = 130;
        private readonly int _localPort;
        private readonly Thread _thread;

        private readonly Action<string> loggingAction;

        private readonly Action<string, IPAddress> onLoginAction;
        private int _currentDevice;

        public AutoConfigThread(
            string accessPoints,
            Action<string, IPAddress> onLoginAction,
            Action<string> loggingAction)
            : this(
                mobileControlSettings.Default.XdServerConfigPort,
                mobileControlSettings.Default.XdServerDevicesFile,
                130,
                accessPoints,
                mobileControlSettings.Default.DataPath,
                onLoginAction,
                loggingAction)
        {
            
        }

        public AutoConfigThread(int localPort, string configFile, int firstDevice, string accessPoints, string dataPath, Action<string, IPAddress> OnLoginAction, Action<string> LoggingAction)
        {
            this._localPort = localPort;
            this._autoconfigfile = configFile;
            this._firstDevice = firstDevice;
            this.onLoginAction = OnLoginAction;
            this.loggingAction = LoggingAction;

            var ap = accessPoints.Split(',');

            foreach (var t in ap)
            {
                this._accessPoints.Add(t.Trim().ToLower());
            }

            this._deviceList = new Dictionary<string, string>();
            this._currentDevice = this._firstDevice;
            this._thread = new Thread(this.Run) { IsBackground = true };
            this.LoadDeviceList();
            this._thread.Start();
        }

        private void LoadDeviceList()
        {
            if (!File.Exists(this._autoconfigfile))
            {
                this.loggingAction("No devices.txt found for Auto Config Thread");
                return;
            }
            var stream = new StreamReader(this._autoconfigfile);
            while (!stream.EndOfStream)
            {
                var line = stream.ReadLine();

                if (line == null)
                {
                    continue;
                }

                var data = line.Split(new[] { '=' });
                
                if (data.Length != 2)
                {
                    continue;
                }

                this._deviceList.Add(data[0], data[1]);

                // current Device hochzählen ...
                int intId;
                var id = data[1].StartsWith("eJS") ? data[1].Substring(3) : data[1];

                if (Int32.TryParse(id, out intId))
                {
                    if (this._currentDevice <= intId)
                    {
                        this._currentDevice = intId + 1;
                    }
                }
            }
            stream.Close();
        }

        private void SaveDeviceList()
        {
            lock (this._deviceList)
            {
                var writer = new StreamWriter(this._autoconfigfile);
                foreach (var key in this._deviceList.Keys)
                {
                    writer.WriteLine(key + "=" + this._deviceList[key]);
                }
                writer.Close();
            }
        }

        private string FindNextId()
        {
            var id = this._firstDevice;

            while (true)
            {
                var devId = string.Format("eJS{0}", id);

                if (this._deviceList.ContainsValue(devId))
                {
                    id++;
                }
                else
                {
                    return devId;
                }
            }
        }

        private void Run()
        {
            // UDP Socket aufmachen und auf Verbindungen warten ...
            var udpclient = new UdpClient(this._localPort, AddressFamily.InterNetwork) { EnableBroadcast = true };
            var remoteIPEndPoint = new IPEndPoint(0, 0);

            while (true)
            {
                try
                {
                    var buffer = udpclient.Receive(ref remoteIPEndPoint);
                    // Wir brauchen nun einen String
                    var data = Encoding.Unicode.GetString(buffer, 0, buffer.Length);
                    this.loggingAction(string.Format("Packet von: {0}: {1}", remoteIPEndPoint, data));
                    char[] sep = { ';' };
                    var s = data.Split(sep);
                    // Damit es keine Vertipper gibt vergleichen wir Caseinsensitiv
                    if (this._accessPoints.Contains(s[0].ToLower()) || this._accessPoints.Contains("*"))
                    {
                        if (this._accessPoints.Contains("*"))
                        {
                            this.loggingAction("* => Accept any Access Points");
                        }

                        lock (this._deviceList)
                        {
                            string deviceName;
                            // OK, wir sind zuständig, also schauen, ob Device bekannt.
                            if (this._deviceList.ContainsKey(s[1]))
                            {   // ja, bekannt ...
                                deviceName = this._deviceList[s[1]];
                            }
                            else
                            {   // nein, also neues device anlegen
                                deviceName = this.FindNextId();
                                this._deviceList.Add(s[1], deviceName);
                                this.SaveDeviceList();
                            }
                            this.SendConfig(remoteIPEndPoint, deviceName, mobileControlSettings.Default.NancyPort);
                            this.OnDeviceLogon(deviceName, remoteIPEndPoint.Address);
                        }
                    }
                }
                catch (Exception e)
                {
                    this.loggingAction(e.Message);
                    this.loggingAction("Error sending config" + e.Message);
                }
            }
        }


        private void OnDeviceLogon(string deviceName, IPAddress ipAddeAddress)
        {
            // Report the new device to the application
            if (this.onLoginAction != null)
            {
                this.onLoginAction(deviceName, ipAddeAddress);
            }
        }

        private void SendConfig(IPEndPoint remoteIPEndPoint, string device, int httpPort)
        {
            var client = new UdpClient();
            var enc = new UnicodeEncoding();
            var b = enc.GetBytes(string.Format("{0};{1}", device, httpPort));

            remoteIPEndPoint.Port = 9095;
            client.Send(b, b.Length, remoteIPEndPoint);

            this.loggingAction("Paket gesendet an: " + remoteIPEndPoint + ": " + device);
        }
    }
}
