﻿// // TPS.net TPS8 mobileControl
// // CommunicationHandler.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Linq;
using System.Messaging;
using com.signalkontor.XD_Server;
using mobileControl.Model;

namespace mobileControl.XDServer
{
    internal class CommunicationHandler
    {
        private static CommunicationHandler _instance;
        private readonly object _lockObj;
        private readonly MessageQueue _outQueue;

        private CommunicationHandler()
        {
            this._lockObj = new object();

            this._outQueue = new MessageQueue(".\\private$\\TPS.eJudge.out");
            var formatter = new XmlMessageFormatter(new[] { typeof(TMQeJSMsg) });
            this._outQueue.Formatter = formatter;

        }

        private static CommunicationHandler Instance
        {
            get
            {
                if(_instance == null)
                {
                    _instance = new CommunicationHandler();
                }
                return _instance;
            }
        }


        public static void SendData(Competition competition)
        {
            if (competition.SelectedDevices.Any() && competition.Judges.All(j => j.Device == null))
            {
                // we use PINs and send this data to any judge
                var dummyJudge = new Judge("", "", "", "");
                foreach (var selectedDevice in competition.SelectedDevices)
                {
                    dummyJudge.Device = selectedDevice;
                    dummyJudge.DeviceId = selectedDevice.Id;
                    SendData(competition, dummyJudge, true);
                    SendStart(competition);
                    SendStart(selectedDevice.Id, competition.Id);
                }

                return;
            }
            // Ok, we do it without pin
            foreach (var judge in competition.Judges)
            {
                SendData(competition, judge, false);
            }
        }

        public static void SendData(Competition competition, Judge judge, bool usePin)
        {
            SendData(competition, judge, competition.Type == 1 ? 101 : 111, usePin);
        }

        public static void SendData(Competition competition, Judge judge, int targetId, bool usePin)
        {
            var data = GetDataForJudge(competition, judge, usePin);

            Instance.SendMessage(judge.DeviceId, targetId, competition.Id, data);

            // Do we have results to send?
            if (competition.Couples.Count(c => c.Judgements.Count(j => j.Judge == judge && j.Value > 0) > 0) > 0)
            {
                data = GetMarksForJudge(competition, judge);

                data += ";" + GetPinCodeString(competition);

                Instance.SendMessage(judge.DeviceId, competition.Type == 1 ? 102 : 112, competition.Id, data);
            }
        }

        private static string GetPinCodeString(Competition competition)
        {
            var pinCodes = "";

            foreach (var j in competition.Judges)
            {
                pinCodes += string.Format(";{0};{1};{2};{3}", j.Pin, j.Sign, j.FirstName, j.LastName);
            }

            // PinCode is starting with ; so we do not need one more here
            pinCodes = string.Format("{0}{1}", competition.Judges.Count, pinCodes);

            return pinCodes;
        }

        public static string GetMarksForJudge(Competition competition, Judge judge)
        {
            
            // Yes, lets build and send:
            var data = competition.Couples.Count.ToString();
            foreach (var couple in competition.Couples.OrderBy(c => c.Number))
            {
                data += ";" + couple.Number.ToString() + ";\"";

                foreach (var dance in competition.Dances.OrderBy(d => d.SortOrder))
                {
                    var mark = couple.Judgements.SingleOrDefault(j => j.Judge.Sign == judge.Sign && j.Dance == dance);
                    if (mark != null)
                    {
                        if (competition.Type == 1)
                        {
                            if (mark.Value == 1)
                            {
                                data += "X";
                            }
                            else
                            {
                                switch (mark.Others)
                                {
                                    case 1:
                                        data += "1";
                                        break;
                                    case 2:
                                        data += "2";
                                        break;
                                    case 3:
                                        data += "L";
                                        break;
                                }
                            }
                        }
                        else
                        {
                            data += mark.Value.ToString();
                        }
                    }
                    data += ";";
                }

                data += "\"";
            }

            return data;
        }

        public static string GetDataForJudge(Competition competition, Judge judge, bool usePincode)
        {
            var offset = 0;

            var data = "";
            data = String.Format(
                "{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13}",
                competition.Type,
                judge.Language,
                competition.Flags,
                competition.Title,
                competition.AgeGroup,
                competition.Round,
                competition.MarksFrom,
                competition.MarksTo,
                competition.MarksStrict ? "1" : "0",
                judge.Sign,
                judge.FirstName,
                judge.LastName,
                judge.Country,
                offset);
            // now add all dances we are dancing:
            data += ";" + competition.Dances.Count;

            if (competition.Type == 2 || competition.Type == 14 || competition.Type == 12)
            {
                // Finale, wir erzeugen schnell ein paar Strings ...
                var heat = "1;\"";
                foreach (var couple in competition.Couples)
                {
                    heat += couple.CompetitionId + " " + couple.Number + ";";
                }
                // remove last ;
                heat = heat.Substring(0, heat.Length - 1);
                heat += "\"";
                competition.HeatStrings = new List<string>();
                foreach (var dance in competition.Dances)
                {
                    competition.HeatStrings.Add(heat);
                }
            }

            foreach (var dance in competition.Dances.OrderBy(d => d.SortOrder))
            {
                data += String.Format(";{0};{1};{2}", dance.Code, dance.Name, dance.Name);
                // Add Heats of this dance
                data += ";" + competition.HeatStrings[dance.SortOrder - 1];
            }

            var pinCodes = "";
            if (usePincode)
            {
                pinCodes = GetPinCodeString(competition);
            }

            if (!string.IsNullOrEmpty(pinCodes))
            {
                data += ";" + pinCodes;
            }

            // that's all, send data now
            return data;
        }

        public static void ClearDevices(Competition competition)
        {
            foreach (var judge in competition.Judges)
            {
                if (!string.IsNullOrEmpty(judge.DeviceId))
                {
                    ClearDevice(judge.DeviceId, competition.Id);
                }

                foreach (var selectedDevice in competition.SelectedDevices)
                {
                    ClearDevice(selectedDevice.Id, competition.Id);
                }
            } 
        }

        public static void ClearDevice(string devicdeId, string competitionId)
        {
            Instance.SendMessage(devicdeId, 122, competitionId, "");
        }

        public static void SendStart(Competition competition)
        {
            foreach (var judge in competition.Judges)
            {
                SendStart(judge.DeviceId, competition.Id);
            }
        }

        public static void SendStart(string deviceId, string competitionId)
        {
            Instance.SendMessage(deviceId, 121, competitionId, "");
        }

        public static void SendSaveCmd(Competition competition)
        {
            foreach (var judge in competition.Judges)
            {
                Instance.SendMessage(judge.DeviceId, competition.Type == 1 ? 103 : 113, competition.Id, "");
            }
        }

        public static void SendConfig(Competition competition)
        {
            // Resend competition data but with targetId = 131
            foreach (var judge in competition.Judges)
            {
                SendData(competition, judge, 131, true);
            }
        }

        public static void MoveJudges(Competition competition, int dance, int heat)
        {
            foreach (var judge in competition.Judges)
            {
                MoveJudge(judge.DeviceId, competition.Id, dance, heat);
            }
        }

        public static void MoveJudge(string deviceId, string competitionId, int dance, int heat)
        {
            var data = String.Format("{0};{1}", dance, heat);
            Instance.SendMessage(deviceId, 300, competitionId, data);

        }

        public static void AddOrMoveCouple(Competition _competition, string number, int dance, int heat, int targetId)
        {
            var str = number;
            for (var i = dance; i <= _competition.Dances.Count;i++ )
            {
                str += String.Format(";{0};{1}", i, heat);
            }
            foreach (var judge in _competition.Judges)
            {
                Instance.SendMessage(judge.DeviceId, targetId, _competition.Id, str);
            }   
        }

        public static void AddCouple(Competition _competition, string number, int dance, int heat)
        {
            AddOrMoveCouple(_competition, number, dance, heat, 220);
        }

        public static void MoveCouple(Competition _competition, string number, int dance, int heat)
        {
            AddOrMoveCouple(_competition, number, dance, heat, 200);
        }

        public static void RemoveCouple(Competition _competition, string number, int dance)
        {
            var str = number;
            for(var i=dance;i<=_competition.Dances.Count;i++)
            {
                str += String.Format(";{0}", i);
            }

            foreach (var judge in _competition.Judges)
            {
                Instance.SendMessage(judge.DeviceId, 210, _competition.Id, str);
            } 
        }

        private void SendMessage(string receiver, int TargetId, string DoCmd, string MsgData)
        {
            var tpsMessage = new TMQeJSMsg()
                            {
                                TimeStamp = DateTime.Now,
                                DoCmd = DoCmd,
                                MsgData = MsgData,
                                Receiver = receiver,
                                ReceiverIP = "",
                                ResponseId = 0,
                                Sender = "mTPS",
                                TargetId = TargetId
                            };

            try
            {
                lock (this._lockObj)
                {
                    this._outQueue.Send(tpsMessage);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
