﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace mobileControl {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "14.0.0.0")]
    internal sealed partial class mobileControlSettings : global::System.Configuration.ApplicationSettingsBase {
        
        private static mobileControlSettings defaultInstance = ((mobileControlSettings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new mobileControlSettings())));
        
        public static mobileControlSettings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("C:\\TPS7\\Data\\Competition")]
        public string DataPath {
            get {
                return ((string)(this["DataPath"]));
            }
            set {
                this["DataPath"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool AutoStart {
            get {
                return ((bool)(this["AutoStart"]));
            }
            set {
                this["AutoStart"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool AutoPrint {
            get {
                return ((bool)(this["AutoPrint"]));
            }
            set {
                this["AutoPrint"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool AutoLoad {
            get {
                return ((bool)(this["AutoLoad"]));
            }
            set {
                this["AutoLoad"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool AutoClear {
            get {
                return ((bool)(this["AutoClear"]));
            }
            set {
                this["AutoClear"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool UseEncryption {
            get {
                return ((bool)(this["UseEncryption"]));
            }
            set {
                this["UseEncryption"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("C:\\TPS7\\Data\\PrintOuts")]
        public string PathToSaveJudgingSheets {
            get {
                return ((string)(this["PathToSaveJudgingSheets"]));
            }
            set {
                this["PathToSaveJudgingSheets"] = value;
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("c:\\temp;c:\\eJudge")]
        public string TVExportPath {
            get {
                return ((string)(this["TVExportPath"]));
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool SaveJudgingSheets {
            get {
                return ((bool)(this["SaveJudgingSheets"]));
            }
            set {
                this["SaveJudgingSheets"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("0")]
        public int FileCodePage {
            get {
                return ((int)(this["FileCodePage"]));
            }
            set {
                this["FileCodePage"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("4")]
        public int PinLength {
            get {
                return ((int)(this["PinLength"]));
            }
            set {
                this["PinLength"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("9100")]
        public int NancyPort {
            get {
                return ((int)(this["NancyPort"]));
            }
            set {
                this["NancyPort"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("http://192.168.1.20:9100")]
        public string SelectedUris {
            get {
                return ((string)(this["SelectedUris"]));
            }
            set {
                this["SelectedUris"] = value;
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("9090")]
        public int XdServerPort {
            get {
                return ((int)(this["XdServerPort"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("9095")]
        public int XdServerConfigPort {
            get {
                return ((int)(this["XdServerConfigPort"]));
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("c:\\Temp\\AutoConfigDevices.txt")]
        public string XdServerDevicesFile {
            get {
                return ((string)(this["XdServerDevicesFile"]));
            }
            set {
                this["XdServerDevicesFile"] = value;
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute(".\\private$\\TPS.eJudge.in")]
        public string XdServerInQueue {
            get {
                return ((string)(this["XdServerInQueue"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute(".\\private$\\TPS.eJudge.out")]
        public string XdServerOutQueue {
            get {
                return ((string)(this["XdServerOutQueue"]));
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("*")]
        public string XdServerAccessPoints {
            get {
                return ((string)(this["XdServerAccessPoints"]));
            }
            set {
                this["XdServerAccessPoints"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("C:\\TPS7\\Data\\cfg")]
        public string PathToTPSSettings {
            get {
                return ((string)(this["PathToTPSSettings"]));
            }
            set {
                this["PathToTPSSettings"] = value;
            }
        }
    }
}
