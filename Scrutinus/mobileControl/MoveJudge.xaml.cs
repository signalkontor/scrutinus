﻿// // TPS.net TPS8 mobileControl
// // MoveJudge.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using mobileControl.Helper;
using mobileControl.Model;
using mobileControl.XDServer;

namespace mobileControl
{
    /// <summary>
    /// Interaction logic for MoveJudge.xaml
    /// </summary>
    public partial class MoveJudge : Window
    {
        private readonly Competition _competition;
        private readonly Judge _judge;

        public MoveJudge(Competition competition, Judge judge)
        {
            var heatCount = 0;
            this.InitializeComponent();
            this._judge = judge;
            this._competition = competition;

            this.ListDance.ItemsSource = competition.Dances;
            this.ListDance.DisplayMemberPath = "Name";
            this.ListDance.SelectedValuePath = "SortOrder";

            if(judge != null)
            {
                this.Title = "Move Judge " + judge.NiceName;
            }

            // Number of heats?
            if (competition.HeatStrings.Count > 0)
            {
                var data = TpsHelper.TPSSplitter(competition.HeatStrings[0]);
                heatCount = data.Count() - 1;
            }
            if(heatCount > 0)
            {
                var list = new List<string>();
                for(var i=1;i<=heatCount;i++)
                {
                    list.Add(String.Format("{0}. Heat", i));
                }
            }
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            if(this.ListDance.SelectedIndex == -1)
            {
                MessageBox.Show("Please select a dance!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (this._judge == null)
            {
                CommunicationHandler.MoveJudges(this._competition, (int) this.ListDance.SelectedValue, 1);
            }
            else
            {
                CommunicationHandler.MoveJudge(this._judge.DeviceId, this._competition.Id, (int) this.ListDance.SelectedValue, 1);
            }

            this.Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
