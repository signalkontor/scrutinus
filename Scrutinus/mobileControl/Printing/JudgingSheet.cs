﻿// // TPS.net TPS8 mobileControl
// // JudgingSheet.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Linq;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Threading;
using mobileControl.Model;

namespace mobileControl.Printing
{
    internal class JudgingSheet : AbstractPrinter
    {
        private Competition competition;
        private FixedPage currentPage;

        protected override void AddCommenContent(FixedPage page)
        {
            this.AddText(page, "(c) 2018 signalkontor GmbH, Germany", 28, 2, 8);
            this.AddText(page, this.competition.Title + " " + this.competition.AgeGroup + " / " + this.competition.Round, 1, 1, 12);
            var marks = this.competition.MarksFrom == this.competition.MarksTo
                            ? String.Format("{0} Marks", this.competition.MarksFrom)
                            : String.Format("{0} - {1} Marks", this.competition.MarksFrom, this.competition.MarksTo);
            this.AddText(page, marks, 1, 18, 12);
        }

        private void PrintJudgementBox(Judge judge, double vertical, int offset)
        {
            var top = vertical;

            var count = offset + 12 < this.competition.Couples.Count ? 12 : this.competition.Couples.Count - offset;
            this.AddBox(this.currentPage, vertical, 1, 1, 4);
            this.AddText(this.currentPage, judge.NiceName, vertical + .3, 1.3, 10);
            for(var i = 0; i < count; i++)
            {
                this.AddBox(this.currentPage, vertical, 4 + i + 1, 1, 1);
                this.AddText(this.currentPage, this.competition.Couples[i + offset].Number.ToString(), vertical + .5, 4.2 + (i + 1), 12);
            }
            top = vertical + 1;
            foreach (var dance in this.competition.Dances)
            {
                this.AddBox(this.currentPage, top, 1, 1, 4);
                this.AddText(this.currentPage, dance.Name, top + .5, 1.3, 12);

                for(var i=0;i<count;i++)
                {
                    this.AddBox(this.currentPage, top, 4 + i + 1, 1, 1);
                    var mark = this.competition.Couples[i + offset].Judgements.SingleOrDefault(
                            j => j.Judge.Sign == judge.Sign && j.Dance.Code == dance.Code);
                    if(mark == null)
                    {
                        continue;
                    }
                    if (this.competition.Type == 1)
                    {
                        if (mark.Value == 1)
                        {
                            this.AddText(this.currentPage, "X", top + .3, 4.3 + i + 1, 18);
                        }
                        else
                        {
                            var code = "";
                            switch(mark.Others)
                            {
                                case 1:
                                    code = ".";
                                    break;
                                case 2:
                                    code = "..";
                                    break;
                                case 3:
                                    code = "L";
                                    break;

                            }
                            this.AddText(this.currentPage, code, top + .3, 4.3 + i + 1, 18);
                        }
                    }
                    else
                    {
                        this.AddText(this.currentPage, mark.Value.ToString(), top + .3, 4.3 + i + 1, 18);
                    }
                }
                top += 1;
            }
            if (this.competition.Type == 1)
            {
                // Hier muss noch die Summe einmal ausgegeben werden
                for (var i = 0; i < count; i++)
                {
                    this.AddBox(this.currentPage, top, 4 + i + 1, 1, 1);
                    var dict = this.competition.Couples[i + offset].SumMarking["all"];
                    var sum = dict.ContainsKey(judge.Sign)
                                  ? dict[judge.Sign]
                                  : 0;
                    this.AddText(this.currentPage, sum.ToString(), top + .3, 4.3 + i + 1, 18);
                }
            }
        }

        public void Print(Competition competition, Judge judge, bool print, string fileName)
        {
            // Create Judging Sheet for this one judge
            double vertical = 3;
            this.competition = competition;
            this.currentPage = this.createPage();
            var offset = 0;

            while (offset < competition.Couples.Count)
            {
                this.PrintJudgementBox(judge, vertical, offset);
                offset += 12;
                vertical += this.competition.Dances.Count + 3;
                if(offset >= this.competition.Couples.Count)
                {
                    if (vertical > 26)
                    {
                        this.currentPage = this.createPage();
                        vertical = 1.5;
                    }
                }
                else
                {
                    if(vertical > 28 - this.competition.Dances.Count - 3)
                    {
                        this.currentPage = this.createPage();
                        vertical = 1.5;
                    }
                }
            }

            this.PrintSignatureBox(judge, vertical);

            if (print)
            {
                this.PrintDocument(fileName, false);
            }
            else
            {
                if (fileName != null)
                {
                    this.PrintDocument(fileName, false);
                }
            }
        }

        private void PrintSignatureBox(Judge judge, double vertical)
        {
            // AddBox(_currentPage, vertical, 1, 2, 4 );
            this.AddText(this.currentPage, judge.NiceName, vertical + 1.7, 1.3, 8);
            foreach (var coordinate in judge.Signature)
            {
                this.AddLine(this.currentPage, coordinate.X1 / 2.0, coordinate.Y1 / 2.0, coordinate.X2 / 2.0, coordinate.Y2 / 2.0, 1.5, vertical, 2);
            }
        }

        public void Print(Competition competition, Judge judge)
        {
            this.Print(competition, judge, true, null);
        }

        public void Print(Competition competition, Dispatcher dispatcher)
        {
            foreach (var judge in competition.Judges)
            {
                dispatcher.Invoke(new Action(
                    () =>
                        {
                            this.doc = new FixedDocument();
                            this.doc.DocumentPaginator.PageSize = new Size(96 * 21 / 2.54, 96 * 29.7 / 2.54);
                            this.Print(competition, judge, true, null);
                        }));
            }
        }
    }
}
