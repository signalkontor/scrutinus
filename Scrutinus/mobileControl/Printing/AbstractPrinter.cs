﻿// // TPS.net TPS8 mobileControl
// // AbstractPrinter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Xps.Packaging;
// using System.Windows.Xps.Serialization;

namespace mobileControl.Printing
{
    public abstract class AbstractPrinter
    {
        protected FixedDocument doc;

        /// <summary>
        /// Setzt Seitengröße auf A4 und erzeugt ein leeres Dokument
        /// </summary>
        protected AbstractPrinter()
        {
            this.doc = new FixedDocument();
            this.doc.DocumentPaginator.PageSize = new Size(96 * 21 / 2.54, 96 * 29.7 / 2.54);
        }

        public FixedDocument Document { get { return this.doc; }
        }

        public bool PageNumbers { get; set; }

        /// <summary>
        /// Wird aufgerufen nachdem eine Seite erzeugt wurde. Hier kann dann auf die Seite geschrieben werden
        /// wass auf jeder Seite drauf stehen soll wie z.B. eine Überschrift
        /// </summary>
        /// <param name="page"></param>
        protected abstract void AddCommenContent(FixedPage page);

        /// <summary>
        /// Erzeugt eine Seite im Standardlayout (Logo, Adresse, ...)
        /// </summary>
        /// <returns></returns>
        public FixedPage createPage()
        {
            var page = new PageContent();
            var fixedPage = new FixedPage();
            ((IAddChild)page).AddChild(fixedPage);
            this.doc.Pages.Add(page);

            fixedPage.Background = Brushes.White;
            // PER MEDIA Logo

            this.AddCommenContent(fixedPage);

            return fixedPage;
        }

        /// <summary>
        /// Text einer Seite hinzufügen
        /// </summary>
        /// <param name="Page">Die Seire</param>
        /// <param name="Text">Text</param>
        /// <param name="Top">in Cm</param>
        /// <param name="Left">in Cm</param>
        /// <param name="FontSize">in Punkten</param>
        /// <param name="Width"></param>
        /// <param name="alignment"></param>
        protected void AddText(FixedPage Page, string Text, double Top, double Left, int FontSize, double Width, HorizontalAlignment alignment)
        {
            this.AddText(Page, Text, Top, Left, FontSize, Width, 3, alignment, FontWeights.Normal, 0);
        }

        /// <summary>
        /// Fügt einen Text hinzu mit der Möglichkeit eines Rahmnens
        /// </summary>
        /// <param name="Page"></param>
        /// <param name="Text"></param>
        /// <param name="Top"></param>
        /// <param name="Left"></param>
        /// <param name="FontSize"></param>
        /// <param name="Width"></param>
        /// <param name="alignment"></param>
        /// <param name="Weight"></param>
        /// <param name="Border"></param>
        protected void AddText(FixedPage Page, string Text, double Top, double Left, int FontSize, double Width, double Height, HorizontalAlignment alignment, FontWeight Weight, int Border)
        {
            var text = new TextBlock();
            text.Text = Text;
            text.FontSize = FontSize;
            text.FontFamily = new FontFamily("Arial");
            text.HorizontalAlignment = alignment;
            text.FontWeight = Weight;
            text.Width = Width * 96 / 2.54;
            text.Height = Height * 96 / 2.54;

            if (Border > 0)
            {
                text.Padding = new Thickness(4, 2, 2, 2);
            }

            var b = new Border();
            b.BorderThickness = new Thickness(Border);
            b.BorderBrush = Brushes.Black;
            b.Child = text;
            FixedPage.SetLeft(b, 96 * Left / 2.54); // left margin
            FixedPage.SetTop(b, 96 / 2.54 * Top); // top margin
            Page.Children.Add((UIElement)b);

        }

        /// <summary>
        /// Fügt einen Rahmen hinzu
        /// </summary>
        /// <param name="Page"></param>
        /// <param name="Top"></param>
        /// <param name="Left"></param>
        /// <param name="Heigh"></param>
        /// <param name="Width"></param>
        protected void AddBox(FixedPage Page, double Top, double Left, double Heigh, double Width)
        {
            var b = new Border();
            b.BorderThickness = new Thickness(1);
            b.BorderBrush = Brushes.Black;
            b.Width = Width * 96 / 2.54;
            b.Height = Heigh * 96 / 2.54;
            FixedPage.SetLeft(b, 96 * Left / 2.54); // left margin
            FixedPage.SetTop(b, 96 / 2.54 * Top); // top margin
            Page.Children.Add((UIElement)b);
        }

        /// <summary>
        /// Fügt eine horizontale Linie hinzu
        /// </summary>
        /// <param name="Page"></param>
        /// <param name="Top"></param>
        /// <param name="Left"></param>
        /// <param name="Width"></param>
        protected void AddHorizontalLine(FixedPage Page, double Top, double Left, double Width)
        {
            var b = new Border();
            b.BorderThickness = new Thickness(1);
            b.BorderBrush = Brushes.Black;
            b.Width = Width * 96 / 2.54;
            b.Height = 1;
            FixedPage.SetLeft(b, 96 * Left / 2.54); // left margin
            FixedPage.SetTop(b, 96 / 2.54 * Top); // top margin
            Page.Children.Add((UIElement)b);
        }

        /// <summary>
        /// Vertikale Linie
        /// </summary>
        /// <param name="Page"></param>
        /// <param name="Top"></param>
        /// <param name="Left"></param>
        /// <param name="Height"></param>
        protected void AddVerticalLine(FixedPage Page, double Top, double Left, double Height)
        {
            var b = new Border();
            b.BorderThickness = new Thickness(1);
            b.BorderBrush = Brushes.Black;
            b.Height = Height * 96 / 2.54;
            b.Width = 1;
            FixedPage.SetLeft(b, 96 * Left / 2.54); // left margin
            FixedPage.SetTop(b, 96 / 2.54 * Top); // top margin
            Page.Children.Add((UIElement)b);
        }

        protected  void AddLine(FixedPage Page, double X1, double Y1, double X2, double Y2, double Left, double Top, double Thickness)
        {
            var line = new Line(){X1 = X1, X2 = X2, Y1 = Y1, Y2 = Y2};
            line.Stroke = Brushes.Black;
            line.StrokeThickness = Thickness;
            FixedPage.SetLeft(line, 96 * Left / 2.54); // left margin
            FixedPage.SetTop(line, 96 / 2.54 * Top); // top margin
            Page.Children.Add((UIElement)line);
        }

        protected void AddText(FixedPage Page, string Text, double Top, double Left, int FontSize)
        {
            this.AddText(Page, Text, Top, Left, FontSize, 30, HorizontalAlignment.Left);
        }

        /// <summary>
        /// Wird unmittelbar vor dem Druck aufgerufen und fügt beispielsweise Seitennummern hinzu
        /// </summary>
        protected void FinalizeDocument()
        {
            var count = 1;
            
            if (this.PageNumbers)
            {
                foreach (var p in this.doc.Pages)
                {
                    // AddHorizontalLine(p.Child, 26.5, 1, 19);
                    this.AddText(p.Child, "Seite " + count + " von " + this.doc.Pages.Count, 27, 10, 12, 100, HorizontalAlignment.Center);
                    count++;
                }
            }
        }

        /// <summary>
        /// Druckt die Datei aus bzw. erzeugt eine XPS Datei
        /// </summary>
        /// <param name="toFile">Dateinamen, wenn null wird das Dokument zum Standardrucker gesendet</param>
        protected void PrintDocument(string toFile, bool showPrintDlg)
        {
            this.FinalizeDocument();
            if (toFile != null)
            {
                try
                {
                    File.Delete(toFile);
                    var xpsd = new XpsDocument(toFile, FileAccess.ReadWrite);
                    var xw = XpsDocument.CreateXpsDocumentWriter(xpsd);
                    xw.Write(this.doc);
                    xpsd.Close();
                }
                catch (Exception)
                {
                    /// todo: log exception
                }

            }
            else
            {
                var prndlg = new PrintDialog();
                if (showPrintDlg)
                {
                    if (prndlg.ShowDialog().Value == false)
                    {
                        return;
                    }
                }
                prndlg.PrintDocument(this.doc.DocumentPaginator, "TPS Judging Sheet");
            }
        }
    }
}
