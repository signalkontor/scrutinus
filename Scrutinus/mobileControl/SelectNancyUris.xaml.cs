﻿// // TPS.net TPS8 mobileControl
// // SelectNancyUris.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows;
using mobileControl.ViewModels;

namespace mobileControl
{
    /// <summary>
    /// Interaction logic for SelectNancyUris.xaml
    /// </summary>
    public partial class SelectNancyUris : Window
    {
        public SelectNancyUris()
        {
            this.InitializeComponent();

            var viewModel = (SelectUrlViewModel)this.DataContext;
            viewModel.DialogWindow = this;
        }
    }
}
