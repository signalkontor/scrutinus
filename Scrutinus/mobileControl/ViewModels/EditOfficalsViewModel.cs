﻿// // TPS.net TPS8 mobileControl
// // EditOfficalsViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using mobileControl.Helper;
using mobileControl.Model;

namespace mobileControl.ViewModels
{
    public class EditOfficalsViewModel : ViewModelBase
    {
        private string importDirectoryName;
        private bool saveNeeded = false;

        public EditOfficalsViewModel()
        {
            this.SelectDirectoryCommand = new RelayCommand(this.SelectDirectoy);
            this.ImportJudgesCommand = new RelayCommand(this.SaveData);

            this.Judges = new ObservableCollection<Judge>();
            this.PinLength = mobileControlSettings.Default.PinLength;
        }

        public ObservableCollection<Judge> Judges { get; set; }

        public string ImportDirectoryName
        {
            get
            {
                return this.importDirectoryName;
            }
            set
            {
                this.importDirectoryName = value;
                this.RaisePropertyChanged(() => this.ImportDirectoryName);
                this.ReadJudges();
            }
        }

        public bool DataChanged
        {
            get
            {
                return this.saveNeeded;
            }
        }

        public ICommand ImportJudgesCommand { get; set; }

        public ICommand SelectDirectoryCommand { get; set; }

        public int PinLength { get; set; }

        private void ReadJudges()
        {
            // Read existing Pins:
            var existingPins = new Dictionary<string, string>();
            var existingPinFile = mobileControlSettings.Default.PathToTPSSettings + "\\PinsJudges.csv";
            if (File.Exists(existingPinFile))
            {
                var pinFile = new StreamReader(existingPinFile);
                while (!pinFile.EndOfStream)
                {
                    var data = pinFile.ReadLine().Split(';');
                    existingPins.Add(data[0], data[2]);
                }
                pinFile.Close();
            }
            
            
            var filename = Path.Combine(this.ImportDirectoryName, "_VaFu.Dat");
            if (!File.Exists(filename))
            {
                MessageBox.Show("No officials found in selected directory");
                return;
            }

            var file = new StreamReader(filename);

            var random = new Random();

            var minRandom = (int) Math.Pow(10, this.PinLength - 1);
            var maxRandom = (int) Math.Pow(10, this.PinLength) - 1;

            while (!file.EndOfStream)
            {
                var line = file.ReadLine();
                var data = TpsHelper.TPSSplitter(line);
                if (data[5].Contains("WR"))
                {
                    var judge = new Judge(data[0], data[1], data[2], data[3]);
                    if (existingPins.ContainsKey(judge.Sign))
                    {
                        judge.Pin = existingPins[judge.Sign];
                    }
                    else
                    {
                        judge.Pin = random.Next(minRandom, maxRandom).ToString();
                        this.saveNeeded = true;
                    }
                    // PIN must be unique:
                    var existingJudge = this.Judges.FirstOrDefault(j => j.Pin == judge.Pin);
                    while(existingJudge != null)
                    {
                        judge.Pin = random.Next(minRandom, maxRandom).ToString();
                        existingJudge = this.Judges.FirstOrDefault(j => j.Pin == judge.Pin);
                    }
                    this.Judges.Add(judge);
                }
            }
            file.Close();
        }

        private void SelectDirectoy()
        {
            var dlg = new FolderBrowserDialog();
            dlg.ShowNewFolderButton = false;
            dlg.SelectedPath = @"C:\TPS7\DatFiles";

            var result = dlg.ShowDialog();

            if (result == DialogResult.OK)
            {
                this.ImportDirectoryName = dlg.SelectedPath;
            }
        }

        private void SaveData()
        {
            var existingPinFile = mobileControlSettings.Default.PathToTPSSettings + "\\PinsJudges.csv";

            var writer = new StreamWriter(existingPinFile);

            foreach (var judge in this.Judges)
            {
                writer.WriteLine("{0};{1};{2};{3}", judge.Sign, judge.NiceName, judge.Pin, judge.DeviceId);
            }

            writer.Close();
            this.saveNeeded = false;
        }
    }
}
