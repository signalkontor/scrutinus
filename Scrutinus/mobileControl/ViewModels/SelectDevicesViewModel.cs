﻿// // TPS.net TPS8 mobileControl
// // SelectDevicesViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using mobileControl.Model;

namespace mobileControl.ViewModels
{
    public class SelectDevicesViewModel : ViewModelBase
    {
        private ObservableCollection<Device> selectedDevices ;

        public SelectDevicesViewModel()
        {
            this.AddSelectedCommand = new RelayCommand(this.AddSelected);
            this.RemoveSelectedCommand = new RelayCommand(this.RemoveSelected);

            this.Devices = new ObservableCollection<Device>();
            this.SelectedDevices = new ObservableCollection<Device>();
            this.SelectionListAdd = new ObservableCollection<Device>();
            this.SelectionListRemove = new ObservableCollection<Device>();
        }

        public ObservableCollection<Device> Devices { get; set; }

        public ObservableCollection<Device> SelectedDevices
        {
            get
            {
                return this.selectedDevices;
            }
            set
            {
                this.selectedDevices = value;
                this.RaisePropertyChanged(() => this.SelectedDevices);
            }
        }

        public ObservableCollection<Device> SelectionListAdd { get; set; }
        public ObservableCollection<Device> SelectionListRemove { get; set; }
        public ICommand AddSelectedCommand { get; set; }
        public ICommand RemoveSelectedCommand { get; set; }

        private void AddSelected()
        {
            foreach (var device in this.SelectionListAdd.ToList())
            {
                this.SelectedDevices.Add(device);
                this.Devices.Remove(device);
            }
            this.SelectionListAdd.Clear();
        }

        private void RemoveSelected()
        {
            var list = this.SelectionListRemove.ToList();
            foreach (var device in list)
            {
                this.SelectedDevices.Remove(device);
                this.Devices.Add(device);
            }
            this.SelectionListRemove.Clear();
        }
    }
}
