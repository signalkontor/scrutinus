﻿// // TPS.net TPS8 mobileControl
// // SelectUrlViewModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using mobileControl.Nancy;

namespace mobileControl.ViewModels
{

    public class UrlElement
    {
        public string Uri { get; set; }

        public bool IsSelected { get; set; }
    }


    internal class SelectUrlViewModel : ViewModelBase
    {
        public SelectUrlViewModel()
        {
            var availableUris = NancyHttpServer.GetUriList(mobileControlSettings.Default.NancyPort);

            this.Uris = new ObservableCollection<UrlElement>();

            var selectedUris = mobileControlSettings.Default.SelectedUris.Split(',');

            foreach (var available in availableUris)
            {
                this.Uris.Add(new UrlElement()
                {
                    Uri = available.ToString(),
                    IsSelected = selectedUris.Any(s => selectedUris.Contains(available.ToString()))
                });
            }

            this.CloseCommand = new RelayCommand(this.Close);
            this.SaveCommand = new RelayCommand(this.Save);
        }

        public ObservableCollection<UrlElement> Uris { get; set; }

        public ICommand CloseCommand { get; set; }

        public ICommand SaveCommand { get; set; }

        public Window DialogWindow { get; set; }

        private void Close()
        {
            this.DialogWindow.Close();
        }

        private void Save()
        {
            var uris = this.Uris.Where(u => u.IsSelected).Select(u => u.Uri);
            mobileControlSettings.Default.SelectedUris = string.Join(",", uris);
            mobileControlSettings.Default.Save();

            this.Close();
        }
    }
}
