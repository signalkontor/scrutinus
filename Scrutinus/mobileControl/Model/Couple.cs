// // TPS.net TPS8 mobileControl
// // Couple.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Linq;
using mobileControl.Helper;

namespace mobileControl.Model
{
    public class Couple : DataObject
    {
        private int _totalSumMarks;
        public List<Judgement> Judgements;

        public Couple(List<Dance> dances)
        {
            this.Judgements = new List<Judgement>();
            this.SumMarking = new Dictionary<string, Dictionary<string, int>>(); 
            if(dances != null)
            {
                foreach (var dance in dances)
                {
                    this.SumMarking.Add(dance.Code, new Dictionary<string, int>());
                }
            }
            else
            {
                this.SumMarking.Add("all", new Dictionary<string, int>());
            }
        }

        public Couple(string line, List<Dance> dances, string originalCompetitionId)
        {
            this.CompetitionId = originalCompetitionId;
            var data = TpsHelper.TPSSplitter(line);
            this.Number = Int32.Parse(data[0]);
            this.NiceName = String.Format("{0} {1} / {2} {3} ({4})", data[1], data[2], data[3], data[4], data[5]).Trim();
            this.Judgements = new List<Judgement>();
            this.SumMarking = new Dictionary<string, Dictionary<string, int>>();
            if (dances != null)
            {
                foreach (var dance in dances)
                {
                    this.SumMarking.Add(dance.Code, new Dictionary<string, int>());
                }
            }
            else
            {
                this.SumMarking.Add("all", new Dictionary<string, int>());
            }
        }

        public int Number { get; set; }

        public string NiceName { get; set; }

        // Dictionary <Dance <Judge, Points>>
        public Dictionary<string, Dictionary<string, int>> SumMarking { get; set; }

        public Dictionary<string, double> MarkingsA { get; set; }

        public Dictionary<string, double> MarkingsB { get; set; }

        public string CompetitionId { get; set; }

        public int TotalSumMarks
        {
            get { return this._totalSumMarks; }
            set
            {
                if (this._totalSumMarks != value)
                {
                    this._totalSumMarks = value;
                    this.NotifyPropertyChanged("TotalSumMarks");
                }
            }
        }

        public void ClearMarking()
        {
            this.Judgements.Clear();
            if(this.SumMarking.ContainsKey("all"))
            {
                this.SumMarking.Remove("all");
                this.SumMarking.Add("all", new Dictionary<string, int>());
            }
        }

        public void UpdateMarkingShowFinal(Competition context, string danceCode, string judgeSign, double A, double B, int place)
        {
            var mark = this.Judgements.SingleOrDefault(j => j.Dance.Code == danceCode && j.Judge.Sign == judgeSign);

            if (mark == null)
            {
                var judge = context.Judges.SingleOrDefault(j => j.Sign == judgeSign);
                var dance = context.Dances.SingleOrDefault(d => d.Code == danceCode);
                mark = new Judgement()
                {
                    Judge = judge,
                    Dance = dance
                };
                this.Judgements.Add(mark);
            }

            mark.Value = place;

            var dict = this.GetSumMarkingForDance(context.Type, danceCode);
            if (!dict.ContainsKey(judgeSign))
            {
                dict.Add(judgeSign, 0);
            }

            dict[judgeSign] = place;

            if (this.MarkingsA == null)
            {
                this.MarkingsA = new Dictionary<string, double>();
            }
            if (this.MarkingsB == null)
            {
                this.MarkingsB = new Dictionary<string, double>();
            }

            if (!this.MarkingsA.ContainsKey(judgeSign))
            {
                this.MarkingsA.Add(judgeSign, 0);                
            }
            if (!this.MarkingsB.ContainsKey(judgeSign))
            {
                this.MarkingsB.Add(judgeSign, 0);
            }

            this.MarkingsA[judgeSign] = A;
            this.MarkingsB[judgeSign] = B;

            this.NotifyPropertyChanged("SumMarking");
            this.NotifyPropertyChanged("MarkingsA");
            this.NotifyPropertyChanged("MarkingsB");
            this.NotifyPropertyChanged("TotalSumMarks");
        }

        public void UpdateMarking(Competition context, string danceCode, string judgeSign, string marking)
        {
            var mark = this.Judgements.SingleOrDefault(j => j.Dance.Code == danceCode && j.Judge.Sign == judgeSign);
            var newMark = false;
            
            if(mark == null)
            {
                newMark = true;
                var judge = context.Judges.SingleOrDefault(j => j.Sign == judgeSign);
                var dance = context.Dances.SingleOrDefault(d => d.Code == danceCode);
                mark = new Judgement()
                {
                    Judge = judge,
                    Dance = dance
                };
                this.Judgements.Add(mark);
            }

            if(context.Type == 1)
            {
                switch(marking)
                {
                    case " ":
                        if (!newMark && mark.Value == 1)
                        {
                            this.TotalSumMarks--;
                            mark.Value = -1;
                        }
                        else
                        {
                            mark.Value = 0;
                        }

                        this.Judgements.Remove(mark);
                        break;
                    case "X":
                        mark.Value = 1;
                        mark.Others = 0;
                        this.TotalSumMarks++;
                        break;
                    case "1":
                        mark.Value = 0;
                        mark.Others = 1;
                        break;
                    case "2":
                        mark.Value = 0;
                        mark.Others = 2;
                        break;
                    case "L":
                        mark.Others = 3;
                        break;
                }
            }
            else
            { // Hier f�r das Finale
                var val = 0;
                if (Int32.TryParse(marking, out val))
                {
                    mark.Value = val;
                }
                else
                {
                    mark.Value = 0;
                }
            }
            

            var sum = 0;
            var dict = this.GetSumMarkingForDance(context.Type, danceCode);
            if(!dict.ContainsKey(judgeSign))
            {
                dict.Add(judgeSign, sum);
            }
            
            if(newMark)
            {
                if (context.Type == 1)
                {
                    dict[judgeSign] += mark.Value > 0 ? mark.Value : 0;
                }
                else
                {
                    dict[judgeSign] = mark.Value > 0 ? mark.Value : 0; // In the final we do not add the marks, just update
                }
            }
            else
            {
                if (context.Type == 1)
                {
                    dict[judgeSign] += mark.Value;
                }
                else
                {
                    dict[judgeSign] = mark.Value;
                }
            }

            this.NotifyPropertyChanged("SumMarking");
            this.NotifyPropertyChanged("TotalSumMarks");
        }

        public Judgement GetResult(Dance dance, Judge judge)
        {
            var judgement = this.Judgements.SingleOrDefault(j => j.Dance.Code == dance.Code && j.Judge.Sign == judge.Sign);
            // recalculate the place based on the marks in other competitions:

            return judgement ?? new Judgement() {Dance = dance, Judge = judge, Others = 0, Value = 0};
        }

        public Dictionary<string, int> GetSumMarkingForDance(int competitionType, string danceCode)
        {
            if (competitionType == 1)
            {
                if(!this.SumMarking.ContainsKey("all"))
                {
                    this.SumMarking.Add("all", new Dictionary<string, int>());
                }
                return this.SumMarking["all"];
            }
            else
            {
                if(!this.SumMarking.ContainsKey(danceCode))
                {
                    this.SumMarking.Add(danceCode, new Dictionary<string, int>());
                }
                return this.SumMarking[danceCode];
            }
        }
    }
}