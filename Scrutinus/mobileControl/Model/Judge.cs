// // TPS.net TPS8 mobileControl
// // Judge.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using mobileControl.Helper;

namespace mobileControl.Model
{
    public class Judge : DataObject
    {
        private readonly string _competitionId;

        private bool _completeMarking;

        private Device _device;

        private string _deviceId;

        private bool _signatureReceived;

        public Judge(string sign, string firstname, string lastname, string country)
        {
            this.FirstName = firstname;
            this.LastName = lastname;
            this.Country = country;
            this.Sign = sign;
        }

        public Judge(string competitionId, string line)
        {
            var data = TpsHelper.TPSSplitter(line);

            this.Sign = data[0];
            this.FirstName = data[1];
            this.LastName = data[2];
            this.Country = data[3];
            this.Chapter = data[4];
            this.DeviceId = data[5];
            this.Language = int.Parse(data[6]);

            var device = MainWindow.Context.Devices.FirstOrDefault(d => d.Id == this.DeviceId);
            this.Device = device;

            this.Language = Int32.Parse(data[6]);
            this.Pin = data[8];
            this._competitionId = competitionId;
            this.Signature = new List<Coordinate>();
            // do we have a signature?
            if (data.Length < 8)
            {
                return;
            }
            var sigData = TpsHelper.TPSSplitter(data[7]);
            if (sigData.Count() < 3)
            {
                return;
            }
            var sigCount = 0;
            if (!Int32.TryParse(sigData[2], out sigCount))
            {
                return;
            }
            for (var i = 0; i < sigCount; i++)
            {
                
                var co = new Coordinate
                {
                    X1 = double.Parse(sigData[3 + i * 4]),
                    Y1 = double.Parse(sigData[4 + i * 4]),
                    X2 = double.Parse(sigData[5 + i * 4]),
                    Y2 = double.Parse(sigData[6 + i * 4])
                };
                this.Signature.Add(co);
            }
        }

        public string Sign { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Country { get; set; }
        public string Chapter { get; set; }
        public int SumOfMarks { get; set; }
        public int Language { get; set;}
        public string Pin { get; set; }
        public List<Coordinate> Signature { get; set; }

        public string DeviceId { 
            get { return this._deviceId; }
            set
            {
                if(this._deviceId != value)
                {
                    this._deviceId = value;
                    this.NotifyPropertyChanged("DeviceId");
                }
            }    
        }

        public Device Device
        {
            get { return this._device; }
            set
            {
                if(this._device != value)
                {
                    this._device = value;
                    if(value != null)
                    {
                        this._device.PropertyChanged += new PropertyChangedEventHandler(this.Device_PropertyChanged);
                    }
                    this.NotifyPropertyChanged("Device");
                    this.NotifyPropertyChanged("State");
                }
            }
        }

        public bool CompleteMarking
        {
            get { return this._completeMarking; }
            set
            {
                if(this._completeMarking != value)
                {
                    this._completeMarking = value;
                    this.NotifyPropertyChanged("CompleteMarking");
                }
            }
        }

        public bool SignatureReceived
        {
            get { return this._signatureReceived; }
            set
            {
                if(this._signatureReceived != value)
                {
                    this._signatureReceived = value;
                    this.NotifyPropertyChanged("SignatureReceived");
                }
            }
        }


        public string NiceName
        {
            get { return String.Format("({0}) - {1} {2}", this.Sign, this.FirstName, this.LastName); }
        }

        public String State
        {
            get
            {
                if (this.Device == null && String.IsNullOrWhiteSpace(this.DeviceId))
                {
                    return "No Device";
                }
                if(this.Device == null && !String.IsNullOrWhiteSpace(this.DeviceId))
                {
                    this.Device = MainWindow.Context.Devices.FirstOrDefault(d => d.Id == this.DeviceId);
                    if (this.Device == null)
                    {
                        return "Device not present";
                    }
                }

                if (!this.Device.Online)
                {
                    return "Device is offline";
                }

                if (!string.IsNullOrEmpty(this.Device.CompetitionLoaded) && this.Device.CompetitionLoaded != this._competitionId)
                {
                    return "Device used in other competition";
                }
                // Bis hier der allgemeine Teil
                // Ab hier sind wir zumindest in der eigenen COmpetition ...
                if(this.Device.State == DeviceStates.MarkingQualification || this.Device.State == DeviceStates.MarkingFinal)
                {
                    return this.Device.Statetext;
                }

                switch(this.Device.State)
                {
                    case DeviceStates.StartNoData:
                        return "Cleared";
                    case DeviceStates.StartDataLoaded:
                        return "Start, data loaded";
                    case DeviceStates.StartRelease:
                        return "Ready for judging";
                    case DeviceStates.StartOldData:
                        return "Start, old data available";
                    case DeviceStates.Signature:
                        return "Doing signature";
                    case DeviceStates.FinalDanceConfirmed:
                        return "Final, dance confirmed";
                    case DeviceStates.FinalAllPlacesSet:
                        return "Final, all places set";
                    case DeviceStates.Message:
                        return "A message is shown";
                    case DeviceStates.LanguageSelection:
                        return "Language selection";
                }

                return "";
            }
        }

        private void Device_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            this.NotifyPropertyChanged("State");
        }

        public string GetCSVData(string CompId)
        {
            var ret = "";

            var signature = "";
            if (this.Signature.Count > 0)
            {
                signature = String.Format("\"\"{0}\"\";\"\"{1}\"\";{2}", CompId, this.Sign, this.Signature.Count);
                foreach (var c in this.Signature)
                {
                    signature += String.Format(";\"\"{0};{1};{2};{3}\"\"", c.X1, c.Y1, c.X2, c.Y2);
                }
                signature += ";";
            }
            ret = String.Format("{0};{1};{2};{3};{4};{5};{6};\"{7}\";{8};\"\";\"\"", this.Sign, this.FirstName, this.LastName, this.Country, this.Chapter, this.Device != null ? this.Device.Id : this.DeviceId, this.Language, signature, string.IsNullOrEmpty(this.Pin) ? "\"\"" : this.Pin);
            return ret;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Judge))
            {
                return false;
            }

            var j = (Judge) obj;
            return this.Sign.Equals(j.Sign);
        }

        public override int GetHashCode()
        {
            return this.Sign.GetHashCode();
        }
    }
}