// // TPS.net TPS8 mobileControl
// // DictionaryDoubleItemConverter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Data;

namespace mobileControl.Model
{
    public class DictionaryDoubleItemConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var dict = value as Dictionary<string, double>;
            if (dict == null)
            {
                return null;
            }
            var parm = parameter as string;
            if (parm == null)
            {
                return null;
            }

            if (dict.ContainsKey(parm))
            {
                return dict[parm];
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}