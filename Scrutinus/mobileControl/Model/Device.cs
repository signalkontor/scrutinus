// // TPS.net TPS8 mobileControl
// // Device.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System;

namespace mobileControl.Model
{
    public class Device : DataObject
    {
        private int _Battery;
        private string _competitionLoaded;
        private string _dance;
        private int _heat;
        private string _Id;
        private bool _isLoading;
        private bool _isOnline;
        private int _lastMessageType;
        private int _marks;
        private DeviceStates _State;
        private DateTime _timeStamp;

        public string Id
        {
            get { return this._Id; }
            set
            {
                if(this._Id != value)
                {
                    this._Id = value;
                    this.NotifyPropertyChanged("Id");
                }
            }
        }

        public int Battery
        {
            get { return this._Battery; }
            set
            {
                if(this._Battery != value)
                {
                    this._Battery = value;
                    this.NotifyPropertyChanged("Battery");
                }
            }
        }

        public DateTime TimeStamp
        {
            get { return this._timeStamp; }
            set
            {
                this._timeStamp = value;
                this.NotifyPropertyChanged("TimeStamp");
            }
        }

        public DeviceStates State
        {
            get { return this._State; }
            set
            {
                if(this._State != value)
                {
                    this._State = value;
                    if (this._State == DeviceStates.StartNoData)
                    {
                        this.LastMessageType = 0;
                    }

                    this.NotifyPropertyChanged("State");
                }
            }
        }

        public bool Online
        {
            get { return this._isOnline; }
            set
            {
                if (this._isOnline != value)
                {
                    this._isOnline = value;
                    this.NotifyPropertyChanged("Online");
                }
            }
        }

        public bool IsCharging
        {
            get { return this._isLoading; }
            set
            {
                if (this._isLoading != value)
                {
                    this._isLoading = value;
                    this.NotifyPropertyChanged("IsCharging");
                }
            }
        }

        public int LastMessageType
        {
            get { return this._lastMessageType; }
            set
            {
                if(this._lastMessageType != value)
                {
                    this._lastMessageType = value;
                    this.NotifyPropertyChanged("LastMessageType");
                }
            }
        }

        public string CompetitionLoaded
        {
            get { return this._competitionLoaded; }
            set
            {
                if (this._competitionLoaded != value)
                {
                    this._competitionLoaded = value;
                    this.NotifyPropertyChanged("CompetitionLoaded");
                }
            }
        }

        public string Statetext
        {
            get { return String.Format("{0}, Heat {1}, Marks {2}", this._dance, this._heat, this._marks); }
        }

        public void UpdateState(string dance, int heat, int marks)
        {
            if (this._dance != dance || heat != this._heat || marks != this._marks)
            {
                this._dance = dance;
                this._heat = heat;
                this._marks = marks;

                this.NotifyPropertyChanged("Statetext");
            }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Device))
            {
                return false;
            }
            if (this.Id == null)
            {
                return false;
            }

            var d = (Device) obj;
            return this.Id.Equals(d.Id);
        }
    }
}