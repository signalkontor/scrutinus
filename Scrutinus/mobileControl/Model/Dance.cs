// // TPS.net TPS8 mobileControl
// // Dance.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH
namespace mobileControl.Model
{
    public class Dance
    {
        public string Code { get; set; }
        public int SortOrder { get; set; }
        public string Name { get; set; }
    }
}