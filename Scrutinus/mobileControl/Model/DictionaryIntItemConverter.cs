// // TPS.net TPS8 mobileControl
// // DictionaryIntItemConverter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Data;

namespace mobileControl.Model
{
    public class DictionaryIntItemConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var dict = value as Dictionary<string, Dictionary<string, int>>;
            var parm = parameter as string;
            string dance;
            string judge;
            var isFinal = false;
            if(parm.Contains("_"))
            {
                var data = parm.Split('_');
                judge = data[0];
                dance = data[1];
                isFinal = true;
            }
            else
            {
                dance = "all";
                judge = parm;
            }
            if (dict != null && dict.ContainsKey(dance))
            {
                var values = dict[dance];
                if(values.ContainsKey(judge))
                {
                    return isFinal ?  (values[judge] == 0 ? " " : values[judge].ToString()) : values[judge].ToString();
                }
            }

            return " ";
        }


        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}