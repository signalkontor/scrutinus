// // TPS.net TPS8 mobileControl
// // BoolToBackgroundConverter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace mobileControl.Model
{
    public class BoolToBackgroundConverter : IValueConverter
    {
        public object Convert(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            var isLoading = (bool) value;
            return isLoading ? new SolidColorBrush(Colors.Green) : new SolidColorBrush(Colors.LightCyan);
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}