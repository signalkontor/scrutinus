// // TPS.net TPS8 mobileControl
// // DeviceStateToStringConverter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System;
using System.Globalization;
using System.Windows.Data;

namespace mobileControl.Model
{
    public class DeviceStateToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            var state = (DeviceStates) value;
            switch (state)
            {
                case DeviceStates.MarkingFinal:
                    return "Marking Final";
                case DeviceStates.MarkingQualification:
                    return "Marking Qualification";
                case DeviceStates.StartNoData:
                    return "Cleared";
                case DeviceStates.StartOldData:
                    return "Start, old data available";
                case DeviceStates.StartDataLoaded:
                    return "Start, data loaded";
                case DeviceStates.StartRelease:
                    return "Start, ready for judging";
                case DeviceStates.Signature:
                    return "Signature";
                case DeviceStates.Message:
                    return "Showing message";
                case DeviceStates.QualfificationHelpmarks:
                    return "Qualification, Helpmarks";
            }

            return "unknow State";
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}