﻿// // TPS.net TPS8 mobileControl
// // DataModel.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using mobileControl.XDServer;

namespace mobileControl.Model
{
    public class PageModel : ViewModelBase
    {
        private bool isactive;
        public string CompetitionId { get; set; }
        public string Title {get;set;}
        public string TabCaption {get;set;}
        public ContentControl TabContent {get; set; }

        public bool IsActive
        {
            get
            {
                return this.isactive;
            }
            set
            {
                this.isactive = value;
                this.RaisePropertyChanged(() => this.IsActive);
            }
        }
    }

    public class DataModel : ViewModelBase
    {
        public delegate void DeviceStateEventHandler(object sender, Device device);

        private string logMessages;

        public DataModel()
        {
            this.Devices = new ObservableCollection<Device>();
            this.Windows = new Dictionary<string, CompetitionWindow>();
            this.Pages = new ObservableCollection<PageModel>();
            this.ClearDeviceCommand = new RelayCommand<Device>(this.ClearDevice);
        }

        public ObservableCollection<Device> Devices { get; set; }

        public Dictionary<string, CompetitionWindow> Windows { get; set; }

        public ObservableCollection<PageModel> Pages { get; set; }

        public ICommand ClearDeviceCommand { get; set; }

        public string LogMessages
        {
            get
            {
                return this.logMessages;
            }
            set
            {
                this.logMessages = value;
                this.RaisePropertyChanged(() => this.LogMessages);
            }
        }

        public void AppendLogMessage(string message)
        {
            var log = this.logMessages + "\r\n" + message;
            if (log.Length > 512)
            {
                log = log.Substring(log.Length - 512);
            }

            this.LogMessages = log;
        }

        public void AddDevice(Device device)
        {
            // Find the right position
            var index = 0;
            while (index < this.Devices.Count && String.Compare(device.Id, this.Devices[index].Id, false) > 0)
            {
                index++;
            }

            this.Devices.Insert(index, device);
            device.PropertyChanged += this.device_PropertyChanged;
        }

        public event DeviceStateEventHandler DeviceStateChanged;

        private void device_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if(e.PropertyName == "State")
            {
                var device = (Device) sender;

                if(this.DeviceStateChanged != null)
                {
                    this.DeviceStateChanged(this, device);
                }          
            }
        }

        private void ClearDevice(Device device)
        {
            var result = MessageBox.Show(
                "This will clear the device regardles what data is loaded. Are you sure",
                "Clear Device",
                MessageBoxButton.OKCancel);

            if (result == MessageBoxResult.Cancel)
            {
                return;
            }

            CommunicationHandler.ClearDevice(device.Id, device.CompetitionLoaded);
        }

        internal void SaveAvailableDevices(string path)
        {
            var writer = new StreamWriter(path + "\\eJS_Available.dat", false, Encoding.Default);

            foreach (var device in this.Devices)
            {
                writer.WriteLine("{0};\"{1}\"", device.Id, DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss"));
            }
            writer.Close();
        }
    }
}
