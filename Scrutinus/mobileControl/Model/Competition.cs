// // TPS.net TPS8 mobileControl
// // Competition.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using mobileControl.Helper;

namespace mobileControl.Model
{
    public class Competition : DataObject
    {
        private readonly byte[] encryptionArray = new byte[]{5, 2, 6, 12, 11, 16, 21};

        private readonly object fileLocker = new object();

        public Competition(string id, string line, bool isNotNew)
        {
            this.SelectedDevices = new ObservableCollection<Device>();

            this.State = isNotNew ? "2 - Used" : "1 - New";

            var data = TpsHelper.TPSSplitter(line);
            this.Type = Int32.Parse(data[0]);
            this.AgeGroup = data[2];
            this.Title = data[5];
            this.Round = data[6];
            this.MarksStrict = Int32.Parse(data[18]) == 1 ? true : false;
            this.MarksFrom = Int32.Parse(data[9]);
            if (string.IsNullOrWhiteSpace(data[10]))
            {
                this.MarksTo = this.MarksFrom;
            }
            else
            {
                this.MarksTo = Int32.Parse(data[10]);   
            }

            this.Section = data[13];
            this.Id = id;
            this.LanguageCode = Int32.Parse(data[16]);
            this.Flags = Int32.Parse(data[17]);

            switch(this.Section)
            {
                case "L":
                    this.Section = "Latin";
                    break;
                case "S":
                    this.Section = "Standard";
                    break;
            }

            this.Dances = new List<Dance>();
            this.HeatStrings = new List<string>();

            if(mobileControlSettings.Default.AutoLoad)
            {
                this.ReadyToLoad = true;
            }
            else
            {
                this.ReadyToLoad = false;
            }
        }

        public string Id { get; set; }
        public bool ReadyToLoad { get; set; }
        public string Title { get; set; }
        public string Round { get; set; }
        public string AgeGroup { get; set; }
        public string Section { get; set; }
        public DateTime Created { get; set; }
        public int Type { get; set; }
        public List<string> HeatStrings { get; set; }
        public ObservableCollection<Couple> Couples { get; set; }
        public ObservableCollection<Judge> Judges { get; set; }
        public ObservableCollection<Device> SelectedDevices { get; set; }
        public int Flags { get; set; }
        public int MarksFrom { get; set; }
        public int MarksTo { get; set; }
        public bool MarksStrict { get; set; }
        public int LanguageCode { get; set; }
        public List<Dance> Dances { get; set; }

        public string State { get; set; }

        private string EncyptString(string str, int counter)
        {
            var index = counter % 6;
            var newLine = "";
            for (var i = 0; i < str.Length; i++)
            {
                newLine += (char)((byte)str[i] + this.encryptionArray[index]);
                index++;
                if (index >= this.encryptionArray.Length)
                {
                    index = 0;
                }
            }

            return newLine;
        }

        public void SaveResults()
        {
            // lets go: write Result to file
            var coupleList = this.Couples.OrderBy(c => c.CompetitionId).ThenBy(o => o.Number).GroupBy(c => c.CompetitionId);
            var judgesList = this.Judges.OrderBy(o => o.Sign).ToList();
            var dances = this.Dances.OrderBy(d => d.SortOrder).ToList();

            StreamWriter stream = null;
            
            var lineCount = 0;
            var marksCount = 0;
           
            foreach (var coupleGroup in coupleList)
            {
                stream?.Close();
                stream = new StreamWriter(mobileControlSettings.Default.DataPath + "\\" + coupleGroup.Key + "_Wt.dat", false, Encoding.Default);

                foreach (var couple in coupleGroup)
                {
                    var line = couple.Number.ToString() + ";" + judgesList.Count();
                    foreach (var judge in judgesList)
                    {
                        marksCount = 0;
                        line += ";" + judge.Sign + ";\"";
                        foreach (var dance in dances)
                        {
                            
                            var mark = couple.GetResult(dance, judge);

                            // do we have merged competitions? than we need to recalculate the place
                            //if (coupleGroup.Count() != this.Couples.Count)
                            //{
                            //    var better = this.Couples.Count(c => c.CompetitionId != coupleGroup.Key
                            //                                         && c.Judgements.Any(j =>
                            //                                             j.Dance.Code == dance.Code &&
                            //                                             j.Judge.Sign == judge.Sign &&
                            //                                             j.Value < mark.Value));

                            //    mark.Value = mark.Value - better;
                            //}

                            if (mark.Value < 1)
                            {
                                if (mark.Others > 0)
                                {
                                    if (mark.Others < 3)
                                    {
                                        line += mark.Others.ToString() + ";";
                                    }
                                    else
                                    {
                                        line += "L;";
                                    }
                                }
                                else
                                {
                                    line += ";";
                                }
                            }
                            else
                            {
                                if (this.Type == 1)
                                {
                                    line += "X;";
                                    marksCount++;
                                }
                                else
                                {
                                    line += mark.Value.ToString() + ";";
                                }
                            }
                        }

                        line += "\";"; // Abschluss muss noch ein leeres ; sein + Anführungszeichen...
                        line += (marksCount * 10).ToString();
                    }
                    stream.WriteLine(line);
                }

                // Write this back to our stream:
                
                lineCount++;
            }
            stream?.Close();
        }

        public void SaveJudgeData()
        {
            lock (this.fileLocker)
            {
                try
                {
                    var writer = new StreamWriter(mobileControlSettings.Default.DataPath + "\\" + this.Id + "_Wr.dat", false, Encoding.Default);
                    foreach (var judge in this.Judges.OrderBy(j => j.Sign))
                    {
                        var data = judge.GetCSVData(this.Id);
                        writer.WriteLine(data);
                    }
                    writer.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Could not save Judge Data: " + ex.Message);
                }
            }
        }

        public void SaveCompetitionData()
        {
            try
            {
                this.SaveResults();
                this.SaveJudgeData();
                this.SaveSelectedDevices();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Could not save results ... " + ex.Message);
            }
        }

        private void SaveSelectedDevices()
        {
            lock (this.fileLocker)
            {
                try
                {
                    var writer = new StreamWriter(mobileControlSettings.Default.DataPath + "\\" + this.Id + "_Devices.dat", false, Encoding.Default);
                    foreach (var device in this.SelectedDevices)
                    {
                        writer.WriteLine(device.Id);
                    }
                    writer.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Could not save Device Data: " + ex.Message);
                }
            }
        }

        public void ReadSelectdDevices(DataModel context)
        {
            var filename = mobileControlSettings.Default.DataPath + "\\" + this.Id + "_Devices.dat";

            if (!File.Exists(filename))
            {
                return;
            }

            var reader = new StreamReader(filename);
            while (!reader.EndOfStream)
            {
                var deviceId = reader.ReadLine();
                var device = context.Devices.SingleOrDefault(d => d.Id == deviceId);
                if (device != null)
                {
                    this.SelectedDevices.Add(device);
                }
            }
            reader.Close();
        }
    }
}