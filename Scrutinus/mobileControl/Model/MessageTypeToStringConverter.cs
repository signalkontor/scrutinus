// // TPS.net TPS8 mobileControl
// // MessageTypeToStringConverter.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH

using System;
using System.Globalization;
using System.Windows.Data;

namespace mobileControl.Model
{
    public class MessageTypeToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            var type = (int) value;
            switch(type)
            {
                case 12:
                case 22:
                    return "Total Marking";
                case 11:
                case 21:
                    return "Single Marking";
                case 31:
                    return "Signature";
            }

            return "";
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}