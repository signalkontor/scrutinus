// // TPS.net TPS8 mobileControl
// // Judgement.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gr�hn / signalkontor GmbH
namespace mobileControl.Model
{
    public class Judgement
    {
        public Judge Judge { get; set; }
        public Dance Dance { get; set; }
        public int Value { get; set; }
        public int Others { get; set; }
    }
}