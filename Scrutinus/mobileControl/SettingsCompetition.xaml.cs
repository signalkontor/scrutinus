﻿// // TPS.net TPS8 mobileControl
// // SettingsCompetition.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows;
using mobileControl.Model;
using mobileControl.XDServer;

namespace mobileControl
{
    /// <summary>
    /// Interaction logic for SettingsCompetition.xaml
    /// </summary>
    public partial class SettingsCompetition : Window
    {
        private readonly Competition _competition;
        private readonly int oldFrom;
        private readonly bool oldStrict;
        private readonly int oldUntil;

        public SettingsCompetition(Competition competition)
        {
            this.InitializeComponent();
            this.oldFrom = competition.MarksFrom;
            this.oldUntil = competition.MarksTo;
            this.oldStrict = competition.MarksStrict;
            this._competition = competition;
            this.DataContext = competition;
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            CommunicationHandler.SendConfig(this._competition);
            this.Close();
        }

        private void bntCancel_Click(object sender, RoutedEventArgs e)
        {
            this._competition.MarksFrom = this.oldFrom;
            this._competition.MarksTo = this.oldUntil;
            this._competition.MarksStrict = this.oldStrict;
            this.Close();
        }
    }
}
