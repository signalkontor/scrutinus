﻿// // TPS.net TPS8 mobileControl
// // OpenCompetition.xaml.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using mobileControl.Model;

namespace mobileControl
{
    /// <summary>
    /// Interaction logic for OpenCompetition.xaml
    /// </summary>
    public partial class OpenCompetition : Window
    {
        public List<Competition> _data;

        private readonly Encoding fileEncoding;

        private readonly FileSystemWatcher fileSystemWatcher;

        public OpenCompetition(Encoding fileEncoding)
        {
            this.InitializeComponent();

            this.fileEncoding = fileEncoding;

            this.LoadData();
            this.fileSystemWatcher = new FileSystemWatcher(mobileControlSettings.Default.DataPath, "*_Td.dat");
            this.fileSystemWatcher.Created += this.fileSystemWatcher_Created;
        }

        public Competition Competition { get; set; }

        public bool ResultOK { get; set; }

        private void fileSystemWatcher_Created(object sender, FileSystemEventArgs e)
        {
            this.LoadData();
        }

        private string getIdFromFilename(string filename)
        {
            var file = Path.GetFileName(filename).ToLower();
            return file.Replace("_td.dat", "");
        }

        private void LoadData()
        {
            this._data = new List<Competition>();
            var files = Directory.GetFiles(mobileControlSettings.Default.DataPath, "*_Td.dat");
            foreach (var file in files)
            {
                var deviceFile = file.Replace("_Td", "_Devices");
                var exists = File.Exists(deviceFile);

                // wir öffnen die Datei und laden die wichtigen Infos heraus
                var inS = new StreamReader(file, this.fileEncoding, true);
                this._data.Add(new Competition(this.getIdFromFilename(file), inS.ReadLine(), exists) {Created = File.GetLastWriteTime(file) });
                inS.Close();
            }

            this.ListCompetition.ItemsSource = this._data.OrderBy(o => o.State).ThenByDescending(c => c.Created);
        }

        private void DataGrid_Sorting(object sender, DataGridSortingEventArgs e)
        {
            return;           
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            if (this.ListCompetition.SelectedItem != null)
            {
                this.DialogResult = true;
                this.Competition = (Competition) this.ListCompetition.SelectedItem;
                this.Close();
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false; 
            this.Close();
        }

        private void ListCompetition_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            this.btnOK_Click(null, null);
        }
    }
}
