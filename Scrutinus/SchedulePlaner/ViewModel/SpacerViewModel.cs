﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;

namespace SchedulePlaner.ViewModel
{
    public class SpacerViewModel : ViewModelBase
    {
        private DateTime plannedStartTime;

        public DateTime PlannedStartTime
        {
            get => plannedStartTime;
            set
            {
                plannedStartTime = value;
                RaisePropertyChanged();
            }
        }
    }
}
