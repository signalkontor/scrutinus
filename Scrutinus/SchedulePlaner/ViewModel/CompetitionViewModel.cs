﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModel.Models;
using GalaSoft.MvvmLight;

namespace SchedulePlaner.ViewModel
{
    public class CompetitionViewModel : ViewModelBase
    {
        public Competition Competition { get; set; }

        public ObservableCollection<RoundViewModel> Rounds { get; set; }

        public int Couples { get; set; }
    }
}
