﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using DataModel.Models;

namespace SchedulePlaner.ViewModel
{
    public class TimePlanerViewModel : ViewModelBase
    {
        public TimePlanerViewModel()
        {
            this.Competitions = GenerateSampleData();
            this.PlannedRounds = new ObservableCollection<ViewModelBase>();
            PlannedRounds.Add(new SpacerViewModel());
        }
        public IEnumerable<CompetitionViewModel> Competitions { get; set; }

        public ObservableCollection<ViewModelBase> PlannedRounds { get; set; }

        public void InsertRound(SpacerViewModel targetObject, RoundViewModel round)
        {
            var index = this.PlannedRounds.IndexOf(targetObject);
            if (index > -1 && !this.CanInsertRoundAtPosition(index, round))
            {
                return;
            }

            index = this.PlannedRounds.IndexOf(round);
            if (index > -1)
            {
                // move the item:
                this.PlannedRounds.RemoveAt(index);
                this.PlannedRounds.RemoveAt(index);
            }
            else
            {
                // Remove the item in the left list
                foreach (var competition in this.Competitions)
                {
                    competition.Rounds.Remove(round);
                }
            }

            index = this.PlannedRounds.IndexOf(targetObject);
            if (index < 0)
            {
                return;
            }

            this.PlannedRounds.Insert(index, new SpacerViewModel());
            this.PlannedRounds.Insert(index + 1, round);

            CalculateTimes();
        }

        private void CalculateTimes()
        {
            TimeSpan time = TimeSpan.Zero;
            foreach (var element in this.PlannedRounds)
            {
                if (element is RoundViewModel round)
                {
                    time = time.Add(round.PlannedTime);
                }
                else
                {
                    var spacer = (SpacerViewModel) element;
                    spacer.PlannedStartTime = DateTime.Today + time;
                }
            }
        }

        private bool CanInsertRoundAtPosition(int index, RoundViewModel round)
        {
            for (int i = index; i < this.PlannedRounds.Count; i++)
            {
                // todo: Check if a round is before this one
                if (this.PlannedRounds[i] is RoundViewModel roundViewModel)
                {
                    if (roundViewModel.RoundNumber < round.RoundNumber &&
                        roundViewModel.CompetitionViewModel.Competition.Id == round.CompetitionViewModel.Competition.Id)
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        private IEnumerable<CompetitionViewModel> GenerateSampleData()
        {
            var competitions = new List<CompetitionViewModel>();

            var competitionViewModel = new CompetitionViewModel()
            {
                Competition = new Competition() { Title = "WDSF World Open Standard", Id = 1},
                
            };

            competitionViewModel.Rounds = new ObservableCollection<RoundViewModel>()
            {
                new RoundViewModel()
                {
                    CompetitionViewModel = competitionViewModel,
                    Name = "Round 1",
                    RoundNumber = 1,
                    NumberParticipants = 48,
                    PlannedHeats = 4,
                    PlannedDances = 5
                },
                new RoundViewModel()
                {
                    CompetitionViewModel = competitionViewModel,
                    Name = "Redance",
                    RoundNumber = 2,
                    NumberParticipants = 12,
                    PlannedHeats = 1,
                    PlannedDances = 5
                },
                new RoundViewModel()
                {
                    CompetitionViewModel = competitionViewModel,
                    Name = "2nd Round",
                    RoundNumber = 3,
                    NumberParticipants = 36,
                    PlannedHeats = 3,
                    PlannedDances = 5
                },
                new RoundViewModel()
                {
                    CompetitionViewModel = competitionViewModel,
                    Name = "3rd Round",
                    RoundNumber = 4,
                    NumberParticipants = 24,
                    PlannedHeats = 3,
                    PlannedDances = 5
                },
                new RoundViewModel()
                {
                    CompetitionViewModel = competitionViewModel,
                    Name = "4th Round",
                    RoundNumber = 5,
                    NumberParticipants = 12,
                    PlannedHeats = 2,
                    PlannedDances = 5
                },
                new RoundViewModel()
                {
                    CompetitionViewModel = competitionViewModel,
                    Name = "Final",
                    RoundNumber = 6,
                    NumberParticipants = 6,
                    PlannedHeats = 1,
                    PlannedDances = 5
                },
            };

            competitions.Add(competitionViewModel);

            competitionViewModel = new CompetitionViewModel()
            {
                Competition = new Competition() { Title = "WDSF World Open Latin", Id = 2 },

            };

            competitionViewModel.Rounds = new ObservableCollection<RoundViewModel>()
            {
                new RoundViewModel()
                {
                    CompetitionViewModel = competitionViewModel,
                    Name = "Round 1",
                    RoundNumber = 1,
                    NumberParticipants = 48,
                    PlannedHeats = 4,
                    PlannedDances = 5
                },
                new RoundViewModel()
                {
                    CompetitionViewModel = competitionViewModel,
                    Name = "Redance",
                    RoundNumber = 2,
                    NumberParticipants = 12,
                    PlannedHeats = 1,
                    PlannedDances = 5
                },
                new RoundViewModel()
                {
                    CompetitionViewModel = competitionViewModel,
                    Name = "2nd Round",
                    RoundNumber = 3,
                    NumberParticipants = 36,
                    PlannedHeats = 3,
                    PlannedDances = 5
                },
                new RoundViewModel()
                {
                    CompetitionViewModel = competitionViewModel,
                    Name = "3rd Round",
                    RoundNumber = 4,
                    NumberParticipants = 24,
                    PlannedHeats = 3,
                    PlannedDances = 5
                },
                new RoundViewModel()
                {
                    CompetitionViewModel = competitionViewModel,
                    Name = "4th Round",
                    RoundNumber = 5,
                    NumberParticipants = 12,
                    PlannedHeats = 2,
                    PlannedDances = 5
                },
                new RoundViewModel()
                {
                    CompetitionViewModel = competitionViewModel,
                    Name = "Final",
                    RoundNumber = 6,
                    NumberParticipants = 6,
                    PlannedHeats = 1,
                    PlannedDances = 5
                },
            };

            competitions.Add(competitionViewModel);

            return competitions;
        }

        
    }
}
