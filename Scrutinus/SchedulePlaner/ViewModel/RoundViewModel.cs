﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight;

namespace SchedulePlaner.ViewModel
{
    public class RoundViewModel : ViewModelBase
    {
        public string Name { get; set; }

        public ICommand EditCommand { get; set; }

        public CompetitionViewModel CompetitionViewModel { get; set; }

        public int RoundNumber { get; set; }

        public int NumberParticipants { get; set; }

        public int PlannedHeats { get; set; }

        public int PlannedDances { get; set; }

        public TimeSpan PlannedTime
        {
            get
            {
                return new TimeSpan(0, this.PlannedHeats * this.PlannedDances * 2, 0);
            }
        }
    }
}
