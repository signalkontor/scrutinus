﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using GalaSoft.MvvmLight;
using SchedulePlaner.ViewModel;

namespace SchedulePlaner
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Point lastMouseDown;
        private ViewModelBase draggedItem;
        private TimePlanerViewModel viewModel;

        public MainWindow()
        {
            InitializeComponent();

            this.viewModel = (TimePlanerViewModel) this.DataContext;
            
        }

        private void SourceTreeMouseDown
            (object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                this.lastMouseDown = e.GetPosition(this.SourceTreeView);
            }
        }

        private void SourceTreeMouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                if (e.LeftButton == MouseButtonState.Pressed)
                {
                    Point currentPosition = e.GetPosition(this.SourceTreeView);

                    if ((Math.Abs(currentPosition.X - this.lastMouseDown.X) > 10.0) ||
                        (Math.Abs(currentPosition.Y - this.lastMouseDown.Y) > 10.0))
                    {
                        if (sender is TreeViewItem treeviewItem)
                        {
                            this.draggedItem = (ViewModelBase)treeviewItem.DataContext;
                        }

                        if (sender is ListViewItem listViewItem)
                        {
                            this.draggedItem = (ViewModelBase)listViewItem.DataContext;
                        }

                        if (draggedItem != null)
                        {
                            DragDropEffects finalDropEffect =
                                DragDrop.DoDragDrop(SourceTreeView,
                                    this.draggedItem,
                                    DragDropEffects.Copy);
                        }
                    }
                }
            }
            catch (Exception)
            {

            }
        }

        private void ListViewDragOver(object sender, DragEventArgs e)
        {
            try
            {
                if (!(sender is ListViewItem item))
                {
                    return;
                }

                var viewModel = item.DataContext;

                if (viewModel is SpacerViewModel)
                {
                    e.Effects = DragDropEffects.Copy;
                    e.Handled = true;
                }
                else
                {
                    e.Effects = DragDropEffects.None;
                    e.Handled = true;
                }
            }
            catch (Exception)
            {

            }
        }

        private void TargetListViewDrop(object sender, DragEventArgs e)
        {
            if (!(sender is ListViewItem item))
            {
                return;
            }
            
            var target = item.DataContext;

            if (target is SpacerViewModel)
            {
                if(this.draggedItem is RoundViewModel)
                { 
                    this.viewModel.InsertRound((SpacerViewModel)target, (RoundViewModel)this.draggedItem);
                }

                if (this.draggedItem is CompetitionViewModel)
                {
                    var competitionViewModel = this.draggedItem as CompetitionViewModel;
                    foreach (var round in competitionViewModel.Rounds.ToList())
                    {
                        this.viewModel.InsertRound((SpacerViewModel)target, round);
                    }
                }
            }

            DragDropHelper.SetIsDragOver((DependencyObject)sender, false);
        }


        private void ListElementOnDragEnter(object sender, DragEventArgs e)
        {
            DragDropHelper.SetIsDragOver((DependencyObject)sender, true);
        }

        private void ListElementPreviewDragLeave(object sender, DragEventArgs e)
        {
            DragDropHelper.SetIsDragOver((DependencyObject) sender, false);
        }
    }
}
