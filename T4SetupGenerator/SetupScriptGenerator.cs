﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TextTemplating;

namespace Scrutinus.T4
{
    public abstract class SetupScriptGenerator : TextTransformation
    {
        private List<Tuple<string, string>> directorysAndFilesList;

        private string baseDir;

        public IEnumerable<IGrouping<string, Tuple<string, string>>> GetFilesGrouped(string path)
        {
            directorysAndFilesList = new List<Tuple<string, string>>();
            this.baseDir = path;
            this.FillList(path);

            var grouping = this.directorysAndFilesList.OrderBy(o => o.Item1).GroupBy(o => o.Item1);

            return grouping;
        }

        private void FillList(string path)
        {
            var directories = Directory.GetDirectories(path);

            foreach (var directory in directories)
            {
                this.FillList(directory);
            }

            var files = Directory.GetFiles(path);

            foreach (var file in files.Where(f => (!f.EndsWith(".pdb") && !f.EndsWith(".xml")) || f.Contains("XMLRules")))
            {
                var newfile = file.Replace(path + "\\", "");
                var newDir = path.Replace(baseDir, "");
                this.directorysAndFilesList.Add(new Tuple<string, string>(newDir, newfile));                
            }
        }
    }
}
