﻿// // TPS.net TPS8 MobileControlLib
// // TMQeJSMsg.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;

namespace com.signalkontor.XD_Server
{
    public class TMQeJSMsg
    {
        // sonst 0
        public string DoCmd;          // Auszuführendes Kommando

        public string MsgData;        // Die eigentliche Nachricht

        public string Receiver;       // Adresse des Empfängers

        public string ReceiverIP;     // IP Adresse Empfänger wird nicht mehr genutzt

        public int ResponseId;        // Im Falle einer Antwort die TargetId der empfangenen Nachricht,

        public string Sender;         // Sender der Nachricht

        public int TargetId;          // Art der Nachricht
        public DateTime TimeStamp;    // Lokaler Timestamp beim Erstellen der Message
    }
}