﻿// // TPS.net TPS8 MobileControlLib
// // TpsHelper.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;

namespace mobileControl.Helper
{
    public static class TpsHelper
    {
        public static string[] TPSSplitter(string data)
        {
            var tokens = new List<string>();
            var upmode = false;
            var token = "";

            for (var i = 0; i < data.Length; i++)
            {
                if (data[i] == ';' && !upmode)
                {
                    tokens.Add(token);
                    token = "";
                    continue;
                }

                if (data[i] == '"')
                {
                    upmode = !upmode;
                    continue;
                }

                token += data[i];
            }
            // Add the last one at end of line
            tokens.Add(token);

            return tokens.ToArray();

        }
    }
}
