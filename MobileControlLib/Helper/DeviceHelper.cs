﻿// // TPS.net TPS8 MobileControlLib
// // DeviceHelper.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using DataModel.Models;
using mobileControl.Helper;
using mobileControl.Model;

namespace MobileControlLib.Helper
{
    public static class DeviceHelper
    {
        public static void UpdateDeviceState(Device dev, string stateData)
        {
            var data = TpsHelper.TPSSplitter(stateData);

            dev.BatteryLevel = int.Parse(data[0]);
            var windowId = int.Parse(data[1]);
            var windowState = int.Parse(data[2]);

            dev.IsCharging = data[6] != "battery";

            // Anhängig von beiden Werten setzen wir unseren internen Status
            switch (windowId)
            {
                case 0: // Start-Screen
                    switch (windowState)
                    {
                        case 0:
                            dev.DeviceState = DeviceStates.StartNoData;
                            break;
                        case 1:
                            dev.DeviceState = DeviceStates.StartDataLoaded;
                            break;
                        case 2:
                            dev.DeviceState = DeviceStates.StartRelease;
                            break;
                        case 3:
                            dev.DeviceState = DeviceStates.StartOldData;
                            break;
                    }
                    break;
                case 1: // Eingabe Vor/Zwischenrunden
                    switch (windowState)
                    {
                        case 0:
                        case 1:
                            dev.DeviceState = DeviceStates.MarkingQualification;
                            break;
                        case 2:
                            dev.DeviceState = DeviceStates.QualfificationHelpmarks;
                            break;
                        case 8:
                            dev.DeviceState = DeviceStates.Signature;
                            break;
                    }
                    break;
                case 2: // Finale
                    switch (windowState)
                    {
                        case 0:
                            dev.DeviceState = DeviceStates.MarkingFinal;
                            break;
                        case 1:
                            dev.DeviceState = DeviceStates.FinalAllPlacesSet;
                            break;
                        case 2:
                            dev.DeviceState = DeviceStates.FinalDanceConfirmed;
                            break;
                        case 8:
                            dev.DeviceState = DeviceStates.Signature;
                            break;
                    }
                    break;
                case 4:
                    dev.DeviceState = DeviceStates.LanguageSelection;
                    break;
                case 5:
                    dev.DeviceState = DeviceStates.Message;
                    break;


            }
        }

        public static IEnumerable<EjudgeData> CreateEjudgeData(ScrutinusContext context, Round round, IEnumerable<Device> existingDevices)
        {
            var result = new List<EjudgeData>();

            foreach (var official in round.Competition.GetJudges())
            {
                var eJudgeData = context.EjudgeDatas.SingleOrDefault(e => e.Round.Id == round.Id && e.Judge.Id == official.Id);
                if (eJudgeData == null)
                {
                    eJudgeData = new EjudgeData()
                    {
                        DefaultLanguage = "EN",
                        DeviceId = "",
                        Judge = official,
                        Round = round,

                    };

                    context.EjudgeDatas.Add(eJudgeData);
                    round.EjudgeData.Add(eJudgeData);
                }
                else
                {
                    var device = existingDevices.FirstOrDefault(d => d.DeviceId == eJudgeData.DeviceId);

                    eJudgeData.Device = device;
                }

                result.Add(eJudgeData);
            }

            context.SaveChanges();

            return result;
        }
    }
}
