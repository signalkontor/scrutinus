﻿// // TPS.net TPS8 MobileControlLib
// // MessageHelper.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using com.signalkontor.XD_Server;
using MobileControlLib.EjudgeMessages;

namespace MobileControlLib.Helper
{
    public static class MessageHelper
    {
        public static EjudgeMessage EjudgeMessageFactory(TMQeJSMsg message)
        {
            switch (message.TargetId)
            {
                case 1:
                    return new DeviceStateMessage(message);
                case 11:
                case 21:
                    return new MarkMessage(message);
                case 12:
                case 22:
                    return new CompleteMarkingMessage(message);
                case 31:
                    return new SignatureMessage(message);
            }
            return null;
        }
    }
}
