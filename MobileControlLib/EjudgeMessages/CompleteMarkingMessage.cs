﻿// // TPS.net TPS8 MobileControlLib
// // CompleteMarkingMessage.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using com.signalkontor.XD_Server;
using mobileControl.Helper;

namespace MobileControlLib.EjudgeMessages
{
    public class CompleteMarkingMessage : EjudgeMessage
    {
        public CompleteMarkingMessage(TMQeJSMsg message)
            : base(message)
        {
            this.Marks = new Dictionary<int, List<string>>();
            var data = TpsHelper.TPSSplitter(message.MsgData);
            // Parse the message:
            var couples = int.Parse(data[0]);
            for (var i = 0; i < couples; i++)
            {
                var number = Int32.Parse(data[i * 2 + 1]);
                var list = new List<string>();
                this.Marks.Add(number, list);
                var marking = TpsHelper.TPSSplitter(data[i * 2 + 2]);
                list.AddRange(marking);
            }
        }

        public Dictionary<int, List<String>> Marks { get; set; }
    }
}
