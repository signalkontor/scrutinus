﻿// // TPS.net TPS8 MobileControlLib
// // DeviceStateMessage.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using com.signalkontor.XD_Server;
using mobileControl.Helper;

namespace MobileControlLib.EjudgeMessages
{
    public class DeviceStateMessage : EjudgeMessage
    {
        public DeviceStateMessage(TMQeJSMsg message) : base(message)
        {
            var data = TpsHelper.TPSSplitter(message.MsgData);

            this.DanceIndex = int.Parse(data[3]);
            this.Heat = Int32.Parse(data[4]);
            this.Marks = int.Parse(data[5]);
        }

        public int DanceIndex { get; set; }

        public int Heat { get; set; }

        public int Marks { get; set; }
    }
}
