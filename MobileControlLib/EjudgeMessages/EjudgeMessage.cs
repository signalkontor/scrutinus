﻿// // TPS.net TPS8 MobileControlLib
// // EjudgeMessage.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using com.signalkontor.XD_Server;

namespace MobileControlLib.EjudgeMessages
{
    public class EjudgeMessage
    {
        public EjudgeMessage(TMQeJSMsg message)
        {
            if (message == null)
            {
                return;
            }

            this.RoundCode = message.DoCmd;
            this.Sender = message.Sender;
        }

        public bool IsHandled { get; set; }

        public string RoundCode { get; set; }

        public string Sender { get; set; }
    }
}
