﻿// // TPS.net TPS8 MobileControlLib
// // MarkMessage.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using com.signalkontor.XD_Server;
using mobileControl.Helper;

namespace MobileControlLib.EjudgeMessages
{
    public class MarkMessage : EjudgeMessage
    {
        public MarkMessage(TMQeJSMsg message) : base(message)
        {
            var data = TpsHelper.TPSSplitter(message.MsgData);
            this.Number = int.Parse(data[0]);
            this.DanceIndex = int.Parse(data[1]);

            this.Mark = data[2];
        }

        public int Number { get; set; }

        public int DanceIndex { get; set; }

        public string Mark { get; set; }
    }
}
