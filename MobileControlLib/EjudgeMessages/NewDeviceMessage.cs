﻿// // TPS.net TPS8 MobileControlLib
// // NewDeviceMessage.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using com.signalkontor.XD_Server;
using DataModel.Models;

namespace MobileControlLib.EjudgeMessages
{
    public class NewDeviceMessage : EjudgeMessage
    {
        public NewDeviceMessage(TMQeJSMsg message)
            : base(message)
        {
            
        }

        public Device Device { get; set; }
    }
}
