﻿// // TPS.net TPS8 MobileControlLib
// // SignatureMessage.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using com.signalkontor.XD_Server;
using mobileControl.Helper;

namespace MobileControlLib.EjudgeMessages
{
    public class SignatureMessage : EjudgeMessage
    {
        public SignatureMessage(TMQeJSMsg Message) : base(Message)
        {
            var data = TpsHelper.TPSSplitter(Message.MsgData);

            this.Coordinates = new List<Tuple<double, double, double, double>>();
            for (var i = 3; i < data.Length; i++)
            {
                var c = TpsHelper.TPSSplitter(data[i]);

                if (c.Length > 3)
                {
                    this.Coordinates.Add(
                        new Tuple<double, double, double, double>(double.Parse(c[0]), double.Parse(c[1]), double.Parse(c[2]), double.Parse(c[3])));
                }
            }
        }

        public IList<Tuple<double, double, double, double>> Coordinates { get; set; }
    }
}
