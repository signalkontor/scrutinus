﻿// // TPS.net TPS8 MobileControlLib
// // JudgeLogonMessage.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using DataModel.Models;

namespace MobileControlLib.EjudgeMessages
{
    public class JudgeLogonMessage : EjudgeMessage
    {
        public JudgeLogonMessage()
            : base(null)
        {
            
        }

        public Official Judge { get; set; }

        public string DeviceId { get; set; }

        public Round Round { get; set; }
    }
}
