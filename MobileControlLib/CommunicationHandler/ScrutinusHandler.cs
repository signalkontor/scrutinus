﻿// // TPS.net TPS8 MobileControlLib
// // ScrutinusHandler.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Linq;
using System.Messaging;
using com.signalkontor.XD_Server;
using DataModel;
using DataModel.ModelHelper;
using DataModel.Models;

namespace mobileControl.Helper
{

    public class ScrutinusCommunicationHandler
    {
        private static ScrutinusCommunicationHandler _instance;
        private readonly object _lockObj;
        private readonly MessageQueue _outQueue;

        private ScrutinusCommunicationHandler()
        {
            this._lockObj = new object();

            this._outQueue = new MessageQueue(".\\private$\\TPS.eJudge.out");
            var formatter = new XmlMessageFormatter(new[] { typeof(TMQeJSMsg) });
            this._outQueue.Formatter = formatter;

        }

        private static ScrutinusCommunicationHandler Instance
        {
            get
            {
                if(_instance == null)
                {
                    _instance = new ScrutinusCommunicationHandler();
                }
                return _instance;
            }
        }


        public static void SendData(Round round)
        {
            // Ok, we do it without pin
            foreach (var eJudgeData in round.EjudgeData)
            {
                SendData(round, eJudgeData);
            }
        }

        public static void SendData(Round round, EjudgeData ejudgeData)
        {
            SendData(round, ejudgeData, round.RoundType.IsFinal ? 111 : 101);
        }

        public static void SendData( Round round, EjudgeData ejudgeData, int targetId)
        {
            var data = GetDataForJudge(round, ejudgeData);

            Instance.SendMessage(ejudgeData.DeviceId, targetId, round.EjudgeName, data);

            // Do we have results to send?
            if (round.Markings.Any(m => m.Judge.Id == ejudgeData.Judge.Id))
            {
                data = GetMarksForJudge(round, ejudgeData.Judge);

                data += ";";

                Instance.SendMessage(ejudgeData.DeviceId, targetId + 1, round.EjudgeName, data);
            }
        }


        public static string GetMarksForJudge(Round round, Official judge)
        {
            // Yes, lets build and send:
            var data = round.Qualifieds.Count.ToString();

            foreach (var couple in round.Qualifieds.OrderBy(c => c.Participant.Number))
            {
                data += ";" + couple.Participant.Number + ";\"";

                foreach (var dance in round.DancesInRound.OrderBy(d => d.SortOrder))
                {
                    var mark = round.Markings.SingleOrDefault(j => j.Participant.Id == couple.Participant.Id && j.Judge.Id == judge.Id && j.Dance.Id == dance.Dance.Id && j.Round.Id == round.Id);
                    if (mark != null)
                    {
                        if (round.MarksInputType == MarkingTypes.Marking)
                        {
                            if (mark.Mark == 1)
                            {
                                data += "X";
                            }
                            else
                            {
                                switch (mark.HelpCode)
                                {
                                    case 1:
                                        data += "1";
                                        break;
                                    case 2:
                                        data += "2";
                                        break;
                                    case 3:
                                        data += "L";
                                        break;
                                }
                            }
                        }
                        else
                        {
                            data += mark.Mark.ToString();
                        }
                    }
                    else
                    {
                        // Add empty string otherwise the mark would be missing.
                        data += " ";
                    }

                    data += ";";
                }

                data += "\"";
            }

            return data;
        }

        public static string GetDataForJudge(Round round, EjudgeData eJudgeData)
        {
            var offset = 0;

            var data = "";
            data = String.Format(
                "{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13}",
                round.RoundType.IsFinal ? 2 : 1,
                eJudgeData.Judge.DefaultLanguage,
                eJudgeData.Flags,
                round.Competition.Title,
                round.Competition.AgeGroup.ShortName,
                round.Name,
                round.MarksFrom.HasValue ? round.MarksFrom.Value : 0,
                round.MarksTo.HasValue ? round.MarksTo.Value : 0,
                "1", // Marks strikt: allways set to true
                eJudgeData.Judge.Sign,
                eJudgeData.Judge.Firstname,
                eJudgeData.Judge.Lastname,
                eJudgeData.Judge.Club,
                offset);

            // now add all dances we are dancing:
            data += ";" + round.DancesInRound.Count;

            var heatStrings = new List<string>();

            if (round.RoundType.IsFinal)
            {
                // Finale, wir erzeugen schnell ein paar Strings ...
                var heat = "1;\"";
                foreach (var qualified in round.Qualifieds.OrderBy(q => q.Participant.Number))
                {
                    heat += qualified.Participant.Number + ";";
                }
                // remove last ;
                heat = heat.Substring(0, heat.Length - 1);
                heat += "\"";
                
                foreach (var dance in round.DancesInRound)
                {
                    heatStrings.Add(heat);
                }
            }
            else
            {
                var heats = HeatsHelper.GetHeats(new ScrutinusContext(), round.Id);
                foreach (var dance in heats.Keys.OrderBy(d => d.SortOrder))
                {
                    var heatString = heats[dance].Count.ToString();
                    foreach (var heat in heats[dance])
                    {
                        heatString += ";\"";
                        heatString += string.Join(";", heat.Select(p => p.Number));
                        heatString += ";\"";
                    }

                    heatStrings.Add(heatString);
                }
            }

            foreach (var dance in round.DancesInRound.OrderBy(d => d.SortOrder))
            {
                data += String.Format(";{0};{1};{2}", dance.Dance.ShortName, dance.Dance.DanceName, dance.Dance.DanceName);
                // Add Heats of this dance
                data += ";" + heatStrings[dance.SortOrder - 1];
            }

            // that's all, send data now
            return data;
        }

        public static void ClearDevices(Round round)
        {
            foreach (var eJugdeData in round.EjudgeData)
            {
                if (!string.IsNullOrEmpty(eJugdeData.DeviceId))
                {
                    ClearDevice(eJugdeData.DeviceId, round.EjudgeName);
                }
            } 
        }

        public static void ClearDevice(string devicdeId, string eJudgeRoundId)
        {
            Instance.SendMessage(devicdeId, 122, eJudgeRoundId, "");
        }

        public static void SendStart(Round round)
        {
            foreach (var judge in round.EjudgeData)
            {
                SendStart(judge.DeviceId, round.EjudgeName);
            }
        }

        public static void SendStart(string deviceId, string eJudgeRoundId)
        {
            Instance.SendMessage(deviceId, 121, eJudgeRoundId, "");
        }

        public static void SendSaveCmd(Round round)
        {
            foreach (var judge in round.EjudgeData)
            {
                Instance.SendMessage(judge.DeviceId, round.RoundType.IsFinal ? 113 : 103, round.EjudgeName, "");
            }
        }

        public static void SendConfig(Round round)
        {
            // Resend competition data but with targetId = 131
            foreach (var eJudgeData in round.EjudgeData)
            {
                SendData(round, eJudgeData, 131);
            }
        }

        public static void MoveJudges(Round round, int dance, int heat)
        {
            foreach (var judge in round.EjudgeData)
            {
                MoveJudge(judge.DeviceId, round.EjudgeName, dance, heat);
            }
        }

        public static void MoveJudge(string deviceId, string competitionId, int dance, int heat)
        {
            var data = String.Format("{0};{1}", dance, heat);
            Instance.SendMessage(deviceId, 300, competitionId, data);

        }

        public static void AddOrMoveCouple(Round round, string number, int dance, int heat, int targetId)
        {
            var str = number;

            for (var i = dance; i <= round.DancesInRound.Count;i++ )
            {
                str += String.Format(";{0};{1}", i, heat);
            }

            foreach (var eJudgeData in round.EjudgeData)
            {
                Instance.SendMessage(eJudgeData.DeviceId, targetId, round.EjudgeName, str);
            }   
        }

        public static void AddCouple(Round round, string number, int dance, int heat)
        {
            AddOrMoveCouple(round, number, dance, heat, 220);
        }

        public static void MoveCouple(Round round, string number, int dance, int heat)
        {
            AddOrMoveCouple(round, number, dance, heat, 200);
        }

        public static void RemoveCouple(Round round, string number, int dance)
        {
            var str = number;
            for(var i=dance;i<=round.DancesInRound.Count;i++)
            {
                str += String.Format(";{0}", i);
            }

            foreach (var eJudgeData in round.EjudgeData)
            {
                Instance.SendMessage(eJudgeData.DeviceId, 210, round.EjudgeName, str);
            } 
        }

        private void SendMessage(string receiver, int TargetId, string DoCmd, string MsgData)
        {
            var tpsMessage = new TMQeJSMsg()
                            {
                                TimeStamp = DateTime.Now,
                                DoCmd = DoCmd,
                                MsgData = MsgData,
                                Receiver = receiver,
                                ReceiverIP = "",
                                ResponseId = 0,
                                Sender = "mTPS",
                                TargetId = TargetId
                            };

            try
            {
                lock (this._lockObj)
                {
                    this._outQueue.Send(tpsMessage);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
