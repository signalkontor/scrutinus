﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NetMQ;
using NetMQ.Sockets;

namespace zeroMqPublisher
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Publisher";

            using (var publisher = new PublisherSocket())
            {
                publisher.Bind("tcp://127.0.0.1:5556");

                /// publisher.SendMoreFrame("url")

                for (var i = 0; i < 100000; i++)
                {
                    var msg = $"url Message No. {i}";

                    publisher.SendMoreFrame("json").SendFrame(msg);
                    Console.WriteLine(msg);

                    // Thread.Sleep(500);
                }
            }
        }
    }
}
