﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetMQ;
using NetMQ.Sockets;

namespace zeroMqSubscriber
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Subscriber";

            using (var subscriber = new SubscriberSocket())
            {
                subscriber.Connect("tcp://127.0.0.1:5556");

                subscriber.Subscribe("json");

                while (true)
                {
                    var message = subscriber.ReceiveFrameString();
                    if (message == "json")
                    {
                        continue;
                    }

                    Console.WriteLine(message);
                }
            }
        }
    }
}
