﻿// // TPS.net TPS8 SkatingBurnTest
// // Program.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

namespace SkatingBurnTest
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var testRunner = new TestRunner();
            testRunner.RunTest();
        }
    }
}
