﻿// // TPS.net TPS8 SkatingBurnTest
// // TestRunner.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DataModel;
using DataModel.Models;
using Helpers.Skating;

namespace SkatingBurnTest
{
    internal class TestRunner
    {
        private readonly List<Couple> couples = new List<Couple>()
                                            {
                                                new Couple() { FirstMan = "A" },
                                                new Couple() { FirstMan = "B" },
                                                new Couple() { FirstMan = "C" },
                                                new Couple() { FirstMan = "D" },
                                                new Couple() { FirstMan = "E" },
                                                new Couple() { FirstMan = "F" },
                                            };

        private readonly List<Official> judges = new List<Official>()
                                            {
                                                new Official() {Sign = "A" },
                                                new Official() {Sign = "B" },
                                                new Official() {Sign = "C" },
                                                new Official() {Sign = "D" },
                                                new Official() {Sign = "E" }
                                            };

        public void RunTest()
        {
            if (File.Exists("TestRunner.sdf"))
            {
                File.Delete("TestRunner.sdf");
            }
            
            var context = new ScrutinusContext("TestRunner.sdf", "DataModel.InitialData.InitialContent.xml");

            var competition = new Competition()
                                  {
                                      Event = context.Events.First(),
                                      AgeGroup = context.AgeGroups.First(),
                                      Class = context.Classes.First(),
                                      CompetitionType = context.CompetitionTypes.First(),
                                  };

            context.Competitions.Add(competition);

            context.Couples.AddRange(this.couples);

            var round = new Round()
                            {
                                Competition = competition,
                                RoundType = context.RoundTypes.First(t => t.IsFinal),
                                MarksInputType = MarkingTypes.Skating
                            };

            foreach (var dance in context.Dances.Where(d => d.Id < 6))
            {
                round.DancesInRound.Add(new DanceInRound() {Dance = dance, Round = round, SortOrder = dance.DefaultOrder });
            }

            var participants = new List<Participant>()
                                   {
                                       new Participant() { Number = 1, Couple = this.couples[0], Competition = competition},
                                       new Participant() { Number = 2, Couple = this.couples[1], Competition = competition},
                                       new Participant() { Number = 3, Couple = this.couples[2], Competition = competition},
                                       new Participant() { Number = 4, Couple = this.couples[3], Competition = competition},
                                       new Participant() { Number = 5, Couple = this.couples[4], Competition = competition},
                                       new Participant() { Number = 6, Couple = this.couples[5], Competition = competition},
                                   };

            foreach (var participant in participants)
            {
                competition.Participants.Add(participant);
            }

            context.SaveChanges();

            foreach (var participant in participants)
            {
                round.Qualifieds.Add(new Qualified() { Participant = participant, Round = round });
            }

            context.Rounds.Add(round);
            context.SaveChanges();

            var count = 0;
            while (true)
            {
                count++;
                Console.WriteLine("Run " + count);

                this.CreateMarkings(context, round, participants);
                var skatingViewModel = new SkatingViewModel(
                    context,
                    round,
                    participants,
                    round.DancesInRound,
                    this.judges);

                try
                {
                    skatingViewModel.DoCalculations();
                    if (skatingViewModel.Participants.Any(p => !p.PlaceFrom.HasValue || p.PlaceFrom == 0))
                    {
                        this.DumpMarksToFile(count, skatingViewModel);
                    }
                }
                catch (Exception)
                {
                    this.DumpMarksToFile(count, skatingViewModel);
                }
                
            }
            
        }

        private void CreateMarkings(ScrutinusContext context, Round round, IEnumerable<Participant> participants)
        {
            var markings = context.Markings.ToList();
            context.Markings.RemoveRange(markings);
            var parts = participants.ToList();

            
            

            foreach (var official in this.judges)
            {
                foreach (var dance in context.Dances.Where(d => d.Id < 6))
                {
                    var marks = this.GetRandomMarking();
                    for(var index=0;index < 6;index++)
                    {
                        parts[index].PlaceFrom = null;
                        parts[index].PlaceTo = null;

                        var mark = new Marking(parts[index], round, dance, official)
                        {
                            Mark = marks[index],
                        };

                        context.Markings.Add(mark);
                    }
                }
            }
            
            context.SaveChanges();
        }

        private int[] GetRandomMarking()
        {
            var result = new int[6];

            var random = new Random();

            for (var i = 0; i < 6; i++)
            {
                var mark = random.Next(1, 7);
                while (result.Any(r => r == mark))
                {
                    mark = random.Next(1, 7);
                }
                result[i] = mark;
            }

            return result;
        }

        private void DumpMarksToFile(int number, SkatingViewModel model)
        {
            var stream = new StreamWriter("c:\\Temp\\Error" + number + ".csv");

            foreach (var result in model.DanceSkating)
            {
                stream.WriteLine(result.DanceInRound.Dance.DanceName);
                foreach (var coupleResultFinal in result.CoupleResults)
                {
                    stream.Write("{0}\t\t", coupleResultFinal.Participant.Number);
                    foreach (var marking in coupleResultFinal.Marks)
                    {
                        stream.Write("{0}\t", marking.Mark);
                    }
                    stream.WriteLine();
                }
            }
            stream.Close();
        }
    }
}
