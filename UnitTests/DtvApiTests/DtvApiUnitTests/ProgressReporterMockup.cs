﻿// // TPS.net TPS8 DtvApiUnitTests
// // ProgressReporterMockup.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Diagnostics;
using System.Threading.Tasks;
using General.Interfaces;
using MahApps.Metro.Controls.Dialogs;

namespace DtvApiUnitTests
{
    public class ProgressReporterMockup : IReportProgress
    {
        public MessageDialogResult ShowMessage(string message)
        {
            Debug.WriteLine(message);

            return MessageDialogResult.Affirmative;
        }

        public void ReportStatus(string message, int progress, int max)
        {
            Debug.WriteLine("{0} {1}/{2}", message, progress, max);
        }

        public void RemoveStatusReport()
        {
            
        }

        public Task<ProgressDialogController> OpenProgressBarAsync(string title, string message)
        {
            throw new NotImplementedException();
        }

        public ProgressDialogController OpenProgressBar(string title, string message)
        {
            throw new NotImplementedException();
        }
    }
}
