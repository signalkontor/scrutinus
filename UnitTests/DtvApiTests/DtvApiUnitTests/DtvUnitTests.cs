﻿// // TPS.net TPS8 DtvApiUnitTests
// // DtvUnitTests.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Linq;
using DataModel;
using DataModel.Models;
using DtvEsvModule;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DtvApiUnitTests
{
    [TestClass]
    public class DtvApiTests
    {
        private ApiClient apiClient;

        private ScrutinusContext context;

        [TestInitialize]
        public void InitTests()
        {
            this.apiClient = new ApiClient("http://ev-q1.tanzsport-portal.de", "API_TPSeJudge", "fkahf76$A");

            //if (File.Exists(@"C:\Users\Olav\Documents\TPS.net\Testdatenbank.sdf"))
            //{
            //    File.Delete(@"C:\Users\Olav\Documents\TPS.net\Testdatenbank.sdf");
            //}

            //this.context = new ScrutinusContext(@"C:\Users\Olav\Documents\TPS.net\Testdatenbank.sdf", "DataModel.InitialData.DTV.xml");
            //var test = this.context.Events.ToList();
        }

        [TestMethod]
        public void TestEvents()
        {
            var events = this.apiClient.GetEvents();

            Assert.IsNotNull(events);
            Assert.IsTrue(events.Any());
        }

        [TestMethod]
        public void TestSingleEvent()
        {
            var events = this.apiClient.GetEvents();

            var ev = this.apiClient.GetVeranstaltung(events.First().id);

            Assert.IsNotNull(ev);
        }

        [TestMethod]
        public void TestStartList()
        {
            var ev = this.apiClient.GetVeranstaltung(44640);

            Assert.IsNotNull(ev);
        }

        [TestMethod]
        public void TestFunktionaere()
        {
            var funktionaere = this.apiClient.GetAllFunktionaere();
            
            Assert.IsNotNull(funktionaere);
            Assert.IsTrue(funktionaere.Any());

            var funktionaer = this.apiClient.GetFunktionaer(funktionaere.First().id);

            Assert.IsNotNull(funktionaer);
        }

        [TestMethod]
        public void TestAufstiegstabelle()
        {
            var tabelle = this.apiClient.GetAufstiegstabelle();

            Assert.IsNotNull(tabelle);
            Assert.IsTrue(tabelle.Length == 1);
            Assert.IsTrue(tabelle[0].minPunkte == 2);
        }

        [TestMethod]
        public void ImportDtv()
        {
            var ev = this.apiClient.GetVeranstaltung(200008);
            var importer = new DtvApiImporter("http://ev-q2.tanzsport-portal.de", "API_TPSeJudge", "fkahf76$A", this.context);
            importer.ImportAsync(new ProgressReporterMockup(), ev, MarkingTypes.Marking);
        }

        [TestMethod]
        public void SendResultTest()
        {
            this.context = new ScrutinusContext(@"C:\Users\Olav\Documents\TPS.net\MitErgebnissen.sdf", "DataModel.InitialData.DTV.xml");

            var importer = new DtvApiImporter("http://ev-q2.tanzsport-portal.de", "API_TPSeJudge", "fkahf76$A", this.context);

            // Sen B I Standard - 
            var competition = this.context.Competitions.Single(c => c.ExternalId == "46475");

            var messages = importer.SaveResult(competition);

            Assert.IsTrue(messages.Length > 0);
            Assert.IsTrue(messages[0] == "Ergebnis gespeichert");
        }

        [TestMethod]
        public void LoadClimbUpTables()
        {
            var importer = new DtvApiImporter("http://ev-q1.tanzsport-portal.de", "API_TPSeJudge", "fkahf76$A", this.context);
            importer.SyncClimbUpTables(null);
            
            Assert.IsTrue(this.context.ClimbUpTables.Any());
        }

        [TestMethod]
        public void UploadJudgingSheets()
        {
            var competition = new Competition() { ExternalId = "52173" };
            competition.JudgingSheets.Add(new JudgingSheetData() { FileName = @"..\..\TestData\WRZettel.pdf" });
            var importer = new DtvApiImporter("http://ev-q1.tanzsport-portal.de", "API_TPSeJudge", "fkahf76$A", this.context);

            importer.SendJudgingSheets(competition);
        }

        [TestMethod]
        public void UploadDigitalJudgingSheets()
        {
            this.context = new ScrutinusContext(@"C:\Users\Olav\Documents\TPS.net\MitErgebnissen.sdf", "DataModel.InitialData.DTV.xml");

            var importer = new DtvApiImporter("http://ev-q1.tanzsport-portal.de", "API_TPSeJudge", "fkahf76$A", this.context);

            // Sen II C Standard - 
            var competition = this.context.Competitions.Single(c => c.ExternalId == "52177");

            importer.SendDigitalJudgingSheetsFromMobileControl(@"..\..\TestData", competition);
        }

        [TestMethod]
        public void SkatingTablesTest()
        {
            this.context = new ScrutinusContext(@"C:\Users\Olav\Documents\TPS.net\skating.sdf", "DataModel.InitialData.DTV.xml");

            var importer = new DtvApiImporter("http://ev-q1.tanzsport-portal.de", "API_TPSeJudge", "fkahf76$A", this.context);

            // Sen II C Standard - 
            var competition = this.context.Competitions.Single(c => c.ExternalId == "52318");

            importer.SaveResult(competition);
        }

        [TestMethod]
        public void UploadPdfTest()
        {
            var apiClient = new ApiClient("", "olav", "test");
            // apiClient.UploadFiles("http://pixw.net/wrzettelpdf/index.php", new List<string>() { @"C:\Users\Olav\Downloads\060316_WR_Zettel_Hgr_D_VOR.pdf" });
        }
    }
}
