﻿// // TPS.net TPS8 PrintingUnitTest
// // PrintingUnitTest.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Linq;
using DataModel.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Scrutinus;
using Scrutinus.Reports;

namespace PrintingUnitTest
{
    [TestClass]
    public class PrintingUnitTest
    {
        [TestMethod]
        public void PrintJudgingSheets()
        {
            ScrutinusContext.CurrentDataSource = @"C:\Users\groehnol\Documents\TPS.net\Einhornpokal 2015.sdf";

            var context = new ScrutinusContext();

            Settings.Default.PrintJudgingSheetsOnSinglePage = false;

            PrintHelper.PrintEmptyJudgingSheets(
                context.Competitions.Single(c => c.Id == 1),
                context.Rounds.Single(r => r.Id == 1),
                "PDFCreator",
                1,
                false
                );
        }
    }
}
