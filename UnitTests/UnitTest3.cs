﻿// // TPS.net TPS8 ViewModelTest
// // UnitTest3.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Linq;
using DataModel.Import;
using DataModel.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests
{
    [TestClass]
    public class ImportExcelUnitTest
    {
        [TestMethod]
        public void ImportExcel()
        {
            var importer = new ExcelImporter();

            var context = new ScrutinusContext(@"C:\Users\Olav\Documents\TPS.net\WDSFTest.sdf", "Dtv.xml");

            var result = importer.ImportFile(context, @"c:\temp\Data_King2015.xlsx", null);

            Assert.IsTrue(result.Any());
        }
    }
}
