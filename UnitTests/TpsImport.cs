﻿// // TPS.net TPS8 ViewModelTest
// // TpsImport.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Threading;
using DataModel.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Scrutinus.Dialogs.DialogViewModels;

namespace UnitTests
{
    /// <summary>
    /// Summary description for TpsImport
    /// </summary>
    [TestClass]
    public class TpsImport
    {
        private TestContext testContextInstance;

        public TpsImport()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return this.testContextInstance;
            }
            set
            {
                this.testContextInstance = value;
            }
        }

        [TestMethod]
        public void ImportTpsTest()
        {
            var context = new ScrutinusContext(@"C:\Users\groehnol\Documents\TPS.net\Alex.sdf", null);
            var viewModel = new ImportLegacyTpsViewModel();
            viewModel.TpsCompetitionPath = @"C:\Users\groehnol\Desktop\CATEGORIES_FEBD";
            viewModel.TpsInfPath = @"C:\TPS7\DatFiles";
            var task = viewModel.ImportLegacyTpsAsync();

            while (!task.IsCompleted)
            {
                Thread.Sleep(1000);
            }
        }

        #region Additional test attributes

        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //

        #endregion
    }
}
