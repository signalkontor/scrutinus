﻿// // TPS.net TPS8 SkatingUnitTest
// // NewSkatingUnitTest.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using DataModel.Models;
using Helpers.Skating;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SkatingUnitTest
{
    [TestClass]
    public class NewSkatingUnitTest
    {
        private ScrutinusContext context = null;

        private List<DanceInRound> dances;

        private List<Official> judges;
        private List<Participant> participants;

        private Round round;

        [TestInitialize]
        public void SetupData()
        {
            this.participants = new List<Participant>()
                                    {
                                        new Participant() {Number = 1, Id = 1},
                                        new Participant() {Number = 2, Id = 2},
                                        new Participant() {Number = 3, Id = 3},
                                        new Participant() {Number = 4, Id = 4},
                                        new Participant() {Number = 5, Id = 5},
                                        new Participant() {Number = 6, Id = 6},
                                    };

            this.judges = new List<Official>()
                              {
                                  new Official() { Sign = "A"},
                                  new Official() { Sign = "B"},
                                  new Official() { Sign = "C"},
                                  new Official() { Sign = "D"},
                                  new Official() { Sign = "E"},
                              };

            this.dances = new List<DanceInRound>()
                              {
                                  new DanceInRound() { Dance = new Dance() {DanceName = "SW "}},
                                  new DanceInRound() { Dance = new Dance() {DanceName = "TG "}},
                                  new DanceInRound() { Dance = new Dance() {DanceName = "VW "}},
                                  new DanceInRound() { Dance = new Dance() {DanceName = "SF "}},
                                  new DanceInRound() { Dance = new Dance() {DanceName = "QS "}},
                              };

            this.context = null;

            this.round = new Round();
        }

        [TestMethod]
        public void SkatingRule8_Test1()
        {

            var Skating = new DanceSkatingViewModel(null, this.round, this.participants, this.dances.First(), this.judges);

            Skating.CoupleResults[0].Marks = this.CreateFromDoubleList(new List<double>() { 1,1,3,2,3 });
            Skating.CoupleResults[1].Marks = this.CreateFromDoubleList(new List<double>() { 6,5,4,1,1 });
            Skating.CoupleResults[2].Marks = this.CreateFromDoubleList(new List<double>() { 2,4,1,5,5 });
            Skating.CoupleResults[3].Marks = this.CreateFromDoubleList(new List<double>() { 4,2,5,6,2 });
            Skating.CoupleResults[4].Marks = this.CreateFromDoubleList(new List<double>() { 5,6,2,3,4 });
            Skating.CoupleResults[5].Marks = this.CreateFromDoubleList(new List<double>() { 3,3,6,4,6 });

            Skating.DoCalculations();

            Assert.IsTrue(Skating.CoupleResults[0].Place == 1);
            Assert.IsTrue(Skating.CoupleResults[1].Place == 2);
            Assert.IsTrue(Skating.CoupleResults[2].Place == 3);
            Assert.IsTrue(Skating.CoupleResults[3].Place == 4);
            Assert.IsTrue(Skating.CoupleResults[4].Place == 5);
            Assert.IsTrue(Skating.CoupleResults[5].Place == 6);
        }

        [TestMethod]
        public void SkatingRule9_Test1()
        {
            var Skating = new DanceSkatingViewModel(this.context, this.round, this.participants, this.dances.First(), this.judges);

            Skating.CoupleResults[0].Marks = this.CreateFromDoubleList(new List<double>() { 1, 1, 2, 1, 2 });
            Skating.CoupleResults[1].Marks = this.CreateFromDoubleList(new List<double>() { 2, 2, 1, 2, 1 });
            Skating.CoupleResults[2].Marks = this.CreateFromDoubleList(new List<double>() { 3, 3, 3, 3, 3 });
            Skating.CoupleResults[3].Marks = this.CreateFromDoubleList(new List<double>() { 4, 4, 5, 5, 6 });
            Skating.CoupleResults[4].Marks = this.CreateFromDoubleList(new List<double>() { 5, 5, 6, 4, 4 });
            Skating.CoupleResults[5].Marks = this.CreateFromDoubleList(new List<double>() { 6, 6, 4, 6, 5 });

            Skating.DoCalculations();

            Assert.IsTrue(Skating.CoupleResults[0].Place == 1);
            Assert.IsTrue(Skating.CoupleResults[1].Place == 2);
            Assert.IsTrue(Skating.CoupleResults[3].Place == 4.5);
            Assert.IsTrue(Skating.CoupleResults[5].Place == 6);
        }

        [TestMethod]
        public void SkatingRule9_Test2()
        {
            var Skating = new DanceSkatingViewModel(this.context, this.round, this.participants, this.dances.First(), this.judges);

            Skating.CoupleResults[0].Marks = this.CreateFromDoubleList(new List<double>() { 2,1,5,1,1 });
            Skating.CoupleResults[1].Marks = this.CreateFromDoubleList(new List<double>() { 1,2,2,5,5 });
            Skating.CoupleResults[2].Marks = this.CreateFromDoubleList(new List<double>() { 5,6,1,2,2 });
            Skating.CoupleResults[3].Marks = this.CreateFromDoubleList(new List<double>() { 3,3,3,3,6 });
            Skating.CoupleResults[4].Marks = this.CreateFromDoubleList(new List<double>() { 4,4,4,6,4 });
            Skating.CoupleResults[5].Marks = this.CreateFromDoubleList(new List<double>() { 6,5,6,4,3 });

            Skating.DoCalculations();

            Assert.IsTrue(Skating.CoupleResults[0].Place == 1);
            Assert.IsTrue(Skating.CoupleResults[1].Place == 2);
            Assert.IsTrue(Skating.CoupleResults[2].Place == 3);
            Assert.IsTrue(Skating.CoupleResults[3].Place == 4);
            Assert.IsTrue(Skating.CoupleResults[4].Place == 5);
            Assert.IsTrue(Skating.CoupleResults[5].Place == 6);
        }

        [TestMethod]
        public void Rule10_Test()
        {
            var skatingViewModel = new SkatingViewModel(this.context, this.round, this.participants, this.dances, this.judges);

            // Dance 1
            skatingViewModel.DanceSkating[0].CoupleResults[0].Marks = this.CreateFromDoubleList(new List<double>() { 2, 2, 2, 2, 2 });
            skatingViewModel.DanceSkating[0].CoupleResults[1].Marks = this.CreateFromDoubleList(new List<double>() { 1, 1, 1, 1, 1});
            skatingViewModel.DanceSkating[0].CoupleResults[2].Marks = this.CreateFromDoubleList(new List<double>() { 6, 6, 6, 6, 6 });
            skatingViewModel.DanceSkating[0].CoupleResults[3].Marks = this.CreateFromDoubleList(new List<double>() { 4, 4, 4, 4, 4 });
            skatingViewModel.DanceSkating[0].CoupleResults[4].Marks = this.CreateFromDoubleList(new List<double>() { 5, 5, 5, 5, 5 });
            skatingViewModel.DanceSkating[0].CoupleResults[5].Marks = this.CreateFromDoubleList(new List<double>() { 3, 3, 3, 3, 3 });
            // Dance 2
            skatingViewModel.DanceSkating[1].CoupleResults[0].Marks = this.CreateFromDoubleList(new List<double>() { 1,1,1,1,1 });
            skatingViewModel.DanceSkating[1].CoupleResults[1].Marks = this.CreateFromDoubleList(new List<double>() { 2,2,2,2,2 });
            skatingViewModel.DanceSkating[1].CoupleResults[2].Marks = this.CreateFromDoubleList(new List<double>() { 6,6,6,6,6 });
            skatingViewModel.DanceSkating[1].CoupleResults[3].Marks = this.CreateFromDoubleList(new List<double>() { 4,4,4,4,4 });
            skatingViewModel.DanceSkating[1].CoupleResults[4].Marks = this.CreateFromDoubleList(new List<double>() { 3,3,3,3,3 });
            skatingViewModel.DanceSkating[1].CoupleResults[5].Marks = this.CreateFromDoubleList(new List<double>() { 5,5,5,5,5 });
            // Dance 3
            skatingViewModel.DanceSkating[2].CoupleResults[0].Marks = this.CreateFromDoubleList(new List<double>() { 5,5,5,5,5 });
            skatingViewModel.DanceSkating[2].CoupleResults[1].Marks = this.CreateFromDoubleList(new List<double>() { 2,2,2,2,2 });
            skatingViewModel.DanceSkating[2].CoupleResults[2].Marks = this.CreateFromDoubleList(new List<double>() { 1,1,1,1,1 });
            skatingViewModel.DanceSkating[2].CoupleResults[3].Marks = this.CreateFromDoubleList(new List<double>() { 4,4,4,4,4 });
            skatingViewModel.DanceSkating[2].CoupleResults[4].Marks = this.CreateFromDoubleList(new List<double>() { 3,3,3,3,3 });
            skatingViewModel.DanceSkating[2].CoupleResults[5].Marks = this.CreateFromDoubleList(new List<double>() { 6,6,6,6,6 });
            // Dance 4
            skatingViewModel.DanceSkating[3].CoupleResults[0].Marks = this.CreateFromDoubleList(new List<double>() { 3,3,3,3,3});
            skatingViewModel.DanceSkating[3].CoupleResults[1].Marks = this.CreateFromDoubleList(new List<double>() { 6,6,6,6,6 });
            skatingViewModel.DanceSkating[3].CoupleResults[2].Marks = this.CreateFromDoubleList(new List<double>() { 1,1,1,1,1 });
            skatingViewModel.DanceSkating[3].CoupleResults[3].Marks = this.CreateFromDoubleList(new List<double>() { 4,4,4,4,4 });
            skatingViewModel.DanceSkating[3].CoupleResults[4].Marks = this.CreateFromDoubleList(new List<double>() { 2,2,2,2,2 });
            skatingViewModel.DanceSkating[3].CoupleResults[5].Marks = this.CreateFromDoubleList(new List<double>() { 5,5,5,5,5 });
            // Dance 5
            skatingViewModel.DanceSkating[4].CoupleResults[0].Marks = this.CreateFromDoubleList(new List<double>() { 2,2,2,2,2 });
            skatingViewModel.DanceSkating[4].CoupleResults[1].Marks = this.CreateFromDoubleList(new List<double>() { 6,6,6,6,6 });
            skatingViewModel.DanceSkating[4].CoupleResults[2].Marks = this.CreateFromDoubleList(new List<double>() { 3,3,3,3,3 });
            skatingViewModel.DanceSkating[4].CoupleResults[3].Marks = this.CreateFromDoubleList(new List<double>() { 1,1,1,1,1 });
            skatingViewModel.DanceSkating[4].CoupleResults[4].Marks = this.CreateFromDoubleList(new List<double>() { 4,4,4,4,4 });
            skatingViewModel.DanceSkating[4].CoupleResults[5].Marks = this.CreateFromDoubleList(new List<double>() { 5,5,5,5,5 });

            skatingViewModel.DoCalculations();

            this.DumpRule10(skatingViewModel.Rule10ViewModel);

            Assert.IsTrue(skatingViewModel.Participants[0].PlaceFrom == 1);
            Assert.IsTrue(skatingViewModel.Participants[1].PlaceFrom == 2);
            Assert.IsTrue(skatingViewModel.Participants[2].PlaceFrom == 3);
            Assert.IsTrue(skatingViewModel.Participants[3].PlaceFrom == 4);
            Assert.IsTrue(skatingViewModel.Participants[4].PlaceFrom == 5);
            Assert.IsTrue(skatingViewModel.Participants[5].PlaceFrom == 6);
        }

        [TestMethod]
        public void SkatingRule11DevidedPlace()
        {
            // This test has only 4 dances
            this.dances.RemoveAt(4);

            var skatingViewModel = new SkatingViewModel(this.context, this.round, this.participants, this.dances, this.judges);

            // Dance 1
            skatingViewModel.DanceSkating[0].CoupleResults[0].Marks = this.CreateFromDoubleList(new List<double>() { 1, 1, 2, 1, 2 });
            skatingViewModel.DanceSkating[0].CoupleResults[1].Marks = this.CreateFromDoubleList(new List<double>() { 2, 2, 1, 2, 1 });
            skatingViewModel.DanceSkating[0].CoupleResults[2].Marks = this.CreateFromDoubleList(new List<double>() { 3, 3, 3, 3, 3 });
            skatingViewModel.DanceSkating[0].CoupleResults[3].Marks = this.CreateFromDoubleList(new List<double>() { 4, 4, 5, 5, 6 });
            skatingViewModel.DanceSkating[0].CoupleResults[4].Marks = this.CreateFromDoubleList(new List<double>() { 5, 5, 6, 4, 4 });
            skatingViewModel.DanceSkating[0].CoupleResults[5].Marks = this.CreateFromDoubleList(new List<double>() { 6, 6, 4, 6, 5 });
            // Dance 2
            skatingViewModel.DanceSkating[1].CoupleResults[0].Marks = this.CreateFromDoubleList(new List<double>() { 1, 1, 2, 1, 2 });
            skatingViewModel.DanceSkating[1].CoupleResults[1].Marks = this.CreateFromDoubleList(new List<double>() { 2, 2, 1, 2, 1 });
            skatingViewModel.DanceSkating[1].CoupleResults[2].Marks = this.CreateFromDoubleList(new List<double>() { 3, 3, 3, 3, 3 });
            skatingViewModel.DanceSkating[1].CoupleResults[3].Marks = this.CreateFromDoubleList(new List<double>() { 4, 4, 5, 5, 6 });
            skatingViewModel.DanceSkating[1].CoupleResults[4].Marks = this.CreateFromDoubleList(new List<double>() { 5, 5, 6, 4, 4 });
            skatingViewModel.DanceSkating[1].CoupleResults[5].Marks = this.CreateFromDoubleList(new List<double>() { 6, 6, 4, 6, 5 });
            // Dance 3
            skatingViewModel.DanceSkating[2].CoupleResults[0].Marks = this.CreateFromDoubleList(new List<double>() { 1, 2, 2, 1, 2 });
            skatingViewModel.DanceSkating[2].CoupleResults[1].Marks = this.CreateFromDoubleList(new List<double>() { 2, 1, 1, 2, 1 });
            skatingViewModel.DanceSkating[2].CoupleResults[2].Marks = this.CreateFromDoubleList(new List<double>() { 3, 3, 3, 3, 3 });
            skatingViewModel.DanceSkating[2].CoupleResults[3].Marks = this.CreateFromDoubleList(new List<double>() { 4, 4, 5, 5, 6 });
            skatingViewModel.DanceSkating[2].CoupleResults[4].Marks = this.CreateFromDoubleList(new List<double>() { 5, 5, 6, 4, 4 });
            skatingViewModel.DanceSkating[2].CoupleResults[5].Marks = this.CreateFromDoubleList(new List<double>() { 6, 6, 4, 6, 5 });
            // Dance 3
            skatingViewModel.DanceSkating[3].CoupleResults[0].Marks = this.CreateFromDoubleList(new List<double>() { 1, 2, 2, 1, 2 });
            skatingViewModel.DanceSkating[3].CoupleResults[1].Marks = this.CreateFromDoubleList(new List<double>() { 2, 1, 1, 2, 1 });
            skatingViewModel.DanceSkating[3].CoupleResults[2].Marks = this.CreateFromDoubleList(new List<double>() { 3, 3, 3, 3, 3 });
            skatingViewModel.DanceSkating[3].CoupleResults[3].Marks = this.CreateFromDoubleList(new List<double>() { 4, 4, 5, 5, 6 });
            skatingViewModel.DanceSkating[3].CoupleResults[4].Marks = this.CreateFromDoubleList(new List<double>() { 5, 5, 6, 4, 4 });
            skatingViewModel.DanceSkating[3].CoupleResults[5].Marks = this.CreateFromDoubleList(new List<double>() { 6, 6, 4, 6, 5 });

            skatingViewModel.DoCalculations();

            this.DumppRule9(skatingViewModel.Rule9ViewModels);
            this.DumpRule10(skatingViewModel.Rule10ViewModel);
            this.DumpRule11(skatingViewModel.Rule11ViewModel);

            Assert.IsTrue(skatingViewModel.Participants[0].PlaceFrom == 1);
            Assert.IsTrue(skatingViewModel.Participants[1].PlaceFrom == 1);
            Assert.IsTrue(skatingViewModel.Participants[0].PlaceTo == 2);
            Assert.IsTrue(skatingViewModel.Participants[1].PlaceTo == 2);
        }


        [TestMethod]
        public void SkatingRule11BerlinTestCouples()
        {
            // This test has only 4 dances
            this.dances.RemoveAt(4);
            this.participants.RemoveAt(5);
            this.participants.RemoveAt(4);

            var skatingViewModel = new SkatingViewModel(this.context, this.round, this.participants, this.dances, this.judges);

            // Dance 1
            skatingViewModel.DanceSkating[0].CoupleResults[0].Marks = this.CreateFromDoubleList(new List<double>() { 1,2,1,1,2 });
            skatingViewModel.DanceSkating[0].CoupleResults[1].Marks = this.CreateFromDoubleList(new List<double>() { 3,1,4,2,1 });
            skatingViewModel.DanceSkating[0].CoupleResults[2].Marks = this.CreateFromDoubleList(new List<double>() { 2,4,2,4,3 });
            skatingViewModel.DanceSkating[0].CoupleResults[3].Marks = this.CreateFromDoubleList(new List<double>() { 4,3,3,3,4 });
            // Dance 2
            skatingViewModel.DanceSkating[1].CoupleResults[0].Marks = this.CreateFromDoubleList(new List<double>() { 1,2,1,2,2 });
            skatingViewModel.DanceSkating[1].CoupleResults[1].Marks = this.CreateFromDoubleList(new List<double>() { 2,1,2,1,1 });
            skatingViewModel.DanceSkating[1].CoupleResults[2].Marks = this.CreateFromDoubleList(new List<double>() { 3,3,3,4,3 });
            skatingViewModel.DanceSkating[1].CoupleResults[3].Marks = this.CreateFromDoubleList(new List<double>() { 4,4,4,3,4 });
            // Dance 3
            skatingViewModel.DanceSkating[2].CoupleResults[0].Marks = this.CreateFromDoubleList(new List<double>() { 1,1,1,2,2 });
            skatingViewModel.DanceSkating[2].CoupleResults[1].Marks = this.CreateFromDoubleList(new List<double>() { 2,2,2,1,1 });
            skatingViewModel.DanceSkating[2].CoupleResults[2].Marks = this.CreateFromDoubleList(new List<double>() { 3,3,3,3,3 });
            skatingViewModel.DanceSkating[2].CoupleResults[3].Marks = this.CreateFromDoubleList(new List<double>() { 4,4,4,4,4 });
            // Dance 4
            skatingViewModel.DanceSkating[3].CoupleResults[0].Marks = this.CreateFromDoubleList(new List<double>() { 1,1,2,2,2 });
            skatingViewModel.DanceSkating[3].CoupleResults[1].Marks = this.CreateFromDoubleList(new List<double>() { 2,2,1,1,1 });
            skatingViewModel.DanceSkating[3].CoupleResults[2].Marks = this.CreateFromDoubleList(new List<double>() { 3,3,3,4,3 });
            skatingViewModel.DanceSkating[3].CoupleResults[3].Marks = this.CreateFromDoubleList(new List<double>() { 4,4,4,3,4 });

            skatingViewModel.DoCalculations();

            this.DumppRule9(skatingViewModel.Rule9ViewModels);
            this.DumpRule10(skatingViewModel.Rule10ViewModel);
            this.DumpRule11(skatingViewModel.Rule11ViewModel);


            Assert.IsTrue(skatingViewModel.Participants[0].PlaceFrom == 1);
            Assert.IsTrue(skatingViewModel.Participants[1].PlaceFrom == 2);
            Assert.IsTrue(skatingViewModel.Participants[2].PlaceFrom == 3);
            Assert.IsTrue(skatingViewModel.Participants[3].PlaceFrom == 4);
        }

        [TestMethod]
        public void SkatingRule11BerlinTestKinder()
        {
            // This test has only 3 dances
            this.dances.RemoveAt(4);
            this.dances.RemoveAt(3);
            this.participants.RemoveAt(5);

            var skatingViewModel = new SkatingViewModel(this.context, this.round, this.participants, this.dances, this.judges);

            // Dance 1
            skatingViewModel.DanceSkating[0].CoupleResults[0].Marks = this.CreateFromDoubleList(new List<double>() { 1,2,4,1,2 });
            skatingViewModel.DanceSkating[0].CoupleResults[1].Marks = this.CreateFromDoubleList(new List<double>() { 2,1,1,4,5 });
            skatingViewModel.DanceSkating[0].CoupleResults[2].Marks = this.CreateFromDoubleList(new List<double>() { 5,3,3,2,4});
            skatingViewModel.DanceSkating[0].CoupleResults[3].Marks = this.CreateFromDoubleList(new List<double>() { 4,4,5,3,3 });
            skatingViewModel.DanceSkating[0].CoupleResults[4].Marks = this.CreateFromDoubleList(new List<double>() { 3,5,2,5,1 });
            // Dance 2
            skatingViewModel.DanceSkating[1].CoupleResults[0].Marks = this.CreateFromDoubleList(new List<double>() { 3,4,4,1,3 });
            skatingViewModel.DanceSkating[1].CoupleResults[1].Marks = this.CreateFromDoubleList(new List<double>() { 4,1,2,3,5 });
            skatingViewModel.DanceSkating[1].CoupleResults[2].Marks = this.CreateFromDoubleList(new List<double>() { 2,3,5,2,1 });
            skatingViewModel.DanceSkating[1].CoupleResults[3].Marks = this.CreateFromDoubleList(new List<double>() { 5,5,3,4,4 });
            skatingViewModel.DanceSkating[1].CoupleResults[4].Marks = this.CreateFromDoubleList(new List<double>() { 1,2,1,5,2 });
            // Dance 3
            skatingViewModel.DanceSkating[2].CoupleResults[0].Marks = this.CreateFromDoubleList(new List<double>() { 3,4,3,1,3 });
            skatingViewModel.DanceSkating[2].CoupleResults[1].Marks = this.CreateFromDoubleList(new List<double>() { 4,2,1,4,5 });
            skatingViewModel.DanceSkating[2].CoupleResults[2].Marks = this.CreateFromDoubleList(new List<double>() { 2,1,4,2,1 });
            skatingViewModel.DanceSkating[2].CoupleResults[3].Marks = this.CreateFromDoubleList(new List<double>() { 1,5,5,3,4 });
            skatingViewModel.DanceSkating[2].CoupleResults[4].Marks = this.CreateFromDoubleList(new List<double>() { 5,3,2,5,2 });

            skatingViewModel.DoCalculations();

            this.DumppRule9(skatingViewModel.Rule9ViewModels);
            this.DumpRule10(skatingViewModel.Rule10ViewModel);
            this.DumpRule11(skatingViewModel.Rule11ViewModel);


            Assert.IsTrue(skatingViewModel.Participants[0].PlaceFrom == 2);
            Assert.IsTrue(skatingViewModel.Participants[1].PlaceFrom == 4);
            Assert.IsTrue(skatingViewModel.Participants[2].PlaceFrom == 1);
            Assert.IsTrue(skatingViewModel.Participants[3].PlaceFrom == 5);
            Assert.IsTrue(skatingViewModel.Participants[4].PlaceFrom == 3);
        }

        [TestMethod]
        public void SkatingRule11SeperatedPlaces()
        {
            // This test has only 4 dances
            this.dances.RemoveAt(4);

            var skatingViewModel = new SkatingViewModel(this.context, this.round, this.participants, this.dances, this.judges);

            // Dance 1
            skatingViewModel.DanceSkating[0].CoupleResults[0].Marks = this.CreateFromDoubleList(new List<double>() { 1, 1, 2, 1, 3 });
            skatingViewModel.DanceSkating[0].CoupleResults[1].Marks = this.CreateFromDoubleList(new List<double>() { 2, 2, 1, 2, 1 });
            skatingViewModel.DanceSkating[0].CoupleResults[2].Marks = this.CreateFromDoubleList(new List<double>() { 3, 3, 3, 3, 2 });
            skatingViewModel.DanceSkating[0].CoupleResults[3].Marks = this.CreateFromDoubleList(new List<double>() { 4, 4, 5, 5, 6 });
            skatingViewModel.DanceSkating[0].CoupleResults[4].Marks = this.CreateFromDoubleList(new List<double>() { 5, 5, 6, 4, 4 });
            skatingViewModel.DanceSkating[0].CoupleResults[5].Marks = this.CreateFromDoubleList(new List<double>() { 6, 6, 4, 6, 5 });
            // Dance 2
            skatingViewModel.DanceSkating[1].CoupleResults[0].Marks = this.CreateFromDoubleList(new List<double>() { 1, 1, 2, 1, 2 });
            skatingViewModel.DanceSkating[1].CoupleResults[1].Marks = this.CreateFromDoubleList(new List<double>() { 2, 2, 1, 2, 1 });
            skatingViewModel.DanceSkating[1].CoupleResults[2].Marks = this.CreateFromDoubleList(new List<double>() { 3, 3, 3, 3, 3 });
            skatingViewModel.DanceSkating[1].CoupleResults[3].Marks = this.CreateFromDoubleList(new List<double>() { 4, 4, 5, 5, 6 });
            skatingViewModel.DanceSkating[1].CoupleResults[4].Marks = this.CreateFromDoubleList(new List<double>() { 5, 5, 6, 4, 4 });
            skatingViewModel.DanceSkating[1].CoupleResults[5].Marks = this.CreateFromDoubleList(new List<double>() { 6, 6, 4, 6, 5 });
            // Dance 3
            skatingViewModel.DanceSkating[2].CoupleResults[0].Marks = this.CreateFromDoubleList(new List<double>() { 1, 2, 2, 1, 2 });
            skatingViewModel.DanceSkating[2].CoupleResults[1].Marks = this.CreateFromDoubleList(new List<double>() { 2, 1, 1, 2, 1 });
            skatingViewModel.DanceSkating[2].CoupleResults[2].Marks = this.CreateFromDoubleList(new List<double>() { 3, 3, 3, 3, 3 });
            skatingViewModel.DanceSkating[2].CoupleResults[3].Marks = this.CreateFromDoubleList(new List<double>() { 4, 4, 5, 5, 6 });
            skatingViewModel.DanceSkating[2].CoupleResults[4].Marks = this.CreateFromDoubleList(new List<double>() { 5, 5, 6, 4, 4 });
            skatingViewModel.DanceSkating[2].CoupleResults[5].Marks = this.CreateFromDoubleList(new List<double>() { 6, 6, 4, 6, 5 });
            // Dance 4
            skatingViewModel.DanceSkating[3].CoupleResults[0].Marks = this.CreateFromDoubleList(new List<double>() { 1, 2, 2, 1, 2 });
            skatingViewModel.DanceSkating[3].CoupleResults[1].Marks = this.CreateFromDoubleList(new List<double>() { 2, 1, 1, 2, 1 });
            skatingViewModel.DanceSkating[3].CoupleResults[2].Marks = this.CreateFromDoubleList(new List<double>() { 3, 3, 3, 3, 3 });
            skatingViewModel.DanceSkating[3].CoupleResults[3].Marks = this.CreateFromDoubleList(new List<double>() { 4, 4, 5, 5, 6 });
            skatingViewModel.DanceSkating[3].CoupleResults[4].Marks = this.CreateFromDoubleList(new List<double>() { 5, 5, 6, 4, 4 });
            skatingViewModel.DanceSkating[3].CoupleResults[5].Marks = this.CreateFromDoubleList(new List<double>() { 6, 6, 4, 6, 5 });

            skatingViewModel.DoCalculations();

            Assert.IsTrue(skatingViewModel.Participants[0].PlaceFrom == 2);
            Assert.IsTrue(skatingViewModel.Participants[1].PlaceFrom == 1);
            Assert.IsTrue(skatingViewModel.Participants[0].PlaceTo == 2);
            Assert.IsTrue(skatingViewModel.Participants[1].PlaceTo == 1);
            Assert.IsTrue(skatingViewModel.Participants[2].PlaceFrom == 3);
            Assert.IsTrue(skatingViewModel.Participants[3].PlaceFrom == 4);
            Assert.IsTrue(skatingViewModel.Participants[3].PlaceTo == 5);
            Assert.IsTrue(skatingViewModel.Participants[4].PlaceFrom == 4);
            Assert.IsTrue(skatingViewModel.Participants[4].PlaceTo == 5);
            Assert.IsTrue(skatingViewModel.Participants[5].PlaceTo == 6);
        }

        [TestMethod]
        public void SkatingRule11_Veiko1()
        {
            // This test has only 3 dances
            this.dances.RemoveAt(4);
            this.dances.RemoveAt(3);

            this.judges.Add(new Official() { Sign = "F" });
            this.judges.Add(new Official() { Sign = "G" });
            this.judges.Add(new Official() { Sign = "H" });
            this.judges.Add(new Official() { Sign = "I" });

            var skatingViewModel = new SkatingViewModel(this.context, this.round, this.participants, this.dances, this.judges);

            // Dance 1
            skatingViewModel.DanceSkating[0].CoupleResults[0].Marks = this.CreateFromDoubleList(new List<double>() { 6, 5, 6, 6, 1, 5, 3, 6, 5 });
            skatingViewModel.DanceSkating[0].CoupleResults[1].Marks = this.CreateFromDoubleList(new List<double>() { 1,6,5,5,6,6,5,2,3 });
            skatingViewModel.DanceSkating[0].CoupleResults[2].Marks = this.CreateFromDoubleList(new List<double>() { 2,2,2,3,4,3,2,1,4 });
            skatingViewModel.DanceSkating[0].CoupleResults[3].Marks = this.CreateFromDoubleList(new List<double>() { 3,4,3,4,5,4,4,4,6 });
            skatingViewModel.DanceSkating[0].CoupleResults[4].Marks = this.CreateFromDoubleList(new List<double>() { 4,3,4,2,2,2,6,3,2});
            skatingViewModel.DanceSkating[0].CoupleResults[5].Marks = this.CreateFromDoubleList(new List<double>() { 5,1,1,1,3,1,1,5,1 });
            // Dance 2
            skatingViewModel.DanceSkating[1].CoupleResults[0].Marks = this.CreateFromDoubleList(new List<double>() { 5,6,6,4,1,5,3,4,5 });
            skatingViewModel.DanceSkating[1].CoupleResults[1].Marks = this.CreateFromDoubleList(new List<double>() { 3,5,5,6,6,6,6,3,3 });
            skatingViewModel.DanceSkating[1].CoupleResults[2].Marks = this.CreateFromDoubleList(new List<double>() { 1,2,2,1,3,2,1,1,6 });
            skatingViewModel.DanceSkating[1].CoupleResults[3].Marks = this.CreateFromDoubleList(new List<double>() { 4,4,4,3,4,3,4,2,2 });
            skatingViewModel.DanceSkating[1].CoupleResults[4].Marks = this.CreateFromDoubleList(new List<double>() { 6,3,3,5,5,4,5,6,4 });
            skatingViewModel.DanceSkating[1].CoupleResults[5].Marks = this.CreateFromDoubleList(new List<double>() { 2,1,1,2,2,1,2,5,1 });
            // Dance 3
            skatingViewModel.DanceSkating[2].CoupleResults[0].Marks = this.CreateFromDoubleList(new List<double>() { 4,5,5,4,1,5,3,5,4 });
            skatingViewModel.DanceSkating[2].CoupleResults[1].Marks = this.CreateFromDoubleList(new List<double>() { 2,4,4,6,6,6,5,2,3 });
            skatingViewModel.DanceSkating[2].CoupleResults[2].Marks = this.CreateFromDoubleList(new List<double>() { 1,3,3,1,3,2,1,1,5 });
            skatingViewModel.DanceSkating[2].CoupleResults[3].Marks = this.CreateFromDoubleList(new List<double>() { 5,6,6,5,4,4,4,3,2 });
            skatingViewModel.DanceSkating[2].CoupleResults[4].Marks = this.CreateFromDoubleList(new List<double>() { 6,2,2,3,5,3,6,6,6 });
            skatingViewModel.DanceSkating[2].CoupleResults[5].Marks = this.CreateFromDoubleList(new List<double>() { 3,1,1,2,2,1,2,4,1 });


            skatingViewModel.DoCalculations();

            this.DumppRule9(skatingViewModel.Rule9ViewModels);
            this.DumpRule10(skatingViewModel.Rule10ViewModel);
            this.DumpRule11(skatingViewModel.Rule11ViewModel);

            Assert.IsTrue(skatingViewModel.Participants[0].PlaceFrom == 4);
            Assert.IsTrue(skatingViewModel.Participants[1].PlaceFrom == 6);
            Assert.IsTrue(skatingViewModel.Participants[2].PlaceFrom == 2);
            Assert.IsTrue(skatingViewModel.Participants[3].PlaceFrom == 3);
            Assert.IsTrue(skatingViewModel.Participants[4].PlaceFrom == 5);
            Assert.IsTrue(skatingViewModel.Participants[5].PlaceFrom == 1);

        }

        [TestMethod]
        public void SkatingRule11Test2()
        {
            var skatingViewModel = new SkatingViewModel(this.context, this.round, this.participants, this.dances, this.judges);

            // Dance 1
            skatingViewModel.DanceSkating[0].CoupleResults[0].Marks = this.CreateFromDoubleList(new List<double>() { 1,1,2,5,3 });
            skatingViewModel.DanceSkating[0].CoupleResults[1].Marks = this.CreateFromDoubleList(new List<double>() { 2,2,5,2,5 });
            skatingViewModel.DanceSkating[0].CoupleResults[2].Marks = this.CreateFromDoubleList(new List<double>() { 4,5,3,1,1 });
            skatingViewModel.DanceSkating[0].CoupleResults[3].Marks = this.CreateFromDoubleList(new List<double>() { 3,3,6,3,6 });
            skatingViewModel.DanceSkating[0].CoupleResults[4].Marks = this.CreateFromDoubleList(new List<double>() { 6,6,1,4,4});
            skatingViewModel.DanceSkating[0].CoupleResults[5].Marks = this.CreateFromDoubleList(new List<double>() { 5,4,4,6,2});
            // Dance 2
            skatingViewModel.DanceSkating[1].CoupleResults[0].Marks = this.CreateFromDoubleList(new List<double>() { 1,2,2,3,4 });
            skatingViewModel.DanceSkating[1].CoupleResults[1].Marks = this.CreateFromDoubleList(new List<double>() { 5,3,1,2,2 });
            skatingViewModel.DanceSkating[1].CoupleResults[2].Marks = this.CreateFromDoubleList(new List<double>() { 3,1,3,1,3 });
            skatingViewModel.DanceSkating[1].CoupleResults[3].Marks = this.CreateFromDoubleList(new List<double>() { 4,4,4,4,1 });
            skatingViewModel.DanceSkating[1].CoupleResults[4].Marks = this.CreateFromDoubleList(new List<double>() { 2,5,5,5,5 });
            skatingViewModel.DanceSkating[1].CoupleResults[5].Marks = this.CreateFromDoubleList(new List<double>() { 6,6,6,6,6 });
            // Dance 3
            skatingViewModel.DanceSkating[2].CoupleResults[0].Marks = this.CreateFromDoubleList(new List<double>() { 1,1,2,2,3});
            skatingViewModel.DanceSkating[2].CoupleResults[1].Marks = this.CreateFromDoubleList(new List<double>() { 3,2,1,1,2});
            skatingViewModel.DanceSkating[2].CoupleResults[2].Marks = this.CreateFromDoubleList(new List<double>() { 5,3,3,6,1 });
            skatingViewModel.DanceSkating[2].CoupleResults[3].Marks = this.CreateFromDoubleList(new List<double>() { 2,6,6,3,4});
            skatingViewModel.DanceSkating[2].CoupleResults[4].Marks = this.CreateFromDoubleList(new List<double>() { 4,4,5,5,6});
            skatingViewModel.DanceSkating[2].CoupleResults[5].Marks = this.CreateFromDoubleList(new List<double>() { 6,5,4,4,5 });
            // Dance 4
            skatingViewModel.DanceSkating[3].CoupleResults[0].Marks = this.CreateFromDoubleList(new List<double>() { 2,2,5,1,5 });
            skatingViewModel.DanceSkating[3].CoupleResults[1].Marks = this.CreateFromDoubleList(new List<double>() { 1,1,2,5,2 });
            skatingViewModel.DanceSkating[3].CoupleResults[2].Marks = this.CreateFromDoubleList(new List<double>() { 5,6,3,2,3 });
            skatingViewModel.DanceSkating[3].CoupleResults[3].Marks = this.CreateFromDoubleList(new List<double>() { 3,3,1,3,6 });
            skatingViewModel.DanceSkating[3].CoupleResults[4].Marks = this.CreateFromDoubleList(new List<double>() { 6,4,6,6,4 });
            skatingViewModel.DanceSkating[3].CoupleResults[5].Marks = this.CreateFromDoubleList(new List<double>() { 4,5,4,4,1 });
            // Dance 5
            skatingViewModel.DanceSkating[4].CoupleResults[0].Marks = this.CreateFromDoubleList(new List<double>() { 2,6,2,6,1 });
            skatingViewModel.DanceSkating[4].CoupleResults[1].Marks = this.CreateFromDoubleList(new List<double>() { 1,5,1,5,2 });
            skatingViewModel.DanceSkating[4].CoupleResults[2].Marks = this.CreateFromDoubleList(new List<double>() { 3,2,4,2,4 });
            skatingViewModel.DanceSkating[4].CoupleResults[3].Marks = this.CreateFromDoubleList(new List<double>() { 5,1,5,1,3 });
            skatingViewModel.DanceSkating[4].CoupleResults[4].Marks = this.CreateFromDoubleList(new List<double>() { 4,4,6,3,6});
            skatingViewModel.DanceSkating[4].CoupleResults[5].Marks = this.CreateFromDoubleList(new List<double>() { 6,3,3,4,5 });

            skatingViewModel.DoCalculations();

            this.DumppRule9(skatingViewModel.Rule9ViewModels);
            this.DumpRule11(skatingViewModel.Rule11ViewModel);

            Assert.IsTrue(skatingViewModel.Participants[0].PlaceFrom == 2);
            Assert.IsTrue(skatingViewModel.Participants[1].PlaceFrom == 1);
            Assert.IsTrue(skatingViewModel.Participants[2].PlaceFrom == 3);
            Assert.IsTrue(skatingViewModel.Participants[3].PlaceFrom == 4);
            Assert.IsTrue(skatingViewModel.Participants[4].PlaceFrom == 6);
            Assert.IsTrue(skatingViewModel.Participants[5].PlaceFrom == 5);

        }

        [TestMethod]
        public void SkatingRule11Test3()
        {
            var skatingViewModel = new SkatingViewModel(this.context, this.round, this.participants, this.dances.Take(4), this.judges);

            // Dance 1
            skatingViewModel.DanceSkating[0].CoupleResults[0].Marks = this.CreateFromDoubleList(new List<double>() { 1,2,1,4,1 });
            skatingViewModel.DanceSkating[0].CoupleResults[1].Marks = this.CreateFromDoubleList(new List<double>() { 5,6,5,3,6 });
            skatingViewModel.DanceSkating[0].CoupleResults[2].Marks = this.CreateFromDoubleList(new List<double>() { 2,1,2,2,3 });
            skatingViewModel.DanceSkating[0].CoupleResults[3].Marks = this.CreateFromDoubleList(new List<double>() { 3,3,4,1,5 });
            skatingViewModel.DanceSkating[0].CoupleResults[4].Marks = this.CreateFromDoubleList(new List<double>() { 4,4,3,5,4 });
            skatingViewModel.DanceSkating[0].CoupleResults[5].Marks = this.CreateFromDoubleList(new List<double>() { 6,5,6,6,2 });
            // Dance 2
            skatingViewModel.DanceSkating[1].CoupleResults[0].Marks = this.CreateFromDoubleList(new List<double>() { 3,2,2,3,4 });
            skatingViewModel.DanceSkating[1].CoupleResults[1].Marks = this.CreateFromDoubleList(new List<double>() { 5,5,6,6,6 });
            skatingViewModel.DanceSkating[1].CoupleResults[2].Marks = this.CreateFromDoubleList(new List<double>() { 1,1,1,4,1 });
            skatingViewModel.DanceSkating[1].CoupleResults[3].Marks = this.CreateFromDoubleList(new List<double>() { 2,3,4,1,5 });
            skatingViewModel.DanceSkating[1].CoupleResults[4].Marks = this.CreateFromDoubleList(new List<double>() { 4,4,3,5,2});
            skatingViewModel.DanceSkating[1].CoupleResults[5].Marks = this.CreateFromDoubleList(new List<double>() { 6,6,5,2,3 });
            // Dance 3
            skatingViewModel.DanceSkating[2].CoupleResults[0].Marks = this.CreateFromDoubleList(new List<double>() { 3,2,1,5,1});
            skatingViewModel.DanceSkating[2].CoupleResults[1].Marks = this.CreateFromDoubleList(new List<double>() { 5,5,5,4,6 });
            skatingViewModel.DanceSkating[2].CoupleResults[2].Marks = this.CreateFromDoubleList(new List<double>() { 1,1,2,2,2 });
            skatingViewModel.DanceSkating[2].CoupleResults[3].Marks = this.CreateFromDoubleList(new List<double>() { 2,4,4,1,4 });
            skatingViewModel.DanceSkating[2].CoupleResults[4].Marks = this.CreateFromDoubleList(new List<double>() { 4,3,3,3,5 });
            skatingViewModel.DanceSkating[2].CoupleResults[5].Marks = this.CreateFromDoubleList(new List<double>() { 6,6,6,6,3 });
            // Dance 4
            skatingViewModel.DanceSkating[3].CoupleResults[0].Marks = this.CreateFromDoubleList(new List<double>() { 2,2,1,3,1 });
            skatingViewModel.DanceSkating[3].CoupleResults[1].Marks = this.CreateFromDoubleList(new List<double>() { 6,6,6,5,6 });
            skatingViewModel.DanceSkating[3].CoupleResults[2].Marks = this.CreateFromDoubleList(new List<double>() { 1,1,2,2,2 });
            skatingViewModel.DanceSkating[3].CoupleResults[3].Marks = this.CreateFromDoubleList(new List<double>() { 3,4,4,1,5 });
            skatingViewModel.DanceSkating[3].CoupleResults[4].Marks = this.CreateFromDoubleList(new List<double>() { 4,3,3,4,4 });
            skatingViewModel.DanceSkating[3].CoupleResults[5].Marks = this.CreateFromDoubleList(new List<double>() { 5,5,5,6,3 });

            skatingViewModel.DoCalculations();

            Assert.IsTrue(skatingViewModel.Participants[0].PlaceFrom == 2);
            Assert.IsTrue(skatingViewModel.Participants[1].PlaceFrom == 6);
            Assert.IsTrue(skatingViewModel.Participants[2].PlaceFrom == 1);
            Assert.IsTrue(skatingViewModel.Participants[3].PlaceFrom == 3);
            Assert.IsTrue(skatingViewModel.Participants[4].PlaceFrom == 4);
            Assert.IsTrue(skatingViewModel.Participants[5].PlaceFrom == 5);
        }

        [TestMethod]
        public void TestPortugalRule11()
        {
            // We have 11 Judges
            this.judges.Add(new Official() { Sign = "F" });
            this.judges.Add(new Official() { Sign = "G" });
            this.judges.Add(new Official() { Sign = "H" });
            this.judges.Add(new Official() { Sign = "I" });
            this.judges.Add(new Official() { Sign = "J" });
            this.judges.Add(new Official() { Sign = "K" });

            var skatingViewModel = new SkatingViewModel(this.context, this.round, this.participants, this.dances, this.judges);

            skatingViewModel.DanceSkating[0].CoupleResults[0].Marks = this.CreateFromDoubleList(new List<double>() { 6, 2, 3, 2, 4, 2, 3, 2, 6, 2, 6 });
            skatingViewModel.DanceSkating[0].CoupleResults[1].Marks = this.CreateFromDoubleList(new List<double>() { 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 });
            skatingViewModel.DanceSkating[0].CoupleResults[2].Marks = this.CreateFromDoubleList(new List<double>() { 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 3 });
            skatingViewModel.DanceSkating[0].CoupleResults[3].Marks = this.CreateFromDoubleList(new List<double>() { 2, 5, 5, 6, 6, 6, 5, 5, 3, 6, 5 });
            skatingViewModel.DanceSkating[0].CoupleResults[4].Marks = this.CreateFromDoubleList(new List<double>() { 5, 6, 2, 3, 2, 4, 2, 3, 2, 3, 4 });
            skatingViewModel.DanceSkating[0].CoupleResults[5].Marks = this.CreateFromDoubleList(new List<double>() { 1, 4, 6, 5, 3, 5, 6, 6, 4, 5, 2 });


            skatingViewModel.DanceSkating[1].CoupleResults[0].Marks = this.CreateFromDoubleList(new List<double>() { 6, 1, 3, 2, 4, 3, 2, 3, 5, 2, 6 });
            skatingViewModel.DanceSkating[1].CoupleResults[1].Marks = this.CreateFromDoubleList(new List<double>() { 3, 2, 1, 1, 1, 1, 1, 1, 2, 1, 1 });
            skatingViewModel.DanceSkating[1].CoupleResults[2].Marks = this.CreateFromDoubleList(new List<double>() { 4, 3, 4, 3, 5, 2, 4, 4, 4, 3, 3 });
            skatingViewModel.DanceSkating[1].CoupleResults[3].Marks = this.CreateFromDoubleList(new List<double>() { 2, 6, 5, 6, 6, 6, 6, 5, 3, 6, 5 });
            skatingViewModel.DanceSkating[1].CoupleResults[4].Marks = this.CreateFromDoubleList(new List<double>() { 5, 4, 2, 4, 2, 5, 3, 2, 1, 4, 4 });
            skatingViewModel.DanceSkating[1].CoupleResults[5].Marks = this.CreateFromDoubleList(new List<double>() { 1, 5, 6, 5, 3, 4, 5, 6, 6, 5, 2 });



            skatingViewModel.DanceSkating[2].CoupleResults[0].Marks = this.CreateFromDoubleList(new List<double>() { 6, 1, 4, 3, 6, 2, 2, 4, 5, 2, 6 });
            skatingViewModel.DanceSkating[2].CoupleResults[1].Marks = this.CreateFromDoubleList(new List<double>() { 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1 });
            skatingViewModel.DanceSkating[2].CoupleResults[2].Marks = this.CreateFromDoubleList(new List<double>() { 5, 3, 3, 4, 5, 3, 4, 3, 4, 4, 3 });
            skatingViewModel.DanceSkating[2].CoupleResults[3].Marks = this.CreateFromDoubleList(new List<double>() { 2, 6, 6, 6, 4, 5, 5, 6, 3, 5, 5 });
            skatingViewModel.DanceSkating[2].CoupleResults[4].Marks = this.CreateFromDoubleList(new List<double>() { 4, 4, 2, 2, 2, 6, 3, 2, 2, 3, 4 });
            skatingViewModel.DanceSkating[2].CoupleResults[5].Marks = this.CreateFromDoubleList(new List<double>() { 3, 5, 5, 5, 3, 4, 6, 5, 6, 6, 2 });



            skatingViewModel.DanceSkating[3].CoupleResults[0].Marks = this.CreateFromDoubleList(new List<double>() { 6, 2, 2, 3, 5, 3, 4, 2, 6, 2, 6 });
            skatingViewModel.DanceSkating[3].CoupleResults[1].Marks = this.CreateFromDoubleList(new List<double>() { 3, 1, 1, 1, 1, 2, 1, 1, 2, 1, 1 });
            skatingViewModel.DanceSkating[3].CoupleResults[2].Marks = this.CreateFromDoubleList(new List<double>() { 4, 3, 4, 2, 4, 1, 3, 4, 4, 3, 2 });
            skatingViewModel.DanceSkating[3].CoupleResults[3].Marks = this.CreateFromDoubleList(new List<double>() { 1, 6, 5, 6, 6, 5, 5, 5, 3, 6, 5 });
            skatingViewModel.DanceSkating[3].CoupleResults[4].Marks = this.CreateFromDoubleList(new List<double>() { 5, 4, 3, 5, 2, 6, 2, 3, 1, 4, 4 });
            skatingViewModel.DanceSkating[3].CoupleResults[5].Marks = this.CreateFromDoubleList(new List<double>() { 2, 5, 6, 4, 3, 4, 6, 6, 5, 5, 3 });



            skatingViewModel.DanceSkating[4].CoupleResults[0].Marks = this.CreateFromDoubleList(new List<double>() { 6, 2, 4, 4, 5, 3, 3, 3, 6, 2, 5 });
            skatingViewModel.DanceSkating[4].CoupleResults[1].Marks = this.CreateFromDoubleList(new List<double>() { 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 });
            skatingViewModel.DanceSkating[4].CoupleResults[2].Marks = this.CreateFromDoubleList(new List<double>() { 4, 3, 3, 2, 6, 2, 4, 4, 5, 3, 2 });
            skatingViewModel.DanceSkating[4].CoupleResults[3].Marks = this.CreateFromDoubleList(new List<double>() { 1, 5, 6, 6, 4, 5, 5, 5, 3, 6, 6 });
            skatingViewModel.DanceSkating[4].CoupleResults[4].Marks = this.CreateFromDoubleList(new List<double>() { 5, 4, 2, 3, 2, 4, 2, 2, 2, 4, 4 });
            skatingViewModel.DanceSkating[4].CoupleResults[5].Marks = this.CreateFromDoubleList(new List<double>() { 3, 6, 5, 5, 3, 6, 6, 6, 4, 5, 3 });

            skatingViewModel.DoCalculations();

            this.DumppRule9(skatingViewModel.Rule9ViewModels);
            this.DumpRule10(skatingViewModel.Rule10ViewModel);
            this.DumpRule11(skatingViewModel.Rule11ViewModel);

            Assert.IsTrue(skatingViewModel.Participants[0].PlaceFrom == 3);
            Assert.IsTrue(skatingViewModel.Participants[1].PlaceFrom == 1);
            Assert.IsTrue(skatingViewModel.Participants[2].PlaceFrom == 4);
            Assert.IsTrue(skatingViewModel.Participants[3].PlaceFrom == 6);
            Assert.IsTrue(skatingViewModel.Participants[4].PlaceFrom == 2);
            Assert.IsTrue(skatingViewModel.Participants[5].PlaceFrom == 5);
        }

        [TestMethod]
        public void Rule10With3SameSumTest()
        {
            this.judges.Add(new Official() { Sign = "F"});
            this.judges.Add(new Official() { Sign = "G" });
            this.dances.Add(new DanceInRound() { Dance = new Dance() { DanceName = "SB " }});
            this.dances.Add(new DanceInRound() { Dance = new Dance() { DanceName = "CC " } });
            this.dances.Add(new DanceInRound() { Dance = new Dance() { DanceName = "RB " } });
            this.participants = new List<Participant>()
            {
                new Participant() {Number = 12, Id = 12},
                new Participant() {Number = 13, Id = 13},
                new Participant() {Number = 14, Id = 14},
            };

            var skatingViewModel = new SkatingViewModel(this.context, this.round, this.participants, this.dances, this.judges);

            skatingViewModel.DanceSkating[0].CoupleResults[0].Marks = this.CreateFromDoubleList(new List<double>() { 3, 3, 3, 3, 3, 3, 3});
            skatingViewModel.DanceSkating[0].CoupleResults[1].Marks = this.CreateFromDoubleList(new List<double>() { 1, 1, 1, 1, 1, 1, 1});
            skatingViewModel.DanceSkating[0].CoupleResults[2].Marks = this.CreateFromDoubleList(new List<double>() { 2, 2, 2, 2, 2, 2, 2});

            skatingViewModel.DanceSkating[1].CoupleResults[0].Marks = this.CreateFromDoubleList(new List<double>() { 3, 3, 3, 3, 3, 3, 2 });
            skatingViewModel.DanceSkating[1].CoupleResults[1].Marks = this.CreateFromDoubleList(new List<double>() { 1, 1, 1, 1, 1, 1, 1 });
            skatingViewModel.DanceSkating[1].CoupleResults[2].Marks = this.CreateFromDoubleList(new List<double>() { 2, 2, 2, 2, 2, 2, 3 });

            skatingViewModel.DanceSkating[2].CoupleResults[0].Marks = this.CreateFromDoubleList(new List<double>() { 1, 1, 1, 1, 1, 1, 1 });
            skatingViewModel.DanceSkating[2].CoupleResults[1].Marks = this.CreateFromDoubleList(new List<double>() { 2, 2, 3, 3, 3, 3, 2 });
            skatingViewModel.DanceSkating[2].CoupleResults[2].Marks = this.CreateFromDoubleList(new List<double>() { 3, 3, 2, 2, 2, 2, 3 });

            skatingViewModel.DanceSkating[3].CoupleResults[0].Marks = this.CreateFromDoubleList(new List<double>() { 1, 1, 1, 1, 1, 1, 1 });
            skatingViewModel.DanceSkating[3].CoupleResults[1].Marks = this.CreateFromDoubleList(new List<double>() { 3, 3, 3, 3, 3, 2, 3 });
            skatingViewModel.DanceSkating[3].CoupleResults[2].Marks = this.CreateFromDoubleList(new List<double>() { 2, 2, 2, 2, 2, 3, 2 });

            skatingViewModel.DanceSkating[4].CoupleResults[0].Marks = this.CreateFromDoubleList(new List<double>() { 1, 2, 2, 1, 1, 1, 1 });
            skatingViewModel.DanceSkating[4].CoupleResults[1].Marks = this.CreateFromDoubleList(new List<double>() { 2, 1, 3, 2, 2, 2, 2 });
            skatingViewModel.DanceSkating[4].CoupleResults[2].Marks = this.CreateFromDoubleList(new List<double>() { 3, 3, 1, 3, 3, 3, 3 });

            skatingViewModel.DanceSkating[5].CoupleResults[0].Marks = this.CreateFromDoubleList(new List<double>() { 3, 2, 3, 3, 2, 3, 3 });
            skatingViewModel.DanceSkating[5].CoupleResults[1].Marks = this.CreateFromDoubleList(new List<double>() { 2, 3, 2, 2, 3, 2, 2 });
            skatingViewModel.DanceSkating[5].CoupleResults[2].Marks = this.CreateFromDoubleList(new List<double>() { 1, 1, 1, 1, 1, 1, 1 });

            skatingViewModel.DanceSkating[6].CoupleResults[0].Marks = this.CreateFromDoubleList(new List<double>() { 1, 1, 1, 1, 1, 1, 1 });
            skatingViewModel.DanceSkating[6].CoupleResults[1].Marks = this.CreateFromDoubleList(new List<double>() { 3, 3, 3, 3, 3, 2, 2 });
            skatingViewModel.DanceSkating[6].CoupleResults[2].Marks = this.CreateFromDoubleList(new List<double>() { 2, 2, 2, 2, 2, 3, 3 });

            skatingViewModel.DanceSkating[7].CoupleResults[0].Marks = this.CreateFromDoubleList(new List<double>() { 3, 2, 3, 3, 3, 3, 3 });
            skatingViewModel.DanceSkating[7].CoupleResults[1].Marks = this.CreateFromDoubleList(new List<double>() { 1, 1, 1, 1, 1, 1, 1 });
            skatingViewModel.DanceSkating[7].CoupleResults[2].Marks = this.CreateFromDoubleList(new List<double>() { 2, 3, 2, 2, 2, 2, 2 });

            skatingViewModel.DoCalculations();

            this.DumppRule9(skatingViewModel.Rule9ViewModels);
            this.DumpRule10(skatingViewModel.Rule10ViewModel);
            this.DumpRule11(skatingViewModel.Rule11ViewModel);

            Assert.IsTrue(skatingViewModel.Participants[0].PlaceFrom == 1);
            Assert.IsTrue(skatingViewModel.Participants[1].PlaceFrom == 3);
            Assert.IsTrue(skatingViewModel.Participants[2].PlaceFrom == 2);
        }

        private List<Marking> CreateFromDoubleList(IEnumerable<double> marks)
        {
            return marks.Select(d => new Marking(null, null, null, null) { Mark = Convert.ToInt32(d) }).ToList();
        }

        private void DumpRule11(List<CoupleResult> results)
        {
            Debug.WriteLine("Rule 11 Dump");
            foreach (var source in results.OrderBy(r => r.Participant.Number))
            {
                Debug.Write(string.Format("{0}\t", source.Participant.Number));
                for(var i=0;i<source.NumberPlaces.Count;i++)
                {
                    Debug.Write(String.Format("{0} ({1})\t", source.NumberPlaces[i], source.Sums[i]));
                }
                Debug.WriteLine(" - {0}", source.Place);
            }
        }

        private void DumpRule10(List<CoupleResult> results)
        {
            Debug.WriteLine("Rule 10 Dump");
            foreach (var source in results.OrderBy(r => r.Participant.Number))
            {
                Debug.Write(string.Format("{0}\t", source.Participant.Number));
                for (var i = 0; i < source.NumberPlaces.Count; i++)
                {
                    Debug.Write(String.Format("{0} ({1})\t", source.NumberPlaces[i], source.Sums[i]));
                }
                Debug.WriteLine(" - {0}", source.Place);
            }
        }

        private void DumppRule9(IEnumerable<Rule9ViewModel> results)
        {
            Debug.WriteLine("Rule 9 Dump");
            foreach (var source in results.OrderBy(r => r.Participant.Number))
            {
                Debug.Write(string.Format("{0}\t", source.Participant.Number));
                for (var i = 0; i < source.PointsInDances.Count(); i++)
                {
                    Debug.Write(String.Format("{0}\t", source.PointsInDances.ToList()[i]));
                }
                Debug.WriteLine(source.Sum);
            }
        }
    }
}
