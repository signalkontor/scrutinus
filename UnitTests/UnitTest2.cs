﻿// // TPS.net TPS8 ViewModelTest
// // UnitTest2.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.IO;
using System.Linq;
using DataModel.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Scrutinus.Dialogs.DialogViewModels;

namespace UnitTests
{
    [TestClass]
    public class UnitTest2
    {
        private ScrutinusContext context;

        [TestInitialize]
        public void Initialize()
        {
            if (File.Exists("c:\\temp\\testdb.sdf"))
            {
                File.Delete("c:\\temp\\testdb.sdf");
            }

            this.context = new ScrutinusContext("c:\\temp\\testdb.sdf", "DataModel.InitialData.DTV.xml");

            var officials = this.context.Officials.ToList();
        }

        [TestMethod]
        public void TestMethod1()
        {
            var viewModel = new ImportLegacyTpsViewModel();

            viewModel.TpsCompetitionPath = @"C:\TPS7\DatFiles\Michel-Pokale_2015";

            viewModel.ImportLegacyTpsCommand.Execute(null);
        }
    }
}
