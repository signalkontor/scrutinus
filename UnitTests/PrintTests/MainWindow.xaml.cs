﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using DataModel.Models;

using General.Rules.Skating;
using General.Rules.Skating.ViewModels;

using Scrutinus.Reports.BasicPrinting;
using Scrutinus.Reports.BasicPrinting.CompetitionPrinting;
using Scrutinus.Reports.BasicPrinting.RoundPrinting;

namespace PrintTests
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            var context = new ScrutinusContext();
            var roundId = 4;
            var competitionId = 2;

            var competition = context.Competitions.Single(c => c.Id == competitionId);

            var competitionReportApendix = new CompetitionReportAppendix(competition);
            competitionReportApendix.CreateReport();
            this.DocumentViewer.Document = competitionReportApendix.Document;

            return;

            var eventData = new Event()
                               {
                                   Title = "Die Ostsee tanzt 2014",
                                   DateFrom = DateTime.Now,
                                   Federation = "DTV",
                                   Organizer = "TSG test Osterhorn",
                                   Place = "Osterhorn",
                                   Competitions =  context.Competitions.ToList()
                               };

            var coverPagePrinter = new CoverPagePrinter(eventData, context);
            coverPagePrinter.CreateReport();
            this.DocumentViewer.Document = coverPagePrinter.Document;
            return;

           
        }

        private void CreateSkatingSumsAndCounts(SkatingFinalDanceViewModel skatingFinalDanceViewModel)
        {
            // Calculate place for this dance ...
           
        }
    }

    
}
