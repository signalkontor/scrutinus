﻿// // TPS.net TPS8 General
// // JsonHelper.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Text;
using Newtonsoft.Json;

namespace General
{
    /// <summary>
    /// </summary>
    public static class JsonHelper
    {
        #region Public Methods and Operators

        /// <summary>
        /// Deserializes the data.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <returns></returns>
        public static T DeserializeData<T>(byte[] data)
        {
            var encoder = Encoding.UTF8;
            var json = encoder.GetString(data);

            return DeserializeData<T>(json);
        }

        public static T DeserializeData<T>(string json)
        {
            JsonConvert.DefaultSettings = () =>
            {
                var settings = new JsonSerializerSettings();
                return settings;
            };

            var dataObject = JsonConvert.DeserializeObject<T>(
                json,
                new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All });

            return dataObject;
        }

        /// <summary>
        /// </summary>
        /// <param name="objectToSerialize">
        /// </param>
        /// <returns>
        /// </returns>
        public static string SerializeObject(object objectToSerialize, Func<string, string> transformJsonStringAction = null)
        {
            JsonConvert.DefaultSettings = () =>
                {
                    var settings = new JsonSerializerSettings();
                    
                    return settings;
                };

           

            var data = JsonConvert.SerializeObject(
                objectToSerialize, 
                Formatting.Indented, 
                new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.None  });

            if (transformJsonStringAction != null)
            {
                data = transformJsonStringAction(data);
            }

            return data;
        }

        #endregion
    }
}