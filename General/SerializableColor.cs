﻿// // TPS.net TPS8 General
// // SerializableColor.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Windows.Media;

namespace General
{
    [Serializable]
    public class SerializableColor : NotificationObject
    {
        #region Constructors and Destructors

        [NonSerialized]
        private Color color = new Color();

        /// <summary>
        /// Initializes a new instance of the <see cref="SerializableColor"/> class.
        /// </summary>
        public SerializableColor()
        {
            this.color = new Color();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SerializableColor"/> class.
        /// </summary>
        /// <param name="color">The color.</param>
        public SerializableColor(Color color)
        {
            this.R = color.R;
            this.G = color.G;
            this.B = color.B;
            this.A = color.A;

            this.color = color;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the r.
        /// </summary>
        /// <value>
        /// The r.
        /// </value>
        public byte R { get; set; }

        /// <summary>
        /// Gets or sets the g.
        /// </summary>
        /// <value>
        /// The g.
        /// </value>
        public byte G { get; set; }

        /// <summary>
        /// Gets or sets the b.
        /// </summary>
        /// <value>
        /// The b.
        /// </value>
        public byte B { get; set; }

        /// <summary>
        /// Gets or sets the a.
        /// </summary>
        /// <value>
        /// The a.
        /// </value>
        public byte A { get; set; }


        public Color Color
        {
            get
            {
                this.color.R = this.R;
                this.color.G = this.G;
                this.color.B = this.B;
                this.color.A = this.A;
                return this.color;
            }

            set
            {
                this.color = value;
                this.R = this.color.R;
                this.G = this.color.G;
                this.B = this.color.B;
                this.A = this.color.A;

                this.OnPropertyChanged("Color");
            }
        }

        public void RaiseEvent()
        {
            this.OnPropertyChanged("Color");
        }

        #endregion

        #region Methods

        #endregion
    }
}
