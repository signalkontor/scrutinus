﻿// // TPS.net TPS8 General
// // IReportProgress.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Threading.Tasks;
using MahApps.Metro.Controls.Dialogs;

namespace General.Interfaces
{
    /// <summary>
    /// Interface for status reports in frontend (progress bar)
    /// </summary>
    public interface IReportProgress
    {
        MessageDialogResult ShowMessage(string message);

        /// <summary>
        /// Reports the status.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="progress">The progress.</param>
        /// <param name="max">The maximum.</param>
        void ReportStatus(string message, int progress, int max);

        /// <summary>
        /// Removes the status report.
        /// </summary>
        void RemoveStatusReport();

        Task<ProgressDialogController> OpenProgressBarAsync(string title, string message);

        ProgressDialogController OpenProgressBar(string title, string message);
    }
}
