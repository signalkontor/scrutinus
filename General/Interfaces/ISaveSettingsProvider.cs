﻿// // TPS.net TPS8 General
// // ISaveSettingsProvider.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

namespace General.Interfaces
{
    public interface ISaveSettingsProvider
    {
        void SaveSettings();
    }
}
