﻿// // TPS.net TPS8 General
// // ApiClientBase.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.IO;
using System.Net;
using System.Text;
using Newtonsoft.Json;

namespace General.ApiClient
{
    public class WebClient : System.Net.WebClient
    {
        public int TimeoutInMilliSeconds { get; set; }

        protected override WebRequest GetWebRequest(Uri address)
        {
            var request = base.GetWebRequest(address);

            if (request == null)
            {
                return null;
            }

            request.Timeout = this.TimeoutInMilliSeconds;

            if (request is HttpWebRequest httpRequest)
            {
                httpRequest.ReadWriteTimeout = TimeoutInMilliSeconds;
            }

            return request;
        }
    }

    public static class ApiClientBase
    {
        public static T GetData<T>(string url, IWebProxy proxy = null, string userName = null, string password = null, bool useNTLM = true)
        {
            using (WebClient webClient = new WebClient())
            {
                ConfigureWebClient<T>(url, proxy, userName, password, useNTLM, webClient);

                try
                {
                    string jsonReply = webClient.DownloadString(url);
                    // Deserialize the json response
                    T response = JsonConvert.DeserializeObject<T>(jsonReply);

                    return response;
                }
                catch (WebException webException)
                {
                    var responseStream = webException.Response?.GetResponseStream();

                    if (responseStream != null)
                    {
                        using (var reader = new StreamReader(responseStream))
                        {
                            var responseText = reader.ReadToEnd();
                            throw new WebException(responseText);
                        }
                    }

                    throw;
                }
            }
        }

        public static T PostDataAsync<T>(string url, object data, IWebProxy proxy = null, string userName = null,
            string password = null, bool useNTLM = true)
        {
            using (var webClient = new WebClient())
            {
                ConfigureWebClient<T>(url, proxy, userName, password, useNTLM, webClient);

                try
                {
                    string serialized = JsonConvert.SerializeObject(data);
                    string jsonReply = webClient.UploadString(url, serialized);

                    var response = JsonConvert.DeserializeObject<T>(jsonReply);

                    return response;
                }
                catch (WebException webException)
                {
                    var responseStream = webException.Response?.GetResponseStream();

                    if (responseStream != null)
                    {
                        using (var reader = new StreamReader(responseStream))
                        {
                            var responseText = reader.ReadToEnd();
                            throw new WebException(responseText);
                        }
                    }

                    throw;
                }
            }
        }

        private static void ConfigureWebClient<T>(string url, IWebProxy proxy, string userName, string password,
            bool useNTLM, WebClient webClient)
        {
            if (userName != null && password != null)
            {
                if (useNTLM)
                {
                    var credentialCache = new CredentialCache
                    {
                        {new Uri(url), "NTLM", new NetworkCredential()} // (userName, password, domain)}
                    };

                    webClient.Credentials = credentialCache;
                }
                else
                {
                    string credentials = Convert.ToBase64String(
                        Encoding.ASCII.GetBytes(userName + ":" + password));
                    webClient.Headers[HttpRequestHeader.Authorization] = $"Basic {credentials}";
                }
            }
            else
            {
                if (useNTLM)
                {
                    NetworkCredential cred = CredentialCache.DefaultNetworkCredentials;
                    webClient.Credentials = cred;
                }
            }

            webClient.TimeoutInMilliSeconds = 60000;
            webClient.Proxy = proxy;
            webClient.Headers["Content-Type"] = "application/json";
            webClient.Encoding = Encoding.UTF8;
        }
    }
}
