﻿// // TPS.net TPS8 General
// // StringExtensions.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Threading;

namespace General.Extensions
{
    public static class StringExtensions
    {
        public static double SaveParse(this string source)
        {
            var decimalChar = Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator;

            var input = source.Replace(".", decimalChar).Replace(",", decimalChar);

            return double.Parse(input);
        }
    }
}
