﻿// // TPS.net TPS8 General
// // FocusExtension.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Windows;

namespace General.Extensions
{
    public static class FocusExtension
    {
        public static readonly DependencyProperty IsFocusedProperty =
            DependencyProperty.RegisterAttached(
             "IsFocused", typeof(bool), typeof(FocusExtension),
             new UIPropertyMetadata(false, OnIsFocusedPropertyChanged));

        public static bool GetIsFocused(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsFocusedProperty);
        }


        public static void SetIsFocused(DependencyObject obj, bool value)
        {
            obj.SetValue(IsFocusedProperty, value);
        }


        private static void OnIsFocusedPropertyChanged(DependencyObject d,
            DependencyPropertyChangedEventArgs e)
        {
            var uie = (UIElement)d;
            if ((bool)e.NewValue)
            {
                uie.Focus(); // Don't care about false values.
                uie.LostFocus -= OnLostFocus;
                uie.LostFocus += OnLostFocus;
            }


        }

        private static void OnLostFocus(object sender, RoutedEventArgs routedEventArgs)
        {
            SetIsFocused((DependencyObject)sender, false);
            var uie = (UIElement)sender;
            if (uie != null)
            {
                uie.LostFocus -= OnLostFocus;
            }
        }
    }
}
