﻿// // TPS.net TPS8 General
// // ObservableCollectionExtensions.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace General.Extensions
{
    public static class Extend
    {
        public static void AddRange<T>(
            this ObservableCollection<T> observableCollection,
            IEnumerable<T> list)
        {
            foreach (var item in list)
            {
                observableCollection.Add(item);
            }
        }
    }
}