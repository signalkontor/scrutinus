﻿// // TPS.net TPS8 General
// // FilteredObservableCollection.cs
// // Last Changed: 2017/06/09
// // (c) 2018 Olav Gröhn / signalkontor GmbH

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;

namespace General
{
    public class FilteredObservableCollection<T> : IList<T>, ICollection<T>, IEnumerable<T>, IList, ICollection, IEnumerable, INotifyCollectionChanged, INotifyPropertyChanged
    {
        private readonly ObservableCollection<T> collection;
        private Predicate<T> filter;

        public FilteredObservableCollection(ObservableCollection<T> collection)
        {
            this.filter = null;
            this.collection = collection;
            this.collection.CollectionChanged += new NotifyCollectionChangedEventHandler(this.OnCollectionChanged);
            ((INotifyPropertyChanged)collection).PropertyChanged += new PropertyChangedEventHandler(this.OnPropertyChanged);
        }

        public Predicate<T> Filter
        {
            get { return this.filter; }
            set { this.filter = value; }
        }

        public int Add(object value)
        {
            this.Add((T)value);
            return this.IndexOf(value);
        }

        public bool Contains(object value)
        {
            if (this.filter != null && this.filter((T)value) == false)
            {
                return false;
            }
            return ((IList) this.collection).Contains(value);
        }

        public void CopyTo(Array array, int index)
        {
            if (this.filter == null)
            {
                ((IList) this.collection).CopyTo(array, index);
            }
            else
            {
                for (var i = 0; i < this.collection.Count; i++)
                {
                    var item = this.collection[i];
                    if (this.filter(item))
                    {
                        array.SetValue(item, index++);
                    }
                }
            }
        }

        public int IndexOf(object value)
        {
            if (value is T)
            {
                return this.IndexOf((T)value);
            }
            else
            {
                return -1;
            }
        }

        public void Insert(int index, object value)
        {
            this.Insert(index, (T)value);
        }

        public bool IsFixedSize
        {
            get { return ((IList) this.collection).IsFixedSize; }
        }

        public bool IsSynchronized
        {
            get { return ((IList) this.collection).IsSynchronized; }
        }

        public void Remove(object value)
        {
            if (!(value is T) || this.filter != null && this.filter((T)value) == false)
            {
                return;
            }

            ((IList) this.collection).Remove(value);
        }

        public object SyncRoot
        {
            get { return ((IList) this.collection).SyncRoot; }
        }

        object IList.this[int index]
        {
            get
            {
                return (object)this[index];
            }
            set
            {
                this[index] = (T)value;
            }
        }

        public void Add(T item)
        {
            if (this.filter != null && this.filter(item) == false)
            {
                throw new InvalidOperationException();
            }
            this.collection.Add(item);
        }

        public void Clear()
        {
            if (this.filter == null)
            {
                this.collection.Clear();
            }
            else
            {
                for (var i = 0; i < this.collection.Count; )
                {
                    var item = this.collection[i];
                    if (this.filter(item))
                    {
                        this.collection.RemoveAt(i);
                    }
                    else
                    {
                        i++;
                    }
                }
            }
        }

        public bool Contains(T item)
        {
            if (this.filter != null && this.filter(item) == false)
            {
                return false;
            }
            return this.collection.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            if (this.filter == null)
            {
                this.collection.CopyTo(array, arrayIndex);
            }
            else
            {
                for (var i = 0; i < this.collection.Count; i++)
                {
                    var item = this.collection[i];
                    if (this.filter(item))
                    {
                        array[arrayIndex++] = item;
                    }
                }
            }
        }

        public int Count
        {
            get
            {
                if (this.filter == null)
                {
                    return this.collection.Count;
                }
                else
                {
                    var count = 0;
                    for (var i = 0; i < this.collection.Count; i++)
                    {
                        var item = this.collection[i];
                        if (this.filter(item))
                        {
                            count++;
                        }
                    }
                    return count;
                }
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            return new FilteredEnumerator(this, this.collection.GetEnumerator());
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return new FilteredEnumerator(this, this.collection.GetEnumerator());
        }

        public int IndexOf(T item)
        {
            if (this.filter == null)
            {
                return this.collection.IndexOf(item);
            }
            else
            {
                var count = 0;
                for (var i = 0; i < this.collection.Count; i++)
                {
                    var indexitem = this.collection[i];
                    if (this.filter(indexitem))
                    {
                        if (indexitem.Equals(item))
                        {
                            return count;
                        }
                        else
                        {
                            count++;
                        }
                    }
                }
                return -1;
            }
        }

        public void Insert(int index, T item)
        {
            if (this.filter != null && this.filter(item) == false)
            {
                throw new InvalidOperationException();
            }

            if (this.filter == null || index == 0)
            {
                this.collection.Insert(index, item);
            }
            else
            {
                for (var i = 0; i < this.collection.Count; i++)
                {
                    var indexitem = this.collection[i];
                    if (this.filter(indexitem))
                    {
                        index--;
                        if (index == 0)
                        {
                            this.collection.Insert(i + 1, item);
                            return;
                        }
                    }
                }
                throw new ArgumentOutOfRangeException();
            }
        }

        public bool IsReadOnly
        {
            get { return ((IList) this.collection).IsReadOnly; }
        }

        public bool Remove(T item)
        {
            if (this.filter != null && this.filter(item) == false)
            {
                return false;
            }

            return this.collection.Remove(item);
        }

        public void RemoveAt(int index)
        {
            if (this.filter == null)
            {
                this.collection.RemoveAt(index);
            }
            else
            {
                for (var i = 0; i < this.collection.Count; i++)
                {
                    var indexitem = this.collection[i];
                    if (this.filter(indexitem))
                    {
                        if (index == 0)
                        {
                            this.collection.RemoveAt(i);
                            return;
                        }
                        else
                        {
                            index--;
                        }
                    }
                }
                throw new ArgumentOutOfRangeException();
            }
        }

        public T this[int index]
        {
            get
            {
                if (this.filter == null)
                {
                    return this.collection[index];
                }
                else
                {
                    for (var i = 0; i < this.collection.Count; i++)
                    {
                        var indexitem = this.collection[i];
                        if (this.filter(indexitem))
                        {
                            if (index == 0)
                            {
                                return indexitem;
                            }
                            else
                            {
                                index--;
                            }
                        }
                    }
                    throw new ArgumentOutOfRangeException();
                }
            }
            set
            {
                if (this.filter == null)
                {
                    this.collection[index] = value;
                }
                else if (this.filter(value) == false)
                {
                    throw new InvalidOperationException();
                }
                else
                {
                    for (var i = 0; i < this.collection.Count; i++)
                    {
                        var indexitem = this.collection[i];
                        if (this.filter(indexitem))
                        {
                            if (index == 0)
                            {
                                this.collection[i] = value;
                            }
                            else
                            {
                                index--;
                            }
                        }
                    }
                    throw new ArgumentOutOfRangeException();
                }
            }
        }

        public event NotifyCollectionChangedEventHandler CollectionChanged
        {
            add { this.collectionchanged += value; }
            remove { this.collectionchanged -= value; }
        }

        public event PropertyChangedEventHandler PropertyChanged
        {
            add { this.propertychanged += value; }
            remove { this.propertychanged -= value; }
        }

        private event NotifyCollectionChangedEventHandler collectionchanged;
        private event PropertyChangedEventHandler propertychanged;

        private void OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (this.collectionchanged != null)
            {
                // Check the NewItems
                var newlist = new List<T>();
                if (e.NewItems != null)
                {
                    foreach (T item in e.NewItems)
                    {
                        if (this.filter(item) == true)
                        {
                            newlist.Add(item);
                        }
                    }
                }

                // Check the OldItems
                var oldlist = new List<T>();
                if (e.OldItems != null)
                {
                    foreach (T item in e.OldItems)
                    {
                        if (this.filter(item) == true)
                        {
                            oldlist.Add(item);
                        }
                    }
                }

                // Create the Add/Remove/Replace lists
                var addlist = new List<T>();
                var removelist = new List<T>();
                var replacelist = new List<T>();

                // Fill the Add/Remove/Replace lists
                foreach (var item in newlist)
                {
                    if (oldlist.Contains(item))
                    {
                        replacelist.Add(item);
                    }
                    else
                    {
                        addlist.Add(item);
                    }
                }
                foreach (var item in oldlist)
                {
                    if (newlist.Contains(item))
                    {
                        continue;
                    }
                    else
                    {
                        removelist.Add(item);
                    }
                }

                // Send the corrected event
                switch (e.Action)
                {
                    case NotifyCollectionChangedAction.Add:
                    case NotifyCollectionChangedAction.Move:
                    case NotifyCollectionChangedAction.Remove:
                    case NotifyCollectionChangedAction.Replace:
                        if (addlist.Count > 0)
                        {
                            this.collectionchanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, addlist));
                        }
                        if (replacelist.Count > 0)
                        {
                            this.collectionchanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, replacelist));
                        }
                        if (removelist.Count > 0)
                        {
                            this.collectionchanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, removelist));
                        }
                        break;
                    case NotifyCollectionChangedAction.Reset:
                        this.collectionchanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
                        break;
                }
            }
        }

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (this.propertychanged != null)
            {
                this.propertychanged(this, e);
            }
        }

        private class FilteredEnumerator : IEnumerator<T>, IEnumerator
        {
            private readonly IEnumerator<T> _enumerator;
            private readonly FilteredObservableCollection<T> _filteredcollection;

            public FilteredEnumerator(FilteredObservableCollection<T> filteredcollection, IEnumerator<T> enumerator)
            {
                this._filteredcollection = filteredcollection;
                this._enumerator = enumerator;
            }

            public T Current
            {
                get
                {
                    if (this._filteredcollection.Filter == null)
                    {
                        return this._enumerator.Current;
                    }
                    else if (this._filteredcollection.Filter(this._enumerator.Current) == false)
                    {
                        throw new InvalidOperationException();
                    }
                    else
                    {
                        return this._enumerator.Current;
                    }
                }
            }

            public void Dispose()
            {
                this._enumerator.Dispose();
            }

            object IEnumerator.Current
            {
                get { return this.Current; }
            }

            public bool MoveNext()
            {
                while (true)
                {
                    if (this._enumerator.MoveNext() == false)
                    {
                        return false;
                    }
                    if (this._filteredcollection.Filter == null
                        || this._filteredcollection.Filter(this._enumerator.Current) == true)
                    {
                        return true;
                    }
                }
            }

            public void Reset()
            {
                this._enumerator.Reset();
            }
        }
    }
}
